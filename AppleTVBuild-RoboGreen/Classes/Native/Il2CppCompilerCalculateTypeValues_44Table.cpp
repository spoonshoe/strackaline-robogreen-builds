﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.Data.Mapping.ControllerMap_Editor
struct ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7;
// Rewired.Data.UserData
struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/JZZyLMAuMxwZaOpbKUYlhMNhVLy
struct JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/JZZyLMAuMxwZaOpbKUYlhMNhVLy/pAOjJqrifZlhrwZoIIsNVLCBaIbb
struct pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/UbGBLdiYFAdtrLoihmYVEnHzVpJE
struct UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/UbGBLdiYFAdtrLoihmYVEnHzVpJE/clJVBdesGVlJeiKXHbOxrlHLKNF
struct clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qEcNdMVgmNokdCmhdodTTEqmHlz
struct qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7;
// Rewired.Data.UserData/kwjCFrMFqcwBpiPxRDvjqsTVGzZ
struct kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205;
// Rewired.HardwareControllerMap_Game
struct HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3;
// Rewired.InputAction
struct InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA;
// Rewired.InputCategory
struct InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918;
// Rewired.InputMapCategory
struct InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602;
// Rewired.Interfaces.IInputManagerJoystickPublic
struct IInputManagerJoystickPublic_t4D44B08E924E115C6EE1E41987B26DB151F7C30B;
// Rewired.Internal.GUIText
struct GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920;
// Rewired.Platforms.Custom.CustomInputSource
struct CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.UnknownControllerHat[]
struct UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t8BA8E0A3600A88AC1F3656FA795732039D920E4B;
// System.Collections.Generic.List`1<Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qYkefkBebypsciWTBlnXilfwNlYB>
struct List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997;
// System.Collections.Generic.List`1<Rewired.InputLayout>
struct List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32>
struct Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FNDHWUXQPYEBJFNELFEBFQFNSFNE_TAC89E25A0A4044DEC44E232C9198937DFA1A8A76_H
#define FNDHWUXQPYEBJFNELFEBFQFNSFNE_TAC89E25A0A4044DEC44E232C9198937DFA1A8A76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FNdhWuxQpyEBjfNELfEbFQFnSFne
struct  FNdhWuxQpyEBjfNELfEbFQFnSFne_tAC89E25A0A4044DEC44E232C9198937DFA1A8A76  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FNDHWUXQPYEBJFNELFEBFQFNSFNE_TAC89E25A0A4044DEC44E232C9198937DFA1A8A76_H
#ifndef GQZUMHSWZOSWVWTJZGOFRJJKNGD_T630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB_H
#define GQZUMHSWZOSWVWTJZGOFRJJKNGD_T630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD
struct  GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB  : public RuntimeObject
{
public:
	// Rewired.InputMapCategory Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::NOArrsNdfKDShNFftfbPqYDzhpq
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 Rewired.Data.UserData_GqzUMHsWzoSwvwtJZGoFrjJkNGD::lEJfSplllLlGeeWbNJFrzDmIVEp
	int32_t ___lEJfSplllLlGeeWbNJFrzDmIVEp_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_lEJfSplllLlGeeWbNJFrzDmIVEp_6() { return static_cast<int32_t>(offsetof(GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB, ___lEJfSplllLlGeeWbNJFrzDmIVEp_6)); }
	inline int32_t get_lEJfSplllLlGeeWbNJFrzDmIVEp_6() const { return ___lEJfSplllLlGeeWbNJFrzDmIVEp_6; }
	inline int32_t* get_address_of_lEJfSplllLlGeeWbNJFrzDmIVEp_6() { return &___lEJfSplllLlGeeWbNJFrzDmIVEp_6; }
	inline void set_lEJfSplllLlGeeWbNJFrzDmIVEp_6(int32_t value)
	{
		___lEJfSplllLlGeeWbNJFrzDmIVEp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GQZUMHSWZOSWVWTJZGOFRJJKNGD_T630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB_H
#ifndef OUUQQZSLRLEIZGNMAKQFHMSWQDH_T57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2_H
#define OUUQQZSLRLEIZGNMAKQFHMSWQDH_T57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh
struct  OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2  : public RuntimeObject
{
public:
	// Rewired.InputCategory Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::NOArrsNdfKDShNFftfbPqYDzhpq
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 Rewired.Data.UserData_OuUQqZsLRLeIZgNMAKqFhmswQDh::vLHeOKbAsNDmvwORrSjMmEJWlNtc
	int32_t ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_6() { return static_cast<int32_t>(offsetof(OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2, ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_6)); }
	inline int32_t get_vLHeOKbAsNDmvwORrSjMmEJWlNtc_6() const { return ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_6; }
	inline int32_t* get_address_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_6() { return &___vLHeOKbAsNDmvwORrSjMmEJWlNtc_6; }
	inline void set_vLHeOKbAsNDmvwORrSjMmEJWlNtc_6(int32_t value)
	{
		___vLHeOKbAsNDmvwORrSjMmEJWlNtc_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUUQQZSLRLEIZGNMAKQFHMSWQDH_T57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2_H
#ifndef OXBKAAYEYNNOOKCELUKYITCDDNW_TA6BC53023F1684464BD9FA81975E720EEE926306_H
#define OXBKAAYEYNNOOKCELUKYITCDDNW_TA6BC53023F1684464BD9FA81975E720EEE926306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW
struct  OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306  : public RuntimeObject
{
public:
	// System.String Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::NOArrsNdfKDShNFftfbPqYDzhpq
	String_t* ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_4;
	// System.Int32 Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::srJAZFfULohezSrPvmIPTKgbuwb
	int32_t ___srJAZFfULohezSrPvmIPTKgbuwb_5;
	// System.Int32 Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::PwepnsvajksKbyDHCUanZMkkmVZ
	int32_t ___PwepnsvajksKbyDHCUanZMkkmVZ_6;
	// Rewired.InputAction Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::hJbwrdltDmfXNminGOuYkoMgDSs
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___hJbwrdltDmfXNminGOuYkoMgDSs_7;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_OxBKaAyEYnNOoKceLukyItcddnW::yxxNlXTPZIjLPseZGjwaIuYuFMO
	RuntimeObject* ___yxxNlXTPZIjLPseZGjwaIuYuFMO_8;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline String_t* get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline String_t** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(String_t* value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___MlHuhkxTPKDycqbMwTBkBIGihuU_4)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_4() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_4(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_4 = value;
	}

	inline static int32_t get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___srJAZFfULohezSrPvmIPTKgbuwb_5)); }
	inline int32_t get_srJAZFfULohezSrPvmIPTKgbuwb_5() const { return ___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline int32_t* get_address_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return &___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline void set_srJAZFfULohezSrPvmIPTKgbuwb_5(int32_t value)
	{
		___srJAZFfULohezSrPvmIPTKgbuwb_5 = value;
	}

	inline static int32_t get_offset_of_PwepnsvajksKbyDHCUanZMkkmVZ_6() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___PwepnsvajksKbyDHCUanZMkkmVZ_6)); }
	inline int32_t get_PwepnsvajksKbyDHCUanZMkkmVZ_6() const { return ___PwepnsvajksKbyDHCUanZMkkmVZ_6; }
	inline int32_t* get_address_of_PwepnsvajksKbyDHCUanZMkkmVZ_6() { return &___PwepnsvajksKbyDHCUanZMkkmVZ_6; }
	inline void set_PwepnsvajksKbyDHCUanZMkkmVZ_6(int32_t value)
	{
		___PwepnsvajksKbyDHCUanZMkkmVZ_6 = value;
	}

	inline static int32_t get_offset_of_hJbwrdltDmfXNminGOuYkoMgDSs_7() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___hJbwrdltDmfXNminGOuYkoMgDSs_7)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_hJbwrdltDmfXNminGOuYkoMgDSs_7() const { return ___hJbwrdltDmfXNminGOuYkoMgDSs_7; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_hJbwrdltDmfXNminGOuYkoMgDSs_7() { return &___hJbwrdltDmfXNminGOuYkoMgDSs_7; }
	inline void set_hJbwrdltDmfXNminGOuYkoMgDSs_7(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___hJbwrdltDmfXNminGOuYkoMgDSs_7 = value;
		Il2CppCodeGenWriteBarrier((&___hJbwrdltDmfXNminGOuYkoMgDSs_7), value);
	}

	inline static int32_t get_offset_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_8() { return static_cast<int32_t>(offsetof(OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306, ___yxxNlXTPZIjLPseZGjwaIuYuFMO_8)); }
	inline RuntimeObject* get_yxxNlXTPZIjLPseZGjwaIuYuFMO_8() const { return ___yxxNlXTPZIjLPseZGjwaIuYuFMO_8; }
	inline RuntimeObject** get_address_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_8() { return &___yxxNlXTPZIjLPseZGjwaIuYuFMO_8; }
	inline void set_yxxNlXTPZIjLPseZGjwaIuYuFMO_8(RuntimeObject* value)
	{
		___yxxNlXTPZIjLPseZGjwaIuYuFMO_8 = value;
		Il2CppCodeGenWriteBarrier((&___yxxNlXTPZIjLPseZGjwaIuYuFMO_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OXBKAAYEYNNOOKCELUKYITCDDNW_TA6BC53023F1684464BD9FA81975E720EEE926306_H
#ifndef PTCDSGDMLGEXBXTRVQJVLUNCYTOQ_T27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5_H
#define PTCDSGDMLGEXBXTRVQJVLUNCYTOQ_T27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ
struct  PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::jBAjmtMSOMsCuqnYPvoeEjlKSHf
	int32_t ___jBAjmtMSOMsCuqnYPvoeEjlKSHf_4;
	// Rewired.InputAction Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::qKrcCDEIdUcccmEYnTYsrCxkrWeN
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5;
	// Rewired.InputCategory Rewired.Data.UserData_PTCdsGDMlGexbXtrvQjvLUnCyToQ::fUxETuHauDmUaHCRcyXkQrxiaqoY
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___fUxETuHauDmUaHCRcyXkQrxiaqoY_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jBAjmtMSOMsCuqnYPvoeEjlKSHf_4() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___jBAjmtMSOMsCuqnYPvoeEjlKSHf_4)); }
	inline int32_t get_jBAjmtMSOMsCuqnYPvoeEjlKSHf_4() const { return ___jBAjmtMSOMsCuqnYPvoeEjlKSHf_4; }
	inline int32_t* get_address_of_jBAjmtMSOMsCuqnYPvoeEjlKSHf_4() { return &___jBAjmtMSOMsCuqnYPvoeEjlKSHf_4; }
	inline void set_jBAjmtMSOMsCuqnYPvoeEjlKSHf_4(int32_t value)
	{
		___jBAjmtMSOMsCuqnYPvoeEjlKSHf_4 = value;
	}

	inline static int32_t get_offset_of_qKrcCDEIdUcccmEYnTYsrCxkrWeN_5() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_qKrcCDEIdUcccmEYnTYsrCxkrWeN_5() const { return ___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_qKrcCDEIdUcccmEYnTYsrCxkrWeN_5() { return &___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5; }
	inline void set_qKrcCDEIdUcccmEYnTYsrCxkrWeN_5(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5 = value;
		Il2CppCodeGenWriteBarrier((&___qKrcCDEIdUcccmEYnTYsrCxkrWeN_5), value);
	}

	inline static int32_t get_offset_of_fUxETuHauDmUaHCRcyXkQrxiaqoY_6() { return static_cast<int32_t>(offsetof(PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5, ___fUxETuHauDmUaHCRcyXkQrxiaqoY_6)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_fUxETuHauDmUaHCRcyXkQrxiaqoY_6() const { return ___fUxETuHauDmUaHCRcyXkQrxiaqoY_6; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_fUxETuHauDmUaHCRcyXkQrxiaqoY_6() { return &___fUxETuHauDmUaHCRcyXkQrxiaqoY_6; }
	inline void set_fUxETuHauDmUaHCRcyXkQrxiaqoY_6(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___fUxETuHauDmUaHCRcyXkQrxiaqoY_6 = value;
		Il2CppCodeGenWriteBarrier((&___fUxETuHauDmUaHCRcyXkQrxiaqoY_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PTCDSGDMLGEXBXTRVQJVLUNCYTOQ_T27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5_H
#ifndef TVJVOSWGIBBIPIPUVMQYPJWHJLA_T84B346FF8D5D99D1875AE108B48587F068323A1C_H
#define TVJVOSWGIBBIPIPUVMQYPJWHJLA_T84B346FF8D5D99D1875AE108B48587F068323A1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA
struct  TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::NOArrsNdfKDShNFftfbPqYDzhpq
	int32_t ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_4;
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::srJAZFfULohezSrPvmIPTKgbuwb
	int32_t ___srJAZFfULohezSrPvmIPTKgbuwb_5;
	// System.Int32 Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::HkwuAIrQONIZFoeUiNNrtBfQtDc
	int32_t ___HkwuAIrQONIZFoeUiNNrtBfQtDc_6;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_TvJVOSWGIbBiPipuvmqYpjwHJlA::zIMdOktLSEteWzOxVhfbmfwLBuKh
	RuntimeObject* ___zIMdOktLSEteWzOxVhfbmfwLBuKh_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline int32_t get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline int32_t* get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(int32_t value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___MlHuhkxTPKDycqbMwTBkBIGihuU_4)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_4() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_4(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_4 = value;
	}

	inline static int32_t get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___srJAZFfULohezSrPvmIPTKgbuwb_5)); }
	inline int32_t get_srJAZFfULohezSrPvmIPTKgbuwb_5() const { return ___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline int32_t* get_address_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return &___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline void set_srJAZFfULohezSrPvmIPTKgbuwb_5(int32_t value)
	{
		___srJAZFfULohezSrPvmIPTKgbuwb_5 = value;
	}

	inline static int32_t get_offset_of_HkwuAIrQONIZFoeUiNNrtBfQtDc_6() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___HkwuAIrQONIZFoeUiNNrtBfQtDc_6)); }
	inline int32_t get_HkwuAIrQONIZFoeUiNNrtBfQtDc_6() const { return ___HkwuAIrQONIZFoeUiNNrtBfQtDc_6; }
	inline int32_t* get_address_of_HkwuAIrQONIZFoeUiNNrtBfQtDc_6() { return &___HkwuAIrQONIZFoeUiNNrtBfQtDc_6; }
	inline void set_HkwuAIrQONIZFoeUiNNrtBfQtDc_6(int32_t value)
	{
		___HkwuAIrQONIZFoeUiNNrtBfQtDc_6 = value;
	}

	inline static int32_t get_offset_of_zIMdOktLSEteWzOxVhfbmfwLBuKh_7() { return static_cast<int32_t>(offsetof(TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C, ___zIMdOktLSEteWzOxVhfbmfwLBuKh_7)); }
	inline RuntimeObject* get_zIMdOktLSEteWzOxVhfbmfwLBuKh_7() const { return ___zIMdOktLSEteWzOxVhfbmfwLBuKh_7; }
	inline RuntimeObject** get_address_of_zIMdOktLSEteWzOxVhfbmfwLBuKh_7() { return &___zIMdOktLSEteWzOxVhfbmfwLBuKh_7; }
	inline void set_zIMdOktLSEteWzOxVhfbmfwLBuKh_7(RuntimeObject* value)
	{
		___zIMdOktLSEteWzOxVhfbmfwLBuKh_7 = value;
		Il2CppCodeGenWriteBarrier((&___zIMdOktLSEteWzOxVhfbmfwLBuKh_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TVJVOSWGIBBIPIPUVMQYPJWHJLA_T84B346FF8D5D99D1875AE108B48587F068323A1C_H
#ifndef UHEVVDXSMRKHLWQJPKQABKYQFTG_T15AB6E575BD79808D061099240DC6652ACE11C39_H
#define UHEVVDXSMRKHLWQJPKQABKYQFTG_T15AB6E575BD79808D061099240DC6652ACE11C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg
struct  UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39  : public RuntimeObject
{
public:
	// System.String Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::NOArrsNdfKDShNFftfbPqYDzhpq
	String_t* ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_4;
	// System.Int32 Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::srJAZFfULohezSrPvmIPTKgbuwb
	int32_t ___srJAZFfULohezSrPvmIPTKgbuwb_5;
	// System.Int32 Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::ZpWyjemSTdQzezCOQdYNJqQbMR
	int32_t ___ZpWyjemSTdQzezCOQdYNJqQbMR_6;
	// Rewired.InputAction Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::znmLWYURolWNezJtEPjUdZnVhOa
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___znmLWYURolWNezJtEPjUdZnVhOa_7;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_UheVvDxsmRkHlWqjPKqAbKYqFTg::kgdXDOuwBknTdVGsGtzXGYuZhje
	RuntimeObject* ___kgdXDOuwBknTdVGsGtzXGYuZhje_8;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline String_t* get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline String_t** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(String_t* value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___MlHuhkxTPKDycqbMwTBkBIGihuU_4)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_4() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_4; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_4(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_4 = value;
	}

	inline static int32_t get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___srJAZFfULohezSrPvmIPTKgbuwb_5)); }
	inline int32_t get_srJAZFfULohezSrPvmIPTKgbuwb_5() const { return ___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline int32_t* get_address_of_srJAZFfULohezSrPvmIPTKgbuwb_5() { return &___srJAZFfULohezSrPvmIPTKgbuwb_5; }
	inline void set_srJAZFfULohezSrPvmIPTKgbuwb_5(int32_t value)
	{
		___srJAZFfULohezSrPvmIPTKgbuwb_5 = value;
	}

	inline static int32_t get_offset_of_ZpWyjemSTdQzezCOQdYNJqQbMR_6() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___ZpWyjemSTdQzezCOQdYNJqQbMR_6)); }
	inline int32_t get_ZpWyjemSTdQzezCOQdYNJqQbMR_6() const { return ___ZpWyjemSTdQzezCOQdYNJqQbMR_6; }
	inline int32_t* get_address_of_ZpWyjemSTdQzezCOQdYNJqQbMR_6() { return &___ZpWyjemSTdQzezCOQdYNJqQbMR_6; }
	inline void set_ZpWyjemSTdQzezCOQdYNJqQbMR_6(int32_t value)
	{
		___ZpWyjemSTdQzezCOQdYNJqQbMR_6 = value;
	}

	inline static int32_t get_offset_of_znmLWYURolWNezJtEPjUdZnVhOa_7() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___znmLWYURolWNezJtEPjUdZnVhOa_7)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_znmLWYURolWNezJtEPjUdZnVhOa_7() const { return ___znmLWYURolWNezJtEPjUdZnVhOa_7; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_znmLWYURolWNezJtEPjUdZnVhOa_7() { return &___znmLWYURolWNezJtEPjUdZnVhOa_7; }
	inline void set_znmLWYURolWNezJtEPjUdZnVhOa_7(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___znmLWYURolWNezJtEPjUdZnVhOa_7 = value;
		Il2CppCodeGenWriteBarrier((&___znmLWYURolWNezJtEPjUdZnVhOa_7), value);
	}

	inline static int32_t get_offset_of_kgdXDOuwBknTdVGsGtzXGYuZhje_8() { return static_cast<int32_t>(offsetof(UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39, ___kgdXDOuwBknTdVGsGtzXGYuZhje_8)); }
	inline RuntimeObject* get_kgdXDOuwBknTdVGsGtzXGYuZhje_8() const { return ___kgdXDOuwBknTdVGsGtzXGYuZhje_8; }
	inline RuntimeObject** get_address_of_kgdXDOuwBknTdVGsGtzXGYuZhje_8() { return &___kgdXDOuwBknTdVGsGtzXGYuZhje_8; }
	inline void set_kgdXDOuwBknTdVGsGtzXGYuZhje_8(RuntimeObject* value)
	{
		___kgdXDOuwBknTdVGsGtzXGYuZhje_8 = value;
		Il2CppCodeGenWriteBarrier((&___kgdXDOuwBknTdVGsGtzXGYuZhje_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UHEVVDXSMRKHLWQJPKQABKYQFTG_T15AB6E575BD79808D061099240DC6652ACE11C39_H
#ifndef WYIRMKFZUREVNYWAXJQNFDFJCSH_T9C0F7DD2D55C861797B90A228E757A51376E5178_H
#define WYIRMKFZUREVNYWAXJQNFDFJCSH_T9C0F7DD2D55C861797B90A228E757A51376E5178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh
struct  WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178  : public RuntimeObject
{
public:
	// Rewired.InputCategory Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::NOArrsNdfKDShNFftfbPqYDzhpq
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 Rewired.Data.UserData_WYirmKfzuREVnYwAXJqNFDfjCSh::pRNxTDMuDtMhsXMShfGGUiXeGQk
	int32_t ___pRNxTDMuDtMhsXMShfGGUiXeGQk_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_6() { return static_cast<int32_t>(offsetof(WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178, ___pRNxTDMuDtMhsXMShfGGUiXeGQk_6)); }
	inline int32_t get_pRNxTDMuDtMhsXMShfGGUiXeGQk_6() const { return ___pRNxTDMuDtMhsXMShfGGUiXeGQk_6; }
	inline int32_t* get_address_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_6() { return &___pRNxTDMuDtMhsXMShfGGUiXeGQk_6; }
	inline void set_pRNxTDMuDtMhsXMShfGGUiXeGQk_6(int32_t value)
	{
		___pRNxTDMuDtMhsXMShfGGUiXeGQk_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WYIRMKFZUREVNYWAXJQNFDFJCSH_T9C0F7DD2D55C861797B90A228E757A51376E5178_H
#ifndef AUGBNUNIDODGWVSJZNRLGQBCMHV_T4E73DEB7A80ECAA7149C2F24855BEB8946309DF3_H
#define AUGBNUNIDODGWVSJZNRLGQBCMHV_T4E73DEB7A80ECAA7149C2F24855BEB8946309DF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV
struct  auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::chTbMAJBwugvHvDEnzgDSlLnSqg
	int32_t ___chTbMAJBwugvHvDEnzgDSlLnSqg_4;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::WCLOpuzlAPeqPEgTxJoAeJojJdS
	int32_t ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5;
	// System.Boolean Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::PfBtErLFmgIwzViqjbjnyRbCRuw
	bool ___PfBtErLFmgIwzViqjbjnyRbCRuw_6;
	// System.Boolean Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::ZyLlrOVJdDTGzRxMxdMMRTlifBO
	bool ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7;
	// Rewired.InputCategory Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::sNnGTIgDJyPxPMNHJHvNuOFurVfh
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::srSvPxedjBeocvpioBWPvuYgtjx
	int32_t ___srSvPxedjBeocvpioBWPvuYgtjx_9;
	// Rewired.InputAction Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::hjgsuFLRTScWkajVxAiiABppxTid
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___hjgsuFLRTScWkajVxAiiABppxTid_10;
	// System.Int32 Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::EChnlGrdrUcyXYSPZFpLOLQYgAx
	int32_t ___EChnlGrdrUcyXYSPZFpLOLQYgAx_11;
	// Rewired.InputAction Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::fpbTLzCSTwYcOnnFJAuVUuFuOIP
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_auGBnUNIDodGwvsjZNRLGQbcmHV::gHOYJROlCWbVZCUTzkskuGcWOOwd
	RuntimeObject* ___gHOYJROlCWbVZCUTzkskuGcWOOwd_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___chTbMAJBwugvHvDEnzgDSlLnSqg_4)); }
	inline int32_t get_chTbMAJBwugvHvDEnzgDSlLnSqg_4() const { return ___chTbMAJBwugvHvDEnzgDSlLnSqg_4; }
	inline int32_t* get_address_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4() { return &___chTbMAJBwugvHvDEnzgDSlLnSqg_4; }
	inline void set_chTbMAJBwugvHvDEnzgDSlLnSqg_4(int32_t value)
	{
		___chTbMAJBwugvHvDEnzgDSlLnSqg_4 = value;
	}

	inline static int32_t get_offset_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5)); }
	inline int32_t get_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() const { return ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5; }
	inline int32_t* get_address_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() { return &___WCLOpuzlAPeqPEgTxJoAeJojJdS_5; }
	inline void set_WCLOpuzlAPeqPEgTxJoAeJojJdS_5(int32_t value)
	{
		___WCLOpuzlAPeqPEgTxJoAeJojJdS_5 = value;
	}

	inline static int32_t get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___PfBtErLFmgIwzViqjbjnyRbCRuw_6)); }
	inline bool get_PfBtErLFmgIwzViqjbjnyRbCRuw_6() const { return ___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline bool* get_address_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return &___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline void set_PfBtErLFmgIwzViqjbjnyRbCRuw_6(bool value)
	{
		___PfBtErLFmgIwzViqjbjnyRbCRuw_6 = value;
	}

	inline static int32_t get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7)); }
	inline bool get_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() const { return ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline bool* get_address_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return &___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline void set_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(bool value)
	{
		___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7 = value;
	}

	inline static int32_t get_offset_of_sNnGTIgDJyPxPMNHJHvNuOFurVfh_8() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_sNnGTIgDJyPxPMNHJHvNuOFurVfh_8() const { return ___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_sNnGTIgDJyPxPMNHJHvNuOFurVfh_8() { return &___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8; }
	inline void set_sNnGTIgDJyPxPMNHJHvNuOFurVfh_8(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8 = value;
		Il2CppCodeGenWriteBarrier((&___sNnGTIgDJyPxPMNHJHvNuOFurVfh_8), value);
	}

	inline static int32_t get_offset_of_srSvPxedjBeocvpioBWPvuYgtjx_9() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___srSvPxedjBeocvpioBWPvuYgtjx_9)); }
	inline int32_t get_srSvPxedjBeocvpioBWPvuYgtjx_9() const { return ___srSvPxedjBeocvpioBWPvuYgtjx_9; }
	inline int32_t* get_address_of_srSvPxedjBeocvpioBWPvuYgtjx_9() { return &___srSvPxedjBeocvpioBWPvuYgtjx_9; }
	inline void set_srSvPxedjBeocvpioBWPvuYgtjx_9(int32_t value)
	{
		___srSvPxedjBeocvpioBWPvuYgtjx_9 = value;
	}

	inline static int32_t get_offset_of_hjgsuFLRTScWkajVxAiiABppxTid_10() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___hjgsuFLRTScWkajVxAiiABppxTid_10)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_hjgsuFLRTScWkajVxAiiABppxTid_10() const { return ___hjgsuFLRTScWkajVxAiiABppxTid_10; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_hjgsuFLRTScWkajVxAiiABppxTid_10() { return &___hjgsuFLRTScWkajVxAiiABppxTid_10; }
	inline void set_hjgsuFLRTScWkajVxAiiABppxTid_10(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___hjgsuFLRTScWkajVxAiiABppxTid_10 = value;
		Il2CppCodeGenWriteBarrier((&___hjgsuFLRTScWkajVxAiiABppxTid_10), value);
	}

	inline static int32_t get_offset_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_11() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___EChnlGrdrUcyXYSPZFpLOLQYgAx_11)); }
	inline int32_t get_EChnlGrdrUcyXYSPZFpLOLQYgAx_11() const { return ___EChnlGrdrUcyXYSPZFpLOLQYgAx_11; }
	inline int32_t* get_address_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_11() { return &___EChnlGrdrUcyXYSPZFpLOLQYgAx_11; }
	inline void set_EChnlGrdrUcyXYSPZFpLOLQYgAx_11(int32_t value)
	{
		___EChnlGrdrUcyXYSPZFpLOLQYgAx_11 = value;
	}

	inline static int32_t get_offset_of_fpbTLzCSTwYcOnnFJAuVUuFuOIP_12() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_fpbTLzCSTwYcOnnFJAuVUuFuOIP_12() const { return ___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_fpbTLzCSTwYcOnnFJAuVUuFuOIP_12() { return &___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12; }
	inline void set_fpbTLzCSTwYcOnnFJAuVUuFuOIP_12(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12 = value;
		Il2CppCodeGenWriteBarrier((&___fpbTLzCSTwYcOnnFJAuVUuFuOIP_12), value);
	}

	inline static int32_t get_offset_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_13() { return static_cast<int32_t>(offsetof(auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3, ___gHOYJROlCWbVZCUTzkskuGcWOOwd_13)); }
	inline RuntimeObject* get_gHOYJROlCWbVZCUTzkskuGcWOOwd_13() const { return ___gHOYJROlCWbVZCUTzkskuGcWOOwd_13; }
	inline RuntimeObject** get_address_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_13() { return &___gHOYJROlCWbVZCUTzkskuGcWOOwd_13; }
	inline void set_gHOYJROlCWbVZCUTzkskuGcWOOwd_13(RuntimeObject* value)
	{
		___gHOYJROlCWbVZCUTzkskuGcWOOwd_13 = value;
		Il2CppCodeGenWriteBarrier((&___gHOYJROlCWbVZCUTzkskuGcWOOwd_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGBNUNIDODGWVSJZNRLGQBCMHV_T4E73DEB7A80ECAA7149C2F24855BEB8946309DF3_H
#ifndef FWMDALFLETUSAJERPFYSVFRPQWO_T2F445DBC5EB323B6821798AE5650C9D527F966C1_H
#define FWMDALFLETUSAJERPFYSVFRPQWO_T2F445DBC5EB323B6821798AE5650C9D527F966C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO
struct  fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::tfUFVQgUsJpcyOvrTozhGNyqUAUN
	String_t* ___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4;
	// System.String Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::HpRmBTNMOJhjiRwCqWZDZFyZBKWB
	String_t* ___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5;
	// System.Boolean Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::PfBtErLFmgIwzViqjbjnyRbCRuw
	bool ___PfBtErLFmgIwzViqjbjnyRbCRuw_6;
	// System.Boolean Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::ZyLlrOVJdDTGzRxMxdMMRTlifBO
	bool ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7;
	// Rewired.InputCategory Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::yCkXaxzjTuzfcCDVbdkzceRrNPLd
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8;
	// System.Int32 Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::vKPsghCwZcBCtuyYRMmUGbOcTQA
	int32_t ___vKPsghCwZcBCtuyYRMmUGbOcTQA_9;
	// Rewired.InputAction Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::pfyTwvHZiBuGvhpfWDOaFzGNVkP
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10;
	// System.Int32 Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::VVmghUeCqrsnjVcDnqzHMLYfXNzc
	int32_t ___VVmghUeCqrsnjVcDnqzHMLYfXNzc_11;
	// Rewired.InputAction Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::nqIBgQJbStWIfQxhIUPqhBicleF
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___nqIBgQJbStWIfQxhIUPqhBicleF_12;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_fWMdAlFLeTUSAJeRpFYsvfrpQWO::dKXJPdhNuyMkyKbBRgczOSzhbqL
	RuntimeObject* ___dKXJPdhNuyMkyKbBRgczOSzhbqL_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_tfUFVQgUsJpcyOvrTozhGNyqUAUN_4() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4)); }
	inline String_t* get_tfUFVQgUsJpcyOvrTozhGNyqUAUN_4() const { return ___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4; }
	inline String_t** get_address_of_tfUFVQgUsJpcyOvrTozhGNyqUAUN_4() { return &___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4; }
	inline void set_tfUFVQgUsJpcyOvrTozhGNyqUAUN_4(String_t* value)
	{
		___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4 = value;
		Il2CppCodeGenWriteBarrier((&___tfUFVQgUsJpcyOvrTozhGNyqUAUN_4), value);
	}

	inline static int32_t get_offset_of_HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5)); }
	inline String_t* get_HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5() const { return ___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5; }
	inline String_t** get_address_of_HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5() { return &___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5; }
	inline void set_HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5(String_t* value)
	{
		___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5 = value;
		Il2CppCodeGenWriteBarrier((&___HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5), value);
	}

	inline static int32_t get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___PfBtErLFmgIwzViqjbjnyRbCRuw_6)); }
	inline bool get_PfBtErLFmgIwzViqjbjnyRbCRuw_6() const { return ___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline bool* get_address_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return &___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline void set_PfBtErLFmgIwzViqjbjnyRbCRuw_6(bool value)
	{
		___PfBtErLFmgIwzViqjbjnyRbCRuw_6 = value;
	}

	inline static int32_t get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7)); }
	inline bool get_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() const { return ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline bool* get_address_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return &___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline void set_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(bool value)
	{
		___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7 = value;
	}

	inline static int32_t get_offset_of_yCkXaxzjTuzfcCDVbdkzceRrNPLd_8() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_yCkXaxzjTuzfcCDVbdkzceRrNPLd_8() const { return ___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_yCkXaxzjTuzfcCDVbdkzceRrNPLd_8() { return &___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8; }
	inline void set_yCkXaxzjTuzfcCDVbdkzceRrNPLd_8(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8 = value;
		Il2CppCodeGenWriteBarrier((&___yCkXaxzjTuzfcCDVbdkzceRrNPLd_8), value);
	}

	inline static int32_t get_offset_of_vKPsghCwZcBCtuyYRMmUGbOcTQA_9() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___vKPsghCwZcBCtuyYRMmUGbOcTQA_9)); }
	inline int32_t get_vKPsghCwZcBCtuyYRMmUGbOcTQA_9() const { return ___vKPsghCwZcBCtuyYRMmUGbOcTQA_9; }
	inline int32_t* get_address_of_vKPsghCwZcBCtuyYRMmUGbOcTQA_9() { return &___vKPsghCwZcBCtuyYRMmUGbOcTQA_9; }
	inline void set_vKPsghCwZcBCtuyYRMmUGbOcTQA_9(int32_t value)
	{
		___vKPsghCwZcBCtuyYRMmUGbOcTQA_9 = value;
	}

	inline static int32_t get_offset_of_pfyTwvHZiBuGvhpfWDOaFzGNVkP_10() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_pfyTwvHZiBuGvhpfWDOaFzGNVkP_10() const { return ___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_pfyTwvHZiBuGvhpfWDOaFzGNVkP_10() { return &___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10; }
	inline void set_pfyTwvHZiBuGvhpfWDOaFzGNVkP_10(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10 = value;
		Il2CppCodeGenWriteBarrier((&___pfyTwvHZiBuGvhpfWDOaFzGNVkP_10), value);
	}

	inline static int32_t get_offset_of_VVmghUeCqrsnjVcDnqzHMLYfXNzc_11() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___VVmghUeCqrsnjVcDnqzHMLYfXNzc_11)); }
	inline int32_t get_VVmghUeCqrsnjVcDnqzHMLYfXNzc_11() const { return ___VVmghUeCqrsnjVcDnqzHMLYfXNzc_11; }
	inline int32_t* get_address_of_VVmghUeCqrsnjVcDnqzHMLYfXNzc_11() { return &___VVmghUeCqrsnjVcDnqzHMLYfXNzc_11; }
	inline void set_VVmghUeCqrsnjVcDnqzHMLYfXNzc_11(int32_t value)
	{
		___VVmghUeCqrsnjVcDnqzHMLYfXNzc_11 = value;
	}

	inline static int32_t get_offset_of_nqIBgQJbStWIfQxhIUPqhBicleF_12() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___nqIBgQJbStWIfQxhIUPqhBicleF_12)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_nqIBgQJbStWIfQxhIUPqhBicleF_12() const { return ___nqIBgQJbStWIfQxhIUPqhBicleF_12; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_nqIBgQJbStWIfQxhIUPqhBicleF_12() { return &___nqIBgQJbStWIfQxhIUPqhBicleF_12; }
	inline void set_nqIBgQJbStWIfQxhIUPqhBicleF_12(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___nqIBgQJbStWIfQxhIUPqhBicleF_12 = value;
		Il2CppCodeGenWriteBarrier((&___nqIBgQJbStWIfQxhIUPqhBicleF_12), value);
	}

	inline static int32_t get_offset_of_dKXJPdhNuyMkyKbBRgczOSzhbqL_13() { return static_cast<int32_t>(offsetof(fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1, ___dKXJPdhNuyMkyKbBRgczOSzhbqL_13)); }
	inline RuntimeObject* get_dKXJPdhNuyMkyKbBRgczOSzhbqL_13() const { return ___dKXJPdhNuyMkyKbBRgczOSzhbqL_13; }
	inline RuntimeObject** get_address_of_dKXJPdhNuyMkyKbBRgczOSzhbqL_13() { return &___dKXJPdhNuyMkyKbBRgczOSzhbqL_13; }
	inline void set_dKXJPdhNuyMkyKbBRgczOSzhbqL_13(RuntimeObject* value)
	{
		___dKXJPdhNuyMkyKbBRgczOSzhbqL_13 = value;
		Il2CppCodeGenWriteBarrier((&___dKXJPdhNuyMkyKbBRgczOSzhbqL_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FWMDALFLETUSAJERPFYSVFRPQWO_T2F445DBC5EB323B6821798AE5650C9D527F966C1_H
#ifndef JZZYLMAUMXWZAOPBKUYLHMNHVLY_T3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_H
#define JZZYLMAUMXWZAOPBKUYLHMNHVLY_T3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy
struct  JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy::fTbGTbjBkADLQBKzHERKZMhLtdA
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___fTbGTbjBkADLQBKzHERKZMhLtdA_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return static_cast<int32_t>(offsetof(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF, ___fTbGTbjBkADLQBKzHERKZMhLtdA_1)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_fTbGTbjBkADLQBKzHERKZMhLtdA_1() const { return ___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return &___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline void set_fTbGTbjBkADLQBKzHERKZMhLtdA_1(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___fTbGTbjBkADLQBKzHERKZMhLtdA_1 = value;
		Il2CppCodeGenWriteBarrier((&___fTbGTbjBkADLQBKzHERKZMhLtdA_1), value);
	}
};

struct JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_StaticFields
{
public:
	// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy::dSKRKwvosVaEirauaqhdBfEDQNT
	Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * ___dSKRKwvosVaEirauaqhdBfEDQNT_2;

public:
	inline static int32_t get_offset_of_dSKRKwvosVaEirauaqhdBfEDQNT_2() { return static_cast<int32_t>(offsetof(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_StaticFields, ___dSKRKwvosVaEirauaqhdBfEDQNT_2)); }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * get_dSKRKwvosVaEirauaqhdBfEDQNT_2() const { return ___dSKRKwvosVaEirauaqhdBfEDQNT_2; }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 ** get_address_of_dSKRKwvosVaEirauaqhdBfEDQNT_2() { return &___dSKRKwvosVaEirauaqhdBfEDQNT_2; }
	inline void set_dSKRKwvosVaEirauaqhdBfEDQNT_2(Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * value)
	{
		___dSKRKwvosVaEirauaqhdBfEDQNT_2 = value;
		Il2CppCodeGenWriteBarrier((&___dSKRKwvosVaEirauaqhdBfEDQNT_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JZZYLMAUMXWZAOPBKUYLHMNHVLY_T3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_H
#ifndef CZMGTLBHUPSFDQAATAIUCNTRKWXC_T536BBEC96A08677A531AD75AD503F560DA15F815_H
#define CZMGTLBHUPSFDQAATAIUCNTRKWXC_T536BBEC96A08677A531AD75AD503F560DA15F815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_czMGTlBHUPsFdqAataIucntrKwXC
struct  czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_czMGTlBHUPsFdqAataIucntrKwXC::CFJiwEBvJujHtEMtAkarYxTAFSAv
	pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB * ___CFJiwEBvJujHtEMtAkarYxTAFSAv_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_czMGTlBHUPsFdqAataIucntrKwXC::mhgrIaReWtlVoWezNqdFXOhDOQn
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * ___mhgrIaReWtlVoWezNqdFXOhDOQn_1;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_czMGTlBHUPsFdqAataIucntrKwXC::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_2;
	// Rewired.ActionElementMap Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_czMGTlBHUPsFdqAataIucntrKwXC::XaepNDmdlDDRBgSPSIhKadPGxnPE
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3;

public:
	inline static int32_t get_offset_of_CFJiwEBvJujHtEMtAkarYxTAFSAv_0() { return static_cast<int32_t>(offsetof(czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815, ___CFJiwEBvJujHtEMtAkarYxTAFSAv_0)); }
	inline pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB * get_CFJiwEBvJujHtEMtAkarYxTAFSAv_0() const { return ___CFJiwEBvJujHtEMtAkarYxTAFSAv_0; }
	inline pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB ** get_address_of_CFJiwEBvJujHtEMtAkarYxTAFSAv_0() { return &___CFJiwEBvJujHtEMtAkarYxTAFSAv_0; }
	inline void set_CFJiwEBvJujHtEMtAkarYxTAFSAv_0(pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB * value)
	{
		___CFJiwEBvJujHtEMtAkarYxTAFSAv_0 = value;
		Il2CppCodeGenWriteBarrier((&___CFJiwEBvJujHtEMtAkarYxTAFSAv_0), value);
	}

	inline static int32_t get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_1() { return static_cast<int32_t>(offsetof(czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815, ___mhgrIaReWtlVoWezNqdFXOhDOQn_1)); }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * get_mhgrIaReWtlVoWezNqdFXOhDOQn_1() const { return ___mhgrIaReWtlVoWezNqdFXOhDOQn_1; }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF ** get_address_of_mhgrIaReWtlVoWezNqdFXOhDOQn_1() { return &___mhgrIaReWtlVoWezNqdFXOhDOQn_1; }
	inline void set_mhgrIaReWtlVoWezNqdFXOhDOQn_1(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * value)
	{
		___mhgrIaReWtlVoWezNqdFXOhDOQn_1 = value;
		Il2CppCodeGenWriteBarrier((&___mhgrIaReWtlVoWezNqdFXOhDOQn_1), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return static_cast<int32_t>(offsetof(czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815, ___NxecGThpOUHPMAqQltxuibhjsBi_2)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_2() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return &___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_2(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_2 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_2), value);
	}

	inline static int32_t get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return static_cast<int32_t>(offsetof(czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815, ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() const { return ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return &___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline void set_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___XaepNDmdlDDRBgSPSIhKadPGxnPE_3 = value;
		Il2CppCodeGenWriteBarrier((&___XaepNDmdlDDRBgSPSIhKadPGxnPE_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CZMGTLBHUPSFDQAATAIUCNTRKWXC_T536BBEC96A08677A531AD75AD503F560DA15F815_H
#ifndef DIPQZQLBYUTBCMOXLBFXFKJINDU_TD1B419D1C19779665440AC3FF722EA42C8FFB498_H
#define DIPQZQLBYUTBCMOXLBFXFKJINDU_TD1B419D1C19779665440AC3FF722EA42C8FFB498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_diPqZQLByUtBcMoxlbfXFKjiNDU
struct  diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_diPqZQLByUtBcMoxlbfXFKjiNDU::mhgrIaReWtlVoWezNqdFXOhDOQn
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * ___mhgrIaReWtlVoWezNqdFXOhDOQn_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_diPqZQLByUtBcMoxlbfXFKjiNDU::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_diPqZQLByUtBcMoxlbfXFKjiNDU::doEmDrDRMAhaULBVLOODFaMXMSO
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___doEmDrDRMAhaULBVLOODFaMXMSO_2;

public:
	inline static int32_t get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0() { return static_cast<int32_t>(offsetof(diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498, ___mhgrIaReWtlVoWezNqdFXOhDOQn_0)); }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * get_mhgrIaReWtlVoWezNqdFXOhDOQn_0() const { return ___mhgrIaReWtlVoWezNqdFXOhDOQn_0; }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF ** get_address_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0() { return &___mhgrIaReWtlVoWezNqdFXOhDOQn_0; }
	inline void set_mhgrIaReWtlVoWezNqdFXOhDOQn_0(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * value)
	{
		___mhgrIaReWtlVoWezNqdFXOhDOQn_0 = value;
		Il2CppCodeGenWriteBarrier((&___mhgrIaReWtlVoWezNqdFXOhDOQn_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return static_cast<int32_t>(offsetof(diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498, ___doEmDrDRMAhaULBVLOODFaMXMSO_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_doEmDrDRMAhaULBVLOODFaMXMSO_2() const { return ___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return &___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline void set_doEmDrDRMAhaULBVLOODFaMXMSO_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___doEmDrDRMAhaULBVLOODFaMXMSO_2 = value;
		Il2CppCodeGenWriteBarrier((&___doEmDrDRMAhaULBVLOODFaMXMSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIPQZQLBYUTBCMOXLBFXFKJINDU_TD1B419D1C19779665440AC3FF722EA42C8FFB498_H
#ifndef PAOJJQRIFZLHRWZOIISNVLCBAIBB_TC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB_H
#define PAOJJQRIFZLHRWZOIISNVLCBAIBB_TC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb
struct  pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb::mhgrIaReWtlVoWezNqdFXOhDOQn
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * ___mhgrIaReWtlVoWezNqdFXOhDOQn_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb::ONFkOOHVwwNwinsmwCbKAlnoQkX
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_JZZyLMAuMxwZaOpbKUYlhMNhVLy_pAOjJqrifZlhrwZoIIsNVLCBaIbb::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * ___RnYngjobuYlteiRkTweuGcJpYtu_3;

public:
	inline static int32_t get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0() { return static_cast<int32_t>(offsetof(pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB, ___mhgrIaReWtlVoWezNqdFXOhDOQn_0)); }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * get_mhgrIaReWtlVoWezNqdFXOhDOQn_0() const { return ___mhgrIaReWtlVoWezNqdFXOhDOQn_0; }
	inline JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF ** get_address_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0() { return &___mhgrIaReWtlVoWezNqdFXOhDOQn_0; }
	inline void set_mhgrIaReWtlVoWezNqdFXOhDOQn_0(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF * value)
	{
		___mhgrIaReWtlVoWezNqdFXOhDOQn_0 = value;
		Il2CppCodeGenWriteBarrier((&___mhgrIaReWtlVoWezNqdFXOhDOQn_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return static_cast<int32_t>(offsetof(pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB, ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() const { return ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return &___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline void set_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___ONFkOOHVwwNwinsmwCbKAlnoQkX_2 = value;
		Il2CppCodeGenWriteBarrier((&___ONFkOOHVwwNwinsmwCbKAlnoQkX_2), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return static_cast<int32_t>(offsetof(pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB, ___RnYngjobuYlteiRkTweuGcJpYtu_3)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * get_RnYngjobuYlteiRkTweuGcJpYtu_3() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return &___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_3(DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAOJJQRIFZLHRWZOIISNVLCBAIBB_TC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB_H
#ifndef UBGBLDIYFADTRLOIHMYVENHZVPJE_T1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_H
#define UBGBLDIYFADTRLOIHMYVENHZVPJE_T1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE
struct  UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE::fTbGTbjBkADLQBKzHERKZMhLtdA
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___fTbGTbjBkADLQBKzHERKZMhLtdA_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return static_cast<int32_t>(offsetof(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2, ___fTbGTbjBkADLQBKzHERKZMhLtdA_1)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_fTbGTbjBkADLQBKzHERKZMhLtdA_1() const { return ___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return &___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline void set_fTbGTbjBkADLQBKzHERKZMhLtdA_1(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___fTbGTbjBkADLQBKzHERKZMhLtdA_1 = value;
		Il2CppCodeGenWriteBarrier((&___fTbGTbjBkADLQBKzHERKZMhLtdA_1), value);
	}
};

struct UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_StaticFields
{
public:
	// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE::mKnMJjALYPDqjAoSzooTqyMcgMHD
	Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * ___mKnMJjALYPDqjAoSzooTqyMcgMHD_2;

public:
	inline static int32_t get_offset_of_mKnMJjALYPDqjAoSzooTqyMcgMHD_2() { return static_cast<int32_t>(offsetof(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_StaticFields, ___mKnMJjALYPDqjAoSzooTqyMcgMHD_2)); }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * get_mKnMJjALYPDqjAoSzooTqyMcgMHD_2() const { return ___mKnMJjALYPDqjAoSzooTqyMcgMHD_2; }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 ** get_address_of_mKnMJjALYPDqjAoSzooTqyMcgMHD_2() { return &___mKnMJjALYPDqjAoSzooTqyMcgMHD_2; }
	inline void set_mKnMJjALYPDqjAoSzooTqyMcgMHD_2(Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * value)
	{
		___mKnMJjALYPDqjAoSzooTqyMcgMHD_2 = value;
		Il2CppCodeGenWriteBarrier((&___mKnMJjALYPDqjAoSzooTqyMcgMHD_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UBGBLDIYFADTRLOIHMYVENHZVPJE_T1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_H
#ifndef CLJVBDESGVLJEIKXHBOXRLHLKNF_TF9CF56353008DD0C7511A59F28EAD3D2C5A20D84_H
#define CLJVBDESGVLJEIKXHBOXRLHLKNF_TF9CF56353008DD0C7511A59F28EAD3D2C5A20D84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF
struct  clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF::CzbgHihIrBdYfLmPHIGillBPleew
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * ___CzbgHihIrBdYfLmPHIGillBPleew_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF::ONFkOOHVwwNwinsmwCbKAlnoQkX
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * ___RnYngjobuYlteiRkTweuGcJpYtu_3;

public:
	inline static int32_t get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_0() { return static_cast<int32_t>(offsetof(clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84, ___CzbgHihIrBdYfLmPHIGillBPleew_0)); }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * get_CzbgHihIrBdYfLmPHIGillBPleew_0() const { return ___CzbgHihIrBdYfLmPHIGillBPleew_0; }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 ** get_address_of_CzbgHihIrBdYfLmPHIGillBPleew_0() { return &___CzbgHihIrBdYfLmPHIGillBPleew_0; }
	inline void set_CzbgHihIrBdYfLmPHIGillBPleew_0(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * value)
	{
		___CzbgHihIrBdYfLmPHIGillBPleew_0 = value;
		Il2CppCodeGenWriteBarrier((&___CzbgHihIrBdYfLmPHIGillBPleew_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return static_cast<int32_t>(offsetof(clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84, ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() const { return ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return &___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline void set_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___ONFkOOHVwwNwinsmwCbKAlnoQkX_2 = value;
		Il2CppCodeGenWriteBarrier((&___ONFkOOHVwwNwinsmwCbKAlnoQkX_2), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return static_cast<int32_t>(offsetof(clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84, ___RnYngjobuYlteiRkTweuGcJpYtu_3)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * get_RnYngjobuYlteiRkTweuGcJpYtu_3() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return &___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_3(DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLJVBDESGVLJEIKXHBOXRLHLKNF_TF9CF56353008DD0C7511A59F28EAD3D2C5A20D84_H
#ifndef FODGYQOFCBZNNDWDFRMSBJTBKJO_T82965906495BD22FE429922D8B5C1708B4FC56CD_H
#define FODGYQOFCBZNNDWDFRMSBJTBKJO_T82965906495BD22FE429922D8B5C1708B4FC56CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_fODgYqoFcBZNNDWDFRmSBJTbkJO
struct  fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_fODgYqoFcBZNNDWDFRmSBJTbkJO::CzbgHihIrBdYfLmPHIGillBPleew
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * ___CzbgHihIrBdYfLmPHIGillBPleew_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_fODgYqoFcBZNNDWDFRmSBJTbkJO::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_fODgYqoFcBZNNDWDFRmSBJTbkJO::doEmDrDRMAhaULBVLOODFaMXMSO
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___doEmDrDRMAhaULBVLOODFaMXMSO_2;

public:
	inline static int32_t get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_0() { return static_cast<int32_t>(offsetof(fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD, ___CzbgHihIrBdYfLmPHIGillBPleew_0)); }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * get_CzbgHihIrBdYfLmPHIGillBPleew_0() const { return ___CzbgHihIrBdYfLmPHIGillBPleew_0; }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 ** get_address_of_CzbgHihIrBdYfLmPHIGillBPleew_0() { return &___CzbgHihIrBdYfLmPHIGillBPleew_0; }
	inline void set_CzbgHihIrBdYfLmPHIGillBPleew_0(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * value)
	{
		___CzbgHihIrBdYfLmPHIGillBPleew_0 = value;
		Il2CppCodeGenWriteBarrier((&___CzbgHihIrBdYfLmPHIGillBPleew_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return static_cast<int32_t>(offsetof(fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD, ___doEmDrDRMAhaULBVLOODFaMXMSO_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_doEmDrDRMAhaULBVLOODFaMXMSO_2() const { return ___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return &___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline void set_doEmDrDRMAhaULBVLOODFaMXMSO_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___doEmDrDRMAhaULBVLOODFaMXMSO_2 = value;
		Il2CppCodeGenWriteBarrier((&___doEmDrDRMAhaULBVLOODFaMXMSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FODGYQOFCBZNNDWDFRMSBJTBKJO_T82965906495BD22FE429922D8B5C1708B4FC56CD_H
#ifndef WIOCUIQVJFVVTMHLZHWAIBEZCGS_T03C04FD24E3FDEA30F9B25652B32F710DEB6E978_H
#define WIOCUIQVJFVVTMHLZHWAIBEZCGS_T03C04FD24E3FDEA30F9B25652B32F710DEB6E978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_wiOcuiQvjFvvtmhlzhwAiBeZcGs
struct  wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_clJVBdesGVlJeiKXHbOxrlHLKNF Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_wiOcuiQvjFvvtmhlzhwAiBeZcGs::LLGuAxNDTisGPebZTuecPvDemqb
	clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84 * ___LLGuAxNDTisGPebZTuecPvDemqb_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_wiOcuiQvjFvvtmhlzhwAiBeZcGs::CzbgHihIrBdYfLmPHIGillBPleew
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * ___CzbgHihIrBdYfLmPHIGillBPleew_1;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_wiOcuiQvjFvvtmhlzhwAiBeZcGs::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_2;
	// Rewired.ActionElementMap Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_UbGBLdiYFAdtrLoihmYVEnHzVpJE_wiOcuiQvjFvvtmhlzhwAiBeZcGs::XaepNDmdlDDRBgSPSIhKadPGxnPE
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3;

public:
	inline static int32_t get_offset_of_LLGuAxNDTisGPebZTuecPvDemqb_0() { return static_cast<int32_t>(offsetof(wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978, ___LLGuAxNDTisGPebZTuecPvDemqb_0)); }
	inline clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84 * get_LLGuAxNDTisGPebZTuecPvDemqb_0() const { return ___LLGuAxNDTisGPebZTuecPvDemqb_0; }
	inline clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84 ** get_address_of_LLGuAxNDTisGPebZTuecPvDemqb_0() { return &___LLGuAxNDTisGPebZTuecPvDemqb_0; }
	inline void set_LLGuAxNDTisGPebZTuecPvDemqb_0(clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84 * value)
	{
		___LLGuAxNDTisGPebZTuecPvDemqb_0 = value;
		Il2CppCodeGenWriteBarrier((&___LLGuAxNDTisGPebZTuecPvDemqb_0), value);
	}

	inline static int32_t get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_1() { return static_cast<int32_t>(offsetof(wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978, ___CzbgHihIrBdYfLmPHIGillBPleew_1)); }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * get_CzbgHihIrBdYfLmPHIGillBPleew_1() const { return ___CzbgHihIrBdYfLmPHIGillBPleew_1; }
	inline UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 ** get_address_of_CzbgHihIrBdYfLmPHIGillBPleew_1() { return &___CzbgHihIrBdYfLmPHIGillBPleew_1; }
	inline void set_CzbgHihIrBdYfLmPHIGillBPleew_1(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2 * value)
	{
		___CzbgHihIrBdYfLmPHIGillBPleew_1 = value;
		Il2CppCodeGenWriteBarrier((&___CzbgHihIrBdYfLmPHIGillBPleew_1), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return static_cast<int32_t>(offsetof(wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978, ___NxecGThpOUHPMAqQltxuibhjsBi_2)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_2() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return &___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_2(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_2 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_2), value);
	}

	inline static int32_t get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return static_cast<int32_t>(offsetof(wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978, ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() const { return ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return &___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline void set_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___XaepNDmdlDDRBgSPSIhKadPGxnPE_3 = value;
		Il2CppCodeGenWriteBarrier((&___XaepNDmdlDDRBgSPSIhKadPGxnPE_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIOCUIQVJFVVTMHLZHWAIBEZCGS_T03C04FD24E3FDEA30F9B25652B32F710DEB6E978_H
#ifndef INNDXEBNNPQXDAELIHDKVRUCGNER_T810B9ABF109031F95CA3D804FFB69F1441224C9C_H
#define INNDXEBNNPQXDAELIHDKVRUCGNER_T810B9ABF109031F95CA3D804FFB69F1441224C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR
struct  iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C  : public RuntimeObject
{
public:
	// Rewired.InputCategory Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR::NOArrsNdfKDShNFftfbPqYDzhpq
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_iNNDxEbNNpqxDAeLihdkVRUCGneR::UTKmqfOTQTBrCjBgXOMUQjMaWpB
	int32_t ___UTKmqfOTQTBrCjBgXOMUQjMaWpB_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_UTKmqfOTQTBrCjBgXOMUQjMaWpB_4() { return static_cast<int32_t>(offsetof(iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C, ___UTKmqfOTQTBrCjBgXOMUQjMaWpB_4)); }
	inline int32_t get_UTKmqfOTQTBrCjBgXOMUQjMaWpB_4() const { return ___UTKmqfOTQTBrCjBgXOMUQjMaWpB_4; }
	inline int32_t* get_address_of_UTKmqfOTQTBrCjBgXOMUQjMaWpB_4() { return &___UTKmqfOTQTBrCjBgXOMUQjMaWpB_4; }
	inline void set_UTKmqfOTQTBrCjBgXOMUQjMaWpB_4(int32_t value)
	{
		___UTKmqfOTQTBrCjBgXOMUQjMaWpB_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INNDXEBNNPQXDAELIHDKVRUCGNER_T810B9ABF109031F95CA3D804FFB69F1441224C9C_H
#ifndef KGEYDNBBNCYBITZCEHFZPEZLNPN_TC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5_H
#define KGEYDNBBNCYBITZCEHFZPEZLNPN_TC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn
struct  kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::DdLBFDSCGIcxnggOHOEzIwvbXCWO
	String_t* ___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4;
	// System.String Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::SMlAuMHVLnsuvgVfNQDkQwkRiRZ
	String_t* ___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5;
	// System.Boolean Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::PfBtErLFmgIwzViqjbjnyRbCRuw
	bool ___PfBtErLFmgIwzViqjbjnyRbCRuw_6;
	// System.Boolean Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::ZyLlrOVJdDTGzRxMxdMMRTlifBO
	bool ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7;
	// System.Int32 Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::PIwpZHvawTjYekPxGTwATVAdBDe
	int32_t ___PIwpZHvawTjYekPxGTwATVAdBDe_8;
	// Rewired.InputCategory Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::cIipAhMAgwhMwgeMkwzVkGWsujb
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___cIipAhMAgwhMwgeMkwzVkGWsujb_9;
	// System.Int32 Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::QCpLFkKDQjUOaJYzIOvwDlenNWW
	int32_t ___QCpLFkKDQjUOaJYzIOvwDlenNWW_10;
	// Rewired.InputAction Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::JeOdrvOPNPfsNJaxdMekBKDgSoGJ
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11;
	// System.Int32 Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::QcpYrCzXoHBprkTYGgNUNLhrDlb
	int32_t ___QcpYrCzXoHBprkTYGgNUNLhrDlb_12;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_kgEYdNbBncYBItzCeHfZPEZLnpn::PalmDKfwodOEEgPtuWClpZscTYK
	RuntimeObject* ___PalmDKfwodOEEgPtuWClpZscTYK_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_DdLBFDSCGIcxnggOHOEzIwvbXCWO_4() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4)); }
	inline String_t* get_DdLBFDSCGIcxnggOHOEzIwvbXCWO_4() const { return ___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4; }
	inline String_t** get_address_of_DdLBFDSCGIcxnggOHOEzIwvbXCWO_4() { return &___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4; }
	inline void set_DdLBFDSCGIcxnggOHOEzIwvbXCWO_4(String_t* value)
	{
		___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4 = value;
		Il2CppCodeGenWriteBarrier((&___DdLBFDSCGIcxnggOHOEzIwvbXCWO_4), value);
	}

	inline static int32_t get_offset_of_SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5)); }
	inline String_t* get_SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5() const { return ___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5; }
	inline String_t** get_address_of_SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5() { return &___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5; }
	inline void set_SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5(String_t* value)
	{
		___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5 = value;
		Il2CppCodeGenWriteBarrier((&___SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5), value);
	}

	inline static int32_t get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___PfBtErLFmgIwzViqjbjnyRbCRuw_6)); }
	inline bool get_PfBtErLFmgIwzViqjbjnyRbCRuw_6() const { return ___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline bool* get_address_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return &___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline void set_PfBtErLFmgIwzViqjbjnyRbCRuw_6(bool value)
	{
		___PfBtErLFmgIwzViqjbjnyRbCRuw_6 = value;
	}

	inline static int32_t get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7)); }
	inline bool get_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() const { return ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline bool* get_address_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return &___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline void set_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(bool value)
	{
		___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7 = value;
	}

	inline static int32_t get_offset_of_PIwpZHvawTjYekPxGTwATVAdBDe_8() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___PIwpZHvawTjYekPxGTwATVAdBDe_8)); }
	inline int32_t get_PIwpZHvawTjYekPxGTwATVAdBDe_8() const { return ___PIwpZHvawTjYekPxGTwATVAdBDe_8; }
	inline int32_t* get_address_of_PIwpZHvawTjYekPxGTwATVAdBDe_8() { return &___PIwpZHvawTjYekPxGTwATVAdBDe_8; }
	inline void set_PIwpZHvawTjYekPxGTwATVAdBDe_8(int32_t value)
	{
		___PIwpZHvawTjYekPxGTwATVAdBDe_8 = value;
	}

	inline static int32_t get_offset_of_cIipAhMAgwhMwgeMkwzVkGWsujb_9() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___cIipAhMAgwhMwgeMkwzVkGWsujb_9)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_cIipAhMAgwhMwgeMkwzVkGWsujb_9() const { return ___cIipAhMAgwhMwgeMkwzVkGWsujb_9; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_cIipAhMAgwhMwgeMkwzVkGWsujb_9() { return &___cIipAhMAgwhMwgeMkwzVkGWsujb_9; }
	inline void set_cIipAhMAgwhMwgeMkwzVkGWsujb_9(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___cIipAhMAgwhMwgeMkwzVkGWsujb_9 = value;
		Il2CppCodeGenWriteBarrier((&___cIipAhMAgwhMwgeMkwzVkGWsujb_9), value);
	}

	inline static int32_t get_offset_of_QCpLFkKDQjUOaJYzIOvwDlenNWW_10() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___QCpLFkKDQjUOaJYzIOvwDlenNWW_10)); }
	inline int32_t get_QCpLFkKDQjUOaJYzIOvwDlenNWW_10() const { return ___QCpLFkKDQjUOaJYzIOvwDlenNWW_10; }
	inline int32_t* get_address_of_QCpLFkKDQjUOaJYzIOvwDlenNWW_10() { return &___QCpLFkKDQjUOaJYzIOvwDlenNWW_10; }
	inline void set_QCpLFkKDQjUOaJYzIOvwDlenNWW_10(int32_t value)
	{
		___QCpLFkKDQjUOaJYzIOvwDlenNWW_10 = value;
	}

	inline static int32_t get_offset_of_JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11() const { return ___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11() { return &___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11; }
	inline void set_JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11 = value;
		Il2CppCodeGenWriteBarrier((&___JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11), value);
	}

	inline static int32_t get_offset_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_12() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___QcpYrCzXoHBprkTYGgNUNLhrDlb_12)); }
	inline int32_t get_QcpYrCzXoHBprkTYGgNUNLhrDlb_12() const { return ___QcpYrCzXoHBprkTYGgNUNLhrDlb_12; }
	inline int32_t* get_address_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_12() { return &___QcpYrCzXoHBprkTYGgNUNLhrDlb_12; }
	inline void set_QcpYrCzXoHBprkTYGgNUNLhrDlb_12(int32_t value)
	{
		___QcpYrCzXoHBprkTYGgNUNLhrDlb_12 = value;
	}

	inline static int32_t get_offset_of_PalmDKfwodOEEgPtuWClpZscTYK_13() { return static_cast<int32_t>(offsetof(kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5, ___PalmDKfwodOEEgPtuWClpZscTYK_13)); }
	inline RuntimeObject* get_PalmDKfwodOEEgPtuWClpZscTYK_13() const { return ___PalmDKfwodOEEgPtuWClpZscTYK_13; }
	inline RuntimeObject** get_address_of_PalmDKfwodOEEgPtuWClpZscTYK_13() { return &___PalmDKfwodOEEgPtuWClpZscTYK_13; }
	inline void set_PalmDKfwodOEEgPtuWClpZscTYK_13(RuntimeObject* value)
	{
		___PalmDKfwodOEEgPtuWClpZscTYK_13 = value;
		Il2CppCodeGenWriteBarrier((&___PalmDKfwodOEEgPtuWClpZscTYK_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KGEYDNBBNCYBITZCEHFZPEZLNPN_TC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5_H
#ifndef KWJCFRMFQCWBPIPXRDVJQSTVGZZ_TC0E7C2207DAC84303C2E8F1DB4A56D04C4732205_H
#define KWJCFRMFQCWBPIPXRDVJQSTVGZZ_TC0E7C2207DAC84303C2E8F1DB4A56D04C4732205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ
struct  kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.InputLayout> Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ::GHSFrqFOciasZnFYOarJvwFvvpiw
	List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * ___GHSFrqFOciasZnFYOarJvwFvvpiw_0;

public:
	inline static int32_t get_offset_of_GHSFrqFOciasZnFYOarJvwFvvpiw_0() { return static_cast<int32_t>(offsetof(kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205, ___GHSFrqFOciasZnFYOarJvwFvvpiw_0)); }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * get_GHSFrqFOciasZnFYOarJvwFvvpiw_0() const { return ___GHSFrqFOciasZnFYOarJvwFvvpiw_0; }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 ** get_address_of_GHSFrqFOciasZnFYOarJvwFvvpiw_0() { return &___GHSFrqFOciasZnFYOarJvwFvvpiw_0; }
	inline void set_GHSFrqFOciasZnFYOarJvwFvvpiw_0(List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * value)
	{
		___GHSFrqFOciasZnFYOarJvwFvvpiw_0 = value;
		Il2CppCodeGenWriteBarrier((&___GHSFrqFOciasZnFYOarJvwFvvpiw_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KWJCFRMFQCWBPIPXRDVJQSTVGZZ_TC0E7C2207DAC84303C2E8F1DB4A56D04C4732205_H
#ifndef HMXVLVNXXNGUMJHAGAAECIJGJEFU_T51A1055BA2C1C341961B7F33A1394814B6D6F82D_H
#define HMXVLVNXXNGUMJHAGAAECIJGJEFU_T51A1055BA2C1C341961B7F33A1394814B6D6F82D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ_HmXVLVNxxnGUmjhaGAaEcIjGJeFU
struct  HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ_HmXVLVNxxnGUmjhaGAaEcIjGJeFU::pwladNKVLiDfTkIGfUdjCuCDhgVa
	kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205 * ___pwladNKVLiDfTkIGfUdjCuCDhgVa_0;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ_HmXVLVNxxnGUmjhaGAaEcIjGJeFU::IPHGhnvmvweeXFsNSmpniuYFNHcM
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___IPHGhnvmvweeXFsNSmpniuYFNHcM_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_kwjCFrMFqcwBpiPxRDvjqsTVGzZ_HmXVLVNxxnGUmjhaGAaEcIjGJeFU::ycICvmDyXClaVDHBtBHHgVBlmCs
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___ycICvmDyXClaVDHBtBHHgVBlmCs_2;

public:
	inline static int32_t get_offset_of_pwladNKVLiDfTkIGfUdjCuCDhgVa_0() { return static_cast<int32_t>(offsetof(HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D, ___pwladNKVLiDfTkIGfUdjCuCDhgVa_0)); }
	inline kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205 * get_pwladNKVLiDfTkIGfUdjCuCDhgVa_0() const { return ___pwladNKVLiDfTkIGfUdjCuCDhgVa_0; }
	inline kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205 ** get_address_of_pwladNKVLiDfTkIGfUdjCuCDhgVa_0() { return &___pwladNKVLiDfTkIGfUdjCuCDhgVa_0; }
	inline void set_pwladNKVLiDfTkIGfUdjCuCDhgVa_0(kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205 * value)
	{
		___pwladNKVLiDfTkIGfUdjCuCDhgVa_0 = value;
		Il2CppCodeGenWriteBarrier((&___pwladNKVLiDfTkIGfUdjCuCDhgVa_0), value);
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_1() { return static_cast<int32_t>(offsetof(HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_1)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_IPHGhnvmvweeXFsNSmpniuYFNHcM_1() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_1; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_1() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_1; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_1(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_1 = value;
		Il2CppCodeGenWriteBarrier((&___IPHGhnvmvweeXFsNSmpniuYFNHcM_1), value);
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_2() { return static_cast<int32_t>(offsetof(HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D, ___ycICvmDyXClaVDHBtBHHgVBlmCs_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_ycICvmDyXClaVDHBtBHHgVBlmCs_2() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_2() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_2; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_2 = value;
		Il2CppCodeGenWriteBarrier((&___ycICvmDyXClaVDHBtBHHgVBlmCs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMXVLVNXXNGUMJHAGAAECIJGJEFU_T51A1055BA2C1C341961B7F33A1394814B6D6F82D_H
#ifndef VTMWSVFRGDAYPOJPQUMZJYVNXRQ_T55B1216A392D60366B63C317F586BA6F1EC99217_H
#define VTMWSVFRGDAYPOJPQUMZJYVNXRQ_T55B1216A392D60366B63C317F586BA6F1EC99217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ
struct  vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::sLDDgBamhzadZbhcjNSKCejHvMJN
	int32_t ___sLDDgBamhzadZbhcjNSKCejHvMJN_6;
	// System.Int32 Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::ujkBcIBmJgeqPhNLwLuqtFbBzmU
	int32_t ___ujkBcIBmJgeqPhNLwLuqtFbBzmU_7;
	// Rewired.InputCategory Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::vGCbgmFWTYIrMYKvjGcCDOiRkSiy
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8;
	// System.Int32 Rewired.Data.UserData_vtmWSvFrgdaypojpqUMZJYvnxRQ::mvDwtqBgtvgsghrHMOXYpHOdbsaR
	int32_t ___mvDwtqBgtvgsghrHMOXYpHOdbsaR_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_sLDDgBamhzadZbhcjNSKCejHvMJN_6() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___sLDDgBamhzadZbhcjNSKCejHvMJN_6)); }
	inline int32_t get_sLDDgBamhzadZbhcjNSKCejHvMJN_6() const { return ___sLDDgBamhzadZbhcjNSKCejHvMJN_6; }
	inline int32_t* get_address_of_sLDDgBamhzadZbhcjNSKCejHvMJN_6() { return &___sLDDgBamhzadZbhcjNSKCejHvMJN_6; }
	inline void set_sLDDgBamhzadZbhcjNSKCejHvMJN_6(int32_t value)
	{
		___sLDDgBamhzadZbhcjNSKCejHvMJN_6 = value;
	}

	inline static int32_t get_offset_of_ujkBcIBmJgeqPhNLwLuqtFbBzmU_7() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___ujkBcIBmJgeqPhNLwLuqtFbBzmU_7)); }
	inline int32_t get_ujkBcIBmJgeqPhNLwLuqtFbBzmU_7() const { return ___ujkBcIBmJgeqPhNLwLuqtFbBzmU_7; }
	inline int32_t* get_address_of_ujkBcIBmJgeqPhNLwLuqtFbBzmU_7() { return &___ujkBcIBmJgeqPhNLwLuqtFbBzmU_7; }
	inline void set_ujkBcIBmJgeqPhNLwLuqtFbBzmU_7(int32_t value)
	{
		___ujkBcIBmJgeqPhNLwLuqtFbBzmU_7 = value;
	}

	inline static int32_t get_offset_of_vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8() const { return ___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8() { return &___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8; }
	inline void set_vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8 = value;
		Il2CppCodeGenWriteBarrier((&___vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8), value);
	}

	inline static int32_t get_offset_of_mvDwtqBgtvgsghrHMOXYpHOdbsaR_9() { return static_cast<int32_t>(offsetof(vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217, ___mvDwtqBgtvgsghrHMOXYpHOdbsaR_9)); }
	inline int32_t get_mvDwtqBgtvgsghrHMOXYpHOdbsaR_9() const { return ___mvDwtqBgtvgsghrHMOXYpHOdbsaR_9; }
	inline int32_t* get_address_of_mvDwtqBgtvgsghrHMOXYpHOdbsaR_9() { return &___mvDwtqBgtvgsghrHMOXYpHOdbsaR_9; }
	inline void set_mvDwtqBgtvgsghrHMOXYpHOdbsaR_9(int32_t value)
	{
		___mvDwtqBgtvgsghrHMOXYpHOdbsaR_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VTMWSVFRGDAYPOJPQUMZJYVNXRQ_T55B1216A392D60366B63C317F586BA6F1EC99217_H
#ifndef WCQHAEYBZCAECQBSQSBYTWBPDCR_TFCB33AC3776188C472BFF89C4B25CB136AE668F1_H
#define WCQHAEYBZCAECQBSQSBYTWBPDCR_TFCB33AC3776188C472BFF89C4B25CB136AE668F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR
struct  wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1  : public RuntimeObject
{
public:
	// Rewired.InputMapCategory Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::NOArrsNdfKDShNFftfbPqYDzhpq
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 Rewired.Data.UserData_wCqhaeyBzcaEcqBSqsBYTwBpDCR::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_6() { return static_cast<int32_t>(offsetof(wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1, ___okMFupfyoBNZDyKMyvAtwGIUiAd_6)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_6() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_6; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_6() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_6; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_6(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WCQHAEYBZCAECQBSQSBYTWBPDCR_TFCB33AC3776188C472BFF89C4B25CB136AE668F1_H
#ifndef YXWJNZEFYIGUCBRHXXEGTZGOKTTG_TB9A14A9CC7B45ED64C87050EF2DC57769120B4C1_H
#define YXWJNZEFYIGUCBRHXXEGTZGOKTTG_TB9A14A9CC7B45ED64C87050EF2DC57769120B4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG
struct  yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::NOArrsNdfKDShNFftfbPqYDzhpq
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::chTbMAJBwugvHvDEnzgDSlLnSqg
	int32_t ___chTbMAJBwugvHvDEnzgDSlLnSqg_4;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::WCLOpuzlAPeqPEgTxJoAeJojJdS
	int32_t ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5;
	// System.Boolean Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::PfBtErLFmgIwzViqjbjnyRbCRuw
	bool ___PfBtErLFmgIwzViqjbjnyRbCRuw_6;
	// System.Boolean Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::ZyLlrOVJdDTGzRxMxdMMRTlifBO
	bool ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::jIRMFIFwtWwANmtBUtLpLFbZRmp
	int32_t ___jIRMFIFwtWwANmtBUtLpLFbZRmp_8;
	// Rewired.InputAction Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::kIkMphgGrIErrMhJQsfdwjJIwHr
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___kIkMphgGrIErrMhJQsfdwjJIwHr_9;
	// System.Int32 Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::eLAERvBUfLIWiIfMBoNRaRMTEvp
	int32_t ___eLAERvBUfLIWiIfMBoNRaRMTEvp_10;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.UserData_yXwjnzeFyIgucBrhXxeGtZGoKtTG::EnyxBHqQzSOfLzNTXVsnPwUnDTg
	RuntimeObject* ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___chTbMAJBwugvHvDEnzgDSlLnSqg_4)); }
	inline int32_t get_chTbMAJBwugvHvDEnzgDSlLnSqg_4() const { return ___chTbMAJBwugvHvDEnzgDSlLnSqg_4; }
	inline int32_t* get_address_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4() { return &___chTbMAJBwugvHvDEnzgDSlLnSqg_4; }
	inline void set_chTbMAJBwugvHvDEnzgDSlLnSqg_4(int32_t value)
	{
		___chTbMAJBwugvHvDEnzgDSlLnSqg_4 = value;
	}

	inline static int32_t get_offset_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5)); }
	inline int32_t get_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() const { return ___WCLOpuzlAPeqPEgTxJoAeJojJdS_5; }
	inline int32_t* get_address_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5() { return &___WCLOpuzlAPeqPEgTxJoAeJojJdS_5; }
	inline void set_WCLOpuzlAPeqPEgTxJoAeJojJdS_5(int32_t value)
	{
		___WCLOpuzlAPeqPEgTxJoAeJojJdS_5 = value;
	}

	inline static int32_t get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___PfBtErLFmgIwzViqjbjnyRbCRuw_6)); }
	inline bool get_PfBtErLFmgIwzViqjbjnyRbCRuw_6() const { return ___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline bool* get_address_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6() { return &___PfBtErLFmgIwzViqjbjnyRbCRuw_6; }
	inline void set_PfBtErLFmgIwzViqjbjnyRbCRuw_6(bool value)
	{
		___PfBtErLFmgIwzViqjbjnyRbCRuw_6 = value;
	}

	inline static int32_t get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7)); }
	inline bool get_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() const { return ___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline bool* get_address_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7() { return &___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7; }
	inline void set_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(bool value)
	{
		___ZyLlrOVJdDTGzRxMxdMMRTlifBO_7 = value;
	}

	inline static int32_t get_offset_of_jIRMFIFwtWwANmtBUtLpLFbZRmp_8() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___jIRMFIFwtWwANmtBUtLpLFbZRmp_8)); }
	inline int32_t get_jIRMFIFwtWwANmtBUtLpLFbZRmp_8() const { return ___jIRMFIFwtWwANmtBUtLpLFbZRmp_8; }
	inline int32_t* get_address_of_jIRMFIFwtWwANmtBUtLpLFbZRmp_8() { return &___jIRMFIFwtWwANmtBUtLpLFbZRmp_8; }
	inline void set_jIRMFIFwtWwANmtBUtLpLFbZRmp_8(int32_t value)
	{
		___jIRMFIFwtWwANmtBUtLpLFbZRmp_8 = value;
	}

	inline static int32_t get_offset_of_kIkMphgGrIErrMhJQsfdwjJIwHr_9() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___kIkMphgGrIErrMhJQsfdwjJIwHr_9)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_kIkMphgGrIErrMhJQsfdwjJIwHr_9() const { return ___kIkMphgGrIErrMhJQsfdwjJIwHr_9; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_kIkMphgGrIErrMhJQsfdwjJIwHr_9() { return &___kIkMphgGrIErrMhJQsfdwjJIwHr_9; }
	inline void set_kIkMphgGrIErrMhJQsfdwjJIwHr_9(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___kIkMphgGrIErrMhJQsfdwjJIwHr_9 = value;
		Il2CppCodeGenWriteBarrier((&___kIkMphgGrIErrMhJQsfdwjJIwHr_9), value);
	}

	inline static int32_t get_offset_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_10() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___eLAERvBUfLIWiIfMBoNRaRMTEvp_10)); }
	inline int32_t get_eLAERvBUfLIWiIfMBoNRaRMTEvp_10() const { return ___eLAERvBUfLIWiIfMBoNRaRMTEvp_10; }
	inline int32_t* get_address_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_10() { return &___eLAERvBUfLIWiIfMBoNRaRMTEvp_10; }
	inline void set_eLAERvBUfLIWiIfMBoNRaRMTEvp_10(int32_t value)
	{
		___eLAERvBUfLIWiIfMBoNRaRMTEvp_10 = value;
	}

	inline static int32_t get_offset_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_11() { return static_cast<int32_t>(offsetof(yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1, ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11)); }
	inline RuntimeObject* get_EnyxBHqQzSOfLzNTXVsnPwUnDTg_11() const { return ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11; }
	inline RuntimeObject** get_address_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_11() { return &___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11; }
	inline void set_EnyxBHqQzSOfLzNTXVsnPwUnDTg_11(RuntimeObject* value)
	{
		___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11 = value;
		Il2CppCodeGenWriteBarrier((&___EnyxBHqQzSOfLzNTXVsnPwUnDTg_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YXWJNZEFYIGUCBRHXXEGTZGOKTTG_TB9A14A9CC7B45ED64C87050EF2DC57769120B4C1_H
#ifndef YYVTDUJNHNGJMCRVXDYOGHAGMDIL_TF0B89C8B7D9373EB175FF7900C87236B0C516B38_H
#define YYVTDUJNHNGJMCRVXDYOGHAGMDIL_TF0B89C8B7D9373EB175FF7900C87236B0C516B38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL
struct  yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38  : public RuntimeObject
{
public:
	// Rewired.InputMapCategory Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL::NOArrsNdfKDShNFftfbPqYDzhpq
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.UserData Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL::DKaFTWTMhFeLQHDExnDMRxZfmROL
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.UserData_yYVtdUjnHnGjMCrVxdyOghAgMdIL::yLrOOaqYzmIyaXSCvApOKYnGotw
	int32_t ___yLrOOaqYzmIyaXSCvApOKYnGotw_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return static_cast<int32_t>(offsetof(yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38, ___yLrOOaqYzmIyaXSCvApOKYnGotw_4)); }
	inline int32_t get_yLrOOaqYzmIyaXSCvApOKYnGotw_4() const { return ___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline int32_t* get_address_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return &___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline void set_yLrOOaqYzmIyaXSCvApOKYnGotw_4(int32_t value)
	{
		___yLrOOaqYzmIyaXSCvApOKYnGotw_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYVTDUJNHNGJMCRVXDYOGHAGMDIL_TF0B89C8B7D9373EB175FF7900C87236B0C516B38_H
#ifndef GCPROFILER_TB3D8D64F6EB5F8F4B950FC3EA6F2289383E69454_H
#define GCPROFILER_TB3D8D64F6EB5F8F4B950FC3EA6F2289383E69454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.GCProfiler
struct  GCProfiler_tB3D8D64F6EB5F8F4B950FC3EA6F2289383E69454  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCPROFILER_TB3D8D64F6EB5F8F4B950FC3EA6F2289383E69454_H
#ifndef LOGGER_TCB983C73CC4B5291036A949C72056086AD9DD9A1_H
#define LOGGER_TCB983C73CC4B5291036A949C72056086AD9DD9A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Logger
struct  Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1  : public RuntimeObject
{
public:

public:
};

struct Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> Rewired.Logger::__screenLog
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * _____screenLog_1;
	// Rewired.Internal.GUIText Rewired.Logger::_guiText
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * ____guiText_2;
	// System.Boolean Rewired.Logger::_logToScreen
	bool ____logToScreen_3;

public:
	inline static int32_t get_offset_of___screenLog_1() { return static_cast<int32_t>(offsetof(Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields, _____screenLog_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get___screenLog_1() const { return _____screenLog_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of___screenLog_1() { return &_____screenLog_1; }
	inline void set___screenLog_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		_____screenLog_1 = value;
		Il2CppCodeGenWriteBarrier((&_____screenLog_1), value);
	}

	inline static int32_t get_offset_of__guiText_2() { return static_cast<int32_t>(offsetof(Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields, ____guiText_2)); }
	inline GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * get__guiText_2() const { return ____guiText_2; }
	inline GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 ** get_address_of__guiText_2() { return &____guiText_2; }
	inline void set__guiText_2(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * value)
	{
		____guiText_2 = value;
		Il2CppCodeGenWriteBarrier((&____guiText_2), value);
	}

	inline static int32_t get_offset_of__logToScreen_3() { return static_cast<int32_t>(offsetof(Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields, ____logToScreen_3)); }
	inline bool get__logToScreen_3() const { return ____logToScreen_3; }
	inline bool* get_address_of__logToScreen_3() { return &____logToScreen_3; }
	inline void set__logToScreen_3(bool value)
	{
		____logToScreen_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_TCB983C73CC4B5291036A949C72056086AD9DD9A1_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CUSTOMCLASSOBFUSCATION_TBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859_H
#define CUSTOMCLASSOBFUSCATION_TBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CustomClassObfuscation
struct  CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Rewired.CustomClassObfuscation::renamePubIntMembers
	bool ___renamePubIntMembers_0;
	// System.Boolean Rewired.CustomClassObfuscation::renamePrivateMembers
	bool ___renamePrivateMembers_1;

public:
	inline static int32_t get_offset_of_renamePubIntMembers_0() { return static_cast<int32_t>(offsetof(CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859, ___renamePubIntMembers_0)); }
	inline bool get_renamePubIntMembers_0() const { return ___renamePubIntMembers_0; }
	inline bool* get_address_of_renamePubIntMembers_0() { return &___renamePubIntMembers_0; }
	inline void set_renamePubIntMembers_0(bool value)
	{
		___renamePubIntMembers_0 = value;
	}

	inline static int32_t get_offset_of_renamePrivateMembers_1() { return static_cast<int32_t>(offsetof(CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859, ___renamePrivateMembers_1)); }
	inline bool get_renamePrivateMembers_1() const { return ___renamePrivateMembers_1; }
	inline bool* get_address_of_renamePrivateMembers_1() { return &___renamePrivateMembers_1; }
	inline void set_renamePrivateMembers_1(bool value)
	{
		___renamePrivateMembers_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCLASSOBFUSCATION_TBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859_H
#ifndef CUSTOMOBFUSCATION_T5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8_H
#define CUSTOMOBFUSCATION_T5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CustomObfuscation
struct  CustomObfuscation_t5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Rewired.CustomObfuscation::rename
	bool ___rename_0;

public:
	inline static int32_t get_offset_of_rename_0() { return static_cast<int32_t>(offsetof(CustomObfuscation_t5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8, ___rename_0)); }
	inline bool get_rename_0() const { return ___rename_0; }
	inline bool* get_address_of_rename_0() { return &___rename_0; }
	inline void set_rename_0(bool value)
	{
		___rename_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMOBFUSCATION_T5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8_H
#ifndef PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#define PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PidVid
struct  PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D 
{
public:
	// System.UInt16 Rewired.PidVid::productId
	uint16_t ___productId_1;
	// System.UInt16 Rewired.PidVid::vendorId
	uint16_t ___vendorId_2;

public:
	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___productId_1)); }
	inline uint16_t get_productId_1() const { return ___productId_1; }
	inline uint16_t* get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(uint16_t value)
	{
		___productId_1 = value;
	}

	inline static int32_t get_offset_of_vendorId_2() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___vendorId_2)); }
	inline uint16_t get_vendorId_2() const { return ___vendorId_2; }
	inline uint16_t* get_address_of_vendorId_2() { return &___vendorId_2; }
	inline void set_vendorId_2(uint16_t value)
	{
		___vendorId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifndef UPDATECONTROLLERINFOEVENTARGS_T2942188A682F1A914A0891D02CCEC456FC39985C_H
#define UPDATECONTROLLERINFOEVENTARGS_T2942188A682F1A914A0891D02CCEC456FC39985C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateControllerInfoEventArgs
struct  UpdateControllerInfoEventArgs_t2942188A682F1A914A0891D02CCEC456FC39985C  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// Rewired.Interfaces.IInputManagerJoystickPublic Rewired.UpdateControllerInfoEventArgs::sourceJoystick
	RuntimeObject* ___sourceJoystick_1;

public:
	inline static int32_t get_offset_of_sourceJoystick_1() { return static_cast<int32_t>(offsetof(UpdateControllerInfoEventArgs_t2942188A682F1A914A0891D02CCEC456FC39985C, ___sourceJoystick_1)); }
	inline RuntimeObject* get_sourceJoystick_1() const { return ___sourceJoystick_1; }
	inline RuntimeObject** get_address_of_sourceJoystick_1() { return &___sourceJoystick_1; }
	inline void set_sourceJoystick_1(RuntimeObject* value)
	{
		___sourceJoystick_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceJoystick_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECONTROLLERINFOEVENTARGS_T2942188A682F1A914A0891D02CCEC456FC39985C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#define AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivity2DType
struct  AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B 
{
public:
	// System.Int32 Rewired.AxisSensitivity2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef AXISTYPE_TD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED_H
#define AXISTYPE_TD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisType
struct  AxisType_tD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED 
{
public:
	// System.Int32 Rewired.AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_tD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_TD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED_H
#ifndef BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#define BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.BoolOption
struct  BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB 
{
public:
	// System.Int32 Rewired.BoolOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#ifndef BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#define BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateFlags
struct  ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A 
{
public:
	// System.Int32 Rewired.ButtonStateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifndef COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#define COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CompoundControllerElementType
struct  CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2 
{
public:
	// System.Int32 Rewired.CompoundControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifndef CONTROLDEVICETYPE_T3E38FA7371FC471A58C41C5A7529D7A0CFDB810B_H
#define CONTROLDEVICETYPE_T3E38FA7371FC471A58C41C5A7529D7A0CFDB810B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControlDeviceType
struct  ControlDeviceType_t3E38FA7371FC471A58C41C5A7529D7A0CFDB810B 
{
public:
	// System.Int32 Rewired.ControlDeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlDeviceType_t3E38FA7371FC471A58C41C5A7529D7A0CFDB810B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLDEVICETYPE_T3E38FA7371FC471A58C41C5A7529D7A0CFDB810B_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERSUBTYPE_T05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3_H
#define CONTROLLERSUBTYPE_T05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerSubType
struct  ControllerSubType_t05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3 
{
public:
	// System.Int32 Rewired.ControllerSubType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerSubType_t05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSUBTYPE_T05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#define DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.DeadZone2DType
struct  DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586 
{
public:
	// System.Int32 Rewired.DeadZone2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#define INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionEventType
struct  InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC 
{
public:
	// System.Int32 Rewired.InputActionEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#ifndef INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#define INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionType
struct  InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4 
{
public:
	// System.Int32 Rewired.InputActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#ifndef INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#define INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputPlatform
struct  InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E 
{
public:
	// System.Int32 Rewired.InputPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#define JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.JoystickType
struct  JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0 
{
public:
	// System.Int32 Rewired.JoystickType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#ifndef KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#define KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.KeyboardKeyCode
struct  KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854 
{
public:
	// System.Int32 Rewired.KeyboardKeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifndef MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#define MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKey
struct  ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0 
{
public:
	// System.Int32 Rewired.ModifierKey::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef MOUSEINPUTELEMENT_T90CF67800AE68523F4A64FE82F58DE4AB22A66F8_H
#define MOUSEINPUTELEMENT_T90CF67800AE68523F4A64FE82F58DE4AB22A66F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseInputElement
struct  MouseInputElement_t90CF67800AE68523F4A64FE82F58DE4AB22A66F8 
{
public:
	// System.Int32 Rewired.MouseInputElement::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseInputElement_t90CF67800AE68523F4A64FE82F58DE4AB22A66F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEINPUTELEMENT_T90CF67800AE68523F4A64FE82F58DE4AB22A66F8_H
#ifndef MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#define MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseOtherAxisMode
struct  MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569 
{
public:
	// System.Int32 Rewired.MouseOtherAxisMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#ifndef MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#define MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseXYAxisDeltaCalc
struct  MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB 
{
public:
	// System.Int32 Rewired.MouseXYAxisDeltaCalc::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#ifndef MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#define MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseXYAxisMode
struct  MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2 
{
public:
	// System.Int32 Rewired.MouseXYAxisMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#ifndef MULTIBOOLVALUE_TEEA4D6B314C3C9411952A6844294F5213E0B80D3_H
#define MULTIBOOLVALUE_TEEA4D6B314C3C9411952A6844294F5213E0B80D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MultiBoolValue
struct  MultiBoolValue_tEEA4D6B314C3C9411952A6844294F5213E0B80D3 
{
public:
	// System.Int32 Rewired.MultiBoolValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MultiBoolValue_tEEA4D6B314C3C9411952A6844294F5213E0B80D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIBOOLVALUE_TEEA4D6B314C3C9411952A6844294F5213E0B80D3_H
#ifndef WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#define WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLGamepadMappingType
struct  WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854 
{
public:
	// System.Int32 Rewired.Platforms.WebGLGamepadMappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#ifndef WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#define WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLOSType
struct  WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939 
{
public:
	// System.Int32 Rewired.Platforms.WebGLOSType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#ifndef WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#define WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLWebBrowserType
struct  WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45 
{
public:
	// System.Int32 Rewired.Platforms.WebGLWebBrowserType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#ifndef XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#define XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputDeviceSubType
struct  XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82 
{
public:
	// System.Int32 Rewired.Platforms.XInputDeviceSubType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#define UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopType
struct  UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A 
{
public:
	// System.Int32 Rewired.UpdateLoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef JEWBEFDGFSJNJIWKKVIDIOYUBRQ_T37B06BC2A12683C511FA4EF47D83411B90DF99E6_H
#define JEWBEFDGFSJNJIWKKVIDIOYUBRQ_T37B06BC2A12683C511FA4EF47D83411B90DF99E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JEWBEfdgfsJnJIwKKvidiOYuBRq
struct  JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6  : public RuntimeObject
{
public:
	// System.Int32 JEWBEfdgfsJnJIwKKvidiOYuBRq::nvorGNbxnIKRTEQakQRxoDaDbkF
	int32_t ___nvorGNbxnIKRTEQakQRxoDaDbkF_0;
	// System.Int32 JEWBEfdgfsJnJIwKKvidiOYuBRq::IWhEyCFiNqzvkyFxBqcPsCDkGwE
	int32_t ___IWhEyCFiNqzvkyFxBqcPsCDkGwE_1;
	// System.Guid JEWBEfdgfsJnJIwKKvidiOYuBRq::YDlaIMRNnbaSMyDyufrzqGbKOHj
	Guid_t  ___YDlaIMRNnbaSMyDyufrzqGbKOHj_2;
	// Rewired.InputSource JEWBEfdgfsJnJIwKKvidiOYuBRq::zSCjKsgSKyXaLUKczdjHusCWAAC
	int32_t ___zSCjKsgSKyXaLUKczdjHusCWAAC_3;
	// System.String JEWBEfdgfsJnJIwKKvidiOYuBRq::reNFYQGuxlDRPhTirIkZzgdrffbW
	String_t* ___reNFYQGuxlDRPhTirIkZzgdrffbW_4;
	// System.String JEWBEfdgfsJnJIwKKvidiOYuBRq::BZCZtCnEaEEjShgHQslaWPjqdiI
	String_t* ___BZCZtCnEaEEjShgHQslaWPjqdiI_5;
	// System.String JEWBEfdgfsJnJIwKKvidiOYuBRq::CRkueCzBdihMDjIglcWKKSWZfkB
	String_t* ___CRkueCzBdihMDjIglcWKKSWZfkB_6;
	// System.Int32 JEWBEfdgfsJnJIwKKvidiOYuBRq::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_7;
	// System.Int32 JEWBEfdgfsJnJIwKKvidiOYuBRq::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8;
	// Rewired.HardwareControllerMap_Game JEWBEfdgfsJnJIwKKvidiOYuBRq::cEbPZtIbxEvXmcDiLibfUegVyao
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___cEbPZtIbxEvXmcDiLibfUegVyao_9;

public:
	inline static int32_t get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___nvorGNbxnIKRTEQakQRxoDaDbkF_0)); }
	inline int32_t get_nvorGNbxnIKRTEQakQRxoDaDbkF_0() const { return ___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline int32_t* get_address_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return &___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline void set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(int32_t value)
	{
		___nvorGNbxnIKRTEQakQRxoDaDbkF_0 = value;
	}

	inline static int32_t get_offset_of_IWhEyCFiNqzvkyFxBqcPsCDkGwE_1() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___IWhEyCFiNqzvkyFxBqcPsCDkGwE_1)); }
	inline int32_t get_IWhEyCFiNqzvkyFxBqcPsCDkGwE_1() const { return ___IWhEyCFiNqzvkyFxBqcPsCDkGwE_1; }
	inline int32_t* get_address_of_IWhEyCFiNqzvkyFxBqcPsCDkGwE_1() { return &___IWhEyCFiNqzvkyFxBqcPsCDkGwE_1; }
	inline void set_IWhEyCFiNqzvkyFxBqcPsCDkGwE_1(int32_t value)
	{
		___IWhEyCFiNqzvkyFxBqcPsCDkGwE_1 = value;
	}

	inline static int32_t get_offset_of_YDlaIMRNnbaSMyDyufrzqGbKOHj_2() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___YDlaIMRNnbaSMyDyufrzqGbKOHj_2)); }
	inline Guid_t  get_YDlaIMRNnbaSMyDyufrzqGbKOHj_2() const { return ___YDlaIMRNnbaSMyDyufrzqGbKOHj_2; }
	inline Guid_t * get_address_of_YDlaIMRNnbaSMyDyufrzqGbKOHj_2() { return &___YDlaIMRNnbaSMyDyufrzqGbKOHj_2; }
	inline void set_YDlaIMRNnbaSMyDyufrzqGbKOHj_2(Guid_t  value)
	{
		___YDlaIMRNnbaSMyDyufrzqGbKOHj_2 = value;
	}

	inline static int32_t get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___zSCjKsgSKyXaLUKczdjHusCWAAC_3)); }
	inline int32_t get_zSCjKsgSKyXaLUKczdjHusCWAAC_3() const { return ___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline int32_t* get_address_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return &___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline void set_zSCjKsgSKyXaLUKczdjHusCWAAC_3(int32_t value)
	{
		___zSCjKsgSKyXaLUKczdjHusCWAAC_3 = value;
	}

	inline static int32_t get_offset_of_reNFYQGuxlDRPhTirIkZzgdrffbW_4() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___reNFYQGuxlDRPhTirIkZzgdrffbW_4)); }
	inline String_t* get_reNFYQGuxlDRPhTirIkZzgdrffbW_4() const { return ___reNFYQGuxlDRPhTirIkZzgdrffbW_4; }
	inline String_t** get_address_of_reNFYQGuxlDRPhTirIkZzgdrffbW_4() { return &___reNFYQGuxlDRPhTirIkZzgdrffbW_4; }
	inline void set_reNFYQGuxlDRPhTirIkZzgdrffbW_4(String_t* value)
	{
		___reNFYQGuxlDRPhTirIkZzgdrffbW_4 = value;
		Il2CppCodeGenWriteBarrier((&___reNFYQGuxlDRPhTirIkZzgdrffbW_4), value);
	}

	inline static int32_t get_offset_of_BZCZtCnEaEEjShgHQslaWPjqdiI_5() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___BZCZtCnEaEEjShgHQslaWPjqdiI_5)); }
	inline String_t* get_BZCZtCnEaEEjShgHQslaWPjqdiI_5() const { return ___BZCZtCnEaEEjShgHQslaWPjqdiI_5; }
	inline String_t** get_address_of_BZCZtCnEaEEjShgHQslaWPjqdiI_5() { return &___BZCZtCnEaEEjShgHQslaWPjqdiI_5; }
	inline void set_BZCZtCnEaEEjShgHQslaWPjqdiI_5(String_t* value)
	{
		___BZCZtCnEaEEjShgHQslaWPjqdiI_5 = value;
		Il2CppCodeGenWriteBarrier((&___BZCZtCnEaEEjShgHQslaWPjqdiI_5), value);
	}

	inline static int32_t get_offset_of_CRkueCzBdihMDjIglcWKKSWZfkB_6() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___CRkueCzBdihMDjIglcWKKSWZfkB_6)); }
	inline String_t* get_CRkueCzBdihMDjIglcWKKSWZfkB_6() const { return ___CRkueCzBdihMDjIglcWKKSWZfkB_6; }
	inline String_t** get_address_of_CRkueCzBdihMDjIglcWKKSWZfkB_6() { return &___CRkueCzBdihMDjIglcWKKSWZfkB_6; }
	inline void set_CRkueCzBdihMDjIglcWKKSWZfkB_6(String_t* value)
	{
		___CRkueCzBdihMDjIglcWKKSWZfkB_6 = value;
		Il2CppCodeGenWriteBarrier((&___CRkueCzBdihMDjIglcWKKSWZfkB_6), value);
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_7() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___itTafNyeQoucrbhPkPsSqRewAEI_7)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_7() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_7; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_7() { return &___itTafNyeQoucrbhPkPsSqRewAEI_7; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_7(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_7 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_8; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_8(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_8 = value;
	}

	inline static int32_t get_offset_of_cEbPZtIbxEvXmcDiLibfUegVyao_9() { return static_cast<int32_t>(offsetof(JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6, ___cEbPZtIbxEvXmcDiLibfUegVyao_9)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_cEbPZtIbxEvXmcDiLibfUegVyao_9() const { return ___cEbPZtIbxEvXmcDiLibfUegVyao_9; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_cEbPZtIbxEvXmcDiLibfUegVyao_9() { return &___cEbPZtIbxEvXmcDiLibfUegVyao_9; }
	inline void set_cEbPZtIbxEvXmcDiLibfUegVyao_9(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___cEbPZtIbxEvXmcDiLibfUegVyao_9 = value;
		Il2CppCodeGenWriteBarrier((&___cEbPZtIbxEvXmcDiLibfUegVyao_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JEWBEFDGFSJNJIWKKVIDIOYUBRQ_T37B06BC2A12683C511FA4EF47D83411B90DF99E6_H
#ifndef BRIDGEDCONTROLLERHWINFO_T804A11568AEF5B3FA5443C76FAE190657808FAF6_H
#define BRIDGEDCONTROLLERHWINFO_T804A11568AEF5B3FA5443C76FAE190657808FAF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.BridgedControllerHWInfo
struct  BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6  : public RuntimeObject
{
public:
	// System.Boolean Rewired.BridgedControllerHWInfo::isMock
	bool ___isMock_0;
	// Rewired.InputSource Rewired.BridgedControllerHWInfo::inputManagerSource
	int32_t ___inputManagerSource_1;
	// Rewired.InputSource Rewired.BridgedControllerHWInfo::inputSource
	int32_t ___inputSource_2;
	// Rewired.ControlDeviceType Rewired.BridgedControllerHWInfo::deviceType
	int32_t ___deviceType_3;
	// System.String Rewired.BridgedControllerHWInfo::hardwareIdentifier
	String_t* ___hardwareIdentifier_4;
	// System.Int32 Rewired.BridgedControllerHWInfo::hardwareAxisCount
	int32_t ___hardwareAxisCount_5;
	// System.Int32 Rewired.BridgedControllerHWInfo::hardwareButtonCount
	int32_t ___hardwareButtonCount_6;
	// System.Int32 Rewired.BridgedControllerHWInfo::hardwareHatCount
	int32_t ___hardwareHatCount_7;
	// System.String Rewired.BridgedControllerHWInfo::hw_productName
	String_t* ___hw_productName_8;
	// Rewired.PidVid Rewired.BridgedControllerHWInfo::hw_pidVid
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  ___hw_pidVid_9;
	// System.Guid Rewired.BridgedControllerHWInfo::hw_deviceGuid
	Guid_t  ___hw_deviceGuid_10;
	// System.Int32 Rewired.BridgedControllerHWInfo::hw_productId
	int32_t ___hw_productId_11;
	// System.String Rewired.BridgedControllerHWInfo::hw_bluetoothDeviceName
	String_t* ___hw_bluetoothDeviceName_12;
	// System.Boolean Rewired.BridgedControllerHWInfo::hw_isBluetoothDevice
	bool ___hw_isBluetoothDevice_13;
	// System.Boolean Rewired.BridgedControllerHWInfo::hw_supportsVoice
	bool ___hw_supportsVoice_14;
	// System.Boolean Rewired.BridgedControllerHWInfo::hw_supportsVibration
	bool ___hw_supportsVibration_15;
	// Rewired.Platforms.XInputDeviceSubType Rewired.BridgedControllerHWInfo::hw_xInputSubType
	int32_t ___hw_xInputSubType_16;
	// System.String Rewired.BridgedControllerHWInfo::hw_manufacturer
	String_t* ___hw_manufacturer_17;
	// System.String Rewired.BridgedControllerHWInfo::hw_serialNumber
	String_t* ___hw_serialNumber_18;
	// System.Int32 Rewired.BridgedControllerHWInfo::hw_vendorId
	int32_t ___hw_vendorId_19;
	// System.Int32 Rewired.BridgedControllerHWInfo::hw_version
	int32_t ___hw_version_20;
	// System.String Rewired.BridgedControllerHWInfo::hw_systemDeviceName
	String_t* ___hw_systemDeviceName_21;
	// System.Boolean Rewired.BridgedControllerHWInfo::hw_isSDL2Gamepad
	bool ___hw_isSDL2Gamepad_22;
	// Rewired.Platforms.WebGLWebBrowserType Rewired.BridgedControllerHWInfo::webGL_webBrowserType
	int32_t ___webGL_webBrowserType_23;
	// Rewired.Platforms.WebGLOSType Rewired.BridgedControllerHWInfo::webGL_osType
	int32_t ___webGL_osType_24;
	// Rewired.Platforms.WebGLGamepadMappingType Rewired.BridgedControllerHWInfo::webGL_mappingType
	int32_t ___webGL_mappingType_25;
	// System.String[] Rewired.BridgedControllerHWInfo::webGL_webBrowserVersionSplit
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___webGL_webBrowserVersionSplit_26;
	// System.String[] Rewired.BridgedControllerHWInfo::webGL_osVersionSplit
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___webGL_osVersionSplit_27;
	// System.Int32 Rewired.BridgedControllerHWInfo::hw_localVibrationMotorCount
	int32_t ___hw_localVibrationMotorCount_28;
	// System.String Rewired.BridgedControllerHWInfo::definitionMatchTag
	String_t* ___definitionMatchTag_29;

public:
	inline static int32_t get_offset_of_isMock_0() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___isMock_0)); }
	inline bool get_isMock_0() const { return ___isMock_0; }
	inline bool* get_address_of_isMock_0() { return &___isMock_0; }
	inline void set_isMock_0(bool value)
	{
		___isMock_0 = value;
	}

	inline static int32_t get_offset_of_inputManagerSource_1() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___inputManagerSource_1)); }
	inline int32_t get_inputManagerSource_1() const { return ___inputManagerSource_1; }
	inline int32_t* get_address_of_inputManagerSource_1() { return &___inputManagerSource_1; }
	inline void set_inputManagerSource_1(int32_t value)
	{
		___inputManagerSource_1 = value;
	}

	inline static int32_t get_offset_of_inputSource_2() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___inputSource_2)); }
	inline int32_t get_inputSource_2() const { return ___inputSource_2; }
	inline int32_t* get_address_of_inputSource_2() { return &___inputSource_2; }
	inline void set_inputSource_2(int32_t value)
	{
		___inputSource_2 = value;
	}

	inline static int32_t get_offset_of_deviceType_3() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___deviceType_3)); }
	inline int32_t get_deviceType_3() const { return ___deviceType_3; }
	inline int32_t* get_address_of_deviceType_3() { return &___deviceType_3; }
	inline void set_deviceType_3(int32_t value)
	{
		___deviceType_3 = value;
	}

	inline static int32_t get_offset_of_hardwareIdentifier_4() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hardwareIdentifier_4)); }
	inline String_t* get_hardwareIdentifier_4() const { return ___hardwareIdentifier_4; }
	inline String_t** get_address_of_hardwareIdentifier_4() { return &___hardwareIdentifier_4; }
	inline void set_hardwareIdentifier_4(String_t* value)
	{
		___hardwareIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___hardwareIdentifier_4), value);
	}

	inline static int32_t get_offset_of_hardwareAxisCount_5() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hardwareAxisCount_5)); }
	inline int32_t get_hardwareAxisCount_5() const { return ___hardwareAxisCount_5; }
	inline int32_t* get_address_of_hardwareAxisCount_5() { return &___hardwareAxisCount_5; }
	inline void set_hardwareAxisCount_5(int32_t value)
	{
		___hardwareAxisCount_5 = value;
	}

	inline static int32_t get_offset_of_hardwareButtonCount_6() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hardwareButtonCount_6)); }
	inline int32_t get_hardwareButtonCount_6() const { return ___hardwareButtonCount_6; }
	inline int32_t* get_address_of_hardwareButtonCount_6() { return &___hardwareButtonCount_6; }
	inline void set_hardwareButtonCount_6(int32_t value)
	{
		___hardwareButtonCount_6 = value;
	}

	inline static int32_t get_offset_of_hardwareHatCount_7() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hardwareHatCount_7)); }
	inline int32_t get_hardwareHatCount_7() const { return ___hardwareHatCount_7; }
	inline int32_t* get_address_of_hardwareHatCount_7() { return &___hardwareHatCount_7; }
	inline void set_hardwareHatCount_7(int32_t value)
	{
		___hardwareHatCount_7 = value;
	}

	inline static int32_t get_offset_of_hw_productName_8() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_productName_8)); }
	inline String_t* get_hw_productName_8() const { return ___hw_productName_8; }
	inline String_t** get_address_of_hw_productName_8() { return &___hw_productName_8; }
	inline void set_hw_productName_8(String_t* value)
	{
		___hw_productName_8 = value;
		Il2CppCodeGenWriteBarrier((&___hw_productName_8), value);
	}

	inline static int32_t get_offset_of_hw_pidVid_9() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_pidVid_9)); }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  get_hw_pidVid_9() const { return ___hw_pidVid_9; }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D * get_address_of_hw_pidVid_9() { return &___hw_pidVid_9; }
	inline void set_hw_pidVid_9(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  value)
	{
		___hw_pidVid_9 = value;
	}

	inline static int32_t get_offset_of_hw_deviceGuid_10() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_deviceGuid_10)); }
	inline Guid_t  get_hw_deviceGuid_10() const { return ___hw_deviceGuid_10; }
	inline Guid_t * get_address_of_hw_deviceGuid_10() { return &___hw_deviceGuid_10; }
	inline void set_hw_deviceGuid_10(Guid_t  value)
	{
		___hw_deviceGuid_10 = value;
	}

	inline static int32_t get_offset_of_hw_productId_11() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_productId_11)); }
	inline int32_t get_hw_productId_11() const { return ___hw_productId_11; }
	inline int32_t* get_address_of_hw_productId_11() { return &___hw_productId_11; }
	inline void set_hw_productId_11(int32_t value)
	{
		___hw_productId_11 = value;
	}

	inline static int32_t get_offset_of_hw_bluetoothDeviceName_12() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_bluetoothDeviceName_12)); }
	inline String_t* get_hw_bluetoothDeviceName_12() const { return ___hw_bluetoothDeviceName_12; }
	inline String_t** get_address_of_hw_bluetoothDeviceName_12() { return &___hw_bluetoothDeviceName_12; }
	inline void set_hw_bluetoothDeviceName_12(String_t* value)
	{
		___hw_bluetoothDeviceName_12 = value;
		Il2CppCodeGenWriteBarrier((&___hw_bluetoothDeviceName_12), value);
	}

	inline static int32_t get_offset_of_hw_isBluetoothDevice_13() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_isBluetoothDevice_13)); }
	inline bool get_hw_isBluetoothDevice_13() const { return ___hw_isBluetoothDevice_13; }
	inline bool* get_address_of_hw_isBluetoothDevice_13() { return &___hw_isBluetoothDevice_13; }
	inline void set_hw_isBluetoothDevice_13(bool value)
	{
		___hw_isBluetoothDevice_13 = value;
	}

	inline static int32_t get_offset_of_hw_supportsVoice_14() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_supportsVoice_14)); }
	inline bool get_hw_supportsVoice_14() const { return ___hw_supportsVoice_14; }
	inline bool* get_address_of_hw_supportsVoice_14() { return &___hw_supportsVoice_14; }
	inline void set_hw_supportsVoice_14(bool value)
	{
		___hw_supportsVoice_14 = value;
	}

	inline static int32_t get_offset_of_hw_supportsVibration_15() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_supportsVibration_15)); }
	inline bool get_hw_supportsVibration_15() const { return ___hw_supportsVibration_15; }
	inline bool* get_address_of_hw_supportsVibration_15() { return &___hw_supportsVibration_15; }
	inline void set_hw_supportsVibration_15(bool value)
	{
		___hw_supportsVibration_15 = value;
	}

	inline static int32_t get_offset_of_hw_xInputSubType_16() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_xInputSubType_16)); }
	inline int32_t get_hw_xInputSubType_16() const { return ___hw_xInputSubType_16; }
	inline int32_t* get_address_of_hw_xInputSubType_16() { return &___hw_xInputSubType_16; }
	inline void set_hw_xInputSubType_16(int32_t value)
	{
		___hw_xInputSubType_16 = value;
	}

	inline static int32_t get_offset_of_hw_manufacturer_17() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_manufacturer_17)); }
	inline String_t* get_hw_manufacturer_17() const { return ___hw_manufacturer_17; }
	inline String_t** get_address_of_hw_manufacturer_17() { return &___hw_manufacturer_17; }
	inline void set_hw_manufacturer_17(String_t* value)
	{
		___hw_manufacturer_17 = value;
		Il2CppCodeGenWriteBarrier((&___hw_manufacturer_17), value);
	}

	inline static int32_t get_offset_of_hw_serialNumber_18() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_serialNumber_18)); }
	inline String_t* get_hw_serialNumber_18() const { return ___hw_serialNumber_18; }
	inline String_t** get_address_of_hw_serialNumber_18() { return &___hw_serialNumber_18; }
	inline void set_hw_serialNumber_18(String_t* value)
	{
		___hw_serialNumber_18 = value;
		Il2CppCodeGenWriteBarrier((&___hw_serialNumber_18), value);
	}

	inline static int32_t get_offset_of_hw_vendorId_19() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_vendorId_19)); }
	inline int32_t get_hw_vendorId_19() const { return ___hw_vendorId_19; }
	inline int32_t* get_address_of_hw_vendorId_19() { return &___hw_vendorId_19; }
	inline void set_hw_vendorId_19(int32_t value)
	{
		___hw_vendorId_19 = value;
	}

	inline static int32_t get_offset_of_hw_version_20() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_version_20)); }
	inline int32_t get_hw_version_20() const { return ___hw_version_20; }
	inline int32_t* get_address_of_hw_version_20() { return &___hw_version_20; }
	inline void set_hw_version_20(int32_t value)
	{
		___hw_version_20 = value;
	}

	inline static int32_t get_offset_of_hw_systemDeviceName_21() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_systemDeviceName_21)); }
	inline String_t* get_hw_systemDeviceName_21() const { return ___hw_systemDeviceName_21; }
	inline String_t** get_address_of_hw_systemDeviceName_21() { return &___hw_systemDeviceName_21; }
	inline void set_hw_systemDeviceName_21(String_t* value)
	{
		___hw_systemDeviceName_21 = value;
		Il2CppCodeGenWriteBarrier((&___hw_systemDeviceName_21), value);
	}

	inline static int32_t get_offset_of_hw_isSDL2Gamepad_22() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_isSDL2Gamepad_22)); }
	inline bool get_hw_isSDL2Gamepad_22() const { return ___hw_isSDL2Gamepad_22; }
	inline bool* get_address_of_hw_isSDL2Gamepad_22() { return &___hw_isSDL2Gamepad_22; }
	inline void set_hw_isSDL2Gamepad_22(bool value)
	{
		___hw_isSDL2Gamepad_22 = value;
	}

	inline static int32_t get_offset_of_webGL_webBrowserType_23() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___webGL_webBrowserType_23)); }
	inline int32_t get_webGL_webBrowserType_23() const { return ___webGL_webBrowserType_23; }
	inline int32_t* get_address_of_webGL_webBrowserType_23() { return &___webGL_webBrowserType_23; }
	inline void set_webGL_webBrowserType_23(int32_t value)
	{
		___webGL_webBrowserType_23 = value;
	}

	inline static int32_t get_offset_of_webGL_osType_24() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___webGL_osType_24)); }
	inline int32_t get_webGL_osType_24() const { return ___webGL_osType_24; }
	inline int32_t* get_address_of_webGL_osType_24() { return &___webGL_osType_24; }
	inline void set_webGL_osType_24(int32_t value)
	{
		___webGL_osType_24 = value;
	}

	inline static int32_t get_offset_of_webGL_mappingType_25() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___webGL_mappingType_25)); }
	inline int32_t get_webGL_mappingType_25() const { return ___webGL_mappingType_25; }
	inline int32_t* get_address_of_webGL_mappingType_25() { return &___webGL_mappingType_25; }
	inline void set_webGL_mappingType_25(int32_t value)
	{
		___webGL_mappingType_25 = value;
	}

	inline static int32_t get_offset_of_webGL_webBrowserVersionSplit_26() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___webGL_webBrowserVersionSplit_26)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_webGL_webBrowserVersionSplit_26() const { return ___webGL_webBrowserVersionSplit_26; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_webGL_webBrowserVersionSplit_26() { return &___webGL_webBrowserVersionSplit_26; }
	inline void set_webGL_webBrowserVersionSplit_26(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___webGL_webBrowserVersionSplit_26 = value;
		Il2CppCodeGenWriteBarrier((&___webGL_webBrowserVersionSplit_26), value);
	}

	inline static int32_t get_offset_of_webGL_osVersionSplit_27() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___webGL_osVersionSplit_27)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_webGL_osVersionSplit_27() const { return ___webGL_osVersionSplit_27; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_webGL_osVersionSplit_27() { return &___webGL_osVersionSplit_27; }
	inline void set_webGL_osVersionSplit_27(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___webGL_osVersionSplit_27 = value;
		Il2CppCodeGenWriteBarrier((&___webGL_osVersionSplit_27), value);
	}

	inline static int32_t get_offset_of_hw_localVibrationMotorCount_28() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___hw_localVibrationMotorCount_28)); }
	inline int32_t get_hw_localVibrationMotorCount_28() const { return ___hw_localVibrationMotorCount_28; }
	inline int32_t* get_address_of_hw_localVibrationMotorCount_28() { return &___hw_localVibrationMotorCount_28; }
	inline void set_hw_localVibrationMotorCount_28(int32_t value)
	{
		___hw_localVibrationMotorCount_28 = value;
	}

	inline static int32_t get_offset_of_definitionMatchTag_29() { return static_cast<int32_t>(offsetof(BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6, ___definitionMatchTag_29)); }
	inline String_t* get_definitionMatchTag_29() const { return ___definitionMatchTag_29; }
	inline String_t** get_address_of_definitionMatchTag_29() { return &___definitionMatchTag_29; }
	inline void set_definitionMatchTag_29(String_t* value)
	{
		___definitionMatchTag_29 = value;
		Il2CppCodeGenWriteBarrier((&___definitionMatchTag_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIDGEDCONTROLLERHWINFO_T804A11568AEF5B3FA5443C76FAE190657808FAF6_H
#ifndef CONTROLLERDATAUPDATER_TBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F_H
#define CONTROLLERDATAUPDATER_TBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerDataUpdater
struct  ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F  : public RuntimeObject
{
public:
	// Rewired.InputSource Rewired.ControllerDataUpdater::source
	int32_t ___source_0;
	// System.Int32 Rewired.ControllerDataUpdater::axisCount
	int32_t ___axisCount_1;
	// System.Int32 Rewired.ControllerDataUpdater::buttonCount
	int32_t ___buttonCount_2;
	// System.Single[] Rewired.ControllerDataUpdater::axisValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___axisValues_3;
	// System.Boolean[] Rewired.ControllerDataUpdater::buttonValues
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___buttonValues_4;
	// System.Single[] Rewired.ControllerDataUpdater::buttonPressureValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___buttonPressureValues_5;
	// System.Boolean[] Rewired.ControllerDataUpdater::axisHasBeenPressedOSXLinux
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___axisHasBeenPressedOSXLinux_6;
	// Rewired.UnknownControllerHat[] Rewired.ControllerDataUpdater::rrqgLXltwFTZacqXaXZyPixozbt
	UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* ___rrqgLXltwFTZacqXaXZyPixozbt_7;
	// System.Boolean Rewired.ControllerDataUpdater::hasReceivedInput
	bool ___hasReceivedInput_8;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___source_0)); }
	inline int32_t get_source_0() const { return ___source_0; }
	inline int32_t* get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(int32_t value)
	{
		___source_0 = value;
	}

	inline static int32_t get_offset_of_axisCount_1() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___axisCount_1)); }
	inline int32_t get_axisCount_1() const { return ___axisCount_1; }
	inline int32_t* get_address_of_axisCount_1() { return &___axisCount_1; }
	inline void set_axisCount_1(int32_t value)
	{
		___axisCount_1 = value;
	}

	inline static int32_t get_offset_of_buttonCount_2() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___buttonCount_2)); }
	inline int32_t get_buttonCount_2() const { return ___buttonCount_2; }
	inline int32_t* get_address_of_buttonCount_2() { return &___buttonCount_2; }
	inline void set_buttonCount_2(int32_t value)
	{
		___buttonCount_2 = value;
	}

	inline static int32_t get_offset_of_axisValues_3() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___axisValues_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_axisValues_3() const { return ___axisValues_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_axisValues_3() { return &___axisValues_3; }
	inline void set_axisValues_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___axisValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___axisValues_3), value);
	}

	inline static int32_t get_offset_of_buttonValues_4() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___buttonValues_4)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_buttonValues_4() const { return ___buttonValues_4; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_buttonValues_4() { return &___buttonValues_4; }
	inline void set_buttonValues_4(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___buttonValues_4 = value;
		Il2CppCodeGenWriteBarrier((&___buttonValues_4), value);
	}

	inline static int32_t get_offset_of_buttonPressureValues_5() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___buttonPressureValues_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_buttonPressureValues_5() const { return ___buttonPressureValues_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_buttonPressureValues_5() { return &___buttonPressureValues_5; }
	inline void set_buttonPressureValues_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___buttonPressureValues_5 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPressureValues_5), value);
	}

	inline static int32_t get_offset_of_axisHasBeenPressedOSXLinux_6() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___axisHasBeenPressedOSXLinux_6)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_axisHasBeenPressedOSXLinux_6() const { return ___axisHasBeenPressedOSXLinux_6; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_axisHasBeenPressedOSXLinux_6() { return &___axisHasBeenPressedOSXLinux_6; }
	inline void set_axisHasBeenPressedOSXLinux_6(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___axisHasBeenPressedOSXLinux_6 = value;
		Il2CppCodeGenWriteBarrier((&___axisHasBeenPressedOSXLinux_6), value);
	}

	inline static int32_t get_offset_of_rrqgLXltwFTZacqXaXZyPixozbt_7() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___rrqgLXltwFTZacqXaXZyPixozbt_7)); }
	inline UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* get_rrqgLXltwFTZacqXaXZyPixozbt_7() const { return ___rrqgLXltwFTZacqXaXZyPixozbt_7; }
	inline UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59** get_address_of_rrqgLXltwFTZacqXaXZyPixozbt_7() { return &___rrqgLXltwFTZacqXaXZyPixozbt_7; }
	inline void set_rrqgLXltwFTZacqXaXZyPixozbt_7(UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* value)
	{
		___rrqgLXltwFTZacqXaXZyPixozbt_7 = value;
		Il2CppCodeGenWriteBarrier((&___rrqgLXltwFTZacqXaXZyPixozbt_7), value);
	}

	inline static int32_t get_offset_of_hasReceivedInput_8() { return static_cast<int32_t>(offsetof(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F, ___hasReceivedInput_8)); }
	inline bool get_hasReceivedInput_8() const { return ___hasReceivedInput_8; }
	inline bool* get_address_of_hasReceivedInput_8() { return &___hasReceivedInput_8; }
	inline void set_hasReceivedInput_8(bool value)
	{
		___hasReceivedInput_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERDATAUPDATER_TBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ACTIVECONTROLLERCHANGEDDELEGATE_T72EDF711948755D44D3A2576F2DCC7643EBB9910_H
#define ACTIVECONTROLLERCHANGEDDELEGATE_T72EDF711948755D44D3A2576F2DCC7643EBB9910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ActiveControllerChangedDelegate
struct  ActiveControllerChangedDelegate_t72EDF711948755D44D3A2576F2DCC7643EBB9910  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVECONTROLLERCHANGEDDELEGATE_T72EDF711948755D44D3A2576F2DCC7643EBB9910_H
#ifndef BRIDGEDCONTROLLER_T2B82D973366724BE432F83EC2B70668AB3D13DAC_H
#define BRIDGEDCONTROLLER_T2B82D973366724BE432F83EC2B70668AB3D13DAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.BridgedController
struct  BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC  : public BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6
{
public:
	// Rewired.Interfaces.IInputManagerJoystickPublic Rewired.BridgedController::sourceJoystick
	RuntimeObject* ___sourceJoystick_30;
	// Rewired.HardwareControllerMap_Game Rewired.BridgedController::gameHardwareMap
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___gameHardwareMap_31;
	// System.Guid Rewired.BridgedController::controllerTypeGuid
	Guid_t  ___controllerTypeGuid_32;
	// Rewired.Controller_Extension Rewired.BridgedController::controllerExtension
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___controllerExtension_33;
	// System.String Rewired.BridgedController::instanceName
	String_t* ___instanceName_34;
	// System.String Rewired.BridgedController::productName
	String_t* ___productName_35;
	// System.Boolean Rewired.BridgedController::isXInputDevice
	bool ___isXInputDevice_36;
	// System.Int32 Rewired.BridgedController::axisCount
	int32_t ___axisCount_37;
	// System.Int32 Rewired.BridgedController::buttonCount
	int32_t ___buttonCount_38;
	// System.Boolean[] Rewired.BridgedController::isButtonPressureSensitive
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___isButtonPressureSensitive_39;
	// Rewired.UnknownControllerHat[] Rewired.BridgedController::unknownControllerHats
	UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* ___unknownControllerHats_40;
	// Rewired.Platforms.Custom.CustomInputSource Rewired.BridgedController::customInputSource
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * ___customInputSource_41;

public:
	inline static int32_t get_offset_of_sourceJoystick_30() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___sourceJoystick_30)); }
	inline RuntimeObject* get_sourceJoystick_30() const { return ___sourceJoystick_30; }
	inline RuntimeObject** get_address_of_sourceJoystick_30() { return &___sourceJoystick_30; }
	inline void set_sourceJoystick_30(RuntimeObject* value)
	{
		___sourceJoystick_30 = value;
		Il2CppCodeGenWriteBarrier((&___sourceJoystick_30), value);
	}

	inline static int32_t get_offset_of_gameHardwareMap_31() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___gameHardwareMap_31)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_gameHardwareMap_31() const { return ___gameHardwareMap_31; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_gameHardwareMap_31() { return &___gameHardwareMap_31; }
	inline void set_gameHardwareMap_31(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___gameHardwareMap_31 = value;
		Il2CppCodeGenWriteBarrier((&___gameHardwareMap_31), value);
	}

	inline static int32_t get_offset_of_controllerTypeGuid_32() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___controllerTypeGuid_32)); }
	inline Guid_t  get_controllerTypeGuid_32() const { return ___controllerTypeGuid_32; }
	inline Guid_t * get_address_of_controllerTypeGuid_32() { return &___controllerTypeGuid_32; }
	inline void set_controllerTypeGuid_32(Guid_t  value)
	{
		___controllerTypeGuid_32 = value;
	}

	inline static int32_t get_offset_of_controllerExtension_33() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___controllerExtension_33)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_controllerExtension_33() const { return ___controllerExtension_33; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_controllerExtension_33() { return &___controllerExtension_33; }
	inline void set_controllerExtension_33(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___controllerExtension_33 = value;
		Il2CppCodeGenWriteBarrier((&___controllerExtension_33), value);
	}

	inline static int32_t get_offset_of_instanceName_34() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___instanceName_34)); }
	inline String_t* get_instanceName_34() const { return ___instanceName_34; }
	inline String_t** get_address_of_instanceName_34() { return &___instanceName_34; }
	inline void set_instanceName_34(String_t* value)
	{
		___instanceName_34 = value;
		Il2CppCodeGenWriteBarrier((&___instanceName_34), value);
	}

	inline static int32_t get_offset_of_productName_35() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___productName_35)); }
	inline String_t* get_productName_35() const { return ___productName_35; }
	inline String_t** get_address_of_productName_35() { return &___productName_35; }
	inline void set_productName_35(String_t* value)
	{
		___productName_35 = value;
		Il2CppCodeGenWriteBarrier((&___productName_35), value);
	}

	inline static int32_t get_offset_of_isXInputDevice_36() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___isXInputDevice_36)); }
	inline bool get_isXInputDevice_36() const { return ___isXInputDevice_36; }
	inline bool* get_address_of_isXInputDevice_36() { return &___isXInputDevice_36; }
	inline void set_isXInputDevice_36(bool value)
	{
		___isXInputDevice_36 = value;
	}

	inline static int32_t get_offset_of_axisCount_37() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___axisCount_37)); }
	inline int32_t get_axisCount_37() const { return ___axisCount_37; }
	inline int32_t* get_address_of_axisCount_37() { return &___axisCount_37; }
	inline void set_axisCount_37(int32_t value)
	{
		___axisCount_37 = value;
	}

	inline static int32_t get_offset_of_buttonCount_38() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___buttonCount_38)); }
	inline int32_t get_buttonCount_38() const { return ___buttonCount_38; }
	inline int32_t* get_address_of_buttonCount_38() { return &___buttonCount_38; }
	inline void set_buttonCount_38(int32_t value)
	{
		___buttonCount_38 = value;
	}

	inline static int32_t get_offset_of_isButtonPressureSensitive_39() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___isButtonPressureSensitive_39)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_isButtonPressureSensitive_39() const { return ___isButtonPressureSensitive_39; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_isButtonPressureSensitive_39() { return &___isButtonPressureSensitive_39; }
	inline void set_isButtonPressureSensitive_39(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___isButtonPressureSensitive_39 = value;
		Il2CppCodeGenWriteBarrier((&___isButtonPressureSensitive_39), value);
	}

	inline static int32_t get_offset_of_unknownControllerHats_40() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___unknownControllerHats_40)); }
	inline UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* get_unknownControllerHats_40() const { return ___unknownControllerHats_40; }
	inline UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59** get_address_of_unknownControllerHats_40() { return &___unknownControllerHats_40; }
	inline void set_unknownControllerHats_40(UnknownControllerHatU5BU5D_tC8E16F55FA601C66DFB576102A75F175E9954B59* value)
	{
		___unknownControllerHats_40 = value;
		Il2CppCodeGenWriteBarrier((&___unknownControllerHats_40), value);
	}

	inline static int32_t get_offset_of_customInputSource_41() { return static_cast<int32_t>(offsetof(BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC, ___customInputSource_41)); }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * get_customInputSource_41() const { return ___customInputSource_41; }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 ** get_address_of_customInputSource_41() { return &___customInputSource_41; }
	inline void set_customInputSource_41(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * value)
	{
		___customInputSource_41 = value;
		Il2CppCodeGenWriteBarrier((&___customInputSource_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIDGEDCONTROLLER_T2B82D973366724BE432F83EC2B70668AB3D13DAC_H
#ifndef PLAYERACTIVECONTROLLERCHANGEDDELEGATE_TC76E434667D24815A312195FD2A4FBD7A78AF23C_H
#define PLAYERACTIVECONTROLLERCHANGEDDELEGATE_TC76E434667D24815A312195FD2A4FBD7A78AF23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerActiveControllerChangedDelegate
struct  PlayerActiveControllerChangedDelegate_tC76E434667D24815A312195FD2A4FBD7A78AF23C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERACTIVECONTROLLERCHANGEDDELEGATE_TC76E434667D24815A312195FD2A4FBD7A78AF23C_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#define USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore
struct  UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#ifndef INITIALIZER_TDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355_H
#define INITIALIZER_TDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Initializer
struct  Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Rewired.Initializer::_inputManagerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputManagerPrefab_4;
	// System.Boolean Rewired.Initializer::_destroySelf
	bool ____destroySelf_5;

public:
	inline static int32_t get_offset_of__inputManagerPrefab_4() { return static_cast<int32_t>(offsetof(Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355, ____inputManagerPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputManagerPrefab_4() const { return ____inputManagerPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputManagerPrefab_4() { return &____inputManagerPrefab_4; }
	inline void set__inputManagerPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputManagerPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputManagerPrefab_4), value);
	}

	inline static int32_t get_offset_of__destroySelf_5() { return static_cast<int32_t>(offsetof(Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355, ____destroySelf_5)); }
	inline bool get__destroySelf_5() const { return ____destroySelf_5; }
	inline bool* get_address_of__destroySelf_5() { return &____destroySelf_5; }
	inline void set__destroySelf_5(bool value)
	{
		____destroySelf_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZER_TDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2), -1, sizeof(UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4400[3] = 
{
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2::get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1(),
	UbGBLdiYFAdtrLoihmYVEnHzVpJE_t1027EFAFA431F4F289EB64C1459B6EFE09E2E6B2_StaticFields::get_offset_of_mKnMJjALYPDqjAoSzooTqyMcgMHD_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4401[3] = 
{
	fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD::get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_0(),
	fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	fODgYqoFcBZNNDWDFRmSBJTbkJO_t82965906495BD22FE429922D8B5C1708B4FC56CD::get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[4] = 
{
	clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84::get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_0(),
	clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84::get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(),
	clJVBdesGVlJeiKXHbOxrlHLKNF_tF9CF56353008DD0C7511A59F28EAD3D2C5A20D84::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4403[4] = 
{
	wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978::get_offset_of_LLGuAxNDTisGPebZTuecPvDemqb_0(),
	wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978::get_offset_of_CzbgHihIrBdYfLmPHIGillBPleew_1(),
	wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2(),
	wiOcuiQvjFvvtmhlzhwAiBeZcGs_t03C04FD24E3FDEA30F9B25652B32F710DEB6E978::get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF), -1, sizeof(JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4404[3] = 
{
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF::get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1(),
	JZZyLMAuMxwZaOpbKUYlhMNhVLy_t3A053D8096061B18491AF1BD5B9FDC4FC0E03CBF_StaticFields::get_offset_of_dSKRKwvosVaEirauaqhdBfEDQNT_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[3] = 
{
	diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498::get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0(),
	diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	diPqZQLByUtBcMoxlbfXFKjiNDU_tD1B419D1C19779665440AC3FF722EA42C8FFB498::get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[4] = 
{
	pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB::get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_0(),
	pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB::get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(),
	pAOjJqrifZlhrwZoIIsNVLCBaIbb_tC52AF3A0BDAA9C70969E59A06E499F2BF56E85EB::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4407[4] = 
{
	czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815::get_offset_of_CFJiwEBvJujHtEMtAkarYxTAFSAv_0(),
	czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815::get_offset_of_mhgrIaReWtlVoWezNqdFXOhDOQn_1(),
	czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2(),
	czMGTlBHUPsFdqAataIucntrKwXC_t536BBEC96A08677A531AD75AD503F560DA15F815::get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4408[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4410[7] = 
{
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	wCqhaeyBzcaEcqBSqsBYTwBpDCR_tFCB33AC3776188C472BFF89C4B25CB136AE668F1::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[5] = 
{
	yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	yYVtdUjnHnGjMCrVxdyOghAgMdIL_tF0B89C8B7D9373EB175FF7900C87236B0C516B38::get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4412[7] = 
{
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	GqzUMHsWzoSwvwtJZGoFrjJkNGD_t630ECFA5D55F2BD2671FF62205DFA0ACB919B7CB::get_offset_of_lEJfSplllLlGeeWbNJFrzDmIVEp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4413[7] = 
{
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	WYirmKfzuREVnYwAXJqNFDfjCSh_t9C0F7DD2D55C861797B90A228E757A51376E5178::get_offset_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4414[5] = 
{
	iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	iNNDxEbNNpqxDAeLihdkVRUCGneR_t810B9ABF109031F95CA3D804FFB69F1441224C9C::get_offset_of_UTKmqfOTQTBrCjBgXOMUQjMaWpB_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[7] = 
{
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	OuUQqZsLRLeIZgNMAKqFhmswQDh_t57EFAA2C72C32FE458D1AC86FA16D39ACF1DF3D2::get_offset_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4416[7] = 
{
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_jBAjmtMSOMsCuqnYPvoeEjlKSHf_4(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_qKrcCDEIdUcccmEYnTYsrCxkrWeN_5(),
	PTCdsGDMlGexbXtrvQjvLUnCyToQ_t27B31DE7E64AAD2D0E5B3EE47D6CF355C58CDBE5::get_offset_of_fUxETuHauDmUaHCRcyXkQrxiaqoY_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4417[12] = 
{
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_jIRMFIFwtWwANmtBUtLpLFbZRmp_8(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_kIkMphgGrIErrMhJQsfdwjJIwHr_9(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_10(),
	yXwjnzeFyIgucBrhXxeGtZGoKtTG_tB9A14A9CC7B45ED64C87050EF2DC57769120B4C1::get_offset_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4418[14] = 
{
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_DdLBFDSCGIcxnggOHOEzIwvbXCWO_4(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_SMlAuMHVLnsuvgVfNQDkQwkRiRZ_5(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_PIwpZHvawTjYekPxGTwATVAdBDe_8(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_cIipAhMAgwhMwgeMkwzVkGWsujb_9(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_QCpLFkKDQjUOaJYzIOvwDlenNWW_10(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_JeOdrvOPNPfsNJaxdMekBKDgSoGJ_11(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_12(),
	kgEYdNbBncYBItzCeHfZPEZLnpn_tC87D1CAEA525C62A84EE1CB6A65FF5E3E40C2AF5::get_offset_of_PalmDKfwodOEEgPtuWClpZscTYK_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4419[10] = 
{
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_sLDDgBamhzadZbhcjNSKCejHvMJN_6(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_ujkBcIBmJgeqPhNLwLuqtFbBzmU_7(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_vGCbgmFWTYIrMYKvjGcCDOiRkSiy_8(),
	vtmWSvFrgdaypojpqUMZJYvnxRQ_t55B1216A392D60366B63C317F586BA6F1EC99217::get_offset_of_mvDwtqBgtvgsghrHMOXYpHOdbsaR_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4420[14] = 
{
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_chTbMAJBwugvHvDEnzgDSlLnSqg_4(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_WCLOpuzlAPeqPEgTxJoAeJojJdS_5(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_sNnGTIgDJyPxPMNHJHvNuOFurVfh_8(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_srSvPxedjBeocvpioBWPvuYgtjx_9(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_hjgsuFLRTScWkajVxAiiABppxTid_10(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_11(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_fpbTLzCSTwYcOnnFJAuVUuFuOIP_12(),
	auGBnUNIDodGwvsjZNRLGQbcmHV_t4E73DEB7A80ECAA7149C2F24855BEB8946309DF3::get_offset_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[14] = 
{
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_tfUFVQgUsJpcyOvrTozhGNyqUAUN_4(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_HpRmBTNMOJhjiRwCqWZDZFyZBKWB_5(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_PfBtErLFmgIwzViqjbjnyRbCRuw_6(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_ZyLlrOVJdDTGzRxMxdMMRTlifBO_7(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_yCkXaxzjTuzfcCDVbdkzceRrNPLd_8(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_vKPsghCwZcBCtuyYRMmUGbOcTQA_9(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_pfyTwvHZiBuGvhpfWDOaFzGNVkP_10(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_VVmghUeCqrsnjVcDnqzHMLYfXNzc_11(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_nqIBgQJbStWIfQxhIUPqhBicleF_12(),
	fWMdAlFLeTUSAJeRpFYsvfrpQWO_t2F445DBC5EB323B6821798AE5650C9D527F966C1::get_offset_of_dKXJPdhNuyMkyKbBRgczOSzhbqL_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4422[9] = 
{
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_ZpWyjemSTdQzezCOQdYNJqQbMR_6(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_znmLWYURolWNezJtEPjUdZnVhOa_7(),
	UheVvDxsmRkHlWqjPKqAbKYqFTg_t15AB6E575BD79808D061099240DC6652ACE11C39::get_offset_of_kgdXDOuwBknTdVGsGtzXGYuZhje_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4423[9] = 
{
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_PwepnsvajksKbyDHCUanZMkkmVZ_6(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_hJbwrdltDmfXNminGOuYkoMgDSs_7(),
	OxBKaAyEYnNOoKceLukyItcddnW_tA6BC53023F1684464BD9FA81975E720EEE926306::get_offset_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4424[8] = 
{
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_4(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_srJAZFfULohezSrPvmIPTKgbuwb_5(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_HkwuAIrQONIZFoeUiNNrtBfQtDc_6(),
	TvJVOSWGIbBiPipuvmqYpjwHJlA_t84B346FF8D5D99D1875AE108B48587F068323A1C::get_offset_of_zIMdOktLSEteWzOxVhfbmfwLBuKh_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4425[1] = 
{
	kwjCFrMFqcwBpiPxRDvjqsTVGzZ_tC0E7C2207DAC84303C2E8F1DB4A56D04C4732205::get_offset_of_GHSFrqFOciasZnFYOarJvwFvvpiw_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4426[3] = 
{
	HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D::get_offset_of_pwladNKVLiDfTkIGfUdjCuCDhgVa_0(),
	HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_1(),
	HmXVLVNxxnGUmjhaGAaEcIjGJeFU_t51A1055BA2C1C341961B7F33A1394814B6D6F82D::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1), -1, sizeof(Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4430[4] = 
{
	0,
	Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields::get_offset_of___screenLog_1(),
	Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields::get_offset_of__guiText_2(),
	Logger_tCB983C73CC4B5291036A949C72056086AD9DD9A1_StaticFields::get_offset_of__logToScreen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (FNdhWuxQpyEBjfNELfEbFQFnSFne_tAC89E25A0A4044DEC44E232C9198937DFA1A8A76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (GCProfiler_tB3D8D64F6EB5F8F4B950FC3EA6F2289383E69454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (ActiveControllerChangedDelegate_t72EDF711948755D44D3A2576F2DCC7643EBB9910), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (PlayerActiveControllerChangedDelegate_tC76E434667D24815A312195FD2A4FBD7A78AF23C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4435[6] = 
{
	ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4436[10] = 
{
	ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4437[133] = 
{
	KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (MouseInputElement_t90CF67800AE68523F4A64FE82F58DE4AB22A66F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4438[11] = 
{
	MouseInputElement_t90CF67800AE68523F4A64FE82F58DE4AB22A66F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4439[5] = 
{
	MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4440[4] = 
{
	MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4441[3] = 
{
	MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4442[3] = 
{
	InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (AxisType_tD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4443[4] = 
{
	AxisType_tD3FAE3D4E29E5ED21ABF674C95A67BCCB0B09AED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4444[3] = 
{
	Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4445[4] = 
{
	AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4446[3] = 
{
	AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4447[5] = 
{
	ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4448[4] = 
{
	ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4449[3] = 
{
	CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4450[3] = 
{
	DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4451[3] = 
{
	AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4452[5] = 
{
	ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4453[4] = 
{
	UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4454[48] = 
{
	InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4455[4] = 
{
	AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4456[34] = 
{
	JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4457[20] = 
{
	InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4458[41] = 
{
	InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (ControlDeviceType_t3E38FA7371FC471A58C41C5A7529D7A0CFDB810B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4459[15] = 
{
	ControlDeviceType_t3E38FA7371FC471A58C41C5A7529D7A0CFDB810B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (ControllerSubType_t05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4460[4] = 
{
	ControllerSubType_t05459A17BF28B6ECCB1A5F24AFD17E8E22B311A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4461[4] = 
{
	BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4462[5] = 
{
	ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (MultiBoolValue_tEEA4D6B314C3C9411952A6844294F5213E0B80D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4463[4] = 
{
	MultiBoolValue_tEEA4D6B314C3C9411952A6844294F5213E0B80D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4464[2] = 
{
	Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355::get_offset_of__inputManagerPrefab_4(),
	Initializer_tDC9F4E89EA7DAEEDE73732AECAD445B88C3C5355::get_offset_of__destroySelf_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[30] = 
{
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_isMock_0(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_inputManagerSource_1(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_inputSource_2(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_deviceType_3(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hardwareIdentifier_4(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hardwareAxisCount_5(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hardwareButtonCount_6(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hardwareHatCount_7(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_productName_8(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_pidVid_9(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_deviceGuid_10(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_productId_11(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_bluetoothDeviceName_12(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_isBluetoothDevice_13(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_supportsVoice_14(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_supportsVibration_15(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_xInputSubType_16(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_manufacturer_17(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_serialNumber_18(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_vendorId_19(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_version_20(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_systemDeviceName_21(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_isSDL2Gamepad_22(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_webGL_webBrowserType_23(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_webGL_osType_24(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_webGL_mappingType_25(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_webGL_webBrowserVersionSplit_26(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_webGL_osVersionSplit_27(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_hw_localVibrationMotorCount_28(),
	BridgedControllerHWInfo_t804A11568AEF5B3FA5443C76FAE190657808FAF6::get_offset_of_definitionMatchTag_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4494[12] = 
{
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_sourceJoystick_30(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_gameHardwareMap_31(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_controllerTypeGuid_32(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_controllerExtension_33(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_instanceName_34(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_productName_35(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_isXInputDevice_36(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_axisCount_37(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_buttonCount_38(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_isButtonPressureSensitive_39(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_unknownControllerHats_40(),
	BridgedController_t2B82D973366724BE432F83EC2B70668AB3D13DAC::get_offset_of_customInputSource_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4495[10] = 
{
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_IWhEyCFiNqzvkyFxBqcPsCDkGwE_1(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_YDlaIMRNnbaSMyDyufrzqGbKOHj_2(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_reNFYQGuxlDRPhTirIkZzgdrffbW_4(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_BZCZtCnEaEEjShgHQslaWPjqdiI_5(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_CRkueCzBdihMDjIglcWKKSWZfkB_6(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_7(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8(),
	JEWBEfdgfsJnJIwKKvidiOYuBRq_t37B06BC2A12683C511FA4EF47D83411B90DF99E6::get_offset_of_cEbPZtIbxEvXmcDiLibfUegVyao_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[9] = 
{
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_source_0(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_axisCount_1(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_buttonCount_2(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_axisValues_3(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_buttonValues_4(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_buttonPressureValues_5(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_axisHasBeenPressedOSXLinux_6(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_rrqgLXltwFTZacqXaXZyPixozbt_7(),
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F::get_offset_of_hasReceivedInput_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (CustomObfuscation_t5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4497[1] = 
{
	CustomObfuscation_t5242E1FB0FD57B7A77D62B9DE17D4EB524323EB8::get_offset_of_rename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4498[2] = 
{
	CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859::get_offset_of_renamePubIntMembers_0(),
	CustomClassObfuscation_tBD4EDA8CB5AF6EF65BF7ECD011E185B7FA693859::get_offset_of_renamePrivateMembers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (UpdateControllerInfoEventArgs_t2942188A682F1A914A0891D02CCEC456FC39985C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4499[1] = 
{
	UpdateControllerInfoEventArgs_t2942188A682F1A914A0891D02CCEC456FC39985C::get_offset_of_sourceJoystick_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
