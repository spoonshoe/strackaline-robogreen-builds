﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.AxisCalibrationData[]
struct AxisCalibrationDataU5BU5D_tB487D04860D203BA3498E0992EEE34BCD1E02F3B;
// Rewired.AxisRange[]
struct AxisRangeU5BU5D_t56B8222EEAB047F19E70E2F90E6CD0483116A157;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.ControllerElementIdentifier[]
struct ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.ControllerMapLayoutManager/StartingSettings
struct StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1;
// Rewired.ControllerMapSaveData
struct ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F;
// Rewired.ControllerMapWithAxes
struct ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36;
// Rewired.ControllerSetSelector
struct ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD;
// Rewired.CustomController
struct CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A;
// Rewired.CustomControllerMap
struct CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A;
// Rewired.CustomControllerMapSaveData[]
struct CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52;
// Rewired.Data.ControllerSetSelector_Editor
struct ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8;
// Rewired.Data.Mapping.ControllerMap_Editor
struct ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7;
// Rewired.Data.Mapping.HardwareAxisInfo[]
struct HardwareAxisInfoU5BU5D_tAA6150D1009F709292A37DD9D80B69A8070DBF41;
// Rewired.Data.Mapping.HardwareButtonInfo[]
struct HardwareButtonInfoU5BU5D_t15045FEBBCBE26B33AE903031072C697D11140A5;
// Rewired.Data.Mapping.HardwareJoystickMap/CompoundElement[]
struct CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform
struct Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789;
// Rewired.ElementAssignmentInfo
struct ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF;
// Rewired.IControllerElementTarget
struct IControllerElementTarget_tBECEEA8A0AF0C16A3D41416E35B98CFD09422658;
// Rewired.InputBehavior[]
struct InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A;
// Rewired.InputMapper
struct InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB;
// Rewired.InputMapper/Context
struct Context_tDDC4D5CD175FF95675F41961F66B1383287803A1;
// Rewired.InputMapper/Options
struct Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939;
// Rewired.InputMapper/WLaANgjWTacepVAvNGdKisMgYMnI
struct WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76;
// Rewired.InputMapper/WLaANgjWTacepVAvNGdKisMgYMnI/cgpUlBrDzLixoMOCLlievvtyCpu
struct cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C;
// Rewired.Joystick
struct Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39;
// Rewired.JoystickMap
struct JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24;
// Rewired.JoystickMapSaveData[]
struct JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63;
// Rewired.JoystickType[]
struct JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA;
// Rewired.KeyboardMapSaveData[]
struct KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95;
// Rewired.MouseMapSaveData[]
struct MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.Player/ControllerHelper
struct ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC;
// Rewired.Player/ControllerHelper/ConflictCheckingHelper
struct ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1;
// Rewired.Player/ControllerHelper/GyRicTMqpYlIDAFpGCLnPVhblMN
struct GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377;
// Rewired.Player/ControllerHelper/MapHelper
struct MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92;
// Rewired.Player/ControllerHelper/PollingHelper
struct PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24;
// Rewired.Player/ControllerHelper/ZgoffaakscXwSecdxgDVDCqXYPjo
struct ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.ControllerElementIdentifier>
struct ADictionary_2_t46071D66C4378848A3237FDEB0CA63F81171558A;
// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap>
struct AList_1_t3CE454271698B03A6EA53DA068272454E1A22108;
// Rewired.Utils.SafeAction`1<Rewired.ControllerAssignmentChangedEventArgs>
struct SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90;
// Rewired.Utils.TempListPool/TList`1<Rewired.ActionElementMap>
struct TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.InputMapper/ConflictResponse>
struct Action_1_tC9795E40AC09CBEE286EFAB4D8DBDA3177E8FB09;
// System.Action`1<System.Exception>
struct Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Rewired.InputMapper/brixqxKPyakMwXgqKReyPWdHwsQ,Rewired.Utils.SafeDelegate>
struct Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568;
// System.Collections.Generic.Dictionary`2<System.String,Rewired.Utils.SafeDelegate>
struct Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677;
// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap>
struct IEnumerator_1_t62E0BA06CEF38BC2D5839771D2A23781A0945E7C;
// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo>
struct IEnumerator_1_tB5F8E3B1AA2E4BF2597F179C159DF6EFBC7DAE69;
// System.Collections.Generic.IList`1<Rewired.ActionElementMap>
struct IList_1_t1C1B8D350299A24510230CF5E934B02259CD3E4C;
// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier>
struct IList_1_t6120EA8B34A2D2D09685DAC6C13DF0D247609195;
// System.Collections.Generic.IList`1<Rewired.ElementAssignmentConflictInfo>
struct IList_1_t0B80B8493D14418828817C1FCDF652ACE639B278;
// System.Collections.Generic.List`1<Rewired.ActionElementMap>
struct List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94;
// System.Collections.Generic.List`1<Rewired.ControllerMapLayoutManager/Rule>
struct List_1_t12B1E729214DA1F7DB2084EB2FA37C04C7F509ED;
// System.Collections.Generic.List`1<Rewired.ControllerMapLayoutManager/RuleSet>
struct List_1_t8B0F77E2955203535F6669AEABC756EEE15692AE;
// System.Collections.Generic.List`1<Rewired.ControllerPollingInfo>
struct List_1_tEB943CFDA4A58C4532C0C4495773F0E6DB543276;
// System.Collections.Generic.List`1<Rewired.Data.ControllerMapEnabler_Rule_Editor>
struct List_1_t8CD0FD6A5FFD5DA598CCADBF633321E480725FB3;
// System.Collections.Generic.List`1<Rewired.Data.ControllerMapLayoutManager_Rule_Editor>
struct List_1_tEAF03A5C372A25CD0CB3FF3D462E0372D0C2CD9F;
// System.Collections.Generic.List`1<Rewired.Player>
struct List_1_t173A087C7487E3961C3BFB37EEE5EB31090BF92B;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_tADAFF4E53554D69184CDFB081796750626C37449;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap>
struct ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// jufVWXOncnQzwwsvxneDefLrtLg[]
struct jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09;
// lUbzJtkApcIkohMrHhJAmcDImRRR
struct lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE;
// rIcvLcWCuQghgaaiofvKjSvndJX
struct rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef QPVVZFYSEMBDHMYLDCLREWBYIKX_TFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_H
#define QPVVZFYSEMBDHMYLDCLREWBYIKX_TFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QpvvZfySeMbDHmyLdcLREwbyiKx
struct  QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C  : public RuntimeObject
{
public:

public:
};

struct QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields
{
public:
	// System.Int32 QpvvZfySeMbDHmyLdcLREwbyiKx::ypmsdOiwBhQCBkOhnBGNddpWwGuj
	int32_t ___ypmsdOiwBhQCBkOhnBGNddpWwGuj_3;
	// System.Int32 QpvvZfySeMbDHmyLdcLREwbyiKx::qvhEmSqxwBIRRYBLviUpqJAtSAd
	int32_t ___qvhEmSqxwBIRRYBLviUpqJAtSAd_4;
	// System.Int32 QpvvZfySeMbDHmyLdcLREwbyiKx::hTwPcflWVMjpEBOLCnrfSQYNgFWk
	int32_t ___hTwPcflWVMjpEBOLCnrfSQYNgFWk_5;
	// System.String QpvvZfySeMbDHmyLdcLREwbyiKx::TOvnMXGXtktHXIImNpDtrdOjnmr
	String_t* ___TOvnMXGXtktHXIImNpDtrdOjnmr_6;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> QpvvZfySeMbDHmyLdcLREwbyiKx::KWcpfLqiYUbpVOdTEffGPEkMXgq
	List_1_tADAFF4E53554D69184CDFB081796750626C37449 * ___KWcpfLqiYUbpVOdTEffGPEkMXgq_7;

public:
	inline static int32_t get_offset_of_ypmsdOiwBhQCBkOhnBGNddpWwGuj_3() { return static_cast<int32_t>(offsetof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields, ___ypmsdOiwBhQCBkOhnBGNddpWwGuj_3)); }
	inline int32_t get_ypmsdOiwBhQCBkOhnBGNddpWwGuj_3() const { return ___ypmsdOiwBhQCBkOhnBGNddpWwGuj_3; }
	inline int32_t* get_address_of_ypmsdOiwBhQCBkOhnBGNddpWwGuj_3() { return &___ypmsdOiwBhQCBkOhnBGNddpWwGuj_3; }
	inline void set_ypmsdOiwBhQCBkOhnBGNddpWwGuj_3(int32_t value)
	{
		___ypmsdOiwBhQCBkOhnBGNddpWwGuj_3 = value;
	}

	inline static int32_t get_offset_of_qvhEmSqxwBIRRYBLviUpqJAtSAd_4() { return static_cast<int32_t>(offsetof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields, ___qvhEmSqxwBIRRYBLviUpqJAtSAd_4)); }
	inline int32_t get_qvhEmSqxwBIRRYBLviUpqJAtSAd_4() const { return ___qvhEmSqxwBIRRYBLviUpqJAtSAd_4; }
	inline int32_t* get_address_of_qvhEmSqxwBIRRYBLviUpqJAtSAd_4() { return &___qvhEmSqxwBIRRYBLviUpqJAtSAd_4; }
	inline void set_qvhEmSqxwBIRRYBLviUpqJAtSAd_4(int32_t value)
	{
		___qvhEmSqxwBIRRYBLviUpqJAtSAd_4 = value;
	}

	inline static int32_t get_offset_of_hTwPcflWVMjpEBOLCnrfSQYNgFWk_5() { return static_cast<int32_t>(offsetof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields, ___hTwPcflWVMjpEBOLCnrfSQYNgFWk_5)); }
	inline int32_t get_hTwPcflWVMjpEBOLCnrfSQYNgFWk_5() const { return ___hTwPcflWVMjpEBOLCnrfSQYNgFWk_5; }
	inline int32_t* get_address_of_hTwPcflWVMjpEBOLCnrfSQYNgFWk_5() { return &___hTwPcflWVMjpEBOLCnrfSQYNgFWk_5; }
	inline void set_hTwPcflWVMjpEBOLCnrfSQYNgFWk_5(int32_t value)
	{
		___hTwPcflWVMjpEBOLCnrfSQYNgFWk_5 = value;
	}

	inline static int32_t get_offset_of_TOvnMXGXtktHXIImNpDtrdOjnmr_6() { return static_cast<int32_t>(offsetof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields, ___TOvnMXGXtktHXIImNpDtrdOjnmr_6)); }
	inline String_t* get_TOvnMXGXtktHXIImNpDtrdOjnmr_6() const { return ___TOvnMXGXtktHXIImNpDtrdOjnmr_6; }
	inline String_t** get_address_of_TOvnMXGXtktHXIImNpDtrdOjnmr_6() { return &___TOvnMXGXtktHXIImNpDtrdOjnmr_6; }
	inline void set_TOvnMXGXtktHXIImNpDtrdOjnmr_6(String_t* value)
	{
		___TOvnMXGXtktHXIImNpDtrdOjnmr_6 = value;
		Il2CppCodeGenWriteBarrier((&___TOvnMXGXtktHXIImNpDtrdOjnmr_6), value);
	}

	inline static int32_t get_offset_of_KWcpfLqiYUbpVOdTEffGPEkMXgq_7() { return static_cast<int32_t>(offsetof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields, ___KWcpfLqiYUbpVOdTEffGPEkMXgq_7)); }
	inline List_1_tADAFF4E53554D69184CDFB081796750626C37449 * get_KWcpfLqiYUbpVOdTEffGPEkMXgq_7() const { return ___KWcpfLqiYUbpVOdTEffGPEkMXgq_7; }
	inline List_1_tADAFF4E53554D69184CDFB081796750626C37449 ** get_address_of_KWcpfLqiYUbpVOdTEffGPEkMXgq_7() { return &___KWcpfLqiYUbpVOdTEffGPEkMXgq_7; }
	inline void set_KWcpfLqiYUbpVOdTEffGPEkMXgq_7(List_1_tADAFF4E53554D69184CDFB081796750626C37449 * value)
	{
		___KWcpfLqiYUbpVOdTEffGPEkMXgq_7 = value;
		Il2CppCodeGenWriteBarrier((&___KWcpfLqiYUbpVOdTEffGPEkMXgq_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QPVVZFYSEMBDHMYLDCLREWBYIKX_TFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_H
#ifndef OZSELFXYUACLZZAVDGNUGAELVQKO_T2F08A6E3E84CC6A2DA07B7ADC6F5841E97ECFB87_H
#define OZSELFXYUACLZZAVDGNUGAELVQKO_T2F08A6E3E84CC6A2DA07B7ADC6F5841E97ECFB87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QpvvZfySeMbDHmyLdcLREwbyiKx_ozSeLFXyuAcLzzaVdgnuGAElVQkO
struct  ozSeLFXyuAcLzzaVdgnuGAElVQkO_t2F08A6E3E84CC6A2DA07B7ADC6F5841E97ECFB87  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OZSELFXYUACLZZAVDGNUGAELVQKO_T2F08A6E3E84CC6A2DA07B7ADC6F5841E97ECFB87_H
#ifndef ONFBCAKOPIVOHUSERJPQCTPYTQE_TF6079688B3422631A334CDB3AF58498916421407_H
#define ONFBCAKOPIVOHUSERJPQCTPYTQE_TF6079688B3422631A334CDB3AF58498916421407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QpvvZfySeMbDHmyLdcLREwbyiKx_ozSeLFXyuAcLzzaVdgnuGAElVQkO_onFbCAKOPIvohuSERjpQctpytQE
struct  onFbCAKOPIvohuSERjpQctpytQE_tF6079688B3422631A334CDB3AF58498916421407  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFBCAKOPIVOHUSERJPQCTPYTQE_TF6079688B3422631A334CDB3AF58498916421407_H
#ifndef CLHHMWWKZZDATIOPFXVSPZWKUEX_TF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58_H
#define CLHHMWWKZZDATIOPFXVSPZWKUEX_TF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx
struct  CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Collections.Generic.IList`1<Rewired.ActionElementMap> Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::ByOqNpDZwJrvfOqQiqTmaleCTNf
	RuntimeObject* ___ByOqNpDZwJrvfOqQiqTmaleCTNf_8;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::NgTrnlpDWwovXpOfghixDogoFqyC
	int32_t ___NgTrnlpDWwovXpOfghixDogoFqyC_9;
	// System.Int32 Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::bbylexxHkiPVNjmmyFKFxUlOenA
	int32_t ___bbylexxHkiPVNjmmyFKFxUlOenA_10;
	// Rewired.ActionElementMap Rewired.ControllerMap_CLhHmwWkZzdaTIopfXvSpZWkuEx::pYinzYhLeyxGzWrExwLAEtbjDxC
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___pYinzYhLeyxGzWrExwLAEtbjDxC_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_ByOqNpDZwJrvfOqQiqTmaleCTNf_8() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___ByOqNpDZwJrvfOqQiqTmaleCTNf_8)); }
	inline RuntimeObject* get_ByOqNpDZwJrvfOqQiqTmaleCTNf_8() const { return ___ByOqNpDZwJrvfOqQiqTmaleCTNf_8; }
	inline RuntimeObject** get_address_of_ByOqNpDZwJrvfOqQiqTmaleCTNf_8() { return &___ByOqNpDZwJrvfOqQiqTmaleCTNf_8; }
	inline void set_ByOqNpDZwJrvfOqQiqTmaleCTNf_8(RuntimeObject* value)
	{
		___ByOqNpDZwJrvfOqQiqTmaleCTNf_8 = value;
		Il2CppCodeGenWriteBarrier((&___ByOqNpDZwJrvfOqQiqTmaleCTNf_8), value);
	}

	inline static int32_t get_offset_of_NgTrnlpDWwovXpOfghixDogoFqyC_9() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___NgTrnlpDWwovXpOfghixDogoFqyC_9)); }
	inline int32_t get_NgTrnlpDWwovXpOfghixDogoFqyC_9() const { return ___NgTrnlpDWwovXpOfghixDogoFqyC_9; }
	inline int32_t* get_address_of_NgTrnlpDWwovXpOfghixDogoFqyC_9() { return &___NgTrnlpDWwovXpOfghixDogoFqyC_9; }
	inline void set_NgTrnlpDWwovXpOfghixDogoFqyC_9(int32_t value)
	{
		___NgTrnlpDWwovXpOfghixDogoFqyC_9 = value;
	}

	inline static int32_t get_offset_of_bbylexxHkiPVNjmmyFKFxUlOenA_10() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___bbylexxHkiPVNjmmyFKFxUlOenA_10)); }
	inline int32_t get_bbylexxHkiPVNjmmyFKFxUlOenA_10() const { return ___bbylexxHkiPVNjmmyFKFxUlOenA_10; }
	inline int32_t* get_address_of_bbylexxHkiPVNjmmyFKFxUlOenA_10() { return &___bbylexxHkiPVNjmmyFKFxUlOenA_10; }
	inline void set_bbylexxHkiPVNjmmyFKFxUlOenA_10(int32_t value)
	{
		___bbylexxHkiPVNjmmyFKFxUlOenA_10 = value;
	}

	inline static int32_t get_offset_of_pYinzYhLeyxGzWrExwLAEtbjDxC_11() { return static_cast<int32_t>(offsetof(CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58, ___pYinzYhLeyxGzWrExwLAEtbjDxC_11)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_pYinzYhLeyxGzWrExwLAEtbjDxC_11() const { return ___pYinzYhLeyxGzWrExwLAEtbjDxC_11; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_pYinzYhLeyxGzWrExwLAEtbjDxC_11() { return &___pYinzYhLeyxGzWrExwLAEtbjDxC_11; }
	inline void set_pYinzYhLeyxGzWrExwLAEtbjDxC_11(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___pYinzYhLeyxGzWrExwLAEtbjDxC_11 = value;
		Il2CppCodeGenWriteBarrier((&___pYinzYhLeyxGzWrExwLAEtbjDxC_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLHHMWWKZZDATIOPFXVSPZWKUEX_TF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58_H
#ifndef GVGOZUJCBXVKFYWQIHPGBEZUIAX_TB87ACCFF8DB0400170F69E1F70B89DC97D053DD5_H
#define GVGOZUJCBXVKFYWQIHPGBEZUIAX_TB87ACCFF8DB0400170F69E1F70B89DC97D053DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX
struct  GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ActionElementMap Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::LHdGnHCkoRIrqjLwZdOwLxKUpPZ
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.ControllerMap_GvgOZuJcbXvKfyWQihPgbEZuIAX::xyrBNgPkbFouqDLYWRndyzUOyMa
	RuntimeObject* ___xyrBNgPkbFouqDLYWRndyzUOyMa_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() const { return ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() { return &___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8; }
	inline void set_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8 = value;
		Il2CppCodeGenWriteBarrier((&___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8), value);
	}

	inline static int32_t get_offset_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9() { return static_cast<int32_t>(offsetof(GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5, ___xyrBNgPkbFouqDLYWRndyzUOyMa_9)); }
	inline RuntimeObject* get_xyrBNgPkbFouqDLYWRndyzUOyMa_9() const { return ___xyrBNgPkbFouqDLYWRndyzUOyMa_9; }
	inline RuntimeObject** get_address_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9() { return &___xyrBNgPkbFouqDLYWRndyzUOyMa_9; }
	inline void set_xyrBNgPkbFouqDLYWRndyzUOyMa_9(RuntimeObject* value)
	{
		___xyrBNgPkbFouqDLYWRndyzUOyMa_9 = value;
		Il2CppCodeGenWriteBarrier((&___xyrBNgPkbFouqDLYWRndyzUOyMa_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVGOZUJCBXVKFYWQIHPGBEZUIAX_TB87ACCFF8DB0400170F69E1F70B89DC97D053DD5_H
#ifndef KWECICOTRMQFPYQFTYPGJEAOPZC_T3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_H
#define KWECICOTRMQFPYQFTYPGJEAOPZC_T3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_kWecicOtRMQfpYQfTypgJeaOPZC
struct  kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46  : public RuntimeObject
{
public:

public:
};

struct kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_StaticFields
{
public:
	// Rewired.ControllerMap_kWecicOtRMQfpYQfTypgJeaOPZC Rewired.ControllerMap_kWecicOtRMQfpYQfTypgJeaOPZC::gwdVBQEakPuszKrkhFaRVdsPWKC
	kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46 * ___gwdVBQEakPuszKrkhFaRVdsPWKC_0;

public:
	inline static int32_t get_offset_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0() { return static_cast<int32_t>(offsetof(kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_StaticFields, ___gwdVBQEakPuszKrkhFaRVdsPWKC_0)); }
	inline kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46 * get_gwdVBQEakPuszKrkhFaRVdsPWKC_0() const { return ___gwdVBQEakPuszKrkhFaRVdsPWKC_0; }
	inline kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46 ** get_address_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0() { return &___gwdVBQEakPuszKrkhFaRVdsPWKC_0; }
	inline void set_gwdVBQEakPuszKrkhFaRVdsPWKC_0(kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46 * value)
	{
		___gwdVBQEakPuszKrkhFaRVdsPWKC_0 = value;
		Il2CppCodeGenWriteBarrier((&___gwdVBQEakPuszKrkhFaRVdsPWKC_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KWECICOTRMQFPYQFTYPGJEAOPZC_T3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_H
#ifndef CONTROLLERMAPLAYOUTMANAGER_T58D8D581E5DB2F37D020E1A35F0182997BE98781_H
#define CONTROLLERMAPLAYOUTMANAGER_T58D8D581E5DB2F37D020E1A35F0182997BE98781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapLayoutManager
struct  ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapLayoutManager::_enabled
	bool ____enabled_0;
	// System.Boolean Rewired.ControllerMapLayoutManager::_loadFromUserDataStore
	bool ____loadFromUserDataStore_1;
	// Rewired.Player Rewired.ControllerMapLayoutManager::_player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ____player_2;
	// Rewired.ControllerMapLayoutManager_StartingSettings Rewired.ControllerMapLayoutManager::_startingSettings
	StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1 * ____startingSettings_3;
	// System.Int32 Rewired.ControllerMapLayoutManager::_reInputId
	int32_t ____reInputId_4;
	// System.Collections.Generic.List`1<Rewired.ControllerMapLayoutManager_RuleSet> Rewired.ControllerMapLayoutManager::_ruleSets
	List_1_t8B0F77E2955203535F6669AEABC756EEE15692AE * ____ruleSets_5;
	// System.Action Rewired.ControllerMapLayoutManager::_ApplyCalledEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____ApplyCalledEvent_6;

public:
	inline static int32_t get_offset_of__enabled_0() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____enabled_0)); }
	inline bool get__enabled_0() const { return ____enabled_0; }
	inline bool* get_address_of__enabled_0() { return &____enabled_0; }
	inline void set__enabled_0(bool value)
	{
		____enabled_0 = value;
	}

	inline static int32_t get_offset_of__loadFromUserDataStore_1() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____loadFromUserDataStore_1)); }
	inline bool get__loadFromUserDataStore_1() const { return ____loadFromUserDataStore_1; }
	inline bool* get_address_of__loadFromUserDataStore_1() { return &____loadFromUserDataStore_1; }
	inline void set__loadFromUserDataStore_1(bool value)
	{
		____loadFromUserDataStore_1 = value;
	}

	inline static int32_t get_offset_of__player_2() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____player_2)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get__player_2() const { return ____player_2; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of__player_2() { return &____player_2; }
	inline void set__player_2(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		____player_2 = value;
		Il2CppCodeGenWriteBarrier((&____player_2), value);
	}

	inline static int32_t get_offset_of__startingSettings_3() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____startingSettings_3)); }
	inline StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1 * get__startingSettings_3() const { return ____startingSettings_3; }
	inline StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1 ** get_address_of__startingSettings_3() { return &____startingSettings_3; }
	inline void set__startingSettings_3(StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1 * value)
	{
		____startingSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&____startingSettings_3), value);
	}

	inline static int32_t get_offset_of__reInputId_4() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____reInputId_4)); }
	inline int32_t get__reInputId_4() const { return ____reInputId_4; }
	inline int32_t* get_address_of__reInputId_4() { return &____reInputId_4; }
	inline void set__reInputId_4(int32_t value)
	{
		____reInputId_4 = value;
	}

	inline static int32_t get_offset_of__ruleSets_5() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____ruleSets_5)); }
	inline List_1_t8B0F77E2955203535F6669AEABC756EEE15692AE * get__ruleSets_5() const { return ____ruleSets_5; }
	inline List_1_t8B0F77E2955203535F6669AEABC756EEE15692AE ** get_address_of__ruleSets_5() { return &____ruleSets_5; }
	inline void set__ruleSets_5(List_1_t8B0F77E2955203535F6669AEABC756EEE15692AE * value)
	{
		____ruleSets_5 = value;
		Il2CppCodeGenWriteBarrier((&____ruleSets_5), value);
	}

	inline static int32_t get_offset_of__ApplyCalledEvent_6() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781, ____ApplyCalledEvent_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__ApplyCalledEvent_6() const { return ____ApplyCalledEvent_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__ApplyCalledEvent_6() { return &____ApplyCalledEvent_6; }
	inline void set__ApplyCalledEvent_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____ApplyCalledEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&____ApplyCalledEvent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPLAYOUTMANAGER_T58D8D581E5DB2F37D020E1A35F0182997BE98781_H
#ifndef RULE_T3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE_H
#define RULE_T3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapLayoutManager_Rule
struct  Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE  : public RuntimeObject
{
public:
	// System.String Rewired.ControllerMapLayoutManager_Rule::_tag
	String_t* ____tag_0;
	// System.Int32[] Rewired.ControllerMapLayoutManager_Rule::_categoryIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____categoryIds_1;
	// System.Int32 Rewired.ControllerMapLayoutManager_Rule::_layoutId
	int32_t ____layoutId_2;
	// Rewired.ControllerSetSelector Rewired.ControllerMapLayoutManager_Rule::_controllerSetSelector
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * ____controllerSetSelector_3;
	// System.String[] Rewired.ControllerMapLayoutManager_Rule::_preInitCategoryNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____preInitCategoryNames_4;
	// System.String Rewired.ControllerMapLayoutManager_Rule::_preInitLayoutName
	String_t* ____preInitLayoutName_5;

public:
	inline static int32_t get_offset_of__tag_0() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____tag_0)); }
	inline String_t* get__tag_0() const { return ____tag_0; }
	inline String_t** get_address_of__tag_0() { return &____tag_0; }
	inline void set__tag_0(String_t* value)
	{
		____tag_0 = value;
		Il2CppCodeGenWriteBarrier((&____tag_0), value);
	}

	inline static int32_t get_offset_of__categoryIds_1() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____categoryIds_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__categoryIds_1() const { return ____categoryIds_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__categoryIds_1() { return &____categoryIds_1; }
	inline void set__categoryIds_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____categoryIds_1 = value;
		Il2CppCodeGenWriteBarrier((&____categoryIds_1), value);
	}

	inline static int32_t get_offset_of__layoutId_2() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____layoutId_2)); }
	inline int32_t get__layoutId_2() const { return ____layoutId_2; }
	inline int32_t* get_address_of__layoutId_2() { return &____layoutId_2; }
	inline void set__layoutId_2(int32_t value)
	{
		____layoutId_2 = value;
	}

	inline static int32_t get_offset_of__controllerSetSelector_3() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____controllerSetSelector_3)); }
	inline ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * get__controllerSetSelector_3() const { return ____controllerSetSelector_3; }
	inline ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD ** get_address_of__controllerSetSelector_3() { return &____controllerSetSelector_3; }
	inline void set__controllerSetSelector_3(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * value)
	{
		____controllerSetSelector_3 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSetSelector_3), value);
	}

	inline static int32_t get_offset_of__preInitCategoryNames_4() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____preInitCategoryNames_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__preInitCategoryNames_4() const { return ____preInitCategoryNames_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__preInitCategoryNames_4() { return &____preInitCategoryNames_4; }
	inline void set__preInitCategoryNames_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____preInitCategoryNames_4 = value;
		Il2CppCodeGenWriteBarrier((&____preInitCategoryNames_4), value);
	}

	inline static int32_t get_offset_of__preInitLayoutName_5() { return static_cast<int32_t>(offsetof(Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE, ____preInitLayoutName_5)); }
	inline String_t* get__preInitLayoutName_5() const { return ____preInitLayoutName_5; }
	inline String_t** get_address_of__preInitLayoutName_5() { return &____preInitLayoutName_5; }
	inline void set__preInitLayoutName_5(String_t* value)
	{
		____preInitLayoutName_5 = value;
		Il2CppCodeGenWriteBarrier((&____preInitLayoutName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RULE_T3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE_H
#ifndef RULESET_TBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3_H
#define RULESET_TBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapLayoutManager_RuleSet
struct  RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapLayoutManager_RuleSet::_enabled
	bool ____enabled_1;
	// System.String Rewired.ControllerMapLayoutManager_RuleSet::_tag
	String_t* ____tag_2;
	// System.Collections.Generic.List`1<Rewired.ControllerMapLayoutManager_Rule> Rewired.ControllerMapLayoutManager_RuleSet::_rules
	List_1_t12B1E729214DA1F7DB2084EB2FA37C04C7F509ED * ____rules_3;

public:
	inline static int32_t get_offset_of__enabled_1() { return static_cast<int32_t>(offsetof(RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3, ____enabled_1)); }
	inline bool get__enabled_1() const { return ____enabled_1; }
	inline bool* get_address_of__enabled_1() { return &____enabled_1; }
	inline void set__enabled_1(bool value)
	{
		____enabled_1 = value;
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__rules_3() { return static_cast<int32_t>(offsetof(RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3, ____rules_3)); }
	inline List_1_t12B1E729214DA1F7DB2084EB2FA37C04C7F509ED * get__rules_3() const { return ____rules_3; }
	inline List_1_t12B1E729214DA1F7DB2084EB2FA37C04C7F509ED ** get_address_of__rules_3() { return &____rules_3; }
	inline void set__rules_3(List_1_t12B1E729214DA1F7DB2084EB2FA37C04C7F509ED * value)
	{
		____rules_3 = value;
		Il2CppCodeGenWriteBarrier((&____rules_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RULESET_TBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3_H
#ifndef STARTINGSETTINGS_T3C9D18DE9D7547393AE30C6222D418639EBB5AA1_H
#define STARTINGSETTINGS_T3C9D18DE9D7547393AE30C6222D418639EBB5AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapLayoutManager_StartingSettings
struct  StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapLayoutManager_StartingSettings::enabled
	bool ___enabled_0;
	// System.Boolean Rewired.ControllerMapLayoutManager_StartingSettings::loadFromUserDataStore
	bool ___loadFromUserDataStore_1;
	// jufVWXOncnQzwwsvxneDefLrtLg[] Rewired.ControllerMapLayoutManager_StartingSettings::startingRuleSets
	jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* ___startingRuleSets_2;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_loadFromUserDataStore_1() { return static_cast<int32_t>(offsetof(StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1, ___loadFromUserDataStore_1)); }
	inline bool get_loadFromUserDataStore_1() const { return ___loadFromUserDataStore_1; }
	inline bool* get_address_of_loadFromUserDataStore_1() { return &___loadFromUserDataStore_1; }
	inline void set_loadFromUserDataStore_1(bool value)
	{
		___loadFromUserDataStore_1 = value;
	}

	inline static int32_t get_offset_of_startingRuleSets_2() { return static_cast<int32_t>(offsetof(StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1, ___startingRuleSets_2)); }
	inline jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* get_startingRuleSets_2() const { return ___startingRuleSets_2; }
	inline jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09** get_address_of_startingRuleSets_2() { return &___startingRuleSets_2; }
	inline void set_startingRuleSets_2(jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* value)
	{
		___startingRuleSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___startingRuleSets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTINGSETTINGS_T3C9D18DE9D7547393AE30C6222D418639EBB5AA1_H
#ifndef CONTROLLERMAPSAVEDATA_T9EF811A272881EFD74E188CA3C628B6B227DDF7F_H
#define CONTROLLERMAPSAVEDATA_T9EF811A272881EFD74E188CA3C628B6B227DDF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapSaveData
struct  ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F  : public RuntimeObject
{
public:
	// Rewired.Controller Rewired.ControllerMapSaveData::_controller
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ____controller_0;
	// Rewired.ControllerMap Rewired.ControllerMapSaveData::_map
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ____map_1;
	// System.Int32 Rewired.ControllerMapSaveData::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2;

public:
	inline static int32_t get_offset_of__controller_0() { return static_cast<int32_t>(offsetof(ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F, ____controller_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get__controller_0() const { return ____controller_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of__controller_0() { return &____controller_0; }
	inline void set__controller_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		____controller_0 = value;
		Il2CppCodeGenWriteBarrier((&____controller_0), value);
	}

	inline static int32_t get_offset_of__map_1() { return static_cast<int32_t>(offsetof(ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F, ____map_1)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get__map_1() const { return ____map_1; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of__map_1() { return &____map_1; }
	inline void set__map_1(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		____map_1 = value;
		Il2CppCodeGenWriteBarrier((&____map_1), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return static_cast<int32_t>(offsetof(ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPSAVEDATA_T9EF811A272881EFD74E188CA3C628B6B227DDF7F_H
#ifndef SSFNWMAVXPSZNKTVDYWWLHIKZNM_T56C84F31517B2CF05B352C4407A51E879C372EFC_H
#define SSFNWMAVXPSZNKTVDYWWLHIKZNM_T56C84F31517B2CF05B352C4407A51E879C372EFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM
struct  SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMapWithAxes Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::LHdGnHCkoRIrqjLwZdOwLxKUpPZ
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.ControllerMapWithAxes_SsfnwMAvxPSzNKtvDYWwLHIKzNM::xyrBNgPkbFouqDLYWRndyzUOyMa
	RuntimeObject* ___xyrBNgPkbFouqDLYWRndyzUOyMa_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() const { return ___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8() { return &___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8; }
	inline void set_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8 = value;
		Il2CppCodeGenWriteBarrier((&___LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8), value);
	}

	inline static int32_t get_offset_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9() { return static_cast<int32_t>(offsetof(SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC, ___xyrBNgPkbFouqDLYWRndyzUOyMa_9)); }
	inline RuntimeObject* get_xyrBNgPkbFouqDLYWRndyzUOyMa_9() const { return ___xyrBNgPkbFouqDLYWRndyzUOyMa_9; }
	inline RuntimeObject** get_address_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9() { return &___xyrBNgPkbFouqDLYWRndyzUOyMa_9; }
	inline void set_xyrBNgPkbFouqDLYWRndyzUOyMa_9(RuntimeObject* value)
	{
		___xyrBNgPkbFouqDLYWRndyzUOyMa_9 = value;
		Il2CppCodeGenWriteBarrier((&___xyrBNgPkbFouqDLYWRndyzUOyMa_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSFNWMAVXPSZNKTVDYWWLHIKZNM_T56C84F31517B2CF05B352C4407A51E879C372EFC_H
#ifndef CONTROLLERMAPENABLER_RULESET_EDITOR_TE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE_H
#define CONTROLLERMAPENABLER_RULESET_EDITOR_TE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerMapEnabler_RuleSet_Editor
struct  ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.ControllerMapEnabler_RuleSet_Editor::_id
	int32_t ____id_0;
	// System.String Rewired.Data.ControllerMapEnabler_RuleSet_Editor::_name
	String_t* ____name_1;
	// System.String Rewired.Data.ControllerMapEnabler_RuleSet_Editor::_tag
	String_t* ____tag_2;
	// System.Collections.Generic.List`1<Rewired.Data.ControllerMapEnabler_Rule_Editor> Rewired.Data.ControllerMapEnabler_RuleSet_Editor::_rules
	List_1_t8CD0FD6A5FFD5DA598CCADBF633321E480725FB3 * ____rules_3;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__rules_3() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE, ____rules_3)); }
	inline List_1_t8CD0FD6A5FFD5DA598CCADBF633321E480725FB3 * get__rules_3() const { return ____rules_3; }
	inline List_1_t8CD0FD6A5FFD5DA598CCADBF633321E480725FB3 ** get_address_of__rules_3() { return &____rules_3; }
	inline void set__rules_3(List_1_t8CD0FD6A5FFD5DA598CCADBF633321E480725FB3 * value)
	{
		____rules_3 = value;
		Il2CppCodeGenWriteBarrier((&____rules_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPENABLER_RULESET_EDITOR_TE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE_H
#ifndef CONTROLLERMAPENABLER_RULE_EDITOR_T6087E64B76D8155566E440E3290A382409B44D11_H
#define CONTROLLERMAPENABLER_RULE_EDITOR_T6087E64B76D8155566E440E3290A382409B44D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerMapEnabler_Rule_Editor
struct  ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11  : public RuntimeObject
{
public:
	// System.String Rewired.Data.ControllerMapEnabler_Rule_Editor::_tag
	String_t* ____tag_0;
	// System.Boolean Rewired.Data.ControllerMapEnabler_Rule_Editor::_enable
	bool ____enable_1;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.ControllerMapEnabler_Rule_Editor::_categoryIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____categoryIds_2;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.ControllerMapEnabler_Rule_Editor::_layoutIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____layoutIds_3;
	// Rewired.Data.ControllerSetSelector_Editor Rewired.Data.ControllerMapEnabler_Rule_Editor::_controllerSetSelector
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * ____controllerSetSelector_4;

public:
	inline static int32_t get_offset_of__tag_0() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11, ____tag_0)); }
	inline String_t* get__tag_0() const { return ____tag_0; }
	inline String_t** get_address_of__tag_0() { return &____tag_0; }
	inline void set__tag_0(String_t* value)
	{
		____tag_0 = value;
		Il2CppCodeGenWriteBarrier((&____tag_0), value);
	}

	inline static int32_t get_offset_of__enable_1() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11, ____enable_1)); }
	inline bool get__enable_1() const { return ____enable_1; }
	inline bool* get_address_of__enable_1() { return &____enable_1; }
	inline void set__enable_1(bool value)
	{
		____enable_1 = value;
	}

	inline static int32_t get_offset_of__categoryIds_2() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11, ____categoryIds_2)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__categoryIds_2() const { return ____categoryIds_2; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__categoryIds_2() { return &____categoryIds_2; }
	inline void set__categoryIds_2(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____categoryIds_2 = value;
		Il2CppCodeGenWriteBarrier((&____categoryIds_2), value);
	}

	inline static int32_t get_offset_of__layoutIds_3() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11, ____layoutIds_3)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__layoutIds_3() const { return ____layoutIds_3; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__layoutIds_3() { return &____layoutIds_3; }
	inline void set__layoutIds_3(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____layoutIds_3 = value;
		Il2CppCodeGenWriteBarrier((&____layoutIds_3), value);
	}

	inline static int32_t get_offset_of__controllerSetSelector_4() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11, ____controllerSetSelector_4)); }
	inline ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * get__controllerSetSelector_4() const { return ____controllerSetSelector_4; }
	inline ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 ** get_address_of__controllerSetSelector_4() { return &____controllerSetSelector_4; }
	inline void set__controllerSetSelector_4(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * value)
	{
		____controllerSetSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSetSelector_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPENABLER_RULE_EDITOR_T6087E64B76D8155566E440E3290A382409B44D11_H
#ifndef CONTROLLERMAPLAYOUTMANAGER_RULESET_EDITOR_T37EFE246C4AA1E23652154683A584721F92EA960_H
#define CONTROLLERMAPLAYOUTMANAGER_RULESET_EDITOR_T37EFE246C4AA1E23652154683A584721F92EA960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor
struct  ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor::_id
	int32_t ____id_0;
	// System.String Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor::_name
	String_t* ____name_1;
	// System.String Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor::_tag
	String_t* ____tag_2;
	// System.Collections.Generic.List`1<Rewired.Data.ControllerMapLayoutManager_Rule_Editor> Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor::_rules
	List_1_tEAF03A5C372A25CD0CB3FF3D462E0372D0C2CD9F * ____rules_3;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__rules_3() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960, ____rules_3)); }
	inline List_1_tEAF03A5C372A25CD0CB3FF3D462E0372D0C2CD9F * get__rules_3() const { return ____rules_3; }
	inline List_1_tEAF03A5C372A25CD0CB3FF3D462E0372D0C2CD9F ** get_address_of__rules_3() { return &____rules_3; }
	inline void set__rules_3(List_1_tEAF03A5C372A25CD0CB3FF3D462E0372D0C2CD9F * value)
	{
		____rules_3 = value;
		Il2CppCodeGenWriteBarrier((&____rules_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPLAYOUTMANAGER_RULESET_EDITOR_T37EFE246C4AA1E23652154683A584721F92EA960_H
#ifndef CONTROLLERMAPLAYOUTMANAGER_RULE_EDITOR_T9560738F08FDF3D8FF6794B6132455764DCA2E07_H
#define CONTROLLERMAPLAYOUTMANAGER_RULE_EDITOR_T9560738F08FDF3D8FF6794B6132455764DCA2E07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerMapLayoutManager_Rule_Editor
struct  ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07  : public RuntimeObject
{
public:
	// System.String Rewired.Data.ControllerMapLayoutManager_Rule_Editor::_tag
	String_t* ____tag_0;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.ControllerMapLayoutManager_Rule_Editor::_categoryIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____categoryIds_1;
	// System.Int32 Rewired.Data.ControllerMapLayoutManager_Rule_Editor::_layoutId
	int32_t ____layoutId_2;
	// Rewired.Data.ControllerSetSelector_Editor Rewired.Data.ControllerMapLayoutManager_Rule_Editor::_controllerSetSelector
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * ____controllerSetSelector_3;

public:
	inline static int32_t get_offset_of__tag_0() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07, ____tag_0)); }
	inline String_t* get__tag_0() const { return ____tag_0; }
	inline String_t** get_address_of__tag_0() { return &____tag_0; }
	inline void set__tag_0(String_t* value)
	{
		____tag_0 = value;
		Il2CppCodeGenWriteBarrier((&____tag_0), value);
	}

	inline static int32_t get_offset_of__categoryIds_1() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07, ____categoryIds_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__categoryIds_1() const { return ____categoryIds_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__categoryIds_1() { return &____categoryIds_1; }
	inline void set__categoryIds_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____categoryIds_1 = value;
		Il2CppCodeGenWriteBarrier((&____categoryIds_1), value);
	}

	inline static int32_t get_offset_of__layoutId_2() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07, ____layoutId_2)); }
	inline int32_t get__layoutId_2() const { return ____layoutId_2; }
	inline int32_t* get_address_of__layoutId_2() { return &____layoutId_2; }
	inline void set__layoutId_2(int32_t value)
	{
		____layoutId_2 = value;
	}

	inline static int32_t get_offset_of__controllerSetSelector_3() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07, ____controllerSetSelector_3)); }
	inline ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * get__controllerSetSelector_3() const { return ____controllerSetSelector_3; }
	inline ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 ** get_address_of__controllerSetSelector_3() { return &____controllerSetSelector_3; }
	inline void set__controllerSetSelector_3(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8 * value)
	{
		____controllerSetSelector_3 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSetSelector_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPLAYOUTMANAGER_RULE_EDITOR_T9560738F08FDF3D8FF6794B6132455764DCA2E07_H
#ifndef CONTROLLERMAP_EDITOR_TFF9297D0655C06B97D1BCA1D434828B2614A7CA7_H
#define CONTROLLERMAP_EDITOR_TFF9297D0655C06B97D1BCA1D434828B2614A7CA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerMap_Editor
struct  ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor::id
	int32_t ___id_0;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor::categoryId
	int32_t ___categoryId_1;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor::layoutId
	int32_t ___layoutId_2;
	// System.String Rewired.Data.Mapping.ControllerMap_Editor::name
	String_t* ___name_3;
	// System.String Rewired.Data.Mapping.ControllerMap_Editor::hardwareGuidString
	String_t* ___hardwareGuidString_4;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor::customControllerUid
	int32_t ___customControllerUid_5;
	// System.Collections.Generic.List`1<Rewired.ActionElementMap> Rewired.Data.Mapping.ControllerMap_Editor::actionElementMaps
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___actionElementMaps_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_categoryId_1() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___categoryId_1)); }
	inline int32_t get_categoryId_1() const { return ___categoryId_1; }
	inline int32_t* get_address_of_categoryId_1() { return &___categoryId_1; }
	inline void set_categoryId_1(int32_t value)
	{
		___categoryId_1 = value;
	}

	inline static int32_t get_offset_of_layoutId_2() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___layoutId_2)); }
	inline int32_t get_layoutId_2() const { return ___layoutId_2; }
	inline int32_t* get_address_of_layoutId_2() { return &___layoutId_2; }
	inline void set_layoutId_2(int32_t value)
	{
		___layoutId_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_hardwareGuidString_4() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___hardwareGuidString_4)); }
	inline String_t* get_hardwareGuidString_4() const { return ___hardwareGuidString_4; }
	inline String_t** get_address_of_hardwareGuidString_4() { return &___hardwareGuidString_4; }
	inline void set_hardwareGuidString_4(String_t* value)
	{
		___hardwareGuidString_4 = value;
		Il2CppCodeGenWriteBarrier((&___hardwareGuidString_4), value);
	}

	inline static int32_t get_offset_of_customControllerUid_5() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___customControllerUid_5)); }
	inline int32_t get_customControllerUid_5() const { return ___customControllerUid_5; }
	inline int32_t* get_address_of_customControllerUid_5() { return &___customControllerUid_5; }
	inline void set_customControllerUid_5(int32_t value)
	{
		___customControllerUid_5 = value;
	}

	inline static int32_t get_offset_of_actionElementMaps_6() { return static_cast<int32_t>(offsetof(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7, ___actionElementMaps_6)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_actionElementMaps_6() const { return ___actionElementMaps_6; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_actionElementMaps_6() { return &___actionElementMaps_6; }
	inline void set_actionElementMaps_6(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___actionElementMaps_6 = value;
		Il2CppCodeGenWriteBarrier((&___actionElementMaps_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAP_EDITOR_TFF9297D0655C06B97D1BCA1D434828B2614A7CA7_H
#ifndef KDFJZLBYJGMOBPKDSPZACPEVKPT_T62D350CF7F3448A234E092F807CBBDAF58693A3D_H
#define KDFJZLBYJGMOBPKDSPZACPEVKPT_T62D350CF7F3448A234E092F807CBBDAF58693A3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT
struct  KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.ControllerMap_Editor_KdfjzLbYjgmoBPkdspzacpEVkPT::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return static_cast<int32_t>(offsetof(KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D, ___okMFupfyoBNZDyKMyvAtwGIUiAd_4)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_4() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_4(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFJZLBYJGMOBPKDSPZACPEVKPT_T62D350CF7F3448A234E092F807CBBDAF58693A3D_H
#ifndef INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#define INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputCategory
struct  InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918  : public RuntimeObject
{
public:
	// System.String Rewired.InputCategory::_name
	String_t* ____name_0;
	// System.String Rewired.InputCategory::_descriptiveName
	String_t* ____descriptiveName_1;
	// System.String Rewired.InputCategory::_tag
	String_t* ____tag_2;
	// System.Int32 Rewired.InputCategory::_id
	int32_t ____id_3;
	// System.Boolean Rewired.InputCategory::_userAssignable
	bool ____userAssignable_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__descriptiveName_1() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____descriptiveName_1)); }
	inline String_t* get__descriptiveName_1() const { return ____descriptiveName_1; }
	inline String_t** get_address_of__descriptiveName_1() { return &____descriptiveName_1; }
	inline void set__descriptiveName_1(String_t* value)
	{
		____descriptiveName_1 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_1), value);
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____id_3)); }
	inline int32_t get__id_3() const { return ____id_3; }
	inline int32_t* get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(int32_t value)
	{
		____id_3 = value;
	}

	inline static int32_t get_offset_of__userAssignable_4() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____userAssignable_4)); }
	inline bool get__userAssignable_4() const { return ____userAssignable_4; }
	inline bool* get_address_of__userAssignable_4() { return &____userAssignable_4; }
	inline void set__userAssignable_4(bool value)
	{
		____userAssignable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#ifndef INPUTLAYOUT_T5DB3F9E538A5635352174D0E54C258BF1A7D4996_H
#define INPUTLAYOUT_T5DB3F9E538A5635352174D0E54C258BF1A7D4996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputLayout
struct  InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996  : public RuntimeObject
{
public:
	// System.String Rewired.InputLayout::_name
	String_t* ____name_0;
	// System.String Rewired.InputLayout::_descriptiveName
	String_t* ____descriptiveName_1;
	// System.Int32 Rewired.InputLayout::_id
	int32_t ____id_2;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__descriptiveName_1() { return static_cast<int32_t>(offsetof(InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996, ____descriptiveName_1)); }
	inline String_t* get__descriptiveName_1() const { return ____descriptiveName_1; }
	inline String_t** get_address_of__descriptiveName_1() { return &____descriptiveName_1; }
	inline void set__descriptiveName_1(String_t* value)
	{
		____descriptiveName_1 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_1), value);
	}

	inline static int32_t get_offset_of__id_2() { return static_cast<int32_t>(offsetof(InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996, ____id_2)); }
	inline int32_t get__id_2() const { return ____id_2; }
	inline int32_t* get_address_of__id_2() { return &____id_2; }
	inline void set__id_2(int32_t value)
	{
		____id_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTLAYOUT_T5DB3F9E538A5635352174D0E54C258BF1A7D4996_H
#ifndef INPUTMAPPER_T88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_H
#define INPUTMAPPER_T88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper
struct  InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB  : public RuntimeObject
{
public:
	// System.Int32 Rewired.InputMapper::qyFSGVtHJSgKjJMsehfQmDTGigQ
	int32_t ___qyFSGVtHJSgKjJMsehfQmDTGigQ_2;
	// System.Boolean Rewired.InputMapper::uFaaNepxrqhsVgtTjhTtdKBmAVO
	bool ___uFaaNepxrqhsVgtTjhTtdKBmAVO_3;
	// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI Rewired.InputMapper::suCpPRGzPwMIarMELiehjJQHZqQb
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76 * ___suCpPRGzPwMIarMELiehjJQHZqQb_4;
	// Rewired.InputMapper_Options Rewired.InputMapper::hQaGSpfGCOBUmxFmTPAEMXgnEuuD
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5;
	// System.Collections.Generic.Dictionary`2<Rewired.InputMapper_brixqxKPyakMwXgqKReyPWdHwsQ,Rewired.Utils.SafeDelegate> Rewired.InputMapper::SpcTsLWPUhOBPVkDNBqZAMhkbqLa
	Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6;

public:
	inline static int32_t get_offset_of_qyFSGVtHJSgKjJMsehfQmDTGigQ_2() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB, ___qyFSGVtHJSgKjJMsehfQmDTGigQ_2)); }
	inline int32_t get_qyFSGVtHJSgKjJMsehfQmDTGigQ_2() const { return ___qyFSGVtHJSgKjJMsehfQmDTGigQ_2; }
	inline int32_t* get_address_of_qyFSGVtHJSgKjJMsehfQmDTGigQ_2() { return &___qyFSGVtHJSgKjJMsehfQmDTGigQ_2; }
	inline void set_qyFSGVtHJSgKjJMsehfQmDTGigQ_2(int32_t value)
	{
		___qyFSGVtHJSgKjJMsehfQmDTGigQ_2 = value;
	}

	inline static int32_t get_offset_of_uFaaNepxrqhsVgtTjhTtdKBmAVO_3() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB, ___uFaaNepxrqhsVgtTjhTtdKBmAVO_3)); }
	inline bool get_uFaaNepxrqhsVgtTjhTtdKBmAVO_3() const { return ___uFaaNepxrqhsVgtTjhTtdKBmAVO_3; }
	inline bool* get_address_of_uFaaNepxrqhsVgtTjhTtdKBmAVO_3() { return &___uFaaNepxrqhsVgtTjhTtdKBmAVO_3; }
	inline void set_uFaaNepxrqhsVgtTjhTtdKBmAVO_3(bool value)
	{
		___uFaaNepxrqhsVgtTjhTtdKBmAVO_3 = value;
	}

	inline static int32_t get_offset_of_suCpPRGzPwMIarMELiehjJQHZqQb_4() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB, ___suCpPRGzPwMIarMELiehjJQHZqQb_4)); }
	inline WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76 * get_suCpPRGzPwMIarMELiehjJQHZqQb_4() const { return ___suCpPRGzPwMIarMELiehjJQHZqQb_4; }
	inline WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76 ** get_address_of_suCpPRGzPwMIarMELiehjJQHZqQb_4() { return &___suCpPRGzPwMIarMELiehjJQHZqQb_4; }
	inline void set_suCpPRGzPwMIarMELiehjJQHZqQb_4(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76 * value)
	{
		___suCpPRGzPwMIarMELiehjJQHZqQb_4 = value;
		Il2CppCodeGenWriteBarrier((&___suCpPRGzPwMIarMELiehjJQHZqQb_4), value);
	}

	inline static int32_t get_offset_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB, ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5)); }
	inline Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * get_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5() const { return ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5; }
	inline Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 ** get_address_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5() { return &___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5; }
	inline void set_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * value)
	{
		___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5 = value;
		Il2CppCodeGenWriteBarrier((&___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5), value);
	}

	inline static int32_t get_offset_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB, ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6)); }
	inline Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * get_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6() const { return ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6; }
	inline Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 ** get_address_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6() { return &___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6; }
	inline void set_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6(Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * value)
	{
		___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6 = value;
		Il2CppCodeGenWriteBarrier((&___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6), value);
	}
};

struct InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields
{
public:
	// Rewired.InputMapper Rewired.InputMapper::gwdVBQEakPuszKrkhFaRVdsPWKC
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___gwdVBQEakPuszKrkhFaRVdsPWKC_0;
	// System.Int32 Rewired.InputMapper::FSpmeDwklQQlpZeqNSHMGkVYZQJ
	int32_t ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_1;
	// System.Action`1<System.Exception> Rewired.InputMapper::LYelxTpCENiezznyJAAbgVviEhM
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___LYelxTpCENiezznyJAAbgVviEhM_7;
	// System.Action`1<System.Exception> Rewired.InputMapper::bIpOcSHJOaNbSfzUQfNHLjsIhGU
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8;
	// System.Action`1<System.Exception> Rewired.InputMapper::SeKbgRoSSdxvLKKSFGtzsUJVIEq
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9;
	// System.Action`1<System.Exception> Rewired.InputMapper::jqUTJMOeCBaPwenidONjsgqqmhF
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___jqUTJMOeCBaPwenidONjsgqqmhF_10;
	// System.Action`1<System.Exception> Rewired.InputMapper::VxgfIwaEpxChiqoBUxMyAXuOvzah
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___VxgfIwaEpxChiqoBUxMyAXuOvzah_11;
	// System.Action`1<System.Exception> Rewired.InputMapper::yGfaFeDTgjQCiyfEnFKwcIrTEKK
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12;
	// System.Action`1<System.Exception> Rewired.InputMapper::bNWtItxSTpScLiOAwzhSFiRFjTf
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___bNWtItxSTpScLiOAwzhSFiRFjTf_13;

public:
	inline static int32_t get_offset_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___gwdVBQEakPuszKrkhFaRVdsPWKC_0)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_gwdVBQEakPuszKrkhFaRVdsPWKC_0() const { return ___gwdVBQEakPuszKrkhFaRVdsPWKC_0; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0() { return &___gwdVBQEakPuszKrkhFaRVdsPWKC_0; }
	inline void set_gwdVBQEakPuszKrkhFaRVdsPWKC_0(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___gwdVBQEakPuszKrkhFaRVdsPWKC_0 = value;
		Il2CppCodeGenWriteBarrier((&___gwdVBQEakPuszKrkhFaRVdsPWKC_0), value);
	}

	inline static int32_t get_offset_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_1() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_1)); }
	inline int32_t get_FSpmeDwklQQlpZeqNSHMGkVYZQJ_1() const { return ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_1; }
	inline int32_t* get_address_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_1() { return &___FSpmeDwklQQlpZeqNSHMGkVYZQJ_1; }
	inline void set_FSpmeDwklQQlpZeqNSHMGkVYZQJ_1(int32_t value)
	{
		___FSpmeDwklQQlpZeqNSHMGkVYZQJ_1 = value;
	}

	inline static int32_t get_offset_of_LYelxTpCENiezznyJAAbgVviEhM_7() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___LYelxTpCENiezznyJAAbgVviEhM_7)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_LYelxTpCENiezznyJAAbgVviEhM_7() const { return ___LYelxTpCENiezznyJAAbgVviEhM_7; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_LYelxTpCENiezznyJAAbgVviEhM_7() { return &___LYelxTpCENiezznyJAAbgVviEhM_7; }
	inline void set_LYelxTpCENiezznyJAAbgVviEhM_7(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___LYelxTpCENiezznyJAAbgVviEhM_7 = value;
		Il2CppCodeGenWriteBarrier((&___LYelxTpCENiezznyJAAbgVviEhM_7), value);
	}

	inline static int32_t get_offset_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_8() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_bIpOcSHJOaNbSfzUQfNHLjsIhGU_8() const { return ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_8() { return &___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8; }
	inline void set_bIpOcSHJOaNbSfzUQfNHLjsIhGU_8(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8 = value;
		Il2CppCodeGenWriteBarrier((&___bIpOcSHJOaNbSfzUQfNHLjsIhGU_8), value);
	}

	inline static int32_t get_offset_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_9() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_SeKbgRoSSdxvLKKSFGtzsUJVIEq_9() const { return ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_9() { return &___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9; }
	inline void set_SeKbgRoSSdxvLKKSFGtzsUJVIEq_9(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9 = value;
		Il2CppCodeGenWriteBarrier((&___SeKbgRoSSdxvLKKSFGtzsUJVIEq_9), value);
	}

	inline static int32_t get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_10() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___jqUTJMOeCBaPwenidONjsgqqmhF_10)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_jqUTJMOeCBaPwenidONjsgqqmhF_10() const { return ___jqUTJMOeCBaPwenidONjsgqqmhF_10; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_jqUTJMOeCBaPwenidONjsgqqmhF_10() { return &___jqUTJMOeCBaPwenidONjsgqqmhF_10; }
	inline void set_jqUTJMOeCBaPwenidONjsgqqmhF_10(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___jqUTJMOeCBaPwenidONjsgqqmhF_10 = value;
		Il2CppCodeGenWriteBarrier((&___jqUTJMOeCBaPwenidONjsgqqmhF_10), value);
	}

	inline static int32_t get_offset_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_11() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___VxgfIwaEpxChiqoBUxMyAXuOvzah_11)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_VxgfIwaEpxChiqoBUxMyAXuOvzah_11() const { return ___VxgfIwaEpxChiqoBUxMyAXuOvzah_11; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_11() { return &___VxgfIwaEpxChiqoBUxMyAXuOvzah_11; }
	inline void set_VxgfIwaEpxChiqoBUxMyAXuOvzah_11(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___VxgfIwaEpxChiqoBUxMyAXuOvzah_11 = value;
		Il2CppCodeGenWriteBarrier((&___VxgfIwaEpxChiqoBUxMyAXuOvzah_11), value);
	}

	inline static int32_t get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_12() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_yGfaFeDTgjQCiyfEnFKwcIrTEKK_12() const { return ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_12() { return &___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12; }
	inline void set_yGfaFeDTgjQCiyfEnFKwcIrTEKK_12(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12 = value;
		Il2CppCodeGenWriteBarrier((&___yGfaFeDTgjQCiyfEnFKwcIrTEKK_12), value);
	}

	inline static int32_t get_offset_of_bNWtItxSTpScLiOAwzhSFiRFjTf_13() { return static_cast<int32_t>(offsetof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields, ___bNWtItxSTpScLiOAwzhSFiRFjTf_13)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_bNWtItxSTpScLiOAwzhSFiRFjTf_13() const { return ___bNWtItxSTpScLiOAwzhSFiRFjTf_13; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_bNWtItxSTpScLiOAwzhSFiRFjTf_13() { return &___bNWtItxSTpScLiOAwzhSFiRFjTf_13; }
	inline void set_bNWtItxSTpScLiOAwzhSFiRFjTf_13(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___bNWtItxSTpScLiOAwzhSFiRFjTf_13 = value;
		Il2CppCodeGenWriteBarrier((&___bNWtItxSTpScLiOAwzhSFiRFjTf_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPPER_T88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_H
#ifndef EVENTDATA_TE4C486A467E7CB80A9F88B04456F63263515B552_H
#define EVENTDATA_TE4C486A467E7CB80A9F88B04456F63263515B552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_EventData
struct  EventData_tE4C486A467E7CB80A9F88B04456F63263515B552  : public RuntimeObject
{
public:
	// Rewired.InputMapper Rewired.InputMapper_EventData::inputMapper
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___inputMapper_0;

public:
	inline static int32_t get_offset_of_inputMapper_0() { return static_cast<int32_t>(offsetof(EventData_tE4C486A467E7CB80A9F88B04456F63263515B552, ___inputMapper_0)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_inputMapper_0() const { return ___inputMapper_0; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_inputMapper_0() { return &___inputMapper_0; }
	inline void set_inputMapper_0(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___inputMapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___inputMapper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_TE4C486A467E7CB80A9F88B04456F63263515B552_H
#ifndef PLAYER_T0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F_H
#define PLAYER_T0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player
struct  Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F  : public RuntimeObject
{
public:
	// lUbzJtkApcIkohMrHhJAmcDImRRR Rewired.Player::hNtENxmAKBGmoTLtEcwPGIjaiDV
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * ___hNtENxmAKBGmoTLtEcwPGIjaiDV_0;
	// System.Boolean Rewired.Player::BrDDeraUArFZwzhmkxdxMTPGpvvB
	bool ___BrDDeraUArFZwzhmkxdxMTPGpvvB_1;
	// System.Int32 Rewired.Player::WHCIpuTfwdHNjfJipsoGkINikLt
	int32_t ___WHCIpuTfwdHNjfJipsoGkINikLt_2;
	// System.String Rewired.Player::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3;
	// System.String Rewired.Player::WnpeOEozhleBiLROoiweUAwvnlR
	String_t* ___WnpeOEozhleBiLROoiweUAwvnlR_4;
	// System.Boolean Rewired.Player::cEEqYVdhEBIXdUFBogquGTYgUYYl
	bool ___cEEqYVdhEBIXdUFBogquGTYgUYYl_5;
	// System.Int32 Rewired.Player::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6;
	// Rewired.Player_ControllerHelper Rewired.Player::controllers
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * ___controllers_7;

public:
	inline static int32_t get_offset_of_hNtENxmAKBGmoTLtEcwPGIjaiDV_0() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___hNtENxmAKBGmoTLtEcwPGIjaiDV_0)); }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * get_hNtENxmAKBGmoTLtEcwPGIjaiDV_0() const { return ___hNtENxmAKBGmoTLtEcwPGIjaiDV_0; }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE ** get_address_of_hNtENxmAKBGmoTLtEcwPGIjaiDV_0() { return &___hNtENxmAKBGmoTLtEcwPGIjaiDV_0; }
	inline void set_hNtENxmAKBGmoTLtEcwPGIjaiDV_0(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * value)
	{
		___hNtENxmAKBGmoTLtEcwPGIjaiDV_0 = value;
		Il2CppCodeGenWriteBarrier((&___hNtENxmAKBGmoTLtEcwPGIjaiDV_0), value);
	}

	inline static int32_t get_offset_of_BrDDeraUArFZwzhmkxdxMTPGpvvB_1() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___BrDDeraUArFZwzhmkxdxMTPGpvvB_1)); }
	inline bool get_BrDDeraUArFZwzhmkxdxMTPGpvvB_1() const { return ___BrDDeraUArFZwzhmkxdxMTPGpvvB_1; }
	inline bool* get_address_of_BrDDeraUArFZwzhmkxdxMTPGpvvB_1() { return &___BrDDeraUArFZwzhmkxdxMTPGpvvB_1; }
	inline void set_BrDDeraUArFZwzhmkxdxMTPGpvvB_1(bool value)
	{
		___BrDDeraUArFZwzhmkxdxMTPGpvvB_1 = value;
	}

	inline static int32_t get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_2() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___WHCIpuTfwdHNjfJipsoGkINikLt_2)); }
	inline int32_t get_WHCIpuTfwdHNjfJipsoGkINikLt_2() const { return ___WHCIpuTfwdHNjfJipsoGkINikLt_2; }
	inline int32_t* get_address_of_WHCIpuTfwdHNjfJipsoGkINikLt_2() { return &___WHCIpuTfwdHNjfJipsoGkINikLt_2; }
	inline void set_WHCIpuTfwdHNjfJipsoGkINikLt_2(int32_t value)
	{
		___WHCIpuTfwdHNjfJipsoGkINikLt_2 = value;
	}

	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_3() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_3() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_3() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_3(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_3), value);
	}

	inline static int32_t get_offset_of_WnpeOEozhleBiLROoiweUAwvnlR_4() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___WnpeOEozhleBiLROoiweUAwvnlR_4)); }
	inline String_t* get_WnpeOEozhleBiLROoiweUAwvnlR_4() const { return ___WnpeOEozhleBiLROoiweUAwvnlR_4; }
	inline String_t** get_address_of_WnpeOEozhleBiLROoiweUAwvnlR_4() { return &___WnpeOEozhleBiLROoiweUAwvnlR_4; }
	inline void set_WnpeOEozhleBiLROoiweUAwvnlR_4(String_t* value)
	{
		___WnpeOEozhleBiLROoiweUAwvnlR_4 = value;
		Il2CppCodeGenWriteBarrier((&___WnpeOEozhleBiLROoiweUAwvnlR_4), value);
	}

	inline static int32_t get_offset_of_cEEqYVdhEBIXdUFBogquGTYgUYYl_5() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___cEEqYVdhEBIXdUFBogquGTYgUYYl_5)); }
	inline bool get_cEEqYVdhEBIXdUFBogquGTYgUYYl_5() const { return ___cEEqYVdhEBIXdUFBogquGTYgUYYl_5; }
	inline bool* get_address_of_cEEqYVdhEBIXdUFBogquGTYgUYYl_5() { return &___cEEqYVdhEBIXdUFBogquGTYgUYYl_5; }
	inline void set_cEEqYVdhEBIXdUFBogquGTYgUYYl_5(bool value)
	{
		___cEEqYVdhEBIXdUFBogquGTYgUYYl_5 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_6 = value;
	}

	inline static int32_t get_offset_of_controllers_7() { return static_cast<int32_t>(offsetof(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F, ___controllers_7)); }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * get_controllers_7() const { return ___controllers_7; }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC ** get_address_of_controllers_7() { return &___controllers_7; }
	inline void set_controllers_7(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * value)
	{
		___controllers_7 = value;
		Il2CppCodeGenWriteBarrier((&___controllers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F_H
#ifndef CONTROLLERHELPER_TBC381D3F373CC57D36553FBA0843F6857C3C8DEC_H
#define CONTROLLERHELPER_TBC381D3F373CC57D36553FBA0843F6857C3C8DEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper
struct  ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC  : public RuntimeObject
{
public:
	// Rewired.Player_ControllerHelper_ZgoffaakscXwSecdxgDVDCqXYPjo Rewired.Player_ControllerHelper::TiDRaJtNeCdLlYBqWUbUiRHsfNd
	ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B * ___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0;
	// System.Boolean Rewired.Player_ControllerHelper::whOXOUQXtLtOtzMQsfCZDcYviPD
	bool ___whOXOUQXtLtOtzMQsfCZDcYviPD_1;
	// System.Boolean Rewired.Player_ControllerHelper::BuGnLKIMjwgXSWwDepEcRQAPEATG
	bool ___BuGnLKIMjwgXSWwDepEcRQAPEATG_2;
	// System.Boolean Rewired.Player_ControllerHelper::yCFRyLTuDDJPotjpDvdOaCEDDvN
	bool ___yCFRyLTuDDJPotjpDvdOaCEDDvN_3;
	// System.Single Rewired.Player_ControllerHelper::EODhOMizDenmfzfktoHJosKgaaN
	float ___EODhOMizDenmfzfktoHJosKgaaN_4;
	// System.Single Rewired.Player_ControllerHelper::StyMgvhXDptNsffkLGbApHZhpv
	float ___StyMgvhXDptNsffkLGbApHZhpv_5;
	// Rewired.Utils.SafeAction`1<Rewired.ControllerAssignmentChangedEventArgs> Rewired.Player_ControllerHelper::MBSfFDOuOKrryYPaClUnEhXiMkX
	SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * ___MBSfFDOuOKrryYPaClUnEhXiMkX_6;
	// Rewired.Utils.SafeAction`1<Rewired.ControllerAssignmentChangedEventArgs> Rewired.Player_ControllerHelper::DQgqMYhPHoLRPOEOdGMwJQINGZeG
	SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * ___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7;
	// Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN Rewired.Player_ControllerHelper::iswBkwCyDabLmfxdAYpLbXBWfvHN
	GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377 * ___iswBkwCyDabLmfxdAYpLbXBWfvHN_8;
	// Rewired.Player Rewired.Player_ControllerHelper::DelHerRPxmEUeLMSQbMNIPccOrs
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___DelHerRPxmEUeLMSQbMNIPccOrs_9;
	// rIcvLcWCuQghgaaiofvKjSvndJX Rewired.Player_ControllerHelper::stygjDDRGdOuLuSiDJenZijkpLMI
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * ___stygjDDRGdOuLuSiDJenZijkpLMI_10;
	// System.Int32 Rewired.Player_ControllerHelper::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper::maps
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___maps_12;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper::conflictChecking
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___conflictChecking_13;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper::polling
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___polling_14;

public:
	inline static int32_t get_offset_of_TiDRaJtNeCdLlYBqWUbUiRHsfNd_0() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0)); }
	inline ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B * get_TiDRaJtNeCdLlYBqWUbUiRHsfNd_0() const { return ___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0; }
	inline ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B ** get_address_of_TiDRaJtNeCdLlYBqWUbUiRHsfNd_0() { return &___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0; }
	inline void set_TiDRaJtNeCdLlYBqWUbUiRHsfNd_0(ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B * value)
	{
		___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0 = value;
		Il2CppCodeGenWriteBarrier((&___TiDRaJtNeCdLlYBqWUbUiRHsfNd_0), value);
	}

	inline static int32_t get_offset_of_whOXOUQXtLtOtzMQsfCZDcYviPD_1() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___whOXOUQXtLtOtzMQsfCZDcYviPD_1)); }
	inline bool get_whOXOUQXtLtOtzMQsfCZDcYviPD_1() const { return ___whOXOUQXtLtOtzMQsfCZDcYviPD_1; }
	inline bool* get_address_of_whOXOUQXtLtOtzMQsfCZDcYviPD_1() { return &___whOXOUQXtLtOtzMQsfCZDcYviPD_1; }
	inline void set_whOXOUQXtLtOtzMQsfCZDcYviPD_1(bool value)
	{
		___whOXOUQXtLtOtzMQsfCZDcYviPD_1 = value;
	}

	inline static int32_t get_offset_of_BuGnLKIMjwgXSWwDepEcRQAPEATG_2() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___BuGnLKIMjwgXSWwDepEcRQAPEATG_2)); }
	inline bool get_BuGnLKIMjwgXSWwDepEcRQAPEATG_2() const { return ___BuGnLKIMjwgXSWwDepEcRQAPEATG_2; }
	inline bool* get_address_of_BuGnLKIMjwgXSWwDepEcRQAPEATG_2() { return &___BuGnLKIMjwgXSWwDepEcRQAPEATG_2; }
	inline void set_BuGnLKIMjwgXSWwDepEcRQAPEATG_2(bool value)
	{
		___BuGnLKIMjwgXSWwDepEcRQAPEATG_2 = value;
	}

	inline static int32_t get_offset_of_yCFRyLTuDDJPotjpDvdOaCEDDvN_3() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___yCFRyLTuDDJPotjpDvdOaCEDDvN_3)); }
	inline bool get_yCFRyLTuDDJPotjpDvdOaCEDDvN_3() const { return ___yCFRyLTuDDJPotjpDvdOaCEDDvN_3; }
	inline bool* get_address_of_yCFRyLTuDDJPotjpDvdOaCEDDvN_3() { return &___yCFRyLTuDDJPotjpDvdOaCEDDvN_3; }
	inline void set_yCFRyLTuDDJPotjpDvdOaCEDDvN_3(bool value)
	{
		___yCFRyLTuDDJPotjpDvdOaCEDDvN_3 = value;
	}

	inline static int32_t get_offset_of_EODhOMizDenmfzfktoHJosKgaaN_4() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___EODhOMizDenmfzfktoHJosKgaaN_4)); }
	inline float get_EODhOMizDenmfzfktoHJosKgaaN_4() const { return ___EODhOMizDenmfzfktoHJosKgaaN_4; }
	inline float* get_address_of_EODhOMizDenmfzfktoHJosKgaaN_4() { return &___EODhOMizDenmfzfktoHJosKgaaN_4; }
	inline void set_EODhOMizDenmfzfktoHJosKgaaN_4(float value)
	{
		___EODhOMizDenmfzfktoHJosKgaaN_4 = value;
	}

	inline static int32_t get_offset_of_StyMgvhXDptNsffkLGbApHZhpv_5() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___StyMgvhXDptNsffkLGbApHZhpv_5)); }
	inline float get_StyMgvhXDptNsffkLGbApHZhpv_5() const { return ___StyMgvhXDptNsffkLGbApHZhpv_5; }
	inline float* get_address_of_StyMgvhXDptNsffkLGbApHZhpv_5() { return &___StyMgvhXDptNsffkLGbApHZhpv_5; }
	inline void set_StyMgvhXDptNsffkLGbApHZhpv_5(float value)
	{
		___StyMgvhXDptNsffkLGbApHZhpv_5 = value;
	}

	inline static int32_t get_offset_of_MBSfFDOuOKrryYPaClUnEhXiMkX_6() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___MBSfFDOuOKrryYPaClUnEhXiMkX_6)); }
	inline SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * get_MBSfFDOuOKrryYPaClUnEhXiMkX_6() const { return ___MBSfFDOuOKrryYPaClUnEhXiMkX_6; }
	inline SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 ** get_address_of_MBSfFDOuOKrryYPaClUnEhXiMkX_6() { return &___MBSfFDOuOKrryYPaClUnEhXiMkX_6; }
	inline void set_MBSfFDOuOKrryYPaClUnEhXiMkX_6(SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * value)
	{
		___MBSfFDOuOKrryYPaClUnEhXiMkX_6 = value;
		Il2CppCodeGenWriteBarrier((&___MBSfFDOuOKrryYPaClUnEhXiMkX_6), value);
	}

	inline static int32_t get_offset_of_DQgqMYhPHoLRPOEOdGMwJQINGZeG_7() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7)); }
	inline SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * get_DQgqMYhPHoLRPOEOdGMwJQINGZeG_7() const { return ___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7; }
	inline SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 ** get_address_of_DQgqMYhPHoLRPOEOdGMwJQINGZeG_7() { return &___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7; }
	inline void set_DQgqMYhPHoLRPOEOdGMwJQINGZeG_7(SafeAction_1_tA11ADD9DAB6EF8AEDCEBEB2219B2D1624292FB90 * value)
	{
		___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7 = value;
		Il2CppCodeGenWriteBarrier((&___DQgqMYhPHoLRPOEOdGMwJQINGZeG_7), value);
	}

	inline static int32_t get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_8() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___iswBkwCyDabLmfxdAYpLbXBWfvHN_8)); }
	inline GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377 * get_iswBkwCyDabLmfxdAYpLbXBWfvHN_8() const { return ___iswBkwCyDabLmfxdAYpLbXBWfvHN_8; }
	inline GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377 ** get_address_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_8() { return &___iswBkwCyDabLmfxdAYpLbXBWfvHN_8; }
	inline void set_iswBkwCyDabLmfxdAYpLbXBWfvHN_8(GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377 * value)
	{
		___iswBkwCyDabLmfxdAYpLbXBWfvHN_8 = value;
		Il2CppCodeGenWriteBarrier((&___iswBkwCyDabLmfxdAYpLbXBWfvHN_8), value);
	}

	inline static int32_t get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_9() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___DelHerRPxmEUeLMSQbMNIPccOrs_9)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_DelHerRPxmEUeLMSQbMNIPccOrs_9() const { return ___DelHerRPxmEUeLMSQbMNIPccOrs_9; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_DelHerRPxmEUeLMSQbMNIPccOrs_9() { return &___DelHerRPxmEUeLMSQbMNIPccOrs_9; }
	inline void set_DelHerRPxmEUeLMSQbMNIPccOrs_9(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___DelHerRPxmEUeLMSQbMNIPccOrs_9 = value;
		Il2CppCodeGenWriteBarrier((&___DelHerRPxmEUeLMSQbMNIPccOrs_9), value);
	}

	inline static int32_t get_offset_of_stygjDDRGdOuLuSiDJenZijkpLMI_10() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___stygjDDRGdOuLuSiDJenZijkpLMI_10)); }
	inline rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * get_stygjDDRGdOuLuSiDJenZijkpLMI_10() const { return ___stygjDDRGdOuLuSiDJenZijkpLMI_10; }
	inline rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 ** get_address_of_stygjDDRGdOuLuSiDJenZijkpLMI_10() { return &___stygjDDRGdOuLuSiDJenZijkpLMI_10; }
	inline void set_stygjDDRGdOuLuSiDJenZijkpLMI_10(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * value)
	{
		___stygjDDRGdOuLuSiDJenZijkpLMI_10 = value;
		Il2CppCodeGenWriteBarrier((&___stygjDDRGdOuLuSiDJenZijkpLMI_10), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_11(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_11 = value;
	}

	inline static int32_t get_offset_of_maps_12() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___maps_12)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_maps_12() const { return ___maps_12; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_maps_12() { return &___maps_12; }
	inline void set_maps_12(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___maps_12 = value;
		Il2CppCodeGenWriteBarrier((&___maps_12), value);
	}

	inline static int32_t get_offset_of_conflictChecking_13() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___conflictChecking_13)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_conflictChecking_13() const { return ___conflictChecking_13; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_conflictChecking_13() { return &___conflictChecking_13; }
	inline void set_conflictChecking_13(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___conflictChecking_13 = value;
		Il2CppCodeGenWriteBarrier((&___conflictChecking_13), value);
	}

	inline static int32_t get_offset_of_polling_14() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC, ___polling_14)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_polling_14() const { return ___polling_14; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_polling_14() { return &___polling_14; }
	inline void set_polling_14(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___polling_14 = value;
		Il2CppCodeGenWriteBarrier((&___polling_14), value);
	}
};

struct ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields
{
public:
	// System.Action`1<System.Exception> Rewired.Player_ControllerHelper::xuKcWqHPyZZLojTegosyfuffNaVL
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___xuKcWqHPyZZLojTegosyfuffNaVL_15;
	// System.Action`1<System.Exception> Rewired.Player_ControllerHelper::xgFcMzbcKwfNhKMwwFcokhPKEXIb
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16;

public:
	inline static int32_t get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_15() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields, ___xuKcWqHPyZZLojTegosyfuffNaVL_15)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_xuKcWqHPyZZLojTegosyfuffNaVL_15() const { return ___xuKcWqHPyZZLojTegosyfuffNaVL_15; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_xuKcWqHPyZZLojTegosyfuffNaVL_15() { return &___xuKcWqHPyZZLojTegosyfuffNaVL_15; }
	inline void set_xuKcWqHPyZZLojTegosyfuffNaVL_15(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___xuKcWqHPyZZLojTegosyfuffNaVL_15 = value;
		Il2CppCodeGenWriteBarrier((&___xuKcWqHPyZZLojTegosyfuffNaVL_15), value);
	}

	inline static int32_t get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_16() { return static_cast<int32_t>(offsetof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields, ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_xgFcMzbcKwfNhKMwwFcokhPKEXIb_16() const { return ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_16() { return &___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16; }
	inline void set_xgFcMzbcKwfNhKMwwFcokhPKEXIb_16(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16 = value;
		Il2CppCodeGenWriteBarrier((&___xgFcMzbcKwfNhKMwwFcokhPKEXIb_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERHELPER_TBC381D3F373CC57D36553FBA0843F6857C3C8DEC_H
#ifndef CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#define CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.CodeHelper
struct  CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef GQXVLRQJYPPKCLZINKOEPJOQGEY_T98ACE43235F8D25C7B8CC4BF4B4C985F779271ED_H
#define GQXVLRQJYPPKCLZINKOEPJOQGEY_T98ACE43235F8D25C7B8CC4BF4B4C985F779271ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gQxvLRqJyPpkclzINkoEPJOqGeY
struct  gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED  : public RuntimeObject
{
public:
	// System.Int32 gQxvLRqJyPpkclzINkoEPJOqGeY::yEkehjyseCgyriougfgfGvsREsrE
	int32_t ___yEkehjyseCgyriougfgfGvsREsrE_0;
	// System.Int32 gQxvLRqJyPpkclzINkoEPJOqGeY::FZXDSDLUnVXQjaPxhAgaCZLUCiyG
	int32_t ___FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1;
	// System.Boolean gQxvLRqJyPpkclzINkoEPJOqGeY::jxXPzEnjWvXFNyxvWfoRieGVFhL
	bool ___jxXPzEnjWvXFNyxvWfoRieGVFhL_2;

public:
	inline static int32_t get_offset_of_yEkehjyseCgyriougfgfGvsREsrE_0() { return static_cast<int32_t>(offsetof(gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED, ___yEkehjyseCgyriougfgfGvsREsrE_0)); }
	inline int32_t get_yEkehjyseCgyriougfgfGvsREsrE_0() const { return ___yEkehjyseCgyriougfgfGvsREsrE_0; }
	inline int32_t* get_address_of_yEkehjyseCgyriougfgfGvsREsrE_0() { return &___yEkehjyseCgyriougfgfGvsREsrE_0; }
	inline void set_yEkehjyseCgyriougfgfGvsREsrE_0(int32_t value)
	{
		___yEkehjyseCgyriougfgfGvsREsrE_0 = value;
	}

	inline static int32_t get_offset_of_FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1() { return static_cast<int32_t>(offsetof(gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED, ___FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1)); }
	inline int32_t get_FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1() const { return ___FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1; }
	inline int32_t* get_address_of_FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1() { return &___FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1; }
	inline void set_FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1(int32_t value)
	{
		___FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1 = value;
	}

	inline static int32_t get_offset_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_2() { return static_cast<int32_t>(offsetof(gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED, ___jxXPzEnjWvXFNyxvWfoRieGVFhL_2)); }
	inline bool get_jxXPzEnjWvXFNyxvWfoRieGVFhL_2() const { return ___jxXPzEnjWvXFNyxvWfoRieGVFhL_2; }
	inline bool* get_address_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_2() { return &___jxXPzEnjWvXFNyxvWfoRieGVFhL_2; }
	inline void set_jxXPzEnjWvXFNyxvWfoRieGVFhL_2(bool value)
	{
		___jxXPzEnjWvXFNyxvWfoRieGVFhL_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GQXVLRQJYPPKCLZINKOEPJOQGEY_T98ACE43235F8D25C7B8CC4BF4B4C985F779271ED_H
#ifndef JUFVWXONCNQZWWSVXNEDEFLRTLG_T66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5_H
#define JUFVWXONCNQZWWSVXNEDEFLRTLG_T66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// jufVWXOncnQzwwsvxneDefLrtLg
struct  jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5  : public RuntimeObject
{
public:
	// System.Boolean jufVWXOncnQzwwsvxneDefLrtLg::jxXPzEnjWvXFNyxvWfoRieGVFhL
	bool ___jxXPzEnjWvXFNyxvWfoRieGVFhL_0;
	// System.Int32 jufVWXOncnQzwwsvxneDefLrtLg::WHCIpuTfwdHNjfJipsoGkINikLt
	int32_t ___WHCIpuTfwdHNjfJipsoGkINikLt_1;

public:
	inline static int32_t get_offset_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_0() { return static_cast<int32_t>(offsetof(jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5, ___jxXPzEnjWvXFNyxvWfoRieGVFhL_0)); }
	inline bool get_jxXPzEnjWvXFNyxvWfoRieGVFhL_0() const { return ___jxXPzEnjWvXFNyxvWfoRieGVFhL_0; }
	inline bool* get_address_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_0() { return &___jxXPzEnjWvXFNyxvWfoRieGVFhL_0; }
	inline void set_jxXPzEnjWvXFNyxvWfoRieGVFhL_0(bool value)
	{
		___jxXPzEnjWvXFNyxvWfoRieGVFhL_0 = value;
	}

	inline static int32_t get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_1() { return static_cast<int32_t>(offsetof(jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5, ___WHCIpuTfwdHNjfJipsoGkINikLt_1)); }
	inline int32_t get_WHCIpuTfwdHNjfJipsoGkINikLt_1() const { return ___WHCIpuTfwdHNjfJipsoGkINikLt_1; }
	inline int32_t* get_address_of_WHCIpuTfwdHNjfJipsoGkINikLt_1() { return &___WHCIpuTfwdHNjfJipsoGkINikLt_1; }
	inline void set_WHCIpuTfwdHNjfJipsoGkINikLt_1(int32_t value)
	{
		___WHCIpuTfwdHNjfJipsoGkINikLt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUFVWXONCNQZWWSVXNEDEFLRTLG_T66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5_H
#ifndef CUSTOMCONTROLLERMAPSAVEDATA_TEEF313BB062EB2DDA52D216138FF9ED7878DEAB1_H
#define CUSTOMCONTROLLERMAPSAVEDATA_TEEF313BB062EB2DDA52D216138FF9ED7878DEAB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CustomControllerMapSaveData
struct  CustomControllerMapSaveData_tEEF313BB062EB2DDA52D216138FF9ED7878DEAB1  : public ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERMAPSAVEDATA_TEEF313BB062EB2DDA52D216138FF9ED7878DEAB1_H
#ifndef INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#define INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapCategory
struct  InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602  : public InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918
{
public:
	// System.Boolean Rewired.InputMapCategory::_checkConflictsWithAllCategories
	bool ____checkConflictsWithAllCategories_5;
	// System.Collections.Generic.List`1<System.Int32> Rewired.InputMapCategory::_checkConflictsCategoryIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____checkConflictsCategoryIds_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> Rewired.InputMapCategory::_checkConflictsCategoryIds_readOnly
	ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * ____checkConflictsCategoryIds_readOnly_7;

public:
	inline static int32_t get_offset_of__checkConflictsWithAllCategories_5() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsWithAllCategories_5)); }
	inline bool get__checkConflictsWithAllCategories_5() const { return ____checkConflictsWithAllCategories_5; }
	inline bool* get_address_of__checkConflictsWithAllCategories_5() { return &____checkConflictsWithAllCategories_5; }
	inline void set__checkConflictsWithAllCategories_5(bool value)
	{
		____checkConflictsWithAllCategories_5 = value;
	}

	inline static int32_t get_offset_of__checkConflictsCategoryIds_6() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsCategoryIds_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__checkConflictsCategoryIds_6() const { return ____checkConflictsCategoryIds_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__checkConflictsCategoryIds_6() { return &____checkConflictsCategoryIds_6; }
	inline void set__checkConflictsCategoryIds_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____checkConflictsCategoryIds_6 = value;
		Il2CppCodeGenWriteBarrier((&____checkConflictsCategoryIds_6), value);
	}

	inline static int32_t get_offset_of__checkConflictsCategoryIds_readOnly_7() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsCategoryIds_readOnly_7)); }
	inline ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * get__checkConflictsCategoryIds_readOnly_7() const { return ____checkConflictsCategoryIds_readOnly_7; }
	inline ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A ** get_address_of__checkConflictsCategoryIds_readOnly_7() { return &____checkConflictsCategoryIds_readOnly_7; }
	inline void set__checkConflictsCategoryIds_readOnly_7(ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * value)
	{
		____checkConflictsCategoryIds_readOnly_7 = value;
		Il2CppCodeGenWriteBarrier((&____checkConflictsCategoryIds_readOnly_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#ifndef CANCELEDEVENTDATA_TE1D69F86DE424A4F6966140508692822A8C0B07C_H
#define CANCELEDEVENTDATA_TE1D69F86DE424A4F6966140508692822A8C0B07C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_CanceledEventData
struct  CanceledEventData_tE1D69F86DE424A4F6966140508692822A8C0B07C  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:
	// System.String Rewired.InputMapper_CanceledEventData::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(CanceledEventData_tE1D69F86DE424A4F6966140508692822A8C0B07C, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCELEDEVENTDATA_TE1D69F86DE424A4F6966140508692822A8C0B07C_H
#ifndef CONFLICTFOUNDEVENTDATA_TF7B847B50A4A524CBF545F22446C6809656D1966_H
#define CONFLICTFOUNDEVENTDATA_TF7B847B50A4A524CBF545F22446C6809656D1966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_ConflictFoundEventData
struct  ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:
	// System.Action`1<Rewired.InputMapper_ConflictResponse> Rewired.InputMapper_ConflictFoundEventData::responseCallback
	Action_1_tC9795E40AC09CBEE286EFAB4D8DBDA3177E8FB09 * ___responseCallback_1;
	// Rewired.ElementAssignmentInfo Rewired.InputMapper_ConflictFoundEventData::assignment
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF * ___assignment_2;
	// System.Collections.Generic.IList`1<Rewired.ElementAssignmentConflictInfo> Rewired.InputMapper_ConflictFoundEventData::conflicts
	RuntimeObject* ___conflicts_3;
	// System.Boolean Rewired.InputMapper_ConflictFoundEventData::isProtected
	bool ___isProtected_4;

public:
	inline static int32_t get_offset_of_responseCallback_1() { return static_cast<int32_t>(offsetof(ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966, ___responseCallback_1)); }
	inline Action_1_tC9795E40AC09CBEE286EFAB4D8DBDA3177E8FB09 * get_responseCallback_1() const { return ___responseCallback_1; }
	inline Action_1_tC9795E40AC09CBEE286EFAB4D8DBDA3177E8FB09 ** get_address_of_responseCallback_1() { return &___responseCallback_1; }
	inline void set_responseCallback_1(Action_1_tC9795E40AC09CBEE286EFAB4D8DBDA3177E8FB09 * value)
	{
		___responseCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_1), value);
	}

	inline static int32_t get_offset_of_assignment_2() { return static_cast<int32_t>(offsetof(ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966, ___assignment_2)); }
	inline ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF * get_assignment_2() const { return ___assignment_2; }
	inline ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF ** get_address_of_assignment_2() { return &___assignment_2; }
	inline void set_assignment_2(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF * value)
	{
		___assignment_2 = value;
		Il2CppCodeGenWriteBarrier((&___assignment_2), value);
	}

	inline static int32_t get_offset_of_conflicts_3() { return static_cast<int32_t>(offsetof(ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966, ___conflicts_3)); }
	inline RuntimeObject* get_conflicts_3() const { return ___conflicts_3; }
	inline RuntimeObject** get_address_of_conflicts_3() { return &___conflicts_3; }
	inline void set_conflicts_3(RuntimeObject* value)
	{
		___conflicts_3 = value;
		Il2CppCodeGenWriteBarrier((&___conflicts_3), value);
	}

	inline static int32_t get_offset_of_isProtected_4() { return static_cast<int32_t>(offsetof(ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966, ___isProtected_4)); }
	inline bool get_isProtected_4() const { return ___isProtected_4; }
	inline bool* get_address_of_isProtected_4() { return &___isProtected_4; }
	inline void set_isProtected_4(bool value)
	{
		___isProtected_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTFOUNDEVENTDATA_TF7B847B50A4A524CBF545F22446C6809656D1966_H
#ifndef ERROREVENTDATA_TBDF0CB7D300C80F052BF9C210915F16E31DB8518_H
#define ERROREVENTDATA_TBDF0CB7D300C80F052BF9C210915F16E31DB8518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_ErrorEventData
struct  ErrorEventData_tBDF0CB7D300C80F052BF9C210915F16E31DB8518  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:
	// System.String Rewired.InputMapper_ErrorEventData::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(ErrorEventData_tBDF0CB7D300C80F052BF9C210915F16E31DB8518, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTDATA_TBDF0CB7D300C80F052BF9C210915F16E31DB8518_H
#ifndef INPUTMAPPEDEVENTDATA_TCA411F2D75C6E2528E495EC25B99F10347B364EA_H
#define INPUTMAPPEDEVENTDATA_TCA411F2D75C6E2528E495EC25B99F10347B364EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_InputMappedEventData
struct  InputMappedEventData_tCA411F2D75C6E2528E495EC25B99F10347B364EA  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:
	// Rewired.ActionElementMap Rewired.InputMapper_InputMappedEventData::actionElementMap
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___actionElementMap_1;

public:
	inline static int32_t get_offset_of_actionElementMap_1() { return static_cast<int32_t>(offsetof(InputMappedEventData_tCA411F2D75C6E2528E495EC25B99F10347B364EA, ___actionElementMap_1)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_actionElementMap_1() const { return ___actionElementMap_1; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_actionElementMap_1() { return &___actionElementMap_1; }
	inline void set_actionElementMap_1(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___actionElementMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionElementMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPPEDEVENTDATA_TCA411F2D75C6E2528E495EC25B99F10347B364EA_H
#ifndef STARTEDEVENTDATA_T796FB80A1C2E105C239083DFB676BAF58114C3BE_H
#define STARTEDEVENTDATA_T796FB80A1C2E105C239083DFB676BAF58114C3BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_StartedEventData
struct  StartedEventData_t796FB80A1C2E105C239083DFB676BAF58114C3BE  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTEDEVENTDATA_T796FB80A1C2E105C239083DFB676BAF58114C3BE_H
#ifndef STOPPEDEVENTDATA_T1261F5DA0E3B39D19C547DAD150E163DA9A521C3_H
#define STOPPEDEVENTDATA_T1261F5DA0E3B39D19C547DAD150E163DA9A521C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_StoppedEventData
struct  StoppedEventData_t1261F5DA0E3B39D19C547DAD150E163DA9A521C3  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPPEDEVENTDATA_T1261F5DA0E3B39D19C547DAD150E163DA9A521C3_H
#ifndef TIMEDOUTEVENTDATA_TFC034B3F052C062A38BD91E6A3D8FB4A068B2DA5_H
#define TIMEDOUTEVENTDATA_TFC034B3F052C062A38BD91E6A3D8FB4A068B2DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_TimedOutEventData
struct  TimedOutEventData_tFC034B3F052C062A38BD91E6A3D8FB4A068B2DA5  : public EventData_tE4C486A467E7CB80A9F88B04456F63263515B552
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOUTEVENTDATA_TFC034B3F052C062A38BD91E6A3D8FB4A068B2DA5_H
#ifndef JOYSTICKMAPSAVEDATA_T9968968EDD66FFE5E773ADDCD8649078DCF6ED4F_H
#define JOYSTICKMAPSAVEDATA_T9968968EDD66FFE5E773ADDCD8649078DCF6ED4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.JoystickMapSaveData
struct  JoystickMapSaveData_t9968968EDD66FFE5E773ADDCD8649078DCF6ED4F  : public ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMAPSAVEDATA_T9968968EDD66FFE5E773ADDCD8649078DCF6ED4F_H
#ifndef KEYBOARDMAPSAVEDATA_T337FD1281183EE41385ED18BE003AAC70261B97C_H
#define KEYBOARDMAPSAVEDATA_T337FD1281183EE41385ED18BE003AAC70261B97C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.KeyboardMapSaveData
struct  KeyboardMapSaveData_t337FD1281183EE41385ED18BE003AAC70261B97C  : public ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDMAPSAVEDATA_T337FD1281183EE41385ED18BE003AAC70261B97C_H
#ifndef MOUSEMAPSAVEDATA_T2A02E2F271B2011AA7C4CB8C9ECCF52468B09CD3_H
#define MOUSEMAPSAVEDATA_T2A02E2F271B2011AA7C4CB8C9ECCF52468B09CD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseMapSaveData
struct  MouseMapSaveData_t2A02E2F271B2011AA7C4CB8C9ECCF52468B09CD3  : public ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEMAPSAVEDATA_T2A02E2F271B2011AA7C4CB8C9ECCF52468B09CD3_H
#ifndef CONFLICTCHECKINGHELPER_T3B5BA120121109EB2736D5466BA25742F049F1B1_H
#define CONFLICTCHECKINGHELPER_T3B5BA120121109EB2736D5466BA25742F049F1B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper
struct  ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:
	// Rewired.Player Rewired.Player_ControllerHelper_ConflictCheckingHelper::DelHerRPxmEUeLMSQbMNIPccOrs
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___DelHerRPxmEUeLMSQbMNIPccOrs_0;
	// Rewired.Player_ControllerHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper::vRRcfyGudPCBcdddZEsLFGidKVD
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * ___vRRcfyGudPCBcdddZEsLFGidKVD_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2;

public:
	inline static int32_t get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_0() { return static_cast<int32_t>(offsetof(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1, ___DelHerRPxmEUeLMSQbMNIPccOrs_0)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_DelHerRPxmEUeLMSQbMNIPccOrs_0() const { return ___DelHerRPxmEUeLMSQbMNIPccOrs_0; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_DelHerRPxmEUeLMSQbMNIPccOrs_0() { return &___DelHerRPxmEUeLMSQbMNIPccOrs_0; }
	inline void set_DelHerRPxmEUeLMSQbMNIPccOrs_0(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___DelHerRPxmEUeLMSQbMNIPccOrs_0 = value;
		Il2CppCodeGenWriteBarrier((&___DelHerRPxmEUeLMSQbMNIPccOrs_0), value);
	}

	inline static int32_t get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_1() { return static_cast<int32_t>(offsetof(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1, ___vRRcfyGudPCBcdddZEsLFGidKVD_1)); }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * get_vRRcfyGudPCBcdddZEsLFGidKVD_1() const { return ___vRRcfyGudPCBcdddZEsLFGidKVD_1; }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC ** get_address_of_vRRcfyGudPCBcdddZEsLFGidKVD_1() { return &___vRRcfyGudPCBcdddZEsLFGidKVD_1; }
	inline void set_vRRcfyGudPCBcdddZEsLFGidKVD_1(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * value)
	{
		___vRRcfyGudPCBcdddZEsLFGidKVD_1 = value;
		Il2CppCodeGenWriteBarrier((&___vRRcfyGudPCBcdddZEsLFGidKVD_1), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return static_cast<int32_t>(offsetof(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTCHECKINGHELPER_T3B5BA120121109EB2736D5466BA25742F049F1B1_H
#ifndef PLAYERSAVEDATA_T9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F_H
#define PLAYERSAVEDATA_T9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerSaveData
struct  PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F 
{
public:
	// Rewired.JoystickMapSaveData[] Rewired.PlayerSaveData::tYkPhHKJbyjvvknVLlMKJTLiOaz
	JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63* ___tYkPhHKJbyjvvknVLlMKJTLiOaz_0;
	// Rewired.KeyboardMapSaveData[] Rewired.PlayerSaveData::JDGDYzdeEzvHIVUZerqzkiMsMiFn
	KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95* ___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1;
	// Rewired.MouseMapSaveData[] Rewired.PlayerSaveData::bYZGQoTMhLQpyPqIskzvubOJRuj
	MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921* ___bYZGQoTMhLQpyPqIskzvubOJRuj_2;
	// Rewired.CustomControllerMapSaveData[] Rewired.PlayerSaveData::zpGetsBoICokKMfkMcJbuggxTfXW
	CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52* ___zpGetsBoICokKMfkMcJbuggxTfXW_3;
	// Rewired.InputBehavior[] Rewired.PlayerSaveData::AVCVdipomjByHRQVMJMnJzrNKqH
	InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A* ___AVCVdipomjByHRQVMJMnJzrNKqH_4;

public:
	inline static int32_t get_offset_of_tYkPhHKJbyjvvknVLlMKJTLiOaz_0() { return static_cast<int32_t>(offsetof(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F, ___tYkPhHKJbyjvvknVLlMKJTLiOaz_0)); }
	inline JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63* get_tYkPhHKJbyjvvknVLlMKJTLiOaz_0() const { return ___tYkPhHKJbyjvvknVLlMKJTLiOaz_0; }
	inline JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63** get_address_of_tYkPhHKJbyjvvknVLlMKJTLiOaz_0() { return &___tYkPhHKJbyjvvknVLlMKJTLiOaz_0; }
	inline void set_tYkPhHKJbyjvvknVLlMKJTLiOaz_0(JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63* value)
	{
		___tYkPhHKJbyjvvknVLlMKJTLiOaz_0 = value;
		Il2CppCodeGenWriteBarrier((&___tYkPhHKJbyjvvknVLlMKJTLiOaz_0), value);
	}

	inline static int32_t get_offset_of_JDGDYzdeEzvHIVUZerqzkiMsMiFn_1() { return static_cast<int32_t>(offsetof(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F, ___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1)); }
	inline KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95* get_JDGDYzdeEzvHIVUZerqzkiMsMiFn_1() const { return ___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1; }
	inline KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95** get_address_of_JDGDYzdeEzvHIVUZerqzkiMsMiFn_1() { return &___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1; }
	inline void set_JDGDYzdeEzvHIVUZerqzkiMsMiFn_1(KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95* value)
	{
		___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1 = value;
		Il2CppCodeGenWriteBarrier((&___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1), value);
	}

	inline static int32_t get_offset_of_bYZGQoTMhLQpyPqIskzvubOJRuj_2() { return static_cast<int32_t>(offsetof(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F, ___bYZGQoTMhLQpyPqIskzvubOJRuj_2)); }
	inline MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921* get_bYZGQoTMhLQpyPqIskzvubOJRuj_2() const { return ___bYZGQoTMhLQpyPqIskzvubOJRuj_2; }
	inline MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921** get_address_of_bYZGQoTMhLQpyPqIskzvubOJRuj_2() { return &___bYZGQoTMhLQpyPqIskzvubOJRuj_2; }
	inline void set_bYZGQoTMhLQpyPqIskzvubOJRuj_2(MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921* value)
	{
		___bYZGQoTMhLQpyPqIskzvubOJRuj_2 = value;
		Il2CppCodeGenWriteBarrier((&___bYZGQoTMhLQpyPqIskzvubOJRuj_2), value);
	}

	inline static int32_t get_offset_of_zpGetsBoICokKMfkMcJbuggxTfXW_3() { return static_cast<int32_t>(offsetof(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F, ___zpGetsBoICokKMfkMcJbuggxTfXW_3)); }
	inline CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52* get_zpGetsBoICokKMfkMcJbuggxTfXW_3() const { return ___zpGetsBoICokKMfkMcJbuggxTfXW_3; }
	inline CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52** get_address_of_zpGetsBoICokKMfkMcJbuggxTfXW_3() { return &___zpGetsBoICokKMfkMcJbuggxTfXW_3; }
	inline void set_zpGetsBoICokKMfkMcJbuggxTfXW_3(CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52* value)
	{
		___zpGetsBoICokKMfkMcJbuggxTfXW_3 = value;
		Il2CppCodeGenWriteBarrier((&___zpGetsBoICokKMfkMcJbuggxTfXW_3), value);
	}

	inline static int32_t get_offset_of_AVCVdipomjByHRQVMJMnJzrNKqH_4() { return static_cast<int32_t>(offsetof(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F, ___AVCVdipomjByHRQVMJMnJzrNKqH_4)); }
	inline InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A* get_AVCVdipomjByHRQVMJMnJzrNKqH_4() const { return ___AVCVdipomjByHRQVMJMnJzrNKqH_4; }
	inline InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A** get_address_of_AVCVdipomjByHRQVMJMnJzrNKqH_4() { return &___AVCVdipomjByHRQVMJMnJzrNKqH_4; }
	inline void set_AVCVdipomjByHRQVMJMnJzrNKqH_4(InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A* value)
	{
		___AVCVdipomjByHRQVMJMnJzrNKqH_4 = value;
		Il2CppCodeGenWriteBarrier((&___AVCVdipomjByHRQVMJMnJzrNKqH_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.PlayerSaveData
struct PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F_marshaled_pinvoke
{
	JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63* ___tYkPhHKJbyjvvknVLlMKJTLiOaz_0;
	KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95* ___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1;
	MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921* ___bYZGQoTMhLQpyPqIskzvubOJRuj_2;
	CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52* ___zpGetsBoICokKMfkMcJbuggxTfXW_3;
	InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A* ___AVCVdipomjByHRQVMJMnJzrNKqH_4;
};
// Native definition for COM marshalling of Rewired.PlayerSaveData
struct PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F_marshaled_com
{
	JoystickMapSaveDataU5BU5D_tAD212C37F27E823DC446C79897B4667EC061AF63* ___tYkPhHKJbyjvvknVLlMKJTLiOaz_0;
	KeyboardMapSaveDataU5BU5D_tB974A8E32C702ABFF025004911669FBCE4F0ED95* ___JDGDYzdeEzvHIVUZerqzkiMsMiFn_1;
	MouseMapSaveDataU5BU5D_tDA52FB98EC2CC7C5B31535792D1D68C6CCAC7921* ___bYZGQoTMhLQpyPqIskzvubOJRuj_2;
	CustomControllerMapSaveDataU5BU5D_tACA0D51753FCFD28B8FDDB83AEE6D49C21411F52* ___zpGetsBoICokKMfkMcJbuggxTfXW_3;
	InputBehaviorU5BU5D_t716F403F6183D3E568690DD2138399AEF530F78A* ___AVCVdipomjByHRQVMJMnJzrNKqH_4;
};
#endif // PLAYERSAVEDATA_T9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F_H
#ifndef ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#define ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<Rewired.ActionElementMap>
struct  Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___list_0)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_list_0() const { return ___list_0; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___current_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_current_3() const { return ___current_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef AXADWPFEENGNQLRKNSRCWEWUEQG_TF7543DB5C6FF42440814E3E193219EA786038F8B_H
#define AXADWPFEENGNQLRKNSRCWEWUEQG_TF7543DB5C6FF42440814E3E193219EA786038F8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg
struct  AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.IControllerElementTarget Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::qJByvTGLorKeLizFyjtftOInyVr
	RuntimeObject* ___qJByvTGLorKeLizFyjtftOInyVr_4;
	// Rewired.IControllerElementTarget Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::SanVLbjjVuCJGdzcsHmJAMmULDaN
	RuntimeObject* ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5;
	// System.Boolean Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.Utils.TempListPool_TList`1<Rewired.ActionElementMap> Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::JJBazOAoLDIHfJdiHCVNlEXQFdst
	TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * ___JJBazOAoLDIHfJdiHCVNlEXQFdst_8;
	// System.Collections.Generic.List`1<Rewired.ActionElementMap> Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::koIGbWtOszkBfeKznqEsjxuJIpw
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___koIGbWtOszkBfeKznqEsjxuJIpw_9;
	// System.Boolean Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::jUlKXGncYHGIVAuxoeAJucdDuGjq
	bool ___jUlKXGncYHGIVAuxoeAJucdDuGjq_10;
	// Rewired.ActionElementMap Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::rXLKfDaDLNcGSyuYNDUiezDBrce
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___rXLKfDaDLNcGSyuYNDUiezDBrce_11;
	// System.Collections.Generic.List`1_Enumerator<Rewired.ActionElementMap> Rewired.ControllerMap_AXADwpfEENgNqLrkNSrcweWuEqg::BPiRbabceIVCHhjEQJeLmAhEVFW
	Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  ___BPiRbabceIVCHhjEQJeLmAhEVFW_12;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___qJByvTGLorKeLizFyjtftOInyVr_4)); }
	inline RuntimeObject* get_qJByvTGLorKeLizFyjtftOInyVr_4() const { return ___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline RuntimeObject** get_address_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return &___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline void set_qJByvTGLorKeLizFyjtftOInyVr_4(RuntimeObject* value)
	{
		___qJByvTGLorKeLizFyjtftOInyVr_4 = value;
		Il2CppCodeGenWriteBarrier((&___qJByvTGLorKeLizFyjtftOInyVr_4), value);
	}

	inline static int32_t get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5)); }
	inline RuntimeObject* get_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() const { return ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline RuntimeObject** get_address_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return &___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline void set_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(RuntimeObject* value)
	{
		___SanVLbjjVuCJGdzcsHmJAMmULDaN_5 = value;
		Il2CppCodeGenWriteBarrier((&___SanVLbjjVuCJGdzcsHmJAMmULDaN_5), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_JJBazOAoLDIHfJdiHCVNlEXQFdst_8() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___JJBazOAoLDIHfJdiHCVNlEXQFdst_8)); }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * get_JJBazOAoLDIHfJdiHCVNlEXQFdst_8() const { return ___JJBazOAoLDIHfJdiHCVNlEXQFdst_8; }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA ** get_address_of_JJBazOAoLDIHfJdiHCVNlEXQFdst_8() { return &___JJBazOAoLDIHfJdiHCVNlEXQFdst_8; }
	inline void set_JJBazOAoLDIHfJdiHCVNlEXQFdst_8(TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * value)
	{
		___JJBazOAoLDIHfJdiHCVNlEXQFdst_8 = value;
		Il2CppCodeGenWriteBarrier((&___JJBazOAoLDIHfJdiHCVNlEXQFdst_8), value);
	}

	inline static int32_t get_offset_of_koIGbWtOszkBfeKznqEsjxuJIpw_9() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___koIGbWtOszkBfeKznqEsjxuJIpw_9)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_koIGbWtOszkBfeKznqEsjxuJIpw_9() const { return ___koIGbWtOszkBfeKznqEsjxuJIpw_9; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_koIGbWtOszkBfeKznqEsjxuJIpw_9() { return &___koIGbWtOszkBfeKznqEsjxuJIpw_9; }
	inline void set_koIGbWtOszkBfeKznqEsjxuJIpw_9(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___koIGbWtOszkBfeKznqEsjxuJIpw_9 = value;
		Il2CppCodeGenWriteBarrier((&___koIGbWtOszkBfeKznqEsjxuJIpw_9), value);
	}

	inline static int32_t get_offset_of_jUlKXGncYHGIVAuxoeAJucdDuGjq_10() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___jUlKXGncYHGIVAuxoeAJucdDuGjq_10)); }
	inline bool get_jUlKXGncYHGIVAuxoeAJucdDuGjq_10() const { return ___jUlKXGncYHGIVAuxoeAJucdDuGjq_10; }
	inline bool* get_address_of_jUlKXGncYHGIVAuxoeAJucdDuGjq_10() { return &___jUlKXGncYHGIVAuxoeAJucdDuGjq_10; }
	inline void set_jUlKXGncYHGIVAuxoeAJucdDuGjq_10(bool value)
	{
		___jUlKXGncYHGIVAuxoeAJucdDuGjq_10 = value;
	}

	inline static int32_t get_offset_of_rXLKfDaDLNcGSyuYNDUiezDBrce_11() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___rXLKfDaDLNcGSyuYNDUiezDBrce_11)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_rXLKfDaDLNcGSyuYNDUiezDBrce_11() const { return ___rXLKfDaDLNcGSyuYNDUiezDBrce_11; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_rXLKfDaDLNcGSyuYNDUiezDBrce_11() { return &___rXLKfDaDLNcGSyuYNDUiezDBrce_11; }
	inline void set_rXLKfDaDLNcGSyuYNDUiezDBrce_11(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___rXLKfDaDLNcGSyuYNDUiezDBrce_11 = value;
		Il2CppCodeGenWriteBarrier((&___rXLKfDaDLNcGSyuYNDUiezDBrce_11), value);
	}

	inline static int32_t get_offset_of_BPiRbabceIVCHhjEQJeLmAhEVFW_12() { return static_cast<int32_t>(offsetof(AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B, ___BPiRbabceIVCHhjEQJeLmAhEVFW_12)); }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  get_BPiRbabceIVCHhjEQJeLmAhEVFW_12() const { return ___BPiRbabceIVCHhjEQJeLmAhEVFW_12; }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC * get_address_of_BPiRbabceIVCHhjEQJeLmAhEVFW_12() { return &___BPiRbabceIVCHhjEQJeLmAhEVFW_12; }
	inline void set_BPiRbabceIVCHhjEQJeLmAhEVFW_12(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  value)
	{
		___BPiRbabceIVCHhjEQJeLmAhEVFW_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXADWPFEENGNQLRKNSRCWEWUEQG_TF7543DB5C6FF42440814E3E193219EA786038F8B_H
#ifndef KVPRZTGTJVHMYMPPRFCEQJHIVOU_T6A5549123B4FFA86C0387E371749414BCF63C03B_H
#define KVPRZTGTJVHMYMPPRFCEQJHIVOU_T6A5549123B4FFA86C0387E371749414BCF63C03B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou
struct  KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.IControllerElementTarget Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::qJByvTGLorKeLizFyjtftOInyVr
	RuntimeObject* ___qJByvTGLorKeLizFyjtftOInyVr_4;
	// Rewired.IControllerElementTarget Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::SanVLbjjVuCJGdzcsHmJAMmULDaN
	RuntimeObject* ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5;
	// System.Int32 Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_6;
	// System.Int32 Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7;
	// System.Boolean Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// Rewired.Utils.TempListPool_TList`1<Rewired.ActionElementMap> Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::UzrYOiFDqERLNuDgbvIJpXnzUyp
	TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * ___UzrYOiFDqERLNuDgbvIJpXnzUyp_10;
	// System.Collections.Generic.List`1<Rewired.ActionElementMap> Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::ryBcAjrEEpazQiPuJlMNICDBkCMM
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___ryBcAjrEEpazQiPuJlMNICDBkCMM_11;
	// System.Boolean Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::zWZVzRewMrLfsDjRNOncamEtigT
	bool ___zWZVzRewMrLfsDjRNOncamEtigT_12;
	// Rewired.ActionElementMap Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::ZEiZXagZaJDLOvzqmSYCjDHuPPr
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13;
	// System.Collections.Generic.List`1_Enumerator<Rewired.ActionElementMap> Rewired.ControllerMap_KvprztGTJVHMYmpprfceQJHIVou::EAYguexQSuaDQQiXPdpMdLgDqhRg
	Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  ___EAYguexQSuaDQQiXPdpMdLgDqhRg_14;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___qJByvTGLorKeLizFyjtftOInyVr_4)); }
	inline RuntimeObject* get_qJByvTGLorKeLizFyjtftOInyVr_4() const { return ___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline RuntimeObject** get_address_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return &___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline void set_qJByvTGLorKeLizFyjtftOInyVr_4(RuntimeObject* value)
	{
		___qJByvTGLorKeLizFyjtftOInyVr_4 = value;
		Il2CppCodeGenWriteBarrier((&___qJByvTGLorKeLizFyjtftOInyVr_4), value);
	}

	inline static int32_t get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5)); }
	inline RuntimeObject* get_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() const { return ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline RuntimeObject** get_address_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return &___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline void set_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(RuntimeObject* value)
	{
		___SanVLbjjVuCJGdzcsHmJAMmULDaN_5 = value;
		Il2CppCodeGenWriteBarrier((&___SanVLbjjVuCJGdzcsHmJAMmULDaN_5), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___pTVJDeVeJlQThAZkFztHqDAWDUa_6)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_6() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_6(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_6 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_UzrYOiFDqERLNuDgbvIJpXnzUyp_10() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___UzrYOiFDqERLNuDgbvIJpXnzUyp_10)); }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * get_UzrYOiFDqERLNuDgbvIJpXnzUyp_10() const { return ___UzrYOiFDqERLNuDgbvIJpXnzUyp_10; }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA ** get_address_of_UzrYOiFDqERLNuDgbvIJpXnzUyp_10() { return &___UzrYOiFDqERLNuDgbvIJpXnzUyp_10; }
	inline void set_UzrYOiFDqERLNuDgbvIJpXnzUyp_10(TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * value)
	{
		___UzrYOiFDqERLNuDgbvIJpXnzUyp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UzrYOiFDqERLNuDgbvIJpXnzUyp_10), value);
	}

	inline static int32_t get_offset_of_ryBcAjrEEpazQiPuJlMNICDBkCMM_11() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___ryBcAjrEEpazQiPuJlMNICDBkCMM_11)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_ryBcAjrEEpazQiPuJlMNICDBkCMM_11() const { return ___ryBcAjrEEpazQiPuJlMNICDBkCMM_11; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_ryBcAjrEEpazQiPuJlMNICDBkCMM_11() { return &___ryBcAjrEEpazQiPuJlMNICDBkCMM_11; }
	inline void set_ryBcAjrEEpazQiPuJlMNICDBkCMM_11(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___ryBcAjrEEpazQiPuJlMNICDBkCMM_11 = value;
		Il2CppCodeGenWriteBarrier((&___ryBcAjrEEpazQiPuJlMNICDBkCMM_11), value);
	}

	inline static int32_t get_offset_of_zWZVzRewMrLfsDjRNOncamEtigT_12() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___zWZVzRewMrLfsDjRNOncamEtigT_12)); }
	inline bool get_zWZVzRewMrLfsDjRNOncamEtigT_12() const { return ___zWZVzRewMrLfsDjRNOncamEtigT_12; }
	inline bool* get_address_of_zWZVzRewMrLfsDjRNOncamEtigT_12() { return &___zWZVzRewMrLfsDjRNOncamEtigT_12; }
	inline void set_zWZVzRewMrLfsDjRNOncamEtigT_12(bool value)
	{
		___zWZVzRewMrLfsDjRNOncamEtigT_12 = value;
	}

	inline static int32_t get_offset_of_ZEiZXagZaJDLOvzqmSYCjDHuPPr_13() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ZEiZXagZaJDLOvzqmSYCjDHuPPr_13() const { return ___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ZEiZXagZaJDLOvzqmSYCjDHuPPr_13() { return &___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13; }
	inline void set_ZEiZXagZaJDLOvzqmSYCjDHuPPr_13(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13 = value;
		Il2CppCodeGenWriteBarrier((&___ZEiZXagZaJDLOvzqmSYCjDHuPPr_13), value);
	}

	inline static int32_t get_offset_of_EAYguexQSuaDQQiXPdpMdLgDqhRg_14() { return static_cast<int32_t>(offsetof(KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B, ___EAYguexQSuaDQQiXPdpMdLgDqhRg_14)); }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  get_EAYguexQSuaDQQiXPdpMdLgDqhRg_14() const { return ___EAYguexQSuaDQQiXPdpMdLgDqhRg_14; }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC * get_address_of_EAYguexQSuaDQQiXPdpMdLgDqhRg_14() { return &___EAYguexQSuaDQQiXPdpMdLgDqhRg_14; }
	inline void set_EAYguexQSuaDQQiXPdpMdLgDqhRg_14(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  value)
	{
		___EAYguexQSuaDQQiXPdpMdLgDqhRg_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KVPRZTGTJVHMYMPPRFCEQJHIVOU_T6A5549123B4FFA86C0387E371749414BCF63C03B_H
#ifndef TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#define TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerSetSelector_Type
struct  Type_t182FB43AE32FF984996E805320E1D5244BCF5887 
{
public:
	// System.Int32 Rewired.ControllerSetSelector_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t182FB43AE32FF984996E805320E1D5244BCF5887, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#define INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionType
struct  InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4 
{
public:
	// System.Int32 Rewired.InputActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionType_t4764596952F35642172A92C2B52AF61BF19955A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTIONTYPE_T4764596952F35642172A92C2B52AF61BF19955A4_H
#ifndef CONFLICTRESPONSE_TD282F0812934B4E73323CF33596FAA52653D8709_H
#define CONFLICTRESPONSE_TD282F0812934B4E73323CF33596FAA52653D8709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_ConflictResponse
struct  ConflictResponse_tD282F0812934B4E73323CF33596FAA52653D8709 
{
public:
	// System.Int32 Rewired.InputMapper_ConflictResponse::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConflictResponse_tD282F0812934B4E73323CF33596FAA52653D8709, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTRESPONSE_TD282F0812934B4E73323CF33596FAA52653D8709_H
#ifndef STATUS_T5F20D760EC07EDF6D070B7873DEC0BF0DC62563F_H
#define STATUS_T5F20D760EC07EDF6D070B7873DEC0BF0DC62563F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_Status
struct  Status_t5F20D760EC07EDF6D070B7873DEC0BF0DC62563F 
{
public:
	// System.Int32 Rewired.InputMapper_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t5F20D760EC07EDF6D070B7873DEC0BF0DC62563F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T5F20D760EC07EDF6D070B7873DEC0BF0DC62563F_H
#ifndef HPATYHNCSBXYOSINVTOZDQCJCGH_TA2328BA9F113B56E8C0B0AE2CEF2E380242996B0_H
#define HPATYHNCSBXYOSINVTOZDQCJCGH_TA2328BA9F113B56E8C0B0AE2CEF2E380242996B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_HpatyHNcsbxyoSINVtOzDqCJcGH
struct  HpatyHNcsbxyoSINVtOzDqCJcGH_tA2328BA9F113B56E8C0B0AE2CEF2E380242996B0 
{
public:
	// System.Int32 Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_HpatyHNcsbxyoSINVtOzDqCJcGH::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HpatyHNcsbxyoSINVtOzDqCJcGH_tA2328BA9F113B56E8C0B0AE2CEF2E380242996B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPATYHNCSBXYOSINVTOZDQCJCGH_TA2328BA9F113B56E8C0B0AE2CEF2E380242996B0_H
#ifndef KRECKFGHZUOVNGGERKWYGMZZOGZ_T3627C5CD6FC8269D93222BD7AB0B9B946605FB27_H
#define KRECKFGHZUOVNGGERKWYGMZZOGZ_T3627C5CD6FC8269D93222BD7AB0B9B946605FB27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_kRecKFGHzUOVnGGERKWYGMZzoGz
struct  kRecKFGHzUOVnGGERKWYGMZzoGz_t3627C5CD6FC8269D93222BD7AB0B9B946605FB27 
{
public:
	// System.Int32 Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_kRecKFGHzUOVnGGERKWYGMZzoGz::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(kRecKFGHzUOVnGGERKWYGMZzoGz_t3627C5CD6FC8269D93222BD7AB0B9B946605FB27, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KRECKFGHZUOVNGGERKWYGMZZOGZ_T3627C5CD6FC8269D93222BD7AB0B9B946605FB27_H
#ifndef BRIXQXKPYAKMWXGQKREYPWDHWSQ_T3A3777B27092A6D39B44AE34783B57673FE32429_H
#define BRIXQXKPYAKMWXGQKREYPWDHWSQ_T3A3777B27092A6D39B44AE34783B57673FE32429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_brixqxKPyakMwXgqKReyPWdHwsQ
struct  brixqxKPyakMwXgqKReyPWdHwsQ_t3A3777B27092A6D39B44AE34783B57673FE32429 
{
public:
	// System.Int32 Rewired.InputMapper_brixqxKPyakMwXgqKReyPWdHwsQ::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(brixqxKPyakMwXgqKReyPWdHwsQ_t3A3777B27092A6D39B44AE34783B57673FE32429, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIXQXKPYAKMWXGQKREYPWDHWSQ_T3A3777B27092A6D39B44AE34783B57673FE32429_H
#ifndef INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#define INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputPlatform
struct  InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E 
{
public:
	// System.Int32 Rewired.InputPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#define MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseOtherAxisMode
struct  MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569 
{
public:
	// System.Int32 Rewired.MouseOtherAxisMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseOtherAxisMode_t71C4BA7F3213B0A84820756481AED35558E79569, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEOTHERAXISMODE_T71C4BA7F3213B0A84820756481AED35558E79569_H
#ifndef MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#define MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseXYAxisDeltaCalc
struct  MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB 
{
public:
	// System.Int32 Rewired.MouseXYAxisDeltaCalc::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseXYAxisDeltaCalc_tFC5AA9BA05C99356CD46AE9C702CA697AF1584AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEXYAXISDELTACALC_TFC5AA9BA05C99356CD46AE9C702CA697AF1584AB_H
#ifndef MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#define MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseXYAxisMode
struct  MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2 
{
public:
	// System.Int32 Rewired.MouseXYAxisMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseXYAxisMode_t0EDC346384E46C57D21A119FBE60AD53A5EFBAE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEXYAXISMODE_T0EDC346384E46C57D21A119FBE60AD53A5EFBAE2_H
#ifndef DIRECTINPUTAXIS_TC8458C206C2EDEE13DE4172B8E19AE815598EFBB_H
#define DIRECTINPUTAXIS_TC8458C206C2EDEE13DE4172B8E19AE815598EFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.DirectInputAxis
struct  DirectInputAxis_tC8458C206C2EDEE13DE4172B8E19AE815598EFBB 
{
public:
	// System.Int32 Rewired.Platforms.DirectInputAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectInputAxis_tC8458C206C2EDEE13DE4172B8E19AE815598EFBB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTINPUTAXIS_TC8458C206C2EDEE13DE4172B8E19AE815598EFBB_H
#ifndef EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#define EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.EditorPlatform
struct  EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC 
{
public:
	// System.Int32 Rewired.Platforms.EditorPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifndef LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#define LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.LinuxStandalonePrimaryInputSource
struct  LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7 
{
public:
	// System.Int32 Rewired.Platforms.LinuxStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#ifndef OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#define OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.OSXAxis
struct  OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390 
{
public:
	// System.Int32 Rewired.Platforms.OSXAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#ifndef OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#define OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.OSXStandalonePrimaryInputSource
struct  OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1 
{
public:
	// System.Int32 Rewired.Platforms.OSXStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#ifndef PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#define PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4PrimaryInputSource
struct  PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA 
{
public:
	// System.Int32 Rewired.Platforms.PS4PrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef RAWINPUTAXIS_T6D0B3C134A3AA6DEB63506F12A711EE09190D71A_H
#define RAWINPUTAXIS_T6D0B3C134A3AA6DEB63506F12A711EE09190D71A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.RawInputAxis
struct  RawInputAxis_t6D0B3C134A3AA6DEB63506F12A711EE09190D71A 
{
public:
	// System.Int32 Rewired.Platforms.RawInputAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RawInputAxis_t6D0B3C134A3AA6DEB63506F12A711EE09190D71A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWINPUTAXIS_T6D0B3C134A3AA6DEB63506F12A711EE09190D71A_H
#ifndef SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#define SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingAPILevel
struct  ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingAPILevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifndef SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#define SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingBackend
struct  ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingBackend::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifndef UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#define UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.UnityAxis
struct  UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4 
{
public:
	// System.Int32 Rewired.Platforms.UnityAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#ifndef UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#define UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.UnityButton
struct  UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E 
{
public:
	// System.Int32 Rewired.Platforms.UnityButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#ifndef WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#define WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLGamepadMappingType
struct  WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854 
{
public:
	// System.Int32 Rewired.Platforms.WebGLGamepadMappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLGAMEPADMAPPINGTYPE_T6261DAA3DB60527AB5E670A22C5A75F0F95B0854_H
#ifndef WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#define WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLOSType
struct  WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939 
{
public:
	// System.Int32 Rewired.Platforms.WebGLOSType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLOSTYPE_T23F15ECBA95C71FB57068082F24204CA5C38B939_H
#ifndef WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#define WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLPrimaryInputSource
struct  WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07 
{
public:
	// System.Int32 Rewired.Platforms.WebGLPrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#ifndef WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#define WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLWebBrowserType
struct  WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45 
{
public:
	// System.Int32 Rewired.Platforms.WebGLWebBrowserType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLWEBBROWSERTYPE_T4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45_H
#ifndef WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#define WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebplayerPlatform
struct  WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82 
{
public:
	// System.Int32 Rewired.Platforms.WebplayerPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifndef WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#define WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WindowsStandalonePrimaryInputSource
struct  WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54 
{
public:
	// System.Int32 Rewired.Platforms.WindowsStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#ifndef WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#define WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WindowsUWPPrimaryInputSource
struct  WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66 
{
public:
	// System.Int32 Rewired.Platforms.WindowsUWPPrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#ifndef XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#define XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputAxis
struct  XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F 
{
public:
	// System.Int32 Rewired.Platforms.XInputAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#ifndef XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#define XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputButton
struct  XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6 
{
public:
	// System.Int32 Rewired.Platforms.XInputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#ifndef XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#define XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputDeviceSubType
struct  XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82 
{
public:
	// System.Int32 Rewired.Platforms.XInputDeviceSubType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTDEVICESUBTYPE_T0AA28279AC0024D8FF2C82D8F9E6F15B67980E82_H
#ifndef XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#define XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XboxOnePrimaryInputSource
struct  XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E 
{
public:
	// System.Int32 Rewired.Platforms.XboxOnePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#ifndef YUSQPKUJMBYOSHDSGGXSBFWSYIYL_TEC6CE14C4DE8B15ADF0A433E294775B1531587A8_H
#define YUSQPKUJMBYOSHDSGGXSBFWSYIYL_TEC6CE14C4DE8B15ADF0A433E294775B1531587A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL
struct  yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8  : public RuntimeObject
{
public:
	// Rewired.ControllerMapSaveData Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.PlayerSaveData Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.PlayerSaveData Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::BBonIgYmbXiDXVdBLuciuUHykHc
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  ___BBonIgYmbXiDXVdBLuciuUHykHc_4;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_5;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::bKJattiDCgqUafgGgzBoXnBGAIWb
	int32_t ___bKJattiDCgqUafgGgzBoXnBGAIWb_6;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::vyUAWHyQzNBXmJvIdvpaOQOqBFQk
	int32_t ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7;
	// System.Int32 Rewired.PlayerSaveData_yUsqPkUjmbyOsHDSGGXSBFwSyIYL::JWGPoriiedknEFskVzNUGNUwScr
	int32_t ___JWGPoriiedknEFskVzNUGNUwScr_8;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F * get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
	}

	inline static int32_t get_offset_of_BBonIgYmbXiDXVdBLuciuUHykHc_4() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___BBonIgYmbXiDXVdBLuciuUHykHc_4)); }
	inline PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  get_BBonIgYmbXiDXVdBLuciuUHykHc_4() const { return ___BBonIgYmbXiDXVdBLuciuUHykHc_4; }
	inline PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F * get_address_of_BBonIgYmbXiDXVdBLuciuUHykHc_4() { return &___BBonIgYmbXiDXVdBLuciuUHykHc_4; }
	inline void set_BBonIgYmbXiDXVdBLuciuUHykHc_4(PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F  value)
	{
		___BBonIgYmbXiDXVdBLuciuUHykHc_4 = value;
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_5() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___okMFupfyoBNZDyKMyvAtwGIUiAd_5)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_5() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_5; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_5() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_5; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_5(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_5 = value;
	}

	inline static int32_t get_offset_of_bKJattiDCgqUafgGgzBoXnBGAIWb_6() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___bKJattiDCgqUafgGgzBoXnBGAIWb_6)); }
	inline int32_t get_bKJattiDCgqUafgGgzBoXnBGAIWb_6() const { return ___bKJattiDCgqUafgGgzBoXnBGAIWb_6; }
	inline int32_t* get_address_of_bKJattiDCgqUafgGgzBoXnBGAIWb_6() { return &___bKJattiDCgqUafgGgzBoXnBGAIWb_6; }
	inline void set_bKJattiDCgqUafgGgzBoXnBGAIWb_6(int32_t value)
	{
		___bKJattiDCgqUafgGgzBoXnBGAIWb_6 = value;
	}

	inline static int32_t get_offset_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7)); }
	inline int32_t get_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() const { return ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7; }
	inline int32_t* get_address_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() { return &___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7; }
	inline void set_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7(int32_t value)
	{
		___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7 = value;
	}

	inline static int32_t get_offset_of_JWGPoriiedknEFskVzNUGNUwScr_8() { return static_cast<int32_t>(offsetof(yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8, ___JWGPoriiedknEFskVzNUGNUwScr_8)); }
	inline int32_t get_JWGPoriiedknEFskVzNUGNUwScr_8() const { return ___JWGPoriiedknEFskVzNUGNUwScr_8; }
	inline int32_t* get_address_of_JWGPoriiedknEFskVzNUGNUwScr_8() { return &___JWGPoriiedknEFskVzNUGNUwScr_8; }
	inline void set_JWGPoriiedknEFskVzNUGNUwScr_8(int32_t value)
	{
		___JWGPoriiedknEFskVzNUGNUwScr_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YUSQPKUJMBYOSHDSGGXSBFWSYIYL_TEC6CE14C4DE8B15ADF0A433E294775B1531587A8_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#define CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap
struct  ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerMap::_id
	int32_t ____id_0;
	// System.Int32 Rewired.ControllerMap::_sourceMapId
	int32_t ____sourceMapId_1;
	// System.Int32 Rewired.ControllerMap::_categoryId
	int32_t ____categoryId_2;
	// System.Int32 Rewired.ControllerMap::_layoutId
	int32_t ____layoutId_3;
	// System.String Rewired.ControllerMap::_name
	String_t* ____name_4;
	// System.Guid Rewired.ControllerMap::_hardwareGuid
	Guid_t  ____hardwareGuid_5;
	// System.Boolean Rewired.ControllerMap::_enabled
	bool ____enabled_6;
	// System.Int32 Rewired.ControllerMap::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap> Rewired.ControllerMap::VWGXUewCkbjAzopbSknmvZALlIZ
	AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * ___VWGXUewCkbjAzopbSknmvZALlIZ_8;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap> Rewired.ControllerMap::TruAdbhnbvpEVsLpODTaGQmazwL
	ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * ___TruAdbhnbvpEVsLpODTaGQmazwL_9;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap> Rewired.ControllerMap::arINfjuBJNQHwNqdOQyXSRjsHrB
	AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * ___arINfjuBJNQHwNqdOQyXSRjsHrB_10;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap> Rewired.ControllerMap::eTKgFTcHaKMRVZDKtoHIpsbImmtw
	ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11;
	// System.Int32 Rewired.ControllerMap::_playerId
	int32_t ____playerId_12;
	// System.Int32 Rewired.ControllerMap::_controllerId
	int32_t ____controllerId_13;
	// Rewired.ControllerType Rewired.ControllerMap::_controllerType
	int32_t ____controllerType_14;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__sourceMapId_1() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____sourceMapId_1)); }
	inline int32_t get__sourceMapId_1() const { return ____sourceMapId_1; }
	inline int32_t* get_address_of__sourceMapId_1() { return &____sourceMapId_1; }
	inline void set__sourceMapId_1(int32_t value)
	{
		____sourceMapId_1 = value;
	}

	inline static int32_t get_offset_of__categoryId_2() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____categoryId_2)); }
	inline int32_t get__categoryId_2() const { return ____categoryId_2; }
	inline int32_t* get_address_of__categoryId_2() { return &____categoryId_2; }
	inline void set__categoryId_2(int32_t value)
	{
		____categoryId_2 = value;
	}

	inline static int32_t get_offset_of__layoutId_3() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____layoutId_3)); }
	inline int32_t get__layoutId_3() const { return ____layoutId_3; }
	inline int32_t* get_address_of__layoutId_3() { return &____layoutId_3; }
	inline void set__layoutId_3(int32_t value)
	{
		____layoutId_3 = value;
	}

	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____name_4)); }
	inline String_t* get__name_4() const { return ____name_4; }
	inline String_t** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(String_t* value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__hardwareGuid_5() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____hardwareGuid_5)); }
	inline Guid_t  get__hardwareGuid_5() const { return ____hardwareGuid_5; }
	inline Guid_t * get_address_of__hardwareGuid_5() { return &____hardwareGuid_5; }
	inline void set__hardwareGuid_5(Guid_t  value)
	{
		____hardwareGuid_5 = value;
	}

	inline static int32_t get_offset_of__enabled_6() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____enabled_6)); }
	inline bool get__enabled_6() const { return ____enabled_6; }
	inline bool* get_address_of__enabled_6() { return &____enabled_6; }
	inline void set__enabled_6(bool value)
	{
		____enabled_6 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_7 = value;
	}

	inline static int32_t get_offset_of_VWGXUewCkbjAzopbSknmvZALlIZ_8() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___VWGXUewCkbjAzopbSknmvZALlIZ_8)); }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * get_VWGXUewCkbjAzopbSknmvZALlIZ_8() const { return ___VWGXUewCkbjAzopbSknmvZALlIZ_8; }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 ** get_address_of_VWGXUewCkbjAzopbSknmvZALlIZ_8() { return &___VWGXUewCkbjAzopbSknmvZALlIZ_8; }
	inline void set_VWGXUewCkbjAzopbSknmvZALlIZ_8(AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * value)
	{
		___VWGXUewCkbjAzopbSknmvZALlIZ_8 = value;
		Il2CppCodeGenWriteBarrier((&___VWGXUewCkbjAzopbSknmvZALlIZ_8), value);
	}

	inline static int32_t get_offset_of_TruAdbhnbvpEVsLpODTaGQmazwL_9() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___TruAdbhnbvpEVsLpODTaGQmazwL_9)); }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * get_TruAdbhnbvpEVsLpODTaGQmazwL_9() const { return ___TruAdbhnbvpEVsLpODTaGQmazwL_9; }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 ** get_address_of_TruAdbhnbvpEVsLpODTaGQmazwL_9() { return &___TruAdbhnbvpEVsLpODTaGQmazwL_9; }
	inline void set_TruAdbhnbvpEVsLpODTaGQmazwL_9(ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * value)
	{
		___TruAdbhnbvpEVsLpODTaGQmazwL_9 = value;
		Il2CppCodeGenWriteBarrier((&___TruAdbhnbvpEVsLpODTaGQmazwL_9), value);
	}

	inline static int32_t get_offset_of_arINfjuBJNQHwNqdOQyXSRjsHrB_10() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___arINfjuBJNQHwNqdOQyXSRjsHrB_10)); }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * get_arINfjuBJNQHwNqdOQyXSRjsHrB_10() const { return ___arINfjuBJNQHwNqdOQyXSRjsHrB_10; }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 ** get_address_of_arINfjuBJNQHwNqdOQyXSRjsHrB_10() { return &___arINfjuBJNQHwNqdOQyXSRjsHrB_10; }
	inline void set_arINfjuBJNQHwNqdOQyXSRjsHrB_10(AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * value)
	{
		___arINfjuBJNQHwNqdOQyXSRjsHrB_10 = value;
		Il2CppCodeGenWriteBarrier((&___arINfjuBJNQHwNqdOQyXSRjsHrB_10), value);
	}

	inline static int32_t get_offset_of_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11)); }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * get_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() const { return ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11; }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 ** get_address_of_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() { return &___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11; }
	inline void set_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11(ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * value)
	{
		___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11 = value;
		Il2CppCodeGenWriteBarrier((&___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11), value);
	}

	inline static int32_t get_offset_of__playerId_12() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____playerId_12)); }
	inline int32_t get__playerId_12() const { return ____playerId_12; }
	inline int32_t* get_address_of__playerId_12() { return &____playerId_12; }
	inline void set__playerId_12(int32_t value)
	{
		____playerId_12 = value;
	}

	inline static int32_t get_offset_of__controllerId_13() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____controllerId_13)); }
	inline int32_t get__controllerId_13() const { return ____controllerId_13; }
	inline int32_t* get_address_of__controllerId_13() { return &____controllerId_13; }
	inline void set__controllerId_13(int32_t value)
	{
		____controllerId_13 = value;
	}

	inline static int32_t get_offset_of__controllerType_14() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____controllerType_14)); }
	inline int32_t get__controllerType_14() const { return ____controllerType_14; }
	inline int32_t* get_address_of__controllerType_14() { return &____controllerType_14; }
	inline void set__controllerType_14(int32_t value)
	{
		____controllerType_14 = value;
	}
};

struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields
{
public:
	// System.Int32 Rewired.ControllerMap::FSpmeDwklQQlpZeqNSHMGkVYZQJ
	int32_t ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15;

public:
	inline static int32_t get_offset_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields, ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15)); }
	inline int32_t get_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() const { return ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15; }
	inline int32_t* get_address_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() { return &___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15; }
	inline void set_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15(int32_t value)
	{
		___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef CONTROLLERSETSELECTOR_EDITOR_TE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8_H
#define CONTROLLERSETSELECTOR_EDITOR_TE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerSetSelector_Editor
struct  ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8  : public RuntimeObject
{
public:
	// Rewired.ControllerSetSelector_Type Rewired.Data.ControllerSetSelector_Editor::_type
	int32_t ____type_0;
	// Rewired.ControllerType Rewired.Data.ControllerSetSelector_Editor::_controllerType
	int32_t ____controllerType_1;
	// System.String Rewired.Data.ControllerSetSelector_Editor::_hardwareTypeGuidString
	String_t* ____hardwareTypeGuidString_2;
	// System.String Rewired.Data.ControllerSetSelector_Editor::_hardwareIdentifier
	String_t* ____hardwareIdentifier_3;
	// System.String Rewired.Data.ControllerSetSelector_Editor::_controllerTemplateTypeGuidString
	String_t* ____controllerTemplateTypeGuidString_4;
	// System.String Rewired.Data.ControllerSetSelector_Editor::_deviceInstanceGuidString
	String_t* ____deviceInstanceGuidString_5;
	// System.Int32 Rewired.Data.ControllerSetSelector_Editor::_customControllerSourceId
	int32_t ____customControllerSourceId_6;
	// System.Int32 Rewired.Data.ControllerSetSelector_Editor::_controllerId
	int32_t ____controllerId_7;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__controllerType_1() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____controllerType_1)); }
	inline int32_t get__controllerType_1() const { return ____controllerType_1; }
	inline int32_t* get_address_of__controllerType_1() { return &____controllerType_1; }
	inline void set__controllerType_1(int32_t value)
	{
		____controllerType_1 = value;
	}

	inline static int32_t get_offset_of__hardwareTypeGuidString_2() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____hardwareTypeGuidString_2)); }
	inline String_t* get__hardwareTypeGuidString_2() const { return ____hardwareTypeGuidString_2; }
	inline String_t** get_address_of__hardwareTypeGuidString_2() { return &____hardwareTypeGuidString_2; }
	inline void set__hardwareTypeGuidString_2(String_t* value)
	{
		____hardwareTypeGuidString_2 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareTypeGuidString_2), value);
	}

	inline static int32_t get_offset_of__hardwareIdentifier_3() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____hardwareIdentifier_3)); }
	inline String_t* get__hardwareIdentifier_3() const { return ____hardwareIdentifier_3; }
	inline String_t** get_address_of__hardwareIdentifier_3() { return &____hardwareIdentifier_3; }
	inline void set__hardwareIdentifier_3(String_t* value)
	{
		____hardwareIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareIdentifier_3), value);
	}

	inline static int32_t get_offset_of__controllerTemplateTypeGuidString_4() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____controllerTemplateTypeGuidString_4)); }
	inline String_t* get__controllerTemplateTypeGuidString_4() const { return ____controllerTemplateTypeGuidString_4; }
	inline String_t** get_address_of__controllerTemplateTypeGuidString_4() { return &____controllerTemplateTypeGuidString_4; }
	inline void set__controllerTemplateTypeGuidString_4(String_t* value)
	{
		____controllerTemplateTypeGuidString_4 = value;
		Il2CppCodeGenWriteBarrier((&____controllerTemplateTypeGuidString_4), value);
	}

	inline static int32_t get_offset_of__deviceInstanceGuidString_5() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____deviceInstanceGuidString_5)); }
	inline String_t* get__deviceInstanceGuidString_5() const { return ____deviceInstanceGuidString_5; }
	inline String_t** get_address_of__deviceInstanceGuidString_5() { return &____deviceInstanceGuidString_5; }
	inline void set__deviceInstanceGuidString_5(String_t* value)
	{
		____deviceInstanceGuidString_5 = value;
		Il2CppCodeGenWriteBarrier((&____deviceInstanceGuidString_5), value);
	}

	inline static int32_t get_offset_of__customControllerSourceId_6() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____customControllerSourceId_6)); }
	inline int32_t get__customControllerSourceId_6() const { return ____customControllerSourceId_6; }
	inline int32_t* get_address_of__customControllerSourceId_6() { return &____customControllerSourceId_6; }
	inline void set__customControllerSourceId_6(int32_t value)
	{
		____customControllerSourceId_6 = value;
	}

	inline static int32_t get_offset_of__controllerId_7() { return static_cast<int32_t>(offsetof(ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8, ____controllerId_7)); }
	inline int32_t get__controllerId_7() const { return ____controllerId_7; }
	inline int32_t* get_address_of__controllerId_7() { return &____controllerId_7; }
	inline void set__controllerId_7(int32_t value)
	{
		____controllerId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSETSELECTOR_EDITOR_TE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8_H
#ifndef ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#define ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignment
struct  ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignment::type
	int32_t ___type_0;
	// System.Int32 Rewired.ElementAssignment::elementMapId
	int32_t ___elementMapId_1;
	// System.Int32 Rewired.ElementAssignment::elementIdentifierId
	int32_t ___elementIdentifierId_2;
	// Rewired.AxisRange Rewired.ElementAssignment::axisRange
	int32_t ___axisRange_3;
	// UnityEngine.KeyCode Rewired.ElementAssignment::keyboardKey
	int32_t ___keyboardKey_4;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignment::modifierKeyFlags
	int32_t ___modifierKeyFlags_5;
	// System.Int32 Rewired.ElementAssignment::actionId
	int32_t ___actionId_6;
	// Rewired.Pole Rewired.ElementAssignment::axisContribution
	int32_t ___axisContribution_7;
	// System.Boolean Rewired.ElementAssignment::invert
	bool ___invert_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_elementMapId_1() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementMapId_1)); }
	inline int32_t get_elementMapId_1() const { return ___elementMapId_1; }
	inline int32_t* get_address_of_elementMapId_1() { return &___elementMapId_1; }
	inline void set_elementMapId_1(int32_t value)
	{
		___elementMapId_1 = value;
	}

	inline static int32_t get_offset_of_elementIdentifierId_2() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementIdentifierId_2)); }
	inline int32_t get_elementIdentifierId_2() const { return ___elementIdentifierId_2; }
	inline int32_t* get_address_of_elementIdentifierId_2() { return &___elementIdentifierId_2; }
	inline void set_elementIdentifierId_2(int32_t value)
	{
		___elementIdentifierId_2 = value;
	}

	inline static int32_t get_offset_of_axisRange_3() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisRange_3)); }
	inline int32_t get_axisRange_3() const { return ___axisRange_3; }
	inline int32_t* get_address_of_axisRange_3() { return &___axisRange_3; }
	inline void set_axisRange_3(int32_t value)
	{
		___axisRange_3 = value;
	}

	inline static int32_t get_offset_of_keyboardKey_4() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___keyboardKey_4)); }
	inline int32_t get_keyboardKey_4() const { return ___keyboardKey_4; }
	inline int32_t* get_address_of_keyboardKey_4() { return &___keyboardKey_4; }
	inline void set_keyboardKey_4(int32_t value)
	{
		___keyboardKey_4 = value;
	}

	inline static int32_t get_offset_of_modifierKeyFlags_5() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___modifierKeyFlags_5)); }
	inline int32_t get_modifierKeyFlags_5() const { return ___modifierKeyFlags_5; }
	inline int32_t* get_address_of_modifierKeyFlags_5() { return &___modifierKeyFlags_5; }
	inline void set_modifierKeyFlags_5(int32_t value)
	{
		___modifierKeyFlags_5 = value;
	}

	inline static int32_t get_offset_of_actionId_6() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___actionId_6)); }
	inline int32_t get_actionId_6() const { return ___actionId_6; }
	inline int32_t* get_address_of_actionId_6() { return &___actionId_6; }
	inline void set_actionId_6(int32_t value)
	{
		___actionId_6 = value;
	}

	inline static int32_t get_offset_of_axisContribution_7() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisContribution_7)); }
	inline int32_t get_axisContribution_7() const { return ___axisContribution_7; }
	inline int32_t* get_address_of_axisContribution_7() { return &___axisContribution_7; }
	inline void set_axisContribution_7(int32_t value)
	{
		___axisContribution_7 = value;
	}

	inline static int32_t get_offset_of_invert_8() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___invert_8)); }
	inline bool get_invert_8() const { return ___invert_8; }
	inline bool* get_address_of_invert_8() { return &___invert_8; }
	inline void set_invert_8(bool value)
	{
		___invert_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
// Native definition for COM marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_com
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
#endif // ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifndef ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#define ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictCheck
struct  ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignmentConflictCheck::PReGOawJrxHuYbkYlXhFoNBbppd
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictCheck::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::fAMwdVQKMkdvbVWXbkkCQoSadHid
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	// Rewired.AxisRange Rewired.ElementAssignmentConflictCheck::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictCheck::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictCheck::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	// Rewired.Pole Rewired.ElementAssignmentConflictCheck::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	// System.Boolean Rewired.ElementAssignmentConflictCheck::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;

public:
	inline static int32_t get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___PReGOawJrxHuYbkYlXhFoNBbppd_0)); }
	inline int32_t get_PReGOawJrxHuYbkYlXhFoNBbppd_0() const { return ___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline int32_t* get_address_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return &___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline void set_PReGOawJrxHuYbkYlXhFoNBbppd_0(int32_t value)
	{
		___PReGOawJrxHuYbkYlXhFoNBbppd_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___kNupzNtNRIxDZONVUSwnnBWcpIT_4)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_4() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_4(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_4 = value;
	}

	inline static int32_t get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5)); }
	inline int32_t get_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() const { return ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline int32_t* get_address_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return &___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline void set_fAMwdVQKMkdvbVWXbkkCQoSadHid_5(int32_t value)
	{
		___fAMwdVQKMkdvbVWXbkkCQoSadHid_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FrsWYgeIWMnjOcXDysagwZGcCMF_7)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_7() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_7(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_7 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___xTkhpClvSKvoiLfYNzcXIDURAPx_8)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_8() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_8(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_8 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___UBcIxoTNwLQLduWHPwWDYgtuvif_9)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_9() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_9(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_12(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_12 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GfvPtFqTnCIcrWebHAQzqOoKceI_13)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_13() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_13(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_com
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
#endif // ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifndef ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#define ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictInfo
struct  ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D 
{
public:
	// System.Boolean Rewired.ElementAssignmentConflictInfo::rLCeavZDeONdAmfQvjLvpIKqsIQ
	bool ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	// System.Boolean Rewired.ElementAssignmentConflictInfo::uUoPCsKlKtgHCnSDBCrTHvhFbFXy
	bool ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// Rewired.ControllerElementType Rewired.ElementAssignmentConflictInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;

public:
	inline static int32_t get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0)); }
	inline bool get_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() const { return ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline bool* get_address_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return &___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline void set_rLCeavZDeONdAmfQvjLvpIKqsIQ_0(bool value)
	{
		___rLCeavZDeONdAmfQvjLvpIKqsIQ_0 = value;
	}

	inline static int32_t get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1)); }
	inline bool get_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() const { return ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline bool* get_address_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return &___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline void set_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1(bool value)
	{
		___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___kNupzNtNRIxDZONVUSwnnBWcpIT_5)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_5() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_5(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___RmmmEkVblcqxoqGYhifgdSESkSn_7)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_7() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_7(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_7 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FrsWYgeIWMnjOcXDysagwZGcCMF_8)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_8() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_8(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_8 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___SkzHHHthNTcRwnaarBvUnkLqLeI_9)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_9() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_9(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_com
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
#endif // ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifndef HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#define HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HardwareControllerMapIdentifier
struct  HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4 
{
public:
	// System.Guid Rewired.HardwareControllerMapIdentifier::guid
	Guid_t  ___guid_0;
	// Rewired.InputSource Rewired.HardwareControllerMapIdentifier::inputSource
	int32_t ___inputSource_1;
	// Rewired.InputPlatform Rewired.HardwareControllerMapIdentifier::actualInputPlatform
	int32_t ___actualInputPlatform_2;
	// System.Int32 Rewired.HardwareControllerMapIdentifier::variantIndex
	int32_t ___variantIndex_3;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}

	inline static int32_t get_offset_of_inputSource_1() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___inputSource_1)); }
	inline int32_t get_inputSource_1() const { return ___inputSource_1; }
	inline int32_t* get_address_of_inputSource_1() { return &___inputSource_1; }
	inline void set_inputSource_1(int32_t value)
	{
		___inputSource_1 = value;
	}

	inline static int32_t get_offset_of_actualInputPlatform_2() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___actualInputPlatform_2)); }
	inline int32_t get_actualInputPlatform_2() const { return ___actualInputPlatform_2; }
	inline int32_t* get_address_of_actualInputPlatform_2() { return &___actualInputPlatform_2; }
	inline void set_actualInputPlatform_2(int32_t value)
	{
		___actualInputPlatform_2 = value;
	}

	inline static int32_t get_offset_of_variantIndex_3() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___variantIndex_3)); }
	inline int32_t get_variantIndex_3() const { return ___variantIndex_3; }
	inline int32_t* get_address_of_variantIndex_3() { return &___variantIndex_3; }
	inline void set_variantIndex_3(int32_t value)
	{
		___variantIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#ifndef INPUTACTION_T9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA_H
#define INPUTACTION_T9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputAction
struct  InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA  : public RuntimeObject
{
public:
	// System.Int32 Rewired.InputAction::_id
	int32_t ____id_0;
	// System.String Rewired.InputAction::_name
	String_t* ____name_1;
	// Rewired.InputActionType Rewired.InputAction::_type
	int32_t ____type_2;
	// System.String Rewired.InputAction::_descriptiveName
	String_t* ____descriptiveName_3;
	// System.String Rewired.InputAction::_positiveDescriptiveName
	String_t* ____positiveDescriptiveName_4;
	// System.String Rewired.InputAction::_negativeDescriptiveName
	String_t* ____negativeDescriptiveName_5;
	// System.Int32 Rewired.InputAction::_behaviorId
	int32_t ____behaviorId_6;
	// System.Boolean Rewired.InputAction::_userAssignable
	bool ____userAssignable_7;
	// System.Int32 Rewired.InputAction::_categoryId
	int32_t ____categoryId_8;
	// System.String Rewired.InputAction::iytzGAPUpbIpzJUCFcpOjccCOyV
	String_t* ___iytzGAPUpbIpzJUCFcpOjccCOyV_9;
	// System.String Rewired.InputAction::sIyECxJaRrJpGveQCRvGGrqHjleG
	String_t* ___sIyECxJaRrJpGveQCRvGGrqHjleG_10;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}

	inline static int32_t get_offset_of__descriptiveName_3() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____descriptiveName_3)); }
	inline String_t* get__descriptiveName_3() const { return ____descriptiveName_3; }
	inline String_t** get_address_of__descriptiveName_3() { return &____descriptiveName_3; }
	inline void set__descriptiveName_3(String_t* value)
	{
		____descriptiveName_3 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_3), value);
	}

	inline static int32_t get_offset_of__positiveDescriptiveName_4() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____positiveDescriptiveName_4)); }
	inline String_t* get__positiveDescriptiveName_4() const { return ____positiveDescriptiveName_4; }
	inline String_t** get_address_of__positiveDescriptiveName_4() { return &____positiveDescriptiveName_4; }
	inline void set__positiveDescriptiveName_4(String_t* value)
	{
		____positiveDescriptiveName_4 = value;
		Il2CppCodeGenWriteBarrier((&____positiveDescriptiveName_4), value);
	}

	inline static int32_t get_offset_of__negativeDescriptiveName_5() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____negativeDescriptiveName_5)); }
	inline String_t* get__negativeDescriptiveName_5() const { return ____negativeDescriptiveName_5; }
	inline String_t** get_address_of__negativeDescriptiveName_5() { return &____negativeDescriptiveName_5; }
	inline void set__negativeDescriptiveName_5(String_t* value)
	{
		____negativeDescriptiveName_5 = value;
		Il2CppCodeGenWriteBarrier((&____negativeDescriptiveName_5), value);
	}

	inline static int32_t get_offset_of__behaviorId_6() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____behaviorId_6)); }
	inline int32_t get__behaviorId_6() const { return ____behaviorId_6; }
	inline int32_t* get_address_of__behaviorId_6() { return &____behaviorId_6; }
	inline void set__behaviorId_6(int32_t value)
	{
		____behaviorId_6 = value;
	}

	inline static int32_t get_offset_of__userAssignable_7() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____userAssignable_7)); }
	inline bool get__userAssignable_7() const { return ____userAssignable_7; }
	inline bool* get_address_of__userAssignable_7() { return &____userAssignable_7; }
	inline void set__userAssignable_7(bool value)
	{
		____userAssignable_7 = value;
	}

	inline static int32_t get_offset_of__categoryId_8() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ____categoryId_8)); }
	inline int32_t get__categoryId_8() const { return ____categoryId_8; }
	inline int32_t* get_address_of__categoryId_8() { return &____categoryId_8; }
	inline void set__categoryId_8(int32_t value)
	{
		____categoryId_8 = value;
	}

	inline static int32_t get_offset_of_iytzGAPUpbIpzJUCFcpOjccCOyV_9() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ___iytzGAPUpbIpzJUCFcpOjccCOyV_9)); }
	inline String_t* get_iytzGAPUpbIpzJUCFcpOjccCOyV_9() const { return ___iytzGAPUpbIpzJUCFcpOjccCOyV_9; }
	inline String_t** get_address_of_iytzGAPUpbIpzJUCFcpOjccCOyV_9() { return &___iytzGAPUpbIpzJUCFcpOjccCOyV_9; }
	inline void set_iytzGAPUpbIpzJUCFcpOjccCOyV_9(String_t* value)
	{
		___iytzGAPUpbIpzJUCFcpOjccCOyV_9 = value;
		Il2CppCodeGenWriteBarrier((&___iytzGAPUpbIpzJUCFcpOjccCOyV_9), value);
	}

	inline static int32_t get_offset_of_sIyECxJaRrJpGveQCRvGGrqHjleG_10() { return static_cast<int32_t>(offsetof(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA, ___sIyECxJaRrJpGveQCRvGGrqHjleG_10)); }
	inline String_t* get_sIyECxJaRrJpGveQCRvGGrqHjleG_10() const { return ___sIyECxJaRrJpGveQCRvGGrqHjleG_10; }
	inline String_t** get_address_of_sIyECxJaRrJpGveQCRvGGrqHjleG_10() { return &___sIyECxJaRrJpGveQCRvGGrqHjleG_10; }
	inline void set_sIyECxJaRrJpGveQCRvGGrqHjleG_10(String_t* value)
	{
		___sIyECxJaRrJpGveQCRvGGrqHjleG_10 = value;
		Il2CppCodeGenWriteBarrier((&___sIyECxJaRrJpGveQCRvGGrqHjleG_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTION_T9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA_H
#ifndef INPUTBEHAVIOR_TA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B_H
#define INPUTBEHAVIOR_TA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputBehavior
struct  InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B  : public RuntimeObject
{
public:
	// System.Int32 Rewired.InputBehavior::_id
	int32_t ____id_0;
	// System.String Rewired.InputBehavior::_name
	String_t* ____name_1;
	// System.Single Rewired.InputBehavior::_joystickAxisSensitivity
	float ____joystickAxisSensitivity_2;
	// System.Boolean Rewired.InputBehavior::_digitalAxisSimulation
	bool ____digitalAxisSimulation_3;
	// System.Boolean Rewired.InputBehavior::_digitalAxisSnap
	bool ____digitalAxisSnap_4;
	// System.Boolean Rewired.InputBehavior::_digitalAxisInstantReverse
	bool ____digitalAxisInstantReverse_5;
	// System.Single Rewired.InputBehavior::_digitalAxisGravity
	float ____digitalAxisGravity_6;
	// System.Single Rewired.InputBehavior::_digitalAxisSensitivity
	float ____digitalAxisSensitivity_7;
	// Rewired.MouseXYAxisMode Rewired.InputBehavior::_mouseXYAxisMode
	int32_t ____mouseXYAxisMode_8;
	// Rewired.MouseOtherAxisMode Rewired.InputBehavior::_mouseOtherAxisMode
	int32_t ____mouseOtherAxisMode_9;
	// System.Single Rewired.InputBehavior::_mouseXYAxisSensitivity
	float ____mouseXYAxisSensitivity_10;
	// Rewired.MouseXYAxisDeltaCalc Rewired.InputBehavior::_mouseXYAxisDeltaCalc
	int32_t ____mouseXYAxisDeltaCalc_11;
	// System.Single Rewired.InputBehavior::_mouseOtherAxisSensitivity
	float ____mouseOtherAxisSensitivity_12;
	// System.Single Rewired.InputBehavior::_customControllerAxisSensitivity
	float ____customControllerAxisSensitivity_13;
	// System.Single Rewired.InputBehavior::_buttonDoublePressSpeed
	float ____buttonDoublePressSpeed_14;
	// System.Single Rewired.InputBehavior::_buttonShortPressTime
	float ____buttonShortPressTime_15;
	// System.Single Rewired.InputBehavior::_buttonShortPressExpiresIn
	float ____buttonShortPressExpiresIn_16;
	// System.Single Rewired.InputBehavior::_buttonLongPressTime
	float ____buttonLongPressTime_17;
	// System.Single Rewired.InputBehavior::_buttonLongPressExpiresIn
	float ____buttonLongPressExpiresIn_18;
	// System.Single Rewired.InputBehavior::_buttonDeadZone
	float ____buttonDeadZone_19;
	// System.Single Rewired.InputBehavior::_buttonDownBuffer
	float ____buttonDownBuffer_20;
	// System.Single Rewired.InputBehavior::_buttonRepeatRate
	float ____buttonRepeatRate_21;
	// System.Single Rewired.InputBehavior::_buttonRepeatDelay
	float ____buttonRepeatDelay_22;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__joystickAxisSensitivity_2() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____joystickAxisSensitivity_2)); }
	inline float get__joystickAxisSensitivity_2() const { return ____joystickAxisSensitivity_2; }
	inline float* get_address_of__joystickAxisSensitivity_2() { return &____joystickAxisSensitivity_2; }
	inline void set__joystickAxisSensitivity_2(float value)
	{
		____joystickAxisSensitivity_2 = value;
	}

	inline static int32_t get_offset_of__digitalAxisSimulation_3() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____digitalAxisSimulation_3)); }
	inline bool get__digitalAxisSimulation_3() const { return ____digitalAxisSimulation_3; }
	inline bool* get_address_of__digitalAxisSimulation_3() { return &____digitalAxisSimulation_3; }
	inline void set__digitalAxisSimulation_3(bool value)
	{
		____digitalAxisSimulation_3 = value;
	}

	inline static int32_t get_offset_of__digitalAxisSnap_4() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____digitalAxisSnap_4)); }
	inline bool get__digitalAxisSnap_4() const { return ____digitalAxisSnap_4; }
	inline bool* get_address_of__digitalAxisSnap_4() { return &____digitalAxisSnap_4; }
	inline void set__digitalAxisSnap_4(bool value)
	{
		____digitalAxisSnap_4 = value;
	}

	inline static int32_t get_offset_of__digitalAxisInstantReverse_5() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____digitalAxisInstantReverse_5)); }
	inline bool get__digitalAxisInstantReverse_5() const { return ____digitalAxisInstantReverse_5; }
	inline bool* get_address_of__digitalAxisInstantReverse_5() { return &____digitalAxisInstantReverse_5; }
	inline void set__digitalAxisInstantReverse_5(bool value)
	{
		____digitalAxisInstantReverse_5 = value;
	}

	inline static int32_t get_offset_of__digitalAxisGravity_6() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____digitalAxisGravity_6)); }
	inline float get__digitalAxisGravity_6() const { return ____digitalAxisGravity_6; }
	inline float* get_address_of__digitalAxisGravity_6() { return &____digitalAxisGravity_6; }
	inline void set__digitalAxisGravity_6(float value)
	{
		____digitalAxisGravity_6 = value;
	}

	inline static int32_t get_offset_of__digitalAxisSensitivity_7() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____digitalAxisSensitivity_7)); }
	inline float get__digitalAxisSensitivity_7() const { return ____digitalAxisSensitivity_7; }
	inline float* get_address_of__digitalAxisSensitivity_7() { return &____digitalAxisSensitivity_7; }
	inline void set__digitalAxisSensitivity_7(float value)
	{
		____digitalAxisSensitivity_7 = value;
	}

	inline static int32_t get_offset_of__mouseXYAxisMode_8() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____mouseXYAxisMode_8)); }
	inline int32_t get__mouseXYAxisMode_8() const { return ____mouseXYAxisMode_8; }
	inline int32_t* get_address_of__mouseXYAxisMode_8() { return &____mouseXYAxisMode_8; }
	inline void set__mouseXYAxisMode_8(int32_t value)
	{
		____mouseXYAxisMode_8 = value;
	}

	inline static int32_t get_offset_of__mouseOtherAxisMode_9() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____mouseOtherAxisMode_9)); }
	inline int32_t get__mouseOtherAxisMode_9() const { return ____mouseOtherAxisMode_9; }
	inline int32_t* get_address_of__mouseOtherAxisMode_9() { return &____mouseOtherAxisMode_9; }
	inline void set__mouseOtherAxisMode_9(int32_t value)
	{
		____mouseOtherAxisMode_9 = value;
	}

	inline static int32_t get_offset_of__mouseXYAxisSensitivity_10() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____mouseXYAxisSensitivity_10)); }
	inline float get__mouseXYAxisSensitivity_10() const { return ____mouseXYAxisSensitivity_10; }
	inline float* get_address_of__mouseXYAxisSensitivity_10() { return &____mouseXYAxisSensitivity_10; }
	inline void set__mouseXYAxisSensitivity_10(float value)
	{
		____mouseXYAxisSensitivity_10 = value;
	}

	inline static int32_t get_offset_of__mouseXYAxisDeltaCalc_11() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____mouseXYAxisDeltaCalc_11)); }
	inline int32_t get__mouseXYAxisDeltaCalc_11() const { return ____mouseXYAxisDeltaCalc_11; }
	inline int32_t* get_address_of__mouseXYAxisDeltaCalc_11() { return &____mouseXYAxisDeltaCalc_11; }
	inline void set__mouseXYAxisDeltaCalc_11(int32_t value)
	{
		____mouseXYAxisDeltaCalc_11 = value;
	}

	inline static int32_t get_offset_of__mouseOtherAxisSensitivity_12() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____mouseOtherAxisSensitivity_12)); }
	inline float get__mouseOtherAxisSensitivity_12() const { return ____mouseOtherAxisSensitivity_12; }
	inline float* get_address_of__mouseOtherAxisSensitivity_12() { return &____mouseOtherAxisSensitivity_12; }
	inline void set__mouseOtherAxisSensitivity_12(float value)
	{
		____mouseOtherAxisSensitivity_12 = value;
	}

	inline static int32_t get_offset_of__customControllerAxisSensitivity_13() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____customControllerAxisSensitivity_13)); }
	inline float get__customControllerAxisSensitivity_13() const { return ____customControllerAxisSensitivity_13; }
	inline float* get_address_of__customControllerAxisSensitivity_13() { return &____customControllerAxisSensitivity_13; }
	inline void set__customControllerAxisSensitivity_13(float value)
	{
		____customControllerAxisSensitivity_13 = value;
	}

	inline static int32_t get_offset_of__buttonDoublePressSpeed_14() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonDoublePressSpeed_14)); }
	inline float get__buttonDoublePressSpeed_14() const { return ____buttonDoublePressSpeed_14; }
	inline float* get_address_of__buttonDoublePressSpeed_14() { return &____buttonDoublePressSpeed_14; }
	inline void set__buttonDoublePressSpeed_14(float value)
	{
		____buttonDoublePressSpeed_14 = value;
	}

	inline static int32_t get_offset_of__buttonShortPressTime_15() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonShortPressTime_15)); }
	inline float get__buttonShortPressTime_15() const { return ____buttonShortPressTime_15; }
	inline float* get_address_of__buttonShortPressTime_15() { return &____buttonShortPressTime_15; }
	inline void set__buttonShortPressTime_15(float value)
	{
		____buttonShortPressTime_15 = value;
	}

	inline static int32_t get_offset_of__buttonShortPressExpiresIn_16() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonShortPressExpiresIn_16)); }
	inline float get__buttonShortPressExpiresIn_16() const { return ____buttonShortPressExpiresIn_16; }
	inline float* get_address_of__buttonShortPressExpiresIn_16() { return &____buttonShortPressExpiresIn_16; }
	inline void set__buttonShortPressExpiresIn_16(float value)
	{
		____buttonShortPressExpiresIn_16 = value;
	}

	inline static int32_t get_offset_of__buttonLongPressTime_17() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonLongPressTime_17)); }
	inline float get__buttonLongPressTime_17() const { return ____buttonLongPressTime_17; }
	inline float* get_address_of__buttonLongPressTime_17() { return &____buttonLongPressTime_17; }
	inline void set__buttonLongPressTime_17(float value)
	{
		____buttonLongPressTime_17 = value;
	}

	inline static int32_t get_offset_of__buttonLongPressExpiresIn_18() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonLongPressExpiresIn_18)); }
	inline float get__buttonLongPressExpiresIn_18() const { return ____buttonLongPressExpiresIn_18; }
	inline float* get_address_of__buttonLongPressExpiresIn_18() { return &____buttonLongPressExpiresIn_18; }
	inline void set__buttonLongPressExpiresIn_18(float value)
	{
		____buttonLongPressExpiresIn_18 = value;
	}

	inline static int32_t get_offset_of__buttonDeadZone_19() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonDeadZone_19)); }
	inline float get__buttonDeadZone_19() const { return ____buttonDeadZone_19; }
	inline float* get_address_of__buttonDeadZone_19() { return &____buttonDeadZone_19; }
	inline void set__buttonDeadZone_19(float value)
	{
		____buttonDeadZone_19 = value;
	}

	inline static int32_t get_offset_of__buttonDownBuffer_20() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonDownBuffer_20)); }
	inline float get__buttonDownBuffer_20() const { return ____buttonDownBuffer_20; }
	inline float* get_address_of__buttonDownBuffer_20() { return &____buttonDownBuffer_20; }
	inline void set__buttonDownBuffer_20(float value)
	{
		____buttonDownBuffer_20 = value;
	}

	inline static int32_t get_offset_of__buttonRepeatRate_21() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonRepeatRate_21)); }
	inline float get__buttonRepeatRate_21() const { return ____buttonRepeatRate_21; }
	inline float* get_address_of__buttonRepeatRate_21() { return &____buttonRepeatRate_21; }
	inline void set__buttonRepeatRate_21(float value)
	{
		____buttonRepeatRate_21 = value;
	}

	inline static int32_t get_offset_of__buttonRepeatDelay_22() { return static_cast<int32_t>(offsetof(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B, ____buttonRepeatDelay_22)); }
	inline float get__buttonRepeatDelay_22() const { return ____buttonRepeatDelay_22; }
	inline float* get_address_of__buttonRepeatDelay_22() { return &____buttonRepeatDelay_22; }
	inline void set__buttonRepeatDelay_22(float value)
	{
		____buttonRepeatDelay_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBEHAVIOR_TA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B_H
#ifndef CONTEXT_TDDC4D5CD175FF95675F41961F66B1383287803A1_H
#define CONTEXT_TDDC4D5CD175FF95675F41961F66B1383287803A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_Context
struct  Context_tDDC4D5CD175FF95675F41961F66B1383287803A1  : public RuntimeObject
{
public:
	// System.Int32 Rewired.InputMapper_Context::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_0;
	// Rewired.ControllerMap Rewired.InputMapper_Context::uNIXhFwhKcbRMaCTeDSCheRwjLdb
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1;
	// Rewired.ActionElementMap Rewired.InputMapper_Context::VBSctPxMxVKKrVoFlGEfnDnmZKE
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___VBSctPxMxVKKrVoFlGEfnDnmZKE_2;
	// Rewired.AxisRange Rewired.InputMapper_Context::hxtkUvmSBcPNTccpytzgYxfepmD
	int32_t ___hxtkUvmSBcPNTccpytzgYxfepmD_3;
	// System.Boolean Rewired.InputMapper_Context::FiGYLzvVYjaKFXwFzEVgnkASbbH
	bool ___FiGYLzvVYjaKFXwFzEVgnkASbbH_4;

public:
	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_0() { return static_cast<int32_t>(offsetof(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1, ___LmADKPFcmVAiabETiHqQTGSgvmcg_0)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_0() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_0; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_0() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_0; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_0(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_0 = value;
	}

	inline static int32_t get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() { return static_cast<int32_t>(offsetof(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1, ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() const { return ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() { return &___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1; }
	inline void set_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1 = value;
		Il2CppCodeGenWriteBarrier((&___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1), value);
	}

	inline static int32_t get_offset_of_VBSctPxMxVKKrVoFlGEfnDnmZKE_2() { return static_cast<int32_t>(offsetof(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1, ___VBSctPxMxVKKrVoFlGEfnDnmZKE_2)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_VBSctPxMxVKKrVoFlGEfnDnmZKE_2() const { return ___VBSctPxMxVKKrVoFlGEfnDnmZKE_2; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_VBSctPxMxVKKrVoFlGEfnDnmZKE_2() { return &___VBSctPxMxVKKrVoFlGEfnDnmZKE_2; }
	inline void set_VBSctPxMxVKKrVoFlGEfnDnmZKE_2(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___VBSctPxMxVKKrVoFlGEfnDnmZKE_2 = value;
		Il2CppCodeGenWriteBarrier((&___VBSctPxMxVKKrVoFlGEfnDnmZKE_2), value);
	}

	inline static int32_t get_offset_of_hxtkUvmSBcPNTccpytzgYxfepmD_3() { return static_cast<int32_t>(offsetof(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1, ___hxtkUvmSBcPNTccpytzgYxfepmD_3)); }
	inline int32_t get_hxtkUvmSBcPNTccpytzgYxfepmD_3() const { return ___hxtkUvmSBcPNTccpytzgYxfepmD_3; }
	inline int32_t* get_address_of_hxtkUvmSBcPNTccpytzgYxfepmD_3() { return &___hxtkUvmSBcPNTccpytzgYxfepmD_3; }
	inline void set_hxtkUvmSBcPNTccpytzgYxfepmD_3(int32_t value)
	{
		___hxtkUvmSBcPNTccpytzgYxfepmD_3 = value;
	}

	inline static int32_t get_offset_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_4() { return static_cast<int32_t>(offsetof(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1, ___FiGYLzvVYjaKFXwFzEVgnkASbbH_4)); }
	inline bool get_FiGYLzvVYjaKFXwFzEVgnkASbbH_4() const { return ___FiGYLzvVYjaKFXwFzEVgnkASbbH_4; }
	inline bool* get_address_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_4() { return &___FiGYLzvVYjaKFXwFzEVgnkASbbH_4; }
	inline void set_FiGYLzvVYjaKFXwFzEVgnkASbbH_4(bool value)
	{
		___FiGYLzvVYjaKFXwFzEVgnkASbbH_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_TDDC4D5CD175FF95675F41961F66B1383287803A1_H
#ifndef OPTIONS_T9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_H
#define OPTIONS_T9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_Options
struct  Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939  : public RuntimeObject
{
public:
	// System.Boolean Rewired.InputMapper_Options::SEVDFzCpjCtpApiwQRcLHQrQkQQf
	bool ___SEVDFzCpjCtpApiwQRcLHQrQkQQf_1;
	// System.Boolean Rewired.InputMapper_Options::IGOSFeqRhMnFLBNwccaEaBGGbWj
	bool ___IGOSFeqRhMnFLBNwccaEaBGGbWj_2;
	// System.Boolean Rewired.InputMapper_Options::HSLudSFRvxufIdhBdMeCsFDES
	bool ___HSLudSFRvxufIdhBdMeCsFDES_3;
	// System.Single Rewired.InputMapper_Options::dpZMOEDlEtSDdcGmfkNWCUboVdF
	float ___dpZMOEDlEtSDdcGmfkNWCUboVdF_4;
	// System.Boolean Rewired.InputMapper_Options::WvuAcjIVQhUWfNyQbmSxXpNkWWO
	bool ___WvuAcjIVQhUWfNyQbmSxXpNkWWO_5;
	// System.Boolean Rewired.InputMapper_Options::mICJNeaKlqPZPYqoGBhEzpLOaJl
	bool ___mICJNeaKlqPZPYqoGBhEzpLOaJl_6;
	// System.Boolean Rewired.InputMapper_Options::qVSUFZqdojuOKnnFGhpHhqfCPhuE
	bool ___qVSUFZqdojuOKnnFGhpHhqfCPhuE_7;
	// System.Boolean Rewired.InputMapper_Options::oNAjftmRmHbVnhAHFntnGVnqaTP
	bool ___oNAjftmRmHbVnhAHFntnGVnqaTP_8;
	// System.Int32[] Rewired.InputMapper_Options::cTKnfXdudtiGNvTKmZVIEstpxVo
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cTKnfXdudtiGNvTKmZVIEstpxVo_9;
	// Rewired.InputMapper_ConflictResponse Rewired.InputMapper_Options::KiSnKvoujFbxeceQXufddMWqUaJ
	int32_t ___KiSnKvoujFbxeceQXufddMWqUaJ_10;
	// System.Boolean Rewired.InputMapper_Options::BecnniJiivNwUjdDVdhjjIvByhX
	bool ___BecnniJiivNwUjdDVdhjjIvByhX_11;
	// System.Boolean Rewired.InputMapper_Options::vXsRwQupDqHXJafbToenzqRwJAi
	bool ___vXsRwQupDqHXJafbToenzqRwJAi_12;
	// System.Boolean Rewired.InputMapper_Options::nrSXHvZeEScuFRfWNmjIVsYZdDD
	bool ___nrSXHvZeEScuFRfWNmjIVsYZdDD_13;
	// System.Boolean Rewired.InputMapper_Options::exushwyBliTrYYYHyOJbNCKGEoh
	bool ___exushwyBliTrYYYHyOJbNCKGEoh_14;
	// System.Single Rewired.InputMapper_Options::PFXJmRDFRPcTIDElVgSaNDXBICR
	float ___PFXJmRDFRPcTIDElVgSaNDXBICR_15;
	// System.Collections.Generic.Dictionary`2<System.String,Rewired.Utils.SafeDelegate> Rewired.InputMapper_Options::GFDmSkApjRNFYNqNgaFsKCNUJsST
	Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * ___GFDmSkApjRNFYNqNgaFsKCNUJsST_16;

public:
	inline static int32_t get_offset_of_SEVDFzCpjCtpApiwQRcLHQrQkQQf_1() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___SEVDFzCpjCtpApiwQRcLHQrQkQQf_1)); }
	inline bool get_SEVDFzCpjCtpApiwQRcLHQrQkQQf_1() const { return ___SEVDFzCpjCtpApiwQRcLHQrQkQQf_1; }
	inline bool* get_address_of_SEVDFzCpjCtpApiwQRcLHQrQkQQf_1() { return &___SEVDFzCpjCtpApiwQRcLHQrQkQQf_1; }
	inline void set_SEVDFzCpjCtpApiwQRcLHQrQkQQf_1(bool value)
	{
		___SEVDFzCpjCtpApiwQRcLHQrQkQQf_1 = value;
	}

	inline static int32_t get_offset_of_IGOSFeqRhMnFLBNwccaEaBGGbWj_2() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___IGOSFeqRhMnFLBNwccaEaBGGbWj_2)); }
	inline bool get_IGOSFeqRhMnFLBNwccaEaBGGbWj_2() const { return ___IGOSFeqRhMnFLBNwccaEaBGGbWj_2; }
	inline bool* get_address_of_IGOSFeqRhMnFLBNwccaEaBGGbWj_2() { return &___IGOSFeqRhMnFLBNwccaEaBGGbWj_2; }
	inline void set_IGOSFeqRhMnFLBNwccaEaBGGbWj_2(bool value)
	{
		___IGOSFeqRhMnFLBNwccaEaBGGbWj_2 = value;
	}

	inline static int32_t get_offset_of_HSLudSFRvxufIdhBdMeCsFDES_3() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___HSLudSFRvxufIdhBdMeCsFDES_3)); }
	inline bool get_HSLudSFRvxufIdhBdMeCsFDES_3() const { return ___HSLudSFRvxufIdhBdMeCsFDES_3; }
	inline bool* get_address_of_HSLudSFRvxufIdhBdMeCsFDES_3() { return &___HSLudSFRvxufIdhBdMeCsFDES_3; }
	inline void set_HSLudSFRvxufIdhBdMeCsFDES_3(bool value)
	{
		___HSLudSFRvxufIdhBdMeCsFDES_3 = value;
	}

	inline static int32_t get_offset_of_dpZMOEDlEtSDdcGmfkNWCUboVdF_4() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___dpZMOEDlEtSDdcGmfkNWCUboVdF_4)); }
	inline float get_dpZMOEDlEtSDdcGmfkNWCUboVdF_4() const { return ___dpZMOEDlEtSDdcGmfkNWCUboVdF_4; }
	inline float* get_address_of_dpZMOEDlEtSDdcGmfkNWCUboVdF_4() { return &___dpZMOEDlEtSDdcGmfkNWCUboVdF_4; }
	inline void set_dpZMOEDlEtSDdcGmfkNWCUboVdF_4(float value)
	{
		___dpZMOEDlEtSDdcGmfkNWCUboVdF_4 = value;
	}

	inline static int32_t get_offset_of_WvuAcjIVQhUWfNyQbmSxXpNkWWO_5() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___WvuAcjIVQhUWfNyQbmSxXpNkWWO_5)); }
	inline bool get_WvuAcjIVQhUWfNyQbmSxXpNkWWO_5() const { return ___WvuAcjIVQhUWfNyQbmSxXpNkWWO_5; }
	inline bool* get_address_of_WvuAcjIVQhUWfNyQbmSxXpNkWWO_5() { return &___WvuAcjIVQhUWfNyQbmSxXpNkWWO_5; }
	inline void set_WvuAcjIVQhUWfNyQbmSxXpNkWWO_5(bool value)
	{
		___WvuAcjIVQhUWfNyQbmSxXpNkWWO_5 = value;
	}

	inline static int32_t get_offset_of_mICJNeaKlqPZPYqoGBhEzpLOaJl_6() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___mICJNeaKlqPZPYqoGBhEzpLOaJl_6)); }
	inline bool get_mICJNeaKlqPZPYqoGBhEzpLOaJl_6() const { return ___mICJNeaKlqPZPYqoGBhEzpLOaJl_6; }
	inline bool* get_address_of_mICJNeaKlqPZPYqoGBhEzpLOaJl_6() { return &___mICJNeaKlqPZPYqoGBhEzpLOaJl_6; }
	inline void set_mICJNeaKlqPZPYqoGBhEzpLOaJl_6(bool value)
	{
		___mICJNeaKlqPZPYqoGBhEzpLOaJl_6 = value;
	}

	inline static int32_t get_offset_of_qVSUFZqdojuOKnnFGhpHhqfCPhuE_7() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___qVSUFZqdojuOKnnFGhpHhqfCPhuE_7)); }
	inline bool get_qVSUFZqdojuOKnnFGhpHhqfCPhuE_7() const { return ___qVSUFZqdojuOKnnFGhpHhqfCPhuE_7; }
	inline bool* get_address_of_qVSUFZqdojuOKnnFGhpHhqfCPhuE_7() { return &___qVSUFZqdojuOKnnFGhpHhqfCPhuE_7; }
	inline void set_qVSUFZqdojuOKnnFGhpHhqfCPhuE_7(bool value)
	{
		___qVSUFZqdojuOKnnFGhpHhqfCPhuE_7 = value;
	}

	inline static int32_t get_offset_of_oNAjftmRmHbVnhAHFntnGVnqaTP_8() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___oNAjftmRmHbVnhAHFntnGVnqaTP_8)); }
	inline bool get_oNAjftmRmHbVnhAHFntnGVnqaTP_8() const { return ___oNAjftmRmHbVnhAHFntnGVnqaTP_8; }
	inline bool* get_address_of_oNAjftmRmHbVnhAHFntnGVnqaTP_8() { return &___oNAjftmRmHbVnhAHFntnGVnqaTP_8; }
	inline void set_oNAjftmRmHbVnhAHFntnGVnqaTP_8(bool value)
	{
		___oNAjftmRmHbVnhAHFntnGVnqaTP_8 = value;
	}

	inline static int32_t get_offset_of_cTKnfXdudtiGNvTKmZVIEstpxVo_9() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___cTKnfXdudtiGNvTKmZVIEstpxVo_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cTKnfXdudtiGNvTKmZVIEstpxVo_9() const { return ___cTKnfXdudtiGNvTKmZVIEstpxVo_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cTKnfXdudtiGNvTKmZVIEstpxVo_9() { return &___cTKnfXdudtiGNvTKmZVIEstpxVo_9; }
	inline void set_cTKnfXdudtiGNvTKmZVIEstpxVo_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cTKnfXdudtiGNvTKmZVIEstpxVo_9 = value;
		Il2CppCodeGenWriteBarrier((&___cTKnfXdudtiGNvTKmZVIEstpxVo_9), value);
	}

	inline static int32_t get_offset_of_KiSnKvoujFbxeceQXufddMWqUaJ_10() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___KiSnKvoujFbxeceQXufddMWqUaJ_10)); }
	inline int32_t get_KiSnKvoujFbxeceQXufddMWqUaJ_10() const { return ___KiSnKvoujFbxeceQXufddMWqUaJ_10; }
	inline int32_t* get_address_of_KiSnKvoujFbxeceQXufddMWqUaJ_10() { return &___KiSnKvoujFbxeceQXufddMWqUaJ_10; }
	inline void set_KiSnKvoujFbxeceQXufddMWqUaJ_10(int32_t value)
	{
		___KiSnKvoujFbxeceQXufddMWqUaJ_10 = value;
	}

	inline static int32_t get_offset_of_BecnniJiivNwUjdDVdhjjIvByhX_11() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___BecnniJiivNwUjdDVdhjjIvByhX_11)); }
	inline bool get_BecnniJiivNwUjdDVdhjjIvByhX_11() const { return ___BecnniJiivNwUjdDVdhjjIvByhX_11; }
	inline bool* get_address_of_BecnniJiivNwUjdDVdhjjIvByhX_11() { return &___BecnniJiivNwUjdDVdhjjIvByhX_11; }
	inline void set_BecnniJiivNwUjdDVdhjjIvByhX_11(bool value)
	{
		___BecnniJiivNwUjdDVdhjjIvByhX_11 = value;
	}

	inline static int32_t get_offset_of_vXsRwQupDqHXJafbToenzqRwJAi_12() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___vXsRwQupDqHXJafbToenzqRwJAi_12)); }
	inline bool get_vXsRwQupDqHXJafbToenzqRwJAi_12() const { return ___vXsRwQupDqHXJafbToenzqRwJAi_12; }
	inline bool* get_address_of_vXsRwQupDqHXJafbToenzqRwJAi_12() { return &___vXsRwQupDqHXJafbToenzqRwJAi_12; }
	inline void set_vXsRwQupDqHXJafbToenzqRwJAi_12(bool value)
	{
		___vXsRwQupDqHXJafbToenzqRwJAi_12 = value;
	}

	inline static int32_t get_offset_of_nrSXHvZeEScuFRfWNmjIVsYZdDD_13() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___nrSXHvZeEScuFRfWNmjIVsYZdDD_13)); }
	inline bool get_nrSXHvZeEScuFRfWNmjIVsYZdDD_13() const { return ___nrSXHvZeEScuFRfWNmjIVsYZdDD_13; }
	inline bool* get_address_of_nrSXHvZeEScuFRfWNmjIVsYZdDD_13() { return &___nrSXHvZeEScuFRfWNmjIVsYZdDD_13; }
	inline void set_nrSXHvZeEScuFRfWNmjIVsYZdDD_13(bool value)
	{
		___nrSXHvZeEScuFRfWNmjIVsYZdDD_13 = value;
	}

	inline static int32_t get_offset_of_exushwyBliTrYYYHyOJbNCKGEoh_14() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___exushwyBliTrYYYHyOJbNCKGEoh_14)); }
	inline bool get_exushwyBliTrYYYHyOJbNCKGEoh_14() const { return ___exushwyBliTrYYYHyOJbNCKGEoh_14; }
	inline bool* get_address_of_exushwyBliTrYYYHyOJbNCKGEoh_14() { return &___exushwyBliTrYYYHyOJbNCKGEoh_14; }
	inline void set_exushwyBliTrYYYHyOJbNCKGEoh_14(bool value)
	{
		___exushwyBliTrYYYHyOJbNCKGEoh_14 = value;
	}

	inline static int32_t get_offset_of_PFXJmRDFRPcTIDElVgSaNDXBICR_15() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___PFXJmRDFRPcTIDElVgSaNDXBICR_15)); }
	inline float get_PFXJmRDFRPcTIDElVgSaNDXBICR_15() const { return ___PFXJmRDFRPcTIDElVgSaNDXBICR_15; }
	inline float* get_address_of_PFXJmRDFRPcTIDElVgSaNDXBICR_15() { return &___PFXJmRDFRPcTIDElVgSaNDXBICR_15; }
	inline void set_PFXJmRDFRPcTIDElVgSaNDXBICR_15(float value)
	{
		___PFXJmRDFRPcTIDElVgSaNDXBICR_15 = value;
	}

	inline static int32_t get_offset_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_16() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939, ___GFDmSkApjRNFYNqNgaFsKCNUJsST_16)); }
	inline Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * get_GFDmSkApjRNFYNqNgaFsKCNUJsST_16() const { return ___GFDmSkApjRNFYNqNgaFsKCNUJsST_16; }
	inline Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 ** get_address_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_16() { return &___GFDmSkApjRNFYNqNgaFsKCNUJsST_16; }
	inline void set_GFDmSkApjRNFYNqNgaFsKCNUJsST_16(Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * value)
	{
		___GFDmSkApjRNFYNqNgaFsKCNUJsST_16 = value;
		Il2CppCodeGenWriteBarrier((&___GFDmSkApjRNFYNqNgaFsKCNUJsST_16), value);
	}
};

struct Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_StaticFields
{
public:
	// System.Action`1<System.Exception> Rewired.InputMapper_Options::rMSAIWRRORgnlBNyMWCUdKjLpiE
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___rMSAIWRRORgnlBNyMWCUdKjLpiE_17;

public:
	inline static int32_t get_offset_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_17() { return static_cast<int32_t>(offsetof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_StaticFields, ___rMSAIWRRORgnlBNyMWCUdKjLpiE_17)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_rMSAIWRRORgnlBNyMWCUdKjLpiE_17() const { return ___rMSAIWRRORgnlBNyMWCUdKjLpiE_17; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_17() { return &___rMSAIWRRORgnlBNyMWCUdKjLpiE_17; }
	inline void set_rMSAIWRRORgnlBNyMWCUdKjLpiE_17(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___rMSAIWRRORgnlBNyMWCUdKjLpiE_17 = value;
		Il2CppCodeGenWriteBarrier((&___rMSAIWRRORgnlBNyMWCUdKjLpiE_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONS_T9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_H
#ifndef EVAMJFHEKQMZGGEXFVXQUCZEHSZ_T84FC3AFA5DB70DDBD23260B6B00153434EADC5C8_H
#define EVAMJFHEKQMZGGEXFVXQUCZEHSZ_T84FC3AFA5DB70DDBD23260B6B00153434EADC5C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ
struct  EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_4;
	// Rewired.ElementAssignmentConflictCheck Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5;
	// System.Boolean Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ElementAssignment Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::ACjiuYYNvcCGLkbnCIoZboxfCFWr
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___ACjiuYYNvcCGLkbnCIoZboxfCFWr_8;
	// System.Int32 Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::pWuGlrwnTxWREPQHcCZSYjOxeYl
	int32_t ___pWuGlrwnTxWREPQHcCZSYjOxeYl_9;
	// Rewired.ActionElementMap Rewired.ControllerMap_EVaMJFheKQmZggexfvxquCZEhSZ::bmcyskvpVsWgsnnVaivmepTdeQk
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___bmcyskvpVsWgsnnVaivmepTdeQk_10;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___RXwTkvaioBOXLvHfDHkciCcOgzY_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_4() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_4 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_ACjiuYYNvcCGLkbnCIoZboxfCFWr_8() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___ACjiuYYNvcCGLkbnCIoZboxfCFWr_8)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_ACjiuYYNvcCGLkbnCIoZboxfCFWr_8() const { return ___ACjiuYYNvcCGLkbnCIoZboxfCFWr_8; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_ACjiuYYNvcCGLkbnCIoZboxfCFWr_8() { return &___ACjiuYYNvcCGLkbnCIoZboxfCFWr_8; }
	inline void set_ACjiuYYNvcCGLkbnCIoZboxfCFWr_8(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___ACjiuYYNvcCGLkbnCIoZboxfCFWr_8 = value;
	}

	inline static int32_t get_offset_of_pWuGlrwnTxWREPQHcCZSYjOxeYl_9() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___pWuGlrwnTxWREPQHcCZSYjOxeYl_9)); }
	inline int32_t get_pWuGlrwnTxWREPQHcCZSYjOxeYl_9() const { return ___pWuGlrwnTxWREPQHcCZSYjOxeYl_9; }
	inline int32_t* get_address_of_pWuGlrwnTxWREPQHcCZSYjOxeYl_9() { return &___pWuGlrwnTxWREPQHcCZSYjOxeYl_9; }
	inline void set_pWuGlrwnTxWREPQHcCZSYjOxeYl_9(int32_t value)
	{
		___pWuGlrwnTxWREPQHcCZSYjOxeYl_9 = value;
	}

	inline static int32_t get_offset_of_bmcyskvpVsWgsnnVaivmepTdeQk_10() { return static_cast<int32_t>(offsetof(EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8, ___bmcyskvpVsWgsnnVaivmepTdeQk_10)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_bmcyskvpVsWgsnnVaivmepTdeQk_10() const { return ___bmcyskvpVsWgsnnVaivmepTdeQk_10; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_bmcyskvpVsWgsnnVaivmepTdeQk_10() { return &___bmcyskvpVsWgsnnVaivmepTdeQk_10; }
	inline void set_bmcyskvpVsWgsnnVaivmepTdeQk_10(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___bmcyskvpVsWgsnnVaivmepTdeQk_10 = value;
		Il2CppCodeGenWriteBarrier((&___bmcyskvpVsWgsnnVaivmepTdeQk_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVAMJFHEKQMZGGEXFVXQUCZEHSZ_T84FC3AFA5DB70DDBD23260B6B00153434EADC5C8_H
#ifndef ZMGMBSZPKBITBKRYVBQSBVBXBPWI_T01F6B7A3C44298D254BEFB4A1BC1931D16C705B1_H
#define ZMGMBSZPKBITBKRYVBQSBVBXBPWI_T01F6B7A3C44298D254BEFB4A1BC1931D16C705B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi
struct  ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ActionElementMap Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::CPuSQMXEaFTdKTziCifqLUFakpe
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CPuSQMXEaFTdKTziCifqLUFakpe_4;
	// Rewired.ActionElementMap Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::xRSEXSmmPMfcXipPcqHmVjqYVhoG
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5;
	// System.Boolean Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Int32 Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::QcpYrCzXoHBprkTYGgNUNLhrDlb
	int32_t ___QcpYrCzXoHBprkTYGgNUNLhrDlb_8;
	// Rewired.ActionElementMap Rewired.ControllerMap_ZMgmBSZPKbITBkRyvbqSbvBxbpwi::bXnYNPiASfROUbaNwkMEADTQxsA
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___bXnYNPiASfROUbaNwkMEADTQxsA_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_4() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___CPuSQMXEaFTdKTziCifqLUFakpe_4)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CPuSQMXEaFTdKTziCifqLUFakpe_4() const { return ___CPuSQMXEaFTdKTziCifqLUFakpe_4; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CPuSQMXEaFTdKTziCifqLUFakpe_4() { return &___CPuSQMXEaFTdKTziCifqLUFakpe_4; }
	inline void set_CPuSQMXEaFTdKTziCifqLUFakpe_4(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CPuSQMXEaFTdKTziCifqLUFakpe_4 = value;
		Il2CppCodeGenWriteBarrier((&___CPuSQMXEaFTdKTziCifqLUFakpe_4), value);
	}

	inline static int32_t get_offset_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() const { return ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() { return &___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5; }
	inline void set_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5 = value;
		Il2CppCodeGenWriteBarrier((&___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_8() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___QcpYrCzXoHBprkTYGgNUNLhrDlb_8)); }
	inline int32_t get_QcpYrCzXoHBprkTYGgNUNLhrDlb_8() const { return ___QcpYrCzXoHBprkTYGgNUNLhrDlb_8; }
	inline int32_t* get_address_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_8() { return &___QcpYrCzXoHBprkTYGgNUNLhrDlb_8; }
	inline void set_QcpYrCzXoHBprkTYGgNUNLhrDlb_8(int32_t value)
	{
		___QcpYrCzXoHBprkTYGgNUNLhrDlb_8 = value;
	}

	inline static int32_t get_offset_of_bXnYNPiASfROUbaNwkMEADTQxsA_9() { return static_cast<int32_t>(offsetof(ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1, ___bXnYNPiASfROUbaNwkMEADTQxsA_9)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_bXnYNPiASfROUbaNwkMEADTQxsA_9() const { return ___bXnYNPiASfROUbaNwkMEADTQxsA_9; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_bXnYNPiASfROUbaNwkMEADTQxsA_9() { return &___bXnYNPiASfROUbaNwkMEADTQxsA_9; }
	inline void set_bXnYNPiASfROUbaNwkMEADTQxsA_9(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___bXnYNPiASfROUbaNwkMEADTQxsA_9 = value;
		Il2CppCodeGenWriteBarrier((&___bXnYNPiASfROUbaNwkMEADTQxsA_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZMGMBSZPKBITBKRYVBQSBVBXBPWI_T01F6B7A3C44298D254BEFB4A1BC1931D16C705B1_H
#ifndef KSUBNEJQTLNCQGXEZMFPAGBJGKHT_T49736CC9017020FD46A1E0732E3AB71D174C2D4C_H
#define KSUBNEJQTLNCQGXEZMFPAGBJGKHT_T49736CC9017020FD46A1E0732E3AB71D174C2D4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt
struct  ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMap Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerMap Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::eDZexKHojMIOIcGgWyZwyijReQJ
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___eDZexKHojMIOIcGgWyZwyijReQJ_4;
	// Rewired.ControllerMap Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::vEmKKUUwDIqfnqRwGeghcRhPDEr
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5;
	// System.Boolean Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Collections.Generic.IList`1<Rewired.ActionElementMap> Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::xZzdvWORdyUNltHXdFeBwXYpAaQ
	RuntimeObject* ___xZzdvWORdyUNltHXdFeBwXYpAaQ_8;
	// System.Int32 Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::UVCamMYFnOKzRiwRmrmqCuTMBps
	int32_t ___UVCamMYFnOKzRiwRmrmqCuTMBps_9;
	// System.Int32 Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::mCtlfnDGBODwtcZpRdLladWaxwoL
	int32_t ___mCtlfnDGBODwtcZpRdLladWaxwoL_10;
	// Rewired.ActionElementMap Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::DkUEgfheIRLBlrdBwlapxNFbOwmc
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___DkUEgfheIRLBlrdBwlapxNFbOwmc_11;
	// System.Int32 Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::LUIqqILPDzJbivmrLbLDVSNNEZd
	int32_t ___LUIqqILPDzJbivmrLbLDVSNNEZd_12;
	// Rewired.ActionElementMap Rewired.ControllerMap_ksuBNeJQTLncqGxeZmfpAGbJgKHt::HMecUabwlSbKzobmDQZgFrHAzGue
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___HMecUabwlSbKzobmDQZgFrHAzGue_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_4() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___eDZexKHojMIOIcGgWyZwyijReQJ_4)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_eDZexKHojMIOIcGgWyZwyijReQJ_4() const { return ___eDZexKHojMIOIcGgWyZwyijReQJ_4; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_eDZexKHojMIOIcGgWyZwyijReQJ_4() { return &___eDZexKHojMIOIcGgWyZwyijReQJ_4; }
	inline void set_eDZexKHojMIOIcGgWyZwyijReQJ_4(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___eDZexKHojMIOIcGgWyZwyijReQJ_4 = value;
		Il2CppCodeGenWriteBarrier((&___eDZexKHojMIOIcGgWyZwyijReQJ_4), value);
	}

	inline static int32_t get_offset_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() const { return ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() { return &___vEmKKUUwDIqfnqRwGeghcRhPDEr_5; }
	inline void set_vEmKKUUwDIqfnqRwGeghcRhPDEr_5(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___vEmKKUUwDIqfnqRwGeghcRhPDEr_5 = value;
		Il2CppCodeGenWriteBarrier((&___vEmKKUUwDIqfnqRwGeghcRhPDEr_5), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_xZzdvWORdyUNltHXdFeBwXYpAaQ_8() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___xZzdvWORdyUNltHXdFeBwXYpAaQ_8)); }
	inline RuntimeObject* get_xZzdvWORdyUNltHXdFeBwXYpAaQ_8() const { return ___xZzdvWORdyUNltHXdFeBwXYpAaQ_8; }
	inline RuntimeObject** get_address_of_xZzdvWORdyUNltHXdFeBwXYpAaQ_8() { return &___xZzdvWORdyUNltHXdFeBwXYpAaQ_8; }
	inline void set_xZzdvWORdyUNltHXdFeBwXYpAaQ_8(RuntimeObject* value)
	{
		___xZzdvWORdyUNltHXdFeBwXYpAaQ_8 = value;
		Il2CppCodeGenWriteBarrier((&___xZzdvWORdyUNltHXdFeBwXYpAaQ_8), value);
	}

	inline static int32_t get_offset_of_UVCamMYFnOKzRiwRmrmqCuTMBps_9() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___UVCamMYFnOKzRiwRmrmqCuTMBps_9)); }
	inline int32_t get_UVCamMYFnOKzRiwRmrmqCuTMBps_9() const { return ___UVCamMYFnOKzRiwRmrmqCuTMBps_9; }
	inline int32_t* get_address_of_UVCamMYFnOKzRiwRmrmqCuTMBps_9() { return &___UVCamMYFnOKzRiwRmrmqCuTMBps_9; }
	inline void set_UVCamMYFnOKzRiwRmrmqCuTMBps_9(int32_t value)
	{
		___UVCamMYFnOKzRiwRmrmqCuTMBps_9 = value;
	}

	inline static int32_t get_offset_of_mCtlfnDGBODwtcZpRdLladWaxwoL_10() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___mCtlfnDGBODwtcZpRdLladWaxwoL_10)); }
	inline int32_t get_mCtlfnDGBODwtcZpRdLladWaxwoL_10() const { return ___mCtlfnDGBODwtcZpRdLladWaxwoL_10; }
	inline int32_t* get_address_of_mCtlfnDGBODwtcZpRdLladWaxwoL_10() { return &___mCtlfnDGBODwtcZpRdLladWaxwoL_10; }
	inline void set_mCtlfnDGBODwtcZpRdLladWaxwoL_10(int32_t value)
	{
		___mCtlfnDGBODwtcZpRdLladWaxwoL_10 = value;
	}

	inline static int32_t get_offset_of_DkUEgfheIRLBlrdBwlapxNFbOwmc_11() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___DkUEgfheIRLBlrdBwlapxNFbOwmc_11)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_DkUEgfheIRLBlrdBwlapxNFbOwmc_11() const { return ___DkUEgfheIRLBlrdBwlapxNFbOwmc_11; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_DkUEgfheIRLBlrdBwlapxNFbOwmc_11() { return &___DkUEgfheIRLBlrdBwlapxNFbOwmc_11; }
	inline void set_DkUEgfheIRLBlrdBwlapxNFbOwmc_11(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___DkUEgfheIRLBlrdBwlapxNFbOwmc_11 = value;
		Il2CppCodeGenWriteBarrier((&___DkUEgfheIRLBlrdBwlapxNFbOwmc_11), value);
	}

	inline static int32_t get_offset_of_LUIqqILPDzJbivmrLbLDVSNNEZd_12() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___LUIqqILPDzJbivmrLbLDVSNNEZd_12)); }
	inline int32_t get_LUIqqILPDzJbivmrLbLDVSNNEZd_12() const { return ___LUIqqILPDzJbivmrLbLDVSNNEZd_12; }
	inline int32_t* get_address_of_LUIqqILPDzJbivmrLbLDVSNNEZd_12() { return &___LUIqqILPDzJbivmrLbLDVSNNEZd_12; }
	inline void set_LUIqqILPDzJbivmrLbLDVSNNEZd_12(int32_t value)
	{
		___LUIqqILPDzJbivmrLbLDVSNNEZd_12 = value;
	}

	inline static int32_t get_offset_of_HMecUabwlSbKzobmDQZgFrHAzGue_13() { return static_cast<int32_t>(offsetof(ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C, ___HMecUabwlSbKzobmDQZgFrHAzGue_13)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_HMecUabwlSbKzobmDQZgFrHAzGue_13() const { return ___HMecUabwlSbKzobmDQZgFrHAzGue_13; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_HMecUabwlSbKzobmDQZgFrHAzGue_13() { return &___HMecUabwlSbKzobmDQZgFrHAzGue_13; }
	inline void set_HMecUabwlSbKzobmDQZgFrHAzGue_13(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___HMecUabwlSbKzobmDQZgFrHAzGue_13 = value;
		Il2CppCodeGenWriteBarrier((&___HMecUabwlSbKzobmDQZgFrHAzGue_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KSUBNEJQTLNCQGXEZMFPAGBJGKHT_T49736CC9017020FD46A1E0732E3AB71D174C2D4C_H
#ifndef CONTROLLERMAPWITHAXES_T6D095E9EE41B757831830413DC6FE457D1892C36_H
#define CONTROLLERMAPWITHAXES_T6D095E9EE41B757831830413DC6FE457D1892C36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapWithAxes
struct  ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36  : public ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB
{
public:
	// System.Collections.Generic.IList`1<Rewired.ActionElementMap> Rewired.ControllerMapWithAxes::DzNPmgjSCVrhFrAmXealRsomAik
	RuntimeObject* ___DzNPmgjSCVrhFrAmXealRsomAik_16;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap> Rewired.ControllerMapWithAxes::uqEGHdRIdILZuXvprphGZfOEfmV
	ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * ___uqEGHdRIdILZuXvprphGZfOEfmV_17;

public:
	inline static int32_t get_offset_of_DzNPmgjSCVrhFrAmXealRsomAik_16() { return static_cast<int32_t>(offsetof(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36, ___DzNPmgjSCVrhFrAmXealRsomAik_16)); }
	inline RuntimeObject* get_DzNPmgjSCVrhFrAmXealRsomAik_16() const { return ___DzNPmgjSCVrhFrAmXealRsomAik_16; }
	inline RuntimeObject** get_address_of_DzNPmgjSCVrhFrAmXealRsomAik_16() { return &___DzNPmgjSCVrhFrAmXealRsomAik_16; }
	inline void set_DzNPmgjSCVrhFrAmXealRsomAik_16(RuntimeObject* value)
	{
		___DzNPmgjSCVrhFrAmXealRsomAik_16 = value;
		Il2CppCodeGenWriteBarrier((&___DzNPmgjSCVrhFrAmXealRsomAik_16), value);
	}

	inline static int32_t get_offset_of_uqEGHdRIdILZuXvprphGZfOEfmV_17() { return static_cast<int32_t>(offsetof(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36, ___uqEGHdRIdILZuXvprphGZfOEfmV_17)); }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * get_uqEGHdRIdILZuXvprphGZfOEfmV_17() const { return ___uqEGHdRIdILZuXvprphGZfOEfmV_17; }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 ** get_address_of_uqEGHdRIdILZuXvprphGZfOEfmV_17() { return &___uqEGHdRIdILZuXvprphGZfOEfmV_17; }
	inline void set_uqEGHdRIdILZuXvprphGZfOEfmV_17(ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * value)
	{
		___uqEGHdRIdILZuXvprphGZfOEfmV_17 = value;
		Il2CppCodeGenWriteBarrier((&___uqEGHdRIdILZuXvprphGZfOEfmV_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPWITHAXES_T6D095E9EE41B757831830413DC6FE457D1892C36_H
#ifndef DIBENVGSLRVNHCINPEEIWUVNYEQU_T8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC_H
#define DIBENVGSLRVNHCINPEEIWUVNYEQU_T8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ
struct  DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMapWithAxes Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::CPuSQMXEaFTdKTziCifqLUFakpe
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CPuSQMXEaFTdKTziCifqLUFakpe_4;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::xRSEXSmmPMfcXipPcqHmVjqYVhoG
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5;
	// System.Boolean Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::OgQTGunYZqUrDsgogEUoMpZVZJL
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___OgQTGunYZqUrDsgogEUoMpZVZJL_8;
	// System.Int32 Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::BXRUazznMnLjBmUEMAlPSqVFAsz
	int32_t ___BXRUazznMnLjBmUEMAlPSqVFAsz_9;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::AYaINkvIvaTTiENOGyVYCIRtebJ
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___AYaINkvIvaTTiENOGyVYCIRtebJ_10;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ControllerMapWithAxes_DIBeNVGslRVnhCinpEEiWUVNYequ::chDEPKpKtufxNzXjtHogGLsHofQU
	RuntimeObject* ___chDEPKpKtufxNzXjtHogGLsHofQU_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_4() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___CPuSQMXEaFTdKTziCifqLUFakpe_4)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CPuSQMXEaFTdKTziCifqLUFakpe_4() const { return ___CPuSQMXEaFTdKTziCifqLUFakpe_4; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CPuSQMXEaFTdKTziCifqLUFakpe_4() { return &___CPuSQMXEaFTdKTziCifqLUFakpe_4; }
	inline void set_CPuSQMXEaFTdKTziCifqLUFakpe_4(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CPuSQMXEaFTdKTziCifqLUFakpe_4 = value;
		Il2CppCodeGenWriteBarrier((&___CPuSQMXEaFTdKTziCifqLUFakpe_4), value);
	}

	inline static int32_t get_offset_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() const { return ___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5() { return &___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5; }
	inline void set_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5 = value;
		Il2CppCodeGenWriteBarrier((&___xRSEXSmmPMfcXipPcqHmVjqYVhoG_5), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_OgQTGunYZqUrDsgogEUoMpZVZJL_8() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___OgQTGunYZqUrDsgogEUoMpZVZJL_8)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_OgQTGunYZqUrDsgogEUoMpZVZJL_8() const { return ___OgQTGunYZqUrDsgogEUoMpZVZJL_8; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_OgQTGunYZqUrDsgogEUoMpZVZJL_8() { return &___OgQTGunYZqUrDsgogEUoMpZVZJL_8; }
	inline void set_OgQTGunYZqUrDsgogEUoMpZVZJL_8(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___OgQTGunYZqUrDsgogEUoMpZVZJL_8 = value;
	}

	inline static int32_t get_offset_of_BXRUazznMnLjBmUEMAlPSqVFAsz_9() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___BXRUazznMnLjBmUEMAlPSqVFAsz_9)); }
	inline int32_t get_BXRUazznMnLjBmUEMAlPSqVFAsz_9() const { return ___BXRUazznMnLjBmUEMAlPSqVFAsz_9; }
	inline int32_t* get_address_of_BXRUazznMnLjBmUEMAlPSqVFAsz_9() { return &___BXRUazznMnLjBmUEMAlPSqVFAsz_9; }
	inline void set_BXRUazznMnLjBmUEMAlPSqVFAsz_9(int32_t value)
	{
		___BXRUazznMnLjBmUEMAlPSqVFAsz_9 = value;
	}

	inline static int32_t get_offset_of_AYaINkvIvaTTiENOGyVYCIRtebJ_10() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___AYaINkvIvaTTiENOGyVYCIRtebJ_10)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_AYaINkvIvaTTiENOGyVYCIRtebJ_10() const { return ___AYaINkvIvaTTiENOGyVYCIRtebJ_10; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_AYaINkvIvaTTiENOGyVYCIRtebJ_10() { return &___AYaINkvIvaTTiENOGyVYCIRtebJ_10; }
	inline void set_AYaINkvIvaTTiENOGyVYCIRtebJ_10(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___AYaINkvIvaTTiENOGyVYCIRtebJ_10 = value;
		Il2CppCodeGenWriteBarrier((&___AYaINkvIvaTTiENOGyVYCIRtebJ_10), value);
	}

	inline static int32_t get_offset_of_chDEPKpKtufxNzXjtHogGLsHofQU_11() { return static_cast<int32_t>(offsetof(DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC, ___chDEPKpKtufxNzXjtHogGLsHofQU_11)); }
	inline RuntimeObject* get_chDEPKpKtufxNzXjtHogGLsHofQU_11() const { return ___chDEPKpKtufxNzXjtHogGLsHofQU_11; }
	inline RuntimeObject** get_address_of_chDEPKpKtufxNzXjtHogGLsHofQU_11() { return &___chDEPKpKtufxNzXjtHogGLsHofQU_11; }
	inline void set_chDEPKpKtufxNzXjtHogGLsHofQU_11(RuntimeObject* value)
	{
		___chDEPKpKtufxNzXjtHogGLsHofQU_11 = value;
		Il2CppCodeGenWriteBarrier((&___chDEPKpKtufxNzXjtHogGLsHofQU_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIBENVGSLRVNHCINPEEIWUVNYEQU_T8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC_H
#ifndef VFCLLGMTAKHGPUAKFODBMPIWGUJ_T34311D9137275F99C568E14BD0633390464E9D88_H
#define VFCLLGMTAKHGPUAKFODBMPIWGUJ_T34311D9137275F99C568E14BD0633390464E9D88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ
struct  VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMapWithAxes Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerMap Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::eDZexKHojMIOIcGgWyZwyijReQJ
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___eDZexKHojMIOIcGgWyZwyijReQJ_4;
	// Rewired.ControllerMap Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::vEmKKUUwDIqfnqRwGeghcRhPDEr
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5;
	// System.Boolean Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::tCVxHXQhWEUGiHHMEurjbDspwep
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___tCVxHXQhWEUGiHHMEurjbDspwep_8;
	// Rewired.ControllerMapWithAxes Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::bMTQDqEhsggiAgeyaWKyEtpPurd
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___bMTQDqEhsggiAgeyaWKyEtpPurd_9;
	// System.Collections.Generic.IList`1<Rewired.ActionElementMap> Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::TuXqbkjfGMnpkuzGuDrLkwcmnBj
	RuntimeObject* ___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10;
	// System.Int32 Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::HgefhEqryspDVGJbRyJdhsDrbHC
	int32_t ___HgefhEqryspDVGJbRyJdhsDrbHC_11;
	// System.Int32 Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::NpcGkrehEogoInReCXlZWKvacYF
	int32_t ___NpcGkrehEogoInReCXlZWKvacYF_12;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::dmBFyXcxqbwWGjWNLrXhYoNNqWY
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13;
	// System.Int32 Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::wlrCnifeuDbCjwSXkGAHmhMcgLj
	int32_t ___wlrCnifeuDbCjwSXkGAHmhMcgLj_14;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::ammWRKrBRUxmlflzLFHJSbijAmC
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ammWRKrBRUxmlflzLFHJSbijAmC_15;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ControllerMapWithAxes_VFcLlGmTAkhGPUAkfoDBmPIWgUJ::NoTAyTxmULscBYhxegWobtJUNJhc
	RuntimeObject* ___NoTAyTxmULscBYhxegWobtJUNJhc_16;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_4() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___eDZexKHojMIOIcGgWyZwyijReQJ_4)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_eDZexKHojMIOIcGgWyZwyijReQJ_4() const { return ___eDZexKHojMIOIcGgWyZwyijReQJ_4; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_eDZexKHojMIOIcGgWyZwyijReQJ_4() { return &___eDZexKHojMIOIcGgWyZwyijReQJ_4; }
	inline void set_eDZexKHojMIOIcGgWyZwyijReQJ_4(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___eDZexKHojMIOIcGgWyZwyijReQJ_4 = value;
		Il2CppCodeGenWriteBarrier((&___eDZexKHojMIOIcGgWyZwyijReQJ_4), value);
	}

	inline static int32_t get_offset_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() const { return ___vEmKKUUwDIqfnqRwGeghcRhPDEr_5; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5() { return &___vEmKKUUwDIqfnqRwGeghcRhPDEr_5; }
	inline void set_vEmKKUUwDIqfnqRwGeghcRhPDEr_5(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___vEmKKUUwDIqfnqRwGeghcRhPDEr_5 = value;
		Il2CppCodeGenWriteBarrier((&___vEmKKUUwDIqfnqRwGeghcRhPDEr_5), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_tCVxHXQhWEUGiHHMEurjbDspwep_8() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___tCVxHXQhWEUGiHHMEurjbDspwep_8)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_tCVxHXQhWEUGiHHMEurjbDspwep_8() const { return ___tCVxHXQhWEUGiHHMEurjbDspwep_8; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_tCVxHXQhWEUGiHHMEurjbDspwep_8() { return &___tCVxHXQhWEUGiHHMEurjbDspwep_8; }
	inline void set_tCVxHXQhWEUGiHHMEurjbDspwep_8(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___tCVxHXQhWEUGiHHMEurjbDspwep_8 = value;
	}

	inline static int32_t get_offset_of_bMTQDqEhsggiAgeyaWKyEtpPurd_9() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___bMTQDqEhsggiAgeyaWKyEtpPurd_9)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_bMTQDqEhsggiAgeyaWKyEtpPurd_9() const { return ___bMTQDqEhsggiAgeyaWKyEtpPurd_9; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_bMTQDqEhsggiAgeyaWKyEtpPurd_9() { return &___bMTQDqEhsggiAgeyaWKyEtpPurd_9; }
	inline void set_bMTQDqEhsggiAgeyaWKyEtpPurd_9(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___bMTQDqEhsggiAgeyaWKyEtpPurd_9 = value;
		Il2CppCodeGenWriteBarrier((&___bMTQDqEhsggiAgeyaWKyEtpPurd_9), value);
	}

	inline static int32_t get_offset_of_TuXqbkjfGMnpkuzGuDrLkwcmnBj_10() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10)); }
	inline RuntimeObject* get_TuXqbkjfGMnpkuzGuDrLkwcmnBj_10() const { return ___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10; }
	inline RuntimeObject** get_address_of_TuXqbkjfGMnpkuzGuDrLkwcmnBj_10() { return &___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10; }
	inline void set_TuXqbkjfGMnpkuzGuDrLkwcmnBj_10(RuntimeObject* value)
	{
		___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10 = value;
		Il2CppCodeGenWriteBarrier((&___TuXqbkjfGMnpkuzGuDrLkwcmnBj_10), value);
	}

	inline static int32_t get_offset_of_HgefhEqryspDVGJbRyJdhsDrbHC_11() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___HgefhEqryspDVGJbRyJdhsDrbHC_11)); }
	inline int32_t get_HgefhEqryspDVGJbRyJdhsDrbHC_11() const { return ___HgefhEqryspDVGJbRyJdhsDrbHC_11; }
	inline int32_t* get_address_of_HgefhEqryspDVGJbRyJdhsDrbHC_11() { return &___HgefhEqryspDVGJbRyJdhsDrbHC_11; }
	inline void set_HgefhEqryspDVGJbRyJdhsDrbHC_11(int32_t value)
	{
		___HgefhEqryspDVGJbRyJdhsDrbHC_11 = value;
	}

	inline static int32_t get_offset_of_NpcGkrehEogoInReCXlZWKvacYF_12() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___NpcGkrehEogoInReCXlZWKvacYF_12)); }
	inline int32_t get_NpcGkrehEogoInReCXlZWKvacYF_12() const { return ___NpcGkrehEogoInReCXlZWKvacYF_12; }
	inline int32_t* get_address_of_NpcGkrehEogoInReCXlZWKvacYF_12() { return &___NpcGkrehEogoInReCXlZWKvacYF_12; }
	inline void set_NpcGkrehEogoInReCXlZWKvacYF_12(int32_t value)
	{
		___NpcGkrehEogoInReCXlZWKvacYF_12 = value;
	}

	inline static int32_t get_offset_of_dmBFyXcxqbwWGjWNLrXhYoNNqWY_13() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_dmBFyXcxqbwWGjWNLrXhYoNNqWY_13() const { return ___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_dmBFyXcxqbwWGjWNLrXhYoNNqWY_13() { return &___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13; }
	inline void set_dmBFyXcxqbwWGjWNLrXhYoNNqWY_13(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13 = value;
		Il2CppCodeGenWriteBarrier((&___dmBFyXcxqbwWGjWNLrXhYoNNqWY_13), value);
	}

	inline static int32_t get_offset_of_wlrCnifeuDbCjwSXkGAHmhMcgLj_14() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___wlrCnifeuDbCjwSXkGAHmhMcgLj_14)); }
	inline int32_t get_wlrCnifeuDbCjwSXkGAHmhMcgLj_14() const { return ___wlrCnifeuDbCjwSXkGAHmhMcgLj_14; }
	inline int32_t* get_address_of_wlrCnifeuDbCjwSXkGAHmhMcgLj_14() { return &___wlrCnifeuDbCjwSXkGAHmhMcgLj_14; }
	inline void set_wlrCnifeuDbCjwSXkGAHmhMcgLj_14(int32_t value)
	{
		___wlrCnifeuDbCjwSXkGAHmhMcgLj_14 = value;
	}

	inline static int32_t get_offset_of_ammWRKrBRUxmlflzLFHJSbijAmC_15() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___ammWRKrBRUxmlflzLFHJSbijAmC_15)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ammWRKrBRUxmlflzLFHJSbijAmC_15() const { return ___ammWRKrBRUxmlflzLFHJSbijAmC_15; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ammWRKrBRUxmlflzLFHJSbijAmC_15() { return &___ammWRKrBRUxmlflzLFHJSbijAmC_15; }
	inline void set_ammWRKrBRUxmlflzLFHJSbijAmC_15(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ammWRKrBRUxmlflzLFHJSbijAmC_15 = value;
		Il2CppCodeGenWriteBarrier((&___ammWRKrBRUxmlflzLFHJSbijAmC_15), value);
	}

	inline static int32_t get_offset_of_NoTAyTxmULscBYhxegWobtJUNJhc_16() { return static_cast<int32_t>(offsetof(VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88, ___NoTAyTxmULscBYhxegWobtJUNJhc_16)); }
	inline RuntimeObject* get_NoTAyTxmULscBYhxegWobtJUNJhc_16() const { return ___NoTAyTxmULscBYhxegWobtJUNJhc_16; }
	inline RuntimeObject** get_address_of_NoTAyTxmULscBYhxegWobtJUNJhc_16() { return &___NoTAyTxmULscBYhxegWobtJUNJhc_16; }
	inline void set_NoTAyTxmULscBYhxegWobtJUNJhc_16(RuntimeObject* value)
	{
		___NoTAyTxmULscBYhxegWobtJUNJhc_16 = value;
		Il2CppCodeGenWriteBarrier((&___NoTAyTxmULscBYhxegWobtJUNJhc_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VFCLLGMTAKHGPUAKFODBMPIWGUJ_T34311D9137275F99C568E14BD0633390464E9D88_H
#ifndef AYGNSNODBROPOVJLHYYIOSMOBOB_T9685055A93FB30E16C426D0AD041185215CDE15F_H
#define AYGNSNODBROPOVJLHYYIOSMOBOB_T9685055A93FB30E16C426D0AD041185215CDE15F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB
struct  aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerMapWithAxes Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_4;
	// Rewired.ElementAssignmentConflictCheck Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5;
	// System.Boolean Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// Rewired.ElementAssignmentConflictInfo Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::ohYdFTWPrnbUbCJgtNMBewHaRsb
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___ohYdFTWPrnbUbCJgtNMBewHaRsb_8;
	// Rewired.ElementAssignment Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::IhtfGVHdmBpoQcPyjpIpgwJziSzq
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___IhtfGVHdmBpoQcPyjpIpgwJziSzq_9;
	// System.Int32 Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::vYyiYpDzEJMjYXRXZFufmzyWdNU
	int32_t ___vYyiYpDzEJMjYXRXZFufmzyWdNU_10;
	// Rewired.ActionElementMap Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::LxqXeBOlKBfqTJPpnWzIVfbORWB
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___LxqXeBOlKBfqTJPpnWzIVfbORWB_11;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ControllerMapWithAxes_aYgnSNODbroPOvjlhYyiOsMobOB::EnyxBHqQzSOfLzNTXVsnPwUnDTg
	RuntimeObject* ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___RXwTkvaioBOXLvHfDHkciCcOgzY_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_4() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_4 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_ohYdFTWPrnbUbCJgtNMBewHaRsb_8() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___ohYdFTWPrnbUbCJgtNMBewHaRsb_8)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_ohYdFTWPrnbUbCJgtNMBewHaRsb_8() const { return ___ohYdFTWPrnbUbCJgtNMBewHaRsb_8; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_ohYdFTWPrnbUbCJgtNMBewHaRsb_8() { return &___ohYdFTWPrnbUbCJgtNMBewHaRsb_8; }
	inline void set_ohYdFTWPrnbUbCJgtNMBewHaRsb_8(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___ohYdFTWPrnbUbCJgtNMBewHaRsb_8 = value;
	}

	inline static int32_t get_offset_of_IhtfGVHdmBpoQcPyjpIpgwJziSzq_9() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___IhtfGVHdmBpoQcPyjpIpgwJziSzq_9)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_IhtfGVHdmBpoQcPyjpIpgwJziSzq_9() const { return ___IhtfGVHdmBpoQcPyjpIpgwJziSzq_9; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_IhtfGVHdmBpoQcPyjpIpgwJziSzq_9() { return &___IhtfGVHdmBpoQcPyjpIpgwJziSzq_9; }
	inline void set_IhtfGVHdmBpoQcPyjpIpgwJziSzq_9(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___IhtfGVHdmBpoQcPyjpIpgwJziSzq_9 = value;
	}

	inline static int32_t get_offset_of_vYyiYpDzEJMjYXRXZFufmzyWdNU_10() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___vYyiYpDzEJMjYXRXZFufmzyWdNU_10)); }
	inline int32_t get_vYyiYpDzEJMjYXRXZFufmzyWdNU_10() const { return ___vYyiYpDzEJMjYXRXZFufmzyWdNU_10; }
	inline int32_t* get_address_of_vYyiYpDzEJMjYXRXZFufmzyWdNU_10() { return &___vYyiYpDzEJMjYXRXZFufmzyWdNU_10; }
	inline void set_vYyiYpDzEJMjYXRXZFufmzyWdNU_10(int32_t value)
	{
		___vYyiYpDzEJMjYXRXZFufmzyWdNU_10 = value;
	}

	inline static int32_t get_offset_of_LxqXeBOlKBfqTJPpnWzIVfbORWB_11() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___LxqXeBOlKBfqTJPpnWzIVfbORWB_11)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_LxqXeBOlKBfqTJPpnWzIVfbORWB_11() const { return ___LxqXeBOlKBfqTJPpnWzIVfbORWB_11; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_LxqXeBOlKBfqTJPpnWzIVfbORWB_11() { return &___LxqXeBOlKBfqTJPpnWzIVfbORWB_11; }
	inline void set_LxqXeBOlKBfqTJPpnWzIVfbORWB_11(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___LxqXeBOlKBfqTJPpnWzIVfbORWB_11 = value;
		Il2CppCodeGenWriteBarrier((&___LxqXeBOlKBfqTJPpnWzIVfbORWB_11), value);
	}

	inline static int32_t get_offset_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_12() { return static_cast<int32_t>(offsetof(aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F, ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12)); }
	inline RuntimeObject* get_EnyxBHqQzSOfLzNTXVsnPwUnDTg_12() const { return ___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12; }
	inline RuntimeObject** get_address_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_12() { return &___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12; }
	inline void set_EnyxBHqQzSOfLzNTXVsnPwUnDTg_12(RuntimeObject* value)
	{
		___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12 = value;
		Il2CppCodeGenWriteBarrier((&___EnyxBHqQzSOfLzNTXVsnPwUnDTg_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AYGNSNODBROPOVJLHYYIOSMOBOB_T9685055A93FB30E16C426D0AD041185215CDE15F_H
#ifndef HARDWARECONTROLLERMAP_GAME_T941EA366B3E6D843EBE08EEBA0930D8A338B49D3_H
#define HARDWARECONTROLLERMAP_GAME_T941EA366B3E6D843EBE08EEBA0930D8A338B49D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HardwareControllerMap_Game
struct  HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3  : public RuntimeObject
{
public:
	// System.String Rewired.HardwareControllerMap_Game::controllerName
	String_t* ___controllerName_0;
	// Rewired.HardwareControllerMapIdentifier Rewired.HardwareControllerMap_Game::hardwareMapIdentifier
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  ___hardwareMapIdentifier_1;
	// System.Int32 Rewired.HardwareControllerMap_Game::customControllerSourceId
	int32_t ___customControllerSourceId_2;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::elementIdentifiers
	ADictionary_2_t46071D66C4378848A3237FDEB0CA63F81171558A * ___elementIdentifiers_3;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareControllerMap_Game::elementIdentifiers_cache
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___elementIdentifiers_cache_4;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareControllerMap_Game::buttonElementIdentifiers_cache
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___buttonElementIdentifiers_cache_5;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareControllerMap_Game::axisElementIdentifiers_cache
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___axisElementIdentifiers_cache_6;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareControllerMap_Game::axis2DElementIdentifiers_cache
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___axis2DElementIdentifiers_cache_7;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareControllerMap_Game::hatElementIdentifiers_cache
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___hatElementIdentifiers_cache_8;
	// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::elementIdentifiers_readOnly
	RuntimeObject* ___elementIdentifiers_readOnly_9;
	// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::buttonElementIdentifiers_readOnly
	RuntimeObject* ___buttonElementIdentifiers_readOnly_10;
	// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::axisElementIdentifiers_readOnly
	RuntimeObject* ___axisElementIdentifiers_readOnly_11;
	// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::axis2DElementIdentifiers_readOnly
	RuntimeObject* ___axis2DElementIdentifiers_readOnly_12;
	// System.Collections.Generic.IList`1<Rewired.ControllerElementIdentifier> Rewired.HardwareControllerMap_Game::hatElementIdentifiers_readOnly
	RuntimeObject* ___hatElementIdentifiers_readOnly_13;
	// System.Int32[] Rewired.HardwareControllerMap_Game::buttonElementIdentifierIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buttonElementIdentifierIds_14;
	// System.Int32[] Rewired.HardwareControllerMap_Game::axisElementIdentifierIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___axisElementIdentifierIds_15;
	// System.Int32[] Rewired.HardwareControllerMap_Game::axis2DElementIdentifierIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___axis2DElementIdentifierIds_16;
	// System.Int32[] Rewired.HardwareControllerMap_Game::hatElementIdentifierIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hatElementIdentifierIds_17;
	// System.Int32 Rewired.HardwareControllerMap_Game::elementIdentifierCount
	int32_t ___elementIdentifierCount_18;
	// System.Int32 Rewired.HardwareControllerMap_Game::axisCount
	int32_t ___axisCount_19;
	// System.Int32 Rewired.HardwareControllerMap_Game::buttonCount
	int32_t ___buttonCount_20;
	// System.Int32 Rewired.HardwareControllerMap_Game::compoundElementCount
	int32_t ___compoundElementCount_21;
	// System.Int32 Rewired.HardwareControllerMap_Game::axis2DCount
	int32_t ___axis2DCount_22;
	// System.Int32 Rewired.HardwareControllerMap_Game::hatCount
	int32_t ___hatCount_23;
	// Rewired.JoystickType[] Rewired.HardwareControllerMap_Game::joystickTypes
	JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* ___joystickTypes_24;
	// Rewired.AxisCalibrationData[] Rewired.HardwareControllerMap_Game::hwAxisCalibrationData
	AxisCalibrationDataU5BU5D_tB487D04860D203BA3498E0992EEE34BCD1E02F3B* ___hwAxisCalibrationData_25;
	// Rewired.AxisRange[] Rewired.HardwareControllerMap_Game::hwAxisRanges
	AxisRangeU5BU5D_t56B8222EEAB047F19E70E2F90E6CD0483116A157* ___hwAxisRanges_26;
	// Rewired.Data.Mapping.HardwareAxisInfo[] Rewired.HardwareControllerMap_Game::hwAxisInfo
	HardwareAxisInfoU5BU5D_tAA6150D1009F709292A37DD9D80B69A8070DBF41* ___hwAxisInfo_27;
	// Rewired.Data.Mapping.HardwareButtonInfo[] Rewired.HardwareControllerMap_Game::hwButtonInfo
	HardwareButtonInfoU5BU5D_t15045FEBBCBE26B33AE903031072C697D11140A5* ___hwButtonInfo_28;
	// Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement[] Rewired.HardwareControllerMap_Game::compoundElements
	CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* ___compoundElements_29;

public:
	inline static int32_t get_offset_of_controllerName_0() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___controllerName_0)); }
	inline String_t* get_controllerName_0() const { return ___controllerName_0; }
	inline String_t** get_address_of_controllerName_0() { return &___controllerName_0; }
	inline void set_controllerName_0(String_t* value)
	{
		___controllerName_0 = value;
		Il2CppCodeGenWriteBarrier((&___controllerName_0), value);
	}

	inline static int32_t get_offset_of_hardwareMapIdentifier_1() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hardwareMapIdentifier_1)); }
	inline HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  get_hardwareMapIdentifier_1() const { return ___hardwareMapIdentifier_1; }
	inline HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4 * get_address_of_hardwareMapIdentifier_1() { return &___hardwareMapIdentifier_1; }
	inline void set_hardwareMapIdentifier_1(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  value)
	{
		___hardwareMapIdentifier_1 = value;
	}

	inline static int32_t get_offset_of_customControllerSourceId_2() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___customControllerSourceId_2)); }
	inline int32_t get_customControllerSourceId_2() const { return ___customControllerSourceId_2; }
	inline int32_t* get_address_of_customControllerSourceId_2() { return &___customControllerSourceId_2; }
	inline void set_customControllerSourceId_2(int32_t value)
	{
		___customControllerSourceId_2 = value;
	}

	inline static int32_t get_offset_of_elementIdentifiers_3() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___elementIdentifiers_3)); }
	inline ADictionary_2_t46071D66C4378848A3237FDEB0CA63F81171558A * get_elementIdentifiers_3() const { return ___elementIdentifiers_3; }
	inline ADictionary_2_t46071D66C4378848A3237FDEB0CA63F81171558A ** get_address_of_elementIdentifiers_3() { return &___elementIdentifiers_3; }
	inline void set_elementIdentifiers_3(ADictionary_2_t46071D66C4378848A3237FDEB0CA63F81171558A * value)
	{
		___elementIdentifiers_3 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_3), value);
	}

	inline static int32_t get_offset_of_elementIdentifiers_cache_4() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___elementIdentifiers_cache_4)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_elementIdentifiers_cache_4() const { return ___elementIdentifiers_cache_4; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_elementIdentifiers_cache_4() { return &___elementIdentifiers_cache_4; }
	inline void set_elementIdentifiers_cache_4(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___elementIdentifiers_cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_cache_4), value);
	}

	inline static int32_t get_offset_of_buttonElementIdentifiers_cache_5() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___buttonElementIdentifiers_cache_5)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_buttonElementIdentifiers_cache_5() const { return ___buttonElementIdentifiers_cache_5; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_buttonElementIdentifiers_cache_5() { return &___buttonElementIdentifiers_cache_5; }
	inline void set_buttonElementIdentifiers_cache_5(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___buttonElementIdentifiers_cache_5 = value;
		Il2CppCodeGenWriteBarrier((&___buttonElementIdentifiers_cache_5), value);
	}

	inline static int32_t get_offset_of_axisElementIdentifiers_cache_6() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axisElementIdentifiers_cache_6)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_axisElementIdentifiers_cache_6() const { return ___axisElementIdentifiers_cache_6; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_axisElementIdentifiers_cache_6() { return &___axisElementIdentifiers_cache_6; }
	inline void set_axisElementIdentifiers_cache_6(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___axisElementIdentifiers_cache_6 = value;
		Il2CppCodeGenWriteBarrier((&___axisElementIdentifiers_cache_6), value);
	}

	inline static int32_t get_offset_of_axis2DElementIdentifiers_cache_7() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axis2DElementIdentifiers_cache_7)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_axis2DElementIdentifiers_cache_7() const { return ___axis2DElementIdentifiers_cache_7; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_axis2DElementIdentifiers_cache_7() { return &___axis2DElementIdentifiers_cache_7; }
	inline void set_axis2DElementIdentifiers_cache_7(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___axis2DElementIdentifiers_cache_7 = value;
		Il2CppCodeGenWriteBarrier((&___axis2DElementIdentifiers_cache_7), value);
	}

	inline static int32_t get_offset_of_hatElementIdentifiers_cache_8() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hatElementIdentifiers_cache_8)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_hatElementIdentifiers_cache_8() const { return ___hatElementIdentifiers_cache_8; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_hatElementIdentifiers_cache_8() { return &___hatElementIdentifiers_cache_8; }
	inline void set_hatElementIdentifiers_cache_8(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___hatElementIdentifiers_cache_8 = value;
		Il2CppCodeGenWriteBarrier((&___hatElementIdentifiers_cache_8), value);
	}

	inline static int32_t get_offset_of_elementIdentifiers_readOnly_9() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___elementIdentifiers_readOnly_9)); }
	inline RuntimeObject* get_elementIdentifiers_readOnly_9() const { return ___elementIdentifiers_readOnly_9; }
	inline RuntimeObject** get_address_of_elementIdentifiers_readOnly_9() { return &___elementIdentifiers_readOnly_9; }
	inline void set_elementIdentifiers_readOnly_9(RuntimeObject* value)
	{
		___elementIdentifiers_readOnly_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_readOnly_9), value);
	}

	inline static int32_t get_offset_of_buttonElementIdentifiers_readOnly_10() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___buttonElementIdentifiers_readOnly_10)); }
	inline RuntimeObject* get_buttonElementIdentifiers_readOnly_10() const { return ___buttonElementIdentifiers_readOnly_10; }
	inline RuntimeObject** get_address_of_buttonElementIdentifiers_readOnly_10() { return &___buttonElementIdentifiers_readOnly_10; }
	inline void set_buttonElementIdentifiers_readOnly_10(RuntimeObject* value)
	{
		___buttonElementIdentifiers_readOnly_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonElementIdentifiers_readOnly_10), value);
	}

	inline static int32_t get_offset_of_axisElementIdentifiers_readOnly_11() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axisElementIdentifiers_readOnly_11)); }
	inline RuntimeObject* get_axisElementIdentifiers_readOnly_11() const { return ___axisElementIdentifiers_readOnly_11; }
	inline RuntimeObject** get_address_of_axisElementIdentifiers_readOnly_11() { return &___axisElementIdentifiers_readOnly_11; }
	inline void set_axisElementIdentifiers_readOnly_11(RuntimeObject* value)
	{
		___axisElementIdentifiers_readOnly_11 = value;
		Il2CppCodeGenWriteBarrier((&___axisElementIdentifiers_readOnly_11), value);
	}

	inline static int32_t get_offset_of_axis2DElementIdentifiers_readOnly_12() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axis2DElementIdentifiers_readOnly_12)); }
	inline RuntimeObject* get_axis2DElementIdentifiers_readOnly_12() const { return ___axis2DElementIdentifiers_readOnly_12; }
	inline RuntimeObject** get_address_of_axis2DElementIdentifiers_readOnly_12() { return &___axis2DElementIdentifiers_readOnly_12; }
	inline void set_axis2DElementIdentifiers_readOnly_12(RuntimeObject* value)
	{
		___axis2DElementIdentifiers_readOnly_12 = value;
		Il2CppCodeGenWriteBarrier((&___axis2DElementIdentifiers_readOnly_12), value);
	}

	inline static int32_t get_offset_of_hatElementIdentifiers_readOnly_13() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hatElementIdentifiers_readOnly_13)); }
	inline RuntimeObject* get_hatElementIdentifiers_readOnly_13() const { return ___hatElementIdentifiers_readOnly_13; }
	inline RuntimeObject** get_address_of_hatElementIdentifiers_readOnly_13() { return &___hatElementIdentifiers_readOnly_13; }
	inline void set_hatElementIdentifiers_readOnly_13(RuntimeObject* value)
	{
		___hatElementIdentifiers_readOnly_13 = value;
		Il2CppCodeGenWriteBarrier((&___hatElementIdentifiers_readOnly_13), value);
	}

	inline static int32_t get_offset_of_buttonElementIdentifierIds_14() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___buttonElementIdentifierIds_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buttonElementIdentifierIds_14() const { return ___buttonElementIdentifierIds_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buttonElementIdentifierIds_14() { return &___buttonElementIdentifierIds_14; }
	inline void set_buttonElementIdentifierIds_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buttonElementIdentifierIds_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonElementIdentifierIds_14), value);
	}

	inline static int32_t get_offset_of_axisElementIdentifierIds_15() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axisElementIdentifierIds_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_axisElementIdentifierIds_15() const { return ___axisElementIdentifierIds_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_axisElementIdentifierIds_15() { return &___axisElementIdentifierIds_15; }
	inline void set_axisElementIdentifierIds_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___axisElementIdentifierIds_15 = value;
		Il2CppCodeGenWriteBarrier((&___axisElementIdentifierIds_15), value);
	}

	inline static int32_t get_offset_of_axis2DElementIdentifierIds_16() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axis2DElementIdentifierIds_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_axis2DElementIdentifierIds_16() const { return ___axis2DElementIdentifierIds_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_axis2DElementIdentifierIds_16() { return &___axis2DElementIdentifierIds_16; }
	inline void set_axis2DElementIdentifierIds_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___axis2DElementIdentifierIds_16 = value;
		Il2CppCodeGenWriteBarrier((&___axis2DElementIdentifierIds_16), value);
	}

	inline static int32_t get_offset_of_hatElementIdentifierIds_17() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hatElementIdentifierIds_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hatElementIdentifierIds_17() const { return ___hatElementIdentifierIds_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hatElementIdentifierIds_17() { return &___hatElementIdentifierIds_17; }
	inline void set_hatElementIdentifierIds_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hatElementIdentifierIds_17 = value;
		Il2CppCodeGenWriteBarrier((&___hatElementIdentifierIds_17), value);
	}

	inline static int32_t get_offset_of_elementIdentifierCount_18() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___elementIdentifierCount_18)); }
	inline int32_t get_elementIdentifierCount_18() const { return ___elementIdentifierCount_18; }
	inline int32_t* get_address_of_elementIdentifierCount_18() { return &___elementIdentifierCount_18; }
	inline void set_elementIdentifierCount_18(int32_t value)
	{
		___elementIdentifierCount_18 = value;
	}

	inline static int32_t get_offset_of_axisCount_19() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axisCount_19)); }
	inline int32_t get_axisCount_19() const { return ___axisCount_19; }
	inline int32_t* get_address_of_axisCount_19() { return &___axisCount_19; }
	inline void set_axisCount_19(int32_t value)
	{
		___axisCount_19 = value;
	}

	inline static int32_t get_offset_of_buttonCount_20() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___buttonCount_20)); }
	inline int32_t get_buttonCount_20() const { return ___buttonCount_20; }
	inline int32_t* get_address_of_buttonCount_20() { return &___buttonCount_20; }
	inline void set_buttonCount_20(int32_t value)
	{
		___buttonCount_20 = value;
	}

	inline static int32_t get_offset_of_compoundElementCount_21() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___compoundElementCount_21)); }
	inline int32_t get_compoundElementCount_21() const { return ___compoundElementCount_21; }
	inline int32_t* get_address_of_compoundElementCount_21() { return &___compoundElementCount_21; }
	inline void set_compoundElementCount_21(int32_t value)
	{
		___compoundElementCount_21 = value;
	}

	inline static int32_t get_offset_of_axis2DCount_22() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___axis2DCount_22)); }
	inline int32_t get_axis2DCount_22() const { return ___axis2DCount_22; }
	inline int32_t* get_address_of_axis2DCount_22() { return &___axis2DCount_22; }
	inline void set_axis2DCount_22(int32_t value)
	{
		___axis2DCount_22 = value;
	}

	inline static int32_t get_offset_of_hatCount_23() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hatCount_23)); }
	inline int32_t get_hatCount_23() const { return ___hatCount_23; }
	inline int32_t* get_address_of_hatCount_23() { return &___hatCount_23; }
	inline void set_hatCount_23(int32_t value)
	{
		___hatCount_23 = value;
	}

	inline static int32_t get_offset_of_joystickTypes_24() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___joystickTypes_24)); }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* get_joystickTypes_24() const { return ___joystickTypes_24; }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA** get_address_of_joystickTypes_24() { return &___joystickTypes_24; }
	inline void set_joystickTypes_24(JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* value)
	{
		___joystickTypes_24 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTypes_24), value);
	}

	inline static int32_t get_offset_of_hwAxisCalibrationData_25() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hwAxisCalibrationData_25)); }
	inline AxisCalibrationDataU5BU5D_tB487D04860D203BA3498E0992EEE34BCD1E02F3B* get_hwAxisCalibrationData_25() const { return ___hwAxisCalibrationData_25; }
	inline AxisCalibrationDataU5BU5D_tB487D04860D203BA3498E0992EEE34BCD1E02F3B** get_address_of_hwAxisCalibrationData_25() { return &___hwAxisCalibrationData_25; }
	inline void set_hwAxisCalibrationData_25(AxisCalibrationDataU5BU5D_tB487D04860D203BA3498E0992EEE34BCD1E02F3B* value)
	{
		___hwAxisCalibrationData_25 = value;
		Il2CppCodeGenWriteBarrier((&___hwAxisCalibrationData_25), value);
	}

	inline static int32_t get_offset_of_hwAxisRanges_26() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hwAxisRanges_26)); }
	inline AxisRangeU5BU5D_t56B8222EEAB047F19E70E2F90E6CD0483116A157* get_hwAxisRanges_26() const { return ___hwAxisRanges_26; }
	inline AxisRangeU5BU5D_t56B8222EEAB047F19E70E2F90E6CD0483116A157** get_address_of_hwAxisRanges_26() { return &___hwAxisRanges_26; }
	inline void set_hwAxisRanges_26(AxisRangeU5BU5D_t56B8222EEAB047F19E70E2F90E6CD0483116A157* value)
	{
		___hwAxisRanges_26 = value;
		Il2CppCodeGenWriteBarrier((&___hwAxisRanges_26), value);
	}

	inline static int32_t get_offset_of_hwAxisInfo_27() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hwAxisInfo_27)); }
	inline HardwareAxisInfoU5BU5D_tAA6150D1009F709292A37DD9D80B69A8070DBF41* get_hwAxisInfo_27() const { return ___hwAxisInfo_27; }
	inline HardwareAxisInfoU5BU5D_tAA6150D1009F709292A37DD9D80B69A8070DBF41** get_address_of_hwAxisInfo_27() { return &___hwAxisInfo_27; }
	inline void set_hwAxisInfo_27(HardwareAxisInfoU5BU5D_tAA6150D1009F709292A37DD9D80B69A8070DBF41* value)
	{
		___hwAxisInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___hwAxisInfo_27), value);
	}

	inline static int32_t get_offset_of_hwButtonInfo_28() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___hwButtonInfo_28)); }
	inline HardwareButtonInfoU5BU5D_t15045FEBBCBE26B33AE903031072C697D11140A5* get_hwButtonInfo_28() const { return ___hwButtonInfo_28; }
	inline HardwareButtonInfoU5BU5D_t15045FEBBCBE26B33AE903031072C697D11140A5** get_address_of_hwButtonInfo_28() { return &___hwButtonInfo_28; }
	inline void set_hwButtonInfo_28(HardwareButtonInfoU5BU5D_t15045FEBBCBE26B33AE903031072C697D11140A5* value)
	{
		___hwButtonInfo_28 = value;
		Il2CppCodeGenWriteBarrier((&___hwButtonInfo_28), value);
	}

	inline static int32_t get_offset_of_compoundElements_29() { return static_cast<int32_t>(offsetof(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3, ___compoundElements_29)); }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* get_compoundElements_29() const { return ___compoundElements_29; }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C** get_address_of_compoundElements_29() { return &___compoundElements_29; }
	inline void set_compoundElements_29(CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* value)
	{
		___compoundElements_29 = value;
		Il2CppCodeGenWriteBarrier((&___compoundElements_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWARECONTROLLERMAP_GAME_T941EA366B3E6D843EBE08EEBA0930D8A338B49D3_H
#ifndef HARDWAREJOYSTICKMAP_INPUTMANAGER_TCBD3AB46AC703AF5780510DD95E1627388F6D9A0_H
#define HARDWAREJOYSTICKMAP_INPUTMANAGER_TCBD3AB46AC703AF5780510DD95E1627388F6D9A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HardwareJoystickMap_InputManager
struct  HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0  : public RuntimeObject
{
public:
	// System.String Rewired.HardwareJoystickMap_InputManager::controllerName
	String_t* ___controllerName_0;
	// Rewired.HardwareControllerMapIdentifier Rewired.HardwareJoystickMap_InputManager::hardwareMapIdentifier
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  ___hardwareMapIdentifier_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform Rewired.HardwareJoystickMap_InputManager::map
	Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * ___map_2;
	// System.Int32 Rewired.HardwareJoystickMap_InputManager::buttonCount
	int32_t ___buttonCount_3;
	// System.Int32 Rewired.HardwareJoystickMap_InputManager::axisCount
	int32_t ___axisCount_4;
	// Rewired.ControllerElementIdentifier[] Rewired.HardwareJoystickMap_InputManager::elementIdentifiers
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___elementIdentifiers_5;
	// Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement[] Rewired.HardwareJoystickMap_InputManager::compoundElements
	CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* ___compoundElements_6;
	// System.Boolean Rewired.HardwareJoystickMap_InputManager::useSystemName
	bool ___useSystemName_7;
	// System.Boolean Rewired.HardwareJoystickMap_InputManager::isUnknownController
	bool ___isUnknownController_8;
	// Rewired.JoystickType[] Rewired.HardwareJoystickMap_InputManager::joystickTypes
	JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* ___joystickTypes_9;

public:
	inline static int32_t get_offset_of_controllerName_0() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___controllerName_0)); }
	inline String_t* get_controllerName_0() const { return ___controllerName_0; }
	inline String_t** get_address_of_controllerName_0() { return &___controllerName_0; }
	inline void set_controllerName_0(String_t* value)
	{
		___controllerName_0 = value;
		Il2CppCodeGenWriteBarrier((&___controllerName_0), value);
	}

	inline static int32_t get_offset_of_hardwareMapIdentifier_1() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___hardwareMapIdentifier_1)); }
	inline HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  get_hardwareMapIdentifier_1() const { return ___hardwareMapIdentifier_1; }
	inline HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4 * get_address_of_hardwareMapIdentifier_1() { return &___hardwareMapIdentifier_1; }
	inline void set_hardwareMapIdentifier_1(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4  value)
	{
		___hardwareMapIdentifier_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___map_2)); }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * get_map_2() const { return ___map_2; }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_buttonCount_3() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___buttonCount_3)); }
	inline int32_t get_buttonCount_3() const { return ___buttonCount_3; }
	inline int32_t* get_address_of_buttonCount_3() { return &___buttonCount_3; }
	inline void set_buttonCount_3(int32_t value)
	{
		___buttonCount_3 = value;
	}

	inline static int32_t get_offset_of_axisCount_4() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___axisCount_4)); }
	inline int32_t get_axisCount_4() const { return ___axisCount_4; }
	inline int32_t* get_address_of_axisCount_4() { return &___axisCount_4; }
	inline void set_axisCount_4(int32_t value)
	{
		___axisCount_4 = value;
	}

	inline static int32_t get_offset_of_elementIdentifiers_5() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___elementIdentifiers_5)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_elementIdentifiers_5() const { return ___elementIdentifiers_5; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_elementIdentifiers_5() { return &___elementIdentifiers_5; }
	inline void set_elementIdentifiers_5(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___elementIdentifiers_5 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_5), value);
	}

	inline static int32_t get_offset_of_compoundElements_6() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___compoundElements_6)); }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* get_compoundElements_6() const { return ___compoundElements_6; }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C** get_address_of_compoundElements_6() { return &___compoundElements_6; }
	inline void set_compoundElements_6(CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* value)
	{
		___compoundElements_6 = value;
		Il2CppCodeGenWriteBarrier((&___compoundElements_6), value);
	}

	inline static int32_t get_offset_of_useSystemName_7() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___useSystemName_7)); }
	inline bool get_useSystemName_7() const { return ___useSystemName_7; }
	inline bool* get_address_of_useSystemName_7() { return &___useSystemName_7; }
	inline void set_useSystemName_7(bool value)
	{
		___useSystemName_7 = value;
	}

	inline static int32_t get_offset_of_isUnknownController_8() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___isUnknownController_8)); }
	inline bool get_isUnknownController_8() const { return ___isUnknownController_8; }
	inline bool* get_address_of_isUnknownController_8() { return &___isUnknownController_8; }
	inline void set_isUnknownController_8(bool value)
	{
		___isUnknownController_8 = value;
	}

	inline static int32_t get_offset_of_joystickTypes_9() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0, ___joystickTypes_9)); }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* get_joystickTypes_9() const { return ___joystickTypes_9; }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA** get_address_of_joystickTypes_9() { return &___joystickTypes_9; }
	inline void set_joystickTypes_9(JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* value)
	{
		___joystickTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTypes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREJOYSTICKMAP_INPUTMANAGER_TCBD3AB46AC703AF5780510DD95E1627388F6D9A0_H
#ifndef WLAANGJWTACEPVAVNGDKISMGYMNI_T0BC62303F41F016E0E87796369BDAD18DCFFEC76_H
#define WLAANGJWTACEPVAVNGDKISMGYMNI_T0BC62303F41F016E0E87796369BDAD18DCFFEC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI
struct  WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76  : public RuntimeObject
{
public:
	// Rewired.InputMapper Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::waEhkaNJermXjlpTevBkHdlvdCD
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___waEhkaNJermXjlpTevBkHdlvdCD_0;
	// Rewired.InputMapper_Options Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::hQaGSpfGCOBUmxFmTPAEMXgnEuuD
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1;
	// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::PgbyaSUZMLVkFsALCONtPTqsomL
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C * ___PgbyaSUZMLVkFsALCONtPTqsomL_2;
	// System.Collections.Generic.Dictionary`2<Rewired.InputMapper_brixqxKPyakMwXgqKReyPWdHwsQ,Rewired.Utils.SafeDelegate> Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::SpcTsLWPUhOBPVkDNBqZAMhkbqLa
	Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3;
	// System.Collections.Generic.Dictionary`2<System.String,Rewired.Utils.SafeDelegate> Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::GFDmSkApjRNFYNqNgaFsKCNUJsST
	Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * ___GFDmSkApjRNFYNqNgaFsKCNUJsST_4;
	// Rewired.InputMapper_Status Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::okVQLiVpJrDffbfnTenwRguCTO
	int32_t ___okVQLiVpJrDffbfnTenwRguCTO_5;
	// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_HpatyHNcsbxyoSINVtOzDqCJcGH Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::ucCLNrZtIdCAupOtarnDQmWuqaY
	int32_t ___ucCLNrZtIdCAupOtarnDQmWuqaY_6;
	// System.Single Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::YKgbhstxodGBLEVVbsiCVCSOOEoT
	float ___YKgbhstxodGBLEVVbsiCVCSOOEoT_7;
	// System.Boolean Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::arhYXtPyWSnzTyYlloYwgcgCIim
	bool ___arhYXtPyWSnzTyYlloYwgcgCIim_8;
	// System.Collections.Generic.List`1<Rewired.Player> Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::uaFbZuRrXXOtzVtiKcPkivhEZUJ
	List_1_t173A087C7487E3961C3BFB37EEE5EB31090BF92B * ___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9;
	// System.Collections.Generic.List`1<Rewired.ControllerPollingInfo> Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::PvUcooLAGoMJLPclRykwKEFFGNc
	List_1_tEB943CFDA4A58C4532C0C4495773F0E6DB543276 * ___PvUcooLAGoMJLPclRykwKEFFGNc_10;
	// Rewired.ElementAssignment Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI::kHLgOPKOLcwzyhtNMzawWuqLDKiA
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___kHLgOPKOLcwzyhtNMzawWuqLDKiA_11;

public:
	inline static int32_t get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_0() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___waEhkaNJermXjlpTevBkHdlvdCD_0)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_waEhkaNJermXjlpTevBkHdlvdCD_0() const { return ___waEhkaNJermXjlpTevBkHdlvdCD_0; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_waEhkaNJermXjlpTevBkHdlvdCD_0() { return &___waEhkaNJermXjlpTevBkHdlvdCD_0; }
	inline void set_waEhkaNJermXjlpTevBkHdlvdCD_0(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___waEhkaNJermXjlpTevBkHdlvdCD_0 = value;
		Il2CppCodeGenWriteBarrier((&___waEhkaNJermXjlpTevBkHdlvdCD_0), value);
	}

	inline static int32_t get_offset_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1)); }
	inline Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * get_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1() const { return ___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1; }
	inline Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 ** get_address_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1() { return &___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1; }
	inline void set_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939 * value)
	{
		___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1 = value;
		Il2CppCodeGenWriteBarrier((&___hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1), value);
	}

	inline static int32_t get_offset_of_PgbyaSUZMLVkFsALCONtPTqsomL_2() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___PgbyaSUZMLVkFsALCONtPTqsomL_2)); }
	inline cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C * get_PgbyaSUZMLVkFsALCONtPTqsomL_2() const { return ___PgbyaSUZMLVkFsALCONtPTqsomL_2; }
	inline cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C ** get_address_of_PgbyaSUZMLVkFsALCONtPTqsomL_2() { return &___PgbyaSUZMLVkFsALCONtPTqsomL_2; }
	inline void set_PgbyaSUZMLVkFsALCONtPTqsomL_2(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C * value)
	{
		___PgbyaSUZMLVkFsALCONtPTqsomL_2 = value;
		Il2CppCodeGenWriteBarrier((&___PgbyaSUZMLVkFsALCONtPTqsomL_2), value);
	}

	inline static int32_t get_offset_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3)); }
	inline Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * get_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3() const { return ___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3; }
	inline Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 ** get_address_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3() { return &___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3; }
	inline void set_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3(Dictionary_2_t647D6E83D667F2F7B402453D4ABD843FD61D0568 * value)
	{
		___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3 = value;
		Il2CppCodeGenWriteBarrier((&___SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3), value);
	}

	inline static int32_t get_offset_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_4() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___GFDmSkApjRNFYNqNgaFsKCNUJsST_4)); }
	inline Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * get_GFDmSkApjRNFYNqNgaFsKCNUJsST_4() const { return ___GFDmSkApjRNFYNqNgaFsKCNUJsST_4; }
	inline Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 ** get_address_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_4() { return &___GFDmSkApjRNFYNqNgaFsKCNUJsST_4; }
	inline void set_GFDmSkApjRNFYNqNgaFsKCNUJsST_4(Dictionary_2_tB10A12FA84F26B48CEE8E634F34A514DC9C4B677 * value)
	{
		___GFDmSkApjRNFYNqNgaFsKCNUJsST_4 = value;
		Il2CppCodeGenWriteBarrier((&___GFDmSkApjRNFYNqNgaFsKCNUJsST_4), value);
	}

	inline static int32_t get_offset_of_okVQLiVpJrDffbfnTenwRguCTO_5() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___okVQLiVpJrDffbfnTenwRguCTO_5)); }
	inline int32_t get_okVQLiVpJrDffbfnTenwRguCTO_5() const { return ___okVQLiVpJrDffbfnTenwRguCTO_5; }
	inline int32_t* get_address_of_okVQLiVpJrDffbfnTenwRguCTO_5() { return &___okVQLiVpJrDffbfnTenwRguCTO_5; }
	inline void set_okVQLiVpJrDffbfnTenwRguCTO_5(int32_t value)
	{
		___okVQLiVpJrDffbfnTenwRguCTO_5 = value;
	}

	inline static int32_t get_offset_of_ucCLNrZtIdCAupOtarnDQmWuqaY_6() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___ucCLNrZtIdCAupOtarnDQmWuqaY_6)); }
	inline int32_t get_ucCLNrZtIdCAupOtarnDQmWuqaY_6() const { return ___ucCLNrZtIdCAupOtarnDQmWuqaY_6; }
	inline int32_t* get_address_of_ucCLNrZtIdCAupOtarnDQmWuqaY_6() { return &___ucCLNrZtIdCAupOtarnDQmWuqaY_6; }
	inline void set_ucCLNrZtIdCAupOtarnDQmWuqaY_6(int32_t value)
	{
		___ucCLNrZtIdCAupOtarnDQmWuqaY_6 = value;
	}

	inline static int32_t get_offset_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_7() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___YKgbhstxodGBLEVVbsiCVCSOOEoT_7)); }
	inline float get_YKgbhstxodGBLEVVbsiCVCSOOEoT_7() const { return ___YKgbhstxodGBLEVVbsiCVCSOOEoT_7; }
	inline float* get_address_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_7() { return &___YKgbhstxodGBLEVVbsiCVCSOOEoT_7; }
	inline void set_YKgbhstxodGBLEVVbsiCVCSOOEoT_7(float value)
	{
		___YKgbhstxodGBLEVVbsiCVCSOOEoT_7 = value;
	}

	inline static int32_t get_offset_of_arhYXtPyWSnzTyYlloYwgcgCIim_8() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___arhYXtPyWSnzTyYlloYwgcgCIim_8)); }
	inline bool get_arhYXtPyWSnzTyYlloYwgcgCIim_8() const { return ___arhYXtPyWSnzTyYlloYwgcgCIim_8; }
	inline bool* get_address_of_arhYXtPyWSnzTyYlloYwgcgCIim_8() { return &___arhYXtPyWSnzTyYlloYwgcgCIim_8; }
	inline void set_arhYXtPyWSnzTyYlloYwgcgCIim_8(bool value)
	{
		___arhYXtPyWSnzTyYlloYwgcgCIim_8 = value;
	}

	inline static int32_t get_offset_of_uaFbZuRrXXOtzVtiKcPkivhEZUJ_9() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9)); }
	inline List_1_t173A087C7487E3961C3BFB37EEE5EB31090BF92B * get_uaFbZuRrXXOtzVtiKcPkivhEZUJ_9() const { return ___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9; }
	inline List_1_t173A087C7487E3961C3BFB37EEE5EB31090BF92B ** get_address_of_uaFbZuRrXXOtzVtiKcPkivhEZUJ_9() { return &___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9; }
	inline void set_uaFbZuRrXXOtzVtiKcPkivhEZUJ_9(List_1_t173A087C7487E3961C3BFB37EEE5EB31090BF92B * value)
	{
		___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9 = value;
		Il2CppCodeGenWriteBarrier((&___uaFbZuRrXXOtzVtiKcPkivhEZUJ_9), value);
	}

	inline static int32_t get_offset_of_PvUcooLAGoMJLPclRykwKEFFGNc_10() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___PvUcooLAGoMJLPclRykwKEFFGNc_10)); }
	inline List_1_tEB943CFDA4A58C4532C0C4495773F0E6DB543276 * get_PvUcooLAGoMJLPclRykwKEFFGNc_10() const { return ___PvUcooLAGoMJLPclRykwKEFFGNc_10; }
	inline List_1_tEB943CFDA4A58C4532C0C4495773F0E6DB543276 ** get_address_of_PvUcooLAGoMJLPclRykwKEFFGNc_10() { return &___PvUcooLAGoMJLPclRykwKEFFGNc_10; }
	inline void set_PvUcooLAGoMJLPclRykwKEFFGNc_10(List_1_tEB943CFDA4A58C4532C0C4495773F0E6DB543276 * value)
	{
		___PvUcooLAGoMJLPclRykwKEFFGNc_10 = value;
		Il2CppCodeGenWriteBarrier((&___PvUcooLAGoMJLPclRykwKEFFGNc_10), value);
	}

	inline static int32_t get_offset_of_kHLgOPKOLcwzyhtNMzawWuqLDKiA_11() { return static_cast<int32_t>(offsetof(WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76, ___kHLgOPKOLcwzyhtNMzawWuqLDKiA_11)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_kHLgOPKOLcwzyhtNMzawWuqLDKiA_11() const { return ___kHLgOPKOLcwzyhtNMzawWuqLDKiA_11; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_kHLgOPKOLcwzyhtNMzawWuqLDKiA_11() { return &___kHLgOPKOLcwzyhtNMzawWuqLDKiA_11; }
	inline void set_kHLgOPKOLcwzyhtNMzawWuqLDKiA_11(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___kHLgOPKOLcwzyhtNMzawWuqLDKiA_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WLAANGJWTACEPVAVNGDKISMGYMNI_T0BC62303F41F016E0E87796369BDAD18DCFFEC76_H
#ifndef CGPULBRDZLIXOMOCLLIEVVTYCPU_TFD0CD2D60B023D814B4E581221B865B3D5673C1C_H
#define CGPULBRDZLIXOMOCLLIEVVTYCPU_TFD0CD2D60B023D814B4E581221B865B3D5673C1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu
struct  cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C  : public RuntimeObject
{
public:
	// Rewired.Player Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::VZYtnnAqBdPNaSltSWGWGTTbFPN
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___VZYtnnAqBdPNaSltSWGWGTTbFPN_0;
	// System.Int32 Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_1;
	// Rewired.InputMapper_Context Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::GgDLdlfkTgFJHhmkQlYKKtxusyw
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * ___GgDLdlfkTgFJHhmkQlYKKtxusyw_2;
	// Rewired.ControllerType Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// Rewired.ControllerPollingInfo Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::ITFRagOFmBEurFeJevBABAnzaiY
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___ITFRagOFmBEurFeJevBABAnzaiY_5;
	// Rewired.ModifierKeyFlags Rewired.InputMapper_WLaANgjWTacepVAvNGdKisMgYMnI_cgpUlBrDzLixoMOCLlievvtyCpu::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_6;

public:
	inline static int32_t get_offset_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_0() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___VZYtnnAqBdPNaSltSWGWGTTbFPN_0)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_VZYtnnAqBdPNaSltSWGWGTTbFPN_0() const { return ___VZYtnnAqBdPNaSltSWGWGTTbFPN_0; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_0() { return &___VZYtnnAqBdPNaSltSWGWGTTbFPN_0; }
	inline void set_VZYtnnAqBdPNaSltSWGWGTTbFPN_0(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___VZYtnnAqBdPNaSltSWGWGTTbFPN_0 = value;
		Il2CppCodeGenWriteBarrier((&___VZYtnnAqBdPNaSltSWGWGTTbFPN_0), value);
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_1() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___LmADKPFcmVAiabETiHqQTGSgvmcg_1)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_1() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_1; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_1() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_1; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_1(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_1 = value;
	}

	inline static int32_t get_offset_of_GgDLdlfkTgFJHhmkQlYKKtxusyw_2() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___GgDLdlfkTgFJHhmkQlYKKtxusyw_2)); }
	inline Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * get_GgDLdlfkTgFJHhmkQlYKKtxusyw_2() const { return ___GgDLdlfkTgFJHhmkQlYKKtxusyw_2; }
	inline Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 ** get_address_of_GgDLdlfkTgFJHhmkQlYKKtxusyw_2() { return &___GgDLdlfkTgFJHhmkQlYKKtxusyw_2; }
	inline void set_GgDLdlfkTgFJHhmkQlYKKtxusyw_2(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * value)
	{
		___GgDLdlfkTgFJHhmkQlYKKtxusyw_2 = value;
		Il2CppCodeGenWriteBarrier((&___GgDLdlfkTgFJHhmkQlYKKtxusyw_2), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_ITFRagOFmBEurFeJevBABAnzaiY_5() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___ITFRagOFmBEurFeJevBABAnzaiY_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_ITFRagOFmBEurFeJevBABAnzaiY_5() const { return ___ITFRagOFmBEurFeJevBABAnzaiY_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_ITFRagOFmBEurFeJevBABAnzaiY_5() { return &___ITFRagOFmBEurFeJevBABAnzaiY_5; }
	inline void set_ITFRagOFmBEurFeJevBABAnzaiY_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___ITFRagOFmBEurFeJevBABAnzaiY_5 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6() { return static_cast<int32_t>(offsetof(cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C, ___rUpHewPnhPrBuIDeIasWkyvRDkW_6)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_6() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_6; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_6; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_6(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGPULBRDZLIXOMOCLLIEVVTYCPU_TFD0CD2D60B023D814B4E581221B865B3D5673C1C_H
#ifndef KEYBOARDMAP_T03C5D3775649F3FEBBE43728D8FB16C32E61EBBA_H
#define KEYBOARDMAP_T03C5D3775649F3FEBBE43728D8FB16C32E61EBBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.KeyboardMap
struct  KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA  : public ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDMAP_T03C5D3775649F3FEBBE43728D8FB16C32E61EBBA_H
#ifndef HQVYXGSPCDJKNLHTKVCNGQHHNKC_TDF363EFAD350ABD854DC2C026463C5726E6C4918_H
#define HQVYXGSPCDJKNLHTKVCNGQHHNKC_TDF363EFAD350ABD854DC2C026463C5726E6C4918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC
struct  HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.JoystickMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::YtyCpfuMjInPTabavmllkqElczPA
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___YtyCpfuMjInPTabavmllkqElczPA_6;
	// Rewired.JoystickMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::RjbgzWvPMFXOEdOLgPwRLLJVLIr
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_13;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::BXRUazznMnLjBmUEMAlPSqVFAsz
	int32_t ___BXRUazznMnLjBmUEMAlPSqVFAsz_14;
	// Rewired.Joystick Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::afZIrUpxaCTlKGOVgcffewJIetd
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___afZIrUpxaCTlKGOVgcffewJIetd_15;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::eIkOLImiGKegUDLZCJYqakcjxCxa
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___eIkOLImiGKegUDLZCJYqakcjxCxa_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_HQvYxgsPCdJKnLhtkVcNGQHHNkC::QZQLDIvnrZfxWplZEbMmdPKplWd
	RuntimeObject* ___QZQLDIvnrZfxWplZEbMmdPKplWd_17;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_6() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___YtyCpfuMjInPTabavmllkqElczPA_6)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_YtyCpfuMjInPTabavmllkqElczPA_6() const { return ___YtyCpfuMjInPTabavmllkqElczPA_6; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_YtyCpfuMjInPTabavmllkqElczPA_6() { return &___YtyCpfuMjInPTabavmllkqElczPA_6; }
	inline void set_YtyCpfuMjInPTabavmllkqElczPA_6(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___YtyCpfuMjInPTabavmllkqElczPA_6 = value;
		Il2CppCodeGenWriteBarrier((&___YtyCpfuMjInPTabavmllkqElczPA_6), value);
	}

	inline static int32_t get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() const { return ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() { return &___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7; }
	inline void set_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7 = value;
		Il2CppCodeGenWriteBarrier((&___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_9; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_9(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_9 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_9), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_13() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___CVShsOfomYgQbeMNmgtRkoanKAb_13)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_13() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_13; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_13() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_13; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_13(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_13 = value;
	}

	inline static int32_t get_offset_of_BXRUazznMnLjBmUEMAlPSqVFAsz_14() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___BXRUazznMnLjBmUEMAlPSqVFAsz_14)); }
	inline int32_t get_BXRUazznMnLjBmUEMAlPSqVFAsz_14() const { return ___BXRUazznMnLjBmUEMAlPSqVFAsz_14; }
	inline int32_t* get_address_of_BXRUazznMnLjBmUEMAlPSqVFAsz_14() { return &___BXRUazznMnLjBmUEMAlPSqVFAsz_14; }
	inline void set_BXRUazznMnLjBmUEMAlPSqVFAsz_14(int32_t value)
	{
		___BXRUazznMnLjBmUEMAlPSqVFAsz_14 = value;
	}

	inline static int32_t get_offset_of_afZIrUpxaCTlKGOVgcffewJIetd_15() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___afZIrUpxaCTlKGOVgcffewJIetd_15)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_afZIrUpxaCTlKGOVgcffewJIetd_15() const { return ___afZIrUpxaCTlKGOVgcffewJIetd_15; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_afZIrUpxaCTlKGOVgcffewJIetd_15() { return &___afZIrUpxaCTlKGOVgcffewJIetd_15; }
	inline void set_afZIrUpxaCTlKGOVgcffewJIetd_15(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___afZIrUpxaCTlKGOVgcffewJIetd_15 = value;
		Il2CppCodeGenWriteBarrier((&___afZIrUpxaCTlKGOVgcffewJIetd_15), value);
	}

	inline static int32_t get_offset_of_eIkOLImiGKegUDLZCJYqakcjxCxa_16() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___eIkOLImiGKegUDLZCJYqakcjxCxa_16)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_eIkOLImiGKegUDLZCJYqakcjxCxa_16() const { return ___eIkOLImiGKegUDLZCJYqakcjxCxa_16; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_eIkOLImiGKegUDLZCJYqakcjxCxa_16() { return &___eIkOLImiGKegUDLZCJYqakcjxCxa_16; }
	inline void set_eIkOLImiGKegUDLZCJYqakcjxCxa_16(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___eIkOLImiGKegUDLZCJYqakcjxCxa_16 = value;
	}

	inline static int32_t get_offset_of_QZQLDIvnrZfxWplZEbMmdPKplWd_17() { return static_cast<int32_t>(offsetof(HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918, ___QZQLDIvnrZfxWplZEbMmdPKplWd_17)); }
	inline RuntimeObject* get_QZQLDIvnrZfxWplZEbMmdPKplWd_17() const { return ___QZQLDIvnrZfxWplZEbMmdPKplWd_17; }
	inline RuntimeObject** get_address_of_QZQLDIvnrZfxWplZEbMmdPKplWd_17() { return &___QZQLDIvnrZfxWplZEbMmdPKplWd_17; }
	inline void set_QZQLDIvnrZfxWplZEbMmdPKplWd_17(RuntimeObject* value)
	{
		___QZQLDIvnrZfxWplZEbMmdPKplWd_17 = value;
		Il2CppCodeGenWriteBarrier((&___QZQLDIvnrZfxWplZEbMmdPKplWd_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HQVYXGSPCDJKNLHTKVCNGQHHNKC_TDF363EFAD350ABD854DC2C026463C5726E6C4918_H
#ifndef OBVQSUVTMYJLCAUAKMRRSONZMBD_TCA6C565C3749C45A489000702F517348E8E2FAFB_H
#define OBVQSUVTMYJLCAUAKMRRSONZMBD_TCA6C565C3749C45A489000702F517348E8E2FAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd
struct  ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomControllerMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::WigXBlhRQEAAKnPZjEeeNdMahyT
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___WigXBlhRQEAAKnPZjEeeNdMahyT_6;
	// Rewired.CustomControllerMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::hqJogBNPbbjuJUDUhZmRhiFNzfx
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_11;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::jEOhhUBDUEDIAqVbvVAWtqVvlhq
	int32_t ___jEOhhUBDUEDIAqVbvVAWtqVvlhq_12;
	// Rewired.CustomController Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::MLVBiKOEXDsCXyZqbbGvLecnvJs
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___MLVBiKOEXDsCXyZqbbGvLecnvJs_13;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::MpmkTLgqzsnDcNivBGXxmvdPdUC
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___MpmkTLgqzsnDcNivBGXxmvdPdUC_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_ObvQSUvTMyjLcAUakMRRSoNZMBd::WUQWpaJgJGBZuAggCsqpFzBHkvp
	RuntimeObject* ___WUQWpaJgJGBZuAggCsqpFzBHkvp_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___WigXBlhRQEAAKnPZjEeeNdMahyT_6)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_WigXBlhRQEAAKnPZjEeeNdMahyT_6() const { return ___WigXBlhRQEAAKnPZjEeeNdMahyT_6; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6() { return &___WigXBlhRQEAAKnPZjEeeNdMahyT_6; }
	inline void set_WigXBlhRQEAAKnPZjEeeNdMahyT_6(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___WigXBlhRQEAAKnPZjEeeNdMahyT_6 = value;
		Il2CppCodeGenWriteBarrier((&___WigXBlhRQEAAKnPZjEeeNdMahyT_6), value);
	}

	inline static int32_t get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() const { return ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() { return &___hqJogBNPbbjuJUDUhZmRhiFNzfx_7; }
	inline void set_hqJogBNPbbjuJUDUhZmRhiFNzfx_7(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___hqJogBNPbbjuJUDUhZmRhiFNzfx_7 = value;
		Il2CppCodeGenWriteBarrier((&___hqJogBNPbbjuJUDUhZmRhiFNzfx_7), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_11() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___CVShsOfomYgQbeMNmgtRkoanKAb_11)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_11() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_11; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_11() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_11; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_11(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_11 = value;
	}

	inline static int32_t get_offset_of_jEOhhUBDUEDIAqVbvVAWtqVvlhq_12() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___jEOhhUBDUEDIAqVbvVAWtqVvlhq_12)); }
	inline int32_t get_jEOhhUBDUEDIAqVbvVAWtqVvlhq_12() const { return ___jEOhhUBDUEDIAqVbvVAWtqVvlhq_12; }
	inline int32_t* get_address_of_jEOhhUBDUEDIAqVbvVAWtqVvlhq_12() { return &___jEOhhUBDUEDIAqVbvVAWtqVvlhq_12; }
	inline void set_jEOhhUBDUEDIAqVbvVAWtqVvlhq_12(int32_t value)
	{
		___jEOhhUBDUEDIAqVbvVAWtqVvlhq_12 = value;
	}

	inline static int32_t get_offset_of_MLVBiKOEXDsCXyZqbbGvLecnvJs_13() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___MLVBiKOEXDsCXyZqbbGvLecnvJs_13)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_MLVBiKOEXDsCXyZqbbGvLecnvJs_13() const { return ___MLVBiKOEXDsCXyZqbbGvLecnvJs_13; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_MLVBiKOEXDsCXyZqbbGvLecnvJs_13() { return &___MLVBiKOEXDsCXyZqbbGvLecnvJs_13; }
	inline void set_MLVBiKOEXDsCXyZqbbGvLecnvJs_13(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___MLVBiKOEXDsCXyZqbbGvLecnvJs_13 = value;
		Il2CppCodeGenWriteBarrier((&___MLVBiKOEXDsCXyZqbbGvLecnvJs_13), value);
	}

	inline static int32_t get_offset_of_MpmkTLgqzsnDcNivBGXxmvdPdUC_14() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___MpmkTLgqzsnDcNivBGXxmvdPdUC_14)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_MpmkTLgqzsnDcNivBGXxmvdPdUC_14() const { return ___MpmkTLgqzsnDcNivBGXxmvdPdUC_14; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_MpmkTLgqzsnDcNivBGXxmvdPdUC_14() { return &___MpmkTLgqzsnDcNivBGXxmvdPdUC_14; }
	inline void set_MpmkTLgqzsnDcNivBGXxmvdPdUC_14(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___MpmkTLgqzsnDcNivBGXxmvdPdUC_14 = value;
	}

	inline static int32_t get_offset_of_WUQWpaJgJGBZuAggCsqpFzBHkvp_15() { return static_cast<int32_t>(offsetof(ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB, ___WUQWpaJgJGBZuAggCsqpFzBHkvp_15)); }
	inline RuntimeObject* get_WUQWpaJgJGBZuAggCsqpFzBHkvp_15() const { return ___WUQWpaJgJGBZuAggCsqpFzBHkvp_15; }
	inline RuntimeObject** get_address_of_WUQWpaJgJGBZuAggCsqpFzBHkvp_15() { return &___WUQWpaJgJGBZuAggCsqpFzBHkvp_15; }
	inline void set_WUQWpaJgJGBZuAggCsqpFzBHkvp_15(RuntimeObject* value)
	{
		___WUQWpaJgJGBZuAggCsqpFzBHkvp_15 = value;
		Il2CppCodeGenWriteBarrier((&___WUQWpaJgJGBZuAggCsqpFzBHkvp_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBVQSUVTMYJLCAUAKMRRSONZMBD_TCA6C565C3749C45A489000702F517348E8E2FAFB_H
#ifndef ZSPJMBEXJJKJEWKVGTWJJMIHLCL_T4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B_H
#define ZSPJMBEXJJKJEWKVGTWJJMIHLCL_T4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL
struct  zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.JoystickMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::YtyCpfuMjInPTabavmllkqElczPA
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___YtyCpfuMjInPTabavmllkqElczPA_6;
	// Rewired.JoystickMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::RjbgzWvPMFXOEdOLgPwRLLJVLIr
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_11;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::EAmCNKrLCDCLzvUcjfqLawIzxXnC
	int32_t ___EAmCNKrLCDCLzvUcjfqLawIzxXnC_12;
	// Rewired.Joystick Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::yBoPvusNueqLLNpcBTmQkrFNwKK
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___yBoPvusNueqLLNpcBTmQkrFNwKK_13;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::IjXjewKXqAsfLkHXGeKSVCASvmk
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___IjXjewKXqAsfLkHXGeKSVCASvmk_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_zSPJMbexjjkjeWKVGtwjjMihLCL::HQPsrbVufqLRYQEIuPjShlVZFeA
	RuntimeObject* ___HQPsrbVufqLRYQEIuPjShlVZFeA_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_6() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___YtyCpfuMjInPTabavmllkqElczPA_6)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_YtyCpfuMjInPTabavmllkqElczPA_6() const { return ___YtyCpfuMjInPTabavmllkqElczPA_6; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_YtyCpfuMjInPTabavmllkqElczPA_6() { return &___YtyCpfuMjInPTabavmllkqElczPA_6; }
	inline void set_YtyCpfuMjInPTabavmllkqElczPA_6(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___YtyCpfuMjInPTabavmllkqElczPA_6 = value;
		Il2CppCodeGenWriteBarrier((&___YtyCpfuMjInPTabavmllkqElczPA_6), value);
	}

	inline static int32_t get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() const { return ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7() { return &___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7; }
	inline void set_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7 = value;
		Il2CppCodeGenWriteBarrier((&___RjbgzWvPMFXOEdOLgPwRLLJVLIr_7), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_11() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___CVShsOfomYgQbeMNmgtRkoanKAb_11)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_11() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_11; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_11() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_11; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_11(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_11 = value;
	}

	inline static int32_t get_offset_of_EAmCNKrLCDCLzvUcjfqLawIzxXnC_12() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___EAmCNKrLCDCLzvUcjfqLawIzxXnC_12)); }
	inline int32_t get_EAmCNKrLCDCLzvUcjfqLawIzxXnC_12() const { return ___EAmCNKrLCDCLzvUcjfqLawIzxXnC_12; }
	inline int32_t* get_address_of_EAmCNKrLCDCLzvUcjfqLawIzxXnC_12() { return &___EAmCNKrLCDCLzvUcjfqLawIzxXnC_12; }
	inline void set_EAmCNKrLCDCLzvUcjfqLawIzxXnC_12(int32_t value)
	{
		___EAmCNKrLCDCLzvUcjfqLawIzxXnC_12 = value;
	}

	inline static int32_t get_offset_of_yBoPvusNueqLLNpcBTmQkrFNwKK_13() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___yBoPvusNueqLLNpcBTmQkrFNwKK_13)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_yBoPvusNueqLLNpcBTmQkrFNwKK_13() const { return ___yBoPvusNueqLLNpcBTmQkrFNwKK_13; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_yBoPvusNueqLLNpcBTmQkrFNwKK_13() { return &___yBoPvusNueqLLNpcBTmQkrFNwKK_13; }
	inline void set_yBoPvusNueqLLNpcBTmQkrFNwKK_13(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___yBoPvusNueqLLNpcBTmQkrFNwKK_13 = value;
		Il2CppCodeGenWriteBarrier((&___yBoPvusNueqLLNpcBTmQkrFNwKK_13), value);
	}

	inline static int32_t get_offset_of_IjXjewKXqAsfLkHXGeKSVCASvmk_14() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___IjXjewKXqAsfLkHXGeKSVCASvmk_14)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_IjXjewKXqAsfLkHXGeKSVCASvmk_14() const { return ___IjXjewKXqAsfLkHXGeKSVCASvmk_14; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_IjXjewKXqAsfLkHXGeKSVCASvmk_14() { return &___IjXjewKXqAsfLkHXGeKSVCASvmk_14; }
	inline void set_IjXjewKXqAsfLkHXGeKSVCASvmk_14(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___IjXjewKXqAsfLkHXGeKSVCASvmk_14 = value;
	}

	inline static int32_t get_offset_of_HQPsrbVufqLRYQEIuPjShlVZFeA_15() { return static_cast<int32_t>(offsetof(zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B, ___HQPsrbVufqLRYQEIuPjShlVZFeA_15)); }
	inline RuntimeObject* get_HQPsrbVufqLRYQEIuPjShlVZFeA_15() const { return ___HQPsrbVufqLRYQEIuPjShlVZFeA_15; }
	inline RuntimeObject** get_address_of_HQPsrbVufqLRYQEIuPjShlVZFeA_15() { return &___HQPsrbVufqLRYQEIuPjShlVZFeA_15; }
	inline void set_HQPsrbVufqLRYQEIuPjShlVZFeA_15(RuntimeObject* value)
	{
		___HQPsrbVufqLRYQEIuPjShlVZFeA_15 = value;
		Il2CppCodeGenWriteBarrier((&___HQPsrbVufqLRYQEIuPjShlVZFeA_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSPJMBEXJJKJEWKVGTWJJMIHLCL_T4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B_H
#ifndef ZZKPZXMMXFKIISDCEVBSMFGUXDL_TEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E_H
#define ZZKPZXMMXFKIISDCEVBSMFGUXDL_TEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl
struct  zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_4;
	// Rewired.ElementAssignmentConflictCheck Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_9;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::mhPcKmSeaabUOVNWPuCtikHcKmD
	int32_t ___mhPcKmSeaabUOVNWPuCtikHcKmD_10;
	// Rewired.Joystick Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::NhYMrXcHzacPXXKJMJTTVZKBfDA
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___NhYMrXcHzacPXXKJMJTTVZKBfDA_11;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::IGigngCNqCmUsaNSVGwbmQviIaE
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___IGigngCNqCmUsaNSVGwbmQviIaE_12;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_zZkPzXmmxFKIisdCevBSmFgUxdl::uxZfSXhGhmKSDzFgWwgHNAtYxXya
	RuntimeObject* ___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___RXwTkvaioBOXLvHfDHkciCcOgzY_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_4() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_4 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_9() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___CVShsOfomYgQbeMNmgtRkoanKAb_9)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_9() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_9; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_9() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_9; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_9(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_9 = value;
	}

	inline static int32_t get_offset_of_mhPcKmSeaabUOVNWPuCtikHcKmD_10() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___mhPcKmSeaabUOVNWPuCtikHcKmD_10)); }
	inline int32_t get_mhPcKmSeaabUOVNWPuCtikHcKmD_10() const { return ___mhPcKmSeaabUOVNWPuCtikHcKmD_10; }
	inline int32_t* get_address_of_mhPcKmSeaabUOVNWPuCtikHcKmD_10() { return &___mhPcKmSeaabUOVNWPuCtikHcKmD_10; }
	inline void set_mhPcKmSeaabUOVNWPuCtikHcKmD_10(int32_t value)
	{
		___mhPcKmSeaabUOVNWPuCtikHcKmD_10 = value;
	}

	inline static int32_t get_offset_of_NhYMrXcHzacPXXKJMJTTVZKBfDA_11() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___NhYMrXcHzacPXXKJMJTTVZKBfDA_11)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_NhYMrXcHzacPXXKJMJTTVZKBfDA_11() const { return ___NhYMrXcHzacPXXKJMJTTVZKBfDA_11; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_NhYMrXcHzacPXXKJMJTTVZKBfDA_11() { return &___NhYMrXcHzacPXXKJMJTTVZKBfDA_11; }
	inline void set_NhYMrXcHzacPXXKJMJTTVZKBfDA_11(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___NhYMrXcHzacPXXKJMJTTVZKBfDA_11 = value;
		Il2CppCodeGenWriteBarrier((&___NhYMrXcHzacPXXKJMJTTVZKBfDA_11), value);
	}

	inline static int32_t get_offset_of_IGigngCNqCmUsaNSVGwbmQviIaE_12() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___IGigngCNqCmUsaNSVGwbmQviIaE_12)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_IGigngCNqCmUsaNSVGwbmQviIaE_12() const { return ___IGigngCNqCmUsaNSVGwbmQviIaE_12; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_IGigngCNqCmUsaNSVGwbmQviIaE_12() { return &___IGigngCNqCmUsaNSVGwbmQviIaE_12; }
	inline void set_IGigngCNqCmUsaNSVGwbmQviIaE_12(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___IGigngCNqCmUsaNSVGwbmQviIaE_12 = value;
	}

	inline static int32_t get_offset_of_uxZfSXhGhmKSDzFgWwgHNAtYxXya_13() { return static_cast<int32_t>(offsetof(zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E, ___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13)); }
	inline RuntimeObject* get_uxZfSXhGhmKSDzFgWwgHNAtYxXya_13() const { return ___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13; }
	inline RuntimeObject** get_address_of_uxZfSXhGhmKSDzFgWwgHNAtYxXya_13() { return &___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13; }
	inline void set_uxZfSXhGhmKSDzFgWwgHNAtYxXya_13(RuntimeObject* value)
	{
		___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13 = value;
		Il2CppCodeGenWriteBarrier((&___uxZfSXhGhmKSDzFgWwgHNAtYxXya_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZZKPZXMMXFKIISDCEVBSMFGUXDL_TEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E_H
#ifndef CUSTOMCONTROLLERMAP_T014AB1F2888502EFF0FF5351FADBA1A05716165A_H
#define CUSTOMCONTROLLERMAP_T014AB1F2888502EFF0FF5351FADBA1A05716165A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CustomControllerMap
struct  CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A  : public ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36
{
public:
	// System.Int32 Rewired.CustomControllerMap::_sourceControllerId
	int32_t ____sourceControllerId_18;

public:
	inline static int32_t get_offset_of__sourceControllerId_18() { return static_cast<int32_t>(offsetof(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A, ____sourceControllerId_18)); }
	inline int32_t get__sourceControllerId_18() const { return ____sourceControllerId_18; }
	inline int32_t* get_address_of__sourceControllerId_18() { return &____sourceControllerId_18; }
	inline void set__sourceControllerId_18(int32_t value)
	{
		____sourceControllerId_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERMAP_T014AB1F2888502EFF0FF5351FADBA1A05716165A_H
#ifndef JOYSTICKMAP_TACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24_H
#define JOYSTICKMAP_TACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.JoystickMap
struct  JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24  : public ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMAP_TACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24_H
#ifndef MOUSEMAP_T58C438E7108115F44D16041A902ECCBD89BD2008_H
#define MOUSEMAP_T58C438E7108115F44D16041A902ECCBD89BD2008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.MouseMap
struct  MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008  : public ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEMAP_T58C438E7108115F44D16041A902ECCBD89BD2008_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4600[4] = 
{
	ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE::get_offset_of__id_0(),
	ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE::get_offset_of__name_1(),
	ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE::get_offset_of__tag_2(),
	ControllerMapEnabler_RuleSet_Editor_tE1CAFA7B57B750A7A256A5A9C374D77F8B12A7DE::get_offset_of__rules_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4601[5] = 
{
	ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11::get_offset_of__tag_0(),
	ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11::get_offset_of__enable_1(),
	ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11::get_offset_of__categoryIds_2(),
	ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11::get_offset_of__layoutIds_3(),
	ControllerMapEnabler_Rule_Editor_t6087E64B76D8155566E440E3290A382409B44D11::get_offset_of__controllerSetSelector_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4602[7] = 
{
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__enabled_0(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__loadFromUserDataStore_1(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__player_2(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__startingSettings_3(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__reInputId_4(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__ruleSets_5(),
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781::get_offset_of__ApplyCalledEvent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4603[3] = 
{
	StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1::get_offset_of_enabled_0(),
	StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1::get_offset_of_loadFromUserDataStore_1(),
	StartingSettings_t3C9D18DE9D7547393AE30C6222D418639EBB5AA1::get_offset_of_startingRuleSets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4604[6] = 
{
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__tag_0(),
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__categoryIds_1(),
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__layoutId_2(),
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__controllerSetSelector_3(),
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__preInitCategoryNames_4(),
	Rule_t3F78ADFDB2129A72BD212B9CF7DD00A531F22FEE::get_offset_of__preInitLayoutName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[4] = 
{
	0,
	RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3::get_offset_of__enabled_1(),
	RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3::get_offset_of__tag_2(),
	RuleSet_tBEED9ED1FBBE3C26F8C85DE95BFC956D6C1759E3::get_offset_of__rules_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4606[4] = 
{
	ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960::get_offset_of__id_0(),
	ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960::get_offset_of__name_1(),
	ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960::get_offset_of__tag_2(),
	ControllerMapLayoutManager_RuleSet_Editor_t37EFE246C4AA1E23652154683A584721F92EA960::get_offset_of__rules_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4607[4] = 
{
	ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07::get_offset_of__tag_0(),
	ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07::get_offset_of__categoryIds_1(),
	ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07::get_offset_of__layoutId_2(),
	ControllerMapLayoutManager_Rule_Editor_t9560738F08FDF3D8FF6794B6132455764DCA2E07::get_offset_of__controllerSetSelector_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4608[8] = 
{
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__type_0(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__controllerType_1(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__hardwareTypeGuidString_2(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__hardwareIdentifier_3(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__controllerTemplateTypeGuidString_4(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__deviceInstanceGuidString_5(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__customControllerSourceId_6(),
	ControllerSetSelector_Editor_tE5E14F30E6A037B9BFB3F265CDB025E449E5DBC8::get_offset_of__controllerId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4609[3] = 
{
	ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F::get_offset_of__controller_0(),
	ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F::get_offset_of__map_1(),
	ControllerMapSaveData_t9EF811A272881EFD74E188CA3C628B6B227DDF7F::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (KeyboardMapSaveData_t337FD1281183EE41385ED18BE003AAC70261B97C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (MouseMapSaveData_t2A02E2F271B2011AA7C4CB8C9ECCF52468B09CD3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (JoystickMapSaveData_t9968968EDD66FFE5E773ADDCD8649078DCF6ED4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (CustomControllerMapSaveData_tEEF313BB062EB2DDA52D216138FF9ED7878DEAB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB), -1, sizeof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4614[16] = 
{
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__id_0(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__sourceMapId_1(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__categoryId_2(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__layoutId_3(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__name_4(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__hardwareGuid_5(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__enabled_6(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of_VWGXUewCkbjAzopbSknmvZALlIZ_8(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of_TruAdbhnbvpEVsLpODTaGQmazwL_9(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of_arINfjuBJNQHwNqdOQyXSRjsHrB_10(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__playerId_12(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__controllerId_13(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB::get_offset_of__controllerType_14(),
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields::get_offset_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46), -1, sizeof(kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4615[1] = 
{
	kWecicOtRMQfpYQfTypgJeaOPZC_t3D4FE17C64FE950DBC80BE0CC98AF37575D22E46_StaticFields::get_offset_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4616[10] = 
{
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8(),
	GvgOZuJcbXvKfyWQihPgbEZuIAX_tB87ACCFF8DB0400170F69E1F70B89DC97D053DD5::get_offset_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4617[13] = 
{
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_JJBazOAoLDIHfJdiHCVNlEXQFdst_8(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_koIGbWtOszkBfeKznqEsjxuJIpw_9(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_jUlKXGncYHGIVAuxoeAJucdDuGjq_10(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_rXLKfDaDLNcGSyuYNDUiezDBrce_11(),
	AXADwpfEENgNqLrkNSrcweWuEqg_tF7543DB5C6FF42440814E3E193219EA786038F8B::get_offset_of_BPiRbabceIVCHhjEQJeLmAhEVFW_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4618[15] = 
{
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_UzrYOiFDqERLNuDgbvIJpXnzUyp_10(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_ryBcAjrEEpazQiPuJlMNICDBkCMM_11(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_zWZVzRewMrLfsDjRNOncamEtigT_12(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_ZEiZXagZaJDLOvzqmSYCjDHuPPr_13(),
	KvprztGTJVHMYmpprfceQJHIVou_t6A5549123B4FFA86C0387E371749414BCF63C03B::get_offset_of_EAYguexQSuaDQQiXPdpMdLgDqhRg_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4619[12] = 
{
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_ByOqNpDZwJrvfOqQiqTmaleCTNf_8(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_NgTrnlpDWwovXpOfghixDogoFqyC_9(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_bbylexxHkiPVNjmmyFKFxUlOenA_10(),
	CLhHmwWkZzdaTIopfXvSpZWkuEx_tF4C538EB7FB0E166A94CB2CE3CEBD7F8A212EF58::get_offset_of_pYinzYhLeyxGzWrExwLAEtbjDxC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4620[14] = 
{
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_4(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_xZzdvWORdyUNltHXdFeBwXYpAaQ_8(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_UVCamMYFnOKzRiwRmrmqCuTMBps_9(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_mCtlfnDGBODwtcZpRdLladWaxwoL_10(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_DkUEgfheIRLBlrdBwlapxNFbOwmc_11(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_LUIqqILPDzJbivmrLbLDVSNNEZd_12(),
	ksuBNeJQTLncqGxeZmfpAGbJgKHt_t49736CC9017020FD46A1E0732E3AB71D174C2D4C::get_offset_of_HMecUabwlSbKzobmDQZgFrHAzGue_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4621[10] = 
{
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_4(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_QcpYrCzXoHBprkTYGgNUNLhrDlb_8(),
	ZMgmBSZPKbITBkRyvbqSbvBxbpwi_t01F6B7A3C44298D254BEFB4A1BC1931D16C705B1::get_offset_of_bXnYNPiASfROUbaNwkMEADTQxsA_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4622[11] = 
{
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_ACjiuYYNvcCGLkbnCIoZboxfCFWr_8(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_pWuGlrwnTxWREPQHcCZSYjOxeYl_9(),
	EVaMJFheKQmZggexfvxquCZEhSZ_t84FC3AFA5DB70DDBD23260B6B00153434EADC5C8::get_offset_of_bmcyskvpVsWgsnnVaivmepTdeQk_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4623[2] = 
{
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36::get_offset_of_DzNPmgjSCVrhFrAmXealRsomAik_16(),
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36::get_offset_of_uqEGHdRIdILZuXvprphGZfOEfmV_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[10] = 
{
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_LHdGnHCkoRIrqjLwZdOwLxKUpPZ_8(),
	SsfnwMAvxPSzNKtvDYWwLHIKzNM_t56C84F31517B2CF05B352C4407A51E879C372EFC::get_offset_of_xyrBNgPkbFouqDLYWRndyzUOyMa_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4625[17] = 
{
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_4(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_vEmKKUUwDIqfnqRwGeghcRhPDEr_5(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_tCVxHXQhWEUGiHHMEurjbDspwep_8(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_bMTQDqEhsggiAgeyaWKyEtpPurd_9(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_TuXqbkjfGMnpkuzGuDrLkwcmnBj_10(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_HgefhEqryspDVGJbRyJdhsDrbHC_11(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_NpcGkrehEogoInReCXlZWKvacYF_12(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_dmBFyXcxqbwWGjWNLrXhYoNNqWY_13(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_wlrCnifeuDbCjwSXkGAHmhMcgLj_14(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_ammWRKrBRUxmlflzLFHJSbijAmC_15(),
	VFcLlGmTAkhGPUAkfoDBmPIWgUJ_t34311D9137275F99C568E14BD0633390464E9D88::get_offset_of_NoTAyTxmULscBYhxegWobtJUNJhc_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[12] = 
{
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_4(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_xRSEXSmmPMfcXipPcqHmVjqYVhoG_5(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_OgQTGunYZqUrDsgogEUoMpZVZJL_8(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_BXRUazznMnLjBmUEMAlPSqVFAsz_9(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_AYaINkvIvaTTiENOGyVYCIRtebJ_10(),
	DIBeNVGslRVnhCinpEEiWUVNYequ_t8BE1D4935582F13CC01BEB16D5B5BC928F6BD0CC::get_offset_of_chDEPKpKtufxNzXjtHogGLsHofQU_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4627[13] = 
{
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_ohYdFTWPrnbUbCJgtNMBewHaRsb_8(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_IhtfGVHdmBpoQcPyjpIpgwJziSzq_9(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_vYyiYpDzEJMjYXRXZFufmzyWdNU_10(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_LxqXeBOlKBfqTJPpnWzIVfbORWB_11(),
	aYgnSNODbroPOvjlhYyiOsMobOB_t9685055A93FB30E16C426D0AD041185215CDE15F::get_offset_of_EnyxBHqQzSOfLzNTXVsnPwUnDTg_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4631[1] = 
{
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A::get_offset_of__sourceControllerId_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4633[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4634[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4635[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4636[7] = 
{
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_id_0(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_categoryId_1(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_layoutId_2(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_name_3(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_hardwareGuidString_4(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_customControllerUid_5(),
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7::get_offset_of_actionElementMaps_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4637[5] = 
{
	KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	KdfjzLbYjgmoBPkdspzacpEVkPT_t62D350CF7F3448A234E092F807CBBDAF58693A3D::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4638[30] = 
{
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_controllerName_0(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hardwareMapIdentifier_1(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_customControllerSourceId_2(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_elementIdentifiers_3(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_elementIdentifiers_cache_4(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_buttonElementIdentifiers_cache_5(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axisElementIdentifiers_cache_6(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axis2DElementIdentifiers_cache_7(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hatElementIdentifiers_cache_8(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_elementIdentifiers_readOnly_9(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_buttonElementIdentifiers_readOnly_10(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axisElementIdentifiers_readOnly_11(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axis2DElementIdentifiers_readOnly_12(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hatElementIdentifiers_readOnly_13(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_buttonElementIdentifierIds_14(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axisElementIdentifierIds_15(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axis2DElementIdentifierIds_16(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hatElementIdentifierIds_17(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_elementIdentifierCount_18(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axisCount_19(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_buttonCount_20(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_compoundElementCount_21(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_axis2DCount_22(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hatCount_23(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_joystickTypes_24(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hwAxisCalibrationData_25(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hwAxisRanges_26(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hwAxisInfo_27(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_hwButtonInfo_28(),
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3::get_offset_of_compoundElements_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[10] = 
{
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_controllerName_0(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_hardwareMapIdentifier_1(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_map_2(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_buttonCount_3(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_axisCount_4(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_elementIdentifiers_5(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_compoundElements_6(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_useSystemName_7(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_isUnknownController_8(),
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0::get_offset_of_joystickTypes_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[11] = 
{
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__id_0(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__name_1(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__type_2(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__descriptiveName_3(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__positiveDescriptiveName_4(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__negativeDescriptiveName_5(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__behaviorId_6(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__userAssignable_7(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of__categoryId_8(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of_iytzGAPUpbIpzJUCFcpOjccCOyV_9(),
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA::get_offset_of_sIyECxJaRrJpGveQCRvGGrqHjleG_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[23] = 
{
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__id_0(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__name_1(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__joystickAxisSensitivity_2(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__digitalAxisSimulation_3(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__digitalAxisSnap_4(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__digitalAxisInstantReverse_5(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__digitalAxisGravity_6(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__digitalAxisSensitivity_7(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__mouseXYAxisMode_8(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__mouseOtherAxisMode_9(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__mouseXYAxisSensitivity_10(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__mouseXYAxisDeltaCalc_11(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__mouseOtherAxisSensitivity_12(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__customControllerAxisSensitivity_13(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonDoublePressSpeed_14(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonShortPressTime_15(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonShortPressExpiresIn_16(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonLongPressTime_17(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonLongPressExpiresIn_18(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonDeadZone_19(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonDownBuffer_20(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonRepeatRate_21(),
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B::get_offset_of__buttonRepeatDelay_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[5] = 
{
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918::get_offset_of__name_0(),
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918::get_offset_of__descriptiveName_1(),
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918::get_offset_of__tag_2(),
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918::get_offset_of__id_3(),
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918::get_offset_of__userAssignable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4643[3] = 
{
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602::get_offset_of__checkConflictsWithAllCategories_5(),
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602::get_offset_of__checkConflictsCategoryIds_6(),
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602::get_offset_of__checkConflictsCategoryIds_readOnly_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[3] = 
{
	InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996::get_offset_of__name_0(),
	InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996::get_offset_of__descriptiveName_1(),
	InputLayout_t5DB3F9E538A5635352174D0E54C258BF1A7D4996::get_offset_of__id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB), -1, sizeof(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4645[14] = 
{
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_gwdVBQEakPuszKrkhFaRVdsPWKC_0(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_1(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB::get_offset_of_qyFSGVtHJSgKjJMsehfQmDTGigQ_2(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB::get_offset_of_uFaaNepxrqhsVgtTjhTtdKBmAVO_3(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB::get_offset_of_suCpPRGzPwMIarMELiehjJQHZqQb_4(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB::get_offset_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_5(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB::get_offset_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_6(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_LYelxTpCENiezznyJAAbgVviEhM_7(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_8(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_9(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_10(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_11(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_12(),
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB_StaticFields::get_offset_of_bNWtItxSTpScLiOAwzhSFiRFjTf_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (Context_tDDC4D5CD175FF95675F41961F66B1383287803A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[5] = 
{
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_0(),
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1::get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1(),
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1::get_offset_of_VBSctPxMxVKKrVoFlGEfnDnmZKE_2(),
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1::get_offset_of_hxtkUvmSBcPNTccpytzgYxfepmD_3(),
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1::get_offset_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (ConflictResponse_tD282F0812934B4E73323CF33596FAA52653D8709)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4647[5] = 
{
	ConflictResponse_tD282F0812934B4E73323CF33596FAA52653D8709::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (EventData_tE4C486A467E7CB80A9F88B04456F63263515B552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[1] = 
{
	EventData_tE4C486A467E7CB80A9F88B04456F63263515B552::get_offset_of_inputMapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (InputMappedEventData_tCA411F2D75C6E2528E495EC25B99F10347B364EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[1] = 
{
	InputMappedEventData_tCA411F2D75C6E2528E495EC25B99F10347B364EA::get_offset_of_actionElementMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (CanceledEventData_tE1D69F86DE424A4F6966140508692822A8C0B07C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[1] = 
{
	CanceledEventData_tE1D69F86DE424A4F6966140508692822A8C0B07C::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (ErrorEventData_tBDF0CB7D300C80F052BF9C210915F16E31DB8518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[1] = 
{
	ErrorEventData_tBDF0CB7D300C80F052BF9C210915F16E31DB8518::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (TimedOutEventData_tFC034B3F052C062A38BD91E6A3D8FB4A068B2DA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (StartedEventData_t796FB80A1C2E105C239083DFB676BAF58114C3BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (StoppedEventData_t1261F5DA0E3B39D19C547DAD150E163DA9A521C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4655[4] = 
{
	ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966::get_offset_of_responseCallback_1(),
	ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966::get_offset_of_assignment_2(),
	ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966::get_offset_of_conflicts_3(),
	ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966::get_offset_of_isProtected_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (brixqxKPyakMwXgqKReyPWdHwsQ_t3A3777B27092A6D39B44AE34783B57673FE32429)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4656[8] = 
{
	brixqxKPyakMwXgqKReyPWdHwsQ_t3A3777B27092A6D39B44AE34783B57673FE32429::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (Status_t5F20D760EC07EDF6D070B7873DEC0BF0DC62563F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4657[4] = 
{
	Status_t5F20D760EC07EDF6D070B7873DEC0BF0DC62563F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4658[12] = 
{
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_0(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_hQaGSpfGCOBUmxFmTPAEMXgnEuuD_1(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_PgbyaSUZMLVkFsALCONtPTqsomL_2(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_SpcTsLWPUhOBPVkDNBqZAMhkbqLa_3(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_4(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_okVQLiVpJrDffbfnTenwRguCTO_5(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_ucCLNrZtIdCAupOtarnDQmWuqaY_6(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_7(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_arhYXtPyWSnzTyYlloYwgcgCIim_8(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_uaFbZuRrXXOtzVtiKcPkivhEZUJ_9(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_PvUcooLAGoMJLPclRykwKEFFGNc_10(),
	WLaANgjWTacepVAvNGdKisMgYMnI_t0BC62303F41F016E0E87796369BDAD18DCFFEC76::get_offset_of_kHLgOPKOLcwzyhtNMzawWuqLDKiA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (kRecKFGHzUOVnGGERKWYGMZzoGz_t3627C5CD6FC8269D93222BD7AB0B9B946605FB27)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4659[3] = 
{
	kRecKFGHzUOVnGGERKWYGMZzoGz_t3627C5CD6FC8269D93222BD7AB0B9B946605FB27::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (HpatyHNcsbxyoSINVtOzDqCJcGH_tA2328BA9F113B56E8C0B0AE2CEF2E380242996B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4660[3] = 
{
	HpatyHNcsbxyoSINVtOzDqCJcGH_tA2328BA9F113B56E8C0B0AE2CEF2E380242996B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4661[7] = 
{
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_0(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_1(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_GgDLdlfkTgFJHhmkQlYKKtxusyw_2(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_ITFRagOFmBEurFeJevBABAnzaiY_5(),
	cgpUlBrDzLixoMOCLlievvtyCpu_tFD0CD2D60B023D814B4E581221B865B3D5673C1C::get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939), -1, sizeof(Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4662[18] = 
{
	0,
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_SEVDFzCpjCtpApiwQRcLHQrQkQQf_1(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_IGOSFeqRhMnFLBNwccaEaBGGbWj_2(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_HSLudSFRvxufIdhBdMeCsFDES_3(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_dpZMOEDlEtSDdcGmfkNWCUboVdF_4(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_WvuAcjIVQhUWfNyQbmSxXpNkWWO_5(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_mICJNeaKlqPZPYqoGBhEzpLOaJl_6(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_qVSUFZqdojuOKnnFGhpHhqfCPhuE_7(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_oNAjftmRmHbVnhAHFntnGVnqaTP_8(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_cTKnfXdudtiGNvTKmZVIEstpxVo_9(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_KiSnKvoujFbxeceQXufddMWqUaJ_10(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_BecnniJiivNwUjdDVdhjjIvByhX_11(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_vXsRwQupDqHXJafbToenzqRwJAi_12(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_nrSXHvZeEScuFRfWNmjIVsYZdDD_13(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_exushwyBliTrYYYHyOJbNCKGEoh_14(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_PFXJmRDFRPcTIDElVgSaNDXBICR_15(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939::get_offset_of_GFDmSkApjRNFYNqNgaFsKCNUJsST_16(),
	Options_t9F3F2D77C9BAFE0CF8DA75255B204034D39D9939_StaticFields::get_offset_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4663[5] = 
{
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F::get_offset_of_tYkPhHKJbyjvvknVLlMKJTLiOaz_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F::get_offset_of_JDGDYzdeEzvHIVUZerqzkiMsMiFn_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F::get_offset_of_bYZGQoTMhLQpyPqIskzvubOJRuj_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F::get_offset_of_zpGetsBoICokKMfkMcJbuggxTfXW_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerSaveData_t9D38ADC9F21E15E0C9B591E69748A7A5D0164B4F::get_offset_of_AVCVdipomjByHRQVMJMnJzrNKqH_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4664[9] = 
{
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_BBonIgYmbXiDXVdBLuciuUHykHc_4(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_5(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_bKJattiDCgqUafgGgzBoXnBGAIWb_6(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7(),
	yUsqPkUjmbyOsHDSGGXSBFwSyIYL_tEC6CE14C4DE8B15ADF0A433E294775B1531587A8::get_offset_of_JWGPoriiedknEFskVzNUGNUwScr_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4665[3] = 
{
	gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED::get_offset_of_yEkehjyseCgyriougfgfGvsREsrE_0(),
	gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED::get_offset_of_FZXDSDLUnVXQjaPxhAgaCZLUCiyG_1(),
	gQxvLRqJyPpkclzINkoEPJOqGeY_t98ACE43235F8D25C7B8CC4BF4B4C985F779271ED::get_offset_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4666[2] = 
{
	jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5::get_offset_of_jxXPzEnjWvXFNyxvWfoRieGVFhL_0(),
	jufVWXOncnQzwwsvxneDefLrtLg_t66F4FC80AB91ADF264CA6D8B3927A37DB7633CF5::get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4667[37] = 
{
	Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4668[6] = 
{
	EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4669[5] = 
{
	WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4670[4] = 
{
	ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4671[5] = 
{
	ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4672[6] = 
{
	WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4673[4] = 
{
	OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4674[4] = 
{
	LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4675[3] = 
{
	WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4676[3] = 
{
	XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4677[3] = 
{
	PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4678[3] = 
{
	WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (DirectInputAxis_tC8458C206C2EDEE13DE4172B8E19AE815598EFBB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4679[34] = 
{
	DirectInputAxis_tC8458C206C2EDEE13DE4172B8E19AE815598EFBB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (RawInputAxis_t6D0B3C134A3AA6DEB63506F12A711EE09190D71A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4680[14] = 
{
	RawInputAxis_t6D0B3C134A3AA6DEB63506F12A711EE09190D71A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4681[11] = 
{
	XInputDeviceSubType_t0AA28279AC0024D8FF2C82D8F9E6F15B67980E82::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4682[8] = 
{
	XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4683[17] = 
{
	XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4684[5] = 
{
	OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4685[32] = 
{
	UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4686[22] = 
{
	UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4687[3] = 
{
	WebGLGamepadMappingType_t6261DAA3DB60527AB5E670A22C5A75F0F95B0854::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4688[9] = 
{
	WebGLWebBrowserType_t4191AA71A6B0A7BC1EE7F9DC4EF653C89F1FCA45::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4689[8] = 
{
	WebGLOSType_t23F15ECBA95C71FB57068082F24204CA5C38B939::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C), -1, sizeof(QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4690[8] = 
{
	0,
	0,
	0,
	QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields::get_offset_of_ypmsdOiwBhQCBkOhnBGNddpWwGuj_3(),
	QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields::get_offset_of_qvhEmSqxwBIRRYBLviUpqJAtSAd_4(),
	QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields::get_offset_of_hTwPcflWVMjpEBOLCnrfSQYNgFWk_5(),
	QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields::get_offset_of_TOvnMXGXtktHXIImNpDtrdOjnmr_6(),
	QpvvZfySeMbDHmyLdcLREwbyiKx_tFBF4EEB6EF5F42231A4B6871F0FC5C500ABA360C_StaticFields::get_offset_of_KWcpfLqiYUbpVOdTEffGPEkMXgq_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (ozSeLFXyuAcLzzaVdgnuGAElVQkO_t2F08A6E3E84CC6A2DA07B7ADC6F5841E97ECFB87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4691[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (onFbCAKOPIvohuSERjpQctpytQE_tF6079688B3422631A334CDB3AF58498916421407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4693[8] = 
{
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_hNtENxmAKBGmoTLtEcwPGIjaiDV_0(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_BrDDeraUArFZwzhmkxdxMTPGpvvB_1(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_2(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_3(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_WnpeOEozhleBiLROoiweUAwvnlR_4(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_cEEqYVdhEBIXdUFBogquGTYgUYYl_5(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(),
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F::get_offset_of_controllers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC), -1, sizeof(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4694[17] = 
{
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_TiDRaJtNeCdLlYBqWUbUiRHsfNd_0(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_whOXOUQXtLtOtzMQsfCZDcYviPD_1(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_BuGnLKIMjwgXSWwDepEcRQAPEATG_2(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_yCFRyLTuDDJPotjpDvdOaCEDDvN_3(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_EODhOMizDenmfzfktoHJosKgaaN_4(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_StyMgvhXDptNsffkLGbApHZhpv_5(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_MBSfFDOuOKrryYPaClUnEhXiMkX_6(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_DQgqMYhPHoLRPOEOdGMwJQINGZeG_7(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_8(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_9(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_stygjDDRGdOuLuSiDJenZijkpLMI_10(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_maps_12(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_conflictChecking_13(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC::get_offset_of_polling_14(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields::get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_15(),
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC_StaticFields::get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4695[3] = 
{
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1::get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_0(),
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1::get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_1(),
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4696[16] = 
{
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_6(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_11(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_EAmCNKrLCDCLzvUcjfqLawIzxXnC_12(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_yBoPvusNueqLLNpcBTmQkrFNwKK_13(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_IjXjewKXqAsfLkHXGeKSVCASvmk_14(),
	zSPJMbexjjkjeWKVGtwjjMihLCL_t4E1039DFD3D91BDAC57E46AFC266ABC7444E7A7B::get_offset_of_HQPsrbVufqLRYQEIuPjShlVZFeA_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { sizeof (HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4697[18] = 
{
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_6(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_7(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_13(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_BXRUazznMnLjBmUEMAlPSqVFAsz_14(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_afZIrUpxaCTlKGOVgcffewJIetd_15(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_eIkOLImiGKegUDLZCJYqakcjxCxa_16(),
	HQvYxgsPCdJKnLhtkVcNGQHHNkC_tDF363EFAD350ABD854DC2C026463C5726E6C4918::get_offset_of_QZQLDIvnrZfxWplZEbMmdPKplWd_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[14] = 
{
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_9(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_mhPcKmSeaabUOVNWPuCtikHcKmD_10(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_NhYMrXcHzacPXXKJMJTTVZKBfDA_11(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_IGigngCNqCmUsaNSVGwbmQviIaE_12(),
	zZkPzXmmxFKIisdCevBSmFgUxdl_tEA5A235BA27DD3C28B2242D5456AAE4FA839DC6E::get_offset_of_uxZfSXhGhmKSDzFgWwgHNAtYxXya_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { sizeof (ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4699[16] = 
{
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_10(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_11(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_jEOhhUBDUEDIAqVbvVAWtqVvlhq_12(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_MLVBiKOEXDsCXyZqbbGvLecnvJs_13(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_MpmkTLgqzsnDcNivBGXxmvdPdUC_14(),
	ObvQSUvTMyjLcAUakMRRSoNZMBd_tCA6C565C3749C45A489000702F517348E8E2FAFB::get_offset_of_WUQWpaJgJGBZuAggCsqpFzBHkvp_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
