﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HeaderInfo>
struct Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket>
struct Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212;
// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue>
struct List_1_t4C4438F9E20C453876568717EEB3C007B6273D94;
// System.Collections.Generic.List`1<System.Net.Http.Headers.RangeItemHeaderValue>
struct List_1_tBC994597F79973AB8B0C84504B8869AD94636845;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.Func`3<UnityEngine.LogType,System.Object,System.Boolean>
struct Func_3_t438CBBFB0B5FB2391C878B5C188BF587009211EB;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Net.Http.Headers.EntityTagHeaderValue
struct EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE;
// System.Net.Http.Headers.HttpHeaders
struct HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1;
// System.Net.Http.Headers.HttpHeaders/HeaderBucket
struct HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F;
// System.Net.Http.Headers.HttpRequestHeaders
struct HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4;
// System.Net.Http.Headers.ProductHeaderValue
struct ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5;
// System.Net.Http.HttpMessageHandler
struct HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F;
// UnityEngine.AssetBundle
struct AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F;
// UnityEngine.TextCore.GlyphRect[]
struct GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2;
// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[]
struct GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662;
// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[]
struct GlyphPairAdjustmentRecordU5BU5D_tE4D7700D820175D7726010904F8477E90C1823E7;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6814C120E23E8C14860FFC42894ADFDD3105B3E6_H
#define U3CMODULEU3E_T6814C120E23E8C14860FFC42894ADFDD3105B3E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6814C120E23E8C14860FFC42894ADFDD3105B3E6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6814C120E23E8C14860FFC42894ADFDD3105B3E6_H
#ifndef U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#define U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tDE5A299227351E064CF5069210AC8ED1294BD51A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#ifndef U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#define U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t410187D184BFEA098C57AA90C1EEBB14DCD72176 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#ifndef U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#define U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t56CA3936A9EFABF2ED20401359C40BFE63F85A11 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#ifndef U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#define U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#ifndef U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#define U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifndef U3CMODULEU3E_T79D7DE725655CFC1B063EA359E8D75692CF5DC2F_H
#define U3CMODULEU3E_T79D7DE725655CFC1B063EA359E8D75692CF5DC2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t79D7DE725655CFC1B063EA359E8D75692CF5DC2F 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T79D7DE725655CFC1B063EA359E8D75692CF5DC2F_H
#ifndef U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#define U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t18E01DC18978C39365422BE25552B43FDE647BA1 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS54_0_T0F2354CF28D5D074D97CF436C40F413F9AA442BE_H
#define U3CU3EC__DISPLAYCLASS54_0_T0F2354CF28D5D074D97CF436C40F413F9AA442BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween_<>c__DisplayClass54_0
struct  U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.DOTween_<>c__DisplayClass54_0::v
	float ___v_0;
	// DG.Tweening.Core.DOSetter`1<System.Single> DG.Tweening.DOTween_<>c__DisplayClass54_0::setter
	DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * ___setter_1;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE, ___v_0)); }
	inline float get_v_0() const { return ___v_0; }
	inline float* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(float value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_setter_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE, ___setter_1)); }
	inline DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * get_setter_1() const { return ___setter_1; }
	inline DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 ** get_address_of_setter_1() { return &___setter_1; }
	inline void set_setter_1(DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * value)
	{
		___setter_1 = value;
		Il2CppCodeGenWriteBarrier((&___setter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS54_0_T0F2354CF28D5D074D97CF436C40F413F9AA442BE_H
#ifndef DOVIRTUAL_T5B66AD4FF33DE5D151DF9AE2CD8AC3EEBFBD9CA3_H
#define DOVIRTUAL_T5B66AD4FF33DE5D151DF9AE2CD8AC3EEBFBD9CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOVirtual
struct  DOVirtual_t5B66AD4FF33DE5D151DF9AE2CD8AC3EEBFBD9CA3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOVIRTUAL_T5B66AD4FF33DE5D151DF9AE2CD8AC3EEBFBD9CA3_H
#ifndef AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#define AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.AuthenticationHeaderValue
struct  AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.AuthenticationHeaderValue::<Parameter>k__BackingField
	String_t* ___U3CParameterU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.AuthenticationHeaderValue::<Scheme>k__BackingField
	String_t* ___U3CSchemeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParameterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6, ___U3CParameterU3Ek__BackingField_0)); }
	inline String_t* get_U3CParameterU3Ek__BackingField_0() const { return ___U3CParameterU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CParameterU3Ek__BackingField_0() { return &___U3CParameterU3Ek__BackingField_0; }
	inline void set_U3CParameterU3Ek__BackingField_0(String_t* value)
	{
		___U3CParameterU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParameterU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSchemeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#ifndef COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#define COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CollectionExtensions
struct  CollectionExtensions_tF55A4677D40CF6208C37CAB46E039D5DF6DC790B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#ifndef COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#define COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CollectionParser
struct  CollectionParser_t1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#ifndef CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#define CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ContentDispositionHeaderValue
struct  ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ContentDispositionHeaderValue::dispositionType
	String_t* ___dispositionType_0;
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.ContentDispositionHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_1;

public:
	inline static int32_t get_offset_of_dispositionType_0() { return static_cast<int32_t>(offsetof(ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0, ___dispositionType_0)); }
	inline String_t* get_dispositionType_0() const { return ___dispositionType_0; }
	inline String_t** get_address_of_dispositionType_0() { return &___dispositionType_0; }
	inline void set_dispositionType_0(String_t* value)
	{
		___dispositionType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dispositionType_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0, ___parameters_1)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_1() const { return ___parameters_1; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#ifndef ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#define ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.EntityTagHeaderValue
struct  EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Http.Headers.EntityTagHeaderValue::<IsWeak>k__BackingField
	bool ___U3CIsWeakU3Ek__BackingField_1;
	// System.String System.Net.Http.Headers.EntityTagHeaderValue::<Tag>k__BackingField
	String_t* ___U3CTagU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsWeakU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE, ___U3CIsWeakU3Ek__BackingField_1)); }
	inline bool get_U3CIsWeakU3Ek__BackingField_1() const { return ___U3CIsWeakU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsWeakU3Ek__BackingField_1() { return &___U3CIsWeakU3Ek__BackingField_1; }
	inline void set_U3CIsWeakU3Ek__BackingField_1(bool value)
	{
		___U3CIsWeakU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE, ___U3CTagU3Ek__BackingField_2)); }
	inline String_t* get_U3CTagU3Ek__BackingField_2() const { return ___U3CTagU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTagU3Ek__BackingField_2() { return &___U3CTagU3Ek__BackingField_2; }
	inline void set_U3CTagU3Ek__BackingField_2(String_t* value)
	{
		___U3CTagU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagU3Ek__BackingField_2), value);
	}
};

struct EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields
{
public:
	// System.Net.Http.Headers.EntityTagHeaderValue System.Net.Http.Headers.EntityTagHeaderValue::any
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * ___any_0;

public:
	inline static int32_t get_offset_of_any_0() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields, ___any_0)); }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * get_any_0() const { return ___any_0; }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE ** get_address_of_any_0() { return &___any_0; }
	inline void set_any_0(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * value)
	{
		___any_0 = value;
		Il2CppCodeGenWriteBarrier((&___any_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#ifndef HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#define HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HashCodeCalculator
struct  HashCodeCalculator_t0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#ifndef HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#define HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders_HeaderBucket
struct  HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F  : public RuntimeObject
{
public:
	// System.Object System.Net.Http.Headers.HttpHeaders_HeaderBucket::Parsed
	RuntimeObject * ___Parsed_0;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.HttpHeaders_HeaderBucket::values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___values_1;
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.HttpHeaders_HeaderBucket::CustomToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___CustomToString_2;

public:
	inline static int32_t get_offset_of_Parsed_0() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___Parsed_0)); }
	inline RuntimeObject * get_Parsed_0() const { return ___Parsed_0; }
	inline RuntimeObject ** get_address_of_Parsed_0() { return &___Parsed_0; }
	inline void set_Parsed_0(RuntimeObject * value)
	{
		___Parsed_0 = value;
		Il2CppCodeGenWriteBarrier((&___Parsed_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___values_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_values_1() const { return ___values_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_CustomToString_2() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___CustomToString_2)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_CustomToString_2() const { return ___CustomToString_2; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_CustomToString_2() { return &___CustomToString_2; }
	inline void set_CustomToString_2(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___CustomToString_2 = value;
		Il2CppCodeGenWriteBarrier((&___CustomToString_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#ifndef LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#define LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Lexer
struct  Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.Lexer::s
	String_t* ___s_3;
	// System.Int32 System.Net.Http.Headers.Lexer::pos
	int32_t ___pos_4;

public:
	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3, ___s_3)); }
	inline String_t* get_s_3() const { return ___s_3; }
	inline String_t** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(String_t* value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}
};

struct Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields
{
public:
	// System.Boolean[] System.Net.Http.Headers.Lexer::token_chars
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___token_chars_0;
	// System.Int32 System.Net.Http.Headers.Lexer::last_token_char
	int32_t ___last_token_char_1;
	// System.String[] System.Net.Http.Headers.Lexer::dt_formats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___dt_formats_2;

public:
	inline static int32_t get_offset_of_token_chars_0() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___token_chars_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_token_chars_0() const { return ___token_chars_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_token_chars_0() { return &___token_chars_0; }
	inline void set_token_chars_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___token_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___token_chars_0), value);
	}

	inline static int32_t get_offset_of_last_token_char_1() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___last_token_char_1)); }
	inline int32_t get_last_token_char_1() const { return ___last_token_char_1; }
	inline int32_t* get_address_of_last_token_char_1() { return &___last_token_char_1; }
	inline void set_last_token_char_1(int32_t value)
	{
		___last_token_char_1 = value;
	}

	inline static int32_t get_offset_of_dt_formats_2() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___dt_formats_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_dt_formats_2() const { return ___dt_formats_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_dt_formats_2() { return &___dt_formats_2; }
	inline void set_dt_formats_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___dt_formats_2 = value;
		Il2CppCodeGenWriteBarrier((&___dt_formats_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#ifndef MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#define MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.MediaTypeHeaderValue
struct  MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.MediaTypeHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_0;
	// System.String System.Net.Http.Headers.MediaTypeHeaderValue::media_type
	String_t* ___media_type_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1, ___parameters_0)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_0() const { return ___parameters_0; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_media_type_1() { return static_cast<int32_t>(offsetof(MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1, ___media_type_1)); }
	inline String_t* get_media_type_1() const { return ___media_type_1; }
	inline String_t** get_address_of_media_type_1() { return &___media_type_1; }
	inline void set_media_type_1(String_t* value)
	{
		___media_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___media_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#ifndef NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#define NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.NameValueHeaderValue
struct  NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.NameValueHeaderValue::value
	String_t* ___value_0;
	// System.String System.Net.Http.Headers.NameValueHeaderValue::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#ifndef PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#define PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser
struct  Parser_tC9CE5F5FFDD4927D039C3189B5945DA5EF958025  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#ifndef DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#define DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_DateTime
struct  DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F  : public RuntimeObject
{
public:

public:
};

struct DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.Parser_DateTime::ToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___ToString_0;

public:
	inline static int32_t get_offset_of_ToString_0() { return static_cast<int32_t>(offsetof(DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields, ___ToString_0)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_ToString_0() const { return ___ToString_0; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_ToString_0() { return &___ToString_0; }
	inline void set_ToString_0(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___ToString_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#ifndef U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#define U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_DateTime_<>c
struct  U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields
{
public:
	// System.Net.Http.Headers.Parser_DateTime_<>c System.Net.Http.Headers.Parser_DateTime_<>c::<>9
	U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#ifndef EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#define EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_EmailAddress
struct  EmailAddress_t34037A92039853D504922D84E1512E6BE4FBDE1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#ifndef HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#define HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_Host
struct  Host_t225394EBFA7427D924DC794F6FEB33C7B19E6583  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#ifndef INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#define INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_Int
struct  Int_t9123F050289CA0271A077EFF47440CBD5C366316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#ifndef LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#define LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_Long
struct  Long_t28918BF6DBAB433B07F2F91D220DF42F896C5C63  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#ifndef MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#define MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_MD5
struct  MD5_tBF240EC56773202152DBB5C53796D718895EC4EC  : public RuntimeObject
{
public:

public:
};

struct MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.Parser_MD5::ToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___ToString_0;

public:
	inline static int32_t get_offset_of_ToString_0() { return static_cast<int32_t>(offsetof(MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields, ___ToString_0)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_ToString_0() const { return ___ToString_0; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_ToString_0() { return &___ToString_0; }
	inline void set_ToString_0(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___ToString_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#ifndef U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#define U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_MD5_<>c
struct  U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields
{
public:
	// System.Net.Http.Headers.Parser_MD5_<>c System.Net.Http.Headers.Parser_MD5_<>c::<>9
	U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#ifndef TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#define TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_TimeSpanSeconds
struct  TimeSpanSeconds_t147412A8A309180783B8369B158301EFA80A9DA5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#ifndef URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#define URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser_Uri
struct  Uri_tAA37A4AC2B4CCC173820F682599007F88D08407D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#ifndef PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#define PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ProductHeaderValue
struct  ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ProductHeaderValue::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.ProductHeaderValue::<Version>k__BackingField
	String_t* ___U3CVersionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5, ___U3CVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CVersionU3Ek__BackingField_1() const { return ___U3CVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CVersionU3Ek__BackingField_1() { return &___U3CVersionU3Ek__BackingField_1; }
	inline void set_U3CVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVersionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#ifndef PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#define PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ProductInfoHeaderValue
struct  ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ProductInfoHeaderValue::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;
	// System.Net.Http.Headers.ProductHeaderValue System.Net.Http.Headers.ProductInfoHeaderValue::<Product>k__BackingField
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * ___U3CProductU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProductU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246, ___U3CProductU3Ek__BackingField_1)); }
	inline ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * get_U3CProductU3Ek__BackingField_1() const { return ___U3CProductU3Ek__BackingField_1; }
	inline ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 ** get_address_of_U3CProductU3Ek__BackingField_1() { return &___U3CProductU3Ek__BackingField_1; }
	inline void set_U3CProductU3Ek__BackingField_1(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * value)
	{
		___U3CProductU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#ifndef RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#define RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeHeaderValue
struct  RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.RangeItemHeaderValue> System.Net.Http.Headers.RangeHeaderValue::ranges
	List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * ___ranges_0;
	// System.String System.Net.Http.Headers.RangeHeaderValue::unit
	String_t* ___unit_1;

public:
	inline static int32_t get_offset_of_ranges_0() { return static_cast<int32_t>(offsetof(RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723, ___ranges_0)); }
	inline List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * get_ranges_0() const { return ___ranges_0; }
	inline List_1_tBC994597F79973AB8B0C84504B8869AD94636845 ** get_address_of_ranges_0() { return &___ranges_0; }
	inline void set_ranges_0(List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * value)
	{
		___ranges_0 = value;
		Il2CppCodeGenWriteBarrier((&___ranges_0), value);
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723, ___unit_1)); }
	inline String_t* get_unit_1() const { return ___unit_1; }
	inline String_t** get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(String_t* value)
	{
		___unit_1 = value;
		Il2CppCodeGenWriteBarrier((&___unit_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#ifndef TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#define TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.TransferCodingHeaderValue
struct  TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.TransferCodingHeaderValue::value
	String_t* ___value_0;
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.TransferCodingHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C, ___parameters_1)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_1() const { return ___parameters_1; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#ifndef VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#define VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ViaHeaderValue
struct  ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ViaHeaderValue::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ProtocolName>k__BackingField
	String_t* ___U3CProtocolNameU3Ek__BackingField_1;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ProtocolVersion>k__BackingField
	String_t* ___U3CProtocolVersionU3Ek__BackingField_2;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ReceivedBy>k__BackingField
	String_t* ___U3CReceivedByU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProtocolNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CProtocolNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CProtocolNameU3Ek__BackingField_1() const { return ___U3CProtocolNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProtocolNameU3Ek__BackingField_1() { return &___U3CProtocolNameU3Ek__BackingField_1; }
	inline void set_U3CProtocolNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CProtocolNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CProtocolVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CProtocolVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CProtocolVersionU3Ek__BackingField_2() const { return ___U3CProtocolVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CProtocolVersionU3Ek__BackingField_2() { return &___U3CProtocolVersionU3Ek__BackingField_2; }
	inline void set_U3CProtocolVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CProtocolVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolVersionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CReceivedByU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CReceivedByU3Ek__BackingField_3)); }
	inline String_t* get_U3CReceivedByU3Ek__BackingField_3() const { return ___U3CReceivedByU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CReceivedByU3Ek__BackingField_3() { return &___U3CReceivedByU3Ek__BackingField_3; }
	inline void set_U3CReceivedByU3Ek__BackingField_3(String_t* value)
	{
		___U3CReceivedByU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReceivedByU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#ifndef HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#define HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpMessageHandler
struct  HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#ifndef HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#define HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpMessageInvoker
struct  HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72  : public RuntimeObject
{
public:
	// System.Net.Http.HttpMessageHandler System.Net.Http.HttpMessageInvoker::handler
	HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * ___handler_0;
	// System.Boolean System.Net.Http.HttpMessageInvoker::disposeHandler
	bool ___disposeHandler_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72, ___handler_0)); }
	inline HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * get_handler_0() const { return ___handler_0; }
	inline HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_disposeHandler_1() { return static_cast<int32_t>(offsetof(HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72, ___disposeHandler_1)); }
	inline bool get_disposeHandler_1() const { return ___disposeHandler_1; }
	inline bool* get_address_of_disposeHandler_1() { return &___disposeHandler_1; }
	inline void set_disposeHandler_1(bool value)
	{
		___disposeHandler_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#define ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo_SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifndef CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#define CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ContinuousEvent
struct  ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifndef CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#define CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#ifndef RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#define RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifndef REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#define REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields
{
public:
	// UnityEngine.RemoteSettings_UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * ___Completed_2;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}

	inline static int32_t get_offset_of_BeforeFetchFromServer_1() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___BeforeFetchFromServer_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_BeforeFetchFromServer_1() const { return ___BeforeFetchFromServer_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_BeforeFetchFromServer_1() { return &___BeforeFetchFromServer_1; }
	inline void set_BeforeFetchFromServer_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___BeforeFetchFromServer_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeFetchFromServer_1), value);
	}

	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Completed_2)); }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * get_Completed_2() const { return ___Completed_2; }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifndef FONTENGINE_T9316586F7FD894C97716C9C48AF485A137284FBC_H
#define FONTENGINE_T9316586F7FD894C97716C9C48AF485A137284FBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.FontEngine
struct  FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC  : public RuntimeObject
{
public:

public:
};

struct FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields
{
public:
	// UnityEngine.TextCore.LowLevel.FontEngine UnityEngine.TextCore.LowLevel.FontEngine::s_Instance
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC * ___s_Instance_0;
	// System.UInt32[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphIndexesToMarshall
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___s_GlyphIndexesToMarshall_1;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_IN
	GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* ___s_GlyphMarshallingStruct_IN_2;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_OUT
	GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* ___s_GlyphMarshallingStruct_OUT_3;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_FreeGlyphRects
	GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* ___s_FreeGlyphRects_4;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_UsedGlyphRects
	GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* ___s_UsedGlyphRects_5;
	// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphPairAdjustmentRecords
	GlyphPairAdjustmentRecordU5BU5D_tE4D7700D820175D7726010904F8477E90C1823E7* ___s_GlyphPairAdjustmentRecords_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphLookupDictionary
	Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * ___s_GlyphLookupDictionary_7;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_Instance_0)); }
	inline FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC * get_s_Instance_0() const { return ___s_Instance_0; }
	inline FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_s_GlyphIndexesToMarshall_1() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_GlyphIndexesToMarshall_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_s_GlyphIndexesToMarshall_1() const { return ___s_GlyphIndexesToMarshall_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_s_GlyphIndexesToMarshall_1() { return &___s_GlyphIndexesToMarshall_1; }
	inline void set_s_GlyphIndexesToMarshall_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___s_GlyphIndexesToMarshall_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphIndexesToMarshall_1), value);
	}

	inline static int32_t get_offset_of_s_GlyphMarshallingStruct_IN_2() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_GlyphMarshallingStruct_IN_2)); }
	inline GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* get_s_GlyphMarshallingStruct_IN_2() const { return ___s_GlyphMarshallingStruct_IN_2; }
	inline GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662** get_address_of_s_GlyphMarshallingStruct_IN_2() { return &___s_GlyphMarshallingStruct_IN_2; }
	inline void set_s_GlyphMarshallingStruct_IN_2(GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* value)
	{
		___s_GlyphMarshallingStruct_IN_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphMarshallingStruct_IN_2), value);
	}

	inline static int32_t get_offset_of_s_GlyphMarshallingStruct_OUT_3() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_GlyphMarshallingStruct_OUT_3)); }
	inline GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* get_s_GlyphMarshallingStruct_OUT_3() const { return ___s_GlyphMarshallingStruct_OUT_3; }
	inline GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662** get_address_of_s_GlyphMarshallingStruct_OUT_3() { return &___s_GlyphMarshallingStruct_OUT_3; }
	inline void set_s_GlyphMarshallingStruct_OUT_3(GlyphMarshallingStructU5BU5D_tB5E281DB809E6848B7CC9F02763B5E5AAE5E5662* value)
	{
		___s_GlyphMarshallingStruct_OUT_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphMarshallingStruct_OUT_3), value);
	}

	inline static int32_t get_offset_of_s_FreeGlyphRects_4() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_FreeGlyphRects_4)); }
	inline GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* get_s_FreeGlyphRects_4() const { return ___s_FreeGlyphRects_4; }
	inline GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2** get_address_of_s_FreeGlyphRects_4() { return &___s_FreeGlyphRects_4; }
	inline void set_s_FreeGlyphRects_4(GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* value)
	{
		___s_FreeGlyphRects_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_FreeGlyphRects_4), value);
	}

	inline static int32_t get_offset_of_s_UsedGlyphRects_5() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_UsedGlyphRects_5)); }
	inline GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* get_s_UsedGlyphRects_5() const { return ___s_UsedGlyphRects_5; }
	inline GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2** get_address_of_s_UsedGlyphRects_5() { return &___s_UsedGlyphRects_5; }
	inline void set_s_UsedGlyphRects_5(GlyphRectU5BU5D_t0C8059848359C24B032007E1B643D747C2BB2FB2* value)
	{
		___s_UsedGlyphRects_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UsedGlyphRects_5), value);
	}

	inline static int32_t get_offset_of_s_GlyphPairAdjustmentRecords_6() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_GlyphPairAdjustmentRecords_6)); }
	inline GlyphPairAdjustmentRecordU5BU5D_tE4D7700D820175D7726010904F8477E90C1823E7* get_s_GlyphPairAdjustmentRecords_6() const { return ___s_GlyphPairAdjustmentRecords_6; }
	inline GlyphPairAdjustmentRecordU5BU5D_tE4D7700D820175D7726010904F8477E90C1823E7** get_address_of_s_GlyphPairAdjustmentRecords_6() { return &___s_GlyphPairAdjustmentRecords_6; }
	inline void set_s_GlyphPairAdjustmentRecords_6(GlyphPairAdjustmentRecordU5BU5D_tE4D7700D820175D7726010904F8477E90C1823E7* value)
	{
		___s_GlyphPairAdjustmentRecords_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphPairAdjustmentRecords_6), value);
	}

	inline static int32_t get_offset_of_s_GlyphLookupDictionary_7() { return static_cast<int32_t>(offsetof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields, ___s_GlyphLookupDictionary_7)); }
	inline Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * get_s_GlyphLookupDictionary_7() const { return ___s_GlyphLookupDictionary_7; }
	inline Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B ** get_address_of_s_GlyphLookupDictionary_7() { return &___s_GlyphLookupDictionary_7; }
	inline void set_s_GlyphLookupDictionary_7(Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * value)
	{
		___s_GlyphLookupDictionary_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphLookupDictionary_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTENGINE_T9316586F7FD894C97716C9C48AF485A137284FBC_H
#ifndef UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#define UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifndef __STATICARRAYINITTYPESIZEU3D127_T9449C85D5F9E6911FB704ED4892BF6F420B567BA_H
#define __STATICARRAYINITTYPESIZEU3D127_T9449C85D5F9E6911FB704ED4892BF6F420B567BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D127
struct  __StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA__padding[127];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D127_T9449C85D5F9E6911FB704ED4892BF6F420B567BA_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#define KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.IEnumerable`1<System.String>>
struct  KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684, ___value_1)); }
	inline RuntimeObject* get_value_1() const { return ___value_1; }
	inline RuntimeObject** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#ifndef KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#define KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Net.Http.Headers.HttpHeaders_HeaderBucket>
struct  KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768, ___value_1)); }
	inline HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * get_value_1() const { return ___value_1; }
	inline HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#define MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.MediaTypeWithQualityHeaderValue
struct  MediaTypeWithQualityHeaderValue_tA90D440F6C0876845E895A0A3C61FA36CA2B120E  : public MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#ifndef NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#define NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.NameValueWithParametersHeaderValue
struct  NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF  : public NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.NameValueWithParametersHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF, ___parameters_2)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_2() const { return ___parameters_2; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#ifndef TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#define TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.TransferCodingWithQualityHeaderValue
struct  TransferCodingWithQualityHeaderValue_tCCBC3E7752056FE94AF475137682B15E84FA626C  : public TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#ifndef HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#define HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClientHandler
struct  HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049  : public HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894
{
public:
	// System.Boolean System.Net.Http.HttpClientHandler::allowAutoRedirect
	bool ___allowAutoRedirect_1;
	// System.Int32 System.Net.Http.HttpClientHandler::maxAutomaticRedirections
	int32_t ___maxAutomaticRedirections_2;
	// System.Int64 System.Net.Http.HttpClientHandler::maxRequestContentBufferSize
	int64_t ___maxRequestContentBufferSize_3;
	// System.Boolean System.Net.Http.HttpClientHandler::useCookies
	bool ___useCookies_4;
	// System.Boolean System.Net.Http.HttpClientHandler::useProxy
	bool ___useProxy_5;
	// System.String System.Net.Http.HttpClientHandler::connectionGroupName
	String_t* ___connectionGroupName_6;
	// System.Boolean System.Net.Http.HttpClientHandler::disposed
	bool ___disposed_7;

public:
	inline static int32_t get_offset_of_allowAutoRedirect_1() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___allowAutoRedirect_1)); }
	inline bool get_allowAutoRedirect_1() const { return ___allowAutoRedirect_1; }
	inline bool* get_address_of_allowAutoRedirect_1() { return &___allowAutoRedirect_1; }
	inline void set_allowAutoRedirect_1(bool value)
	{
		___allowAutoRedirect_1 = value;
	}

	inline static int32_t get_offset_of_maxAutomaticRedirections_2() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___maxAutomaticRedirections_2)); }
	inline int32_t get_maxAutomaticRedirections_2() const { return ___maxAutomaticRedirections_2; }
	inline int32_t* get_address_of_maxAutomaticRedirections_2() { return &___maxAutomaticRedirections_2; }
	inline void set_maxAutomaticRedirections_2(int32_t value)
	{
		___maxAutomaticRedirections_2 = value;
	}

	inline static int32_t get_offset_of_maxRequestContentBufferSize_3() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___maxRequestContentBufferSize_3)); }
	inline int64_t get_maxRequestContentBufferSize_3() const { return ___maxRequestContentBufferSize_3; }
	inline int64_t* get_address_of_maxRequestContentBufferSize_3() { return &___maxRequestContentBufferSize_3; }
	inline void set_maxRequestContentBufferSize_3(int64_t value)
	{
		___maxRequestContentBufferSize_3 = value;
	}

	inline static int32_t get_offset_of_useCookies_4() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___useCookies_4)); }
	inline bool get_useCookies_4() const { return ___useCookies_4; }
	inline bool* get_address_of_useCookies_4() { return &___useCookies_4; }
	inline void set_useCookies_4(bool value)
	{
		___useCookies_4 = value;
	}

	inline static int32_t get_offset_of_useProxy_5() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___useProxy_5)); }
	inline bool get_useProxy_5() const { return ___useProxy_5; }
	inline bool* get_address_of_useProxy_5() { return &___useProxy_5; }
	inline void set_useProxy_5(bool value)
	{
		___useProxy_5 = value;
	}

	inline static int32_t get_offset_of_connectionGroupName_6() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___connectionGroupName_6)); }
	inline String_t* get_connectionGroupName_6() const { return ___connectionGroupName_6; }
	inline String_t** get_address_of_connectionGroupName_6() { return &___connectionGroupName_6; }
	inline void set_connectionGroupName_6(String_t* value)
	{
		___connectionGroupName_6 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroupName_6), value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}
};

struct HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields
{
public:
	// System.Int64 System.Net.Http.HttpClientHandler::groupCounter
	int64_t ___groupCounter_0;

public:
	inline static int32_t get_offset_of_groupCounter_0() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields, ___groupCounter_0)); }
	inline int64_t get_groupCounter_0() const { return ___groupCounter_0; }
	inline int64_t* get_address_of_groupCounter_0() { return &___groupCounter_0; }
	inline void set_groupCounter_0(int64_t value)
	{
		___groupCounter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_TA635682CABFD60B7DF73271614FC04085E333AC5_H
#define NULLABLE_1_TA635682CABFD60B7DF73271614FC04085E333AC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Double>
struct  Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5 
{
public:
	// T System.Nullable`1::value
	double ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5, ___value_0)); }
	inline double get_value_0() const { return ___value_0; }
	inline double* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(double value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TA635682CABFD60B7DF73271614FC04085E333AC5_H
#ifndef NULLABLE_1_T802480A692F4F0D29F2185320296572054FB8C0B_H
#define NULLABLE_1_T802480A692F4F0D29F2185320296572054FB8C0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T802480A692F4F0D29F2185320296572054FB8C0B_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#define GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.GlyphMetrics
struct  GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB 
{
public:
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_Width_0)); }
	inline float get_m_Width_0() const { return ___m_Width_0; }
	inline float* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(float value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_Height_1)); }
	inline float get_m_Height_1() const { return ___m_Height_1; }
	inline float* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(float value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalBearingX_2() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalBearingX_2)); }
	inline float get_m_HorizontalBearingX_2() const { return ___m_HorizontalBearingX_2; }
	inline float* get_address_of_m_HorizontalBearingX_2() { return &___m_HorizontalBearingX_2; }
	inline void set_m_HorizontalBearingX_2(float value)
	{
		___m_HorizontalBearingX_2 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalBearingY_3() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalBearingY_3)); }
	inline float get_m_HorizontalBearingY_3() const { return ___m_HorizontalBearingY_3; }
	inline float* get_address_of_m_HorizontalBearingY_3() { return &___m_HorizontalBearingY_3; }
	inline void set_m_HorizontalBearingY_3(float value)
	{
		___m_HorizontalBearingY_3 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAdvance_4() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalAdvance_4)); }
	inline float get_m_HorizontalAdvance_4() const { return ___m_HorizontalAdvance_4; }
	inline float* get_address_of_m_HorizontalAdvance_4() { return &___m_HorizontalAdvance_4; }
	inline void set_m_HorizontalAdvance_4(float value)
	{
		___m_HorizontalAdvance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#ifndef GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#define GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.GlyphRect
struct  GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C 
{
public:
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Width_2)); }
	inline int32_t get_m_Width_2() const { return ___m_Width_2; }
	inline int32_t* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(int32_t value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Height_3)); }
	inline int32_t get_m_Height_3() const { return ___m_Height_3; }
	inline int32_t* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(int32_t value)
	{
		___m_Height_3 = value;
	}
};

struct GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C_StaticFields
{
public:
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___s_ZeroGlyphRect_4;

public:
	inline static int32_t get_offset_of_s_ZeroGlyphRect_4() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C_StaticFields, ___s_ZeroGlyphRect_4)); }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  get_s_ZeroGlyphRect_4() const { return ___s_ZeroGlyphRect_4; }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C * get_address_of_s_ZeroGlyphRect_4() { return &___s_ZeroGlyphRect_4; }
	inline void set_s_ZeroGlyphRect_4(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  value)
	{
		___s_ZeroGlyphRect_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#ifndef GLYPHVALUERECORD_TC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D_H
#define GLYPHVALUERECORD_TC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphValueRecord
struct  GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D 
{
public:
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;

public:
	inline static int32_t get_offset_of_m_XPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D, ___m_XPlacement_0)); }
	inline float get_m_XPlacement_0() const { return ___m_XPlacement_0; }
	inline float* get_address_of_m_XPlacement_0() { return &___m_XPlacement_0; }
	inline void set_m_XPlacement_0(float value)
	{
		___m_XPlacement_0 = value;
	}

	inline static int32_t get_offset_of_m_YPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D, ___m_YPlacement_1)); }
	inline float get_m_YPlacement_1() const { return ___m_YPlacement_1; }
	inline float* get_address_of_m_YPlacement_1() { return &___m_YPlacement_1; }
	inline void set_m_YPlacement_1(float value)
	{
		___m_YPlacement_1 = value;
	}

	inline static int32_t get_offset_of_m_XAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D, ___m_XAdvance_2)); }
	inline float get_m_XAdvance_2() const { return ___m_XAdvance_2; }
	inline float* get_address_of_m_XAdvance_2() { return &___m_XAdvance_2; }
	inline void set_m_XAdvance_2(float value)
	{
		___m_XAdvance_2 = value;
	}

	inline static int32_t get_offset_of_m_YAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D, ___m_YAdvance_3)); }
	inline float get_m_YAdvance_3() const { return ___m_YAdvance_3; }
	inline float* get_address_of_m_YAdvance_3() { return &___m_YAdvance_3; }
	inline void set_m_YAdvance_3(float value)
	{
		___m_YAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_TC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D127 <PrivateImplementationDetails>::A097044521F478B3A2A9A3AC52887BA733E4DE56
	__StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA  ___A097044521F478B3A2A9A3AC52887BA733E4DE56_0;

public:
	inline static int32_t get_offset_of_A097044521F478B3A2A9A3AC52887BA733E4DE56_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_StaticFields, ___A097044521F478B3A2A9A3AC52887BA733E4DE56_0)); }
	inline __StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA  get_A097044521F478B3A2A9A3AC52887BA733E4DE56_0() const { return ___A097044521F478B3A2A9A3AC52887BA733E4DE56_0; }
	inline __StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA * get_address_of_A097044521F478B3A2A9A3AC52887BA733E4DE56_0() { return &___A097044521F478B3A2A9A3AC52887BA733E4DE56_0; }
	inline void set_A097044521F478B3A2A9A3AC52887BA733E4DE56_0(__StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA  value)
	{
		___A097044521F478B3A2A9A3AC52887BA733E4DE56_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_H
#ifndef AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#define AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifndef AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#define AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifndef COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#define COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Color2
struct  Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E 
{
public:
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___cb_1;

public:
	inline static int32_t get_offset_of_ca_0() { return static_cast<int32_t>(offsetof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E, ___ca_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_ca_0() const { return ___ca_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_ca_0() { return &___ca_0; }
	inline void set_ca_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___ca_0 = value;
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E, ___cb_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_cb_1() const { return ___cb_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___cb_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#ifndef NESTEDTWEENFAILUREBEHAVIOUR_TC6EED7548BA8FCF6B20E0909C2D7355CD09BB183_H
#define NESTEDTWEENFAILUREBEHAVIOUR_TC6EED7548BA8FCF6B20E0909C2D7355CD09BB183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.NestedTweenFailureBehaviour
struct  NestedTweenFailureBehaviour_tC6EED7548BA8FCF6B20E0909C2D7355CD09BB183 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.NestedTweenFailureBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NestedTweenFailureBehaviour_tC6EED7548BA8FCF6B20E0909C2D7355CD09BB183, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDTWEENFAILUREBEHAVIOUR_TC6EED7548BA8FCF6B20E0909C2D7355CD09BB183_H
#ifndef REWINDCALLBACKMODE_T2F1DD72D77203C98698C6FDD4C6B5683E019F84A_H
#define REWINDCALLBACKMODE_T2F1DD72D77203C98698C6FDD4C6B5683E019F84A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.RewindCallbackMode
struct  RewindCallbackMode_t2F1DD72D77203C98698C6FDD4C6B5683E019F84A 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.RewindCallbackMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RewindCallbackMode_t2F1DD72D77203C98698C6FDD4C6B5683E019F84A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWINDCALLBACKMODE_T2F1DD72D77203C98698C6FDD4C6B5683E019F84A_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#define LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#define UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifndef ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#define ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2_Enumerator<System.String,System.Net.Http.Headers.HttpHeaders_HeaderBucket>
struct  Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___dictionary_0)); }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___current_3)); }
	inline KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#ifndef DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#define DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 
{
public:
	// System.DateTime System.DateTimeOffset::m_dateTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dateTime_2;
	// System.Int16 System.DateTimeOffset::m_offsetMinutes
	int16_t ___m_offsetMinutes_3;

public:
	inline static int32_t get_offset_of_m_dateTime_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85, ___m_dateTime_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dateTime_2() const { return ___m_dateTime_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dateTime_2() { return &___m_dateTime_2; }
	inline void set_m_dateTime_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dateTime_2 = value;
	}

	inline static int32_t get_offset_of_m_offsetMinutes_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85, ___m_offsetMinutes_3)); }
	inline int16_t get_m_offsetMinutes_3() const { return ___m_offsetMinutes_3; }
	inline int16_t* get_address_of_m_offsetMinutes_3() { return &___m_offsetMinutes_3; }
	inline void set_m_offsetMinutes_3(int16_t value)
	{
		___m_offsetMinutes_3 = value;
	}
};

struct DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___MinValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields, ___MinValue_0)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_MinValue_0() const { return ___MinValue_0; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields, ___MaxValue_1)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_MaxValue_1() const { return ___MaxValue_1; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#define CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ContentRangeHeaderValue
struct  ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ContentRangeHeaderValue::unit
	String_t* ___unit_0;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<From>k__BackingField
	Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  ___U3CFromU3Ek__BackingField_1;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<Length>k__BackingField
	Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  ___U3CLengthU3Ek__BackingField_2;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<To>k__BackingField
	Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  ___U3CToU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_unit_0() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___unit_0)); }
	inline String_t* get_unit_0() const { return ___unit_0; }
	inline String_t** get_address_of_unit_0() { return &___unit_0; }
	inline void set_unit_0(String_t* value)
	{
		___unit_0 = value;
		Il2CppCodeGenWriteBarrier((&___unit_0), value);
	}

	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CFromU3Ek__BackingField_1)); }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  get_U3CFromU3Ek__BackingField_1() const { return ___U3CFromU3Ek__BackingField_1; }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B * get_address_of_U3CFromU3Ek__BackingField_1() { return &___U3CFromU3Ek__BackingField_1; }
	inline void set_U3CFromU3Ek__BackingField_1(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  value)
	{
		___U3CFromU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CLengthU3Ek__BackingField_2)); }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  get_U3CLengthU3Ek__BackingField_2() const { return ___U3CLengthU3Ek__BackingField_2; }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B * get_address_of_U3CLengthU3Ek__BackingField_2() { return &___U3CLengthU3Ek__BackingField_2; }
	inline void set_U3CLengthU3Ek__BackingField_2(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  value)
	{
		___U3CLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CToU3Ek__BackingField_3)); }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  get_U3CToU3Ek__BackingField_3() const { return ___U3CToU3Ek__BackingField_3; }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B * get_address_of_U3CToU3Ek__BackingField_3() { return &___U3CToU3Ek__BackingField_3; }
	inline void set_U3CToU3Ek__BackingField_3(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  value)
	{
		___U3CToU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#ifndef HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#define HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaderKind
struct  HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97 
{
public:
	// System.Int32 System.Net.Http.Headers.HttpHeaderKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#ifndef RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#define RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeItemHeaderValue
struct  RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.RangeItemHeaderValue::<From>k__BackingField
	Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  ___U3CFromU3Ek__BackingField_0;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.RangeItemHeaderValue::<To>k__BackingField
	Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  ___U3CToU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664, ___U3CFromU3Ek__BackingField_0)); }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  get_U3CFromU3Ek__BackingField_0() const { return ___U3CFromU3Ek__BackingField_0; }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B * get_address_of_U3CFromU3Ek__BackingField_0() { return &___U3CFromU3Ek__BackingField_0; }
	inline void set_U3CFromU3Ek__BackingField_0(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  value)
	{
		___U3CFromU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664, ___U3CToU3Ek__BackingField_1)); }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  get_U3CToU3Ek__BackingField_1() const { return ___U3CToU3Ek__BackingField_1; }
	inline Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B * get_address_of_U3CToU3Ek__BackingField_1() { return &___U3CToU3Ek__BackingField_1; }
	inline void set_U3CToU3Ek__BackingField_1(Nullable_1_t802480A692F4F0D29F2185320296572054FB8C0B  value)
	{
		___U3CToU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#ifndef STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#define STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.StringWithQualityHeaderValue
struct  StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Double> System.Net.Http.Headers.StringWithQualityHeaderValue::<Quality>k__BackingField
	Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5  ___U3CQualityU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.StringWithQualityHeaderValue::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CQualityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8, ___U3CQualityU3Ek__BackingField_0)); }
	inline Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5  get_U3CQualityU3Ek__BackingField_0() const { return ___U3CQualityU3Ek__BackingField_0; }
	inline Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5 * get_address_of_U3CQualityU3Ek__BackingField_0() { return &___U3CQualityU3Ek__BackingField_0; }
	inline void set_U3CQualityU3Ek__BackingField_0(Nullable_1_tA635682CABFD60B7DF73271614FC04085E333AC5  value)
	{
		___U3CQualityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#ifndef TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#define TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Token_Type
struct  Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72 
{
public:
	// System.Int32 System.Net.Http.Headers.Token_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#define ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AdditionalCanvasShaderChannels
struct  AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E 
{
public:
	// System.Int32 UnityEngine.AdditionalCanvasShaderChannels::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#ifndef ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#define ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#define REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteConfigSettings
struct  RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___Updated_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_Updated_1() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___Updated_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_Updated_1() const { return ___Updated_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_Updated_1() { return &___Updated_1; }
	inline void set_Updated_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___Updated_1 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
#endif // REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifndef RENDERMODE_TB54632E74CDC4A990E815EB8C3CC515D3A9E2F60_H
#define RENDERMODE_TB54632E74CDC4A990E815EB8C3CC515D3A9E2F60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_TB54632E74CDC4A990E815EB8C3CC515D3A9E2F60_H
#ifndef FONTENGINEERROR_T64F8E9C372D8AC63E9BA857AC757719A321C357B_H
#define FONTENGINEERROR_T64F8E9C372D8AC63E9BA857AC757719A321C357B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.FontEngineError
struct  FontEngineError_t64F8E9C372D8AC63E9BA857AC757719A321C357B 
{
public:
	// System.Int32 UnityEngine.TextCore.LowLevel.FontEngineError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontEngineError_t64F8E9C372D8AC63E9BA857AC757719A321C357B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTENGINEERROR_T64F8E9C372D8AC63E9BA857AC757719A321C357B_H
#ifndef GLYPHADJUSTMENTRECORD_T771BE41EF0B790AF1E65928F401E3AB0EE548D00_H
#define GLYPHADJUSTMENTRECORD_T771BE41EF0B790AF1E65928F401E3AB0EE548D00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord
struct  GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00 
{
public:
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// UnityEngine.TextCore.LowLevel.GlyphValueRecord UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphValueRecord
	GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D  ___m_GlyphValueRecord_1;

public:
	inline static int32_t get_offset_of_m_GlyphIndex_0() { return static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00, ___m_GlyphIndex_0)); }
	inline uint32_t get_m_GlyphIndex_0() const { return ___m_GlyphIndex_0; }
	inline uint32_t* get_address_of_m_GlyphIndex_0() { return &___m_GlyphIndex_0; }
	inline void set_m_GlyphIndex_0(uint32_t value)
	{
		___m_GlyphIndex_0 = value;
	}

	inline static int32_t get_offset_of_m_GlyphValueRecord_1() { return static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00, ___m_GlyphValueRecord_1)); }
	inline GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D  get_m_GlyphValueRecord_1() const { return ___m_GlyphValueRecord_1; }
	inline GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D * get_address_of_m_GlyphValueRecord_1() { return &___m_GlyphValueRecord_1; }
	inline void set_m_GlyphValueRecord_1(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D  value)
	{
		___m_GlyphValueRecord_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHADJUSTMENTRECORD_T771BE41EF0B790AF1E65928F401E3AB0EE548D00_H
#ifndef GLYPHLOADFLAGS_T009F1B3663C1AC0CE62D615975C1F77A986C9F09_H
#define GLYPHLOADFLAGS_T009F1B3663C1AC0CE62D615975C1F77A986C9F09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphLoadFlags
struct  GlyphLoadFlags_t009F1B3663C1AC0CE62D615975C1F77A986C9F09 
{
public:
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphLoadFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GlyphLoadFlags_t009F1B3663C1AC0CE62D615975C1F77A986C9F09, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHLOADFLAGS_T009F1B3663C1AC0CE62D615975C1F77A986C9F09_H
#ifndef GLYPHMARSHALLINGSTRUCT_T4A13978D8A28D0D54B36F37557770DCD83219448_H
#define GLYPHMARSHALLINGSTRUCT_T4A13978D8A28D0D54B36F37557770DCD83219448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct
struct  GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448 
{
public:
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::index
	uint32_t ___index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::metrics
	GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  ___metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::glyphRect
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___glyphRect_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::scale
	float ___scale_3;
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::atlasIndex
	int32_t ___atlasIndex_4;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448, ___index_0)); }
	inline uint32_t get_index_0() const { return ___index_0; }
	inline uint32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(uint32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_metrics_1() { return static_cast<int32_t>(offsetof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448, ___metrics_1)); }
	inline GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  get_metrics_1() const { return ___metrics_1; }
	inline GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB * get_address_of_metrics_1() { return &___metrics_1; }
	inline void set_metrics_1(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  value)
	{
		___metrics_1 = value;
	}

	inline static int32_t get_offset_of_glyphRect_2() { return static_cast<int32_t>(offsetof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448, ___glyphRect_2)); }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  get_glyphRect_2() const { return ___glyphRect_2; }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C * get_address_of_glyphRect_2() { return &___glyphRect_2; }
	inline void set_glyphRect_2(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  value)
	{
		___glyphRect_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_atlasIndex_4() { return static_cast<int32_t>(offsetof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448, ___atlasIndex_4)); }
	inline int32_t get_atlasIndex_4() const { return ___atlasIndex_4; }
	inline int32_t* get_address_of_atlasIndex_4() { return &___atlasIndex_4; }
	inline void set_atlasIndex_4(int32_t value)
	{
		___atlasIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHMARSHALLINGSTRUCT_T4A13978D8A28D0D54B36F37557770DCD83219448_H
#ifndef GLYPHPACKINGMODE_T61CDE7969B3E630F7BF8D982B58F0F35B367D9AE_H
#define GLYPHPACKINGMODE_T61CDE7969B3E630F7BF8D982B58F0F35B367D9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphPackingMode
struct  GlyphPackingMode_t61CDE7969B3E630F7BF8D982B58F0F35B367D9AE 
{
public:
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphPackingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GlyphPackingMode_t61CDE7969B3E630F7BF8D982B58F0F35B367D9AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHPACKINGMODE_T61CDE7969B3E630F7BF8D982B58F0F35B367D9AE_H
#ifndef GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#define GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphRenderMode
struct  GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D 
{
public:
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#ifndef THREADPRIORITY_TCA32DC97B1FAF52087C84FF48A88EBFF9AB637B7_H
#define THREADPRIORITY_TCA32DC97B1FAF52087C84FF48A88EBFF9AB637B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadPriority
struct  ThreadPriority_tCA32DC97B1FAF52087C84FF48A88EBFF9AB637B7 
{
public:
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadPriority_tCA32DC97B1FAF52087C84FF48A88EBFF9AB637B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_TCA32DC97B1FAF52087C84FF48A88EBFF9AB637B7_H
#ifndef SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#define SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi_SampleType
struct  SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi_SampleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifndef DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#define DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween
struct  DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D  : public RuntimeObject
{
public:

public:
};

struct DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// DG.Tweening.Core.Enums.NestedTweenFailureBehaviour DG.Tweening.DOTween::nestedTweenFailureBehaviour
	int32_t ___nestedTweenFailureBehaviour_2;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_3;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_5;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_6;
	// DG.Tweening.Core.Enums.RewindCallbackMode DG.Tweening.DOTween::rewindCallbackMode
	int32_t ___rewindCallbackMode_7;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_8;
	// System.Func`3<UnityEngine.LogType,System.Object,System.Boolean> DG.Tweening.DOTween::onWillLog
	Func_3_t438CBBFB0B5FB2391C878B5C188BF587009211EB * ___onWillLog_9;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_10;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_11;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_12;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_13;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_14;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_15;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_16;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_17;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_18;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_19;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * ___instance_20;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_21;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_22;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * ___GizmosDelegates_23;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_24;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_25;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_nestedTweenFailureBehaviour_2() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___nestedTweenFailureBehaviour_2)); }
	inline int32_t get_nestedTweenFailureBehaviour_2() const { return ___nestedTweenFailureBehaviour_2; }
	inline int32_t* get_address_of_nestedTweenFailureBehaviour_2() { return &___nestedTweenFailureBehaviour_2; }
	inline void set_nestedTweenFailureBehaviour_2(int32_t value)
	{
		___nestedTweenFailureBehaviour_2 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_3() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___showUnityEditorReport_3)); }
	inline bool get_showUnityEditorReport_3() const { return ___showUnityEditorReport_3; }
	inline bool* get_address_of_showUnityEditorReport_3() { return &___showUnityEditorReport_3; }
	inline void set_showUnityEditorReport_3(bool value)
	{
		___showUnityEditorReport_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_5() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___useSmoothDeltaTime_5)); }
	inline bool get_useSmoothDeltaTime_5() const { return ___useSmoothDeltaTime_5; }
	inline bool* get_address_of_useSmoothDeltaTime_5() { return &___useSmoothDeltaTime_5; }
	inline void set_useSmoothDeltaTime_5(bool value)
	{
		___useSmoothDeltaTime_5 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_6() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxSmoothUnscaledTime_6)); }
	inline float get_maxSmoothUnscaledTime_6() const { return ___maxSmoothUnscaledTime_6; }
	inline float* get_address_of_maxSmoothUnscaledTime_6() { return &___maxSmoothUnscaledTime_6; }
	inline void set_maxSmoothUnscaledTime_6(float value)
	{
		___maxSmoothUnscaledTime_6 = value;
	}

	inline static int32_t get_offset_of_rewindCallbackMode_7() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___rewindCallbackMode_7)); }
	inline int32_t get_rewindCallbackMode_7() const { return ___rewindCallbackMode_7; }
	inline int32_t* get_address_of_rewindCallbackMode_7() { return &___rewindCallbackMode_7; }
	inline void set_rewindCallbackMode_7(int32_t value)
	{
		___rewindCallbackMode_7 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_8() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ____logBehaviour_8)); }
	inline int32_t get__logBehaviour_8() const { return ____logBehaviour_8; }
	inline int32_t* get_address_of__logBehaviour_8() { return &____logBehaviour_8; }
	inline void set__logBehaviour_8(int32_t value)
	{
		____logBehaviour_8 = value;
	}

	inline static int32_t get_offset_of_onWillLog_9() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___onWillLog_9)); }
	inline Func_3_t438CBBFB0B5FB2391C878B5C188BF587009211EB * get_onWillLog_9() const { return ___onWillLog_9; }
	inline Func_3_t438CBBFB0B5FB2391C878B5C188BF587009211EB ** get_address_of_onWillLog_9() { return &___onWillLog_9; }
	inline void set_onWillLog_9(Func_3_t438CBBFB0B5FB2391C878B5C188BF587009211EB * value)
	{
		___onWillLog_9 = value;
		Il2CppCodeGenWriteBarrier((&___onWillLog_9), value);
	}

	inline static int32_t get_offset_of_drawGizmos_10() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___drawGizmos_10)); }
	inline bool get_drawGizmos_10() const { return ___drawGizmos_10; }
	inline bool* get_address_of_drawGizmos_10() { return &___drawGizmos_10; }
	inline void set_drawGizmos_10(bool value)
	{
		___drawGizmos_10 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_11() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultUpdateType_11)); }
	inline int32_t get_defaultUpdateType_11() const { return ___defaultUpdateType_11; }
	inline int32_t* get_address_of_defaultUpdateType_11() { return &___defaultUpdateType_11; }
	inline void set_defaultUpdateType_11(int32_t value)
	{
		___defaultUpdateType_11 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_12() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultTimeScaleIndependent_12)); }
	inline bool get_defaultTimeScaleIndependent_12() const { return ___defaultTimeScaleIndependent_12; }
	inline bool* get_address_of_defaultTimeScaleIndependent_12() { return &___defaultTimeScaleIndependent_12; }
	inline void set_defaultTimeScaleIndependent_12(bool value)
	{
		___defaultTimeScaleIndependent_12 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_13() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultAutoPlay_13)); }
	inline int32_t get_defaultAutoPlay_13() const { return ___defaultAutoPlay_13; }
	inline int32_t* get_address_of_defaultAutoPlay_13() { return &___defaultAutoPlay_13; }
	inline void set_defaultAutoPlay_13(int32_t value)
	{
		___defaultAutoPlay_13 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_14() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultAutoKill_14)); }
	inline bool get_defaultAutoKill_14() const { return ___defaultAutoKill_14; }
	inline bool* get_address_of_defaultAutoKill_14() { return &___defaultAutoKill_14; }
	inline void set_defaultAutoKill_14(bool value)
	{
		___defaultAutoKill_14 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_15() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultLoopType_15)); }
	inline int32_t get_defaultLoopType_15() const { return ___defaultLoopType_15; }
	inline int32_t* get_address_of_defaultLoopType_15() { return &___defaultLoopType_15; }
	inline void set_defaultLoopType_15(int32_t value)
	{
		___defaultLoopType_15 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_16() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultRecyclable_16)); }
	inline bool get_defaultRecyclable_16() const { return ___defaultRecyclable_16; }
	inline bool* get_address_of_defaultRecyclable_16() { return &___defaultRecyclable_16; }
	inline void set_defaultRecyclable_16(bool value)
	{
		___defaultRecyclable_16 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_17() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEaseType_17)); }
	inline int32_t get_defaultEaseType_17() const { return ___defaultEaseType_17; }
	inline int32_t* get_address_of_defaultEaseType_17() { return &___defaultEaseType_17; }
	inline void set_defaultEaseType_17(int32_t value)
	{
		___defaultEaseType_17 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_18() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEaseOvershootOrAmplitude_18)); }
	inline float get_defaultEaseOvershootOrAmplitude_18() const { return ___defaultEaseOvershootOrAmplitude_18; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_18() { return &___defaultEaseOvershootOrAmplitude_18; }
	inline void set_defaultEaseOvershootOrAmplitude_18(float value)
	{
		___defaultEaseOvershootOrAmplitude_18 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_19() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEasePeriod_19)); }
	inline float get_defaultEasePeriod_19() const { return ___defaultEasePeriod_19; }
	inline float* get_address_of_defaultEasePeriod_19() { return &___defaultEasePeriod_19; }
	inline void set_defaultEasePeriod_19(float value)
	{
		___defaultEasePeriod_19 = value;
	}

	inline static int32_t get_offset_of_instance_20() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___instance_20)); }
	inline DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * get_instance_20() const { return ___instance_20; }
	inline DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F ** get_address_of_instance_20() { return &___instance_20; }
	inline void set_instance_20(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * value)
	{
		___instance_20 = value;
		Il2CppCodeGenWriteBarrier((&___instance_20), value);
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_21() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxActiveTweenersReached_21)); }
	inline int32_t get_maxActiveTweenersReached_21() const { return ___maxActiveTweenersReached_21; }
	inline int32_t* get_address_of_maxActiveTweenersReached_21() { return &___maxActiveTweenersReached_21; }
	inline void set_maxActiveTweenersReached_21(int32_t value)
	{
		___maxActiveTweenersReached_21 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_22() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxActiveSequencesReached_22)); }
	inline int32_t get_maxActiveSequencesReached_22() const { return ___maxActiveSequencesReached_22; }
	inline int32_t* get_address_of_maxActiveSequencesReached_22() { return &___maxActiveSequencesReached_22; }
	inline void set_maxActiveSequencesReached_22(int32_t value)
	{
		___maxActiveSequencesReached_22 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_23() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___GizmosDelegates_23)); }
	inline List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * get_GizmosDelegates_23() const { return ___GizmosDelegates_23; }
	inline List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 ** get_address_of_GizmosDelegates_23() { return &___GizmosDelegates_23; }
	inline void set_GizmosDelegates_23(List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * value)
	{
		___GizmosDelegates_23 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosDelegates_23), value);
	}

	inline static int32_t get_offset_of_initialized_24() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___initialized_24)); }
	inline bool get_initialized_24() const { return ___initialized_24; }
	inline bool* get_address_of_initialized_24() { return &___initialized_24; }
	inline void set_initialized_24(bool value)
	{
		___initialized_24 = value;
	}

	inline static int32_t get_offset_of_isQuitting_25() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___isQuitting_25)); }
	inline bool get_isQuitting_25() const { return ___isQuitting_25; }
	inline bool* get_address_of_isQuitting_25() { return &___isQuitting_25; }
	inline void set_isQuitting_25(bool value)
	{
		___isQuitting_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#define HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HeaderInfo
struct  HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Http.Headers.HeaderInfo::AllowsMany
	bool ___AllowsMany_0;
	// System.Net.Http.Headers.HttpHeaderKind System.Net.Http.Headers.HeaderInfo::HeaderKind
	int32_t ___HeaderKind_1;
	// System.String System.Net.Http.Headers.HeaderInfo::Name
	String_t* ___Name_2;
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.HeaderInfo::<CustomToString>k__BackingField
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CCustomToStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_AllowsMany_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___AllowsMany_0)); }
	inline bool get_AllowsMany_0() const { return ___AllowsMany_0; }
	inline bool* get_address_of_AllowsMany_0() { return &___AllowsMany_0; }
	inline void set_AllowsMany_0(bool value)
	{
		___AllowsMany_0 = value;
	}

	inline static int32_t get_offset_of_HeaderKind_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___HeaderKind_1)); }
	inline int32_t get_HeaderKind_1() const { return ___HeaderKind_1; }
	inline int32_t* get_address_of_HeaderKind_1() { return &___HeaderKind_1; }
	inline void set_HeaderKind_1(int32_t value)
	{
		___HeaderKind_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomToStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___U3CCustomToStringU3Ek__BackingField_3)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CCustomToStringU3Ek__BackingField_3() const { return ___U3CCustomToStringU3Ek__BackingField_3; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CCustomToStringU3Ek__BackingField_3() { return &___U3CCustomToStringU3Ek__BackingField_3; }
	inline void set_U3CCustomToStringU3Ek__BackingField_3(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CCustomToStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomToStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#ifndef HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#define HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders
struct  HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HttpHeaders_HeaderBucket> System.Net.Http.Headers.HttpHeaders::headers
	Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * ___headers_1;
	// System.Net.Http.Headers.HttpHeaderKind System.Net.Http.Headers.HttpHeaders::HeaderKind
	int32_t ___HeaderKind_2;
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpHeaders::connectionclose
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___connectionclose_3;
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpHeaders::transferEncodingChunked
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___transferEncodingChunked_4;

public:
	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___headers_1)); }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_HeaderKind_2() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___HeaderKind_2)); }
	inline int32_t get_HeaderKind_2() const { return ___HeaderKind_2; }
	inline int32_t* get_address_of_HeaderKind_2() { return &___HeaderKind_2; }
	inline void set_HeaderKind_2(int32_t value)
	{
		___HeaderKind_2 = value;
	}

	inline static int32_t get_offset_of_connectionclose_3() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___connectionclose_3)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_connectionclose_3() const { return ___connectionclose_3; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_connectionclose_3() { return &___connectionclose_3; }
	inline void set_connectionclose_3(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___connectionclose_3 = value;
	}

	inline static int32_t get_offset_of_transferEncodingChunked_4() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___transferEncodingChunked_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_transferEncodingChunked_4() const { return ___transferEncodingChunked_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_transferEncodingChunked_4() { return &___transferEncodingChunked_4; }
	inline void set_transferEncodingChunked_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___transferEncodingChunked_4 = value;
	}
};

struct HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HeaderInfo> System.Net.Http.Headers.HttpHeaders::known_headers
	Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * ___known_headers_0;

public:
	inline static int32_t get_offset_of_known_headers_0() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields, ___known_headers_0)); }
	inline Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * get_known_headers_0() const { return ___known_headers_0; }
	inline Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 ** get_address_of_known_headers_0() { return &___known_headers_0; }
	inline void set_known_headers_0(Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * value)
	{
		___known_headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___known_headers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#ifndef U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#define U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders_<GetEnumerator>d__19
struct  U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A  : public RuntimeObject
{
public:
	// System.Int32 System.Net.Http.Headers.HttpHeaders_<GetEnumerator>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.IEnumerable`1<System.String>> System.Net.Http.Headers.HttpHeaders_<GetEnumerator>d__19::<>2__current
	KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  ___U3CU3E2__current_1;
	// System.Net.Http.Headers.HttpHeaders System.Net.Http.Headers.HttpHeaders_<GetEnumerator>d__19::<>4__this
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * ___U3CU3E4__this_2;
	// System.Collections.Generic.Dictionary`2_Enumerator<System.String,System.Net.Http.Headers.HttpHeaders_HeaderBucket> System.Net.Http.Headers.HttpHeaders_<GetEnumerator>d__19::<>7__wrap1
	Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E4__this_2)); }
	inline HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#ifndef TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#define TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Token
struct  Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 
{
public:
	// System.Net.Http.Headers.Token_Type System.Net.Http.Headers.Token::type
	int32_t ___type_1;
	// System.Int32 System.Net.Http.Headers.Token::<StartPosition>k__BackingField
	int32_t ___U3CStartPositionU3Ek__BackingField_2;
	// System.Int32 System.Net.Http.Headers.Token::<EndPosition>k__BackingField
	int32_t ___U3CEndPositionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___U3CStartPositionU3Ek__BackingField_2)); }
	inline int32_t get_U3CStartPositionU3Ek__BackingField_2() const { return ___U3CStartPositionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStartPositionU3Ek__BackingField_2() { return &___U3CStartPositionU3Ek__BackingField_2; }
	inline void set_U3CStartPositionU3Ek__BackingField_2(int32_t value)
	{
		___U3CStartPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CEndPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___U3CEndPositionU3Ek__BackingField_3)); }
	inline int32_t get_U3CEndPositionU3Ek__BackingField_3() const { return ___U3CEndPositionU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CEndPositionU3Ek__BackingField_3() { return &___U3CEndPositionU3Ek__BackingField_3; }
	inline void set_U3CEndPositionU3Ek__BackingField_3(int32_t value)
	{
		___U3CEndPositionU3Ek__BackingField_3 = value;
	}
};

struct Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields
{
public:
	// System.Net.Http.Headers.Token System.Net.Http.Headers.Token::Empty
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields, ___Empty_0)); }
	inline Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  get_Empty_0() const { return ___Empty_0; }
	inline Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  value)
	{
		___Empty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#ifndef HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#define HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClient
struct  HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7  : public HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72
{
public:
	// System.Uri System.Net.Http.HttpClient::base_address
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___base_address_3;
	// System.Threading.CancellationTokenSource System.Net.Http.HttpClient::cts
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___cts_4;
	// System.Boolean System.Net.Http.HttpClient::disposed
	bool ___disposed_5;
	// System.Net.Http.Headers.HttpRequestHeaders System.Net.Http.HttpClient::headers
	HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * ___headers_6;
	// System.Int64 System.Net.Http.HttpClient::buffer_size
	int64_t ___buffer_size_7;
	// System.TimeSpan System.Net.Http.HttpClient::timeout
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___timeout_8;

public:
	inline static int32_t get_offset_of_base_address_3() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___base_address_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_base_address_3() const { return ___base_address_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_base_address_3() { return &___base_address_3; }
	inline void set_base_address_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___base_address_3 = value;
		Il2CppCodeGenWriteBarrier((&___base_address_3), value);
	}

	inline static int32_t get_offset_of_cts_4() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___cts_4)); }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * get_cts_4() const { return ___cts_4; }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE ** get_address_of_cts_4() { return &___cts_4; }
	inline void set_cts_4(CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * value)
	{
		___cts_4 = value;
		Il2CppCodeGenWriteBarrier((&___cts_4), value);
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}

	inline static int32_t get_offset_of_headers_6() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___headers_6)); }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * get_headers_6() const { return ___headers_6; }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 ** get_address_of_headers_6() { return &___headers_6; }
	inline void set_headers_6(HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * value)
	{
		___headers_6 = value;
		Il2CppCodeGenWriteBarrier((&___headers_6), value);
	}

	inline static int32_t get_offset_of_buffer_size_7() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___buffer_size_7)); }
	inline int64_t get_buffer_size_7() const { return ___buffer_size_7; }
	inline int64_t* get_address_of_buffer_size_7() { return &___buffer_size_7; }
	inline void set_buffer_size_7(int64_t value)
	{
		___buffer_size_7 = value;
	}

	inline static int32_t get_offset_of_timeout_8() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___timeout_8)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_timeout_8() const { return ___timeout_8; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_timeout_8() { return &___timeout_8; }
	inline void set_timeout_8(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___timeout_8 = value;
	}
};

struct HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields
{
public:
	// System.TimeSpan System.Net.Http.HttpClient::TimeoutDefault
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___TimeoutDefault_2;

public:
	inline static int32_t get_offset_of_TimeoutDefault_2() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields, ___TimeoutDefault_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_TimeoutDefault_2() const { return ___TimeoutDefault_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_TimeoutDefault_2() { return &___TimeoutDefault_2; }
	inline void set_TimeoutDefault_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___TimeoutDefault_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#ifndef NULLABLE_1_TEAC92A3CD08AD5B689D47A057A14A8842B012102_H
#define NULLABLE_1_TEAC92A3CD08AD5B689D47A057A14A8842B012102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTimeOffset>
struct  Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102 
{
public:
	// T System.Nullable`1::value
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102, ___value_0)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_value_0() const { return ___value_0; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TEAC92A3CD08AD5B689D47A057A14A8842B012102_H
#ifndef NULLABLE_1_TAE17873A01A7B115BFE9B673C3101F3F5569359F_H
#define NULLABLE_1_TAE17873A01A7B115BFE9B673C3101F3F5569359F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F 
{
public:
	// T System.Nullable`1::value
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F, ___value_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_value_0() const { return ___value_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TAE17873A01A7B115BFE9B673C3101F3F5569359F_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GLYPHPAIRADJUSTMENTRECORD_T4D86058777EDA2219FB8211B4C63EDD2B090239C_H
#define GLYPHPAIRADJUSTMENTRECORD_T4D86058777EDA2219FB8211B4C63EDD2B090239C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord
struct  GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C 
{
public:
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  ___m_FirstAdjustmentRecord_0;
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  ___m_SecondAdjustmentRecord_1;

public:
	inline static int32_t get_offset_of_m_FirstAdjustmentRecord_0() { return static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C, ___m_FirstAdjustmentRecord_0)); }
	inline GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  get_m_FirstAdjustmentRecord_0() const { return ___m_FirstAdjustmentRecord_0; }
	inline GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00 * get_address_of_m_FirstAdjustmentRecord_0() { return &___m_FirstAdjustmentRecord_0; }
	inline void set_m_FirstAdjustmentRecord_0(GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  value)
	{
		___m_FirstAdjustmentRecord_0 = value;
	}

	inline static int32_t get_offset_of_m_SecondAdjustmentRecord_1() { return static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C, ___m_SecondAdjustmentRecord_1)); }
	inline GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  get_m_SecondAdjustmentRecord_1() const { return ___m_SecondAdjustmentRecord_1; }
	inline GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00 * get_address_of_m_SecondAdjustmentRecord_1() { return &___m_SecondAdjustmentRecord_1; }
	inline void set_m_SecondAdjustmentRecord_1(GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00  value)
	{
		___m_SecondAdjustmentRecord_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHPAIRADJUSTMENTRECORD_T4D86058777EDA2219FB8211B4C63EDD2B090239C_H
#ifndef WWW_TA50AFB5DE276783409B4CE88FE9B772322EE5664_H
#define WWW_TA50AFB5DE276783409B4CE88FE9B772322EE5664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// UnityEngine.ThreadPriority UnityEngine.WWW::<threadPriority>k__BackingField
	int32_t ___U3CthreadPriorityU3Ek__BackingField_0;
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ____uwr_1;
	// UnityEngine.AssetBundle UnityEngine.WWW::_assetBundle
	AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * ____assetBundle_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::_responseHeaders
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____responseHeaders_3;

public:
	inline static int32_t get_offset_of_U3CthreadPriorityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664, ___U3CthreadPriorityU3Ek__BackingField_0)); }
	inline int32_t get_U3CthreadPriorityU3Ek__BackingField_0() const { return ___U3CthreadPriorityU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CthreadPriorityU3Ek__BackingField_0() { return &___U3CthreadPriorityU3Ek__BackingField_0; }
	inline void set_U3CthreadPriorityU3Ek__BackingField_0(int32_t value)
	{
		___U3CthreadPriorityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__uwr_1() { return static_cast<int32_t>(offsetof(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664, ____uwr_1)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get__uwr_1() const { return ____uwr_1; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of__uwr_1() { return &____uwr_1; }
	inline void set__uwr_1(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		____uwr_1 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_1), value);
	}

	inline static int32_t get_offset_of__assetBundle_2() { return static_cast<int32_t>(offsetof(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664, ____assetBundle_2)); }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * get__assetBundle_2() const { return ____assetBundle_2; }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 ** get_address_of__assetBundle_2() { return &____assetBundle_2; }
	inline void set__assetBundle_2(AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * value)
	{
		____assetBundle_2 = value;
		Il2CppCodeGenWriteBarrier((&____assetBundle_2), value);
	}

	inline static int32_t get_offset_of__responseHeaders_3() { return static_cast<int32_t>(offsetof(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664, ____responseHeaders_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__responseHeaders_3() const { return ____responseHeaders_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__responseHeaders_3() { return &____responseHeaders_3; }
	inline void set__responseHeaders_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____responseHeaders_3 = value;
		Il2CppCodeGenWriteBarrier((&____responseHeaders_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_TA50AFB5DE276783409B4CE88FE9B772322EE5664_H
#ifndef EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#define EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFunction
struct  EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#ifndef TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#define TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenCallback
struct  TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#ifndef CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#define CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CacheControlHeaderValue
struct  CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.CacheControlHeaderValue::extensions
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___extensions_0;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.CacheControlHeaderValue::no_cache_headers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___no_cache_headers_1;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.CacheControlHeaderValue::private_headers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___private_headers_2;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MaxAge>k__BackingField
	Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  ___U3CMaxAgeU3Ek__BackingField_3;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<MaxStale>k__BackingField
	bool ___U3CMaxStaleU3Ek__BackingField_4;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MaxStaleLimit>k__BackingField
	Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  ___U3CMaxStaleLimitU3Ek__BackingField_5;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MinFresh>k__BackingField
	Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  ___U3CMinFreshU3Ek__BackingField_6;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<MustRevalidate>k__BackingField
	bool ___U3CMustRevalidateU3Ek__BackingField_7;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoCache>k__BackingField
	bool ___U3CNoCacheU3Ek__BackingField_8;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoStore>k__BackingField
	bool ___U3CNoStoreU3Ek__BackingField_9;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoTransform>k__BackingField
	bool ___U3CNoTransformU3Ek__BackingField_10;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<OnlyIfCached>k__BackingField
	bool ___U3COnlyIfCachedU3Ek__BackingField_11;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<Private>k__BackingField
	bool ___U3CPrivateU3Ek__BackingField_12;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<ProxyRevalidate>k__BackingField
	bool ___U3CProxyRevalidateU3Ek__BackingField_13;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<Public>k__BackingField
	bool ___U3CPublicU3Ek__BackingField_14;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<SharedMaxAge>k__BackingField
	Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  ___U3CSharedMaxAgeU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_extensions_0() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___extensions_0)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_extensions_0() const { return ___extensions_0; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_extensions_0() { return &___extensions_0; }
	inline void set_extensions_0(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_0), value);
	}

	inline static int32_t get_offset_of_no_cache_headers_1() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___no_cache_headers_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_no_cache_headers_1() const { return ___no_cache_headers_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_no_cache_headers_1() { return &___no_cache_headers_1; }
	inline void set_no_cache_headers_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___no_cache_headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___no_cache_headers_1), value);
	}

	inline static int32_t get_offset_of_private_headers_2() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___private_headers_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_private_headers_2() const { return ___private_headers_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_private_headers_2() { return &___private_headers_2; }
	inline void set_private_headers_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___private_headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___private_headers_2), value);
	}

	inline static int32_t get_offset_of_U3CMaxAgeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxAgeU3Ek__BackingField_3)); }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  get_U3CMaxAgeU3Ek__BackingField_3() const { return ___U3CMaxAgeU3Ek__BackingField_3; }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F * get_address_of_U3CMaxAgeU3Ek__BackingField_3() { return &___U3CMaxAgeU3Ek__BackingField_3; }
	inline void set_U3CMaxAgeU3Ek__BackingField_3(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  value)
	{
		___U3CMaxAgeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CMaxStaleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxStaleU3Ek__BackingField_4)); }
	inline bool get_U3CMaxStaleU3Ek__BackingField_4() const { return ___U3CMaxStaleU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CMaxStaleU3Ek__BackingField_4() { return &___U3CMaxStaleU3Ek__BackingField_4; }
	inline void set_U3CMaxStaleU3Ek__BackingField_4(bool value)
	{
		___U3CMaxStaleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMaxStaleLimitU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxStaleLimitU3Ek__BackingField_5)); }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  get_U3CMaxStaleLimitU3Ek__BackingField_5() const { return ___U3CMaxStaleLimitU3Ek__BackingField_5; }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F * get_address_of_U3CMaxStaleLimitU3Ek__BackingField_5() { return &___U3CMaxStaleLimitU3Ek__BackingField_5; }
	inline void set_U3CMaxStaleLimitU3Ek__BackingField_5(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  value)
	{
		___U3CMaxStaleLimitU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMinFreshU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMinFreshU3Ek__BackingField_6)); }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  get_U3CMinFreshU3Ek__BackingField_6() const { return ___U3CMinFreshU3Ek__BackingField_6; }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F * get_address_of_U3CMinFreshU3Ek__BackingField_6() { return &___U3CMinFreshU3Ek__BackingField_6; }
	inline void set_U3CMinFreshU3Ek__BackingField_6(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  value)
	{
		___U3CMinFreshU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMustRevalidateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMustRevalidateU3Ek__BackingField_7)); }
	inline bool get_U3CMustRevalidateU3Ek__BackingField_7() const { return ___U3CMustRevalidateU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CMustRevalidateU3Ek__BackingField_7() { return &___U3CMustRevalidateU3Ek__BackingField_7; }
	inline void set_U3CMustRevalidateU3Ek__BackingField_7(bool value)
	{
		___U3CMustRevalidateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CNoCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoCacheU3Ek__BackingField_8)); }
	inline bool get_U3CNoCacheU3Ek__BackingField_8() const { return ___U3CNoCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CNoCacheU3Ek__BackingField_8() { return &___U3CNoCacheU3Ek__BackingField_8; }
	inline void set_U3CNoCacheU3Ek__BackingField_8(bool value)
	{
		___U3CNoCacheU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CNoStoreU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoStoreU3Ek__BackingField_9)); }
	inline bool get_U3CNoStoreU3Ek__BackingField_9() const { return ___U3CNoStoreU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CNoStoreU3Ek__BackingField_9() { return &___U3CNoStoreU3Ek__BackingField_9; }
	inline void set_U3CNoStoreU3Ek__BackingField_9(bool value)
	{
		___U3CNoStoreU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CNoTransformU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoTransformU3Ek__BackingField_10)); }
	inline bool get_U3CNoTransformU3Ek__BackingField_10() const { return ___U3CNoTransformU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CNoTransformU3Ek__BackingField_10() { return &___U3CNoTransformU3Ek__BackingField_10; }
	inline void set_U3CNoTransformU3Ek__BackingField_10(bool value)
	{
		___U3CNoTransformU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3COnlyIfCachedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3COnlyIfCachedU3Ek__BackingField_11)); }
	inline bool get_U3COnlyIfCachedU3Ek__BackingField_11() const { return ___U3COnlyIfCachedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3COnlyIfCachedU3Ek__BackingField_11() { return &___U3COnlyIfCachedU3Ek__BackingField_11; }
	inline void set_U3COnlyIfCachedU3Ek__BackingField_11(bool value)
	{
		___U3COnlyIfCachedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CPrivateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CPrivateU3Ek__BackingField_12)); }
	inline bool get_U3CPrivateU3Ek__BackingField_12() const { return ___U3CPrivateU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CPrivateU3Ek__BackingField_12() { return &___U3CPrivateU3Ek__BackingField_12; }
	inline void set_U3CPrivateU3Ek__BackingField_12(bool value)
	{
		___U3CPrivateU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CProxyRevalidateU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CProxyRevalidateU3Ek__BackingField_13)); }
	inline bool get_U3CProxyRevalidateU3Ek__BackingField_13() const { return ___U3CProxyRevalidateU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CProxyRevalidateU3Ek__BackingField_13() { return &___U3CProxyRevalidateU3Ek__BackingField_13; }
	inline void set_U3CProxyRevalidateU3Ek__BackingField_13(bool value)
	{
		___U3CProxyRevalidateU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CPublicU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CPublicU3Ek__BackingField_14)); }
	inline bool get_U3CPublicU3Ek__BackingField_14() const { return ___U3CPublicU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CPublicU3Ek__BackingField_14() { return &___U3CPublicU3Ek__BackingField_14; }
	inline void set_U3CPublicU3Ek__BackingField_14(bool value)
	{
		___U3CPublicU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CSharedMaxAgeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CSharedMaxAgeU3Ek__BackingField_15)); }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  get_U3CSharedMaxAgeU3Ek__BackingField_15() const { return ___U3CSharedMaxAgeU3Ek__BackingField_15; }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F * get_address_of_U3CSharedMaxAgeU3Ek__BackingField_15() { return &___U3CSharedMaxAgeU3Ek__BackingField_15; }
	inline void set_U3CSharedMaxAgeU3Ek__BackingField_15(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  value)
	{
		___U3CSharedMaxAgeU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#ifndef HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#define HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpRequestHeaders
struct  HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4  : public HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1
{
public:
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpRequestHeaders::expectContinue
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___expectContinue_5;

public:
	inline static int32_t get_offset_of_expectContinue_5() { return static_cast<int32_t>(offsetof(HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4, ___expectContinue_5)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_expectContinue_5() const { return ___expectContinue_5; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_expectContinue_5() { return &___expectContinue_5; }
	inline void set_expectContinue_5(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___expectContinue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#ifndef RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#define RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeConditionHeaderValue
struct  RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92  : public RuntimeObject
{
public:
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.RangeConditionHeaderValue::<Date>k__BackingField
	Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  ___U3CDateU3Ek__BackingField_0;
	// System.Net.Http.Headers.EntityTagHeaderValue System.Net.Http.Headers.RangeConditionHeaderValue::<EntityTag>k__BackingField
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * ___U3CEntityTagU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92, ___U3CDateU3Ek__BackingField_0)); }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  get_U3CDateU3Ek__BackingField_0() const { return ___U3CDateU3Ek__BackingField_0; }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102 * get_address_of_U3CDateU3Ek__BackingField_0() { return &___U3CDateU3Ek__BackingField_0; }
	inline void set_U3CDateU3Ek__BackingField_0(Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  value)
	{
		___U3CDateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEntityTagU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92, ___U3CEntityTagU3Ek__BackingField_1)); }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * get_U3CEntityTagU3Ek__BackingField_1() const { return ___U3CEntityTagU3Ek__BackingField_1; }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE ** get_address_of_U3CEntityTagU3Ek__BackingField_1() { return &___U3CEntityTagU3Ek__BackingField_1; }
	inline void set_U3CEntityTagU3Ek__BackingField_1(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * value)
	{
		___U3CEntityTagU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntityTagU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#ifndef RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#define RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RetryConditionHeaderValue
struct  RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6  : public RuntimeObject
{
public:
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.RetryConditionHeaderValue::<Date>k__BackingField
	Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  ___U3CDateU3Ek__BackingField_0;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.RetryConditionHeaderValue::<Delta>k__BackingField
	Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  ___U3CDeltaU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6, ___U3CDateU3Ek__BackingField_0)); }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  get_U3CDateU3Ek__BackingField_0() const { return ___U3CDateU3Ek__BackingField_0; }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102 * get_address_of_U3CDateU3Ek__BackingField_0() { return &___U3CDateU3Ek__BackingField_0; }
	inline void set_U3CDateU3Ek__BackingField_0(Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  value)
	{
		___U3CDateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6, ___U3CDeltaU3Ek__BackingField_1)); }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  get_U3CDeltaU3Ek__BackingField_1() const { return ___U3CDeltaU3Ek__BackingField_1; }
	inline Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F * get_address_of_U3CDeltaU3Ek__BackingField_1() { return &___U3CDeltaU3Ek__BackingField_1; }
	inline void set_U3CDeltaU3Ek__BackingField_1(Nullable_1_tAE17873A01A7B115BFE9B673C3101F3F5569359F  value)
	{
		___U3CDeltaU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#ifndef WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#define WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.WarningHeaderValue
struct  WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.WarningHeaderValue::<Agent>k__BackingField
	String_t* ___U3CAgentU3Ek__BackingField_0;
	// System.Int32 System.Net.Http.Headers.WarningHeaderValue::<Code>k__BackingField
	int32_t ___U3CCodeU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.WarningHeaderValue::<Date>k__BackingField
	Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  ___U3CDateU3Ek__BackingField_2;
	// System.String System.Net.Http.Headers.WarningHeaderValue::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAgentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CAgentU3Ek__BackingField_0)); }
	inline String_t* get_U3CAgentU3Ek__BackingField_0() const { return ___U3CAgentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAgentU3Ek__BackingField_0() { return &___U3CAgentU3Ek__BackingField_0; }
	inline void set_U3CAgentU3Ek__BackingField_0(String_t* value)
	{
		___U3CAgentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAgentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCodeU3Ek__BackingField_1() const { return ___U3CCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCodeU3Ek__BackingField_1() { return &___U3CCodeU3Ek__BackingField_1; }
	inline void set_U3CCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CDateU3Ek__BackingField_2)); }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  get_U3CDateU3Ek__BackingField_2() const { return ___U3CDateU3Ek__BackingField_2; }
	inline Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102 * get_address_of_U3CDateU3Ek__BackingField_2() { return &___U3CDateU3Ek__BackingField_2; }
	inline void set_U3CDateU3Ek__BackingField_2(Nullable_1_tEAC92A3CD08AD5B689D47A057A14A8842B012102  value)
	{
		___U3CDateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CTextU3Ek__BackingField_3)); }
	inline String_t* get_U3CTextU3Ek__BackingField_3() const { return ___U3CTextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_3() { return &___U3CTextU3Ek__BackingField_3; }
	inline void set_U3CTextU3Ek__BackingField_3(String_t* value)
	{
		___U3CTextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#ifndef SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#define SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo_SessionStateChanged
struct  SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#define WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas_WillRenderCanvases
struct  WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifndef CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#define CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisMaskU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72, ___U3CisMaskU3Ek__BackingField_4)); }
	inline bool get_U3CisMaskU3Ek__BackingField_4() const { return ___U3CisMaskU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisMaskU3Ek__BackingField_4() { return &___U3CisMaskU3Ek__BackingField_4; }
	inline void set_U3CisMaskU3Ek__BackingField_4(bool value)
	{
		___U3CisMaskU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifndef UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#define UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings_UpdatedEventHandler
struct  UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifndef CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#define CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifndef CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#define CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D ), 0, 0 };
extern const int32_t g_FieldOffsetTable2800[4] = 
{
	GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D::get_offset_of_m_XPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D::get_offset_of_m_YPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D::get_offset_of_m_XAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_tC8C22AFC124DD2B4F0E9383A9C14AA8661359A5D::get_offset_of_m_YAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00)+ sizeof (RuntimeObject), sizeof(GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[2] = 
{
	GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00::get_offset_of_m_GlyphIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphAdjustmentRecord_t771BE41EF0B790AF1E65928F401E3AB0EE548D00::get_offset_of_m_GlyphValueRecord_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C)+ sizeof (RuntimeObject), sizeof(GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C ), 0, 0 };
extern const int32_t g_FieldOffsetTable2802[2] = 
{
	GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C::get_offset_of_m_FirstAdjustmentRecord_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphPairAdjustmentRecord_t4D86058777EDA2219FB8211B4C63EDD2B090239C::get_offset_of_m_SecondAdjustmentRecord_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (GlyphLoadFlags_t009F1B3663C1AC0CE62D615975C1F77A986C9F09)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2803[11] = 
{
	GlyphLoadFlags_t009F1B3663C1AC0CE62D615975C1F77A986C9F09::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (FontEngineError_t64F8E9C372D8AC63E9BA857AC757719A321C357B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2804[15] = 
{
	FontEngineError_t64F8E9C372D8AC63E9BA857AC757719A321C357B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2805[11] = 
{
	GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (GlyphPackingMode_t61CDE7969B3E630F7BF8D982B58F0F35B367D9AE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2806[6] = 
{
	GlyphPackingMode_t61CDE7969B3E630F7BF8D982B58F0F35B367D9AE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC), -1, sizeof(FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[8] = 
{
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_Instance_0(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_GlyphIndexesToMarshall_1(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_GlyphMarshallingStruct_IN_2(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_GlyphMarshallingStruct_OUT_3(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_FreeGlyphRects_4(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_UsedGlyphRects_5(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_GlyphPairAdjustmentRecords_6(),
	FontEngine_t9316586F7FD894C97716C9C48AF485A137284FBC_StaticFields::get_offset_of_s_GlyphLookupDictionary_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448)+ sizeof (RuntimeObject), sizeof(GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2808[5] = 
{
	GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448::get_offset_of_metrics_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448::get_offset_of_glyphRect_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphMarshallingStruct_t4A13978D8A28D0D54B36F37557770DCD83219448::get_offset_of_atlasIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (U3CModuleU3E_t79D7DE725655CFC1B063EA359E8D75692CF5DC2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2810[4] = 
{
	RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2811[7] = 
{
	AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591), -1, sizeof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2812[1] = 
{
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields::get_offset_of_willRenderCanvases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2815[3] = 
{
	SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[1] = 
{
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72::get_offset_of_U3CisMaskU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA), -1, sizeof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2819[1] = 
{
	RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2822[5] = 
{
	AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C), -1, sizeof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2823[1] = 
{
	AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED), -1, sizeof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2825[3] = 
{
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Updated_0(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_BeforeFetchFromServer_1(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A), sizeof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2827[2] = 
{
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_m_Ptr_0(),
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_Updated_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (U3CModuleU3E_t6814C120E23E8C14860FFC42894ADFDD3105B3E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[4] = 
{
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664::get_offset_of_U3CthreadPriorityU3Ek__BackingField_0(),
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664::get_offset_of__uwr_1(),
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664::get_offset_of__assetBundle_2(),
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664::get_offset_of__responseHeaders_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (U3CModuleU3E_tDE5A299227351E064CF5069210AC8ED1294BD51A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (U3CModuleU3E_t410187D184BFEA098C57AA90C1EEBB14DCD72176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (U3CModuleU3E_t56CA3936A9EFABF2ED20401359C40BFE63F85A11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (U3CModuleU3E_t62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7), -1, sizeof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2834[7] = 
{
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields::get_offset_of_TimeoutDefault_2(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_base_address_3(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_cts_4(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_disposed_5(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_headers_6(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_buffer_size_7(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_timeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049), -1, sizeof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2835[8] = 
{
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields::get_offset_of_groupCounter_0(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_allowAutoRedirect_1(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_maxAutomaticRedirections_2(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_maxRequestContentBufferSize_3(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_useCookies_4(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_useProxy_5(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_connectionGroupName_6(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[2] = 
{
	HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72::get_offset_of_handler_0(),
	HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72::get_offset_of_disposeHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[2] = 
{
	AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6::get_offset_of_U3CParameterU3Ek__BackingField_0(),
	AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6::get_offset_of_U3CSchemeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[16] = 
{
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_extensions_0(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_no_cache_headers_1(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_private_headers_2(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxAgeU3Ek__BackingField_3(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxStaleU3Ek__BackingField_4(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxStaleLimitU3Ek__BackingField_5(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMinFreshU3Ek__BackingField_6(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMustRevalidateU3Ek__BackingField_7(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoCacheU3Ek__BackingField_8(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoStoreU3Ek__BackingField_9(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoTransformU3Ek__BackingField_10(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3COnlyIfCachedU3Ek__BackingField_11(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CPrivateU3Ek__BackingField_12(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CProxyRevalidateU3Ek__BackingField_13(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CPublicU3Ek__BackingField_14(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CSharedMaxAgeU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (CollectionExtensions_tF55A4677D40CF6208C37CAB46E039D5DF6DC790B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (CollectionParser_t1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[2] = 
{
	ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0::get_offset_of_dispositionType_0(),
	ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[4] = 
{
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_unit_0(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CFromU3Ek__BackingField_1(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CLengthU3Ek__BackingField_2(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CToU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE), -1, sizeof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2845[3] = 
{
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields::get_offset_of_any_0(),
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE::get_offset_of_U3CIsWeakU3Ek__BackingField_1(),
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE::get_offset_of_U3CTagU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (HashCodeCalculator_t0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[4] = 
{
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_AllowsMany_0(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_HeaderKind_1(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_Name_2(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_U3CCustomToStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2852[5] = 
{
	HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1), -1, sizeof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2854[5] = 
{
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields::get_offset_of_known_headers_0(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_headers_1(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_HeaderKind_2(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_connectionclose_3(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_transferEncodingChunked_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[3] = 
{
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_Parsed_0(),
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_values_1(),
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_CustomToString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[4] = 
{
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4::get_offset_of_expectContinue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846)+ sizeof (RuntimeObject), sizeof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 ), sizeof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[4] = 
{
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields::get_offset_of_Empty_0(),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_U3CStartPositionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_U3CEndPositionU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[11] = 
{
	Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3), -1, sizeof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2860[5] = 
{
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_token_chars_0(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_last_token_char_1(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_dt_formats_2(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3::get_offset_of_s_3(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3::get_offset_of_pos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[2] = 
{
	MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1::get_offset_of_parameters_0(),
	MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1::get_offset_of_media_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (MediaTypeWithQualityHeaderValue_tA90D440F6C0876845E895A0A3C61FA36CA2B120E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[2] = 
{
	NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F::get_offset_of_value_0(),
	NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[1] = 
{
	NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (Parser_tC9CE5F5FFDD4927D039C3189B5945DA5EF958025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F), -1, sizeof(DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2866[1] = 
{
	DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields::get_offset_of_ToString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE), -1, sizeof(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (EmailAddress_t34037A92039853D504922D84E1512E6BE4FBDE1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (Host_t225394EBFA7427D924DC794F6FEB33C7B19E6583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (Int_t9123F050289CA0271A077EFF47440CBD5C366316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (Long_t28918BF6DBAB433B07F2F91D220DF42F896C5C63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (MD5_tBF240EC56773202152DBB5C53796D718895EC4EC), -1, sizeof(MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields::get_offset_of_ToString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F), -1, sizeof(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (TimeSpanSeconds_t147412A8A309180783B8369B158301EFA80A9DA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (Uri_tAA37A4AC2B4CCC173820F682599007F88D08407D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[2] = 
{
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5::get_offset_of_U3CNameU3Ek__BackingField_0(),
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5::get_offset_of_U3CVersionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[2] = 
{
	ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246::get_offset_of_U3CCommentU3Ek__BackingField_0(),
	ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246::get_offset_of_U3CProductU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[2] = 
{
	RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92::get_offset_of_U3CDateU3Ek__BackingField_0(),
	RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92::get_offset_of_U3CEntityTagU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[2] = 
{
	RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723::get_offset_of_ranges_0(),
	RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723::get_offset_of_unit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[2] = 
{
	RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664::get_offset_of_U3CFromU3Ek__BackingField_0(),
	RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664::get_offset_of_U3CToU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[2] = 
{
	RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6::get_offset_of_U3CDateU3Ek__BackingField_0(),
	RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6::get_offset_of_U3CDeltaU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[2] = 
{
	StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8::get_offset_of_U3CQualityU3Ek__BackingField_0(),
	StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[2] = 
{
	TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C::get_offset_of_value_0(),
	TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (TransferCodingWithQualityHeaderValue_tCCBC3E7752056FE94AF475137682B15E84FA626C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[4] = 
{
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CCommentU3Ek__BackingField_0(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CProtocolNameU3Ek__BackingField_1(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CProtocolVersionU3Ek__BackingField_2(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CReceivedByU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[4] = 
{
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CAgentU3Ek__BackingField_0(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CCodeU3Ek__BackingField_1(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CDateU3Ek__BackingField_2(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CTextU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF), -1, sizeof(U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2887[1] = 
{
	U3CPrivateImplementationDetailsU3E_t82FD2C9472E0E4ECBE205F80B57D1BE55B7D18EF_StaticFields::get_offset_of_A097044521F478B3A2A9A3AC52887BA733E4DE56_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (__StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D127_t9449C85D5F9E6911FB704ED4892BF6F420B567BA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (U3CModuleU3E_t18E01DC18978C39365422BE25552B43FDE647BA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2891[5] = 
{
	AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2892[6] = 
{
	AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E)+ sizeof (RuntimeObject), sizeof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2893[2] = 
{
	Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D), -1, sizeof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2897[26] = 
{
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_Version_0(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_nestedTweenFailureBehaviour_2(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_showUnityEditorReport_3(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_timeScale_4(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_useSmoothDeltaTime_5(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxSmoothUnscaledTime_6(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_rewindCallbackMode_7(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of__logBehaviour_8(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_onWillLog_9(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_drawGizmos_10(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultUpdateType_11(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultTimeScaleIndependent_12(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultAutoPlay_13(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultAutoKill_14(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultLoopType_15(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultRecyclable_16(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEaseType_17(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_18(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEasePeriod_19(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_instance_20(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxActiveTweenersReached_21(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxActiveSequencesReached_22(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_GizmosDelegates_23(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_initialized_24(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_isQuitting_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[2] = 
{
	U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass54_0_t0F2354CF28D5D074D97CF436C40F413F9AA442BE::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (DOVirtual_t5B66AD4FF33DE5D151DF9AE2CD8AC3EEBFBD9CA3), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
