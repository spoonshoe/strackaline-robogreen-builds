﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct VirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericVirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct InterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericInterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};

// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8;
// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA;
// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A;
// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B;
// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779;
// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30;
// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E;
// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929;
// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95;
// LoginViewController
struct LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833;
// Strackaline.MemberDetail
struct MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B;
// Strackaline.User
struct User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo>
struct Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.FileInfo
struct FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Principal.IPrincipal
struct IPrincipal_t63FD7F58FBBE134C8FE4D31710AAEA00B000F0BF;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A;
// System.Threading.ExecutionContext
struct ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70;
// System.Threading.InternalThread
struct InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Threading.ThreadStart
struct ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// TriLib.TextureLoadHandle
struct TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C;
// TriLib.TexturePreLoadHandle
struct TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24;
// TriLib.ThreadUtils/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7;
// TriLib.ThreadUtils/<>c__DisplayClass0_1
struct U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68;
// TwistMe
struct TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431;
// TwoDoubleTapMe
struct TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6;
// TwoDragMe
struct TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744;
// TwoLongTapMe
struct TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90;
// TwoSwipe
struct TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1;
// TwoTapMe
struct TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39;
// TwoTouchMe
struct TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4;
// UICompatibility
struct UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2;
// UIDrag
struct UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D;
// UIPinch
struct UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4;
// UITwist
struct UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0;
// UIWindow
struct UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090;
// UniformTexturedLine
struct UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityStandardAssets.ImageEffects.BloomOptimized
struct BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730;
// UnityStandardAssets.ImageEffects.PostEffectsBase
struct PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E;
// UserController
struct UserController_tFD917E07B535D607AFD7056993FB736FD366E600;
// UserSubmitData
struct UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF;
// VectorObject
struct VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B;
// Vectrosity.IVectorObject
struct IVectorObject_t6A320644FF8ACBE266FD9BA1E8D31DED52FAE201;
// Vectrosity.LineManager
struct LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776;
// Vectrosity.VectorLine
struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F;
// XrayLineData
struct XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3;

extern RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var;
extern RuntimeClass* Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_il2cpp_TypeInfo_var;
extern RuntimeClass* DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8_il2cpp_TypeInfo_var;
extern RuntimeClass* Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA_il2cpp_TypeInfo_var;
extern RuntimeClass* DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A_il2cpp_TypeInfo_var;
extern RuntimeClass* DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var;
extern RuntimeClass* LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_il2cpp_TypeInfo_var;
extern RuntimeClass* LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_il2cpp_TypeInfo_var;
extern RuntimeClass* LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779_il2cpp_TypeInfo_var;
extern RuntimeClass* Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862_il2cpp_TypeInfo_var;
extern RuntimeClass* SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE_il2cpp_TypeInfo_var;
extern RuntimeClass* SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_il2cpp_TypeInfo_var;
extern RuntimeClass* SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_il2cpp_TypeInfo_var;
extern RuntimeClass* TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5_il2cpp_TypeInfo_var;
extern RuntimeClass* TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F_il2cpp_TypeInfo_var;
extern RuntimeClass* ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF_il2cpp_TypeInfo_var;
extern RuntimeClass* Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
extern RuntimeClass* TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929_il2cpp_TypeInfo_var;
extern RuntimeClass* TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
extern RuntimeClass* VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var;
extern RuntimeClass* VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var;
extern RuntimeClass* XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral04A6EA50E92943F99BDE01B3CD227BA68A3EE75E;
extern String_t* _stringLiteral0A7D6582906A95B793B13B299CCFE2E94E81B566;
extern String_t* _stringLiteral0D0C4DDD7CAB8253149568AACCC1F72AA7F9ACF8;
extern String_t* _stringLiteral138799543726AA05424D18E73444A6C7AE801CD1;
extern String_t* _stringLiteral1A960D7D734E11913921D735AA4177AC122CABF7;
extern String_t* _stringLiteral3FD9B24FFD9A11F33EFDC7D229AD99867E6F5BEE;
extern String_t* _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8;
extern String_t* _stringLiteral48F24C9D20CEBDADAD22D9795E25D2284B0D7269;
extern String_t* _stringLiteral523FD7FA92D538B546335BF7C14572CD17CAAD15;
extern String_t* _stringLiteral5D4C761EAD1A15EE4348E51DC4C16D41A9C3604C;
extern String_t* _stringLiteral629553D78E18022708836F7773B70DC9DFC3AF5C;
extern String_t* _stringLiteral76AF80CC54267D2532EEDE98F8E5E95CF36F4E5A;
extern String_t* _stringLiteral76E34C4759CBCC4A5ECE2529ADFF033A72857F95;
extern String_t* _stringLiteral7B87A56EFD62101B4B8A4AAEEA3E8058B16EEDC4;
extern String_t* _stringLiteral8747FBD2838A430FE3A98F274FB1202FF95DC3BC;
extern String_t* _stringLiteral90D8307A0AF5EE8F6CC07794AEBAD275A58B04C0;
extern String_t* _stringLiteralA9DBC977A94F4F310A01C6C83731C39712AAD98B;
extern String_t* _stringLiteralB8A178409953D7928C4F1F94E9C06A5A9D23B355;
extern String_t* _stringLiteralC403BDE4925A5B437875D8BF934C7BAEE5B043CB;
extern String_t* _stringLiteralCF1126F67238BF3E85FCC8C8737B72E80DDCFDDB;
extern String_t* _stringLiteralD2227D5D6B26800F2C5978F782A05D6F0DC11F02;
extern String_t* _stringLiteralDE4D08C6D1642BC4345A93EFD01414BF4668EA4B;
extern String_t* _stringLiteralE1DF8B0AA9251994C53B6AE47BEFF033E7CA298B;
extern String_t* _stringLiteralE8368D7779E8560C0C02CE5983B80423E277F284;
extern String_t* _stringLiteralEA5C1A20B7CFCFEC8A35FB4C6A419A1314233755;
extern String_t* _stringLiteralEA9676003762818513C636984FAEAA0AE356839A;
extern String_t* _stringLiteralFD5E191A9C5EB57D0731B3A4BB8EB5E24AC6746C;
extern const RuntimeMethod* Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mFF06011DFE2A1EC6DFC8FF1C1E78EF60CA07E9D9_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m85AD1D8D83D26FF89816DBA34C37F77BA3B35518_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2C4E9711CA5C59D4FA95D74A13D54C8EF72E55BF_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962_RuntimeMethod_var;
extern const RuntimeMethod* TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A_RuntimeMethod_var;
extern const RuntimeMethod* TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44_RuntimeMethod_var;
extern const RuntimeMethod* TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A_RuntimeMethod_var;
extern const RuntimeMethod* TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1_RuntimeMethod_var;
extern const RuntimeMethod* TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D_RuntimeMethod_var;
extern const RuntimeMethod* TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB_RuntimeMethod_var;
extern const RuntimeMethod* TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88_RuntimeMethod_var;
extern const RuntimeMethod* TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7_RuntimeMethod_var;
extern const RuntimeMethod* TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5_RuntimeMethod_var;
extern const RuntimeMethod* TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA_RuntimeMethod_var;
extern const RuntimeMethod* TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0_RuntimeMethod_var;
extern const RuntimeMethod* TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA_RuntimeMethod_var;
extern const RuntimeMethod* TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB_RuntimeMethod_var;
extern const RuntimeMethod* TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895_RuntimeMethod_var;
extern const RuntimeMethod* TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0_RuntimeMethod_var;
extern const RuntimeMethod* TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79_RuntimeMethod_var;
extern const RuntimeMethod* TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF_RuntimeMethod_var;
extern const RuntimeMethod* TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511_RuntimeMethod_var;
extern const RuntimeMethod* TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE_RuntimeMethod_var;
extern const RuntimeMethod* TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7_RuntimeMethod_var;
extern const RuntimeMethod* TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass0_0_U3CRunThreadU3Eb__0_m7868462EB15600B4492B50FA3EEFC5A006E2C082_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC_RuntimeMethod_var;
extern const RuntimeMethod* UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E_RuntimeMethod_var;
extern const RuntimeMethod* UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C_RuntimeMethod_var;
extern const RuntimeMethod* UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63_RuntimeMethod_var;
extern const uint32_t BloomOptimized_OnDisable_m6C76582A668E9F0AD13481E54FF47AFBF5FE7EFE_MetadataUsageId;
extern const uint32_t BloomOptimized_OnRenderImage_m8ACD073311ECEB136FF61084DD541125C69F2C42_MetadataUsageId;
extern const uint32_t PostEffectsBase_CheckResources_m31A44EE19F985DCD4D3242F9197BF1435F1C8094_MetadataUsageId;
extern const uint32_t PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865_MetadataUsageId;
extern const uint32_t PostEffectsBase_CheckShader_mE87A8B176C2F6264E568AD9EFFE7A64F9057CB43_MetadataUsageId;
extern const uint32_t PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04_MetadataUsageId;
extern const uint32_t PostEffectsBase_CreateMaterial_mA2EDA33D7CD6FA380975DA46597540AAB17B89AE_MetadataUsageId;
extern const uint32_t PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C_MetadataUsageId;
extern const uint32_t TexturePreLoadHandle_BeginInvoke_m3F82EF8D6D50FAA81FAA5EF7508BDC32CEEBD034_MetadataUsageId;
extern const uint32_t ThreadUtils_RunThread_m037EF6B6DA34BC8BC277B0A778E128968E9727DE_MetadataUsageId;
extern const uint32_t TransformExtensions_DestroyChildren_m329CE7E105CDC9B880D01D98872F639B05DD3720_MetadataUsageId;
extern const uint32_t TransformExtensions_EncapsulateBounds_m829059F264548DCC07D3CA2773E12C4B03421C4F_MetadataUsageId;
extern const uint32_t TransformExtensions_FindDeepChild_mD574BD864794563C7ED11DE03BA4E83450E0F8B1_MetadataUsageId;
extern const uint32_t TwistMe_OnEnable_mC088E661BE5C529F317B51AA0B78AEC4ACFCB869_MetadataUsageId;
extern const uint32_t TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A_MetadataUsageId;
extern const uint32_t TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44_MetadataUsageId;
extern const uint32_t TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A_MetadataUsageId;
extern const uint32_t TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1_MetadataUsageId;
extern const uint32_t TwistMe_Start_mC6A0F64168BF753E5AE9E3FB1D020E5FE655EB68_MetadataUsageId;
extern const uint32_t TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F_MetadataUsageId;
extern const uint32_t TwoDoubleTapMe_OnEnable_m2B11FBB5777441BE991146F63778A1A570E1D1F6_MetadataUsageId;
extern const uint32_t TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D_MetadataUsageId;
extern const uint32_t TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657_MetadataUsageId;
extern const uint32_t TwoDragMe_OnEnable_m4D9291A4F42D0E4CCAD0A5E0A38C9530559F97BE_MetadataUsageId;
extern const uint32_t TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88_MetadataUsageId;
extern const uint32_t TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7_MetadataUsageId;
extern const uint32_t TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5_MetadataUsageId;
extern const uint32_t TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031_MetadataUsageId;
extern const uint32_t TwoDragMe_Start_m8299AE6AFFA99BDA8D0F51E23B9EF81A3D7C4BA2_MetadataUsageId;
extern const uint32_t TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C_MetadataUsageId;
extern const uint32_t TwoLongTapMe_OnEnable_m24553C57C72307C05864F880DE2E06DF759B5179_MetadataUsageId;
extern const uint32_t TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0_MetadataUsageId;
extern const uint32_t TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA_MetadataUsageId;
extern const uint32_t TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB_MetadataUsageId;
extern const uint32_t TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F_MetadataUsageId;
extern const uint32_t TwoLongTapMe_Start_m25B7567F5274044250351D028F3413A15B68DB13_MetadataUsageId;
extern const uint32_t TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E_MetadataUsageId;
extern const uint32_t TwoSwipe_OnEnable_mD01720E2E3D1FB0F5F6485FDE2E89087724160D2_MetadataUsageId;
extern const uint32_t TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0_MetadataUsageId;
extern const uint32_t TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79_MetadataUsageId;
extern const uint32_t TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE_MetadataUsageId;
extern const uint32_t TwoTapMe_OnEnable_mE11A33FC435C98E2DD8CAC8417E8CD0C181CBF8C_MetadataUsageId;
extern const uint32_t TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF_MetadataUsageId;
extern const uint32_t TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7_MetadataUsageId;
extern const uint32_t TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59_MetadataUsageId;
extern const uint32_t TwoTouchMe_OnEnable_mEADF58E4D9A10CE69524FAC5F05EEB0445DD6540_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06_MetadataUsageId;
extern const uint32_t TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322_MetadataUsageId;
extern const uint32_t TwoTouchMe_Start_m29E68CF505FCC00AF0CF622AF832E2AE063D1DEC_MetadataUsageId;
extern const uint32_t TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass0_0_U3CRunThreadU3Eb__0_m7868462EB15600B4492B50FA3EEFC5A006E2C082_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419_MetadataUsageId;
extern const uint32_t UIDrag_OnDestroy_mDBA34F76DF1D49FC4EAB15E75C912C9B6D1A419B_MetadataUsageId;
extern const uint32_t UIDrag_OnEnable_m53F3D1A1B1141E2A30F7633C81224DEAE59EE620_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC_MetadataUsageId;
extern const uint32_t UIPinch_OnDestroy_mB910E8CCBD3760031F31C9EAA2B20F0E8CDF62EE_MetadataUsageId;
extern const uint32_t UIPinch_OnEnable_m63DC2215BEC9FB17C5D4C3859FE58565DA8E3239_MetadataUsageId;
extern const uint32_t UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C_MetadataUsageId;
extern const uint32_t UITwist_OnDestroy_m183699EEB9681FC54940FB2114819D819E5B05DB_MetadataUsageId;
extern const uint32_t UITwist_OnEnable_m197511549C6C8217FDE1FA2264F5A0A25B1F0FC2_MetadataUsageId;
extern const uint32_t UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63_MetadataUsageId;
extern const uint32_t UIWindow_OnDrag_m0C823B17A262F9D6294E0015EB1627EE246FD591_MetadataUsageId;
extern const uint32_t UniformTexturedLine_Start_m2613952894AC753DDD633A16090196662A275C5F_MetadataUsageId;
extern const uint32_t UserController_LoadUserFromLocalFile_m503330B8722440718126146EA03DF95D972004AE_MetadataUsageId;
extern const uint32_t UserController_LogoutUserDataFromLocalFile_m0ECF6195FC75133A06C7B7700A29AC19D974F817_MetadataUsageId;
extern const uint32_t UserController_SaveUserToLocalFile_m2738573B388C610FCBA285F51CB76DEC7B29CC12_MetadataUsageId;
extern const uint32_t UserController_UserIsSavedLocally_mBAC5EB856667987C90C44761F20B711FD54B0D0A_MetadataUsageId;
extern const uint32_t VectorObject_Start_m5159770FFE85C518C2AD104F3B788EDE7333E180_MetadataUsageId;
extern const uint32_t XrayLineData_Awake_m933A7EB80FCED093E1EA6068852E340EC2DC9C3A_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#define USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.User
struct  User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371  : public RuntimeObject
{
public:
	// System.String Strackaline.User::name
	String_t* ___name_0;
	// System.String Strackaline.User::roboGreenID
	String_t* ___roboGreenID_1;
	// System.Int32 Strackaline.User::memberId
	int32_t ___memberId_2;
	// System.String Strackaline.User::token
	String_t* ___token_3;
	// Strackaline.MemberDetail Strackaline.User::memberDetail
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * ___memberDetail_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_roboGreenID_1() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___roboGreenID_1)); }
	inline String_t* get_roboGreenID_1() const { return ___roboGreenID_1; }
	inline String_t** get_address_of_roboGreenID_1() { return &___roboGreenID_1; }
	inline void set_roboGreenID_1(String_t* value)
	{
		___roboGreenID_1 = value;
		Il2CppCodeGenWriteBarrier((&___roboGreenID_1), value);
	}

	inline static int32_t get_offset_of_memberId_2() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberId_2)); }
	inline int32_t get_memberId_2() const { return ___memberId_2; }
	inline int32_t* get_address_of_memberId_2() { return &___memberId_2; }
	inline void set_memberId_2(int32_t value)
	{
		___memberId_2 = value;
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier((&___token_3), value);
	}

	inline static int32_t get_offset_of_memberDetail_4() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberDetail_4)); }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * get_memberDetail_4() const { return ___memberDetail_4; }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B ** get_address_of_memberDetail_4() { return &___memberDetail_4; }
	inline void set_memberDetail_4(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * value)
	{
		___memberDetail_4 = value;
		Il2CppCodeGenWriteBarrier((&___memberDetail_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T32916246A3DDDBFC219E335CCF07C7F68024800C_H
#define LIST_1_T32916246A3DDDBFC219E335CCF07C7F68024800C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C, ____items_1)); }
	inline List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C_StaticFields, ____emptyArray_5)); }
	inline List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* get__emptyArray_5() const { return ____emptyArray_5; }
	inline List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(List_1U5BU5D_t24A52F1892688FCF646EDEA73CCB670C5BBD6005* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T32916246A3DDDBFC219E335CCF07C7F68024800C_H
#ifndef LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#define LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifndef LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#define LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#define CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#define THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils
struct  ThreadUtils_tC54E980B81AFB99EAA5C04548985934AD90B7C49  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#define U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7  : public RuntimeObject
{
public:
	// System.Action TriLib.ThreadUtils_<>c__DisplayClass0_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;
	// System.Action TriLib.ThreadUtils_<>c__DisplayClass0_0::onComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7, ___onComplete_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#ifndef U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#define U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_<>c__DisplayClass0_1
struct  U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68  : public RuntimeObject
{
public:
	// System.Exception TriLib.ThreadUtils_<>c__DisplayClass0_1::exception
	Exception_t * ___exception_0;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68, ___exception_0)); }
	inline Exception_t * get_exception_0() const { return ___exception_0; }
	inline Exception_t ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier((&___exception_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#ifndef TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#define TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_tF34DF4728FF207B44EF4E4213DB8F391CD4CAABE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#ifndef ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#define ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifndef USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#define USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserSubmitData
struct  UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF  : public RuntimeObject
{
public:
	// System.String UserSubmitData::userName
	String_t* ___userName_0;
	// System.String UserSubmitData::password
	String_t* ___password_1;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#define THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7  : public CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___internal_thread_6)); }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((&___internal_thread_6), value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreadStartArg_7), value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_8), value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((&___principal_9), value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegate_12), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutionContext_13), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStoreMgr_0), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentCulture_4), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentUICulture_5), value);
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStore_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCulture_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentUICulture_3), value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((&___current_thread_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#define BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5  : public AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5, ___m_EventSystem_1)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#define EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_EvtType
struct  EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_EvtType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifndef SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#define SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection
struct  SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef FILEATTRIBUTES_T224B42F6F82954C94B51791913857C005C559876_H
#define FILEATTRIBUTES_T224B42F6F82954C94B51791913857C005C559876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t224B42F6F82954C94B51791913857C005C559876 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t224B42F6F82954C94B51791913857C005C559876, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T224B42F6F82954C94B51791913857C005C559876_H
#ifndef TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#define TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#ifndef AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#define AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.aiPropertyTypeInfo
struct  aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF 
{
public:
	// System.Int32 TriLib.aiPropertyTypeInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#define DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifndef INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#define INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData_InputButton
struct  InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifndef RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#define RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#define HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#define RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifndef TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#define TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#define BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized_BlurType
struct  BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized_BlurType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#ifndef RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#define RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized_Resolution
struct  Resolution_t8B57100A723C02C86B12199571699A420CA4B409 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized_Resolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Resolution_t8B57100A723C02C86B12199571699A420CA4B409, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#ifndef SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#define SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorObject_Shape
struct  Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB 
{
public:
	// System.Int32 VectorObject_Shape::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#ifndef BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#define BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Brightness
struct  Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4 
{
public:
	// System.Int32 Vectrosity.Brightness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifndef CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#define CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.CanvasState
struct  CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6 
{
public:
	// System.Int32 Vectrosity.CanvasState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifndef ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#define ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.EndCap
struct  EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F 
{
public:
	// System.Int32 Vectrosity.EndCap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifndef JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#define JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Joins
struct  Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F 
{
public:
	// System.Int32 Vectrosity.Joins::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifndef LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#define LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.LineType
struct  LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1 
{
public:
	// System.Int32 Vectrosity.LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifndef VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#define VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Visibility
struct  Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174 
{
public:
	// System.Int32 Vectrosity.Visibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifndef BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#define BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.BaseFinger
struct  BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::touchCount
	int32_t ___touchCount_1;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_2;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_3;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::deltaPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___deltaPosition_4;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::actionTime
	float ___actionTime_5;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::deltaTime
	float ___deltaTime_6;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.BaseFinger::pickedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___pickedCamera_7;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedObject_8;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isGuiCamera
	bool ___isGuiCamera_9;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isOverGui
	bool ___isOverGui_10;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedUIElement
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedUIElement_11;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::altitudeAngle
	float ___altitudeAngle_12;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::azimuthAngle
	float ___azimuthAngle_13;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::maximumPossiblePressure
	float ___maximumPossiblePressure_14;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::pressure
	float ___pressure_15;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radius
	float ___radius_16;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radiusVariance
	float ___radiusVariance_17;
	// UnityEngine.TouchType HedgehogTeam.EasyTouch.BaseFinger::touchType
	int32_t ___touchType_18;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_touchCount_1() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___touchCount_1)); }
	inline int32_t get_touchCount_1() const { return ___touchCount_1; }
	inline int32_t* get_address_of_touchCount_1() { return &___touchCount_1; }
	inline void set_touchCount_1(int32_t value)
	{
		___touchCount_1 = value;
	}

	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___startPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___position_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_3() const { return ___position_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_4() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___deltaPosition_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_deltaPosition_4() const { return ___deltaPosition_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_deltaPosition_4() { return &___deltaPosition_4; }
	inline void set_deltaPosition_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___deltaPosition_4 = value;
	}

	inline static int32_t get_offset_of_actionTime_5() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___actionTime_5)); }
	inline float get_actionTime_5() const { return ___actionTime_5; }
	inline float* get_address_of_actionTime_5() { return &___actionTime_5; }
	inline void set_actionTime_5(float value)
	{
		___actionTime_5 = value;
	}

	inline static int32_t get_offset_of_deltaTime_6() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___deltaTime_6)); }
	inline float get_deltaTime_6() const { return ___deltaTime_6; }
	inline float* get_address_of_deltaTime_6() { return &___deltaTime_6; }
	inline void set_deltaTime_6(float value)
	{
		___deltaTime_6 = value;
	}

	inline static int32_t get_offset_of_pickedCamera_7() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_pickedCamera_7() const { return ___pickedCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_pickedCamera_7() { return &___pickedCamera_7; }
	inline void set_pickedCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___pickedCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_7), value);
	}

	inline static int32_t get_offset_of_pickedObject_8() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedObject_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedObject_8() const { return ___pickedObject_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedObject_8() { return &___pickedObject_8; }
	inline void set_pickedObject_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_8), value);
	}

	inline static int32_t get_offset_of_isGuiCamera_9() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___isGuiCamera_9)); }
	inline bool get_isGuiCamera_9() const { return ___isGuiCamera_9; }
	inline bool* get_address_of_isGuiCamera_9() { return &___isGuiCamera_9; }
	inline void set_isGuiCamera_9(bool value)
	{
		___isGuiCamera_9 = value;
	}

	inline static int32_t get_offset_of_isOverGui_10() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___isOverGui_10)); }
	inline bool get_isOverGui_10() const { return ___isOverGui_10; }
	inline bool* get_address_of_isOverGui_10() { return &___isOverGui_10; }
	inline void set_isOverGui_10(bool value)
	{
		___isOverGui_10 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_11() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedUIElement_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedUIElement_11() const { return ___pickedUIElement_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedUIElement_11() { return &___pickedUIElement_11; }
	inline void set_pickedUIElement_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedUIElement_11 = value;
		Il2CppCodeGenWriteBarrier((&___pickedUIElement_11), value);
	}

	inline static int32_t get_offset_of_altitudeAngle_12() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___altitudeAngle_12)); }
	inline float get_altitudeAngle_12() const { return ___altitudeAngle_12; }
	inline float* get_address_of_altitudeAngle_12() { return &___altitudeAngle_12; }
	inline void set_altitudeAngle_12(float value)
	{
		___altitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_azimuthAngle_13() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___azimuthAngle_13)); }
	inline float get_azimuthAngle_13() const { return ___azimuthAngle_13; }
	inline float* get_address_of_azimuthAngle_13() { return &___azimuthAngle_13; }
	inline void set_azimuthAngle_13(float value)
	{
		___azimuthAngle_13 = value;
	}

	inline static int32_t get_offset_of_maximumPossiblePressure_14() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___maximumPossiblePressure_14)); }
	inline float get_maximumPossiblePressure_14() const { return ___maximumPossiblePressure_14; }
	inline float* get_address_of_maximumPossiblePressure_14() { return &___maximumPossiblePressure_14; }
	inline void set_maximumPossiblePressure_14(float value)
	{
		___maximumPossiblePressure_14 = value;
	}

	inline static int32_t get_offset_of_pressure_15() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pressure_15)); }
	inline float get_pressure_15() const { return ___pressure_15; }
	inline float* get_address_of_pressure_15() { return &___pressure_15; }
	inline void set_pressure_15(float value)
	{
		___pressure_15 = value;
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___radius_16)); }
	inline float get_radius_16() const { return ___radius_16; }
	inline float* get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(float value)
	{
		___radius_16 = value;
	}

	inline static int32_t get_offset_of_radiusVariance_17() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___radiusVariance_17)); }
	inline float get_radiusVariance_17() const { return ___radiusVariance_17; }
	inline float* get_address_of_radiusVariance_17() { return &___radiusVariance_17; }
	inline void set_radiusVariance_17(float value)
	{
		___radiusVariance_17 = value;
	}

	inline static int32_t get_offset_of_touchType_18() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___touchType_18)); }
	inline int32_t get_touchType_18() const { return ___touchType_18; }
	inline int32_t* get_address_of_touchType_18() { return &___touchType_18; }
	inline void set_touchType_18(int32_t value)
	{
		___touchType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#ifndef MONOIOSTAT_T819C03DA1902AA493BDBF4B55E76DCE6FB16A124_H
#define MONOIOSTAT_T819C03DA1902AA493BDBF4B55E76DCE6FB16A124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124 
{
public:
	// System.IO.FileAttributes System.IO.MonoIOStat::fileAttributes
	int32_t ___fileAttributes_0;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_1;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_2;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_3;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_fileAttributes_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124, ___fileAttributes_0)); }
	inline int32_t get_fileAttributes_0() const { return ___fileAttributes_0; }
	inline int32_t* get_address_of_fileAttributes_0() { return &___fileAttributes_0; }
	inline void set_fileAttributes_0(int32_t value)
	{
		___fileAttributes_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124, ___Length_1)); }
	inline int64_t get_Length_1() const { return ___Length_1; }
	inline int64_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int64_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_CreationTime_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124, ___CreationTime_2)); }
	inline int64_t get_CreationTime_2() const { return ___CreationTime_2; }
	inline int64_t* get_address_of_CreationTime_2() { return &___CreationTime_2; }
	inline void set_CreationTime_2(int64_t value)
	{
		___CreationTime_2 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124, ___LastAccessTime_3)); }
	inline int64_t get_LastAccessTime_3() const { return ___LastAccessTime_3; }
	inline int64_t* get_address_of_LastAccessTime_3() { return &___LastAccessTime_3; }
	inline void set_LastAccessTime_3(int64_t value)
	{
		___LastAccessTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124, ___LastWriteTime_4)); }
	inline int64_t get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline int64_t* get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(int64_t value)
	{
		___LastWriteTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIOSTAT_T819C03DA1902AA493BDBF4B55E76DCE6FB16A124_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#define POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___m_PointerPress_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___hovered_9)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#define MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifndef SHADER_TE2731FF351B74AB4186897484FB01E000C1160CA_H
#define SHADER_TE2731FF351B74AB4186897484FB01E000C1160CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_tE2731FF351B74AB4186897484FB01E000C1160CA  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_TE2731FF351B74AB4186897484FB01E000C1160CA_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#define VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine
struct  VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_lineVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_lineVertices_0;
	// UnityEngine.Vector2[] Vectrosity.VectorLine::m_lineUVs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_lineUVs_1;
	// UnityEngine.Color32[] Vectrosity.VectorLine::m_lineColors
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_lineColors_2;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::m_lineTriangles
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lineTriangles_3;
	// System.Int32 Vectrosity.VectorLine::m_vertexCount
	int32_t ___m_vertexCount_4;
	// UnityEngine.GameObject Vectrosity.VectorLine::m_go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_go_5;
	// UnityEngine.RectTransform Vectrosity.VectorLine::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_6;
	// Vectrosity.IVectorObject Vectrosity.VectorLine::m_vectorObject
	RuntimeObject* ___m_vectorObject_7;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_color_8;
	// Vectrosity.CanvasState Vectrosity.VectorLine::m_canvasState
	int32_t ___m_canvasState_9;
	// System.Boolean Vectrosity.VectorLine::m_is2D
	bool ___m_is2D_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::m_points2
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_points2_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::m_points3
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_points3_12;
	// System.Int32 Vectrosity.VectorLine::m_pointsCount
	int32_t ___m_pointsCount_13;
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_screenPoints
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_screenPoints_14;
	// System.Single[] Vectrosity.VectorLine::m_lineWidths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_lineWidths_15;
	// System.Single Vectrosity.VectorLine::m_lineWidth
	float ___m_lineWidth_16;
	// System.Single Vectrosity.VectorLine::m_maxWeldDistance
	float ___m_maxWeldDistance_17;
	// System.Single[] Vectrosity.VectorLine::m_distances
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_distances_18;
	// System.String Vectrosity.VectorLine::m_name
	String_t* ___m_name_19;
	// UnityEngine.Material Vectrosity.VectorLine::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_20;
	// UnityEngine.Texture Vectrosity.VectorLine::m_originalTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_originalTexture_21;
	// UnityEngine.Texture Vectrosity.VectorLine::m_texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_texture_22;
	// System.Boolean Vectrosity.VectorLine::m_active
	bool ___m_active_23;
	// Vectrosity.LineType Vectrosity.VectorLine::m_lineType
	int32_t ___m_lineType_24;
	// System.Single Vectrosity.VectorLine::m_capLength
	float ___m_capLength_25;
	// System.Boolean Vectrosity.VectorLine::m_smoothWidth
	bool ___m_smoothWidth_26;
	// System.Boolean Vectrosity.VectorLine::m_smoothColor
	bool ___m_smoothColor_27;
	// Vectrosity.Joins Vectrosity.VectorLine::m_joins
	int32_t ___m_joins_28;
	// System.Boolean Vectrosity.VectorLine::m_isAutoDrawing
	bool ___m_isAutoDrawing_29;
	// System.Int32 Vectrosity.VectorLine::m_drawStart
	int32_t ___m_drawStart_30;
	// System.Int32 Vectrosity.VectorLine::m_drawEnd
	int32_t ___m_drawEnd_31;
	// System.Int32 Vectrosity.VectorLine::m_endPointsUpdate
	int32_t ___m_endPointsUpdate_32;
	// System.Boolean Vectrosity.VectorLine::m_useNormals
	bool ___m_useNormals_33;
	// System.Boolean Vectrosity.VectorLine::m_useTangents
	bool ___m_useTangents_34;
	// System.Boolean Vectrosity.VectorLine::m_normalsCalculated
	bool ___m_normalsCalculated_35;
	// System.Boolean Vectrosity.VectorLine::m_tangentsCalculated
	bool ___m_tangentsCalculated_36;
	// Vectrosity.EndCap Vectrosity.VectorLine::m_capType
	int32_t ___m_capType_37;
	// System.String Vectrosity.VectorLine::m_endCap
	String_t* ___m_endCap_38;
	// System.Boolean Vectrosity.VectorLine::m_useCapColors
	bool ___m_useCapColors_39;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_frontColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_frontColor_40;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_backColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_backColor_41;
	// System.Int32 Vectrosity.VectorLine::m_frontEndCapIndex
	int32_t ___m_frontEndCapIndex_42;
	// System.Int32 Vectrosity.VectorLine::m_backEndCapIndex
	int32_t ___m_backEndCapIndex_43;
	// System.Single Vectrosity.VectorLine::m_lineUVBottom
	float ___m_lineUVBottom_44;
	// System.Single Vectrosity.VectorLine::m_lineUVTop
	float ___m_lineUVTop_45;
	// System.Single Vectrosity.VectorLine::m_frontCapUVBottom
	float ___m_frontCapUVBottom_46;
	// System.Single Vectrosity.VectorLine::m_frontCapUVTop
	float ___m_frontCapUVTop_47;
	// System.Single Vectrosity.VectorLine::m_backCapUVBottom
	float ___m_backCapUVBottom_48;
	// System.Single Vectrosity.VectorLine::m_backCapUVTop
	float ___m_backCapUVTop_49;
	// System.Boolean Vectrosity.VectorLine::m_continuousTexture
	bool ___m_continuousTexture_50;
	// UnityEngine.Transform Vectrosity.VectorLine::m_drawTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_drawTransform_51;
	// System.Boolean Vectrosity.VectorLine::m_viewportDraw
	bool ___m_viewportDraw_52;
	// System.Single Vectrosity.VectorLine::m_textureScale
	float ___m_textureScale_53;
	// System.Boolean Vectrosity.VectorLine::m_useTextureScale
	bool ___m_useTextureScale_54;
	// System.Single Vectrosity.VectorLine::m_textureOffset
	float ___m_textureOffset_55;
	// System.Boolean Vectrosity.VectorLine::m_useMatrix
	bool ___m_useMatrix_56;
	// UnityEngine.Matrix4x4 Vectrosity.VectorLine::m_matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_matrix_57;
	// System.Boolean Vectrosity.VectorLine::m_collider
	bool ___m_collider_58;
	// System.Boolean Vectrosity.VectorLine::m_trigger
	bool ___m_trigger_59;
	// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::m_physicsMaterial
	PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * ___m_physicsMaterial_60;
	// System.Boolean Vectrosity.VectorLine::m_alignOddWidthToPixels
	bool ___m_alignOddWidthToPixels_61;

public:
	inline static int32_t get_offset_of_m_lineVertices_0() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineVertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_lineVertices_0() const { return ___m_lineVertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_lineVertices_0() { return &___m_lineVertices_0; }
	inline void set_m_lineVertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_lineVertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineVertices_0), value);
	}

	inline static int32_t get_offset_of_m_lineUVs_1() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVs_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_lineUVs_1() const { return ___m_lineUVs_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_lineUVs_1() { return &___m_lineUVs_1; }
	inline void set_m_lineUVs_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_lineUVs_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineUVs_1), value);
	}

	inline static int32_t get_offset_of_m_lineColors_2() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineColors_2)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_lineColors_2() const { return ___m_lineColors_2; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_lineColors_2() { return &___m_lineColors_2; }
	inline void set_m_lineColors_2(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_lineColors_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineColors_2), value);
	}

	inline static int32_t get_offset_of_m_lineTriangles_3() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineTriangles_3)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lineTriangles_3() const { return ___m_lineTriangles_3; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lineTriangles_3() { return &___m_lineTriangles_3; }
	inline void set_m_lineTriangles_3(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lineTriangles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineTriangles_3), value);
	}

	inline static int32_t get_offset_of_m_vertexCount_4() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vertexCount_4)); }
	inline int32_t get_m_vertexCount_4() const { return ___m_vertexCount_4; }
	inline int32_t* get_address_of_m_vertexCount_4() { return &___m_vertexCount_4; }
	inline void set_m_vertexCount_4(int32_t value)
	{
		___m_vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_m_go_5() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_go_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_go_5() const { return ___m_go_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_go_5() { return &___m_go_5; }
	inline void set_m_go_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_go_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_go_5), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_6() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_rectTransform_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_6() const { return ___m_rectTransform_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_6() { return &___m_rectTransform_6; }
	inline void set_m_rectTransform_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_6), value);
	}

	inline static int32_t get_offset_of_m_vectorObject_7() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vectorObject_7)); }
	inline RuntimeObject* get_m_vectorObject_7() const { return ___m_vectorObject_7; }
	inline RuntimeObject** get_address_of_m_vectorObject_7() { return &___m_vectorObject_7; }
	inline void set_m_vectorObject_7(RuntimeObject* value)
	{
		___m_vectorObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorObject_7), value);
	}

	inline static int32_t get_offset_of_m_color_8() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_color_8)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_color_8() const { return ___m_color_8; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_color_8() { return &___m_color_8; }
	inline void set_m_color_8(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_color_8 = value;
	}

	inline static int32_t get_offset_of_m_canvasState_9() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_canvasState_9)); }
	inline int32_t get_m_canvasState_9() const { return ___m_canvasState_9; }
	inline int32_t* get_address_of_m_canvasState_9() { return &___m_canvasState_9; }
	inline void set_m_canvasState_9(int32_t value)
	{
		___m_canvasState_9 = value;
	}

	inline static int32_t get_offset_of_m_is2D_10() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_is2D_10)); }
	inline bool get_m_is2D_10() const { return ___m_is2D_10; }
	inline bool* get_address_of_m_is2D_10() { return &___m_is2D_10; }
	inline void set_m_is2D_10(bool value)
	{
		___m_is2D_10 = value;
	}

	inline static int32_t get_offset_of_m_points2_11() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points2_11)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_points2_11() const { return ___m_points2_11; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_points2_11() { return &___m_points2_11; }
	inline void set_m_points2_11(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_points2_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_points2_11), value);
	}

	inline static int32_t get_offset_of_m_points3_12() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points3_12)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_points3_12() const { return ___m_points3_12; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_points3_12() { return &___m_points3_12; }
	inline void set_m_points3_12(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_points3_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_points3_12), value);
	}

	inline static int32_t get_offset_of_m_pointsCount_13() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_pointsCount_13)); }
	inline int32_t get_m_pointsCount_13() const { return ___m_pointsCount_13; }
	inline int32_t* get_address_of_m_pointsCount_13() { return &___m_pointsCount_13; }
	inline void set_m_pointsCount_13(int32_t value)
	{
		___m_pointsCount_13 = value;
	}

	inline static int32_t get_offset_of_m_screenPoints_14() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_screenPoints_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_screenPoints_14() const { return ___m_screenPoints_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_screenPoints_14() { return &___m_screenPoints_14; }
	inline void set_m_screenPoints_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_screenPoints_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_screenPoints_14), value);
	}

	inline static int32_t get_offset_of_m_lineWidths_15() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidths_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_lineWidths_15() const { return ___m_lineWidths_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_lineWidths_15() { return &___m_lineWidths_15; }
	inline void set_m_lineWidths_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_lineWidths_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWidths_15), value);
	}

	inline static int32_t get_offset_of_m_lineWidth_16() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidth_16)); }
	inline float get_m_lineWidth_16() const { return ___m_lineWidth_16; }
	inline float* get_address_of_m_lineWidth_16() { return &___m_lineWidth_16; }
	inline void set_m_lineWidth_16(float value)
	{
		___m_lineWidth_16 = value;
	}

	inline static int32_t get_offset_of_m_maxWeldDistance_17() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_maxWeldDistance_17)); }
	inline float get_m_maxWeldDistance_17() const { return ___m_maxWeldDistance_17; }
	inline float* get_address_of_m_maxWeldDistance_17() { return &___m_maxWeldDistance_17; }
	inline void set_m_maxWeldDistance_17(float value)
	{
		___m_maxWeldDistance_17 = value;
	}

	inline static int32_t get_offset_of_m_distances_18() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_distances_18)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_distances_18() const { return ___m_distances_18; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_distances_18() { return &___m_distances_18; }
	inline void set_m_distances_18(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_distances_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_distances_18), value);
	}

	inline static int32_t get_offset_of_m_name_19() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_name_19)); }
	inline String_t* get_m_name_19() const { return ___m_name_19; }
	inline String_t** get_address_of_m_name_19() { return &___m_name_19; }
	inline void set_m_name_19(String_t* value)
	{
		___m_name_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_19), value);
	}

	inline static int32_t get_offset_of_m_material_20() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_material_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_20() const { return ___m_material_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_20() { return &___m_material_20; }
	inline void set_m_material_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_20), value);
	}

	inline static int32_t get_offset_of_m_originalTexture_21() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_originalTexture_21)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_originalTexture_21() const { return ___m_originalTexture_21; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_originalTexture_21() { return &___m_originalTexture_21; }
	inline void set_m_originalTexture_21(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_originalTexture_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalTexture_21), value);
	}

	inline static int32_t get_offset_of_m_texture_22() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_texture_22)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_texture_22() const { return ___m_texture_22; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_texture_22() { return &___m_texture_22; }
	inline void set_m_texture_22(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_texture_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_texture_22), value);
	}

	inline static int32_t get_offset_of_m_active_23() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_active_23)); }
	inline bool get_m_active_23() const { return ___m_active_23; }
	inline bool* get_address_of_m_active_23() { return &___m_active_23; }
	inline void set_m_active_23(bool value)
	{
		___m_active_23 = value;
	}

	inline static int32_t get_offset_of_m_lineType_24() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineType_24)); }
	inline int32_t get_m_lineType_24() const { return ___m_lineType_24; }
	inline int32_t* get_address_of_m_lineType_24() { return &___m_lineType_24; }
	inline void set_m_lineType_24(int32_t value)
	{
		___m_lineType_24 = value;
	}

	inline static int32_t get_offset_of_m_capLength_25() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capLength_25)); }
	inline float get_m_capLength_25() const { return ___m_capLength_25; }
	inline float* get_address_of_m_capLength_25() { return &___m_capLength_25; }
	inline void set_m_capLength_25(float value)
	{
		___m_capLength_25 = value;
	}

	inline static int32_t get_offset_of_m_smoothWidth_26() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothWidth_26)); }
	inline bool get_m_smoothWidth_26() const { return ___m_smoothWidth_26; }
	inline bool* get_address_of_m_smoothWidth_26() { return &___m_smoothWidth_26; }
	inline void set_m_smoothWidth_26(bool value)
	{
		___m_smoothWidth_26 = value;
	}

	inline static int32_t get_offset_of_m_smoothColor_27() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothColor_27)); }
	inline bool get_m_smoothColor_27() const { return ___m_smoothColor_27; }
	inline bool* get_address_of_m_smoothColor_27() { return &___m_smoothColor_27; }
	inline void set_m_smoothColor_27(bool value)
	{
		___m_smoothColor_27 = value;
	}

	inline static int32_t get_offset_of_m_joins_28() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_joins_28)); }
	inline int32_t get_m_joins_28() const { return ___m_joins_28; }
	inline int32_t* get_address_of_m_joins_28() { return &___m_joins_28; }
	inline void set_m_joins_28(int32_t value)
	{
		___m_joins_28 = value;
	}

	inline static int32_t get_offset_of_m_isAutoDrawing_29() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_isAutoDrawing_29)); }
	inline bool get_m_isAutoDrawing_29() const { return ___m_isAutoDrawing_29; }
	inline bool* get_address_of_m_isAutoDrawing_29() { return &___m_isAutoDrawing_29; }
	inline void set_m_isAutoDrawing_29(bool value)
	{
		___m_isAutoDrawing_29 = value;
	}

	inline static int32_t get_offset_of_m_drawStart_30() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawStart_30)); }
	inline int32_t get_m_drawStart_30() const { return ___m_drawStart_30; }
	inline int32_t* get_address_of_m_drawStart_30() { return &___m_drawStart_30; }
	inline void set_m_drawStart_30(int32_t value)
	{
		___m_drawStart_30 = value;
	}

	inline static int32_t get_offset_of_m_drawEnd_31() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawEnd_31)); }
	inline int32_t get_m_drawEnd_31() const { return ___m_drawEnd_31; }
	inline int32_t* get_address_of_m_drawEnd_31() { return &___m_drawEnd_31; }
	inline void set_m_drawEnd_31(int32_t value)
	{
		___m_drawEnd_31 = value;
	}

	inline static int32_t get_offset_of_m_endPointsUpdate_32() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endPointsUpdate_32)); }
	inline int32_t get_m_endPointsUpdate_32() const { return ___m_endPointsUpdate_32; }
	inline int32_t* get_address_of_m_endPointsUpdate_32() { return &___m_endPointsUpdate_32; }
	inline void set_m_endPointsUpdate_32(int32_t value)
	{
		___m_endPointsUpdate_32 = value;
	}

	inline static int32_t get_offset_of_m_useNormals_33() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useNormals_33)); }
	inline bool get_m_useNormals_33() const { return ___m_useNormals_33; }
	inline bool* get_address_of_m_useNormals_33() { return &___m_useNormals_33; }
	inline void set_m_useNormals_33(bool value)
	{
		___m_useNormals_33 = value;
	}

	inline static int32_t get_offset_of_m_useTangents_34() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTangents_34)); }
	inline bool get_m_useTangents_34() const { return ___m_useTangents_34; }
	inline bool* get_address_of_m_useTangents_34() { return &___m_useTangents_34; }
	inline void set_m_useTangents_34(bool value)
	{
		___m_useTangents_34 = value;
	}

	inline static int32_t get_offset_of_m_normalsCalculated_35() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_normalsCalculated_35)); }
	inline bool get_m_normalsCalculated_35() const { return ___m_normalsCalculated_35; }
	inline bool* get_address_of_m_normalsCalculated_35() { return &___m_normalsCalculated_35; }
	inline void set_m_normalsCalculated_35(bool value)
	{
		___m_normalsCalculated_35 = value;
	}

	inline static int32_t get_offset_of_m_tangentsCalculated_36() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_tangentsCalculated_36)); }
	inline bool get_m_tangentsCalculated_36() const { return ___m_tangentsCalculated_36; }
	inline bool* get_address_of_m_tangentsCalculated_36() { return &___m_tangentsCalculated_36; }
	inline void set_m_tangentsCalculated_36(bool value)
	{
		___m_tangentsCalculated_36 = value;
	}

	inline static int32_t get_offset_of_m_capType_37() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capType_37)); }
	inline int32_t get_m_capType_37() const { return ___m_capType_37; }
	inline int32_t* get_address_of_m_capType_37() { return &___m_capType_37; }
	inline void set_m_capType_37(int32_t value)
	{
		___m_capType_37 = value;
	}

	inline static int32_t get_offset_of_m_endCap_38() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endCap_38)); }
	inline String_t* get_m_endCap_38() const { return ___m_endCap_38; }
	inline String_t** get_address_of_m_endCap_38() { return &___m_endCap_38; }
	inline void set_m_endCap_38(String_t* value)
	{
		___m_endCap_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_endCap_38), value);
	}

	inline static int32_t get_offset_of_m_useCapColors_39() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useCapColors_39)); }
	inline bool get_m_useCapColors_39() const { return ___m_useCapColors_39; }
	inline bool* get_address_of_m_useCapColors_39() { return &___m_useCapColors_39; }
	inline void set_m_useCapColors_39(bool value)
	{
		___m_useCapColors_39 = value;
	}

	inline static int32_t get_offset_of_m_frontColor_40() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontColor_40)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_frontColor_40() const { return ___m_frontColor_40; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_frontColor_40() { return &___m_frontColor_40; }
	inline void set_m_frontColor_40(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_frontColor_40 = value;
	}

	inline static int32_t get_offset_of_m_backColor_41() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backColor_41)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_backColor_41() const { return ___m_backColor_41; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_backColor_41() { return &___m_backColor_41; }
	inline void set_m_backColor_41(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_backColor_41 = value;
	}

	inline static int32_t get_offset_of_m_frontEndCapIndex_42() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontEndCapIndex_42)); }
	inline int32_t get_m_frontEndCapIndex_42() const { return ___m_frontEndCapIndex_42; }
	inline int32_t* get_address_of_m_frontEndCapIndex_42() { return &___m_frontEndCapIndex_42; }
	inline void set_m_frontEndCapIndex_42(int32_t value)
	{
		___m_frontEndCapIndex_42 = value;
	}

	inline static int32_t get_offset_of_m_backEndCapIndex_43() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backEndCapIndex_43)); }
	inline int32_t get_m_backEndCapIndex_43() const { return ___m_backEndCapIndex_43; }
	inline int32_t* get_address_of_m_backEndCapIndex_43() { return &___m_backEndCapIndex_43; }
	inline void set_m_backEndCapIndex_43(int32_t value)
	{
		___m_backEndCapIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_lineUVBottom_44() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVBottom_44)); }
	inline float get_m_lineUVBottom_44() const { return ___m_lineUVBottom_44; }
	inline float* get_address_of_m_lineUVBottom_44() { return &___m_lineUVBottom_44; }
	inline void set_m_lineUVBottom_44(float value)
	{
		___m_lineUVBottom_44 = value;
	}

	inline static int32_t get_offset_of_m_lineUVTop_45() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVTop_45)); }
	inline float get_m_lineUVTop_45() const { return ___m_lineUVTop_45; }
	inline float* get_address_of_m_lineUVTop_45() { return &___m_lineUVTop_45; }
	inline void set_m_lineUVTop_45(float value)
	{
		___m_lineUVTop_45 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVBottom_46() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVBottom_46)); }
	inline float get_m_frontCapUVBottom_46() const { return ___m_frontCapUVBottom_46; }
	inline float* get_address_of_m_frontCapUVBottom_46() { return &___m_frontCapUVBottom_46; }
	inline void set_m_frontCapUVBottom_46(float value)
	{
		___m_frontCapUVBottom_46 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVTop_47() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVTop_47)); }
	inline float get_m_frontCapUVTop_47() const { return ___m_frontCapUVTop_47; }
	inline float* get_address_of_m_frontCapUVTop_47() { return &___m_frontCapUVTop_47; }
	inline void set_m_frontCapUVTop_47(float value)
	{
		___m_frontCapUVTop_47 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVBottom_48() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVBottom_48)); }
	inline float get_m_backCapUVBottom_48() const { return ___m_backCapUVBottom_48; }
	inline float* get_address_of_m_backCapUVBottom_48() { return &___m_backCapUVBottom_48; }
	inline void set_m_backCapUVBottom_48(float value)
	{
		___m_backCapUVBottom_48 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVTop_49() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVTop_49)); }
	inline float get_m_backCapUVTop_49() const { return ___m_backCapUVTop_49; }
	inline float* get_address_of_m_backCapUVTop_49() { return &___m_backCapUVTop_49; }
	inline void set_m_backCapUVTop_49(float value)
	{
		___m_backCapUVTop_49 = value;
	}

	inline static int32_t get_offset_of_m_continuousTexture_50() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_continuousTexture_50)); }
	inline bool get_m_continuousTexture_50() const { return ___m_continuousTexture_50; }
	inline bool* get_address_of_m_continuousTexture_50() { return &___m_continuousTexture_50; }
	inline void set_m_continuousTexture_50(bool value)
	{
		___m_continuousTexture_50 = value;
	}

	inline static int32_t get_offset_of_m_drawTransform_51() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawTransform_51)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_drawTransform_51() const { return ___m_drawTransform_51; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_drawTransform_51() { return &___m_drawTransform_51; }
	inline void set_m_drawTransform_51(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_drawTransform_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_drawTransform_51), value);
	}

	inline static int32_t get_offset_of_m_viewportDraw_52() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_viewportDraw_52)); }
	inline bool get_m_viewportDraw_52() const { return ___m_viewportDraw_52; }
	inline bool* get_address_of_m_viewportDraw_52() { return &___m_viewportDraw_52; }
	inline void set_m_viewportDraw_52(bool value)
	{
		___m_viewportDraw_52 = value;
	}

	inline static int32_t get_offset_of_m_textureScale_53() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureScale_53)); }
	inline float get_m_textureScale_53() const { return ___m_textureScale_53; }
	inline float* get_address_of_m_textureScale_53() { return &___m_textureScale_53; }
	inline void set_m_textureScale_53(float value)
	{
		___m_textureScale_53 = value;
	}

	inline static int32_t get_offset_of_m_useTextureScale_54() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTextureScale_54)); }
	inline bool get_m_useTextureScale_54() const { return ___m_useTextureScale_54; }
	inline bool* get_address_of_m_useTextureScale_54() { return &___m_useTextureScale_54; }
	inline void set_m_useTextureScale_54(bool value)
	{
		___m_useTextureScale_54 = value;
	}

	inline static int32_t get_offset_of_m_textureOffset_55() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureOffset_55)); }
	inline float get_m_textureOffset_55() const { return ___m_textureOffset_55; }
	inline float* get_address_of_m_textureOffset_55() { return &___m_textureOffset_55; }
	inline void set_m_textureOffset_55(float value)
	{
		___m_textureOffset_55 = value;
	}

	inline static int32_t get_offset_of_m_useMatrix_56() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useMatrix_56)); }
	inline bool get_m_useMatrix_56() const { return ___m_useMatrix_56; }
	inline bool* get_address_of_m_useMatrix_56() { return &___m_useMatrix_56; }
	inline void set_m_useMatrix_56(bool value)
	{
		___m_useMatrix_56 = value;
	}

	inline static int32_t get_offset_of_m_matrix_57() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_matrix_57)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_matrix_57() const { return ___m_matrix_57; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_matrix_57() { return &___m_matrix_57; }
	inline void set_m_matrix_57(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_matrix_57 = value;
	}

	inline static int32_t get_offset_of_m_collider_58() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_collider_58)); }
	inline bool get_m_collider_58() const { return ___m_collider_58; }
	inline bool* get_address_of_m_collider_58() { return &___m_collider_58; }
	inline void set_m_collider_58(bool value)
	{
		___m_collider_58 = value;
	}

	inline static int32_t get_offset_of_m_trigger_59() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_trigger_59)); }
	inline bool get_m_trigger_59() const { return ___m_trigger_59; }
	inline bool* get_address_of_m_trigger_59() { return &___m_trigger_59; }
	inline void set_m_trigger_59(bool value)
	{
		___m_trigger_59 = value;
	}

	inline static int32_t get_offset_of_m_physicsMaterial_60() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_physicsMaterial_60)); }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * get_m_physicsMaterial_60() const { return ___m_physicsMaterial_60; }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 ** get_address_of_m_physicsMaterial_60() { return &___m_physicsMaterial_60; }
	inline void set_m_physicsMaterial_60(PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * value)
	{
		___m_physicsMaterial_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_physicsMaterial_60), value);
	}

	inline static int32_t get_offset_of_m_alignOddWidthToPixels_61() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_alignOddWidthToPixels_61)); }
	inline bool get_m_alignOddWidthToPixels_61() const { return ___m_alignOddWidthToPixels_61; }
	inline bool* get_address_of_m_alignOddWidthToPixels_61() { return &___m_alignOddWidthToPixels_61; }
	inline void set_m_alignOddWidthToPixels_61(bool value)
	{
		___m_alignOddWidthToPixels_61 = value;
	}
};

struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields
{
public:
	// UnityEngine.Vector3 Vectrosity.VectorLine::v3zero
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v3zero_62;
	// UnityEngine.Canvas Vectrosity.VectorLine::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_63;
	// UnityEngine.Transform Vectrosity.VectorLine::camTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___camTransform_64;
	// UnityEngine.Camera Vectrosity.VectorLine::cam3D
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam3D_65;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldPosition_66;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldRotation_67;
	// System.Boolean Vectrosity.VectorLine::lineManagerCreated
	bool ___lineManagerCreated_68;
	// Vectrosity.LineManager Vectrosity.VectorLine::m_lineManager
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * ___m_lineManager_69;
	// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo> Vectrosity.VectorLine::capDictionary
	Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * ___capDictionary_70;
	// System.Int32 Vectrosity.VectorLine::endianDiff1
	int32_t ___endianDiff1_71;
	// System.Int32 Vectrosity.VectorLine::endianDiff2
	int32_t ___endianDiff2_72;
	// System.Byte[] Vectrosity.VectorLine::byteBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___byteBlock_73;
	// System.String[] Vectrosity.VectorLine::functionNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___functionNames_74;

public:
	inline static int32_t get_offset_of_v3zero_62() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___v3zero_62)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v3zero_62() const { return ___v3zero_62; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v3zero_62() { return &___v3zero_62; }
	inline void set_v3zero_62(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v3zero_62 = value;
	}

	inline static int32_t get_offset_of_m_canvas_63() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_canvas_63)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_63() const { return ___m_canvas_63; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_63() { return &___m_canvas_63; }
	inline void set_m_canvas_63(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_63), value);
	}

	inline static int32_t get_offset_of_camTransform_64() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___camTransform_64)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_camTransform_64() const { return ___camTransform_64; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_camTransform_64() { return &___camTransform_64; }
	inline void set_camTransform_64(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___camTransform_64 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_64), value);
	}

	inline static int32_t get_offset_of_cam3D_65() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___cam3D_65)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam3D_65() const { return ___cam3D_65; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam3D_65() { return &___cam3D_65; }
	inline void set_cam3D_65(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam3D_65 = value;
		Il2CppCodeGenWriteBarrier((&___cam3D_65), value);
	}

	inline static int32_t get_offset_of_oldPosition_66() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldPosition_66)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldPosition_66() const { return ___oldPosition_66; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldPosition_66() { return &___oldPosition_66; }
	inline void set_oldPosition_66(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldPosition_66 = value;
	}

	inline static int32_t get_offset_of_oldRotation_67() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldRotation_67)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldRotation_67() const { return ___oldRotation_67; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldRotation_67() { return &___oldRotation_67; }
	inline void set_oldRotation_67(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldRotation_67 = value;
	}

	inline static int32_t get_offset_of_lineManagerCreated_68() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___lineManagerCreated_68)); }
	inline bool get_lineManagerCreated_68() const { return ___lineManagerCreated_68; }
	inline bool* get_address_of_lineManagerCreated_68() { return &___lineManagerCreated_68; }
	inline void set_lineManagerCreated_68(bool value)
	{
		___lineManagerCreated_68 = value;
	}

	inline static int32_t get_offset_of_m_lineManager_69() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_lineManager_69)); }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * get_m_lineManager_69() const { return ___m_lineManager_69; }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 ** get_address_of_m_lineManager_69() { return &___m_lineManager_69; }
	inline void set_m_lineManager_69(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * value)
	{
		___m_lineManager_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineManager_69), value);
	}

	inline static int32_t get_offset_of_capDictionary_70() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___capDictionary_70)); }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * get_capDictionary_70() const { return ___capDictionary_70; }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D ** get_address_of_capDictionary_70() { return &___capDictionary_70; }
	inline void set_capDictionary_70(Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * value)
	{
		___capDictionary_70 = value;
		Il2CppCodeGenWriteBarrier((&___capDictionary_70), value);
	}

	inline static int32_t get_offset_of_endianDiff1_71() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff1_71)); }
	inline int32_t get_endianDiff1_71() const { return ___endianDiff1_71; }
	inline int32_t* get_address_of_endianDiff1_71() { return &___endianDiff1_71; }
	inline void set_endianDiff1_71(int32_t value)
	{
		___endianDiff1_71 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_72() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff2_72)); }
	inline int32_t get_endianDiff2_72() const { return ___endianDiff2_72; }
	inline int32_t* get_address_of_endianDiff2_72() { return &___endianDiff2_72; }
	inline void set_endianDiff2_72(int32_t value)
	{
		___endianDiff2_72 = value;
	}

	inline static int32_t get_offset_of_byteBlock_73() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___byteBlock_73)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_byteBlock_73() const { return ___byteBlock_73; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_byteBlock_73() { return &___byteBlock_73; }
	inline void set_byteBlock_73(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___byteBlock_73 = value;
		Il2CppCodeGenWriteBarrier((&___byteBlock_73), value);
	}

	inline static int32_t get_offset_of_functionNames_74() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___functionNames_74)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_functionNames_74() const { return ___functionNames_74; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_functionNames_74() { return &___functionNames_74; }
	inline void set_functionNames_74(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___functionNames_74 = value;
		Il2CppCodeGenWriteBarrier((&___functionNames_74), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifndef CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#define CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler
struct  Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#ifndef DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#define DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler
struct  DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#ifndef DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#define DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler
struct  Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#ifndef DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#define DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler
struct  DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#ifndef DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#define DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler
struct  DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#ifndef LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#define LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler
struct  LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#ifndef LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#define LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler
struct  LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#ifndef LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#define LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler
struct  LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#ifndef PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#define PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PinchHandler
struct  PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#ifndef SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#define SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler
struct  SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#ifndef SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#define SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler
struct  Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#ifndef SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#define SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler
struct  SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#ifndef SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#define SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler
struct  SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#ifndef TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#define TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler
struct  TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#ifndef TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#define TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler
struct  TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#ifndef TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#define TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler
struct  TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#ifndef TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#define TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler
struct  TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#ifndef TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#define TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler
struct  TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#ifndef TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#define TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler
struct  TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#ifndef TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#define TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler
struct  TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#ifndef TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#define TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TwistHandler
struct  TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#ifndef GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#define GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Gesture
struct  Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95  : public BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection HedgehogTeam.EasyTouch.Gesture::swipe
	int32_t ___swipe_19;
	// System.Single HedgehogTeam.EasyTouch.Gesture::swipeLength
	float ___swipeLength_20;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Gesture::swipeVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___swipeVector_21;
	// System.Single HedgehogTeam.EasyTouch.Gesture::deltaPinch
	float ___deltaPinch_22;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twistAngle
	float ___twistAngle_23;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twoFingerDistance
	float ___twoFingerDistance_24;
	// HedgehogTeam.EasyTouch.EasyTouch_EvtType HedgehogTeam.EasyTouch.Gesture::type
	int32_t ___type_25;

public:
	inline static int32_t get_offset_of_swipe_19() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipe_19)); }
	inline int32_t get_swipe_19() const { return ___swipe_19; }
	inline int32_t* get_address_of_swipe_19() { return &___swipe_19; }
	inline void set_swipe_19(int32_t value)
	{
		___swipe_19 = value;
	}

	inline static int32_t get_offset_of_swipeLength_20() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipeLength_20)); }
	inline float get_swipeLength_20() const { return ___swipeLength_20; }
	inline float* get_address_of_swipeLength_20() { return &___swipeLength_20; }
	inline void set_swipeLength_20(float value)
	{
		___swipeLength_20 = value;
	}

	inline static int32_t get_offset_of_swipeVector_21() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipeVector_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_swipeVector_21() const { return ___swipeVector_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_swipeVector_21() { return &___swipeVector_21; }
	inline void set_swipeVector_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___swipeVector_21 = value;
	}

	inline static int32_t get_offset_of_deltaPinch_22() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___deltaPinch_22)); }
	inline float get_deltaPinch_22() const { return ___deltaPinch_22; }
	inline float* get_address_of_deltaPinch_22() { return &___deltaPinch_22; }
	inline void set_deltaPinch_22(float value)
	{
		___deltaPinch_22 = value;
	}

	inline static int32_t get_offset_of_twistAngle_23() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___twistAngle_23)); }
	inline float get_twistAngle_23() const { return ___twistAngle_23; }
	inline float* get_address_of_twistAngle_23() { return &___twistAngle_23; }
	inline void set_twistAngle_23(float value)
	{
		___twistAngle_23 = value;
	}

	inline static int32_t get_offset_of_twoFingerDistance_24() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___twoFingerDistance_24)); }
	inline float get_twoFingerDistance_24() const { return ___twoFingerDistance_24; }
	inline float* get_address_of_twoFingerDistance_24() { return &___twoFingerDistance_24; }
	inline void set_twoFingerDistance_24(float value)
	{
		___twoFingerDistance_24 = value;
	}

	inline static int32_t get_offset_of_type_25() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___type_25)); }
	inline int32_t get_type_25() const { return ___type_25; }
	inline int32_t* get_address_of_type_25() { return &___type_25; }
	inline void set_type_25(int32_t value)
	{
		___type_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#ifndef ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#define ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef FILESYSTEMINFO_T6831B76FBA37F7181E4A5AEB28194730EB356A3D_H
#define FILESYSTEMINFO_T6831B76FBA37F7181E4A5AEB28194730EB356A3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.MonoIOStat System.IO.FileSystemInfo::_data
	MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124  ____data_1;
	// System.Int32 System.IO.FileSystemInfo::_dataInitialised
	int32_t ____dataInitialised_2;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_3;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_4;
	// System.String System.IO.FileSystemInfo::_displayPath
	String_t* ____displayPath_5;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D, ____data_1)); }
	inline MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124  get__data_1() const { return ____data_1; }
	inline MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124 * get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(MonoIOStat_t819C03DA1902AA493BDBF4B55E76DCE6FB16A124  value)
	{
		____data_1 = value;
	}

	inline static int32_t get_offset_of__dataInitialised_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D, ____dataInitialised_2)); }
	inline int32_t get__dataInitialised_2() const { return ____dataInitialised_2; }
	inline int32_t* get_address_of__dataInitialised_2() { return &____dataInitialised_2; }
	inline void set__dataInitialised_2(int32_t value)
	{
		____dataInitialised_2 = value;
	}

	inline static int32_t get_offset_of_FullPath_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D, ___FullPath_3)); }
	inline String_t* get_FullPath_3() const { return ___FullPath_3; }
	inline String_t** get_address_of_FullPath_3() { return &___FullPath_3; }
	inline void set_FullPath_3(String_t* value)
	{
		___FullPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_3), value);
	}

	inline static int32_t get_offset_of_OriginalPath_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D, ___OriginalPath_4)); }
	inline String_t* get_OriginalPath_4() const { return ___OriginalPath_4; }
	inline String_t** get_address_of_OriginalPath_4() { return &___OriginalPath_4; }
	inline void set_OriginalPath_4(String_t* value)
	{
		___OriginalPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_4), value);
	}

	inline static int32_t get_offset_of__displayPath_5() { return static_cast<int32_t>(offsetof(FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D, ____displayPath_5)); }
	inline String_t* get__displayPath_5() const { return ____displayPath_5; }
	inline String_t** get_address_of__displayPath_5() { return &____displayPath_5; }
	inline void set__displayPath_5(String_t* value)
	{
		____displayPath_5 = value;
		Il2CppCodeGenWriteBarrier((&____displayPath_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T6831B76FBA37F7181E4A5AEB28194730EB356A3D_H
#ifndef THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#define THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadStart
struct  ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#ifndef TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#define TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#ifndef TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#define TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TexturePreLoadHandle
struct  TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#define RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#ifndef RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#define RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifndef TEXTMESH_T327D0DAFEF431170D8C2882083D442AF4D4A0E4A_H
#define TEXTMESH_T327D0DAFEF431170D8C2882083D442AF4D4A0E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextMesh
struct  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESH_T327D0DAFEF431170D8C2882083D442AF4D4A0E4A_H
#ifndef TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#define TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef FILEINFO_TF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_H
#define FILEINFO_TF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileInfo
struct  FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C  : public FileSystemInfo_t6831B76FBA37F7181E4A5AEB28194730EB356A3D
{
public:
	// System.String System.IO.FileInfo::_name
	String_t* ____name_6;

public:
	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier((&____name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEINFO_TF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_H
#ifndef CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#define CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#define LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginViewController
struct  LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI LoginViewController::loginUserNavTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___loginUserNavTxt_4;
	// UnityEngine.UI.Text LoginViewController::navTxt
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___navTxt_5;
	// UserController LoginViewController::userController
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * ___userController_6;

public:
	inline static int32_t get_offset_of_loginUserNavTxt_4() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___loginUserNavTxt_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_loginUserNavTxt_4() const { return ___loginUserNavTxt_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_loginUserNavTxt_4() { return &___loginUserNavTxt_4; }
	inline void set_loginUserNavTxt_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___loginUserNavTxt_4 = value;
		Il2CppCodeGenWriteBarrier((&___loginUserNavTxt_4), value);
	}

	inline static int32_t get_offset_of_navTxt_5() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___navTxt_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_navTxt_5() const { return ___navTxt_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_navTxt_5() { return &___navTxt_5; }
	inline void set_navTxt_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___navTxt_5 = value;
		Il2CppCodeGenWriteBarrier((&___navTxt_5), value);
	}

	inline static int32_t get_offset_of_userController_6() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___userController_6)); }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * get_userController_6() const { return ___userController_6; }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 ** get_address_of_userController_6() { return &___userController_6; }
	inline void set_userController_6(UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * value)
	{
		___userController_6 = value;
		Il2CppCodeGenWriteBarrier((&___userController_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifndef TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#define TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwistMe
struct  TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwistMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#ifndef TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#define TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoDoubleTapMe
struct  TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#ifndef TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#define TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoDragMe
struct  TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoDragMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Vector3 TwoDragMe::deltaPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deltaPosition_5;
	// UnityEngine.Color TwoDragMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_6;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_deltaPosition_5() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___deltaPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deltaPosition_5() const { return ___deltaPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deltaPosition_5() { return &___deltaPosition_5; }
	inline void set_deltaPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deltaPosition_5 = value;
	}

	inline static int32_t get_offset_of_startColor_6() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___startColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_6() const { return ___startColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_6() { return &___startColor_6; }
	inline void set_startColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#ifndef TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#define TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoLongTapMe
struct  TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoLongTapMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color TwoLongTapMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#ifndef TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#define TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoSwipe
struct  TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject TwoSwipe::trail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trail_4;
	// UnityEngine.UI.Text TwoSwipe::swipeData
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___swipeData_5;

public:
	inline static int32_t get_offset_of_trail_4() { return static_cast<int32_t>(offsetof(TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1, ___trail_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trail_4() const { return ___trail_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trail_4() { return &___trail_4; }
	inline void set_trail_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trail_4 = value;
		Il2CppCodeGenWriteBarrier((&___trail_4), value);
	}

	inline static int32_t get_offset_of_swipeData_5() { return static_cast<int32_t>(offsetof(TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1, ___swipeData_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_swipeData_5() const { return ___swipeData_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_swipeData_5() { return &___swipeData_5; }
	inline void set_swipeData_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___swipeData_5 = value;
		Il2CppCodeGenWriteBarrier((&___swipeData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#ifndef TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#define TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoTapMe
struct  TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#ifndef TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#define TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoTouchMe
struct  TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoTouchMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color TwoTouchMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#ifndef UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#define UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICompatibility
struct  UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#ifndef UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#define UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrag
struct  UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UIDrag::fingerId
	int32_t ___fingerId_4;
	// System.Boolean UIDrag::drag
	bool ___drag_5;

public:
	inline static int32_t get_offset_of_fingerId_4() { return static_cast<int32_t>(offsetof(UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D, ___fingerId_4)); }
	inline int32_t get_fingerId_4() const { return ___fingerId_4; }
	inline int32_t* get_address_of_fingerId_4() { return &___fingerId_4; }
	inline void set_fingerId_4(int32_t value)
	{
		___fingerId_4 = value;
	}

	inline static int32_t get_offset_of_drag_5() { return static_cast<int32_t>(offsetof(UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D, ___drag_5)); }
	inline bool get_drag_5() const { return ___drag_5; }
	inline bool* get_address_of_drag_5() { return &___drag_5; }
	inline void set_drag_5(bool value)
	{
		___drag_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#ifndef UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#define UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPinch
struct  UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#ifndef UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#define UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITwist
struct  UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#ifndef UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#define UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWindow
struct  UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#ifndef UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#define UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniformTexturedLine
struct  UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture UniformTexturedLine::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Single UniformTexturedLine::lineWidth
	float ___lineWidth_5;
	// System.Single UniformTexturedLine::textureScale
	float ___textureScale_6;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_lineWidth_5() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___lineWidth_5)); }
	inline float get_lineWidth_5() const { return ___lineWidth_5; }
	inline float* get_address_of_lineWidth_5() { return &___lineWidth_5; }
	inline void set_lineWidth_5(float value)
	{
		___lineWidth_5 = value;
	}

	inline static int32_t get_offset_of_textureScale_6() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___textureScale_6)); }
	inline float get_textureScale_6() const { return ___textureScale_6; }
	inline float* get_address_of_textureScale_6() { return &___textureScale_6; }
	inline void set_textureScale_6(float value)
	{
		___textureScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#define POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_4;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_5;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_6;

public:
	inline static int32_t get_offset_of_supportHDRTextures_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportHDRTextures_4)); }
	inline bool get_supportHDRTextures_4() const { return ___supportHDRTextures_4; }
	inline bool* get_address_of_supportHDRTextures_4() { return &___supportHDRTextures_4; }
	inline void set_supportHDRTextures_4(bool value)
	{
		___supportHDRTextures_4 = value;
	}

	inline static int32_t get_offset_of_supportDX11_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportDX11_5)); }
	inline bool get_supportDX11_5() const { return ___supportDX11_5; }
	inline bool* get_address_of_supportDX11_5() { return &___supportDX11_5; }
	inline void set_supportDX11_5(bool value)
	{
		___supportDX11_5 = value;
	}

	inline static int32_t get_offset_of_isSupported_6() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___isSupported_6)); }
	inline bool get_isSupported_6() const { return ___isSupported_6; }
	inline bool* get_address_of_isSupported_6() { return &___isSupported_6; }
	inline void set_isSupported_6(bool value)
	{
		___isSupported_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#ifndef USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#define USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserController
struct  UserController_tFD917E07B535D607AFD7056993FB736FD366E600  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.User UserController::user
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___user_4;
	// System.String UserController::fileNameForUser
	String_t* ___fileNameForUser_5;
	// System.Boolean UserController::userLoggedIn
	bool ___userLoggedIn_6;

public:
	inline static int32_t get_offset_of_user_4() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___user_4)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_user_4() const { return ___user_4; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_user_4() { return &___user_4; }
	inline void set_user_4(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___user_4 = value;
		Il2CppCodeGenWriteBarrier((&___user_4), value);
	}

	inline static int32_t get_offset_of_fileNameForUser_5() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___fileNameForUser_5)); }
	inline String_t* get_fileNameForUser_5() const { return ___fileNameForUser_5; }
	inline String_t** get_address_of_fileNameForUser_5() { return &___fileNameForUser_5; }
	inline void set_fileNameForUser_5(String_t* value)
	{
		___fileNameForUser_5 = value;
		Il2CppCodeGenWriteBarrier((&___fileNameForUser_5), value);
	}

	inline static int32_t get_offset_of_userLoggedIn_6() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___userLoggedIn_6)); }
	inline bool get_userLoggedIn_6() const { return ___userLoggedIn_6; }
	inline bool* get_address_of_userLoggedIn_6() { return &___userLoggedIn_6; }
	inline void set_userLoggedIn_6(bool value)
	{
		___userLoggedIn_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#ifndef VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#define VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorObject
struct  VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// VectorObject_Shape VectorObject::shape
	int32_t ___shape_4;

public:
	inline static int32_t get_offset_of_shape_4() { return static_cast<int32_t>(offsetof(VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B, ___shape_4)); }
	inline int32_t get_shape_4() const { return ___shape_4; }
	inline int32_t* get_address_of_shape_4() { return &___shape_4; }
	inline void set_shape_4(int32_t value)
	{
		___shape_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#ifndef XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H
#define XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XrayLineData
struct  XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture XrayLineData::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_5;
	// System.Single XrayLineData::lineWidth
	float ___lineWidth_6;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>> XrayLineData::shapePoints
	List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * ___shapePoints_7;

public:
	inline static int32_t get_offset_of_lineTexture_5() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___lineTexture_5)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_5() const { return ___lineTexture_5; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_5() { return &___lineTexture_5; }
	inline void set_lineTexture_5(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_5), value);
	}

	inline static int32_t get_offset_of_lineWidth_6() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___lineWidth_6)); }
	inline float get_lineWidth_6() const { return ___lineWidth_6; }
	inline float* get_address_of_lineWidth_6() { return &___lineWidth_6; }
	inline void set_lineWidth_6(float value)
	{
		___lineWidth_6 = value;
	}

	inline static int32_t get_offset_of_shapePoints_7() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___shapePoints_7)); }
	inline List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * get_shapePoints_7() const { return ___shapePoints_7; }
	inline List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C ** get_address_of_shapePoints_7() { return &___shapePoints_7; }
	inline void set_shapePoints_7(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * value)
	{
		___shapePoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___shapePoints_7), value);
	}
};

struct XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields
{
public:
	// XrayLineData XrayLineData::use
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * ___use_4;

public:
	inline static int32_t get_offset_of_use_4() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields, ___use_4)); }
	inline XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * get_use_4() const { return ___use_4; }
	inline XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 ** get_address_of_use_4() { return &___use_4; }
	inline void set_use_4(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * value)
	{
		___use_4 = value;
		Il2CppCodeGenWriteBarrier((&___use_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H
#define BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized
struct  BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730  : public PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E
{
public:
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::threshold
	float ___threshold_7;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::intensity
	float ___intensity_8;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::blurSize
	float ___blurSize_9;
	// UnityStandardAssets.ImageEffects.BloomOptimized_Resolution UnityStandardAssets.ImageEffects.BloomOptimized::resolution
	int32_t ___resolution_10;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized::blurIterations
	int32_t ___blurIterations_11;
	// UnityStandardAssets.ImageEffects.BloomOptimized_BlurType UnityStandardAssets.ImageEffects.BloomOptimized::blurType
	int32_t ___blurType_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___fastBloomShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fastBloomMaterial_14;

public:
	inline static int32_t get_offset_of_threshold_7() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___threshold_7)); }
	inline float get_threshold_7() const { return ___threshold_7; }
	inline float* get_address_of_threshold_7() { return &___threshold_7; }
	inline void set_threshold_7(float value)
	{
		___threshold_7 = value;
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___intensity_8)); }
	inline float get_intensity_8() const { return ___intensity_8; }
	inline float* get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(float value)
	{
		___intensity_8 = value;
	}

	inline static int32_t get_offset_of_blurSize_9() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurSize_9)); }
	inline float get_blurSize_9() const { return ___blurSize_9; }
	inline float* get_address_of_blurSize_9() { return &___blurSize_9; }
	inline void set_blurSize_9(float value)
	{
		___blurSize_9 = value;
	}

	inline static int32_t get_offset_of_resolution_10() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___resolution_10)); }
	inline int32_t get_resolution_10() const { return ___resolution_10; }
	inline int32_t* get_address_of_resolution_10() { return &___resolution_10; }
	inline void set_resolution_10(int32_t value)
	{
		___resolution_10 = value;
	}

	inline static int32_t get_offset_of_blurIterations_11() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurIterations_11)); }
	inline int32_t get_blurIterations_11() const { return ___blurIterations_11; }
	inline int32_t* get_address_of_blurIterations_11() { return &___blurIterations_11; }
	inline void set_blurIterations_11(int32_t value)
	{
		___blurIterations_11 = value;
	}

	inline static int32_t get_offset_of_blurType_12() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurType_12)); }
	inline int32_t get_blurType_12() const { return ___blurType_12; }
	inline int32_t* get_address_of_blurType_12() { return &___blurType_12; }
	inline void set_blurType_12(int32_t value)
	{
		___blurType_12 = value;
	}

	inline static int32_t get_offset_of_fastBloomShader_13() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___fastBloomShader_13)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_fastBloomShader_13() const { return ___fastBloomShader_13; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_fastBloomShader_13() { return &___fastBloomShader_13; }
	inline void set_fastBloomShader_13(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___fastBloomShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomShader_13), value);
	}

	inline static int32_t get_offset_of_fastBloomMaterial_14() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___fastBloomMaterial_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fastBloomMaterial_14() const { return ___fastBloomMaterial_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fastBloomMaterial_14() { return &___fastBloomMaterial_14; }
	inline void set_fastBloomMaterial_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fastBloomMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * m_Items[1];

public:
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m0A52F3C5B5FF663BE30EF619B4D961185A4C97EC_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared (String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared (const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void TriLib.ThreadUtils/<>c__DisplayClass0_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0__ctor_m8B63335104308B697201A3702FDEEB14232A0056 (U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * __this, const RuntimeMethod* method);
// System.Void TriLib.Dispatcher::CheckInstance()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_CheckInstance_m5F5199CD32ACB7257AE6D13D5218521893B9CFAD (const RuntimeMethod* method);
// System.Void System.Threading.ThreadStart::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ThreadStart__ctor_m692348FEAEBAF381D62984EE95B217CC024A77D5 (ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart)
extern "C" IL2CPP_METHOD_ATTR void Thread__ctor_m36A33B990160C4499E991D288341CA325AE66DDD (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * __this, ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * p0, const RuntimeMethod* method);
// System.Void System.Threading.Thread::Start()
extern "C" IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void Thread_Start_mE2AC4744AFD147FDC84E8D9317B4E3AB890BC2D6 (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// System.Void TriLib.Dispatcher::InvokeAsync(System.Action)
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_InvokeAsync_mF7879C7EAEDF1DD3DE9C64B0EF6F307744682D6F (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method);
// System.Void TriLib.ThreadUtils/<>c__DisplayClass0_1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_1__ctor_mF2D994C009AA1D8B0D0970BA5EB93503A29F4174 (U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractScale(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractScale_m968F89B9F513DA0ECED2073AE487B136AC47AB0E (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion TriLib.MatrixExtensions::ExtractRotation(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  MatrixExtensions_ExtractRotation_m154F673DD3D41FDDC1959A284D16D9850F1D8286 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractPosition(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractPosition_mC44BA529F7D22D71919EBE31AEF81BDB99E7E6F9 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>()
inline RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mFF06011DFE2A1EC6DFC8FF1C1E78EF60CA07E9D9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared)(__this, method);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR void Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  p0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Boolean System.String::EndsWith(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_EndsWith_mE4F039DCC2A9FCB8C1ED2D04B00A35E3CE16DE99 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * TransformExtensions_FindDeepChild_mD574BD864794563C7ED11DE03BA4E83450E0F8B1 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_GetChild_mC86B9B61E4EC086A571B09EA7A33FFBF50DF52D3 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858 (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0 (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TwistHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62 (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Twist(HedgehogTeam.EasyTouch.EasyTouch/TwistHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_Twist_mE80E52A3010E98705B3319F87DDC34706601239A (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TwistEndHandler__ctor_m23A0EAEC768D7D9F26492DE6F184B39A4BF38DDE (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TwistEnd_m46230202EA91287F5E52C55CDD43F0611B8C0166 (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0 (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * ___value0, const RuntimeMethod* method);
// System.Void TwistMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Twist(HedgehogTeam.EasyTouch.EasyTouch/TwistHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_Twist_mF6873B2804131E986AC0C40F390EC4E487A466D5 (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TwistEnd_m27D30CE3446F715A85B737E2F6B9825D43DEB8C7 (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335 (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.TextMesh>()
inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m0A52F3C5B5FF663BE30EF619B4D961185A4C97EC_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnableTwist(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_SetEnableTwist_m5F025CF421BE62026A1836F9122901D3AB90CC6D (bool ___enable0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnablePinch(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102 (bool ___enable0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_Rotate_m610B6793DCC2F987290D328942E649B5B7DE0F9A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.String System.Single::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3 (float* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DoubleTap2FingersHandler__ctor_mCF4A990BD0889F2497F5FD37DB1478EE8D5B7CC6 (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_DoubleTap2Fingers_mA847C67B0A30C81BC9A12D4FD3046C8F094047E1 (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * ___value0, const RuntimeMethod* method);
// System.Void TwoDoubleTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657 (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_DoubleTap2Fingers_m4D6E01A1404E74A7C937B777B442320A9AD139BD (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384 (float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DragStart2FingersHandler__ctor_m96F1FEE595DC4B38964C2CE9FFBF2D4577802249 (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_DragStart2Fingers_mDE91C6A5A0E98EF2EFCB655FAE2FBAA27DA3BB17 (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Drag2FingersHandler__ctor_mE97E75C61AC4B4C0A4AD34A67B30259DD0EAF444 (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_Drag2Fingers_mA7EB90CBF64A6EE2D985AC3DB16795DAD80E5113 (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DragEnd2FingersHandler__ctor_mE400CA66F2292D2F12A1A5B67E79ABC72ABE16D7 (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_DragEnd2Fingers_mE2E7397978E3B634279BDC927E61CB9B92887B0F (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * ___value0, const RuntimeMethod* method);
// System.Void TwoDragMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_DragStart2Fingers_mEBEF7A5C1788FB0D372C873EE7D3CD04C514689E (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_Drag2Fingers_mC6D908A3893EF1F9E966C8AA502FD786B859D8C2 (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_DragEnd2Fingers_m36A33E27ABD55E19913DA5C8FB24BA6CE4DE00D4 (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Material::get_color()
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, const RuntimeMethod* method);
// System.Void TwoDragMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Gesture_GetTouchToWorldPoint_m976B9A97EA151D764514B372966D71E653072BB4 (Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position3D0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Single HedgehogTeam.EasyTouch.Gesture::GetSwipeOrDragAngle()
extern "C" IL2CPP_METHOD_ATTR float Gesture_GetSwipeOrDragAngle_m2F365EE39BDBCE136ABD4084BFA17E62AD9EEF85 (Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * __this, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE (float* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void TwoDragMe::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void LongTapStart2FingersHandler__ctor_mCEAE610D6ACD375506E88B77B757791DB6E9ECBF (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_LongTapStart2Fingers_mD6EAB281C23078CDBB179CF030DF06364730DCA2 (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void LongTap2FingersHandler__ctor_m4962BF8633C3CBAE28F1D75AE74CC7329E59CFF8 (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_LongTap2Fingers_mEEBDCAA62C01A97A50392545C95A826662D0B109 (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void LongTapEnd2FingersHandler__ctor_m855897087EE65EFC27D178330E0CD77AEB5A08F9 (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_LongTapEnd2Fingers_mD897971A4E5DA92088145FF69C32BCA7C5F16BE5 (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * ___value0, const RuntimeMethod* method);
// System.Void TwoLongTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_LongTapStart2Fingers_mC01DA2731180F0C2282F828900820B9DA907D1AB (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_LongTap2Fingers_m45F4803E90895FAE4523272CA320188C07360A06 (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_LongTapEnd2Fingers_m3BCFD86E017A40A1E7CF6F187EC582A0402428CF (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * ___value0, const RuntimeMethod* method);
// System.Void TwoLongTapMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method);
// System.Void TwoLongTapMe::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void SwipeStart2FingersHandler__ctor_m4316083F72EA7135ACFFAFE6896C4743941B5AB2 (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_SwipeStart2Fingers_mD066B4DF4DED0C2A5ECDC0B63E2E23F85970DF6B (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Swipe2FingersHandler__ctor_m041539F2580BD4669C44668741A7506D0745A5F9 (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_Swipe2Fingers_m29F2B727DF6BB6F5DA51998D5F391A46CAB590AC (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void SwipeEnd2FingersHandler__ctor_m7CF9535201124466FC7577CB425378F9A359BB92 (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_SwipeEnd2Fingers_mACCBCCA09EE9D34F296C454A4A7BB284E623A456 (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * ___value0, const RuntimeMethod* method);
// System.Void TwoSwipe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_SwipeStart2Fingers_mC014048396BCDB5E414C8CB51E2FAE989CD3FCDA (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_Swipe2Fingers_mE50D63E2CB409D988CF47532116E799801F7C753 (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_SwipeEnd2Fingers_m52A6FE0854E1376170C0128AD1240929AF3CE3AD (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Gesture_GetTouchToWorldPoint_mCEBE61ED58EC849C47A2C093A7BD71DA650BE72F (Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * __this, float ___z0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void SimpleTap2FingersHandler__ctor_m8AA13BBCDA12B5701474963698625B52D828586D (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_SimpleTap2Fingers_mC19305DAB55E6BCFB82C8FB6E4123255B82E7738 (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * ___value0, const RuntimeMethod* method);
// System.Void TwoTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_SimpleTap2Fingers_m1FF34C9A9AEDAAD4145E7DE230A27009F229D680 (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * ___value0, const RuntimeMethod* method);
// System.Void TwoTapMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2 (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchDown2Fingers_mCEC9C49CDAC504B09D7E9BCDE555469C3DC7EBF8 (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6 (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchUp2Fingers_m039A663F83056984EA1F7C82CE246C3040CF6556 (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * ___value0, const RuntimeMethod* method);
// System.Void TwoTouchMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchDown2Fingers_m8FC281BAD18B3C3C2B9A87424BB7573424CF122A (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchUp2Fingers_m6E4F3B68FA23F90751EC9D4E5E90FF58ED717122 (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * ___value0, const RuntimeMethod* method);
// System.Void TwoTouchMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method);
// System.Void TwoTouchMe::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetUICompatibily(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_SetUICompatibily_m9DC3C352B33E7DF4B4DAED2FB5C49D6AEA94551E (bool ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchDownHandler__ctor_m266AB85478489C46AE8B9803142E973BB9679DA8 (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchDown_m2D2D5E25BB869F6A6B394F3566EEBE4BAD522EEC (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchStartHandler__ctor_m8B03D7113759D0BA11A017A3766C383F16B8D268 (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchStart_m97F8CDF7FB22E6FB7515E5BB618C41D675A03BFA (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TouchUpHandler__ctor_mFCAF0BE55F280AB1DD20F6AC020DE2E3B4A1E85F (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_TouchUp_mA1891CE45733A36181535ABFE611B6FFC194FC6A (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchDown_m700E4BBE8FFA99A81B81C8E89F213F1EC410A19C (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchStart_mC872F67C12E8BEC695AF6214936B10D6BB8AA8DC (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_TouchUp_m0BBFA87933DF34D98AB73029DE067840F76F0E03 (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR bool Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetAsLastSibling()
extern "C" IL2CPP_METHOD_ATTR void Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch/PinchHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PinchHandler__ctor_m56455C5FFAECE920632FE4C6D67A884CADDA6B03 (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch/PinchHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_add_On_Pinch_m3AB86DB5A8558B4BA19968EE71452F82CACA6352 (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * ___value0, const RuntimeMethod* method);
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch/PinchHandler)
extern "C" IL2CPP_METHOD_ATTR void EasyTouch_remove_On_Pinch_m0144B1E20FF8546AFF793BBDF6794007FD2E4BE1 (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_delta()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  PointerEventData_get_delta_m70E69337BC484503C037D1D292C37E6926B6F3A7 (PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
inline void List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37_gshared)(__this, method);
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
inline void List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*))List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_gshared)(__this, p0, method);
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Texture,System.Single)
extern "C" IL2CPP_METHOD_ATTR void VectorLine__ctor_mA5F79C0A5CC77F5EDEFCBB9F5C51179077BF6467 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, String_t* p0, List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * p1, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p2, float p3, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::set_textureScale(System.Single)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_set_textureScale_mEE94B92E3502B3354ABD562ED1BF43E88BCAAC46 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, float p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Draw()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, bool ___needDepth0, const RuntimeMethod* method);
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___s0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m2Create1, const RuntimeMethod* method);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C" IL2CPP_METHOD_ATTR void Material_SetVector_m6FC2CC4EBE6C45D48D8B9164148A0CB3124335EC (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* p0, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern "C" IL2CPP_METHOD_ATTR int32_t RenderTexture_get_format_mC500BCC10B2A6D6808645B505DB510056516D1FF (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * __this, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C" IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * RenderTexture_GetTemporary_m6E0EF85D2DEC0626DE5BB5D008A659F1CD66D9F8 (int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * p2, int32_t p3, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* p0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p1, const RuntimeMethod* method);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase__ctor_m440C9B609EF88230A2EB266FD3E6C624431E1368 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C" IL2CPP_METHOD_ATTR bool Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D (Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * __this, const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C" IL2CPP_METHOD_ATTR Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, const RuntimeMethod* method);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m232E857CA5107EA6AC52E7DD7018716C021F237B (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C" IL2CPP_METHOD_ATTR void Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" IL2CPP_METHOD_ATTR void Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C" IL2CPP_METHOD_ATTR bool SystemInfo_SupportsRenderTextureFormat_m74D259714A97501D28951CA48298D9F0AE3B5907 (int32_t p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C" IL2CPP_METHOD_ATTR int32_t SystemInfo_get_graphicsShaderLevel_m04540C22D983AE52CBCC6E1026D16F0C548248EB (const RuntimeMethod* method);
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C" IL2CPP_METHOD_ATTR bool SystemInfo_get_supportsComputeShaders_m257AEF98322259041D3358B801FD6F34DB13013F (const RuntimeMethod* method);
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C" IL2CPP_METHOD_ATTR bool SystemInfo_get_supportsImageEffects_m5606438D404910FADC9C50DC29E0649E49B08267 (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Camera_get_depthTextureMode_m157C12D349137B72F27061C027E6954EC65D54AD (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C" IL2CPP_METHOD_ATTR void Camera_set_depthTextureMode_mBD8E259A3E29C4A5AC1FA8898700789B43264D2C (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1 (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GL::PushMatrix()
extern "C" IL2CPP_METHOD_ATTR void GL_PushMatrix_mE47A23F3A906899E88AC525FFE2C3C2BD834DFF9 (const RuntimeMethod* method);
// System.Void UnityEngine.GL::LoadOrtho()
extern "C" IL2CPP_METHOD_ATTR void GL_LoadOrtho_m6139139E4CFF2CA0CD0DF91D4B578D13E253DD9C (const RuntimeMethod* method);
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GL_Begin_m9A48BD6A2DA850D54250EF638DF5EC61F83E293C (int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF (float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E (float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.GL::End()
extern "C" IL2CPP_METHOD_ATTR void GL_End_m7EDEB843BD9F7E00BD838FDE074B4688C55C0755 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Material::get_passCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Material_get_passCount_m790A357C17FB4D829C479652B1B65BC15F73FBB1 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GL::PopMatrix()
extern "C" IL2CPP_METHOD_ATTR void GL_PopMatrix_mCAA6BC17D97358A4BC329E789AF2CA26C1204112 (const RuntimeMethod* method);
// System.Void UserController::LoadUserFromLocalFile()
extern "C" IL2CPP_METHOD_ATTR void UserController_LoadUserFromLocalFile_m503330B8722440718126146EA03DF95D972004AE (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<Strackaline.User>(System.String)
inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A (String_t* p0, const RuntimeMethod* method)
{
	return ((  User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared)(p0, method);
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B (const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllText(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void File_WriteAllText_m7BA355E5631C6A3E3D3378D6101EF65E72A45F0A (String_t* p0, String_t* p1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<LoginViewController>()
inline LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962 (const RuntimeMethod* method)
{
	return ((  LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared)(method);
}
// System.Void LoginViewController::UpdateNavWithUserData()
extern "C" IL2CPP_METHOD_ATTR void LoginViewController_UpdateNavWithUserData_m06786D1F38AD2260231904FC325400C19E918E9D (LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * __this, const RuntimeMethod* method);
// System.Void System.IO.FileInfo::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void FileInfo__ctor_m77D19A494A542C924C36FDD8AE5CDBEA97CE68B8 (FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
extern "C" IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* p0, const RuntimeMethod* method);
// System.String System.IO.File::ReadAllText(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF (String_t* p0, const RuntimeMethod* method);
// System.Void LoginViewController::SetNavBasedOnLocalUserSaved()
extern "C" IL2CPP_METHOD_ATTR void LoginViewController_SetNavBasedOnLocalUserSaved_mC5543F8E4774C7A0D42893E7C318D5EBD719BE35 (LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * __this, const RuntimeMethod* method);
// System.Void System.IO.File::Delete(System.String)
extern "C" IL2CPP_METHOD_ATTR void File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2 (String_t* p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_Item(System.Int32)
inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * List_1_get_Item_m2C4E9711CA5C59D4FA95D74A13D54C8EF72E55BF (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * (*) (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Texture,System.Single)
extern "C" IL2CPP_METHOD_ATTR void VectorLine__ctor_mFF68E3BA7BA26A321AD1BA25D70A07DDADACB67F (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, String_t* p0, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * p1, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p2, float p3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0 (const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::set_color(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_set_color_mA5BB3AE242CC3B03E53243380D8662B392538ED0 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_mBE9DB9235D4177C91832964336FC68925FEDF55C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * p0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor()
inline void List_1__ctor_m85AD1D8D83D26FF89816DBA34C37F77BA3B35518 (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , const RuntimeMethod*))List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Add(!0)
inline void List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2 (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * __this, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C *, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TextureLoadHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle__ctor_m4A5A97816456AD3D38E4FB787C9FF08B1FF48648 (TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.TextureLoadHandle::Invoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_Invoke_m6ACC46BAA0AB494ABA0BD4A156CE391870D7802D (TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * __this, String_t* ___sourcePath0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, String_t* ___propertyName2, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture3, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
			else if (___parameterCount != 4)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								GenericVirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								VirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								GenericVirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								VirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							GenericVirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							VirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							GenericVirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							VirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.TextureLoadHandle::BeginInvoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TextureLoadHandle_BeginInvoke_m983BEC7DA03BDB695A3CC95EEE6AB823B9726D25 (TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * __this, String_t* ___sourcePath0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, String_t* ___propertyName2, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___sourcePath0;
	__d_args[1] = ___material1;
	__d_args[2] = ___propertyName2;
	__d_args[3] = ___texture3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void TriLib.TextureLoadHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_EndInvoke_m2FA9B2CC1BDAF21D2FF705CE11799B0999C5B37F (TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TexturePreLoadHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle__ctor_mDDBED448E76F5F62E23ABACC2F215635B9A453CF (TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.TexturePreLoadHandle::Invoke(System.IntPtr,System.String,System.String,UnityEngine.Material,System.String,System.BooleanU26,UnityEngine.TextureWrapMode,System.String,TriLib.TextureLoadHandle,TriLib.TextureCompression,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle_Invoke_m673695CBD0871B256B5B73D0DEF56E58600BD4E5 (TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24 * __this, intptr_t ___scene0, String_t* ___path1, String_t* ___name2, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material3, String_t* ___propertyName4, bool* ___checkAlphaChannel5, int32_t ___textureWrapMode6, String_t* ___basePath7, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * ___onTextureLoaded8, int32_t ___textureCompression9, bool ___isNormalMap10, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 11)
				{
					// open
					typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
							else
								GenericVirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
							else
								VirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 11)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						else
							GenericVirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						else
							VirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.TexturePreLoadHandle::BeginInvoke(System.IntPtr,System.String,System.String,UnityEngine.Material,System.String,System.BooleanU26,UnityEngine.TextureWrapMode,System.String,TriLib.TextureLoadHandle,TriLib.TextureCompression,System.Boolean,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TexturePreLoadHandle_BeginInvoke_m3F82EF8D6D50FAA81FAA5EF7508BDC32CEEBD034 (TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24 * __this, intptr_t ___scene0, String_t* ___path1, String_t* ___name2, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material3, String_t* ___propertyName4, bool* ___checkAlphaChannel5, int32_t ___textureWrapMode6, String_t* ___basePath7, TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * ___onTextureLoaded8, int32_t ___textureCompression9, bool ___isNormalMap10, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback11, RuntimeObject * ___object12, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TexturePreLoadHandle_BeginInvoke_m3F82EF8D6D50FAA81FAA5EF7508BDC32CEEBD034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[12] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___scene0);
	__d_args[1] = ___path1;
	__d_args[2] = ___name2;
	__d_args[3] = ___material3;
	__d_args[4] = ___propertyName4;
	__d_args[5] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &*___checkAlphaChannel5);
	__d_args[6] = Box(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F_il2cpp_TypeInfo_var, &___textureWrapMode6);
	__d_args[7] = ___basePath7;
	__d_args[8] = ___onTextureLoaded8;
	__d_args[9] = Box(TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5_il2cpp_TypeInfo_var, &___textureCompression9);
	__d_args[10] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &___isNormalMap10);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback11, (RuntimeObject*)___object12);
}
// System.Void TriLib.TexturePreLoadHandle::EndInvoke(System.BooleanU26,System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle_EndInvoke_mCA9E0B4C3AA6F65873E2B79C0693FA9CE08F840B (TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24 * __this, bool* ___checkAlphaChannel0, RuntimeObject* ___result1, const RuntimeMethod* method)
{
	void* ___out_args[] = {
	___checkAlphaChannel0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Threading.Thread TriLib.ThreadUtils::RunThread(System.Action,System.Action)
extern "C" IL2CPP_METHOD_ATTR Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ThreadUtils_RunThread_m037EF6B6DA34BC8BC277B0A778E128968E9727DE (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadUtils_RunThread_m037EF6B6DA34BC8BC277B0A778E128968E9727DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * L_0 = (U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_m8B63335104308B697201A3702FDEEB14232A0056(L_0, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * L_1 = L_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * L_3 = L_1;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_4 = ___onComplete1;
		NullCheck(L_3);
		L_3->set_onComplete_1(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_il2cpp_TypeInfo_var);
		Dispatcher_CheckInstance_m5F5199CD32ACB7257AE6D13D5218521893B9CFAD(/*hidden argument*/NULL);
		ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * L_5 = (ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF *)il2cpp_codegen_object_new(ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF_il2cpp_TypeInfo_var);
		ThreadStart__ctor_m692348FEAEBAF381D62984EE95B217CC024A77D5(L_5, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass0_0_U3CRunThreadU3Eb__0_m7868462EB15600B4492B50FA3EEFC5A006E2C082_RuntimeMethod_var), /*hidden argument*/NULL);
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_6 = (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)il2cpp_codegen_object_new(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_il2cpp_TypeInfo_var);
		Thread__ctor_m36A33B990160C4499E991D288341CA325AE66DDD(L_6, L_5, /*hidden argument*/NULL);
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_7 = L_6;
		NullCheck(L_7);
		Thread_Start_mE2AC4744AFD147FDC84E8D9317B4E3AB890BC2D6(L_7, /*hidden argument*/NULL);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ThreadUtils_<>c__DisplayClass0_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0__ctor_m8B63335104308B697201A3702FDEEB14232A0056 (U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriLib.ThreadUtils_<>c__DisplayClass0_0::<RunThread>b__0()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0_U3CRunThreadU3Eb__0_m7868462EB15600B4492B50FA3EEFC5A006E2C082 (U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass0_0_U3CRunThreadU3Eb__0_m7868462EB15600B4492B50FA3EEFC5A006E2C082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * V_0 = NULL;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_action_0();
		NullCheck(L_0);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_0, /*hidden argument*/NULL);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_onComplete_1();
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_il2cpp_TypeInfo_var);
		Dispatcher_InvokeAsync_mF7879C7EAEDF1DD3DE9C64B0EF6F307744682D6F(L_1, /*hidden argument*/NULL);
		goto IL_0039;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Exception)
		U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * L_2 = (U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_1__ctor_mF2D994C009AA1D8B0D0970BA5EB93503A29F4174(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = ((Exception_t *)__exception_local);
		U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * L_3 = V_0;
		Exception_t * L_4 = V_1;
		NullCheck(L_3);
		L_3->set_exception_0(L_4);
		U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * L_5 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_il2cpp_TypeInfo_var);
		Dispatcher_InvokeAsync_mF7879C7EAEDF1DD3DE9C64B0EF6F307744682D6F(L_6, /*hidden argument*/NULL);
		goto IL_0039;
	} // end catch (depth: 1)

IL_0039:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ThreadUtils_<>c__DisplayClass0_1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_1__ctor_mF2D994C009AA1D8B0D0970BA5EB93503A29F4174 (U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriLib.ThreadUtils_<>c__DisplayClass0_1::<RunThread>b__1()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419 (U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t * L_0 = __this->get_exception_0();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CU3Ec__DisplayClass0_1_U3CRunThreadU3Eb__1_mD8DEC174F10EF2C4A2DD9F51F89BF98CCC317419_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TransformExtensions::LoadMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TransformExtensions_LoadMatrix_mF0707C49852A7C6F5687987A7216435BE73247F3 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix1, bool ___local2, const RuntimeMethod* method)
{
	{
		bool L_0 = ___local2;
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_2 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = MatrixExtensions_ExtractScale_m968F89B9F513DA0ECED2073AE487B136AC47AB0E(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_1, L_3, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_5 = ___matrix1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_6 = MatrixExtensions_ExtractRotation_m154F673DD3D41FDDC1959A284D16D9850F1D8286(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_4, L_6, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_8 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = MatrixExtensions_ExtractPosition_mC44BA529F7D22D71919EBE31AEF81BDB99E7E6F9(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_7, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_11 = ___matrix1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = MatrixExtensions_ExtractRotation_m154F673DD3D41FDDC1959A284D16D9850F1D8286(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_10, L_12, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_14 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = MatrixExtensions_ExtractPosition_mC44BA529F7D22D71919EBE31AEF81BDB99E7E6F9(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Bounds TriLib.TransformExtensions::EncapsulateBounds(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  TransformExtensions_EncapsulateBounds_m829059F264548DCC07D3CA2773E12C4B03421C4F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_EncapsulateBounds_m829059F264548DCC07D3CA2773E12C4B03421C4F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* V_1 = NULL;
	int32_t V_2 = 0;
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * V_3 = NULL;
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		NullCheck(L_0);
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_1 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mFF06011DFE2A1EC6DFC8FF1C1E78EF60CA07E9D9(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mFF06011DFE2A1EC6DFC8FF1C1E78EF60CA07E9D9_RuntimeMethod_var);
		V_1 = L_1;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_2 = V_1;
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_3 = V_1;
		NullCheck(L_3);
		if (!(((RuntimeArray *)L_3)->max_length))
		{
			goto IL_0038;
		}
	}
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = 0;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_7 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		V_2 = 1;
		goto IL_0030;
	}

IL_001b:
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_12 = V_3;
		NullCheck(L_12);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_13 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_12, /*hidden argument*/NULL);
		Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0030:
	{
		int32_t L_15 = V_2;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0040;
	}

IL_0038:
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 ));
	}

IL_0040:
	{
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * TransformExtensions_FindDeepChild_mD574BD864794563C7ED11DE03BA4E83450E0F8B1 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_FindDeepChild_mD574BD864794563C7ED11DE03BA4E83450E0F8B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_1 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	bool G_B3_0 = false;
	{
		bool L_0 = ___endsWith2;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = ___transform0;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___name1;
		NullCheck(L_2);
		bool L_4 = String_EndsWith_mE4F039DCC2A9FCB8C1ED2D04B00A35E3CE16DE99(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001d;
	}

IL_0011:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = ___transform0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___name1;
		bool L_8 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_001d:
	{
		if (!G_B3_0)
		{
			goto IL_0021;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = ___transform0;
		return L_9;
	}

IL_0021:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___transform0;
		NullCheck(L_10);
		RuntimeObject* L_11 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_002a:
		{
			RuntimeObject* L_12 = V_0;
			NullCheck(L_12);
			RuntimeObject * L_13 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_12);
			String_t* L_14 = ___name1;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = TransformExtensions_FindDeepChild_mD574BD864794563C7ED11DE03BA4E83450E0F8B1(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_13, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)), L_14, (bool)0, /*hidden argument*/NULL);
			V_1 = L_15;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_17 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_16, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_004a;
			}
		}

IL_0046:
		{
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = V_1;
			V_2 = L_18;
			IL2CPP_LEAVE(0x67, FINALLY_0054);
		}

IL_004a:
		{
			RuntimeObject* L_19 = V_0;
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_19);
			if (L_20)
			{
				goto IL_002a;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0054);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_21 = V_0;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_21, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_22 = V_3;
			if (!L_22)
			{
				goto IL_0064;
			}
		}

IL_005e:
		{
			RuntimeObject* L_23 = V_3;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_23);
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(84)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0065:
	{
		return (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL;
	}

IL_0067:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = V_2;
		return L_24;
	}
}
// System.Void TriLib.TransformExtensions::DestroyChildren(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TransformExtensions_DestroyChildren_m329CE7E105CDC9B880D01D98872F639B05DD3720 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, bool ___destroyImmediate1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_DestroyChildren_m329CE7E105CDC9B880D01D98872F639B05DD3720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_1 = NULL;
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724(L_0, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_0032;
	}

IL_000b:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = ___transform0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Transform_GetChild_mC86B9B61E4EC086A571B09EA7A33FFBF50DF52D3(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = ___destroyImmediate1;
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = V_1;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_7, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0023:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = V_1;
		NullCheck(L_8);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_9, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)1));
	}

IL_0032:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwistMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_OnEnable_mC088E661BE5C529F317B51AA0B78AEC4ACFCB869 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_OnEnable_mC088E661BE5C529F317B51AA0B78AEC4ACFCB869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_0 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_0, __this, (intptr_t)((intptr_t)TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0(L_0, /*hidden argument*/NULL);
		TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * L_1 = (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D *)il2cpp_codegen_object_new(TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62(L_1, __this, (intptr_t)((intptr_t)TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Twist_mE80E52A3010E98705B3319F87DDC34706601239A(L_1, /*hidden argument*/NULL);
		TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * L_2 = (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 *)il2cpp_codegen_object_new(TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929_il2cpp_TypeInfo_var);
		TwistEndHandler__ctor_m23A0EAEC768D7D9F26492DE6F184B39A4BF38DDE(L_2, __this, (intptr_t)((intptr_t)TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TwistEnd_m46230202EA91287F5E52C55CDD43F0611B8C0166(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_OnDisable_mD7F7DDD38EF945CB620ED4DFC10A349530986022 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	{
		TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_OnDestroy_mB8A5F5FEF51E82322ECEF08B1B82E3967062DB3E (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	{
		TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_0 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_0, __this, (intptr_t)((intptr_t)TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F(L_0, /*hidden argument*/NULL);
		TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * L_1 = (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D *)il2cpp_codegen_object_new(TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62(L_1, __this, (intptr_t)((intptr_t)TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Twist_mF6873B2804131E986AC0C40F390EC4E487A466D5(L_1, /*hidden argument*/NULL);
		TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * L_2 = (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 *)il2cpp_codegen_object_new(TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929_il2cpp_TypeInfo_var);
		TwistEndHandler__ctor_m23A0EAEC768D7D9F26492DE6F184B39A4BF38DDE(L_2, __this, (intptr_t)((intptr_t)TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TwistEnd_m27D30CE3446F715A85B737E2F6B9825D43DEB8C7(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::Start()
extern "C" IL2CPP_METHOD_ATTR void TwistMe_Start_mC6A0F64168BF753E5AE9E3FB1D020E5FE655EB68 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_Start_mC6A0F64168BF753E5AE9E3FB1D020E5FE655EB68_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_0 = Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699_RuntimeMethod_var);
		__this->set_textMesh_4(L_0);
		return;
	}
}
// System.Void TwistMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		EasyTouch_SetEnableTwist_m5F025CF421BE62026A1836F9122901D3AB90CC6D((bool)1, /*hidden argument*/NULL);
		EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102((bool)0, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void TwistMe::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0053;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_5 = ___gesture0;
		NullCheck(L_5);
		float L_6 = L_5->get_twistAngle_23();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (0.0f), (0.0f), L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Rotate_m610B6793DCC2F987290D328942E649B5B7DE0F9A(L_4, L_7, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_8 = __this->get_textMesh_4();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_9 = ___gesture0;
		NullCheck(L_9);
		float* L_10 = L_9->get_address_of_twistAngle_23();
		String_t* L_11 = Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3((float*)L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral04A6EA50E92943F99BDE01B3CD227BA68A3EE75E, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_8, L_12, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void TwistMe::On_TwistEnd(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102((bool)1, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_5 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_4, L_5, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_6 = __this->get_textMesh_4();
		NullCheck(L_6);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_6, _stringLiteralDE4D08C6D1642BC4345A93EFD01414BF4668EA4B, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void TwistMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102((bool)1, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_1 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_0, L_1, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_2 = __this->get_textMesh_4();
		NullCheck(L_2);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_2, _stringLiteralDE4D08C6D1642BC4345A93EFD01414BF4668EA4B, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwistMe__ctor_m06744BE7272661D1B810646B384755C1A4509651 (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoDoubleTapMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_OnEnable_m2B11FBB5777441BE991146F63778A1A570E1D1F6 (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_OnEnable_m2B11FBB5777441BE991146F63778A1A570E1D1F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * L_0 = (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 *)il2cpp_codegen_object_new(DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8_il2cpp_TypeInfo_var);
		DoubleTap2FingersHandler__ctor_mCF4A990BD0889F2497F5FD37DB1478EE8D5B7CC6(L_0, __this, (intptr_t)((intptr_t)TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_DoubleTap2Fingers_mA847C67B0A30C81BC9A12D4FD3046C8F094047E1(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_OnDisable_mE0C828F1C208572A1D74337F9DAAE4003E04FDFF (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method)
{
	{
		TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_OnDestroy_m0537A4F20B25F0CCB1235D7297D264944A1230A7 (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method)
{
	{
		TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657 (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * L_0 = (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 *)il2cpp_codegen_object_new(DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8_il2cpp_TypeInfo_var);
		DoubleTap2FingersHandler__ctor_mCF4A990BD0889F2497F5FD37DB1478EE8D5B7CC6(L_0, __this, (intptr_t)((intptr_t)TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_DoubleTap2Fingers_m4D6E01A1404E74A7C937B777B442320A9AD139BD(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_5);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_5, /*hidden argument*/NULL);
		float L_7 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_8 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_9 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_10), L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_6, L_10, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void TwoDoubleTapMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoDoubleTapMe__ctor_mD2BCCE139F26114852238D88404541A3B48BD474 (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoDragMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_OnEnable_m4D9291A4F42D0E4CCAD0A5E0A38C9530559F97BE (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_OnEnable_m4D9291A4F42D0E4CCAD0A5E0A38C9530559F97BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * L_0 = (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B *)il2cpp_codegen_object_new(DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B_il2cpp_TypeInfo_var);
		DragStart2FingersHandler__ctor_m96F1FEE595DC4B38964C2CE9FFBF2D4577802249(L_0, __this, (intptr_t)((intptr_t)TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_DragStart2Fingers_mDE91C6A5A0E98EF2EFCB655FAE2FBAA27DA3BB17(L_0, /*hidden argument*/NULL);
		Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * L_1 = (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA *)il2cpp_codegen_object_new(Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA_il2cpp_TypeInfo_var);
		Drag2FingersHandler__ctor_mE97E75C61AC4B4C0A4AD34A67B30259DD0EAF444(L_1, __this, (intptr_t)((intptr_t)TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Drag2Fingers_mA7EB90CBF64A6EE2D985AC3DB16795DAD80E5113(L_1, /*hidden argument*/NULL);
		DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * L_2 = (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A *)il2cpp_codegen_object_new(DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A_il2cpp_TypeInfo_var);
		DragEnd2FingersHandler__ctor_mE400CA66F2292D2F12A1A5B67E79ABC72ABE16D7(L_2, __this, (intptr_t)((intptr_t)TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_DragEnd2Fingers_mE2E7397978E3B634279BDC927E61CB9B92887B0F(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_OnDisable_m5B906CF331E3D147D171A6BFE12EB394BC26E7E8 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	{
		TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_OnDestroy_mD49987B836652C20657BE68EB543623FB3095834 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	{
		TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * L_0 = (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B *)il2cpp_codegen_object_new(DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B_il2cpp_TypeInfo_var);
		DragStart2FingersHandler__ctor_m96F1FEE595DC4B38964C2CE9FFBF2D4577802249(L_0, __this, (intptr_t)((intptr_t)TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_DragStart2Fingers_mEBEF7A5C1788FB0D372C873EE7D3CD04C514689E(L_0, /*hidden argument*/NULL);
		Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * L_1 = (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA *)il2cpp_codegen_object_new(Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA_il2cpp_TypeInfo_var);
		Drag2FingersHandler__ctor_mE97E75C61AC4B4C0A4AD34A67B30259DD0EAF444(L_1, __this, (intptr_t)((intptr_t)TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Drag2Fingers_mC6D908A3893EF1F9E966C8AA502FD786B859D8C2(L_1, /*hidden argument*/NULL);
		DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * L_2 = (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A *)il2cpp_codegen_object_new(DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A_il2cpp_TypeInfo_var);
		DragEnd2FingersHandler__ctor_mE400CA66F2292D2F12A1A5B67E79ABC72ABE16D7(L_2, __this, (intptr_t)((intptr_t)TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_DragEnd2Fingers_m36A33E27ABD55E19913DA5C8FB24BA6CE4DE00D4(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::Start()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_Start_m8299AE6AFFA99BDA8D0F51E23B9EF81A3D7C4BA2 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_Start_m8299AE6AFFA99BDA8D0F51E23B9EF81A3D7C4BA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_0 = Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699_RuntimeMethod_var);
		__this->set_textMesh_4(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_2 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_2);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4 = Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9(L_3, /*hidden argument*/NULL);
		__this->set_startColor_6(L_4);
		return;
	}
}
// System.Void TwoDragMe::On_DragStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031(__this, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_4 = ___gesture0;
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_5 = ___gesture0;
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_5)->get_pickedObject_8();
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Gesture_GetTouchToWorldPoint_m976B9A97EA151D764514B372966D71E653072BB4(L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_10, L_12, /*hidden argument*/NULL);
		__this->set_deltaPosition_5(L_13);
	}

IL_0047:
	{
		return;
	}
}
// System.Void TwoDragMe::On_Drag2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_007a;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_4 = ___gesture0;
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_5 = ___gesture0;
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_5)->get_pickedObject_8();
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Gesture_GetTouchToWorldPoint_m976B9A97EA151D764514B372966D71E653072BB4(L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = __this->get_deltaPosition_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_10, L_13, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_14 = ___gesture0;
		NullCheck(L_14);
		float L_15 = Gesture_GetSwipeOrDragAngle_m2F365EE39BDBCE136ABD4084BFA17E62AD9EEF85(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_16 = __this->get_textMesh_4();
		String_t* L_17 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_1), _stringLiteralCF1126F67238BF3E85FCC8C8737B72E80DDCFDDB, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_18 = ___gesture0;
		NullCheck(L_18);
		int32_t* L_19 = L_18->get_address_of_swipe_19();
		RuntimeObject * L_20 = Box(SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE_il2cpp_TypeInfo_var, L_19);
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		*L_19 = *(int32_t*)UnBox(L_20);
		String_t* L_22 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_17, _stringLiteral0D0C4DDD7CAB8253149568AACCC1F72AA7F9ACF8, L_21, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_16, L_22, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// System.Void TwoDragMe::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_5);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_5, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_7 = __this->get_startColor_6();
		NullCheck(L_6);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_8 = __this->get_textMesh_4();
		NullCheck(L_8);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_8, _stringLiteralD2227D5D6B26800F2C5978F782A05D6F0DC11F02, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void TwoDragMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031 (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_1 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_6), L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoDragMe__ctor_mAB9F5C5DEE23B4AFA27EE46FC1A9DE277C27A44E (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoLongTapMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_OnEnable_m24553C57C72307C05864F880DE2E06DF759B5179 (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_OnEnable_m24553C57C72307C05864F880DE2E06DF759B5179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * L_0 = (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 *)il2cpp_codegen_object_new(LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779_il2cpp_TypeInfo_var);
		LongTapStart2FingersHandler__ctor_mCEAE610D6ACD375506E88B77B757791DB6E9ECBF(L_0, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_LongTapStart2Fingers_mD6EAB281C23078CDBB179CF030DF06364730DCA2(L_0, /*hidden argument*/NULL);
		LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * L_1 = (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 *)il2cpp_codegen_object_new(LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_il2cpp_TypeInfo_var);
		LongTap2FingersHandler__ctor_m4962BF8633C3CBAE28F1D75AE74CC7329E59CFF8(L_1, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_LongTap2Fingers_mEEBDCAA62C01A97A50392545C95A826662D0B109(L_1, /*hidden argument*/NULL);
		LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * L_2 = (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 *)il2cpp_codegen_object_new(LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_il2cpp_TypeInfo_var);
		LongTapEnd2FingersHandler__ctor_m855897087EE65EFC27D178330E0CD77AEB5A08F9(L_2, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_LongTapEnd2Fingers_mD897971A4E5DA92088145FF69C32BCA7C5F16BE5(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_OnDisable_m8A597E272F4A4DCA984CF98175BD334EF6D90D4C (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	{
		TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_OnDestroy_m834BA7E34436FBD3F1168C92BC41D8C806CE4DDC (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	{
		TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * L_0 = (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 *)il2cpp_codegen_object_new(LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779_il2cpp_TypeInfo_var);
		LongTapStart2FingersHandler__ctor_mCEAE610D6ACD375506E88B77B757791DB6E9ECBF(L_0, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTapStart2Fingers_mC01DA2731180F0C2282F828900820B9DA907D1AB(L_0, /*hidden argument*/NULL);
		LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * L_1 = (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 *)il2cpp_codegen_object_new(LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_il2cpp_TypeInfo_var);
		LongTap2FingersHandler__ctor_m4962BF8633C3CBAE28F1D75AE74CC7329E59CFF8(L_1, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTap2Fingers_m45F4803E90895FAE4523272CA320188C07360A06(L_1, /*hidden argument*/NULL);
		LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * L_2 = (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 *)il2cpp_codegen_object_new(LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_il2cpp_TypeInfo_var);
		LongTapEnd2FingersHandler__ctor_m855897087EE65EFC27D178330E0CD77AEB5A08F9(L_2, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTapEnd2Fingers_m3BCFD86E017A40A1E7CF6F187EC582A0402428CF(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::Start()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_Start_m25B7567F5274044250351D028F3413A15B68DB13 (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_Start_m25B7567F5274044250351D028F3413A15B68DB13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_0 = Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699_RuntimeMethod_var);
		__this->set_textMesh_4(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_2 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_2);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4 = Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9(L_3, /*hidden argument*/NULL);
		__this->set_startColor_5(L_4);
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0 (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_4 = __this->get_textMesh_4();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_5 = ___gesture0;
		NullCheck(L_5);
		float* L_6 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_5)->get_address_of_actionTime_5();
		String_t* L_7 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)L_6, _stringLiteralCF1126F67238BF3E85FCC8C8737B72E80DDCFDDB, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_4, L_7, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_5);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_5, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_7 = __this->get_startColor_5();
		NullCheck(L_6);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_8 = __this->get_textMesh_4();
		NullCheck(L_8);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_8, _stringLiteral5D4C761EAD1A15EE4348E51DC4C16D41A9C3604C, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_1 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_6), L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoLongTapMe__ctor_m6BAB410EEAFED8FB06624FC8E66EA9506601D434 (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoSwipe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_OnEnable_mD01720E2E3D1FB0F5F6485FDE2E89087724160D2 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_OnEnable_mD01720E2E3D1FB0F5F6485FDE2E89087724160D2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * L_0 = (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 *)il2cpp_codegen_object_new(SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_il2cpp_TypeInfo_var);
		SwipeStart2FingersHandler__ctor_m4316083F72EA7135ACFFAFE6896C4743941B5AB2(L_0, __this, (intptr_t)((intptr_t)TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeStart2Fingers_mD066B4DF4DED0C2A5ECDC0B63E2E23F85970DF6B(L_0, /*hidden argument*/NULL);
		Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * L_1 = (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 *)il2cpp_codegen_object_new(Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862_il2cpp_TypeInfo_var);
		Swipe2FingersHandler__ctor_m041539F2580BD4669C44668741A7506D0745A5F9(L_1, __this, (intptr_t)((intptr_t)TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Swipe2Fingers_m29F2B727DF6BB6F5DA51998D5F391A46CAB590AC(L_1, /*hidden argument*/NULL);
		SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * L_2 = (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC *)il2cpp_codegen_object_new(SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_il2cpp_TypeInfo_var);
		SwipeEnd2FingersHandler__ctor_m7CF9535201124466FC7577CB425378F9A359BB92(L_2, __this, (intptr_t)((intptr_t)TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeEnd2Fingers_mACCBCCA09EE9D34F296C454A4A7BB284E623A456(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_OnDisable_m072399D0FB362E3F9BAD878F4F9D30A5BDDD48A5 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method)
{
	{
		TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_OnDestroy_m1F1BB31AECD941C287714260116BDC48312A0020 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method)
{
	{
		TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * L_0 = (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 *)il2cpp_codegen_object_new(SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_il2cpp_TypeInfo_var);
		SwipeStart2FingersHandler__ctor_m4316083F72EA7135ACFFAFE6896C4743941B5AB2(L_0, __this, (intptr_t)((intptr_t)TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeStart2Fingers_mC014048396BCDB5E414C8CB51E2FAE989CD3FCDA(L_0, /*hidden argument*/NULL);
		Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * L_1 = (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 *)il2cpp_codegen_object_new(Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862_il2cpp_TypeInfo_var);
		Swipe2FingersHandler__ctor_m041539F2580BD4669C44668741A7506D0745A5F9(L_1, __this, (intptr_t)((intptr_t)TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Swipe2Fingers_mE50D63E2CB409D988CF47532116E799801F7C753(L_1, /*hidden argument*/NULL);
		SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * L_2 = (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC *)il2cpp_codegen_object_new(SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_il2cpp_TypeInfo_var);
		SwipeEnd2FingersHandler__ctor_m7CF9535201124466FC7577CB425378F9A359BB92(L_2, __this, (intptr_t)((intptr_t)TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeEnd2Fingers_m52A6FE0854E1376170C0128AD1240929AF3CE3AD(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get_swipeData_5();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteralFD5E191A9C5EB57D0731B3A4BB8EB5E24AC6746C);
		return;
	}
}
// System.Void TwoSwipe::On_Swipe2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Gesture_GetTouchToWorldPoint_mCEBE61ED58EC849C47A2C093A7BD71DA650BE72F(L_0, (5.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_trail_4();
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_2, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = V_0;
		NullCheck(L_3);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		float L_1 = Gesture_GetSwipeOrDragAngle_m2F365EE39BDBCE136ABD4084BFA17E62AD9EEF85(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_swipeData_5();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral0A7D6582906A95B793B13B299CCFE2E94E81B566);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral0A7D6582906A95B793B13B299CCFE2E94E81B566);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		int32_t* L_7 = L_6->get_address_of_swipe_19();
		RuntimeObject * L_8 = Box(SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE_il2cpp_TypeInfo_var, L_7);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		*L_7 = *(int32_t*)UnBox(L_8);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_5;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3FD9B24FFD9A11F33EFDC7D229AD99867E6F5BEE);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3FD9B24FFD9A11F33EFDC7D229AD99867E6F5BEE);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_12 = ___gesture0;
		NullCheck(L_12);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_13 = L_12->get_address_of_swipeVector_21();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_13, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = L_14;
		RuntimeObject * L_16 = Box(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_16);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_11;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral1A960D7D734E11913921D735AA4177AC122CABF7);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1A960D7D734E11913921D735AA4177AC122CABF7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_17;
		String_t* L_19 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteralCF1126F67238BF3E85FCC8C8737B72E80DDCFDDB, /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_19);
		String_t* L_20 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_18, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_20);
		return;
	}
}
// System.Void TwoSwipe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoSwipe__ctor_mBD0137AA0BA8843547F3646F7B7D27C26DF5B255 (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoTapMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_OnEnable_mE11A33FC435C98E2DD8CAC8417E8CD0C181CBF8C (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_OnEnable_mE11A33FC435C98E2DD8CAC8417E8CD0C181CBF8C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * L_0 = (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 *)il2cpp_codegen_object_new(SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30_il2cpp_TypeInfo_var);
		SimpleTap2FingersHandler__ctor_m8AA13BBCDA12B5701474963698625B52D828586D(L_0, __this, (intptr_t)((intptr_t)TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_SimpleTap2Fingers_mC19305DAB55E6BCFB82C8FB6E4123255B82E7738(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_OnDisable_m090FCAF372A993258677033A9FA146AAC24DB1D8 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	{
		TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_OnDestroy_mC61C7535ACB2FCC296C68513E7907F72F7BC7201 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	{
		TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * L_0 = (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 *)il2cpp_codegen_object_new(SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30_il2cpp_TypeInfo_var);
		SimpleTap2FingersHandler__ctor_m8AA13BBCDA12B5701474963698625B52D828586D(L_0, __this, (intptr_t)((intptr_t)TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_SimpleTap2Fingers_m1FF34C9A9AEDAAD4145E7DE230A27009F229D680(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void TwoTapMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_1 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_6), L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoTapMe__ctor_mCBA3A52FFCF5F4A399A4CE9AD115DBBF3E8BC095 (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoTouchMe::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_OnEnable_mEADF58E4D9A10CE69524FAC5F05EEB0445DD6540 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_OnEnable_mEADF58E4D9A10CE69524FAC5F05EEB0445DD6540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_0 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_0, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0(L_0, /*hidden argument*/NULL);
		TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * L_1 = (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2(L_1, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown2Fingers_mCEC9C49CDAC504B09D7E9BCDE555469C3DC7EBF8(L_1, /*hidden argument*/NULL);
		TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * L_2 = (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6(L_2, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp2Fingers_m039A663F83056984EA1F7C82CE246C3040CF6556(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_OnDisable_mAA27BCB9D49F63DE42CD442377ACD14743F30E82 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	{
		TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_OnDestroy_m154CB1223709052D3CF256B51017D289036F770E (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	{
		TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::UnsubscribeEvent()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_0 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_0, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F(L_0, /*hidden argument*/NULL);
		TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * L_1 = (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2(L_1, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown2Fingers_m8FC281BAD18B3C3C2B9A87424BB7573424CF122A(L_1, /*hidden argument*/NULL);
		TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * L_2 = (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6(L_2, __this, (intptr_t)((intptr_t)TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp2Fingers_m6E4F3B68FA23F90751EC9D4E5E90FF58ED717122(L_2, /*hidden argument*/NULL);
		Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * L_3 = (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD *)il2cpp_codegen_object_new(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0(L_3, __this, (intptr_t)((intptr_t)TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::Start()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_Start_m29E68CF505FCC00AF0CF622AF832E2AE063D1DEC (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_Start_m29E68CF505FCC00AF0CF622AF832E2AE063D1DEC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_0 = Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF4FC730BE39BA499BD890B437FE1DD83BEE57699_RuntimeMethod_var);
		__this->set_textMesh_4(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_2 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_2);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4 = Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9(L_3, /*hidden argument*/NULL);
		__this->set_startColor_5(L_4);
		return;
	}
}
// System.Void TwoTouchMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_4 = __this->get_textMesh_4();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_5 = ___gesture0;
		NullCheck(L_5);
		float* L_6 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_5)->get_address_of_actionTime_5();
		String_t* L_7 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)L_6, _stringLiteralCF1126F67238BF3E85FCC8C8737B72E80DDCFDDB, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralC403BDE4925A5B437875D8BF934C7BAEE5B043CB, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_4, L_8, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_pickedObject_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_5);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_5, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_7 = __this->get_startColor_5();
		NullCheck(L_6);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_8 = __this->get_textMesh_4();
		NullCheck(L_8);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_8, _stringLiteralA9DBC977A94F4F310A01C6C83731C39712AAD98B, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::RandomColor()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322 (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_1 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_6), L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TwoTouchMe__ctor_mEB426FAC62BB58413573FBD998C5E81F89AE94BC (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UICompatibility::SetCompatibility(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UICompatibility_SetCompatibility_m469CB267AC36BBA0FB46FAA5DF5FE93BAC664ED9 (UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		EasyTouch_SetUICompatibily_m9DC3C352B33E7DF4B4DAED2FB5C49D6AEA94551E(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UICompatibility::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UICompatibility__ctor_mE0E38E77CBFDEA33A1E3632FBE804F6064E03A73 (UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIDrag::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UIDrag_OnEnable_m53F3D1A1B1141E2A30F7633C81224DEAE59EE620 (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_OnEnable_m53F3D1A1B1141E2A30F7633C81224DEAE59EE620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * L_0 = (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 *)il2cpp_codegen_object_new(TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m266AB85478489C46AE8B9803142E973BB9679DA8(L_0, __this, (intptr_t)((intptr_t)UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown_m2D2D5E25BB869F6A6B394F3566EEBE4BAD522EEC(L_0, /*hidden argument*/NULL);
		TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * L_1 = (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 *)il2cpp_codegen_object_new(TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m8B03D7113759D0BA11A017A3766C383F16B8D268(L_1, __this, (intptr_t)((intptr_t)UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart_m97F8CDF7FB22E6FB7515E5BB618C41D675A03BFA(L_1, /*hidden argument*/NULL);
		TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * L_2 = (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E *)il2cpp_codegen_object_new(TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_mFCAF0BE55F280AB1DD20F6AC020DE2E3B4A1E85F(L_2, __this, (intptr_t)((intptr_t)UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp_mA1891CE45733A36181535ABFE611B6FFC194FC6A(L_2, /*hidden argument*/NULL);
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_3 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_3, __this, (intptr_t)((intptr_t)UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0(L_3, /*hidden argument*/NULL);
		TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * L_4 = (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2(L_4, __this, (intptr_t)((intptr_t)UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown2Fingers_mCEC9C49CDAC504B09D7E9BCDE555469C3DC7EBF8(L_4, /*hidden argument*/NULL);
		TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * L_5 = (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6(L_5, __this, (intptr_t)((intptr_t)UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp2Fingers_m039A663F83056984EA1F7C82CE246C3040CF6556(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void UIDrag_OnDestroy_mDBA34F76DF1D49FC4EAB15E75C912C9B6D1A419B (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_OnDestroy_mDBA34F76DF1D49FC4EAB15E75C912C9B6D1A419B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * L_0 = (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 *)il2cpp_codegen_object_new(TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m266AB85478489C46AE8B9803142E973BB9679DA8(L_0, __this, (intptr_t)((intptr_t)UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown_m700E4BBE8FFA99A81B81C8E89F213F1EC410A19C(L_0, /*hidden argument*/NULL);
		TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * L_1 = (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 *)il2cpp_codegen_object_new(TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m8B03D7113759D0BA11A017A3766C383F16B8D268(L_1, __this, (intptr_t)((intptr_t)UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart_mC872F67C12E8BEC695AF6214936B10D6BB8AA8DC(L_1, /*hidden argument*/NULL);
		TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * L_2 = (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E *)il2cpp_codegen_object_new(TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_mFCAF0BE55F280AB1DD20F6AC020DE2E3B4A1E85F(L_2, __this, (intptr_t)((intptr_t)UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp_m0BBFA87933DF34D98AB73029DE067840F76F0E03(L_2, /*hidden argument*/NULL);
		TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * L_3 = (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE *)il2cpp_codegen_object_new(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858(L_3, __this, (intptr_t)((intptr_t)UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F(L_3, /*hidden argument*/NULL);
		TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * L_4 = (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2(L_4, __this, (intptr_t)((intptr_t)UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown2Fingers_m8FC281BAD18B3C3C2B9A87424BB7573424CF122A(L_4, /*hidden argument*/NULL);
		TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * L_5 = (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6(L_5, __this, (intptr_t)((intptr_t)UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp2Fingers_m6E4F3B68FA23F90751EC9D4E5E90FF58ED717122(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90 (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_005b;
		}
	}
	{
		bool L_2 = __this->get_drag_5();
		if (!L_2)
		{
			goto IL_005b;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_3 = ___gesture0;
		NullCheck(L_3);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_3)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_7 = ___gesture0;
		NullCheck(L_7);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_7)->get_pickedUIElement_11();
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_11 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005b;
		}
	}

IL_003b:
	{
		int32_t L_12 = __this->get_fingerId_4();
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_005b;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_13 = ___gesture0;
		NullCheck(L_13);
		int32_t L_14 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_13)->get_fingerIndex_0();
		__this->set_fingerId_4(L_14);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990(L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_fingerId_4();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_1 = ___gesture0;
		NullCheck(L_1);
		int32_t L_2 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_1)->get_fingerIndex_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_006a;
		}
	}
	{
		bool L_3 = __this->get_drag_5();
		if (!L_3)
		{
			goto IL_006a;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_4 = ___gesture0;
		NullCheck(L_4);
		bool L_5 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_4)->get_isOverGui_10();
		if (!L_5)
		{
			goto IL_006a;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_6)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0049;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_10 = ___gesture0;
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_10)->get_pickedUIElement_11();
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_11, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_14 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006a;
		}
	}

IL_0049:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = L_15;
		NullCheck(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_16, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_18 = ___gesture0;
		NullCheck(L_18);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_18)->get_deltaPosition_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_17, L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_16, L_21, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_fingerId_4();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_1 = ___gesture0;
		NullCheck(L_1);
		int32_t L_2 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_1)->get_fingerIndex_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0015;
		}
	}
	{
		__this->set_fingerId_4((-1));
	}

IL_0015:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62 (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = __this->get_drag_5();
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_3 = ___gesture0;
		NullCheck(L_3);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_3)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_7 = ___gesture0;
		NullCheck(L_7);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_7)->get_pickedUIElement_11();
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_11 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004f;
		}
	}

IL_003b:
	{
		int32_t L_12 = __this->get_fingerId_4();
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_004f;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990(L_13, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598 (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_0086;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_2)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0086;
		}
	}

IL_0033:
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_11 = ___gesture0;
		NullCheck(L_11);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_11)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_12, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_005e;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_15 = ___gesture0;
		NullCheck(L_15);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_15)->get_pickedUIElement_11();
		NullCheck(L_16);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_16, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_19 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_007f;
		}
	}

IL_005e:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = L_20;
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_21, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_23 = ___gesture0;
		NullCheck(L_23);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_23)->get_deltaPosition_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_22, L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_21, L_26, /*hidden argument*/NULL);
	}

IL_007f:
	{
		__this->set_drag_5((bool)0);
	}

IL_0086:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_2)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003a;
		}
	}

IL_0033:
	{
		__this->set_drag_5((bool)1);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UIDrag::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIDrag__ctor_mA0B84C6372221FB61265BCDA320B577BE40C2A4B (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D * __this, const RuntimeMethod* method)
{
	{
		__this->set_fingerId_4((-1));
		__this->set_drag_5((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPinch::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UIPinch_OnEnable_m63DC2215BEC9FB17C5D4C3859FE58565DA8E3239 (UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_OnEnable_m63DC2215BEC9FB17C5D4C3859FE58565DA8E3239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * L_0 = (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 *)il2cpp_codegen_object_new(PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4_il2cpp_TypeInfo_var);
		PinchHandler__ctor_m56455C5FFAECE920632FE4C6D67A884CADDA6B03(L_0, __this, (intptr_t)((intptr_t)UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Pinch_m3AB86DB5A8558B4BA19968EE71452F82CACA6352(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPinch::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void UIPinch_OnDestroy_mB910E8CCBD3760031F31C9EAA2B20F0E8CDF62EE (UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_OnDestroy_mB910E8CCBD3760031F31C9EAA2B20F0E8CDF62EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * L_0 = (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 *)il2cpp_codegen_object_new(PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4_il2cpp_TypeInfo_var);
		PinchHandler__ctor_m56455C5FFAECE920632FE4C6D67A884CADDA6B03(L_0, __this, (intptr_t)((intptr_t)UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Pinch_m0144B1E20FF8546AFF793BBDF6794007FD2E4BE1(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPinch::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C (UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_0090;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_2)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0036;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0090;
		}
	}

IL_0036:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_x_2();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_15 = ___gesture0;
		NullCheck(L_15);
		float L_16 = L_15->get_deltaPinch_22();
		float L_17 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_y_3();
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_21 = ___gesture0;
		NullCheck(L_21);
		float L_22 = L_21->get_deltaPinch_22();
		float L_23 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_24, /*hidden argument*/NULL);
		float L_26 = L_25.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_27), ((float)il2cpp_codegen_add((float)L_14, (float)((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)))), ((float)il2cpp_codegen_add((float)L_20, (float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_23)))), L_26, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_11, L_27, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void UIPinch::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIPinch__ctor_m13DB91E62E69AAFDAE6272E07AD753438853B548 (UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UITwist::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UITwist_OnEnable_m197511549C6C8217FDE1FA2264F5A0A25B1F0FC2 (UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_OnEnable_m197511549C6C8217FDE1FA2264F5A0A25B1F0FC2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * L_0 = (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D *)il2cpp_codegen_object_new(TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62(L_0, __this, (intptr_t)((intptr_t)UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_add_On_Twist_mE80E52A3010E98705B3319F87DDC34706601239A(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITwist::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void UITwist_OnDestroy_m183699EEB9681FC54940FB2114819D819E5B05DB (UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_OnDestroy_m183699EEB9681FC54940FB2114819D819E5B05DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * L_0 = (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D *)il2cpp_codegen_object_new(TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62(L_0, __this, (intptr_t)((intptr_t)UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63_RuntimeMethod_var), /*hidden argument*/NULL);
		EasyTouch_remove_On_Twist_mF6873B2804131E986AC0C40F390EC4E487A466D5(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITwist::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern "C" IL2CPP_METHOD_ATTR void UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63 (UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0 * __this, Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_2)->get_pickedUIElement_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = ((BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0053;
		}
	}

IL_0033:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * L_12 = ___gesture0;
		NullCheck(L_12);
		float L_13 = L_12->get_twistAngle_23();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_14), (0.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_Rotate_m610B6793DCC2F987290D328942E649B5B7DE0F9A(L_11, L_14, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UITwist::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UITwist__ctor_mE6B86357BEEF6CE575A51F4163A2263C9489ACC5 (UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWindow::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIWindow_OnDrag_m0C823B17A262F9D6294E0015EB1627EE246FD591 (UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWindow_OnDrag_m0C823B17A262F9D6294E0015EB1627EE246FD591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = L_0;
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * L_3 = ___eventData0;
		NullCheck(L_3);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = PointerEventData_get_delta_m70E69337BC484503C037D1D292C37E6926B6F3A7(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_2, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_1, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWindow::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIWindow_OnPointerDown_m58491AECE96A6331EC5C91614A1F78126F189AB0 (UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWindow::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIWindow__ctor_m789C7BB02C86B2D6627A8A13CC4BBECF9D0207E8 (UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniformTexturedLine::Start()
extern "C" IL2CPP_METHOD_ATTR void UniformTexturedLine_Start_m2613952894AC753DDD633A16090196662A275C5F (UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniformTexturedLine_Start_m2613952894AC753DDD633A16090196662A275C5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * V_0 = NULL;
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37(L_0, /*hidden argument*/List_1__ctor_m982E98A00E43FB2B088FDD98D6DEBD8797698B37_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = V_0;
		int32_t L_2 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		int32_t L_3 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, ((int32_t)((int32_t)L_2/(int32_t)2)), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_4), (0.0f), (((float)((float)L_3))), /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_1, L_4, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_5 = V_0;
		int32_t L_6 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		int32_t L_8 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, L_7, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_9), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1))))), (((float)((float)L_8))), /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_5, L_9, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_10 = V_0;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_11 = __this->get_lineTexture_4();
		float L_12 = __this->get_lineWidth_5();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_13 = (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F *)il2cpp_codegen_object_new(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine__ctor_mA5F79C0A5CC77F5EDEFCBB9F5C51179077BF6467(L_13, _stringLiteralEA9676003762818513C636984FAEAA0AE356839A, L_10, L_11, L_12, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_14 = L_13;
		float L_15 = __this->get_textureScale_6();
		NullCheck(L_14);
		VectorLine_set_textureScale_mEE94B92E3502B3354ABD562ED1BF43E88BCAAC46(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723(L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniformTexturedLine::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UniformTexturedLine__ctor_m7C576F482AD134BE14C3D1B0D8DC4C76D9C0CB58 (UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F * __this, const RuntimeMethod* method)
{
	{
		__this->set_lineWidth_5((8.0f));
		__this->set_textureScale_6((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern "C" IL2CPP_METHOD_ATTR bool BloomOptimized_CheckResources_mD632E635CB25AD6B910137EACA4B47413782B297 (BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730 * __this, const RuntimeMethod* method)
{
	{
		PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04(__this, (bool)0, /*hidden argument*/NULL);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_0 = __this->get_fastBloomShader_13();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_1 = __this->get_fastBloomMaterial_14();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->set_fastBloomMaterial_14(L_2);
		bool L_3 = ((PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E *)__this)->get_isSupported_6();
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		bool L_4 = ((PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E *)__this)->get_isSupported_6();
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void BloomOptimized_OnDisable_m6C76582A668E9F0AD13481E54FF47AFBF5FE7EFE (BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BloomOptimized_OnDisable_m6C76582A668E9F0AD13481E54FF47AFBF5FE7EFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = __this->get_fastBloomMaterial_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = __this->get_fastBloomMaterial_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void BloomOptimized_OnRenderImage_m8ACD073311ECEB136FF61084DD541125C69F2C42 (BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BloomOptimized_OnRenderImage_m8ACD073311ECEB136FF61084DD541125C69F2C42_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * V_7 = NULL;
	int32_t G_B5_0 = 0;
	float G_B8_0 = 0.0f;
	int32_t G_B11_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_1 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = ___destination1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113(L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0010:
	{
		int32_t L_3 = __this->get_resolution_10();
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		G_B5_0 = 2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B5_0 = 4;
	}

IL_001c:
	{
		V_0 = G_B5_0;
		int32_t L_4 = __this->get_resolution_10();
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		G_B8_0 = (1.0f);
		goto IL_0031;
	}

IL_002c:
	{
		G_B8_0 = (0.5f);
	}

IL_0031:
	{
		V_1 = G_B8_0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_5 = __this->get_fastBloomMaterial_14();
		float L_6 = __this->get_blurSize_9();
		float L_7 = V_1;
		float L_8 = __this->get_threshold_7();
		float L_9 = __this->get_intensity_8();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D((&L_10), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)), (0.0f), L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetVector_m6FC2CC4EBE6C45D48D8B9164148A0CB3124335EC(L_5, _stringLiteral629553D78E18022708836F7773B70DC9DFC3AF5C, L_10, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_11 = ___source0;
		NullCheck(L_11);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_11, 1, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_12 = ___source0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		int32_t L_14 = V_0;
		V_2 = ((int32_t)((int32_t)L_13/(int32_t)L_14));
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_15 = ___source0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_15);
		int32_t L_17 = V_0;
		V_3 = ((int32_t)((int32_t)L_16/(int32_t)L_17));
		int32_t L_18 = V_2;
		int32_t L_19 = V_3;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_20 = ___source0;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_format_mC500BCC10B2A6D6808645B505DB510056516D1FF(L_20, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_22 = RenderTexture_GetTemporary_m6E0EF85D2DEC0626DE5BB5D008A659F1CD66D9F8(L_18, L_19, 0, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_23 = V_4;
		NullCheck(L_23);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_23, 1, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_24 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_25 = V_4;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_26 = __this->get_fastBloomMaterial_14();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_24, L_25, L_26, 1, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_blurType_12();
		if (!L_27)
		{
			goto IL_00ab;
		}
	}
	{
		G_B11_0 = 2;
		goto IL_00ac;
	}

IL_00ab:
	{
		G_B11_0 = 0;
	}

IL_00ac:
	{
		V_5 = G_B11_0;
		V_6 = 0;
		goto IL_0160;
	}

IL_00b6:
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_28 = __this->get_fastBloomMaterial_14();
		float L_29 = __this->get_blurSize_9();
		float L_30 = V_1;
		int32_t L_31 = V_6;
		float L_32 = __this->get_threshold_7();
		float L_33 = __this->get_intensity_8();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D((&L_34), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_29, (float)L_30)), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_31))), (float)(1.0f))))), (0.0f), L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m6FC2CC4EBE6C45D48D8B9164148A0CB3124335EC(L_28, _stringLiteral629553D78E18022708836F7773B70DC9DFC3AF5C, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_2;
		int32_t L_36 = V_3;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_37 = ___source0;
		NullCheck(L_37);
		int32_t L_38 = RenderTexture_get_format_mC500BCC10B2A6D6808645B505DB510056516D1FF(L_37, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_39 = RenderTexture_GetTemporary_m6E0EF85D2DEC0626DE5BB5D008A659F1CD66D9F8(L_35, L_36, 0, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_40 = V_7;
		NullCheck(L_40);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_40, 1, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_41 = V_4;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_42 = V_7;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_43 = __this->get_fastBloomMaterial_14();
		int32_t L_44 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_41, L_42, L_43, ((int32_t)il2cpp_codegen_add((int32_t)2, (int32_t)L_44)), /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_45 = V_4;
		RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA(L_45, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_46 = V_7;
		V_4 = L_46;
		int32_t L_47 = V_2;
		int32_t L_48 = V_3;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_49 = ___source0;
		NullCheck(L_49);
		int32_t L_50 = RenderTexture_get_format_mC500BCC10B2A6D6808645B505DB510056516D1FF(L_49, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_51 = RenderTexture_GetTemporary_m6E0EF85D2DEC0626DE5BB5D008A659F1CD66D9F8(L_47, L_48, 0, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_52 = V_7;
		NullCheck(L_52);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_52, 1, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_53 = V_4;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_54 = V_7;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_55 = __this->get_fastBloomMaterial_14();
		int32_t L_56 = V_5;
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_53, L_54, L_55, ((int32_t)il2cpp_codegen_add((int32_t)3, (int32_t)L_56)), /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_57 = V_4;
		RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA(L_57, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_58 = V_7;
		V_4 = L_58;
		int32_t L_59 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
	}

IL_0160:
	{
		int32_t L_60 = V_6;
		int32_t L_61 = __this->get_blurIterations_11();
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_00b6;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_62 = __this->get_fastBloomMaterial_14();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_63 = V_4;
		NullCheck(L_62);
		Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61(L_62, _stringLiteral8747FBD2838A430FE3A98F274FB1202FF95DC3BC, L_63, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_64 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_65 = ___destination1;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_66 = __this->get_fastBloomMaterial_14();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_64, L_65, L_66, 0, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_67 = V_4;
		RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA(L_67, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BloomOptimized__ctor_m9A393CAC94F698AC6DFD0407C439C885DF249189 (BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730 * __this, const RuntimeMethod* method)
{
	{
		__this->set_threshold_7((0.25f));
		__this->set_intensity_8((0.75f));
		__this->set_blurSize_9((1.0f));
		__this->set_blurIterations_11(1);
		PostEffectsBase__ctor_m440C9B609EF88230A2EB266FD3E6C624431E1368(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___s0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m2Create1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral76E34C4759CBCC4A5ECE2529ADFF033A72857F95, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(__this, (bool)0, /*hidden argument*/NULL);
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}

IL_0026:
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_4 = ___s0;
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_8 = ___m2Create1;
		NullCheck(L_8);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_9 = Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0(L_8, /*hidden argument*/NULL);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_10 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_12 = ___m2Create1;
		return L_12;
	}

IL_0046:
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0090;
		}
	}
	{
		PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C(__this, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral90D8307A0AF5EE8F6CC07794AEBAD275A58B04C0);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral90D8307A0AF5EE8F6CC07794AEBAD275A58B04C0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_17 = L_16;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_18 = ___s0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_19);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_20 = L_17;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteralE8368D7779E8560C0C02CE5983B80423E277F284);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE8368D7779E8560C0C02CE5983B80423E277F284);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = L_20;
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_22);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_23 = L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral138799543726AA05424D18E73444A6C7AE801CD1);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral138799543726AA05424D18E73444A6C7AE801CD1);
		String_t* L_24 = String_Concat_m232E857CA5107EA6AC52E7DD7018716C021F237B(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_24, /*hidden argument*/NULL);
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}

IL_0090:
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_25 = ___s0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_26 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_26, L_25, /*hidden argument*/NULL);
		___m2Create1 = L_26;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_27 = ___m2Create1;
		NullCheck(L_27);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_27, ((int32_t)52), /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_28 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00aa;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_30 = ___m2Create1;
		return L_30;
	}

IL_00aa:
	{
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * PostEffectsBase_CreateMaterial_mA2EDA33D7CD6FA380975DA46597540AAB17B89AE (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___s0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m2Create1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CreateMaterial_mA2EDA33D7CD6FA380975DA46597540AAB17B89AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral76E34C4759CBCC4A5ECE2529ADFF033A72857F95, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_3, /*hidden argument*/NULL);
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}

IL_001f:
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_4 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = ___m2Create1;
		NullCheck(L_6);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_7 = Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0(L_6, /*hidden argument*/NULL);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_8 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_10 = ___s0;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003f;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_12 = ___m2Create1;
		return L_12;
	}

IL_003f:
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0049;
		}
	}
	{
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}

IL_0049:
	{
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_15 = ___s0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_16 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_16, L_15, /*hidden argument*/NULL);
		___m2Create1 = L_16;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_17 = ___m2Create1;
		NullCheck(L_17);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_17, ((int32_t)52), /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_18 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0063;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_20 = ___m2Create1;
		return L_20;
	}

IL_0063:
	{
		return (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)NULL;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_OnEnable_mFEA4058D703A6A068ECFA899FCCF6CEC8E742967 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		__this->set_isSupported_6((bool)1);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckSupport_m33F744872944BE0FB9A9FBF5FB73CF1CC62BD012 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckResources_m31A44EE19F985DCD4D3242F9197BF1435F1C8094 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckResources_m31A44EE19F985DCD4D3242F9197BF1435F1C8094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_1 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE1DF8B0AA9251994C53B6AE47BEFF033E7CA298B, L_0, _stringLiteral48F24C9D20CEBDADAD22D9795E25D2284B0D7269, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_isSupported_6();
		return L_2;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_Start_mA2E9CD553BD5AB2AD1963EC250854408434701F1 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, bool ___needDepth0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * G_B2_0 = NULL;
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * G_B3_1 = NULL;
	{
		__this->set_isSupported_6((bool)1);
		bool L_0 = SystemInfo_SupportsRenderTextureFormat_m74D259714A97501D28951CA48298D9F0AE3B5907(2, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_4(L_0);
		int32_t L_1 = SystemInfo_get_graphicsShaderLevel_m04540C22D983AE52CBCC6E1026D16F0C548248EB(/*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)50))))
		{
			G_B2_0 = __this;
			goto IL_0024;
		}
	}
	{
		bool L_2 = SystemInfo_get_supportsComputeShaders_m257AEF98322259041D3358B801FD6F34DB13013F(/*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		G_B3_1 = G_B1_0;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0025:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_supportDX11_5((bool)G_B3_0);
		bool L_3 = SystemInfo_get_supportsImageEffects_m5606438D404910FADC9C50DC29E0649E49B08267(/*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0039:
	{
		bool L_4 = ___needDepth0;
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		bool L_5 = SystemInfo_SupportsRenderTextureFormat_m74D259714A97501D28951CA48298D9F0AE3B5907(1, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004c;
		}
	}
	{
		PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_004c:
	{
		bool L_6 = ___needDepth0;
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_7 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = Camera_get_depthTextureMode_m157C12D349137B72F27061C027E6954EC65D54AD(L_8, /*hidden argument*/NULL);
		NullCheck(L_8);
		Camera_set_depthTextureMode_mBD8E259A3E29C4A5AC1FA8898700789B43264D2C(L_8, ((int32_t)((int32_t)L_9|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0062:
	{
		return (bool)1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckSupport_mB672E1075EC2C7E643A512D22CFB3000CC681636 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, bool ___needDepth0, bool ___needHdr1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___needDepth0;
		bool L_1 = PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return (bool)0;
	}

IL_000b:
	{
		bool L_2 = ___needHdr1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = __this->get_supportHDRTextures_4();
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001e:
	{
		return (bool)1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_Dx11Support_m51594DC020FEA76B63FC6804CAB21D88B8497C75 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_supportDX11_5();
		return L_0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_1 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral76AF80CC54267D2532EEDE98F8E5E95CF36F4E5A, L_0, _stringLiteral7B87A56EFD62101B4B8A4AAEEA3E8058B16EEDC4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern "C" IL2CPP_METHOD_ATTR bool PostEffectsBase_CheckShader_mE87A8B176C2F6264E568AD9EFFE7A64F9057CB43 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShader_mE87A8B176C2F6264E568AD9EFFE7A64F9057CB43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral90D8307A0AF5EE8F6CC07794AEBAD275A58B04C0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral90D8307A0AF5EE8F6CC07794AEBAD275A58B04C0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = L_1;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_3 = ___s0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_4);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_2;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralE8368D7779E8560C0C02CE5983B80423E277F284);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE8368D7779E8560C0C02CE5983B80423E277F284);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = L_5;
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = L_6;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralB8A178409953D7928C4F1F94E9C06A5A9D23B355);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralB8A178409953D7928C4F1F94E9C06A5A9D23B355);
		String_t* L_9 = String_Concat_m232E857CA5107EA6AC52E7DD7018716C021F237B(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_9, /*hidden argument*/NULL);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_10 = ___s0;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_004a;
		}
	}
	{
		PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_004a:
	{
		return (bool)0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_isSupported_6((bool)0);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase_DrawBorder_m3B6891159B6BFFA4621F38DCE1648176C0DC4C2B (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___dest0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	bool V_3 = false;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___dest0;
		RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1(L_0, /*hidden argument*/NULL);
		V_3 = (bool)1;
		GL_PushMatrix_mE47A23F3A906899E88AC525FFE2C3C2BD834DFF9(/*hidden argument*/NULL);
		GL_LoadOrtho_m6139139E4CFF2CA0CD0DF91D4B578D13E253DD9C(/*hidden argument*/NULL);
		V_4 = 0;
		goto IL_027d;
	}

IL_001a:
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_1 = ___material1;
		int32_t L_2 = V_4;
		NullCheck(L_1);
		Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_3;
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		V_5 = (1.0f);
		V_6 = (0.0f);
		goto IL_0044;
	}

IL_0036:
	{
		V_5 = (0.0f);
		V_6 = (1.0f);
	}

IL_0044:
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		V_0 = ((float)il2cpp_codegen_add((float)(0.0f), (float)((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_5))), (float)(1.0f)))))));
		V_1 = (0.0f);
		V_2 = (1.0f);
		GL_Begin_m9A48BD6A2DA850D54250EF638DF5EC61F83E293C(7, /*hidden argument*/NULL);
		float L_6 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = (0.0f);
		float L_8 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_0;
		float L_11 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_0;
		float L_14 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_7, L_16, (0.1f), /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_17 = ___dest0;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		V_0 = (1.0f);
		V_1 = (0.0f);
		V_2 = (1.0f);
		float L_19 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_19, /*hidden argument*/NULL);
		float L_20 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_18))), (float)(1.0f)))))));
		float L_21 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_20, L_21, (0.1f), /*hidden argument*/NULL);
		float L_22 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_22, /*hidden argument*/NULL);
		float L_23 = V_0;
		float L_24 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_23, L_24, (0.1f), /*hidden argument*/NULL);
		float L_25 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_25, /*hidden argument*/NULL);
		float L_26 = V_0;
		float L_27 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_26, L_27, (0.1f), /*hidden argument*/NULL);
		float L_28 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_28, /*hidden argument*/NULL);
		float L_29 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_20, L_29, (0.1f), /*hidden argument*/NULL);
		V_0 = (1.0f);
		V_1 = (0.0f);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_30 = ___dest0;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_30);
		V_2 = ((float)il2cpp_codegen_add((float)(0.0f), (float)((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_31))), (float)(1.0f)))))));
		float L_32 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_32, /*hidden argument*/NULL);
		float L_33 = (0.0f);
		float L_34 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_33, L_34, (0.1f), /*hidden argument*/NULL);
		float L_35 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_35, /*hidden argument*/NULL);
		float L_36 = V_0;
		float L_37 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_36, L_37, (0.1f), /*hidden argument*/NULL);
		float L_38 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_38, /*hidden argument*/NULL);
		float L_39 = V_0;
		float L_40 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_39, L_40, (0.1f), /*hidden argument*/NULL);
		float L_41 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_41, /*hidden argument*/NULL);
		float L_42 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_33, L_42, (0.1f), /*hidden argument*/NULL);
		V_0 = (1.0f);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_43 = ___dest0;
		NullCheck(L_43);
		int32_t L_44 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_43);
		V_1 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_44))), (float)(1.0f)))))));
		V_2 = (1.0f);
		float L_45 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_45, /*hidden argument*/NULL);
		float L_46 = (0.0f);
		float L_47 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_46, L_47, (0.1f), /*hidden argument*/NULL);
		float L_48 = V_5;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_1;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_0;
		float L_53 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_6;
		GL_TexCoord2_mB3400867C23FD8FFB1332F46D4B3AA382C15DAFF((0.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_2;
		GL_Vertex3_mE94809C1522CE96DF4C6CD218B1A26D5E60A114E(L_46, L_55, (0.1f), /*hidden argument*/NULL);
		GL_End_m7EDEB843BD9F7E00BD838FDE074B4688C55C0755(/*hidden argument*/NULL);
		int32_t L_56 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_027d:
	{
		int32_t L_57 = V_4;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_58 = ___material1;
		NullCheck(L_58);
		int32_t L_59 = Material_get_passCount_m790A357C17FB4D829C479652B1B65BC15F73FBB1(L_58, /*hidden argument*/NULL);
		if ((((int32_t)L_57) < ((int32_t)L_59)))
		{
			goto IL_001a;
		}
	}
	{
		GL_PopMatrix_mCAA6BC17D97358A4BC329E789AF2CA26C1204112(/*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PostEffectsBase__ctor_m440C9B609EF88230A2EB266FD3E6C624431E1368 (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E * __this, const RuntimeMethod* method)
{
	{
		__this->set_supportHDRTextures_4((bool)1);
		__this->set_isSupported_6((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UserController::Awake()
extern "C" IL2CPP_METHOD_ATTR void UserController_Awake_mF984E343AE6151B9892391C552155B6A909F1811 (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method)
{
	{
		UserController_LoadUserFromLocalFile_m503330B8722440718126146EA03DF95D972004AE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserController::SaveUserToLocalFile(System.String)
extern "C" IL2CPP_METHOD_ATTR void UserController_SaveUserToLocalFile_m2738573B388C610FCBA285F51CB76DEC7B29CC12 (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, String_t* ____user0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserController_SaveUserToLocalFile_m2738573B388C610FCBA285F51CB76DEC7B29CC12_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ____user0;
		User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * L_1 = JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A(L_0, /*hidden argument*/JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A_RuntimeMethod_var);
		__this->set_user_4(L_1);
		String_t* L_2 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_3 = __this->get_fileNameForUser_5();
		String_t* L_4 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_2, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ____user0;
		File_WriteAllText_m7BA355E5631C6A3E3D3378D6101EF65E72A45F0A(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * L_6 = Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962(/*hidden argument*/Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962_RuntimeMethod_var);
		NullCheck(L_6);
		LoginViewController_UpdateNavWithUserData_m06786D1F38AD2260231904FC325400C19E918E9D(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UserController::UserIsSavedLocally()
extern "C" IL2CPP_METHOD_ATTR bool UserController_UserIsSavedLocally_mBAC5EB856667987C90C44761F20B711FD54B0D0A (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserController_UserIsSavedLocally_mBAC5EB856667987C90C44761F20B711FD54B0D0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * V_1 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_1 = __this->get_fileNameForUser_5();
		String_t* L_2 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_0, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_1, /*hidden argument*/NULL);
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_3 = (FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C *)il2cpp_codegen_object_new(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_il2cpp_TypeInfo_var);
		FileInfo__ctor_m77D19A494A542C924C36FDD8AE5CDBEA97CE68B8(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_4 = V_1;
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.FileSystemInfo::get_Exists() */, L_5);
		if (L_6)
		{
			goto IL_002a;
		}
	}

IL_0026:
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_002a:
	{
		V_0 = (bool)1;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UserController::LoadUserFromLocalFile()
extern "C" IL2CPP_METHOD_ATTR void UserController_LoadUserFromLocalFile_m503330B8722440718126146EA03DF95D972004AE (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserController_LoadUserFromLocalFile_m503330B8722440718126146EA03DF95D972004AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_1 = __this->get_fileNameForUser_5();
		String_t* L_2 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_0, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_1, /*hidden argument*/NULL);
		bool L_3 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_userLoggedIn_6((bool)0);
		return;
	}

IL_0024:
	{
		String_t* L_4 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_5 = __this->get_fileNameForUser_5();
		String_t* L_6 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_4, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_5, /*hidden argument*/NULL);
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_7 = (FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C *)il2cpp_codegen_object_new(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C_il2cpp_TypeInfo_var);
		FileInfo__ctor_m77D19A494A542C924C36FDD8AE5CDBEA97CE68B8(L_7, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_8 = V_0;
		if (L_8)
		{
			goto IL_004a;
		}
	}
	{
		FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.FileSystemInfo::get_Exists() */, L_9);
		if (!L_10)
		{
			goto IL_0082;
		}
	}

IL_004a:
	{
		__this->set_userLoggedIn_6((bool)1);
		String_t* L_11 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_12 = __this->get_fileNameForUser_5();
		String_t* L_13 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_11, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_12, /*hidden argument*/NULL);
		String_t* L_14 = File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * L_16 = JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A(L_15, /*hidden argument*/JsonUtility_FromJson_TisUser_tDA60B60B80C63C1D6DD68A774400AB1263BD2371_mAB7F4825B5A8DB44D7FB4C50F178A897381DF19A_RuntimeMethod_var);
		__this->set_user_4(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833 * L_17 = Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962(/*hidden argument*/Object_FindObjectOfType_TisLoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833_m5D3A761773C7640557E1A07D1CE0E1CE72B4F962_RuntimeMethod_var);
		NullCheck(L_17);
		LoginViewController_SetNavBasedOnLocalUserSaved_mC5543F8E4774C7A0D42893E7C318D5EBD719BE35(L_17, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Void UserController::LogoutUserDataFromLocalFile()
extern "C" IL2CPP_METHOD_ATTR void UserController_LogoutUserDataFromLocalFile_m0ECF6195FC75133A06C7B7700A29AC19D974F817 (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserController_LogoutUserDataFromLocalFile_m0ECF6195FC75133A06C7B7700A29AC19D974F817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_userLoggedIn_6((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral523FD7FA92D538B546335BF7C14572CD17CAAD15, /*hidden argument*/NULL);
		String_t* L_0 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_1 = __this->get_fileNameForUser_5();
		String_t* L_2 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_0, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_1, /*hidden argument*/NULL);
		File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2(L_2, /*hidden argument*/NULL);
		__this->set_user_4((User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 *)NULL);
		return;
	}
}
// System.Void UserController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UserController__ctor_mA89DAF6076E23362D5AB6E1409B2A0B21E4E028D (UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UserSubmitData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UserSubmitData__ctor_m742670ABAC329A1276DEDD3E35B8BEE93CE88EBA (UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VectorObject::Start()
extern "C" IL2CPP_METHOD_ATTR void VectorObject_Start_m5159770FFE85C518C2AD104F3B788EDE7333E180 (VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject_Start_m5159770FFE85C518C2AD104F3B788EDE7333E180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * V_0 = NULL;
	{
		XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * L_0 = ((XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields*)il2cpp_codegen_static_fields_for(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_il2cpp_TypeInfo_var))->get_use_4();
		NullCheck(L_0);
		List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * L_1 = L_0->get_shapePoints_7();
		int32_t L_2 = __this->get_shape_4();
		NullCheck(L_1);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_3 = List_1_get_Item_m2C4E9711CA5C59D4FA95D74A13D54C8EF72E55BF(L_1, L_2, /*hidden argument*/List_1_get_Item_m2C4E9711CA5C59D4FA95D74A13D54C8EF72E55BF_RuntimeMethod_var);
		XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * L_4 = ((XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields*)il2cpp_codegen_static_fields_for(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_il2cpp_TypeInfo_var))->get_use_4();
		NullCheck(L_4);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_5 = L_4->get_lineTexture_5();
		XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * L_6 = ((XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields*)il2cpp_codegen_static_fields_for(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_il2cpp_TypeInfo_var))->get_use_4();
		NullCheck(L_6);
		float L_7 = L_6->get_lineWidth_6();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_8 = (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F *)il2cpp_codegen_object_new(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine__ctor_mFF68E3BA7BA26A321AD1BA25D70A07DDADACB67F(L_8, _stringLiteralEA5C1A20B7CFCFEC8A35FB4C6A419A1314233755, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_9 = V_0;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_10 = Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0(/*hidden argument*/NULL);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_11 = Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VectorLine_set_color_mA5BB3AE242CC3B03E53243380D8662B392538ED0(L_9, L_11, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_ObjectSetup_mBE9DB9235D4177C91832964336FC68925FEDF55C(L_12, L_13, 2, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VectorObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VectorObject__ctor_m8D7B491EBB1AFBA71C06A84E8EBF646248A0787F (VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void XrayLineData::Awake()
extern "C" IL2CPP_METHOD_ATTR void XrayLineData_Awake_m933A7EB80FCED093E1EA6068852E340EC2DC9C3A (XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XrayLineData_Awake_m933A7EB80FCED093E1EA6068852E340EC2DC9C3A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields*)il2cpp_codegen_static_fields_for(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_il2cpp_TypeInfo_var))->set_use_4(__this);
		List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * L_0 = (List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C *)il2cpp_codegen_object_new(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C_il2cpp_TypeInfo_var);
		List_1__ctor_m85AD1D8D83D26FF89816DBA34C37F77BA3B35518(L_0, /*hidden argument*/List_1__ctor_m85AD1D8D83D26FF89816DBA34C37F77BA3B35518_RuntimeMethod_var);
		__this->set_shapePoints_7(L_0);
		List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * L_1 = __this->get_shapePoints_7();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)il2cpp_codegen_object_new(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var);
		List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6(L_2, /*hidden argument*/List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_3 = L_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (-0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_3, L_4, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_5 = L_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_6), (0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_5, L_6, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_7 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_8), (-0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_7, L_8, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_9 = L_7;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_10), (-0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_9, L_10, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_11 = L_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_12), (0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_11, L_12, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_13 = L_11;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_14), (0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_13, L_14, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_15 = L_13;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_16), (0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_15);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_15, L_16, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_17 = L_15;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_18), (-0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_17);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_17, L_18, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_19 = L_17;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_20), (-0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_19, L_20, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_21 = L_19;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_22), (-0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_21, L_22, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_23 = L_21;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_24), (0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_23, L_24, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_25 = L_23;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_26), (0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_25, L_26, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_27 = L_25;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_28), (0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_27, L_28, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_29 = L_27;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_30), (-0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_29, L_30, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_31 = L_29;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_32), (-0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_31);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_31, L_32, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_33 = L_31;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_34), (-0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_33);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_33, L_34, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_35 = L_33;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_36), (0.5f), (0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_35);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_35, L_36, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_37 = L_35;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_38), (0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_37);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_37, L_38, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_39 = L_37;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_40), (0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_39);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_39, L_40, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_41 = L_39;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_42), (-0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_41);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_41, L_42, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_43 = L_41;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_44), (-0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_43);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_43, L_44, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_45 = L_43;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_46), (-0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_45);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_45, L_46, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_47 = L_45;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_48), (0.5f), (-0.5f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_47, L_48, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_49 = L_47;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_50), (0.5f), (-0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_49);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_49, L_50, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		NullCheck(L_1);
		List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2(L_1, L_49, /*hidden argument*/List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2_RuntimeMethod_var);
		List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * L_51 = __this->get_shapePoints_7();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_52 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)il2cpp_codegen_object_new(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var);
		List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6(L_52, /*hidden argument*/List_1__ctor_mDD66CB0E332C805F66C5E97C388E977D555222C6_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_53 = L_52;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_54), (-0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_53);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_53, L_54, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_55 = L_53;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_56), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_55);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_55, L_56, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_57 = L_55;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_58), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_57);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_57, L_58, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_59 = L_57;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_60), (-0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_59, L_60, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_61 = L_59;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_62), (-0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_61);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_61, L_62, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_63 = L_61;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_64), (-0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_63);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_63, L_64, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_65 = L_63;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_66), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_65, L_66, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_67 = L_65;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_68;
		memset(&L_68, 0, sizeof(L_68));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_68), (0.0f), (0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_67);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_67, L_68, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_69 = L_67;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_70;
		memset(&L_70, 0, sizeof(L_70));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_70), (0.0f), (0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_69);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_69, L_70, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_71 = L_69;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_72), (-0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_71);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_71, L_72, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_73 = L_71;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_74), (-0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_73);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_73, L_74, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_75 = L_73;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_76;
		memset(&L_76, 0, sizeof(L_76));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_76), (-0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_75);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_75, L_76, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_77 = L_75;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_78), (0.0f), (0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_77);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_77, L_78, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_79 = L_77;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_80), (0.0f), (0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_79, L_80, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_81 = L_79;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_82;
		memset(&L_82, 0, sizeof(L_82));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_82), (0.0f), (0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_81);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_81, L_82, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_83 = L_81;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_84;
		memset(&L_84, 0, sizeof(L_84));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_84), (-0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_83);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_83, L_84, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_85 = L_83;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_86), (-0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_85);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_85, L_86, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_87 = L_85;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_88;
		memset(&L_88, 0, sizeof(L_88));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_88), (-0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_87);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_87, L_88, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_89 = L_87;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_90;
		memset(&L_90, 0, sizeof(L_90));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_90), (0.0f), (0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_89);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_89, L_90, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_91 = L_89;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_92;
		memset(&L_92, 0, sizeof(L_92));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_92), (0.0f), (0.0f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_91);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_91, L_92, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_93 = L_91;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_94;
		memset(&L_94, 0, sizeof(L_94));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_94), (0.0f), (0.0f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_93);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_93, L_94, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_95 = L_93;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_96), (-0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_95);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_95, L_96, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_97 = L_95;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_98;
		memset(&L_98, 0, sizeof(L_98));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_98), (-0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_97);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_97, L_98, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_99 = L_97;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_100), (-0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_99);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_99, L_100, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_101 = L_99;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_102;
		memset(&L_102, 0, sizeof(L_102));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_102), (0.0f), (0.0f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_101);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_101, L_102, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_103 = L_101;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_104;
		memset(&L_104, 0, sizeof(L_104));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_104), (0.0f), (-0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_103);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_103, L_104, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_105 = L_103;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_106;
		memset(&L_106, 0, sizeof(L_106));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_106), (0.0f), (-0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_105);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_105, L_106, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_107 = L_105;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_108), (-0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_107);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_107, L_108, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_109 = L_107;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_110), (-0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_109);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_109, L_110, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_111 = L_109;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_112), (-0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_111);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_111, L_112, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_113 = L_111;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_114), (0.0f), (-0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_113);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_113, L_114, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_115 = L_113;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_116;
		memset(&L_116, 0, sizeof(L_116));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_116), (0.0f), (-0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_115);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_115, L_116, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_117 = L_115;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_118;
		memset(&L_118, 0, sizeof(L_118));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_118), (0.0f), (-0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_117);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_117, L_118, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_119 = L_117;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_120;
		memset(&L_120, 0, sizeof(L_120));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_120), (-0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_119);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_119, L_120, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_121 = L_119;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_122;
		memset(&L_122, 0, sizeof(L_122));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_122), (0.0f), (-0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_121);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_121, L_122, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_123 = L_121;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_124;
		memset(&L_124, 0, sizeof(L_124));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_124), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_123);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_123, L_124, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_125 = L_123;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_126;
		memset(&L_126, 0, sizeof(L_126));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_126), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_125);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_125, L_126, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_127 = L_125;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_128;
		memset(&L_128, 0, sizeof(L_128));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_128), (-0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_127);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_127, L_128, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_129 = L_127;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_130;
		memset(&L_130, 0, sizeof(L_130));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_130), (0.0f), (-0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_129);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_129, L_130, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_131 = L_129;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_132;
		memset(&L_132, 0, sizeof(L_132));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_132), (0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_131);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_131, L_132, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_133 = L_131;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_134;
		memset(&L_134, 0, sizeof(L_134));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_134), (0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_133);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_133, L_134, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_135 = L_133;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_136;
		memset(&L_136, 0, sizeof(L_136));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_136), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_135);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_135, L_136, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_137 = L_135;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_138;
		memset(&L_138, 0, sizeof(L_138));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_138), (0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_137);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_137, L_138, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_139 = L_137;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_140;
		memset(&L_140, 0, sizeof(L_140));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_140), (0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_139);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_139, L_140, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_141 = L_139;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_142;
		memset(&L_142, 0, sizeof(L_142));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_142), (0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_141);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_141, L_142, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_143 = L_141;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_144), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_143);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_143, L_144, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_145 = L_143;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_146;
		memset(&L_146, 0, sizeof(L_146));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_146), (0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_145);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_145, L_146, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_147 = L_145;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_148;
		memset(&L_148, 0, sizeof(L_148));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_148), (0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_147);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_147, L_148, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_149 = L_147;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_150;
		memset(&L_150, 0, sizeof(L_150));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_150), (0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_149);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_149, L_150, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_151 = L_149;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_152;
		memset(&L_152, 0, sizeof(L_152));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_152), (0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_151);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_151, L_152, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_153 = L_151;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_154;
		memset(&L_154, 0, sizeof(L_154));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_154), (0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_153);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_153, L_154, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_155 = L_153;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_156;
		memset(&L_156, 0, sizeof(L_156));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_156), (0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_155);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_155, L_156, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_157 = L_155;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_158;
		memset(&L_158, 0, sizeof(L_158));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_158), (0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_157);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_157, L_158, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_159 = L_157;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_160;
		memset(&L_160, 0, sizeof(L_160));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_160), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_159);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_159, L_160, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_161 = L_159;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_162;
		memset(&L_162, 0, sizeof(L_162));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_162), (0.0f), (0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_161);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_161, L_162, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_163 = L_161;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_164;
		memset(&L_164, 0, sizeof(L_164));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_164), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_163);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_163, L_164, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_165 = L_163;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_166;
		memset(&L_166, 0, sizeof(L_166));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_166), (0.0f), (0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_165);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_165, L_166, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_167 = L_165;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_168;
		memset(&L_168, 0, sizeof(L_168));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_168), (-0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_167);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_167, L_168, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_169 = L_167;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_170;
		memset(&L_170, 0, sizeof(L_170));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_170), (-0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_169);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_169, L_170, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_171 = L_169;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_172;
		memset(&L_172, 0, sizeof(L_172));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_172), (-0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_171);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_171, L_172, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_173 = L_171;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_174;
		memset(&L_174, 0, sizeof(L_174));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_174), (-0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_173);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_173, L_174, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_175 = L_173;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_176;
		memset(&L_176, 0, sizeof(L_176));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_176), (-0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_175);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_175, L_176, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_177 = L_175;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_178;
		memset(&L_178, 0, sizeof(L_178));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_178), (-0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_177);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_177, L_178, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_179 = L_177;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_180;
		memset(&L_180, 0, sizeof(L_180));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_180), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_179);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_179, L_180, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_181 = L_179;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_182;
		memset(&L_182, 0, sizeof(L_182));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_182), (0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_181);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_181, L_182, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_183 = L_181;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_184;
		memset(&L_184, 0, sizeof(L_184));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_184), (0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_183);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_183, L_184, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_185 = L_183;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_186;
		memset(&L_186, 0, sizeof(L_186));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_186), (0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_185);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_185, L_186, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_187 = L_185;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_188;
		memset(&L_188, 0, sizeof(L_188));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_188), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_187);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_187, L_188, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_189 = L_187;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_190;
		memset(&L_190, 0, sizeof(L_190));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_190), (0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_189);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_189, L_190, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_191 = L_189;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_192;
		memset(&L_192, 0, sizeof(L_192));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_192), (0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_191);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_191, L_192, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_193 = L_191;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_194;
		memset(&L_194, 0, sizeof(L_194));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_194), (0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_193);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_193, L_194, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_195 = L_193;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_196;
		memset(&L_196, 0, sizeof(L_196));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_196), (0.0f), (-0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_195);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_195, L_196, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_197 = L_195;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_198;
		memset(&L_198, 0, sizeof(L_198));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_198), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_197);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_197, L_198, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_199 = L_197;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_200;
		memset(&L_200, 0, sizeof(L_200));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_200), (0.0f), (-0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_199);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_199, L_200, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_201 = L_199;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_202;
		memset(&L_202, 0, sizeof(L_202));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_202), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_201);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_201, L_202, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_203 = L_201;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_204;
		memset(&L_204, 0, sizeof(L_204));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_204), (-0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_203);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_203, L_204, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_205 = L_203;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_206;
		memset(&L_206, 0, sizeof(L_206));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_206), (-0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_205);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_205, L_206, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_207 = L_205;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_208;
		memset(&L_208, 0, sizeof(L_208));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_208), (-0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_207);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_207, L_208, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_209 = L_207;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_210;
		memset(&L_210, 0, sizeof(L_210));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_210), (-0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_209);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_209, L_210, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_211 = L_209;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_212;
		memset(&L_212, 0, sizeof(L_212));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_212), (-0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_211);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_211, L_212, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_213 = L_211;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_214;
		memset(&L_214, 0, sizeof(L_214));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_214), (-0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_213);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_213, L_214, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_215 = L_213;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_216;
		memset(&L_216, 0, sizeof(L_216));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_216), (-0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_215);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_215, L_216, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_217 = L_215;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_218;
		memset(&L_218, 0, sizeof(L_218));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_218), (0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_217);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_217, L_218, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_219 = L_217;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_220;
		memset(&L_220, 0, sizeof(L_220));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_220), (0.0f), (-0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_219);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_219, L_220, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_221 = L_219;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_222;
		memset(&L_222, 0, sizeof(L_222));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_222), (0.0f), (-0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_221);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_221, L_222, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_223 = L_221;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_224;
		memset(&L_224, 0, sizeof(L_224));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_224), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_223);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_223, L_224, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_225 = L_223;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_226;
		memset(&L_226, 0, sizeof(L_226));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_226), (0.0f), (-0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_225);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_225, L_226, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_227 = L_225;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_228;
		memset(&L_228, 0, sizeof(L_228));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_228), (0.0f), (-0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_227);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_227, L_228, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_229 = L_227;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_230;
		memset(&L_230, 0, sizeof(L_230));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_230), (0.0f), (-0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_229);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_229, L_230, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_231 = L_229;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_232;
		memset(&L_232, 0, sizeof(L_232));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_232), (-0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_231);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_231, L_232, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_233 = L_231;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_234;
		memset(&L_234, 0, sizeof(L_234));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_234), (-0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_233);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_233, L_234, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_235 = L_233;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_236;
		memset(&L_236, 0, sizeof(L_236));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_236), (-0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_235);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_235, L_236, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_237 = L_235;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_238;
		memset(&L_238, 0, sizeof(L_238));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_238), (-0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_237);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_237, L_238, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_239 = L_237;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_240;
		memset(&L_240, 0, sizeof(L_240));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_240), (-0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_239);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_239, L_240, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_241 = L_239;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_242;
		memset(&L_242, 0, sizeof(L_242));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_242), (-0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_241);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_241, L_242, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_243 = L_241;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_244;
		memset(&L_244, 0, sizeof(L_244));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_244), (-0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_243);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_243, L_244, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_245 = L_243;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_246;
		memset(&L_246, 0, sizeof(L_246));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_246), (-0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_245);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_245, L_246, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_247 = L_245;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_248;
		memset(&L_248, 0, sizeof(L_248));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_248), (-0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_247);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_247, L_248, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_249 = L_247;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_250;
		memset(&L_250, 0, sizeof(L_250));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_250), (0.0f), (-0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_249);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_249, L_250, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_251 = L_249;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_252;
		memset(&L_252, 0, sizeof(L_252));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_252), (-0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_251);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_251, L_252, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_253 = L_251;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_254;
		memset(&L_254, 0, sizeof(L_254));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_254), (-0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_253);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_253, L_254, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_255 = L_253;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_256;
		memset(&L_256, 0, sizeof(L_256));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_256), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_255);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_255, L_256, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_257 = L_255;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_258;
		memset(&L_258, 0, sizeof(L_258));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_258), (-0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_257);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_257, L_258, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_259 = L_257;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_260;
		memset(&L_260, 0, sizeof(L_260));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_260), (-0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_259);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_259, L_260, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_261 = L_259;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_262;
		memset(&L_262, 0, sizeof(L_262));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_262), (-0.306f), (-0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_261);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_261, L_262, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_263 = L_261;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_264;
		memset(&L_264, 0, sizeof(L_264));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_264), (-0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_263);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_263, L_264, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_265 = L_263;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_266;
		memset(&L_266, 0, sizeof(L_266));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_266), (-0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_265);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_265, L_266, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_267 = L_265;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_268;
		memset(&L_268, 0, sizeof(L_268));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_268), (-0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_267);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_267, L_268, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_269 = L_267;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_270;
		memset(&L_270, 0, sizeof(L_270));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_270), (-0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_269);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_269, L_270, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_271 = L_269;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_272;
		memset(&L_272, 0, sizeof(L_272));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_272), (-0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_271);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_271, L_272, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_273 = L_271;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_274;
		memset(&L_274, 0, sizeof(L_274));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_274), (-0.177f), (-0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_273);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_273, L_274, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_275 = L_273;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_276;
		memset(&L_276, 0, sizeof(L_276));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_276), (-0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_275);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_275, L_276, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_277 = L_275;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_278;
		memset(&L_278, 0, sizeof(L_278));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_278), (-0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_277);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_277, L_278, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_279 = L_277;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_280;
		memset(&L_280, 0, sizeof(L_280));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_280), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_279);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_279, L_280, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_281 = L_279;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_282;
		memset(&L_282, 0, sizeof(L_282));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_282), (-0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_281);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_281, L_282, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_283 = L_281;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_284;
		memset(&L_284, 0, sizeof(L_284));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_284), (-0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_283);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_283, L_284, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_285 = L_283;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_286;
		memset(&L_286, 0, sizeof(L_286));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_286), (-0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_285);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_285, L_286, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_287 = L_285;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_288;
		memset(&L_288, 0, sizeof(L_288));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_288), (-0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_287);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_287, L_288, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_289 = L_287;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_290;
		memset(&L_290, 0, sizeof(L_290));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_290), (0.0f), (0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_289);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_289, L_290, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_291 = L_289;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_292;
		memset(&L_292, 0, sizeof(L_292));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_292), (0.0f), (0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_291);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_291, L_292, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_293 = L_291;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_294;
		memset(&L_294, 0, sizeof(L_294));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_294), (0.0f), (0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_293);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_293, L_294, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_295 = L_293;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_296;
		memset(&L_296, 0, sizeof(L_296));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_296), (-0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_295);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_295, L_296, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_297 = L_295;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_298;
		memset(&L_298, 0, sizeof(L_298));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_298), (0.0f), (0.25f), (0.433f), /*hidden argument*/NULL);
		NullCheck(L_297);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_297, L_298, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_299 = L_297;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_300;
		memset(&L_300, 0, sizeof(L_300));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_300), (0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_299);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_299, L_300, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_301 = L_299;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_302;
		memset(&L_302, 0, sizeof(L_302));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_302), (0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_301);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_301, L_302, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_303 = L_301;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_304;
		memset(&L_304, 0, sizeof(L_304));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_304), (0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_303);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_303, L_304, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_305 = L_303;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_306;
		memset(&L_306, 0, sizeof(L_306));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_306), (0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_305);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_305, L_306, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_307 = L_305;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_308;
		memset(&L_308, 0, sizeof(L_308));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_308), (0.0f), (0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_307);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_307, L_308, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_309 = L_307;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_310;
		memset(&L_310, 0, sizeof(L_310));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_310), (0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_309);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_309, L_310, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_311 = L_309;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_312;
		memset(&L_312, 0, sizeof(L_312));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_312), (0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_311);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_311, L_312, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_313 = L_311;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_314;
		memset(&L_314, 0, sizeof(L_314));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_314), (0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_313);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_313, L_314, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_315 = L_313;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_316;
		memset(&L_316, 0, sizeof(L_316));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_316), (0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_315);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_315, L_316, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_317 = L_315;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_318;
		memset(&L_318, 0, sizeof(L_318));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_318), (0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_317);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_317, L_318, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_319 = L_317;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_320;
		memset(&L_320, 0, sizeof(L_320));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_320), (0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_319);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_319, L_320, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_321 = L_319;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_322;
		memset(&L_322, 0, sizeof(L_322));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_322), (0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_321);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_321, L_322, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_323 = L_321;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_324;
		memset(&L_324, 0, sizeof(L_324));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_324), (0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_323);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_323, L_324, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_325 = L_323;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_326;
		memset(&L_326, 0, sizeof(L_326));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_326), (0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_325);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_325, L_326, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_327 = L_325;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_328;
		memset(&L_328, 0, sizeof(L_328));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_328), (0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_327);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_327, L_328, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_329 = L_327;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_330;
		memset(&L_330, 0, sizeof(L_330));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_330), (0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_329);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_329, L_330, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_331 = L_329;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_332;
		memset(&L_332, 0, sizeof(L_332));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_332), (0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_331);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_331, L_332, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_333 = L_331;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_334;
		memset(&L_334, 0, sizeof(L_334));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_334), (0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_333);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_333, L_334, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_335 = L_333;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_336;
		memset(&L_336, 0, sizeof(L_336));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_336), (0.0f), (0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_335);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_335, L_336, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_337 = L_335;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_338;
		memset(&L_338, 0, sizeof(L_338));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_338), (0.0f), (0.433f), (-0.25f), /*hidden argument*/NULL);
		NullCheck(L_337);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_337, L_338, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_339 = L_337;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_340;
		memset(&L_340, 0, sizeof(L_340));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_340), (0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_339);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_339, L_340, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_341 = L_339;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_342;
		memset(&L_342, 0, sizeof(L_342));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_342), (0.0f), (0.433f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_341);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_341, L_342, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_343 = L_341;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_344;
		memset(&L_344, 0, sizeof(L_344));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_344), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_343);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_343, L_344, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_345 = L_343;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_346;
		memset(&L_346, 0, sizeof(L_346));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_346), (0.177f), (0.433f), (0.177f), /*hidden argument*/NULL);
		NullCheck(L_345);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_345, L_346, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_347 = L_345;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_348;
		memset(&L_348, 0, sizeof(L_348));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_348), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_347);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_347, L_348, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_349 = L_347;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_350;
		memset(&L_350, 0, sizeof(L_350));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_350), (0.25f), (0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_349);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_349, L_350, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_351 = L_349;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_352;
		memset(&L_352, 0, sizeof(L_352));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_352), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_351);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_351, L_352, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_353 = L_351;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_354;
		memset(&L_354, 0, sizeof(L_354));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_354), (0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_353);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_353, L_354, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_355 = L_353;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_356;
		memset(&L_356, 0, sizeof(L_356));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_356), (0.306f), (0.25f), (0.306f), /*hidden argument*/NULL);
		NullCheck(L_355);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_355, L_356, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_357 = L_355;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_358;
		memset(&L_358, 0, sizeof(L_358));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_358), (0.354f), (0.0f), (0.354f), /*hidden argument*/NULL);
		NullCheck(L_357);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_357, L_358, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_359 = L_357;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_360;
		memset(&L_360, 0, sizeof(L_360));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_360), (0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_359);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_359, L_360, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_361 = L_359;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_362;
		memset(&L_362, 0, sizeof(L_362));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_362), (0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_361);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_361, L_362, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_363 = L_361;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_364;
		memset(&L_364, 0, sizeof(L_364));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_364), (0.433f), (0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_363);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_363, L_364, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_365 = L_363;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_366;
		memset(&L_366, 0, sizeof(L_366));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_366), (0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_365);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_365, L_366, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_367 = L_365;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_368;
		memset(&L_368, 0, sizeof(L_368));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_368), (0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_367);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_367, L_368, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_369 = L_367;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_370;
		memset(&L_370, 0, sizeof(L_370));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_370), (0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_369);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_369, L_370, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_371 = L_369;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_372;
		memset(&L_372, 0, sizeof(L_372));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_372), (0.306f), (0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_371);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_371, L_372, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_373 = L_371;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_374;
		memset(&L_374, 0, sizeof(L_374));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_374), (0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_373);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_373, L_374, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_375 = L_373;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_376;
		memset(&L_376, 0, sizeof(L_376));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_376), (0.0f), (0.0f), (-0.5f), /*hidden argument*/NULL);
		NullCheck(L_375);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_375, L_376, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_377 = L_375;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_378;
		memset(&L_378, 0, sizeof(L_378));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_378), (0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_377);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_377, L_378, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_379 = L_377;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_380;
		memset(&L_380, 0, sizeof(L_380));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_380), (0.5f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_379);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_379, L_380, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_381 = L_379;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_382;
		memset(&L_382, 0, sizeof(L_382));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_382), (0.433f), (-0.25f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_381);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_381, L_382, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_383 = L_381;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_384;
		memset(&L_384, 0, sizeof(L_384));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_384), (0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_383);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_383, L_384, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_385 = L_383;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_386;
		memset(&L_386, 0, sizeof(L_386));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_386), (0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_385);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_385, L_386, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_387 = L_385;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_388;
		memset(&L_388, 0, sizeof(L_388));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_388), (0.354f), (0.0f), (-0.354f), /*hidden argument*/NULL);
		NullCheck(L_387);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_387, L_388, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_389 = L_387;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_390;
		memset(&L_390, 0, sizeof(L_390));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_390), (0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_389);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_389, L_390, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_391 = L_389;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_392;
		memset(&L_392, 0, sizeof(L_392));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_392), (0.0f), (-0.25f), (-0.433f), /*hidden argument*/NULL);
		NullCheck(L_391);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_391, L_392, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_393 = L_391;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_394;
		memset(&L_394, 0, sizeof(L_394));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_394), (0.177f), (0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_393);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_393, L_394, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_395 = L_393;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_396;
		memset(&L_396, 0, sizeof(L_396));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_396), (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_395);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_395, L_396, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_397 = L_395;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_398;
		memset(&L_398, 0, sizeof(L_398));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_398), (0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_397);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_397, L_398, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_399 = L_397;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_400;
		memset(&L_400, 0, sizeof(L_400));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_400), (0.306f), (-0.25f), (-0.306f), /*hidden argument*/NULL);
		NullCheck(L_399);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_399, L_400, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_401 = L_399;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_402;
		memset(&L_402, 0, sizeof(L_402));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_402), (-0.25f), (-0.433f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_401);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_401, L_402, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_403 = L_401;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_404;
		memset(&L_404, 0, sizeof(L_404));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_404), (-0.177f), (-0.433f), (-0.177f), /*hidden argument*/NULL);
		NullCheck(L_403);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_403, L_404, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		NullCheck(L_51);
		List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2(L_51, L_403, /*hidden argument*/List_1_Add_mDAC68A9888300F34AF0CAF02C7090A68F813B2E2_RuntimeMethod_var);
		return;
	}
}
// System.Void XrayLineData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void XrayLineData__ctor_m7925D64BDE20FACD898AE985150E02121A1C5639 (XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * __this, const RuntimeMethod* method)
{
	{
		__this->set_lineWidth_6((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
