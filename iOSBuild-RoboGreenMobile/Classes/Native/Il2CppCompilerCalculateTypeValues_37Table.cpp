﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// APIGreenController
struct APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C;
// APILoginController
struct APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05;
// APIPuttForm.BallLocation
struct BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42;
// APIPuttForm.HoleLocation
struct HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64;
// APIPuttForm.SendPuttFormData[]
struct SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15;
// CameraManager
struct CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085;
// ConfigurationViewController
struct ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB;
// CourseManager
struct CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// Doozy.Engine.UI.UIButton
struct UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527;
// HoleController
struct HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0;
// ProjectorStatusData
struct ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD;
// PuttInteractive
struct PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60;
// PuttSimulation
struct PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD;
// RegionalSelectFilter
struct RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2;
// RoboData
struct RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D;
// SimulationManager
struct SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9;
// Strackaline.ArrowColor
struct ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E;
// Strackaline.ArrowLocation
struct ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41;
// Strackaline.Arrow[]
struct ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40;
// Strackaline.Ball
struct Ball_t094765B02879225F8C595564E40A823B70AEE45E;
// Strackaline.CalendarDay[]
struct CalendarDayU5BU5D_t0EFA9C9D3036304C4A13A761168098E951BC3F98;
// Strackaline.Calendar[]
struct CalendarU5BU5D_tDDE442247D07B621468D9A4F30310A2A25CD375F;
// Strackaline.Configurator[]
struct ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45;
// Strackaline.Contour
struct Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42;
// Strackaline.ContourPathPoints[]
struct ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69;
// Strackaline.ContourPath[]
struct ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E;
// Strackaline.Course
struct Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A;
// Strackaline.CourseCalender
struct CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA;
// Strackaline.CourseCalenderSettings
struct CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1;
// Strackaline.CourseData
struct CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF;
// Strackaline.CourseData[]
struct CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC;
// Strackaline.CourseGreens
struct CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D;
// Strackaline.CourseSettings
struct CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352;
// Strackaline.DailyTemplate
struct DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED;
// Strackaline.Green
struct Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6;
// Strackaline.GreenActivity
struct GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B;
// Strackaline.GreenArrow
struct GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A;
// Strackaline.GreenCameraSettings
struct GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA;
// Strackaline.GreenGroup[]
struct GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D;
// Strackaline.GreenPutt[]
struct GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E;
// Strackaline.Green[]
struct GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1;
// Strackaline.Hole
struct Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB;
// Strackaline.HoleContainerSettings
struct HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175;
// Strackaline.HoleLocation[]
struct HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2;
// Strackaline.Instruction
struct Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C;
// Strackaline.JackData
struct JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1;
// Strackaline.Location
struct Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6;
// Strackaline.MemberDetail
struct MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B;
// Strackaline.Point[]
struct PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9;
// Strackaline.Putt
struct Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49;
// Strackaline.PuttVariation
struct PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31;
// Strackaline.Putt[]
struct PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52;
// Strackaline.Region
struct Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E;
// Strackaline.RegionData
struct RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F;
// Strackaline.RegionData[]
struct RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119;
// Strackaline.Roles[]
struct RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E;
// Strackaline.Target
struct Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139;
// Strackaline.TemplateHoleLocation[]
struct TemplateHoleLocationU5BU5D_t4D7C597F0890863D5E5B359A052EEEE0C469CCD2;
// Strackaline.User
struct User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371;
// Strackaline.UserCourse[]
struct UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE;
// Strackaline.UserPrinter[]
struct UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Int32>
struct Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<Jack>
struct List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD;
// System.Collections.Generic.List`1<PuttSimulation>
struct List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD;
// System.Collections.Generic.List`1<Strackaline.Arrow>
struct List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34;
// System.Collections.Generic.List`1<Strackaline.ContourPath>
struct List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D;
// System.Collections.Generic.List`1<Strackaline.Jack>
struct List_1_tC846C91557298786F744DB8E2A439B8F3F6B06AF;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>>
struct List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct List_1_t6A863775481DD4E1BDD024A35AC5E83118209168;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_InputField
struct TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UserController
struct UserController_tFD917E07B535D607AFD7056993FB736FD366E600;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#define BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.BallLocation
struct  BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42  : public RuntimeObject
{
public:
	// System.Single APIPuttForm.BallLocation::x
	float ___x_0;
	// System.Single APIPuttForm.BallLocation::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#ifndef HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#define HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.HoleLocation
struct  HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64  : public RuntimeObject
{
public:
	// System.Single APIPuttForm.HoleLocation::x
	float ___x_0;
	// System.Single APIPuttForm.HoleLocation::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#ifndef SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#define SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.SendPuttForm
struct  SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02  : public RuntimeObject
{
public:
	// System.Int32 APIPuttForm.SendPuttForm::greenId
	int32_t ___greenId_0;
	// System.Single APIPuttForm.SendPuttForm::rotation
	float ___rotation_1;
	// APIPuttForm.SendPuttFormData[] APIPuttForm.SendPuttForm::data
	SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* ___data_2;

public:
	inline static int32_t get_offset_of_greenId_0() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___greenId_0)); }
	inline int32_t get_greenId_0() const { return ___greenId_0; }
	inline int32_t* get_address_of_greenId_0() { return &___greenId_0; }
	inline void set_greenId_0(int32_t value)
	{
		___greenId_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___rotation_1)); }
	inline float get_rotation_1() const { return ___rotation_1; }
	inline float* get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(float value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___data_2)); }
	inline SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* get_data_2() const { return ___data_2; }
	inline SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#ifndef SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#define SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.SendPuttFormData
struct  SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE  : public RuntimeObject
{
public:
	// System.Int32 APIPuttForm.SendPuttFormData::stimp
	int32_t ___stimp_0;
	// System.Single APIPuttForm.SendPuttFormData::firmness
	float ___firmness_1;
	// System.Boolean APIPuttForm.SendPuttFormData::isMetric
	bool ___isMetric_2;
	// APIPuttForm.HoleLocation APIPuttForm.SendPuttFormData::holeLocation
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * ___holeLocation_3;
	// APIPuttForm.BallLocation APIPuttForm.SendPuttFormData::ballLocation
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * ___ballLocation_4;

public:
	inline static int32_t get_offset_of_stimp_0() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___stimp_0)); }
	inline int32_t get_stimp_0() const { return ___stimp_0; }
	inline int32_t* get_address_of_stimp_0() { return &___stimp_0; }
	inline void set_stimp_0(int32_t value)
	{
		___stimp_0 = value;
	}

	inline static int32_t get_offset_of_firmness_1() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___firmness_1)); }
	inline float get_firmness_1() const { return ___firmness_1; }
	inline float* get_address_of_firmness_1() { return &___firmness_1; }
	inline void set_firmness_1(float value)
	{
		___firmness_1 = value;
	}

	inline static int32_t get_offset_of_isMetric_2() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___isMetric_2)); }
	inline bool get_isMetric_2() const { return ___isMetric_2; }
	inline bool* get_address_of_isMetric_2() { return &___isMetric_2; }
	inline void set_isMetric_2(bool value)
	{
		___isMetric_2 = value;
	}

	inline static int32_t get_offset_of_holeLocation_3() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___holeLocation_3)); }
	inline HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * get_holeLocation_3() const { return ___holeLocation_3; }
	inline HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 ** get_address_of_holeLocation_3() { return &___holeLocation_3; }
	inline void set_holeLocation_3(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * value)
	{
		___holeLocation_3 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocation_3), value);
	}

	inline static int32_t get_offset_of_ballLocation_4() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___ballLocation_4)); }
	inline BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * get_ballLocation_4() const { return ___ballLocation_4; }
	inline BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 ** get_address_of_ballLocation_4() { return &___ballLocation_4; }
	inline void set_ballLocation_4(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * value)
	{
		___ballLocation_4 = value;
		Il2CppCodeGenWriteBarrier((&___ballLocation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_TB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4_H
#define U3CU3EC__DISPLAYCLASS13_0_TB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigurationViewController_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4  : public RuntimeObject
{
public:
	// UnityEngine.GameObject ConfigurationViewController_<>c__DisplayClass13_0::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	// ConfigurationViewController ConfigurationViewController_<>c__DisplayClass13_0::<>4__this
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4, ___go_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_0() const { return ___go_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4, ___U3CU3E4__this_1)); }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_TB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_TA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B_H
#define U3CU3EC__DISPLAYCLASS14_0_TA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigurationViewController_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B  : public RuntimeObject
{
public:
	// UnityEngine.GameObject ConfigurationViewController_<>c__DisplayClass14_0::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	// ConfigurationViewController ConfigurationViewController_<>c__DisplayClass14_0::<>4__this
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B, ___go_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_0() const { return ___go_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B, ___U3CU3E4__this_1)); }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_TA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B_H
#ifndef JACK_T5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9_H
#define JACK_T5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jack
struct  Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9  : public RuntimeObject
{
public:
	// System.Int32 Jack::jackIndex
	int32_t ___jackIndex_0;
	// System.Single Jack::elevation
	float ___elevation_1;
	// System.Single Jack::offset
	float ___offset_2;

public:
	inline static int32_t get_offset_of_jackIndex_0() { return static_cast<int32_t>(offsetof(Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9, ___jackIndex_0)); }
	inline int32_t get_jackIndex_0() const { return ___jackIndex_0; }
	inline int32_t* get_address_of_jackIndex_0() { return &___jackIndex_0; }
	inline void set_jackIndex_0(int32_t value)
	{
		___jackIndex_0 = value;
	}

	inline static int32_t get_offset_of_elevation_1() { return static_cast<int32_t>(offsetof(Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9, ___elevation_1)); }
	inline float get_elevation_1() const { return ___elevation_1; }
	inline float* get_address_of_elevation_1() { return &___elevation_1; }
	inline void set_elevation_1(float value)
	{
		___elevation_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9, ___offset_2)); }
	inline float get_offset_2() const { return ___offset_2; }
	inline float* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(float value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACK_T5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9_H
#ifndef PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#define PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectorStatusData
struct  ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD  : public RuntimeObject
{
public:
	// Strackaline.JackData ProjectorStatusData::jackData
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * ___jackData_0;
	// Strackaline.GreenActivity ProjectorStatusData::greenActivity
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * ___greenActivity_1;

public:
	inline static int32_t get_offset_of_jackData_0() { return static_cast<int32_t>(offsetof(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD, ___jackData_0)); }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * get_jackData_0() const { return ___jackData_0; }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 ** get_address_of_jackData_0() { return &___jackData_0; }
	inline void set_jackData_0(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * value)
	{
		___jackData_0 = value;
		Il2CppCodeGenWriteBarrier((&___jackData_0), value);
	}

	inline static int32_t get_offset_of_greenActivity_1() { return static_cast<int32_t>(offsetof(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD, ___greenActivity_1)); }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * get_greenActivity_1() const { return ___greenActivity_1; }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B ** get_address_of_greenActivity_1() { return &___greenActivity_1; }
	inline void set_greenActivity_1(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * value)
	{
		___greenActivity_1 = value;
		Il2CppCodeGenWriteBarrier((&___greenActivity_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T8C706C33ED5FA412CB1F3C2760DE58203706F7F2_H
#define U3CU3EC__DISPLAYCLASS11_0_T8C706C33ED5FA412CB1F3C2760DE58203706F7F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegionalSelectFilter_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2  : public RuntimeObject
{
public:
	// Strackaline.CourseData RegionalSelectFilter_<>c__DisplayClass11_0::item
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF * ___item_0;
	// RegionalSelectFilter RegionalSelectFilter_<>c__DisplayClass11_0::<>4__this
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2, ___item_0)); }
	inline CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF * get_item_0() const { return ___item_0; }
	inline CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2, ___U3CU3E4__this_1)); }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T8C706C33ED5FA412CB1F3C2760DE58203706F7F2_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_TB0566369EDF4B3232C57A065AA40552CFA2F594B_H
#define U3CU3EC__DISPLAYCLASS13_0_TB0566369EDF4B3232C57A065AA40552CFA2F594B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegionalSelectFilter_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B  : public RuntimeObject
{
public:
	// UnityEngine.GameObject RegionalSelectFilter_<>c__DisplayClass13_0::listBtn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___listBtn_0;
	// RegionalSelectFilter RegionalSelectFilter_<>c__DisplayClass13_0::<>4__this
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_listBtn_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B, ___listBtn_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_listBtn_0() const { return ___listBtn_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_listBtn_0() { return &___listBtn_0; }
	inline void set_listBtn_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___listBtn_0 = value;
		Il2CppCodeGenWriteBarrier((&___listBtn_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B, ___U3CU3E4__this_1)); }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_TB0566369EDF4B3232C57A065AA40552CFA2F594B_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T302499D805D0C2134517491D33971886561A0383_H
#define U3CU3EC__DISPLAYCLASS14_0_T302499D805D0C2134517491D33971886561A0383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegionalSelectFilter_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383  : public RuntimeObject
{
public:
	// Strackaline.Green RegionalSelectFilter_<>c__DisplayClass14_0::item
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * ___item_0;
	// RegionalSelectFilter RegionalSelectFilter_<>c__DisplayClass14_0::<>4__this
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383, ___item_0)); }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * get_item_0() const { return ___item_0; }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383, ___U3CU3E4__this_1)); }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T302499D805D0C2134517491D33971886561A0383_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_TBB84438049B445B2A7D56DCD0AD84F8B79E92A4E_H
#define U3CU3EC__DISPLAYCLASS9_0_TBB84438049B445B2A7D56DCD0AD84F8B79E92A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegionalSelectFilter_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E  : public RuntimeObject
{
public:
	// Strackaline.RegionData RegionalSelectFilter_<>c__DisplayClass9_0::item
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F * ___item_0;
	// RegionalSelectFilter RegionalSelectFilter_<>c__DisplayClass9_0::<>4__this
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E, ___item_0)); }
	inline RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F * get_item_0() const { return ___item_0; }
	inline RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E, ___U3CU3E4__this_1)); }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_TBB84438049B445B2A7D56DCD0AD84F8B79E92A4E_H
#ifndef ROBODATA_T7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D_H
#define ROBODATA_T7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoboData
struct  RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D  : public RuntimeObject
{
public:
	// System.Int32 RoboData::puttTrainerId
	int32_t ___puttTrainerId_0;
	// System.String RoboData::configurationData
	String_t* ___configurationData_1;
	// System.Int32 RoboData::configTypeId
	int32_t ___configTypeId_2;

public:
	inline static int32_t get_offset_of_puttTrainerId_0() { return static_cast<int32_t>(offsetof(RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D, ___puttTrainerId_0)); }
	inline int32_t get_puttTrainerId_0() const { return ___puttTrainerId_0; }
	inline int32_t* get_address_of_puttTrainerId_0() { return &___puttTrainerId_0; }
	inline void set_puttTrainerId_0(int32_t value)
	{
		___puttTrainerId_0 = value;
	}

	inline static int32_t get_offset_of_configurationData_1() { return static_cast<int32_t>(offsetof(RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D, ___configurationData_1)); }
	inline String_t* get_configurationData_1() const { return ___configurationData_1; }
	inline String_t** get_address_of_configurationData_1() { return &___configurationData_1; }
	inline void set_configurationData_1(String_t* value)
	{
		___configurationData_1 = value;
		Il2CppCodeGenWriteBarrier((&___configurationData_1), value);
	}

	inline static int32_t get_offset_of_configTypeId_2() { return static_cast<int32_t>(offsetof(RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D, ___configTypeId_2)); }
	inline int32_t get_configTypeId_2() const { return ___configTypeId_2; }
	inline int32_t* get_address_of_configTypeId_2() { return &___configTypeId_2; }
	inline void set_configTypeId_2(int32_t value)
	{
		___configTypeId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROBODATA_T7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D_H
#ifndef ROBOJACKDATA_T96D97C9BE56685CFE6AED4237E2B0E55782A3FF5_H
#define ROBOJACKDATA_T96D97C9BE56685CFE6AED4237E2B0E55782A3FF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoboJackData
struct  RoboJackData_t96D97C9BE56685CFE6AED4237E2B0E55782A3FF5  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Jack> RoboJackData::jackList
	List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * ___jackList_0;

public:
	inline static int32_t get_offset_of_jackList_0() { return static_cast<int32_t>(offsetof(RoboJackData_t96D97C9BE56685CFE6AED4237E2B0E55782A3FF5, ___jackList_0)); }
	inline List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * get_jackList_0() const { return ___jackList_0; }
	inline List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD ** get_address_of_jackList_0() { return &___jackList_0; }
	inline void set_jackList_0(List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * value)
	{
		___jackList_0 = value;
		Il2CppCodeGenWriteBarrier((&___jackList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROBOJACKDATA_T96D97C9BE56685CFE6AED4237E2B0E55782A3FF5_H
#ifndef ROBOPOST_T2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0_H
#define ROBOPOST_T2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoboPost
struct  RoboPost_t2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0  : public RuntimeObject
{
public:
	// RoboData RoboPost::data
	RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(RoboPost_t2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0, ___data_0)); }
	inline RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D * get_data_0() const { return ___data_0; }
	inline RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROBOPOST_T2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0_H
#ifndef ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#define ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Arrow
struct  Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520  : public RuntimeObject
{
public:
	// Strackaline.ArrowLocation Strackaline.Arrow::point
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * ___point_0;
	// System.Single Strackaline.Arrow::slope
	float ___slope_1;
	// System.Single Strackaline.Arrow::rotation
	float ___rotation_2;
	// System.Int32 Strackaline.Arrow::level
	int32_t ___level_3;
	// Strackaline.ArrowColor Strackaline.Arrow::color
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * ___color_4;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___point_0)); }
	inline ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * get_point_0() const { return ___point_0; }
	inline ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 ** get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * value)
	{
		___point_0 = value;
		Il2CppCodeGenWriteBarrier((&___point_0), value);
	}

	inline static int32_t get_offset_of_slope_1() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___slope_1)); }
	inline float get_slope_1() const { return ___slope_1; }
	inline float* get_address_of_slope_1() { return &___slope_1; }
	inline void set_slope_1(float value)
	{
		___slope_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___level_3)); }
	inline int32_t get_level_3() const { return ___level_3; }
	inline int32_t* get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(int32_t value)
	{
		___level_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___color_4)); }
	inline ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * get_color_4() const { return ___color_4; }
	inline ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E ** get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * value)
	{
		___color_4 = value;
		Il2CppCodeGenWriteBarrier((&___color_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#ifndef ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#define ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ArrowColor
struct  ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E  : public RuntimeObject
{
public:
	// System.Single Strackaline.ArrowColor::r
	float ___r_0;
	// System.Single Strackaline.ArrowColor::g
	float ___g_1;
	// System.Single Strackaline.ArrowColor::b
	float ___b_2;
	// System.Single Strackaline.ArrowColor::a
	float ___a_3;
	// System.String Strackaline.ArrowColor::hex
	String_t* ___hex_4;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}

	inline static int32_t get_offset_of_hex_4() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___hex_4)); }
	inline String_t* get_hex_4() const { return ___hex_4; }
	inline String_t** get_address_of_hex_4() { return &___hex_4; }
	inline void set_hex_4(String_t* value)
	{
		___hex_4 = value;
		Il2CppCodeGenWriteBarrier((&___hex_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#ifndef ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#define ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ArrowLocation
struct  ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41  : public RuntimeObject
{
public:
	// System.Single Strackaline.ArrowLocation::x
	float ___x_0;
	// System.Single Strackaline.ArrowLocation::y
	float ___y_1;
	// System.Single Strackaline.ArrowLocation::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#ifndef BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#define BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Ball
struct  Ball_t094765B02879225F8C595564E40A823B70AEE45E  : public RuntimeObject
{
public:
	// System.Single Strackaline.Ball::x
	float ___x_0;
	// System.Single Strackaline.Ball::y
	float ___y_1;
	// System.Single Strackaline.Ball::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#ifndef CALENDAR_TD7C76FE9D351E82FCD7746A2C72220767BF93093_H
#define CALENDAR_TD7C76FE9D351E82FCD7746A2C72220767BF93093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Calendar
struct  Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Calendar::year
	int32_t ___year_0;
	// System.Int32 Strackaline.Calendar::month
	int32_t ___month_1;
	// System.String Strackaline.Calendar::name
	String_t* ___name_2;
	// Strackaline.CalendarDay[] Strackaline.Calendar::days
	CalendarDayU5BU5D_t0EFA9C9D3036304C4A13A761168098E951BC3F98* ___days_3;

public:
	inline static int32_t get_offset_of_year_0() { return static_cast<int32_t>(offsetof(Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093, ___year_0)); }
	inline int32_t get_year_0() const { return ___year_0; }
	inline int32_t* get_address_of_year_0() { return &___year_0; }
	inline void set_year_0(int32_t value)
	{
		___year_0 = value;
	}

	inline static int32_t get_offset_of_month_1() { return static_cast<int32_t>(offsetof(Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093, ___month_1)); }
	inline int32_t get_month_1() const { return ___month_1; }
	inline int32_t* get_address_of_month_1() { return &___month_1; }
	inline void set_month_1(int32_t value)
	{
		___month_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_days_3() { return static_cast<int32_t>(offsetof(Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093, ___days_3)); }
	inline CalendarDayU5BU5D_t0EFA9C9D3036304C4A13A761168098E951BC3F98* get_days_3() const { return ___days_3; }
	inline CalendarDayU5BU5D_t0EFA9C9D3036304C4A13A761168098E951BC3F98** get_address_of_days_3() { return &___days_3; }
	inline void set_days_3(CalendarDayU5BU5D_t0EFA9C9D3036304C4A13A761168098E951BC3F98* value)
	{
		___days_3 = value;
		Il2CppCodeGenWriteBarrier((&___days_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALENDAR_TD7C76FE9D351E82FCD7746A2C72220767BF93093_H
#ifndef CALENDARDAY_T7DA31A0F3CD6385D90898E4DF03BF90496743CEA_H
#define CALENDARDAY_T7DA31A0F3CD6385D90898E4DF03BF90496743CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CalendarDay
struct  CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.CalendarDay::year
	int32_t ___year_0;
	// System.Int32 Strackaline.CalendarDay::month
	int32_t ___month_1;
	// System.Int32 Strackaline.CalendarDay::day
	int32_t ___day_2;
	// System.Int32 Strackaline.CalendarDay::weekday
	int32_t ___weekday_3;
	// System.String Strackaline.CalendarDay::name
	String_t* ___name_4;
	// Strackaline.DailyTemplate Strackaline.CalendarDay::dailyTemplate
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * ___dailyTemplate_5;

public:
	inline static int32_t get_offset_of_year_0() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___year_0)); }
	inline int32_t get_year_0() const { return ___year_0; }
	inline int32_t* get_address_of_year_0() { return &___year_0; }
	inline void set_year_0(int32_t value)
	{
		___year_0 = value;
	}

	inline static int32_t get_offset_of_month_1() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___month_1)); }
	inline int32_t get_month_1() const { return ___month_1; }
	inline int32_t* get_address_of_month_1() { return &___month_1; }
	inline void set_month_1(int32_t value)
	{
		___month_1 = value;
	}

	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___day_2)); }
	inline int32_t get_day_2() const { return ___day_2; }
	inline int32_t* get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(int32_t value)
	{
		___day_2 = value;
	}

	inline static int32_t get_offset_of_weekday_3() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___weekday_3)); }
	inline int32_t get_weekday_3() const { return ___weekday_3; }
	inline int32_t* get_address_of_weekday_3() { return &___weekday_3; }
	inline void set_weekday_3(int32_t value)
	{
		___weekday_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_dailyTemplate_5() { return static_cast<int32_t>(offsetof(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA, ___dailyTemplate_5)); }
	inline DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * get_dailyTemplate_5() const { return ___dailyTemplate_5; }
	inline DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED ** get_address_of_dailyTemplate_5() { return &___dailyTemplate_5; }
	inline void set_dailyTemplate_5(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * value)
	{
		___dailyTemplate_5 = value;
		Il2CppCodeGenWriteBarrier((&___dailyTemplate_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALENDARDAY_T7DA31A0F3CD6385D90898E4DF03BF90496743CEA_H
#ifndef CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#define CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Configurator
struct  Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Configurator::puttTrainerId
	int32_t ___puttTrainerId_0;
	// System.Int32 Strackaline.Configurator::configurationId
	int32_t ___configurationId_1;
	// System.String Strackaline.Configurator::createdOn
	String_t* ___createdOn_2;
	// System.String Strackaline.Configurator::configurationData
	String_t* ___configurationData_3;

public:
	inline static int32_t get_offset_of_puttTrainerId_0() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___puttTrainerId_0)); }
	inline int32_t get_puttTrainerId_0() const { return ___puttTrainerId_0; }
	inline int32_t* get_address_of_puttTrainerId_0() { return &___puttTrainerId_0; }
	inline void set_puttTrainerId_0(int32_t value)
	{
		___puttTrainerId_0 = value;
	}

	inline static int32_t get_offset_of_configurationId_1() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___configurationId_1)); }
	inline int32_t get_configurationId_1() const { return ___configurationId_1; }
	inline int32_t* get_address_of_configurationId_1() { return &___configurationId_1; }
	inline void set_configurationId_1(int32_t value)
	{
		___configurationId_1 = value;
	}

	inline static int32_t get_offset_of_createdOn_2() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___createdOn_2)); }
	inline String_t* get_createdOn_2() const { return ___createdOn_2; }
	inline String_t** get_address_of_createdOn_2() { return &___createdOn_2; }
	inline void set_createdOn_2(String_t* value)
	{
		___createdOn_2 = value;
		Il2CppCodeGenWriteBarrier((&___createdOn_2), value);
	}

	inline static int32_t get_offset_of_configurationData_3() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___configurationData_3)); }
	inline String_t* get_configurationData_3() const { return ___configurationData_3; }
	inline String_t** get_address_of_configurationData_3() { return &___configurationData_3; }
	inline void set_configurationData_3(String_t* value)
	{
		___configurationData_3 = value;
		Il2CppCodeGenWriteBarrier((&___configurationData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#ifndef CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#define CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Contour
struct  Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42  : public RuntimeObject
{
public:
	// Strackaline.ContourPath[] Strackaline.Contour::contourPaths
	ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* ___contourPaths_0;

public:
	inline static int32_t get_offset_of_contourPaths_0() { return static_cast<int32_t>(offsetof(Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42, ___contourPaths_0)); }
	inline ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* get_contourPaths_0() const { return ___contourPaths_0; }
	inline ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E** get_address_of_contourPaths_0() { return &___contourPaths_0; }
	inline void set_contourPaths_0(ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* value)
	{
		___contourPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___contourPaths_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#ifndef CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#define CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ContourPath
struct  ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6  : public RuntimeObject
{
public:
	// Strackaline.ContourPathPoints[] Strackaline.ContourPath::path
	ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* ___path_0;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6, ___path_0)); }
	inline ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* get_path_0() const { return ___path_0; }
	inline ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#ifndef CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#define CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ContourPathPoints
struct  ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6  : public RuntimeObject
{
public:
	// System.Single Strackaline.ContourPathPoints::x
	float ___x_0;
	// System.Single Strackaline.ContourPathPoints::y
	float ___y_1;
	// System.Single Strackaline.ContourPathPoints::z
	float ___z_2;
	// System.Int32 Strackaline.ContourPathPoints::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#ifndef COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#define COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Course
struct  Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A  : public RuntimeObject
{
public:
	// Strackaline.CourseData[] Strackaline.Course::value
	CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A, ___value_0)); }
	inline CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* get_value_0() const { return ___value_0; }
	inline CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#ifndef COURSECALENDER_TBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA_H
#define COURSECALENDER_TBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseCalender
struct  CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA  : public RuntimeObject
{
public:
	// Strackaline.CourseCalenderSettings Strackaline.CourseCalender::settings
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1 * ___settings_0;
	// Strackaline.Calendar[] Strackaline.CourseCalender::calendar
	CalendarU5BU5D_tDDE442247D07B621468D9A4F30310A2A25CD375F* ___calendar_1;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA, ___settings_0)); }
	inline CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1 * get_settings_0() const { return ___settings_0; }
	inline CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1 ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1 * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___settings_0), value);
	}

	inline static int32_t get_offset_of_calendar_1() { return static_cast<int32_t>(offsetof(CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA, ___calendar_1)); }
	inline CalendarU5BU5D_tDDE442247D07B621468D9A4F30310A2A25CD375F* get_calendar_1() const { return ___calendar_1; }
	inline CalendarU5BU5D_tDDE442247D07B621468D9A4F30310A2A25CD375F** get_address_of_calendar_1() { return &___calendar_1; }
	inline void set_calendar_1(CalendarU5BU5D_tDDE442247D07B621468D9A4F30310A2A25CD375F* value)
	{
		___calendar_1 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSECALENDER_TBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA_H
#ifndef COURSECALENDERSETTINGS_T3648F4699BFE264FFFBF537C2B0655E969B7F6B1_H
#define COURSECALENDERSETTINGS_T3648F4699BFE264FFFBF537C2B0655E969B7F6B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseCalenderSettings
struct  CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1  : public RuntimeObject
{
public:
	// System.String Strackaline.CourseCalenderSettings::dailyId
	String_t* ___dailyId_0;
	// System.Int32 Strackaline.CourseCalenderSettings::courseId
	int32_t ___courseId_1;
	// System.Single Strackaline.CourseCalenderSettings::version
	float ___version_2;
	// System.Boolean Strackaline.CourseCalenderSettings::isActive
	bool ___isActive_3;
	// System.Boolean Strackaline.CourseCalenderSettings::isSunday
	bool ___isSunday_4;
	// System.Boolean Strackaline.CourseCalenderSettings::isMonday
	bool ___isMonday_5;
	// System.Boolean Strackaline.CourseCalenderSettings::isTuesday
	bool ___isTuesday_6;
	// System.Boolean Strackaline.CourseCalenderSettings::isWednesday
	bool ___isWednesday_7;
	// System.Boolean Strackaline.CourseCalenderSettings::isThursday
	bool ___isThursday_8;
	// System.Boolean Strackaline.CourseCalenderSettings::isFriday
	bool ___isFriday_9;
	// System.Boolean Strackaline.CourseCalenderSettings::isSaturday
	bool ___isSaturday_10;

public:
	inline static int32_t get_offset_of_dailyId_0() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___dailyId_0)); }
	inline String_t* get_dailyId_0() const { return ___dailyId_0; }
	inline String_t** get_address_of_dailyId_0() { return &___dailyId_0; }
	inline void set_dailyId_0(String_t* value)
	{
		___dailyId_0 = value;
		Il2CppCodeGenWriteBarrier((&___dailyId_0), value);
	}

	inline static int32_t get_offset_of_courseId_1() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___courseId_1)); }
	inline int32_t get_courseId_1() const { return ___courseId_1; }
	inline int32_t* get_address_of_courseId_1() { return &___courseId_1; }
	inline void set_courseId_1(int32_t value)
	{
		___courseId_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___version_2)); }
	inline float get_version_2() const { return ___version_2; }
	inline float* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(float value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_isActive_3() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isActive_3)); }
	inline bool get_isActive_3() const { return ___isActive_3; }
	inline bool* get_address_of_isActive_3() { return &___isActive_3; }
	inline void set_isActive_3(bool value)
	{
		___isActive_3 = value;
	}

	inline static int32_t get_offset_of_isSunday_4() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isSunday_4)); }
	inline bool get_isSunday_4() const { return ___isSunday_4; }
	inline bool* get_address_of_isSunday_4() { return &___isSunday_4; }
	inline void set_isSunday_4(bool value)
	{
		___isSunday_4 = value;
	}

	inline static int32_t get_offset_of_isMonday_5() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isMonday_5)); }
	inline bool get_isMonday_5() const { return ___isMonday_5; }
	inline bool* get_address_of_isMonday_5() { return &___isMonday_5; }
	inline void set_isMonday_5(bool value)
	{
		___isMonday_5 = value;
	}

	inline static int32_t get_offset_of_isTuesday_6() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isTuesday_6)); }
	inline bool get_isTuesday_6() const { return ___isTuesday_6; }
	inline bool* get_address_of_isTuesday_6() { return &___isTuesday_6; }
	inline void set_isTuesday_6(bool value)
	{
		___isTuesday_6 = value;
	}

	inline static int32_t get_offset_of_isWednesday_7() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isWednesday_7)); }
	inline bool get_isWednesday_7() const { return ___isWednesday_7; }
	inline bool* get_address_of_isWednesday_7() { return &___isWednesday_7; }
	inline void set_isWednesday_7(bool value)
	{
		___isWednesday_7 = value;
	}

	inline static int32_t get_offset_of_isThursday_8() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isThursday_8)); }
	inline bool get_isThursday_8() const { return ___isThursday_8; }
	inline bool* get_address_of_isThursday_8() { return &___isThursday_8; }
	inline void set_isThursday_8(bool value)
	{
		___isThursday_8 = value;
	}

	inline static int32_t get_offset_of_isFriday_9() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isFriday_9)); }
	inline bool get_isFriday_9() const { return ___isFriday_9; }
	inline bool* get_address_of_isFriday_9() { return &___isFriday_9; }
	inline void set_isFriday_9(bool value)
	{
		___isFriday_9 = value;
	}

	inline static int32_t get_offset_of_isSaturday_10() { return static_cast<int32_t>(offsetof(CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1, ___isSaturday_10)); }
	inline bool get_isSaturday_10() const { return ___isSaturday_10; }
	inline bool* get_address_of_isSaturday_10() { return &___isSaturday_10; }
	inline void set_isSaturday_10(bool value)
	{
		___isSaturday_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSECALENDERSETTINGS_T3648F4699BFE264FFFBF537C2B0655E969B7F6B1_H
#ifndef COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#define COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseData
struct  CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.CourseData::courseId
	int32_t ___courseId_0;
	// System.Int32 Strackaline.CourseData::sourceId
	int32_t ___sourceId_1;
	// System.String Strackaline.CourseData::name
	String_t* ___name_2;
	// System.String Strackaline.CourseData::address
	String_t* ___address_3;
	// System.String Strackaline.CourseData::city
	String_t* ___city_4;
	// System.String Strackaline.CourseData::region
	String_t* ___region_5;
	// System.String Strackaline.CourseData::postalCode
	String_t* ___postalCode_6;
	// System.String Strackaline.CourseData::countryCode
	String_t* ___countryCode_7;
	// System.String Strackaline.CourseData::profileImage
	String_t* ___profileImage_8;
	// System.Double Strackaline.CourseData::latitude
	double ___latitude_9;
	// System.Double Strackaline.CourseData::longitude
	double ___longitude_10;
	// System.Boolean Strackaline.CourseData::isActive
	bool ___isActive_11;
	// Strackaline.CourseSettings Strackaline.CourseData::settings
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * ___settings_12;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_sourceId_1() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___sourceId_1)); }
	inline int32_t get_sourceId_1() const { return ___sourceId_1; }
	inline int32_t* get_address_of_sourceId_1() { return &___sourceId_1; }
	inline void set_sourceId_1(int32_t value)
	{
		___sourceId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_address_3() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___address_3)); }
	inline String_t* get_address_3() const { return ___address_3; }
	inline String_t** get_address_of_address_3() { return &___address_3; }
	inline void set_address_3(String_t* value)
	{
		___address_3 = value;
		Il2CppCodeGenWriteBarrier((&___address_3), value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___city_4)); }
	inline String_t* get_city_4() const { return ___city_4; }
	inline String_t** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(String_t* value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier((&___city_4), value);
	}

	inline static int32_t get_offset_of_region_5() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___region_5)); }
	inline String_t* get_region_5() const { return ___region_5; }
	inline String_t** get_address_of_region_5() { return &___region_5; }
	inline void set_region_5(String_t* value)
	{
		___region_5 = value;
		Il2CppCodeGenWriteBarrier((&___region_5), value);
	}

	inline static int32_t get_offset_of_postalCode_6() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___postalCode_6)); }
	inline String_t* get_postalCode_6() const { return ___postalCode_6; }
	inline String_t** get_address_of_postalCode_6() { return &___postalCode_6; }
	inline void set_postalCode_6(String_t* value)
	{
		___postalCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___postalCode_6), value);
	}

	inline static int32_t get_offset_of_countryCode_7() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___countryCode_7)); }
	inline String_t* get_countryCode_7() const { return ___countryCode_7; }
	inline String_t** get_address_of_countryCode_7() { return &___countryCode_7; }
	inline void set_countryCode_7(String_t* value)
	{
		___countryCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___countryCode_7), value);
	}

	inline static int32_t get_offset_of_profileImage_8() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___profileImage_8)); }
	inline String_t* get_profileImage_8() const { return ___profileImage_8; }
	inline String_t** get_address_of_profileImage_8() { return &___profileImage_8; }
	inline void set_profileImage_8(String_t* value)
	{
		___profileImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___profileImage_8), value);
	}

	inline static int32_t get_offset_of_latitude_9() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___latitude_9)); }
	inline double get_latitude_9() const { return ___latitude_9; }
	inline double* get_address_of_latitude_9() { return &___latitude_9; }
	inline void set_latitude_9(double value)
	{
		___latitude_9 = value;
	}

	inline static int32_t get_offset_of_longitude_10() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___longitude_10)); }
	inline double get_longitude_10() const { return ___longitude_10; }
	inline double* get_address_of_longitude_10() { return &___longitude_10; }
	inline void set_longitude_10(double value)
	{
		___longitude_10 = value;
	}

	inline static int32_t get_offset_of_isActive_11() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___isActive_11)); }
	inline bool get_isActive_11() const { return ___isActive_11; }
	inline bool* get_address_of_isActive_11() { return &___isActive_11; }
	inline void set_isActive_11(bool value)
	{
		___isActive_11 = value;
	}

	inline static int32_t get_offset_of_settings_12() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___settings_12)); }
	inline CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * get_settings_12() const { return ___settings_12; }
	inline CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 ** get_address_of_settings_12() { return &___settings_12; }
	inline void set_settings_12(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * value)
	{
		___settings_12 = value;
		Il2CppCodeGenWriteBarrier((&___settings_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#ifndef COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#define COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseGreens
struct  CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.CourseGreens::courseId
	int32_t ___courseId_0;
	// System.String Strackaline.CourseGreens::name
	String_t* ___name_1;
	// Strackaline.GreenGroup[] Strackaline.CourseGreens::groups
	GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* ___groups_2;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_groups_2() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___groups_2)); }
	inline GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* get_groups_2() const { return ___groups_2; }
	inline GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D** get_address_of_groups_2() { return &___groups_2; }
	inline void set_groups_2(GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* value)
	{
		___groups_2 = value;
		Il2CppCodeGenWriteBarrier((&___groups_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#ifndef COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#define COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseSettings
struct  CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352  : public RuntimeObject
{
public:
	// System.Boolean Strackaline.CourseSettings::isHoleLocationSoftware
	bool ___isHoleLocationSoftware_0;
	// System.Int32 Strackaline.CourseSettings::holoVersion
	int32_t ___holoVersion_1;
	// System.Boolean Strackaline.CourseSettings::isLockedFromSale
	bool ___isLockedFromSale_2;
	// System.String Strackaline.CourseSettings::customLockMessage
	String_t* ___customLockMessage_3;
	// System.Int32 Strackaline.CourseSettings::lastPinsheetTemplateId
	int32_t ___lastPinsheetTemplateId_4;
	// System.Boolean Strackaline.CourseSettings::isCourseDataLocked
	bool ___isCourseDataLocked_5;
	// System.Boolean Strackaline.CourseSettings::isGreenDataLocked
	bool ___isGreenDataLocked_6;
	// System.Boolean Strackaline.CourseSettings::isGpsDataLocked
	bool ___isGpsDataLocked_7;

public:
	inline static int32_t get_offset_of_isHoleLocationSoftware_0() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isHoleLocationSoftware_0)); }
	inline bool get_isHoleLocationSoftware_0() const { return ___isHoleLocationSoftware_0; }
	inline bool* get_address_of_isHoleLocationSoftware_0() { return &___isHoleLocationSoftware_0; }
	inline void set_isHoleLocationSoftware_0(bool value)
	{
		___isHoleLocationSoftware_0 = value;
	}

	inline static int32_t get_offset_of_holoVersion_1() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___holoVersion_1)); }
	inline int32_t get_holoVersion_1() const { return ___holoVersion_1; }
	inline int32_t* get_address_of_holoVersion_1() { return &___holoVersion_1; }
	inline void set_holoVersion_1(int32_t value)
	{
		___holoVersion_1 = value;
	}

	inline static int32_t get_offset_of_isLockedFromSale_2() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isLockedFromSale_2)); }
	inline bool get_isLockedFromSale_2() const { return ___isLockedFromSale_2; }
	inline bool* get_address_of_isLockedFromSale_2() { return &___isLockedFromSale_2; }
	inline void set_isLockedFromSale_2(bool value)
	{
		___isLockedFromSale_2 = value;
	}

	inline static int32_t get_offset_of_customLockMessage_3() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___customLockMessage_3)); }
	inline String_t* get_customLockMessage_3() const { return ___customLockMessage_3; }
	inline String_t** get_address_of_customLockMessage_3() { return &___customLockMessage_3; }
	inline void set_customLockMessage_3(String_t* value)
	{
		___customLockMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___customLockMessage_3), value);
	}

	inline static int32_t get_offset_of_lastPinsheetTemplateId_4() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___lastPinsheetTemplateId_4)); }
	inline int32_t get_lastPinsheetTemplateId_4() const { return ___lastPinsheetTemplateId_4; }
	inline int32_t* get_address_of_lastPinsheetTemplateId_4() { return &___lastPinsheetTemplateId_4; }
	inline void set_lastPinsheetTemplateId_4(int32_t value)
	{
		___lastPinsheetTemplateId_4 = value;
	}

	inline static int32_t get_offset_of_isCourseDataLocked_5() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isCourseDataLocked_5)); }
	inline bool get_isCourseDataLocked_5() const { return ___isCourseDataLocked_5; }
	inline bool* get_address_of_isCourseDataLocked_5() { return &___isCourseDataLocked_5; }
	inline void set_isCourseDataLocked_5(bool value)
	{
		___isCourseDataLocked_5 = value;
	}

	inline static int32_t get_offset_of_isGreenDataLocked_6() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isGreenDataLocked_6)); }
	inline bool get_isGreenDataLocked_6() const { return ___isGreenDataLocked_6; }
	inline bool* get_address_of_isGreenDataLocked_6() { return &___isGreenDataLocked_6; }
	inline void set_isGreenDataLocked_6(bool value)
	{
		___isGreenDataLocked_6 = value;
	}

	inline static int32_t get_offset_of_isGpsDataLocked_7() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isGpsDataLocked_7)); }
	inline bool get_isGpsDataLocked_7() const { return ___isGpsDataLocked_7; }
	inline bool* get_address_of_isGpsDataLocked_7() { return &___isGpsDataLocked_7; }
	inline void set_isGpsDataLocked_7(bool value)
	{
		___isGpsDataLocked_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#ifndef DAILYTEMPLATE_T5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED_H
#define DAILYTEMPLATE_T5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.DailyTemplate
struct  DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.DailyTemplate::dailyTemplateId
	int32_t ___dailyTemplateId_0;
	// System.Int32 Strackaline.DailyTemplate::dailyTemplateTypeId
	int32_t ___dailyTemplateTypeId_1;
	// System.String Strackaline.DailyTemplate::name
	String_t* ___name_2;
	// System.Boolean Strackaline.DailyTemplate::isActive
	bool ___isActive_3;
	// Strackaline.TemplateHoleLocation[] Strackaline.DailyTemplate::holeLocations
	TemplateHoleLocationU5BU5D_t4D7C597F0890863D5E5B359A052EEEE0C469CCD2* ___holeLocations_4;

public:
	inline static int32_t get_offset_of_dailyTemplateId_0() { return static_cast<int32_t>(offsetof(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED, ___dailyTemplateId_0)); }
	inline int32_t get_dailyTemplateId_0() const { return ___dailyTemplateId_0; }
	inline int32_t* get_address_of_dailyTemplateId_0() { return &___dailyTemplateId_0; }
	inline void set_dailyTemplateId_0(int32_t value)
	{
		___dailyTemplateId_0 = value;
	}

	inline static int32_t get_offset_of_dailyTemplateTypeId_1() { return static_cast<int32_t>(offsetof(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED, ___dailyTemplateTypeId_1)); }
	inline int32_t get_dailyTemplateTypeId_1() const { return ___dailyTemplateTypeId_1; }
	inline int32_t* get_address_of_dailyTemplateTypeId_1() { return &___dailyTemplateTypeId_1; }
	inline void set_dailyTemplateTypeId_1(int32_t value)
	{
		___dailyTemplateTypeId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_isActive_3() { return static_cast<int32_t>(offsetof(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED, ___isActive_3)); }
	inline bool get_isActive_3() const { return ___isActive_3; }
	inline bool* get_address_of_isActive_3() { return &___isActive_3; }
	inline void set_isActive_3(bool value)
	{
		___isActive_3 = value;
	}

	inline static int32_t get_offset_of_holeLocations_4() { return static_cast<int32_t>(offsetof(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED, ___holeLocations_4)); }
	inline TemplateHoleLocationU5BU5D_t4D7C597F0890863D5E5B359A052EEEE0C469CCD2* get_holeLocations_4() const { return ___holeLocations_4; }
	inline TemplateHoleLocationU5BU5D_t4D7C597F0890863D5E5B359A052EEEE0C469CCD2** get_address_of_holeLocations_4() { return &___holeLocations_4; }
	inline void set_holeLocations_4(TemplateHoleLocationU5BU5D_t4D7C597F0890863D5E5B359A052EEEE0C469CCD2* value)
	{
		___holeLocations_4 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocations_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAILYTEMPLATE_T5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED_H
#ifndef ELEVATIONDATAFORTRANSFER_TBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D_H
#define ELEVATIONDATAFORTRANSFER_TBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ElevationDataForTransfer
struct  ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Strackaline.Arrow> Strackaline.ElevationDataForTransfer::arrowCollectedList
	List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * ___arrowCollectedList_0;
	// System.Collections.Generic.List`1<Strackaline.ContourPath> Strackaline.ElevationDataForTransfer::contourCollectedList
	List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * ___contourCollectedList_1;

public:
	inline static int32_t get_offset_of_arrowCollectedList_0() { return static_cast<int32_t>(offsetof(ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D, ___arrowCollectedList_0)); }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * get_arrowCollectedList_0() const { return ___arrowCollectedList_0; }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 ** get_address_of_arrowCollectedList_0() { return &___arrowCollectedList_0; }
	inline void set_arrowCollectedList_0(List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * value)
	{
		___arrowCollectedList_0 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCollectedList_0), value);
	}

	inline static int32_t get_offset_of_contourCollectedList_1() { return static_cast<int32_t>(offsetof(ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D, ___contourCollectedList_1)); }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * get_contourCollectedList_1() const { return ___contourCollectedList_1; }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D ** get_address_of_contourCollectedList_1() { return &___contourCollectedList_1; }
	inline void set_contourCollectedList_1(List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * value)
	{
		___contourCollectedList_1 = value;
		Il2CppCodeGenWriteBarrier((&___contourCollectedList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEVATIONDATAFORTRANSFER_TBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D_H
#ifndef GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#define GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Green
struct  Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6  : public RuntimeObject
{
public:
	// Strackaline.HoleLocation[] Strackaline.Green::holeLocations
	HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* ___holeLocations_0;
	// System.String Strackaline.Green::name
	String_t* ___name_1;
	// System.Single Strackaline.Green::rotation
	float ___rotation_2;
	// System.Int32 Strackaline.Green::greenId
	int32_t ___greenId_3;
	// System.Int32 Strackaline.Green::groupId
	int32_t ___groupId_4;
	// System.Int32 Strackaline.Green::number
	int32_t ___number_5;
	// System.Single Strackaline.Green::latitude
	float ___latitude_6;
	// System.Single Strackaline.Green::longitude
	float ___longitude_7;
	// System.Int32 Strackaline.Green::trueNorth
	int32_t ___trueNorth_8;
	// System.String Strackaline.Green::DXFFile
	String_t* ___DXFFile_9;

public:
	inline static int32_t get_offset_of_holeLocations_0() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___holeLocations_0)); }
	inline HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* get_holeLocations_0() const { return ___holeLocations_0; }
	inline HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2** get_address_of_holeLocations_0() { return &___holeLocations_0; }
	inline void set_holeLocations_0(HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* value)
	{
		___holeLocations_0 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocations_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_greenId_3() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___greenId_3)); }
	inline int32_t get_greenId_3() const { return ___greenId_3; }
	inline int32_t* get_address_of_greenId_3() { return &___greenId_3; }
	inline void set_greenId_3(int32_t value)
	{
		___greenId_3 = value;
	}

	inline static int32_t get_offset_of_groupId_4() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___groupId_4)); }
	inline int32_t get_groupId_4() const { return ___groupId_4; }
	inline int32_t* get_address_of_groupId_4() { return &___groupId_4; }
	inline void set_groupId_4(int32_t value)
	{
		___groupId_4 = value;
	}

	inline static int32_t get_offset_of_number_5() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___number_5)); }
	inline int32_t get_number_5() const { return ___number_5; }
	inline int32_t* get_address_of_number_5() { return &___number_5; }
	inline void set_number_5(int32_t value)
	{
		___number_5 = value;
	}

	inline static int32_t get_offset_of_latitude_6() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___latitude_6)); }
	inline float get_latitude_6() const { return ___latitude_6; }
	inline float* get_address_of_latitude_6() { return &___latitude_6; }
	inline void set_latitude_6(float value)
	{
		___latitude_6 = value;
	}

	inline static int32_t get_offset_of_longitude_7() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___longitude_7)); }
	inline float get_longitude_7() const { return ___longitude_7; }
	inline float* get_address_of_longitude_7() { return &___longitude_7; }
	inline void set_longitude_7(float value)
	{
		___longitude_7 = value;
	}

	inline static int32_t get_offset_of_trueNorth_8() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___trueNorth_8)); }
	inline int32_t get_trueNorth_8() const { return ___trueNorth_8; }
	inline int32_t* get_address_of_trueNorth_8() { return &___trueNorth_8; }
	inline void set_trueNorth_8(int32_t value)
	{
		___trueNorth_8 = value;
	}

	inline static int32_t get_offset_of_DXFFile_9() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___DXFFile_9)); }
	inline String_t* get_DXFFile_9() const { return ___DXFFile_9; }
	inline String_t** get_address_of_DXFFile_9() { return &___DXFFile_9; }
	inline void set_DXFFile_9(String_t* value)
	{
		___DXFFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___DXFFile_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#ifndef GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#define GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenActivity
struct  GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B  : public RuntimeObject
{
public:
	// System.String Strackaline.GreenActivity::greenId
	String_t* ___greenId_0;
	// System.Single Strackaline.GreenActivity::holeXPos
	float ___holeXPos_1;
	// System.Single Strackaline.GreenActivity::holeYPos
	float ___holeYPos_2;
	// System.Boolean Strackaline.GreenActivity::isPlaying
	bool ___isPlaying_3;
	// System.Boolean Strackaline.GreenActivity::isLooping
	bool ___isLooping_4;
	// System.Int32 Strackaline.GreenActivity::speed
	int32_t ___speed_5;
	// Strackaline.GreenCameraSettings Strackaline.GreenActivity::greenCameraSettings
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * ___greenCameraSettings_6;
	// Strackaline.HoleContainerSettings Strackaline.GreenActivity::holeContainerSettings
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * ___holeContainerSettings_7;
	// Strackaline.GreenPutt[] Strackaline.GreenActivity::greenPutts
	GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* ___greenPutts_8;

public:
	inline static int32_t get_offset_of_greenId_0() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenId_0)); }
	inline String_t* get_greenId_0() const { return ___greenId_0; }
	inline String_t** get_address_of_greenId_0() { return &___greenId_0; }
	inline void set_greenId_0(String_t* value)
	{
		___greenId_0 = value;
		Il2CppCodeGenWriteBarrier((&___greenId_0), value);
	}

	inline static int32_t get_offset_of_holeXPos_1() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeXPos_1)); }
	inline float get_holeXPos_1() const { return ___holeXPos_1; }
	inline float* get_address_of_holeXPos_1() { return &___holeXPos_1; }
	inline void set_holeXPos_1(float value)
	{
		___holeXPos_1 = value;
	}

	inline static int32_t get_offset_of_holeYPos_2() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeYPos_2)); }
	inline float get_holeYPos_2() const { return ___holeYPos_2; }
	inline float* get_address_of_holeYPos_2() { return &___holeYPos_2; }
	inline void set_holeYPos_2(float value)
	{
		___holeYPos_2 = value;
	}

	inline static int32_t get_offset_of_isPlaying_3() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___isPlaying_3)); }
	inline bool get_isPlaying_3() const { return ___isPlaying_3; }
	inline bool* get_address_of_isPlaying_3() { return &___isPlaying_3; }
	inline void set_isPlaying_3(bool value)
	{
		___isPlaying_3 = value;
	}

	inline static int32_t get_offset_of_isLooping_4() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___isLooping_4)); }
	inline bool get_isLooping_4() const { return ___isLooping_4; }
	inline bool* get_address_of_isLooping_4() { return &___isLooping_4; }
	inline void set_isLooping_4(bool value)
	{
		___isLooping_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___speed_5)); }
	inline int32_t get_speed_5() const { return ___speed_5; }
	inline int32_t* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(int32_t value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_greenCameraSettings_6() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenCameraSettings_6)); }
	inline GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * get_greenCameraSettings_6() const { return ___greenCameraSettings_6; }
	inline GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA ** get_address_of_greenCameraSettings_6() { return &___greenCameraSettings_6; }
	inline void set_greenCameraSettings_6(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * value)
	{
		___greenCameraSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___greenCameraSettings_6), value);
	}

	inline static int32_t get_offset_of_holeContainerSettings_7() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeContainerSettings_7)); }
	inline HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * get_holeContainerSettings_7() const { return ___holeContainerSettings_7; }
	inline HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 ** get_address_of_holeContainerSettings_7() { return &___holeContainerSettings_7; }
	inline void set_holeContainerSettings_7(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * value)
	{
		___holeContainerSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainerSettings_7), value);
	}

	inline static int32_t get_offset_of_greenPutts_8() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenPutts_8)); }
	inline GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* get_greenPutts_8() const { return ___greenPutts_8; }
	inline GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E** get_address_of_greenPutts_8() { return &___greenPutts_8; }
	inline void set_greenPutts_8(GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* value)
	{
		___greenPutts_8 = value;
		Il2CppCodeGenWriteBarrier((&___greenPutts_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#ifndef GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#define GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenArrow
struct  GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A  : public RuntimeObject
{
public:
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows1
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows1_0;
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows2
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows2_1;
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows3
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows3_2;

public:
	inline static int32_t get_offset_of_arrows1_0() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows1_0)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows1_0() const { return ___arrows1_0; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows1_0() { return &___arrows1_0; }
	inline void set_arrows1_0(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows1_0 = value;
		Il2CppCodeGenWriteBarrier((&___arrows1_0), value);
	}

	inline static int32_t get_offset_of_arrows2_1() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows2_1)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows2_1() const { return ___arrows2_1; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows2_1() { return &___arrows2_1; }
	inline void set_arrows2_1(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows2_1 = value;
		Il2CppCodeGenWriteBarrier((&___arrows2_1), value);
	}

	inline static int32_t get_offset_of_arrows3_2() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows3_2)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows3_2() const { return ___arrows3_2; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows3_2() { return &___arrows3_2; }
	inline void set_arrows3_2(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows3_2 = value;
		Il2CppCodeGenWriteBarrier((&___arrows3_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#ifndef GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#define GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenCameraSettings
struct  GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA  : public RuntimeObject
{
public:
	// System.Single Strackaline.GreenCameraSettings::xPos
	float ___xPos_0;
	// System.Single Strackaline.GreenCameraSettings::yPos
	float ___yPos_1;
	// System.Single Strackaline.GreenCameraSettings::zPos
	float ___zPos_2;
	// System.Single Strackaline.GreenCameraSettings::size
	float ___size_3;
	// System.Single Strackaline.GreenCameraSettings::yRot
	float ___yRot_4;

public:
	inline static int32_t get_offset_of_xPos_0() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___xPos_0)); }
	inline float get_xPos_0() const { return ___xPos_0; }
	inline float* get_address_of_xPos_0() { return &___xPos_0; }
	inline void set_xPos_0(float value)
	{
		___xPos_0 = value;
	}

	inline static int32_t get_offset_of_yPos_1() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___yPos_1)); }
	inline float get_yPos_1() const { return ___yPos_1; }
	inline float* get_address_of_yPos_1() { return &___yPos_1; }
	inline void set_yPos_1(float value)
	{
		___yPos_1 = value;
	}

	inline static int32_t get_offset_of_zPos_2() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___zPos_2)); }
	inline float get_zPos_2() const { return ___zPos_2; }
	inline float* get_address_of_zPos_2() { return &___zPos_2; }
	inline void set_zPos_2(float value)
	{
		___zPos_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___size_3)); }
	inline float get_size_3() const { return ___size_3; }
	inline float* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(float value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_yRot_4() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___yRot_4)); }
	inline float get_yRot_4() const { return ___yRot_4; }
	inline float* get_address_of_yRot_4() { return &___yRot_4; }
	inline void set_yRot_4(float value)
	{
		___yRot_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#ifndef GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#define GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenPutt
struct  GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF  : public RuntimeObject
{
public:
	// System.Single Strackaline.GreenPutt::ballXPos
	float ___ballXPos_0;
	// System.Single Strackaline.GreenPutt::ballYPos
	float ___ballYPos_1;
	// System.Int32 Strackaline.GreenPutt::puttId
	int32_t ___puttId_2;

public:
	inline static int32_t get_offset_of_ballXPos_0() { return static_cast<int32_t>(offsetof(GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF, ___ballXPos_0)); }
	inline float get_ballXPos_0() const { return ___ballXPos_0; }
	inline float* get_address_of_ballXPos_0() { return &___ballXPos_0; }
	inline void set_ballXPos_0(float value)
	{
		___ballXPos_0 = value;
	}

	inline static int32_t get_offset_of_ballYPos_1() { return static_cast<int32_t>(offsetof(GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF, ___ballYPos_1)); }
	inline float get_ballYPos_1() const { return ___ballYPos_1; }
	inline float* get_address_of_ballYPos_1() { return &___ballYPos_1; }
	inline void set_ballYPos_1(float value)
	{
		___ballYPos_1 = value;
	}

	inline static int32_t get_offset_of_puttId_2() { return static_cast<int32_t>(offsetof(GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF, ___puttId_2)); }
	inline int32_t get_puttId_2() const { return ___puttId_2; }
	inline int32_t* get_address_of_puttId_2() { return &___puttId_2; }
	inline void set_puttId_2(int32_t value)
	{
		___puttId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#ifndef HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#define HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Hole
struct  Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB  : public RuntimeObject
{
public:
	// System.Single Strackaline.Hole::x
	float ___x_0;
	// System.Single Strackaline.Hole::y
	float ___y_1;
	// System.Single Strackaline.Hole::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#ifndef HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#define HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.HoleContainerSettings
struct  HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175  : public RuntimeObject
{
public:
	// System.Single Strackaline.HoleContainerSettings::xPos
	float ___xPos_0;
	// System.Single Strackaline.HoleContainerSettings::yPos
	float ___yPos_1;
	// System.Single Strackaline.HoleContainerSettings::zPos
	float ___zPos_2;
	// System.Single Strackaline.HoleContainerSettings::holeRectYRotation
	float ___holeRectYRotation_3;

public:
	inline static int32_t get_offset_of_xPos_0() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___xPos_0)); }
	inline float get_xPos_0() const { return ___xPos_0; }
	inline float* get_address_of_xPos_0() { return &___xPos_0; }
	inline void set_xPos_0(float value)
	{
		___xPos_0 = value;
	}

	inline static int32_t get_offset_of_yPos_1() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___yPos_1)); }
	inline float get_yPos_1() const { return ___yPos_1; }
	inline float* get_address_of_yPos_1() { return &___yPos_1; }
	inline void set_yPos_1(float value)
	{
		___yPos_1 = value;
	}

	inline static int32_t get_offset_of_zPos_2() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___zPos_2)); }
	inline float get_zPos_2() const { return ___zPos_2; }
	inline float* get_address_of_zPos_2() { return &___zPos_2; }
	inline void set_zPos_2(float value)
	{
		___zPos_2 = value;
	}

	inline static int32_t get_offset_of_holeRectYRotation_3() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___holeRectYRotation_3)); }
	inline float get_holeRectYRotation_3() const { return ___holeRectYRotation_3; }
	inline float* get_address_of_holeRectYRotation_3() { return &___holeRectYRotation_3; }
	inline void set_holeRectYRotation_3(float value)
	{
		___holeRectYRotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#ifndef HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#define HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.HoleLocation
struct  HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.HoleLocation::id
	int32_t ___id_0;
	// System.Int32 Strackaline.HoleLocation::greenId
	int32_t ___greenId_1;
	// System.Single Strackaline.HoleLocation::rotation
	float ___rotation_2;
	// System.Single Strackaline.HoleLocation::x
	float ___x_3;
	// System.Single Strackaline.HoleLocation::y
	float ___y_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_greenId_1() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___greenId_1)); }
	inline int32_t get_greenId_1() const { return ___greenId_1; }
	inline int32_t* get_address_of_greenId_1() { return &___greenId_1; }
	inline void set_greenId_1(int32_t value)
	{
		___greenId_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___x_3)); }
	inline float get_x_3() const { return ___x_3; }
	inline float* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(float value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___y_4)); }
	inline float get_y_4() const { return ___y_4; }
	inline float* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(float value)
	{
		___y_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#ifndef INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#define INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Instruction
struct  Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C  : public RuntimeObject
{
public:
	// System.String Strackaline.Instruction::aim
	String_t* ___aim_0;
	// System.String Strackaline.Instruction::weight
	String_t* ___weight_1;
	// System.String Strackaline.Instruction::toHole
	String_t* ___toHole_2;
	// System.String Strackaline.Instruction::elevation
	String_t* ___elevation_3;

public:
	inline static int32_t get_offset_of_aim_0() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___aim_0)); }
	inline String_t* get_aim_0() const { return ___aim_0; }
	inline String_t** get_address_of_aim_0() { return &___aim_0; }
	inline void set_aim_0(String_t* value)
	{
		___aim_0 = value;
		Il2CppCodeGenWriteBarrier((&___aim_0), value);
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___weight_1)); }
	inline String_t* get_weight_1() const { return ___weight_1; }
	inline String_t** get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(String_t* value)
	{
		___weight_1 = value;
		Il2CppCodeGenWriteBarrier((&___weight_1), value);
	}

	inline static int32_t get_offset_of_toHole_2() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___toHole_2)); }
	inline String_t* get_toHole_2() const { return ___toHole_2; }
	inline String_t** get_address_of_toHole_2() { return &___toHole_2; }
	inline void set_toHole_2(String_t* value)
	{
		___toHole_2 = value;
		Il2CppCodeGenWriteBarrier((&___toHole_2), value);
	}

	inline static int32_t get_offset_of_elevation_3() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___elevation_3)); }
	inline String_t* get_elevation_3() const { return ___elevation_3; }
	inline String_t** get_address_of_elevation_3() { return &___elevation_3; }
	inline void set_elevation_3(String_t* value)
	{
		___elevation_3 = value;
		Il2CppCodeGenWriteBarrier((&___elevation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#ifndef JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#define JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Jack
struct  Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Jack::index
	int32_t ___index_0;
	// System.Single Strackaline.Jack::elevation
	float ___elevation_1;
	// System.Single Strackaline.Jack::offset
	float ___offset_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_elevation_1() { return static_cast<int32_t>(offsetof(Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC, ___elevation_1)); }
	inline float get_elevation_1() const { return ___elevation_1; }
	inline float* get_address_of_elevation_1() { return &___elevation_1; }
	inline void set_elevation_1(float value)
	{
		___elevation_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC, ___offset_2)); }
	inline float get_offset_2() const { return ___offset_2; }
	inline float* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(float value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#ifndef JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#define JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.JackData
struct  JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Strackaline.Jack> Strackaline.JackData::jacks
	List_1_tC846C91557298786F744DB8E2A439B8F3F6B06AF * ___jacks_0;
	// System.Int32 Strackaline.JackData::jackId
	int32_t ___jackId_1;
	// System.String Strackaline.JackData::location
	String_t* ___location_2;

public:
	inline static int32_t get_offset_of_jacks_0() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___jacks_0)); }
	inline List_1_tC846C91557298786F744DB8E2A439B8F3F6B06AF * get_jacks_0() const { return ___jacks_0; }
	inline List_1_tC846C91557298786F744DB8E2A439B8F3F6B06AF ** get_address_of_jacks_0() { return &___jacks_0; }
	inline void set_jacks_0(List_1_tC846C91557298786F744DB8E2A439B8F3F6B06AF * value)
	{
		___jacks_0 = value;
		Il2CppCodeGenWriteBarrier((&___jacks_0), value);
	}

	inline static int32_t get_offset_of_jackId_1() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___jackId_1)); }
	inline int32_t get_jackId_1() const { return ___jackId_1; }
	inline int32_t* get_address_of_jackId_1() { return &___jackId_1; }
	inline void set_jackId_1(int32_t value)
	{
		___jackId_1 = value;
	}

	inline static int32_t get_offset_of_location_2() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___location_2)); }
	inline String_t* get_location_2() const { return ___location_2; }
	inline String_t** get_address_of_location_2() { return &___location_2; }
	inline void set_location_2(String_t* value)
	{
		___location_2 = value;
		Il2CppCodeGenWriteBarrier((&___location_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#ifndef LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#define LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Location
struct  Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6  : public RuntimeObject
{
public:
	// System.Single Strackaline.Location::x
	float ___x_0;
	// System.Single Strackaline.Location::y
	float ___y_1;
	// System.Single Strackaline.Location::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#ifndef MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#define MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.MemberDetail
struct  MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B  : public RuntimeObject
{
public:
	// System.String Strackaline.MemberDetail::emailAddress
	String_t* ___emailAddress_0;
	// Strackaline.Roles[] Strackaline.MemberDetail::roles
	RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* ___roles_1;
	// Strackaline.UserCourse[] Strackaline.MemberDetail::courses
	UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* ___courses_2;
	// Strackaline.UserPrinter[] Strackaline.MemberDetail::printers
	UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* ___printers_3;

public:
	inline static int32_t get_offset_of_emailAddress_0() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___emailAddress_0)); }
	inline String_t* get_emailAddress_0() const { return ___emailAddress_0; }
	inline String_t** get_address_of_emailAddress_0() { return &___emailAddress_0; }
	inline void set_emailAddress_0(String_t* value)
	{
		___emailAddress_0 = value;
		Il2CppCodeGenWriteBarrier((&___emailAddress_0), value);
	}

	inline static int32_t get_offset_of_roles_1() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___roles_1)); }
	inline RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* get_roles_1() const { return ___roles_1; }
	inline RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E** get_address_of_roles_1() { return &___roles_1; }
	inline void set_roles_1(RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* value)
	{
		___roles_1 = value;
		Il2CppCodeGenWriteBarrier((&___roles_1), value);
	}

	inline static int32_t get_offset_of_courses_2() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___courses_2)); }
	inline UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* get_courses_2() const { return ___courses_2; }
	inline UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE** get_address_of_courses_2() { return &___courses_2; }
	inline void set_courses_2(UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* value)
	{
		___courses_2 = value;
		Il2CppCodeGenWriteBarrier((&___courses_2), value);
	}

	inline static int32_t get_offset_of_printers_3() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___printers_3)); }
	inline UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* get_printers_3() const { return ___printers_3; }
	inline UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601** get_address_of_printers_3() { return &___printers_3; }
	inline void set_printers_3(UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* value)
	{
		___printers_3 = value;
		Il2CppCodeGenWriteBarrier((&___printers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#ifndef POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#define POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Point
struct  Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2  : public RuntimeObject
{
public:
	// System.Single Strackaline.Point::speed
	float ___speed_0;
	// Strackaline.Location Strackaline.Point::location
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * ___location_1;
	// System.Single Strackaline.Point::slope
	float ___slope_2;

public:
	inline static int32_t get_offset_of_speed_0() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___speed_0)); }
	inline float get_speed_0() const { return ___speed_0; }
	inline float* get_address_of_speed_0() { return &___speed_0; }
	inline void set_speed_0(float value)
	{
		___speed_0 = value;
	}

	inline static int32_t get_offset_of_location_1() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___location_1)); }
	inline Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * get_location_1() const { return ___location_1; }
	inline Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 ** get_address_of_location_1() { return &___location_1; }
	inline void set_location_1(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * value)
	{
		___location_1 = value;
		Il2CppCodeGenWriteBarrier((&___location_1), value);
	}

	inline static int32_t get_offset_of_slope_2() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___slope_2)); }
	inline float get_slope_2() const { return ___slope_2; }
	inline float* get_address_of_slope_2() { return &___slope_2; }
	inline void set_slope_2(float value)
	{
		___slope_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#ifndef PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#define PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ProjectorData
struct  ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88  : public RuntimeObject
{
public:
	// Strackaline.Configurator[] Strackaline.ProjectorData::value
	ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88, ___value_0)); }
	inline ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* get_value_0() const { return ___value_0; }
	inline ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#ifndef PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#define PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Putt
struct  Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Putt::puttID
	int32_t ___puttID_0;
	// System.Boolean Strackaline.Putt::isValidPutt
	bool ___isValidPutt_1;
	// System.String Strackaline.Putt::message
	String_t* ___message_2;
	// Strackaline.Ball Strackaline.Putt::ball
	Ball_t094765B02879225F8C595564E40A823B70AEE45E * ___ball_3;
	// Strackaline.Hole Strackaline.Putt::hole
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * ___hole_4;
	// Strackaline.Target Strackaline.Putt::target
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * ___target_5;
	// System.Single Strackaline.Putt::ballToHoleDistance
	float ___ballToHoleDistance_6;
	// System.Single Strackaline.Putt::puttingDistance
	float ___puttingDistance_7;
	// System.Single Strackaline.Putt::correctionAngle
	float ___correctionAngle_8;
	// System.Single Strackaline.Putt::elevationChange
	float ___elevationChange_9;
	// System.Single Strackaline.Putt::speedFactor
	float ___speedFactor_10;
	// Strackaline.Point[] Strackaline.Putt::points
	PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* ___points_11;
	// Strackaline.Instruction Strackaline.Putt::instructions
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * ___instructions_12;

public:
	inline static int32_t get_offset_of_puttID_0() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___puttID_0)); }
	inline int32_t get_puttID_0() const { return ___puttID_0; }
	inline int32_t* get_address_of_puttID_0() { return &___puttID_0; }
	inline void set_puttID_0(int32_t value)
	{
		___puttID_0 = value;
	}

	inline static int32_t get_offset_of_isValidPutt_1() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___isValidPutt_1)); }
	inline bool get_isValidPutt_1() const { return ___isValidPutt_1; }
	inline bool* get_address_of_isValidPutt_1() { return &___isValidPutt_1; }
	inline void set_isValidPutt_1(bool value)
	{
		___isValidPutt_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_ball_3() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___ball_3)); }
	inline Ball_t094765B02879225F8C595564E40A823B70AEE45E * get_ball_3() const { return ___ball_3; }
	inline Ball_t094765B02879225F8C595564E40A823B70AEE45E ** get_address_of_ball_3() { return &___ball_3; }
	inline void set_ball_3(Ball_t094765B02879225F8C595564E40A823B70AEE45E * value)
	{
		___ball_3 = value;
		Il2CppCodeGenWriteBarrier((&___ball_3), value);
	}

	inline static int32_t get_offset_of_hole_4() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___hole_4)); }
	inline Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * get_hole_4() const { return ___hole_4; }
	inline Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB ** get_address_of_hole_4() { return &___hole_4; }
	inline void set_hole_4(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * value)
	{
		___hole_4 = value;
		Il2CppCodeGenWriteBarrier((&___hole_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___target_5)); }
	inline Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * get_target_5() const { return ___target_5; }
	inline Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_ballToHoleDistance_6() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___ballToHoleDistance_6)); }
	inline float get_ballToHoleDistance_6() const { return ___ballToHoleDistance_6; }
	inline float* get_address_of_ballToHoleDistance_6() { return &___ballToHoleDistance_6; }
	inline void set_ballToHoleDistance_6(float value)
	{
		___ballToHoleDistance_6 = value;
	}

	inline static int32_t get_offset_of_puttingDistance_7() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___puttingDistance_7)); }
	inline float get_puttingDistance_7() const { return ___puttingDistance_7; }
	inline float* get_address_of_puttingDistance_7() { return &___puttingDistance_7; }
	inline void set_puttingDistance_7(float value)
	{
		___puttingDistance_7 = value;
	}

	inline static int32_t get_offset_of_correctionAngle_8() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___correctionAngle_8)); }
	inline float get_correctionAngle_8() const { return ___correctionAngle_8; }
	inline float* get_address_of_correctionAngle_8() { return &___correctionAngle_8; }
	inline void set_correctionAngle_8(float value)
	{
		___correctionAngle_8 = value;
	}

	inline static int32_t get_offset_of_elevationChange_9() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___elevationChange_9)); }
	inline float get_elevationChange_9() const { return ___elevationChange_9; }
	inline float* get_address_of_elevationChange_9() { return &___elevationChange_9; }
	inline void set_elevationChange_9(float value)
	{
		___elevationChange_9 = value;
	}

	inline static int32_t get_offset_of_speedFactor_10() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___speedFactor_10)); }
	inline float get_speedFactor_10() const { return ___speedFactor_10; }
	inline float* get_address_of_speedFactor_10() { return &___speedFactor_10; }
	inline void set_speedFactor_10(float value)
	{
		___speedFactor_10 = value;
	}

	inline static int32_t get_offset_of_points_11() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___points_11)); }
	inline PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* get_points_11() const { return ___points_11; }
	inline PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9** get_address_of_points_11() { return &___points_11; }
	inline void set_points_11(PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* value)
	{
		___points_11 = value;
		Il2CppCodeGenWriteBarrier((&___points_11), value);
	}

	inline static int32_t get_offset_of_instructions_12() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___instructions_12)); }
	inline Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * get_instructions_12() const { return ___instructions_12; }
	inline Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C ** get_address_of_instructions_12() { return &___instructions_12; }
	inline void set_instructions_12(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * value)
	{
		___instructions_12 = value;
		Il2CppCodeGenWriteBarrier((&___instructions_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#ifndef PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#define PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.PuttVariation
struct  PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31  : public RuntimeObject
{
public:
	// Strackaline.Putt[] Strackaline.PuttVariation::value
	PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* ___value_0;
	// System.Int32 Strackaline.PuttVariation::puttVariationID
	int32_t ___puttVariationID_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31, ___value_0)); }
	inline PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* get_value_0() const { return ___value_0; }
	inline PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_puttVariationID_1() { return static_cast<int32_t>(offsetof(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31, ___puttVariationID_1)); }
	inline int32_t get_puttVariationID_1() const { return ___puttVariationID_1; }
	inline int32_t* get_address_of_puttVariationID_1() { return &___puttVariationID_1; }
	inline void set_puttVariationID_1(int32_t value)
	{
		___puttVariationID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#ifndef REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#define REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Region
struct  Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E  : public RuntimeObject
{
public:
	// Strackaline.RegionData[] Strackaline.Region::value
	RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E, ___value_0)); }
	inline RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* get_value_0() const { return ___value_0; }
	inline RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#ifndef REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#define REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.RegionData
struct  RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F  : public RuntimeObject
{
public:
	// System.String Strackaline.RegionData::name
	String_t* ___name_0;
	// System.String Strackaline.RegionData::regionCode
	String_t* ___regionCode_1;
	// System.String Strackaline.RegionData::countryCode
	String_t* ___countryCode_2;
	// System.String Strackaline.RegionData::courseCount
	String_t* ___courseCount_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_regionCode_1() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___regionCode_1)); }
	inline String_t* get_regionCode_1() const { return ___regionCode_1; }
	inline String_t** get_address_of_regionCode_1() { return &___regionCode_1; }
	inline void set_regionCode_1(String_t* value)
	{
		___regionCode_1 = value;
		Il2CppCodeGenWriteBarrier((&___regionCode_1), value);
	}

	inline static int32_t get_offset_of_countryCode_2() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___countryCode_2)); }
	inline String_t* get_countryCode_2() const { return ___countryCode_2; }
	inline String_t** get_address_of_countryCode_2() { return &___countryCode_2; }
	inline void set_countryCode_2(String_t* value)
	{
		___countryCode_2 = value;
		Il2CppCodeGenWriteBarrier((&___countryCode_2), value);
	}

	inline static int32_t get_offset_of_courseCount_3() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___courseCount_3)); }
	inline String_t* get_courseCount_3() const { return ___courseCount_3; }
	inline String_t** get_address_of_courseCount_3() { return &___courseCount_3; }
	inline void set_courseCount_3(String_t* value)
	{
		___courseCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___courseCount_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#ifndef ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#define ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Roles
struct  Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Roles::roleId
	int32_t ___roleId_0;
	// System.String Strackaline.Roles::name
	String_t* ___name_1;
	// System.String Strackaline.Roles::domain
	String_t* ___domain_2;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___roleId_0)); }
	inline int32_t get_roleId_0() const { return ___roleId_0; }
	inline int32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(int32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_domain_2() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___domain_2)); }
	inline String_t* get_domain_2() const { return ___domain_2; }
	inline String_t** get_address_of_domain_2() { return &___domain_2; }
	inline void set_domain_2(String_t* value)
	{
		___domain_2 = value;
		Il2CppCodeGenWriteBarrier((&___domain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#ifndef TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#define TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Target
struct  Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139  : public RuntimeObject
{
public:
	// System.Single Strackaline.Target::x
	float ___x_0;
	// System.Single Strackaline.Target::y
	float ___y_1;
	// System.Single Strackaline.Target::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#ifndef TEMPLATEHOLELOCATION_TF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF_H
#define TEMPLATEHOLELOCATION_TF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.TemplateHoleLocation
struct  TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.TemplateHoleLocation::dailyTemplateHoleLocationId
	int32_t ___dailyTemplateHoleLocationId_0;
	// System.Int32 Strackaline.TemplateHoleLocation::greenId
	int32_t ___greenId_1;
	// System.Int32 Strackaline.TemplateHoleLocation::groupId
	int32_t ___groupId_2;
	// System.Int32 Strackaline.TemplateHoleLocation::holeNumber
	int32_t ___holeNumber_3;
	// System.Single Strackaline.TemplateHoleLocation::x
	float ___x_4;
	// System.Single Strackaline.TemplateHoleLocation::y
	float ___y_5;

public:
	inline static int32_t get_offset_of_dailyTemplateHoleLocationId_0() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___dailyTemplateHoleLocationId_0)); }
	inline int32_t get_dailyTemplateHoleLocationId_0() const { return ___dailyTemplateHoleLocationId_0; }
	inline int32_t* get_address_of_dailyTemplateHoleLocationId_0() { return &___dailyTemplateHoleLocationId_0; }
	inline void set_dailyTemplateHoleLocationId_0(int32_t value)
	{
		___dailyTemplateHoleLocationId_0 = value;
	}

	inline static int32_t get_offset_of_greenId_1() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___greenId_1)); }
	inline int32_t get_greenId_1() const { return ___greenId_1; }
	inline int32_t* get_address_of_greenId_1() { return &___greenId_1; }
	inline void set_greenId_1(int32_t value)
	{
		___greenId_1 = value;
	}

	inline static int32_t get_offset_of_groupId_2() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___groupId_2)); }
	inline int32_t get_groupId_2() const { return ___groupId_2; }
	inline int32_t* get_address_of_groupId_2() { return &___groupId_2; }
	inline void set_groupId_2(int32_t value)
	{
		___groupId_2 = value;
	}

	inline static int32_t get_offset_of_holeNumber_3() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___holeNumber_3)); }
	inline int32_t get_holeNumber_3() const { return ___holeNumber_3; }
	inline int32_t* get_address_of_holeNumber_3() { return &___holeNumber_3; }
	inline void set_holeNumber_3(int32_t value)
	{
		___holeNumber_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPLATEHOLELOCATION_TF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF_H
#ifndef USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#define USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.User
struct  User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371  : public RuntimeObject
{
public:
	// System.String Strackaline.User::name
	String_t* ___name_0;
	// System.String Strackaline.User::roboGreenID
	String_t* ___roboGreenID_1;
	// System.Int32 Strackaline.User::memberId
	int32_t ___memberId_2;
	// System.String Strackaline.User::token
	String_t* ___token_3;
	// Strackaline.MemberDetail Strackaline.User::memberDetail
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * ___memberDetail_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_roboGreenID_1() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___roboGreenID_1)); }
	inline String_t* get_roboGreenID_1() const { return ___roboGreenID_1; }
	inline String_t** get_address_of_roboGreenID_1() { return &___roboGreenID_1; }
	inline void set_roboGreenID_1(String_t* value)
	{
		___roboGreenID_1 = value;
		Il2CppCodeGenWriteBarrier((&___roboGreenID_1), value);
	}

	inline static int32_t get_offset_of_memberId_2() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberId_2)); }
	inline int32_t get_memberId_2() const { return ___memberId_2; }
	inline int32_t* get_address_of_memberId_2() { return &___memberId_2; }
	inline void set_memberId_2(int32_t value)
	{
		___memberId_2 = value;
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier((&___token_3), value);
	}

	inline static int32_t get_offset_of_memberDetail_4() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberDetail_4)); }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * get_memberDetail_4() const { return ___memberDetail_4; }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B ** get_address_of_memberDetail_4() { return &___memberDetail_4; }
	inline void set_memberDetail_4(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * value)
	{
		___memberDetail_4 = value;
		Il2CppCodeGenWriteBarrier((&___memberDetail_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#ifndef USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#define USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.UserCourse
struct  UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.UserCourse::courseId
	int32_t ___courseId_0;
	// System.Int32 Strackaline.UserCourse::sourceId
	int32_t ___sourceId_1;
	// System.String Strackaline.UserCourse::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_sourceId_1() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___sourceId_1)); }
	inline int32_t get_sourceId_1() const { return ___sourceId_1; }
	inline int32_t* get_address_of_sourceId_1() { return &___sourceId_1; }
	inline void set_sourceId_1(int32_t value)
	{
		___sourceId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#ifndef USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#define USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.UserPrinter
struct  UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.UserPrinter::printerId
	int32_t ___printerId_0;
	// System.String Strackaline.UserPrinter::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_printerId_0() { return static_cast<int32_t>(offsetof(UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838, ___printerId_0)); }
	inline int32_t get_printerId_0() const { return ___printerId_0; }
	inline int32_t* get_address_of_printerId_0() { return &___printerId_0; }
	inline void set_printerId_0(int32_t value)
	{
		___printerId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#define USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserSubmitData
struct  UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF  : public RuntimeObject
{
public:
	// System.String UserSubmitData::userName
	String_t* ___userName_0;
	// System.String UserSubmitData::password
	String_t* ___password_1;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#define DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttRythm_Direction
struct  Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845 
{
public:
	// System.Int32 PuttRythm_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#ifndef PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#define PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationManager_PuttLineSpeed
struct  PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD 
{
public:
	// System.Int32 SimulationManager_PuttLineSpeed::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#ifndef GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#define GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenGroup_GreenGroupType
struct  GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42 
{
public:
	// System.Int32 Strackaline.GreenGroup_GreenGroupType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#define BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized_BlurType
struct  BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized_BlurType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_TD22A6B2AD87D6A22FD745EC9E44EEF824400CD44_H
#ifndef RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#define RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized_Resolution
struct  Resolution_t8B57100A723C02C86B12199571699A420CA4B409 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized_Resolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Resolution_t8B57100A723C02C86B12199571699A420CA4B409, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T8B57100A723C02C86B12199571699A420CA4B409_H
#ifndef GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#define GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenGroup
struct  GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.GreenGroup::groupId
	int32_t ___groupId_0;
	// System.String Strackaline.GreenGroup::name
	String_t* ___name_1;
	// System.Int32 Strackaline.GreenGroup::sortOrder
	int32_t ___sortOrder_2;
	// System.Single Strackaline.GreenGroup::stimp
	float ___stimp_3;
	// Strackaline.Green[] Strackaline.GreenGroup::greens
	GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* ___greens_4;
	// Strackaline.GreenGroup_GreenGroupType Strackaline.GreenGroup::greenGroupType
	int32_t ___greenGroupType_5;

public:
	inline static int32_t get_offset_of_groupId_0() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___groupId_0)); }
	inline int32_t get_groupId_0() const { return ___groupId_0; }
	inline int32_t* get_address_of_groupId_0() { return &___groupId_0; }
	inline void set_groupId_0(int32_t value)
	{
		___groupId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_sortOrder_2() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___sortOrder_2)); }
	inline int32_t get_sortOrder_2() const { return ___sortOrder_2; }
	inline int32_t* get_address_of_sortOrder_2() { return &___sortOrder_2; }
	inline void set_sortOrder_2(int32_t value)
	{
		___sortOrder_2 = value;
	}

	inline static int32_t get_offset_of_stimp_3() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___stimp_3)); }
	inline float get_stimp_3() const { return ___stimp_3; }
	inline float* get_address_of_stimp_3() { return &___stimp_3; }
	inline void set_stimp_3(float value)
	{
		___stimp_3 = value;
	}

	inline static int32_t get_offset_of_greens_4() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___greens_4)); }
	inline GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* get_greens_4() const { return ___greens_4; }
	inline GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1** get_address_of_greens_4() { return &___greens_4; }
	inline void set_greens_4(GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* value)
	{
		___greens_4 = value;
		Il2CppCodeGenWriteBarrier((&___greens_4), value);
	}

	inline static int32_t get_offset_of_greenGroupType_5() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___greenGroupType_5)); }
	inline int32_t get_greenGroupType_5() const { return ___greenGroupType_5; }
	inline int32_t* get_address_of_greenGroupType_5() { return &___greenGroupType_5; }
	inline void set_greenGroupType_5(int32_t value)
	{
		___greenGroupType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ALERTPOPUP_T676CD1E44D14D59932D918F6DEDD73E67231168F_H
#define ALERTPOPUP_T676CD1E44D14D59932D918F6DEDD73E67231168F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlertPopup
struct  AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button AlertPopup::okayBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___okayBtn_4;
	// UnityEngine.UI.Button AlertPopup::cancelBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___cancelBtn_5;
	// UnityEngine.UI.Text AlertPopup::headerTxt
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___headerTxt_6;
	// TMPro.TextMeshProUGUI AlertPopup::bodyTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___bodyTxt_7;

public:
	inline static int32_t get_offset_of_okayBtn_4() { return static_cast<int32_t>(offsetof(AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F, ___okayBtn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_okayBtn_4() const { return ___okayBtn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_okayBtn_4() { return &___okayBtn_4; }
	inline void set_okayBtn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___okayBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&___okayBtn_4), value);
	}

	inline static int32_t get_offset_of_cancelBtn_5() { return static_cast<int32_t>(offsetof(AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F, ___cancelBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_cancelBtn_5() const { return ___cancelBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_cancelBtn_5() { return &___cancelBtn_5; }
	inline void set_cancelBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___cancelBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___cancelBtn_5), value);
	}

	inline static int32_t get_offset_of_headerTxt_6() { return static_cast<int32_t>(offsetof(AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F, ___headerTxt_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_headerTxt_6() const { return ___headerTxt_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_headerTxt_6() { return &___headerTxt_6; }
	inline void set_headerTxt_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___headerTxt_6 = value;
		Il2CppCodeGenWriteBarrier((&___headerTxt_6), value);
	}

	inline static int32_t get_offset_of_bodyTxt_7() { return static_cast<int32_t>(offsetof(AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F, ___bodyTxt_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_bodyTxt_7() const { return ___bodyTxt_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_bodyTxt_7() { return &___bodyTxt_7; }
	inline void set_bodyTxt_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___bodyTxt_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTxt_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTPOPUP_T676CD1E44D14D59932D918F6DEDD73E67231168F_H
#ifndef ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#define ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowManager
struct  ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ArrowManager::arrowPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowPrefab_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrowManager::arrowList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___arrowList_6;
	// UnityEngine.Color ArrowManager::arrowColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___arrowColor_7;
	// UnityEngine.Color ArrowManager::arrowPrefColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___arrowPrefColor_8;
	// UnityEngine.GameObject ArrowManager::arrowContGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowContGO_9;
	// System.Boolean ArrowManager::isArrowAnimating
	bool ___isArrowAnimating_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrowManager::arrowLevel1List
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___arrowLevel1List_11;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>> ArrowManager::arrowLevelsList
	List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * ___arrowLevelsList_12;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>> ArrowManager::arrowsAllLevelsList
	List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * ___arrowsAllLevelsList_13;
	// UnityEngine.GameObject ArrowManager::arrowCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowCont_14;
	// System.Single ArrowManager::maxArrowSpeed
	float ___maxArrowSpeed_15;
	// CourseManager ArrowManager::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_16;

public:
	inline static int32_t get_offset_of_arrowPrefab_5() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowPrefab_5() const { return ___arrowPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowPrefab_5() { return &___arrowPrefab_5; }
	inline void set_arrowPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___arrowPrefab_5), value);
	}

	inline static int32_t get_offset_of_arrowList_6() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowList_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_arrowList_6() const { return ___arrowList_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_arrowList_6() { return &___arrowList_6; }
	inline void set_arrowList_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___arrowList_6 = value;
		Il2CppCodeGenWriteBarrier((&___arrowList_6), value);
	}

	inline static int32_t get_offset_of_arrowColor_7() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_arrowColor_7() const { return ___arrowColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_arrowColor_7() { return &___arrowColor_7; }
	inline void set_arrowColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___arrowColor_7 = value;
	}

	inline static int32_t get_offset_of_arrowPrefColor_8() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowPrefColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_arrowPrefColor_8() const { return ___arrowPrefColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_arrowPrefColor_8() { return &___arrowPrefColor_8; }
	inline void set_arrowPrefColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___arrowPrefColor_8 = value;
	}

	inline static int32_t get_offset_of_arrowContGO_9() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowContGO_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowContGO_9() const { return ___arrowContGO_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowContGO_9() { return &___arrowContGO_9; }
	inline void set_arrowContGO_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowContGO_9 = value;
		Il2CppCodeGenWriteBarrier((&___arrowContGO_9), value);
	}

	inline static int32_t get_offset_of_isArrowAnimating_10() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___isArrowAnimating_10)); }
	inline bool get_isArrowAnimating_10() const { return ___isArrowAnimating_10; }
	inline bool* get_address_of_isArrowAnimating_10() { return &___isArrowAnimating_10; }
	inline void set_isArrowAnimating_10(bool value)
	{
		___isArrowAnimating_10 = value;
	}

	inline static int32_t get_offset_of_arrowLevel1List_11() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowLevel1List_11)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_arrowLevel1List_11() const { return ___arrowLevel1List_11; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_arrowLevel1List_11() { return &___arrowLevel1List_11; }
	inline void set_arrowLevel1List_11(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___arrowLevel1List_11 = value;
		Il2CppCodeGenWriteBarrier((&___arrowLevel1List_11), value);
	}

	inline static int32_t get_offset_of_arrowLevelsList_12() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowLevelsList_12)); }
	inline List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * get_arrowLevelsList_12() const { return ___arrowLevelsList_12; }
	inline List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF ** get_address_of_arrowLevelsList_12() { return &___arrowLevelsList_12; }
	inline void set_arrowLevelsList_12(List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * value)
	{
		___arrowLevelsList_12 = value;
		Il2CppCodeGenWriteBarrier((&___arrowLevelsList_12), value);
	}

	inline static int32_t get_offset_of_arrowsAllLevelsList_13() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowsAllLevelsList_13)); }
	inline List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * get_arrowsAllLevelsList_13() const { return ___arrowsAllLevelsList_13; }
	inline List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 ** get_address_of_arrowsAllLevelsList_13() { return &___arrowsAllLevelsList_13; }
	inline void set_arrowsAllLevelsList_13(List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * value)
	{
		___arrowsAllLevelsList_13 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsAllLevelsList_13), value);
	}

	inline static int32_t get_offset_of_arrowCont_14() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowCont_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowCont_14() const { return ___arrowCont_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowCont_14() { return &___arrowCont_14; }
	inline void set_arrowCont_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowCont_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCont_14), value);
	}

	inline static int32_t get_offset_of_maxArrowSpeed_15() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___maxArrowSpeed_15)); }
	inline float get_maxArrowSpeed_15() const { return ___maxArrowSpeed_15; }
	inline float* get_address_of_maxArrowSpeed_15() { return &___maxArrowSpeed_15; }
	inline void set_maxArrowSpeed_15(float value)
	{
		___maxArrowSpeed_15 = value;
	}

	inline static int32_t get_offset_of_courseManager_16() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___courseManager_16)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_16() const { return ___courseManager_16; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_16() { return &___courseManager_16; }
	inline void set_courseManager_16(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_16 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_16), value);
	}
};

struct ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields
{
public:
	// System.Action ArrowManager::ArrowObjectsCreated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ArrowObjectsCreated_4;

public:
	inline static int32_t get_offset_of_ArrowObjectsCreated_4() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields, ___ArrowObjectsCreated_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ArrowObjectsCreated_4() const { return ___ArrowObjectsCreated_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ArrowObjectsCreated_4() { return &___ArrowObjectsCreated_4; }
	inline void set_ArrowObjectsCreated_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ArrowObjectsCreated_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrowObjectsCreated_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#ifndef CAMERAMANAGER_TFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085_H
#define CAMERAMANAGER_TFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraManager
struct  CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera CameraManager::cameraUI
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cameraUI_4;
	// UnityEngine.Camera CameraManager::cameraGreen
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cameraGreen_5;
	// UnityEngine.Camera CameraManager::holeCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___holeCamera_6;

public:
	inline static int32_t get_offset_of_cameraUI_4() { return static_cast<int32_t>(offsetof(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085, ___cameraUI_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cameraUI_4() const { return ___cameraUI_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cameraUI_4() { return &___cameraUI_4; }
	inline void set_cameraUI_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cameraUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraUI_4), value);
	}

	inline static int32_t get_offset_of_cameraGreen_5() { return static_cast<int32_t>(offsetof(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085, ___cameraGreen_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cameraGreen_5() const { return ___cameraGreen_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cameraGreen_5() { return &___cameraGreen_5; }
	inline void set_cameraGreen_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cameraGreen_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraGreen_5), value);
	}

	inline static int32_t get_offset_of_holeCamera_6() { return static_cast<int32_t>(offsetof(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085, ___holeCamera_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_holeCamera_6() const { return ___holeCamera_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_holeCamera_6() { return &___holeCamera_6; }
	inline void set_holeCamera_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___holeCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___holeCamera_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMANAGER_TFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085_H
#ifndef CONFIGURATIONVIEWCONTROLLER_T62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_H
#define CONFIGURATIONVIEWCONTROLLER_T62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigurationViewController
struct  ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ConfigurationViewController::greenJackConfigBtnPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenJackConfigBtnPrefab_5;
	// UnityEngine.RectTransform ConfigurationViewController::greenJackConfigContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___greenJackConfigContainer_6;
	// UnityEngine.GameObject ConfigurationViewController::selectedGreenJack
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___selectedGreenJack_7;
	// UnityEngine.UI.Slider ConfigurationViewController::elevationSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___elevationSlider_8;
	// TMPro.TextMeshProUGUI ConfigurationViewController::jackSelectedDisplayTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___jackSelectedDisplayTxt_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ConfigurationViewController::jackConfigBtnList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___jackConfigBtnList_10;
	// Strackaline.JackData ConfigurationViewController::jackData
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * ___jackData_11;

public:
	inline static int32_t get_offset_of_greenJackConfigBtnPrefab_5() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___greenJackConfigBtnPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenJackConfigBtnPrefab_5() const { return ___greenJackConfigBtnPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenJackConfigBtnPrefab_5() { return &___greenJackConfigBtnPrefab_5; }
	inline void set_greenJackConfigBtnPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenJackConfigBtnPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___greenJackConfigBtnPrefab_5), value);
	}

	inline static int32_t get_offset_of_greenJackConfigContainer_6() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___greenJackConfigContainer_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_greenJackConfigContainer_6() const { return ___greenJackConfigContainer_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_greenJackConfigContainer_6() { return &___greenJackConfigContainer_6; }
	inline void set_greenJackConfigContainer_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___greenJackConfigContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___greenJackConfigContainer_6), value);
	}

	inline static int32_t get_offset_of_selectedGreenJack_7() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___selectedGreenJack_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_selectedGreenJack_7() const { return ___selectedGreenJack_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_selectedGreenJack_7() { return &___selectedGreenJack_7; }
	inline void set_selectedGreenJack_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___selectedGreenJack_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectedGreenJack_7), value);
	}

	inline static int32_t get_offset_of_elevationSlider_8() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___elevationSlider_8)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_elevationSlider_8() const { return ___elevationSlider_8; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_elevationSlider_8() { return &___elevationSlider_8; }
	inline void set_elevationSlider_8(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___elevationSlider_8 = value;
		Il2CppCodeGenWriteBarrier((&___elevationSlider_8), value);
	}

	inline static int32_t get_offset_of_jackSelectedDisplayTxt_9() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___jackSelectedDisplayTxt_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_jackSelectedDisplayTxt_9() const { return ___jackSelectedDisplayTxt_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_jackSelectedDisplayTxt_9() { return &___jackSelectedDisplayTxt_9; }
	inline void set_jackSelectedDisplayTxt_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___jackSelectedDisplayTxt_9 = value;
		Il2CppCodeGenWriteBarrier((&___jackSelectedDisplayTxt_9), value);
	}

	inline static int32_t get_offset_of_jackConfigBtnList_10() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___jackConfigBtnList_10)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_jackConfigBtnList_10() const { return ___jackConfigBtnList_10; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_jackConfigBtnList_10() { return &___jackConfigBtnList_10; }
	inline void set_jackConfigBtnList_10(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___jackConfigBtnList_10 = value;
		Il2CppCodeGenWriteBarrier((&___jackConfigBtnList_10), value);
	}

	inline static int32_t get_offset_of_jackData_11() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB, ___jackData_11)); }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * get_jackData_11() const { return ___jackData_11; }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 ** get_address_of_jackData_11() { return &___jackData_11; }
	inline void set_jackData_11(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * value)
	{
		___jackData_11 = value;
		Il2CppCodeGenWriteBarrier((&___jackData_11), value);
	}
};

struct ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_StaticFields
{
public:
	// System.Action ConfigurationViewController::ConfigurationViewInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ConfigurationViewInit_4;

public:
	inline static int32_t get_offset_of_ConfigurationViewInit_4() { return static_cast<int32_t>(offsetof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_StaticFields, ___ConfigurationViewInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ConfigurationViewInit_4() const { return ___ConfigurationViewInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ConfigurationViewInit_4() { return &___ConfigurationViewInit_4; }
	inline void set_ConfigurationViewInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ConfigurationViewInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigurationViewInit_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVIEWCONTROLLER_T62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_H
#ifndef CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#define CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContourManager
struct  ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ContourManager::contourLineList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___contourLineList_5;
	// UnityEngine.GameObject ContourManager::contourCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___contourCont_6;
	// UnityEngine.Material ContourManager::lineMat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMat_7;
	// CourseManager ContourManager::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_8;

public:
	inline static int32_t get_offset_of_contourLineList_5() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___contourLineList_5)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_contourLineList_5() const { return ___contourLineList_5; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_contourLineList_5() { return &___contourLineList_5; }
	inline void set_contourLineList_5(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___contourLineList_5 = value;
		Il2CppCodeGenWriteBarrier((&___contourLineList_5), value);
	}

	inline static int32_t get_offset_of_contourCont_6() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___contourCont_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_contourCont_6() const { return ___contourCont_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_contourCont_6() { return &___contourCont_6; }
	inline void set_contourCont_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___contourCont_6 = value;
		Il2CppCodeGenWriteBarrier((&___contourCont_6), value);
	}

	inline static int32_t get_offset_of_lineMat_7() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___lineMat_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMat_7() const { return ___lineMat_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMat_7() { return &___lineMat_7; }
	inline void set_lineMat_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMat_7 = value;
		Il2CppCodeGenWriteBarrier((&___lineMat_7), value);
	}

	inline static int32_t get_offset_of_courseManager_8() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___courseManager_8)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_8() const { return ___courseManager_8; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_8() { return &___courseManager_8; }
	inline void set_courseManager_8(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_8), value);
	}
};

struct ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278_StaticFields
{
public:
	// System.Action ContourManager::ContoursLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ContoursLoaded_4;

public:
	inline static int32_t get_offset_of_ContoursLoaded_4() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278_StaticFields, ___ContoursLoaded_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ContoursLoaded_4() const { return ___ContoursLoaded_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ContoursLoaded_4() { return &___ContoursLoaded_4; }
	inline void set_ContoursLoaded_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ContoursLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___ContoursLoaded_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#ifndef COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#define COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseManager
struct  CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.Region CourseManager::region
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * ___region_6;
	// Strackaline.Course CourseManager::course
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * ___course_7;
	// Strackaline.CourseGreens CourseManager::courseGreens
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * ___courseGreens_8;
	// Strackaline.Green CourseManager::green
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * ___green_9;
	// Strackaline.CourseCalender CourseManager::courseCalender
	CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA * ___courseCalender_10;
	// Strackaline.DailyTemplate CourseManager::dailyTemplate
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * ___dailyTemplate_11;
	// Strackaline.Region CourseManager::currRegion
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * ___currRegion_12;
	// Strackaline.Course CourseManager::currCourse
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * ___currCourse_13;
	// Strackaline.Green CourseManager::currGreen
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * ___currGreen_14;
	// UnityEngine.GameObject CourseManager::greenModelLoaded
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenModelLoaded_15;
	// Strackaline.GreenArrow CourseManager::arrowsData
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * ___arrowsData_16;
	// Strackaline.Contour CourseManager::contourData
	Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * ___contourData_17;
	// System.String CourseManager::selectedRegionCode
	String_t* ___selectedRegionCode_18;
	// System.String CourseManager::selectedCourseID
	String_t* ___selectedCourseID_19;
	// System.String CourseManager::selectedGroupID
	String_t* ___selectedGroupID_20;
	// System.String CourseManager::selectedGreenID
	String_t* ___selectedGreenID_21;
	// UnityEngine.Material CourseManager::greenMAT
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___greenMAT_22;

public:
	inline static int32_t get_offset_of_region_6() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___region_6)); }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * get_region_6() const { return ___region_6; }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E ** get_address_of_region_6() { return &___region_6; }
	inline void set_region_6(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * value)
	{
		___region_6 = value;
		Il2CppCodeGenWriteBarrier((&___region_6), value);
	}

	inline static int32_t get_offset_of_course_7() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___course_7)); }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * get_course_7() const { return ___course_7; }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A ** get_address_of_course_7() { return &___course_7; }
	inline void set_course_7(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * value)
	{
		___course_7 = value;
		Il2CppCodeGenWriteBarrier((&___course_7), value);
	}

	inline static int32_t get_offset_of_courseGreens_8() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___courseGreens_8)); }
	inline CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * get_courseGreens_8() const { return ___courseGreens_8; }
	inline CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D ** get_address_of_courseGreens_8() { return &___courseGreens_8; }
	inline void set_courseGreens_8(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * value)
	{
		___courseGreens_8 = value;
		Il2CppCodeGenWriteBarrier((&___courseGreens_8), value);
	}

	inline static int32_t get_offset_of_green_9() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___green_9)); }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * get_green_9() const { return ___green_9; }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 ** get_address_of_green_9() { return &___green_9; }
	inline void set_green_9(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * value)
	{
		___green_9 = value;
		Il2CppCodeGenWriteBarrier((&___green_9), value);
	}

	inline static int32_t get_offset_of_courseCalender_10() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___courseCalender_10)); }
	inline CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA * get_courseCalender_10() const { return ___courseCalender_10; }
	inline CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA ** get_address_of_courseCalender_10() { return &___courseCalender_10; }
	inline void set_courseCalender_10(CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA * value)
	{
		___courseCalender_10 = value;
		Il2CppCodeGenWriteBarrier((&___courseCalender_10), value);
	}

	inline static int32_t get_offset_of_dailyTemplate_11() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___dailyTemplate_11)); }
	inline DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * get_dailyTemplate_11() const { return ___dailyTemplate_11; }
	inline DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED ** get_address_of_dailyTemplate_11() { return &___dailyTemplate_11; }
	inline void set_dailyTemplate_11(DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED * value)
	{
		___dailyTemplate_11 = value;
		Il2CppCodeGenWriteBarrier((&___dailyTemplate_11), value);
	}

	inline static int32_t get_offset_of_currRegion_12() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currRegion_12)); }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * get_currRegion_12() const { return ___currRegion_12; }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E ** get_address_of_currRegion_12() { return &___currRegion_12; }
	inline void set_currRegion_12(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * value)
	{
		___currRegion_12 = value;
		Il2CppCodeGenWriteBarrier((&___currRegion_12), value);
	}

	inline static int32_t get_offset_of_currCourse_13() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currCourse_13)); }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * get_currCourse_13() const { return ___currCourse_13; }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A ** get_address_of_currCourse_13() { return &___currCourse_13; }
	inline void set_currCourse_13(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * value)
	{
		___currCourse_13 = value;
		Il2CppCodeGenWriteBarrier((&___currCourse_13), value);
	}

	inline static int32_t get_offset_of_currGreen_14() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currGreen_14)); }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * get_currGreen_14() const { return ___currGreen_14; }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 ** get_address_of_currGreen_14() { return &___currGreen_14; }
	inline void set_currGreen_14(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * value)
	{
		___currGreen_14 = value;
		Il2CppCodeGenWriteBarrier((&___currGreen_14), value);
	}

	inline static int32_t get_offset_of_greenModelLoaded_15() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___greenModelLoaded_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenModelLoaded_15() const { return ___greenModelLoaded_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenModelLoaded_15() { return &___greenModelLoaded_15; }
	inline void set_greenModelLoaded_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenModelLoaded_15 = value;
		Il2CppCodeGenWriteBarrier((&___greenModelLoaded_15), value);
	}

	inline static int32_t get_offset_of_arrowsData_16() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___arrowsData_16)); }
	inline GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * get_arrowsData_16() const { return ___arrowsData_16; }
	inline GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A ** get_address_of_arrowsData_16() { return &___arrowsData_16; }
	inline void set_arrowsData_16(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * value)
	{
		___arrowsData_16 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsData_16), value);
	}

	inline static int32_t get_offset_of_contourData_17() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___contourData_17)); }
	inline Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * get_contourData_17() const { return ___contourData_17; }
	inline Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 ** get_address_of_contourData_17() { return &___contourData_17; }
	inline void set_contourData_17(Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * value)
	{
		___contourData_17 = value;
		Il2CppCodeGenWriteBarrier((&___contourData_17), value);
	}

	inline static int32_t get_offset_of_selectedRegionCode_18() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedRegionCode_18)); }
	inline String_t* get_selectedRegionCode_18() const { return ___selectedRegionCode_18; }
	inline String_t** get_address_of_selectedRegionCode_18() { return &___selectedRegionCode_18; }
	inline void set_selectedRegionCode_18(String_t* value)
	{
		___selectedRegionCode_18 = value;
		Il2CppCodeGenWriteBarrier((&___selectedRegionCode_18), value);
	}

	inline static int32_t get_offset_of_selectedCourseID_19() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedCourseID_19)); }
	inline String_t* get_selectedCourseID_19() const { return ___selectedCourseID_19; }
	inline String_t** get_address_of_selectedCourseID_19() { return &___selectedCourseID_19; }
	inline void set_selectedCourseID_19(String_t* value)
	{
		___selectedCourseID_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectedCourseID_19), value);
	}

	inline static int32_t get_offset_of_selectedGroupID_20() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedGroupID_20)); }
	inline String_t* get_selectedGroupID_20() const { return ___selectedGroupID_20; }
	inline String_t** get_address_of_selectedGroupID_20() { return &___selectedGroupID_20; }
	inline void set_selectedGroupID_20(String_t* value)
	{
		___selectedGroupID_20 = value;
		Il2CppCodeGenWriteBarrier((&___selectedGroupID_20), value);
	}

	inline static int32_t get_offset_of_selectedGreenID_21() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedGreenID_21)); }
	inline String_t* get_selectedGreenID_21() const { return ___selectedGreenID_21; }
	inline String_t** get_address_of_selectedGreenID_21() { return &___selectedGreenID_21; }
	inline void set_selectedGreenID_21(String_t* value)
	{
		___selectedGreenID_21 = value;
		Il2CppCodeGenWriteBarrier((&___selectedGreenID_21), value);
	}

	inline static int32_t get_offset_of_greenMAT_22() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___greenMAT_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_greenMAT_22() const { return ___greenMAT_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_greenMAT_22() { return &___greenMAT_22; }
	inline void set_greenMAT_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___greenMAT_22 = value;
		Il2CppCodeGenWriteBarrier((&___greenMAT_22), value);
	}
};

struct CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields
{
public:
	// System.Action CourseManager::CalenderForCourseLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CalenderForCourseLoaded_4;
	// System.Action CourseManager::DailyTemplateLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DailyTemplateLoaded_5;

public:
	inline static int32_t get_offset_of_CalenderForCourseLoaded_4() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields, ___CalenderForCourseLoaded_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CalenderForCourseLoaded_4() const { return ___CalenderForCourseLoaded_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CalenderForCourseLoaded_4() { return &___CalenderForCourseLoaded_4; }
	inline void set_CalenderForCourseLoaded_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CalenderForCourseLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___CalenderForCourseLoaded_4), value);
	}

	inline static int32_t get_offset_of_DailyTemplateLoaded_5() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields, ___DailyTemplateLoaded_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DailyTemplateLoaded_5() const { return ___DailyTemplateLoaded_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DailyTemplateLoaded_5() { return &___DailyTemplateLoaded_5; }
	inline void set_DailyTemplateLoaded_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DailyTemplateLoaded_5 = value;
		Il2CppCodeGenWriteBarrier((&___DailyTemplateLoaded_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#ifndef HOMEVIEWCONTROLLER_T85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79_H
#define HOMEVIEWCONTROLLER_T85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeViewController
struct  HomeViewController_t85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UserController HomeViewController::userController
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * ___userController_4;

public:
	inline static int32_t get_offset_of_userController_4() { return static_cast<int32_t>(offsetof(HomeViewController_t85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79, ___userController_4)); }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * get_userController_4() const { return ___userController_4; }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 ** get_address_of_userController_4() { return &___userController_4; }
	inline void set_userController_4(UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * value)
	{
		___userController_4 = value;
		Il2CppCodeGenWriteBarrier((&___userController_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMEVIEWCONTROLLER_T85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79_H
#ifndef JACKCONFIGBTN_T981FA576C2E62D051E054D1C748D67271A6D973D_H
#define JACKCONFIGBTN_T981FA576C2E62D051E054D1C748D67271A6D973D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JackConfigBtn
struct  JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI JackConfigBtn::jackSettingTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___jackSettingTxt_4;
	// TMPro.TextMeshProUGUI JackConfigBtn::jackIDTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___jackIDTxt_5;
	// System.Int32 JackConfigBtn::index
	int32_t ___index_6;
	// System.Single JackConfigBtn::elevation
	float ___elevation_7;
	// System.Single JackConfigBtn::offset
	float ___offset_8;

public:
	inline static int32_t get_offset_of_jackSettingTxt_4() { return static_cast<int32_t>(offsetof(JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D, ___jackSettingTxt_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_jackSettingTxt_4() const { return ___jackSettingTxt_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_jackSettingTxt_4() { return &___jackSettingTxt_4; }
	inline void set_jackSettingTxt_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___jackSettingTxt_4 = value;
		Il2CppCodeGenWriteBarrier((&___jackSettingTxt_4), value);
	}

	inline static int32_t get_offset_of_jackIDTxt_5() { return static_cast<int32_t>(offsetof(JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D, ___jackIDTxt_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_jackIDTxt_5() const { return ___jackIDTxt_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_jackIDTxt_5() { return &___jackIDTxt_5; }
	inline void set_jackIDTxt_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___jackIDTxt_5 = value;
		Il2CppCodeGenWriteBarrier((&___jackIDTxt_5), value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}

	inline static int32_t get_offset_of_elevation_7() { return static_cast<int32_t>(offsetof(JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D, ___elevation_7)); }
	inline float get_elevation_7() const { return ___elevation_7; }
	inline float* get_address_of_elevation_7() { return &___elevation_7; }
	inline void set_elevation_7(float value)
	{
		___elevation_7 = value;
	}

	inline static int32_t get_offset_of_offset_8() { return static_cast<int32_t>(offsetof(JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D, ___offset_8)); }
	inline float get_offset_8() const { return ___offset_8; }
	inline float* get_address_of_offset_8() { return &___offset_8; }
	inline void set_offset_8(float value)
	{
		___offset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACKCONFIGBTN_T981FA576C2E62D051E054D1C748D67271A6D973D_H
#ifndef LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#define LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineToHole
struct  LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material LineToHole::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_4;
	// UnityEngine.GameObject LineToHole::lineToHoleGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lineToHoleGO_5;
	// System.Int32 LineToHole::indexOfAnimationPoint
	int32_t ___indexOfAnimationPoint_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LineToHole::animatedLineToHoleList
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___animatedLineToHoleList_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LineToHole::puttPointsFull
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsFull_8;
	// System.Boolean LineToHole::animateLineToHole
	bool ___animateLineToHole_9;
	// UnityEngine.LineRenderer LineToHole::lineRenderer
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___lineRenderer_10;
	// SimulationManager LineToHole::simulationManager
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * ___simulationManager_11;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_lineToHoleGO_5() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineToHoleGO_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lineToHoleGO_5() const { return ___lineToHoleGO_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lineToHoleGO_5() { return &___lineToHoleGO_5; }
	inline void set_lineToHoleGO_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lineToHoleGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineToHoleGO_5), value);
	}

	inline static int32_t get_offset_of_indexOfAnimationPoint_6() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___indexOfAnimationPoint_6)); }
	inline int32_t get_indexOfAnimationPoint_6() const { return ___indexOfAnimationPoint_6; }
	inline int32_t* get_address_of_indexOfAnimationPoint_6() { return &___indexOfAnimationPoint_6; }
	inline void set_indexOfAnimationPoint_6(int32_t value)
	{
		___indexOfAnimationPoint_6 = value;
	}

	inline static int32_t get_offset_of_animatedLineToHoleList_7() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___animatedLineToHoleList_7)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_animatedLineToHoleList_7() const { return ___animatedLineToHoleList_7; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_animatedLineToHoleList_7() { return &___animatedLineToHoleList_7; }
	inline void set_animatedLineToHoleList_7(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___animatedLineToHoleList_7 = value;
		Il2CppCodeGenWriteBarrier((&___animatedLineToHoleList_7), value);
	}

	inline static int32_t get_offset_of_puttPointsFull_8() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___puttPointsFull_8)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsFull_8() const { return ___puttPointsFull_8; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsFull_8() { return &___puttPointsFull_8; }
	inline void set_puttPointsFull_8(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsFull_8 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsFull_8), value);
	}

	inline static int32_t get_offset_of_animateLineToHole_9() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___animateLineToHole_9)); }
	inline bool get_animateLineToHole_9() const { return ___animateLineToHole_9; }
	inline bool* get_address_of_animateLineToHole_9() { return &___animateLineToHole_9; }
	inline void set_animateLineToHole_9(bool value)
	{
		___animateLineToHole_9 = value;
	}

	inline static int32_t get_offset_of_lineRenderer_10() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineRenderer_10)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_lineRenderer_10() const { return ___lineRenderer_10; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_lineRenderer_10() { return &___lineRenderer_10; }
	inline void set_lineRenderer_10(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___lineRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_10), value);
	}

	inline static int32_t get_offset_of_simulationManager_11() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___simulationManager_11)); }
	inline SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * get_simulationManager_11() const { return ___simulationManager_11; }
	inline SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 ** get_address_of_simulationManager_11() { return &___simulationManager_11; }
	inline void set_simulationManager_11(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * value)
	{
		___simulationManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___simulationManager_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#ifndef LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#define LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineToTarget
struct  LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> LineToTarget::pathToTargetMarkerList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___pathToTargetMarkerList_4;
	// UnityEngine.Vector3 LineToTarget::startLinePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startLinePosition_5;
	// UnityEngine.Vector3 LineToTarget::endLinePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endLinePosition_6;
	// UnityEngine.GameObject LineToTarget::targetLineSegmentPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetLineSegmentPrefab_7;
	// UnityEngine.GameObject LineToTarget::targetPrefabInst
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefabInst_8;
	// UnityEngine.GameObject LineToTarget::targetPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefab_9;
	// PuttSimulation LineToTarget::puttSimulation
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * ___puttSimulation_10;

public:
	inline static int32_t get_offset_of_pathToTargetMarkerList_4() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___pathToTargetMarkerList_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_pathToTargetMarkerList_4() const { return ___pathToTargetMarkerList_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_pathToTargetMarkerList_4() { return &___pathToTargetMarkerList_4; }
	inline void set_pathToTargetMarkerList_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___pathToTargetMarkerList_4 = value;
		Il2CppCodeGenWriteBarrier((&___pathToTargetMarkerList_4), value);
	}

	inline static int32_t get_offset_of_startLinePosition_5() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___startLinePosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startLinePosition_5() const { return ___startLinePosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startLinePosition_5() { return &___startLinePosition_5; }
	inline void set_startLinePosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startLinePosition_5 = value;
	}

	inline static int32_t get_offset_of_endLinePosition_6() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___endLinePosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endLinePosition_6() const { return ___endLinePosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endLinePosition_6() { return &___endLinePosition_6; }
	inline void set_endLinePosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endLinePosition_6 = value;
	}

	inline static int32_t get_offset_of_targetLineSegmentPrefab_7() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetLineSegmentPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetLineSegmentPrefab_7() const { return ___targetLineSegmentPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetLineSegmentPrefab_7() { return &___targetLineSegmentPrefab_7; }
	inline void set_targetLineSegmentPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetLineSegmentPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetLineSegmentPrefab_7), value);
	}

	inline static int32_t get_offset_of_targetPrefabInst_8() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetPrefabInst_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefabInst_8() const { return ___targetPrefabInst_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefabInst_8() { return &___targetPrefabInst_8; }
	inline void set_targetPrefabInst_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefabInst_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefabInst_8), value);
	}

	inline static int32_t get_offset_of_targetPrefab_9() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefab_9() const { return ___targetPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefab_9() { return &___targetPrefab_9; }
	inline void set_targetPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefab_9), value);
	}

	inline static int32_t get_offset_of_puttSimulation_10() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___puttSimulation_10)); }
	inline PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * get_puttSimulation_10() const { return ___puttSimulation_10; }
	inline PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD ** get_address_of_puttSimulation_10() { return &___puttSimulation_10; }
	inline void set_puttSimulation_10(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * value)
	{
		___puttSimulation_10 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulation_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#ifndef LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#define LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginValidateCredentials
struct  LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_InputField LoginValidateCredentials::userNameIF
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ___userNameIF_5;
	// TMPro.TMP_InputField LoginValidateCredentials::passwordIF
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ___passwordIF_6;
	// UnityEngine.UI.Button LoginValidateCredentials::submitBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___submitBtn_7;
	// APILoginController LoginValidateCredentials::apiLoginController
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * ___apiLoginController_8;
	// Strackaline.User LoginValidateCredentials::user
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___user_9;

public:
	inline static int32_t get_offset_of_userNameIF_5() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___userNameIF_5)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get_userNameIF_5() const { return ___userNameIF_5; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of_userNameIF_5() { return &___userNameIF_5; }
	inline void set_userNameIF_5(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		___userNameIF_5 = value;
		Il2CppCodeGenWriteBarrier((&___userNameIF_5), value);
	}

	inline static int32_t get_offset_of_passwordIF_6() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___passwordIF_6)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get_passwordIF_6() const { return ___passwordIF_6; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of_passwordIF_6() { return &___passwordIF_6; }
	inline void set_passwordIF_6(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		___passwordIF_6 = value;
		Il2CppCodeGenWriteBarrier((&___passwordIF_6), value);
	}

	inline static int32_t get_offset_of_submitBtn_7() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___submitBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_submitBtn_7() const { return ___submitBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_submitBtn_7() { return &___submitBtn_7; }
	inline void set_submitBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___submitBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___submitBtn_7), value);
	}

	inline static int32_t get_offset_of_apiLoginController_8() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___apiLoginController_8)); }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * get_apiLoginController_8() const { return ___apiLoginController_8; }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 ** get_address_of_apiLoginController_8() { return &___apiLoginController_8; }
	inline void set_apiLoginController_8(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * value)
	{
		___apiLoginController_8 = value;
		Il2CppCodeGenWriteBarrier((&___apiLoginController_8), value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___user_9)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_user_9() const { return ___user_9; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier((&___user_9), value);
	}
};

struct LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields
{
public:
	// System.Action LoginValidateCredentials::UserDataSet
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___UserDataSet_4;

public:
	inline static int32_t get_offset_of_UserDataSet_4() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields, ___UserDataSet_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_UserDataSet_4() const { return ___UserDataSet_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_UserDataSet_4() { return &___UserDataSet_4; }
	inline void set_UserDataSet_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___UserDataSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___UserDataSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#ifndef LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#define LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginViewController
struct  LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI LoginViewController::loginUserNavTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___loginUserNavTxt_4;
	// UnityEngine.UI.Text LoginViewController::navTxt
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___navTxt_5;
	// UserController LoginViewController::userController
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * ___userController_6;

public:
	inline static int32_t get_offset_of_loginUserNavTxt_4() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___loginUserNavTxt_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_loginUserNavTxt_4() const { return ___loginUserNavTxt_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_loginUserNavTxt_4() { return &___loginUserNavTxt_4; }
	inline void set_loginUserNavTxt_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___loginUserNavTxt_4 = value;
		Il2CppCodeGenWriteBarrier((&___loginUserNavTxt_4), value);
	}

	inline static int32_t get_offset_of_navTxt_5() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___navTxt_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_navTxt_5() const { return ___navTxt_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_navTxt_5() { return &___navTxt_5; }
	inline void set_navTxt_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___navTxt_5 = value;
		Il2CppCodeGenWriteBarrier((&___navTxt_5), value);
	}

	inline static int32_t get_offset_of_userController_6() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___userController_6)); }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * get_userController_6() const { return ___userController_6; }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 ** get_address_of_userController_6() { return &___userController_6; }
	inline void set_userController_6(UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * value)
	{
		___userController_6 = value;
		Il2CppCodeGenWriteBarrier((&___userController_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifndef MANUALLYSETHOLESVIEWCONTROLLER_T29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_H
#define MANUALLYSETHOLESVIEWCONTROLLER_T29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManuallySetHolesViewController
struct  ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ManuallySetHolesViewController::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_5;

public:
	inline static int32_t get_offset_of_holeContainer_5() { return static_cast<int32_t>(offsetof(ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA, ___holeContainer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_5() const { return ___holeContainer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_5() { return &___holeContainer_5; }
	inline void set_holeContainer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_5), value);
	}
};

struct ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_StaticFields
{
public:
	// System.Action ManuallySetHolesViewController::ManuallySetHoleViewInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ManuallySetHoleViewInit_4;

public:
	inline static int32_t get_offset_of_ManuallySetHoleViewInit_4() { return static_cast<int32_t>(offsetof(ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_StaticFields, ___ManuallySetHoleViewInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ManuallySetHoleViewInit_4() const { return ___ManuallySetHoleViewInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ManuallySetHoleViewInit_4() { return &___ManuallySetHoleViewInit_4; }
	inline void set_ManuallySetHoleViewInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ManuallySetHoleViewInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___ManuallySetHoleViewInit_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALLYSETHOLESVIEWCONTROLLER_T29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_H
#ifndef OFFSETLOCALFILE_TEFB035EDF2C230390495C009ABD4F499748FF148_H
#define OFFSETLOCALFILE_TEFB035EDF2C230390495C009ABD4F499748FF148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OffsetLocalFile
struct  OffsetLocalFile_tEFB035EDF2C230390495C009ABD4F499748FF148  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ConfigurationViewController OffsetLocalFile::configViewController
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * ___configViewController_4;

public:
	inline static int32_t get_offset_of_configViewController_4() { return static_cast<int32_t>(offsetof(OffsetLocalFile_tEFB035EDF2C230390495C009ABD4F499748FF148, ___configViewController_4)); }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * get_configViewController_4() const { return ___configViewController_4; }
	inline ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB ** get_address_of_configViewController_4() { return &___configViewController_4; }
	inline void set_configViewController_4(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB * value)
	{
		___configViewController_4 = value;
		Il2CppCodeGenWriteBarrier((&___configViewController_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFSETLOCALFILE_TEFB035EDF2C230390495C009ABD4F499748FF148_H
#ifndef PLACEBALLSMANAGER_T4C2DF545A77F766939498D1F976E271DEED3E134_H
#define PLACEBALLSMANAGER_T4C2DF545A77F766939498D1F976E271DEED3E134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceBallsManager
struct  PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button PlaceBallsManager::addBallBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___addBallBtn_5;
	// UnityEngine.UI.Button PlaceBallsManager::removeBallBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___removeBallBtn_6;
	// UnityEngine.GameObject PlaceBallsManager::ballPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ballPrefab_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlaceBallsManager::ballList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___ballList_8;

public:
	inline static int32_t get_offset_of_addBallBtn_5() { return static_cast<int32_t>(offsetof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134, ___addBallBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_addBallBtn_5() const { return ___addBallBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_addBallBtn_5() { return &___addBallBtn_5; }
	inline void set_addBallBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___addBallBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___addBallBtn_5), value);
	}

	inline static int32_t get_offset_of_removeBallBtn_6() { return static_cast<int32_t>(offsetof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134, ___removeBallBtn_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_removeBallBtn_6() const { return ___removeBallBtn_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_removeBallBtn_6() { return &___removeBallBtn_6; }
	inline void set_removeBallBtn_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___removeBallBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&___removeBallBtn_6), value);
	}

	inline static int32_t get_offset_of_ballPrefab_7() { return static_cast<int32_t>(offsetof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134, ___ballPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ballPrefab_7() const { return ___ballPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ballPrefab_7() { return &___ballPrefab_7; }
	inline void set_ballPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ballPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_7), value);
	}

	inline static int32_t get_offset_of_ballList_8() { return static_cast<int32_t>(offsetof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134, ___ballList_8)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_ballList_8() const { return ___ballList_8; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_ballList_8() { return &___ballList_8; }
	inline void set_ballList_8(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___ballList_8 = value;
		Il2CppCodeGenWriteBarrier((&___ballList_8), value);
	}
};

struct PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134_StaticFields
{
public:
	// System.Action PlaceBallsManager::RemoveBallEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___RemoveBallEvent_4;

public:
	inline static int32_t get_offset_of_RemoveBallEvent_4() { return static_cast<int32_t>(offsetof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134_StaticFields, ___RemoveBallEvent_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_RemoveBallEvent_4() const { return ___RemoveBallEvent_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_RemoveBallEvent_4() { return &___RemoveBallEvent_4; }
	inline void set_RemoveBallEvent_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___RemoveBallEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveBallEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEBALLSMANAGER_T4C2DF545A77F766939498D1F976E271DEED3E134_H
#ifndef PLACEBALLSVIEWCONTROLLER_T2A36E2422A7B3D80CE764E9D0896DF7D1481432E_H
#define PLACEBALLSVIEWCONTROLLER_T2A36E2422A7B3D80CE764E9D0896DF7D1481432E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceBallsViewController
struct  PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CameraManager PlaceBallsViewController::cameraManager
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * ___cameraManager_6;
	// Doozy.Engine.UI.UIButton PlaceBallsViewController::addPlaceBallsSubNavBtn
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___addPlaceBallsSubNavBtn_7;
	// CourseManager PlaceBallsViewController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_8;
	// System.Boolean PlaceBallsViewController::reloadDataInitialize
	bool ___reloadDataInitialize_9;

public:
	inline static int32_t get_offset_of_cameraManager_6() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E, ___cameraManager_6)); }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * get_cameraManager_6() const { return ___cameraManager_6; }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 ** get_address_of_cameraManager_6() { return &___cameraManager_6; }
	inline void set_cameraManager_6(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * value)
	{
		___cameraManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_6), value);
	}

	inline static int32_t get_offset_of_addPlaceBallsSubNavBtn_7() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E, ___addPlaceBallsSubNavBtn_7)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_addPlaceBallsSubNavBtn_7() const { return ___addPlaceBallsSubNavBtn_7; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_addPlaceBallsSubNavBtn_7() { return &___addPlaceBallsSubNavBtn_7; }
	inline void set_addPlaceBallsSubNavBtn_7(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___addPlaceBallsSubNavBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___addPlaceBallsSubNavBtn_7), value);
	}

	inline static int32_t get_offset_of_courseManager_8() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E, ___courseManager_8)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_8() const { return ___courseManager_8; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_8() { return &___courseManager_8; }
	inline void set_courseManager_8(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_8), value);
	}

	inline static int32_t get_offset_of_reloadDataInitialize_9() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E, ___reloadDataInitialize_9)); }
	inline bool get_reloadDataInitialize_9() const { return ___reloadDataInitialize_9; }
	inline bool* get_address_of_reloadDataInitialize_9() { return &___reloadDataInitialize_9; }
	inline void set_reloadDataInitialize_9(bool value)
	{
		___reloadDataInitialize_9 = value;
	}
};

struct PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields
{
public:
	// System.Action PlaceBallsViewController::PlaceBallsViewInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PlaceBallsViewInit_4;
	// System.Action PlaceBallsViewController::PlaceBallsTransmitData
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PlaceBallsTransmitData_5;

public:
	inline static int32_t get_offset_of_PlaceBallsViewInit_4() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields, ___PlaceBallsViewInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PlaceBallsViewInit_4() const { return ___PlaceBallsViewInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PlaceBallsViewInit_4() { return &___PlaceBallsViewInit_4; }
	inline void set_PlaceBallsViewInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PlaceBallsViewInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceBallsViewInit_4), value);
	}

	inline static int32_t get_offset_of_PlaceBallsTransmitData_5() { return static_cast<int32_t>(offsetof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields, ___PlaceBallsTransmitData_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PlaceBallsTransmitData_5() const { return ___PlaceBallsTransmitData_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PlaceBallsTransmitData_5() { return &___PlaceBallsTransmitData_5; }
	inline void set_PlaceBallsTransmitData_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PlaceBallsTransmitData_5 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceBallsTransmitData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEBALLSVIEWCONTROLLER_T2A36E2422A7B3D80CE764E9D0896DF7D1481432E_H
#ifndef PREFERENCESVIEWCONTROLLER_TAD526B71F173BFC7288C62D614709D306E1F514B_H
#define PREFERENCESVIEWCONTROLLER_TAD526B71F173BFC7288C62D614709D306E1F514B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PreferencesViewController
struct  PreferencesViewController_tAD526B71F173BFC7288C62D614709D306E1F514B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERENCESVIEWCONTROLLER_TAD526B71F173BFC7288C62D614709D306E1F514B_H
#ifndef PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#define PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttAssistance
struct  PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PuttAssistance::puttRhythmPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRhythmPrefab_4;
	// PuttInteractive PuttAssistance::puttInteractive
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * ___puttInteractive_5;
	// UnityEngine.GameObject PuttAssistance::puttStanceBoxInst
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttStanceBoxInst_6;
	// UnityEngine.GameObject PuttAssistance::puttRhythmIcon
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRhythmIcon_7;
	// UnityEngine.GameObject PuttAssistance::stanceRightHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceRightHandedLine_8;
	// UnityEngine.GameObject PuttAssistance::stanceLeftHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceLeftHandedLine_9;
	// UnityEngine.Vector3 PuttAssistance::directionOfHoleLocation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___directionOfHoleLocation_10;

public:
	inline static int32_t get_offset_of_puttRhythmPrefab_4() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttRhythmPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRhythmPrefab_4() const { return ___puttRhythmPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRhythmPrefab_4() { return &___puttRhythmPrefab_4; }
	inline void set_puttRhythmPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRhythmPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___puttRhythmPrefab_4), value);
	}

	inline static int32_t get_offset_of_puttInteractive_5() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttInteractive_5)); }
	inline PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * get_puttInteractive_5() const { return ___puttInteractive_5; }
	inline PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 ** get_address_of_puttInteractive_5() { return &___puttInteractive_5; }
	inline void set_puttInteractive_5(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * value)
	{
		___puttInteractive_5 = value;
		Il2CppCodeGenWriteBarrier((&___puttInteractive_5), value);
	}

	inline static int32_t get_offset_of_puttStanceBoxInst_6() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttStanceBoxInst_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttStanceBoxInst_6() const { return ___puttStanceBoxInst_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttStanceBoxInst_6() { return &___puttStanceBoxInst_6; }
	inline void set_puttStanceBoxInst_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttStanceBoxInst_6 = value;
		Il2CppCodeGenWriteBarrier((&___puttStanceBoxInst_6), value);
	}

	inline static int32_t get_offset_of_puttRhythmIcon_7() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttRhythmIcon_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRhythmIcon_7() const { return ___puttRhythmIcon_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRhythmIcon_7() { return &___puttRhythmIcon_7; }
	inline void set_puttRhythmIcon_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRhythmIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___puttRhythmIcon_7), value);
	}

	inline static int32_t get_offset_of_stanceRightHandedLine_8() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___stanceRightHandedLine_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceRightHandedLine_8() const { return ___stanceRightHandedLine_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceRightHandedLine_8() { return &___stanceRightHandedLine_8; }
	inline void set_stanceRightHandedLine_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceRightHandedLine_8 = value;
		Il2CppCodeGenWriteBarrier((&___stanceRightHandedLine_8), value);
	}

	inline static int32_t get_offset_of_stanceLeftHandedLine_9() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___stanceLeftHandedLine_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceLeftHandedLine_9() const { return ___stanceLeftHandedLine_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceLeftHandedLine_9() { return &___stanceLeftHandedLine_9; }
	inline void set_stanceLeftHandedLine_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceLeftHandedLine_9 = value;
		Il2CppCodeGenWriteBarrier((&___stanceLeftHandedLine_9), value);
	}

	inline static int32_t get_offset_of_directionOfHoleLocation_10() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___directionOfHoleLocation_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_directionOfHoleLocation_10() const { return ___directionOfHoleLocation_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_directionOfHoleLocation_10() { return &___directionOfHoleLocation_10; }
	inline void set_directionOfHoleLocation_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___directionOfHoleLocation_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#ifndef PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#define PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttInteractive
struct  PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.PuttVariation PuttInteractive::_puttVariation
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * ____puttVariation_5;
	// UnityEngine.GameObject PuttInteractive::ballDragInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ballDragInput_6;
	// UnityEngine.GameObject PuttInteractive::holeDragInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeDragInput_7;
	// System.Boolean PuttInteractive::isActive
	bool ___isActive_8;
	// System.Collections.Generic.List`1<PuttSimulation> PuttInteractive::puttSimulationsList
	List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * ___puttSimulationsList_9;
	// UnityEngine.GameObject PuttInteractive::puttSimulationPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttSimulationPrefab_10;
	// UnityEngine.Material PuttInteractive::invalidPuttMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___invalidPuttMaterial_11;
	// UnityEngine.Material PuttInteractive::validPuttMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___validPuttMaterial_12;

public:
	inline static int32_t get_offset_of__puttVariation_5() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ____puttVariation_5)); }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * get__puttVariation_5() const { return ____puttVariation_5; }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 ** get_address_of__puttVariation_5() { return &____puttVariation_5; }
	inline void set__puttVariation_5(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * value)
	{
		____puttVariation_5 = value;
		Il2CppCodeGenWriteBarrier((&____puttVariation_5), value);
	}

	inline static int32_t get_offset_of_ballDragInput_6() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___ballDragInput_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ballDragInput_6() const { return ___ballDragInput_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ballDragInput_6() { return &___ballDragInput_6; }
	inline void set_ballDragInput_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ballDragInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___ballDragInput_6), value);
	}

	inline static int32_t get_offset_of_holeDragInput_7() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___holeDragInput_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeDragInput_7() const { return ___holeDragInput_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeDragInput_7() { return &___holeDragInput_7; }
	inline void set_holeDragInput_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeDragInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___holeDragInput_7), value);
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}

	inline static int32_t get_offset_of_puttSimulationsList_9() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___puttSimulationsList_9)); }
	inline List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * get_puttSimulationsList_9() const { return ___puttSimulationsList_9; }
	inline List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD ** get_address_of_puttSimulationsList_9() { return &___puttSimulationsList_9; }
	inline void set_puttSimulationsList_9(List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * value)
	{
		___puttSimulationsList_9 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulationsList_9), value);
	}

	inline static int32_t get_offset_of_puttSimulationPrefab_10() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___puttSimulationPrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttSimulationPrefab_10() const { return ___puttSimulationPrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttSimulationPrefab_10() { return &___puttSimulationPrefab_10; }
	inline void set_puttSimulationPrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttSimulationPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulationPrefab_10), value);
	}

	inline static int32_t get_offset_of_invalidPuttMaterial_11() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___invalidPuttMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_invalidPuttMaterial_11() const { return ___invalidPuttMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_invalidPuttMaterial_11() { return &___invalidPuttMaterial_11; }
	inline void set_invalidPuttMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___invalidPuttMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___invalidPuttMaterial_11), value);
	}

	inline static int32_t get_offset_of_validPuttMaterial_12() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___validPuttMaterial_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_validPuttMaterial_12() const { return ___validPuttMaterial_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_validPuttMaterial_12() { return &___validPuttMaterial_12; }
	inline void set_validPuttMaterial_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___validPuttMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___validPuttMaterial_12), value);
	}
};

struct PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_StaticFields
{
public:
	// System.Action PuttInteractive::PuttUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PuttUpdated_4;

public:
	inline static int32_t get_offset_of_PuttUpdated_4() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_StaticFields, ___PuttUpdated_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PuttUpdated_4() const { return ___PuttUpdated_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PuttUpdated_4() { return &___PuttUpdated_4; }
	inline void set_PuttUpdated_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PuttUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___PuttUpdated_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#ifndef PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#define PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttManager
struct  PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields
{
public:
	// System.Action PuttManager::PuttControlStatusUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PuttControlStatusUpdated_4;
	// System.Boolean PuttManager::isAnimating
	bool ___isAnimating_5;
	// System.Boolean PuttManager::isLooping
	bool ___isLooping_6;
	// System.Int32 PuttManager::speed
	int32_t ___speed_7;

public:
	inline static int32_t get_offset_of_PuttControlStatusUpdated_4() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields, ___PuttControlStatusUpdated_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PuttControlStatusUpdated_4() const { return ___PuttControlStatusUpdated_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PuttControlStatusUpdated_4() { return &___PuttControlStatusUpdated_4; }
	inline void set_PuttControlStatusUpdated_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PuttControlStatusUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___PuttControlStatusUpdated_4), value);
	}

	inline static int32_t get_offset_of_isAnimating_5() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields, ___isAnimating_5)); }
	inline bool get_isAnimating_5() const { return ___isAnimating_5; }
	inline bool* get_address_of_isAnimating_5() { return &___isAnimating_5; }
	inline void set_isAnimating_5(bool value)
	{
		___isAnimating_5 = value;
	}

	inline static int32_t get_offset_of_isLooping_6() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields, ___isLooping_6)); }
	inline bool get_isLooping_6() const { return ___isLooping_6; }
	inline bool* get_address_of_isLooping_6() { return &___isLooping_6; }
	inline void set_isLooping_6(bool value)
	{
		___isLooping_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields, ___speed_7)); }
	inline int32_t get_speed_7() const { return ___speed_7; }
	inline int32_t* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(int32_t value)
	{
		___speed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#ifndef PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#define PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttRythm
struct  PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PuttRythm::puttRythmGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRythmGO_4;
	// DG.Tweening.Tween PuttRythm::tween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___tween_5;
	// UnityEngine.Transform PuttRythm::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_6;
	// UnityEngine.Vector3 PuttRythm::targetPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPos_7;
	// UnityEngine.Vector3 PuttRythm::origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin_8;
	// UnityEngine.GameObject PuttRythm::targeBehindBallCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targeBehindBallCube_9;
	// UnityEngine.GameObject PuttRythm::targetHoleCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetHoleCube_10;
	// System.Single PuttRythm::speed
	float ___speed_11;
	// UnityEngine.Vector3 PuttRythm::puttRythmLineVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___puttRythmLineVector_12;
	// UnityEngine.GameObject PuttRythm::stanceRightHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceRightHandedLine_13;
	// UnityEngine.GameObject PuttRythm::stanceLeftHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceLeftHandedLine_14;
	// PuttRythm_Direction PuttRythm::direction
	int32_t ___direction_15;

public:
	inline static int32_t get_offset_of_puttRythmGO_4() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___puttRythmGO_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRythmGO_4() const { return ___puttRythmGO_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRythmGO_4() { return &___puttRythmGO_4; }
	inline void set_puttRythmGO_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRythmGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___puttRythmGO_4), value);
	}

	inline static int32_t get_offset_of_tween_5() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___tween_5)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_tween_5() const { return ___tween_5; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_tween_5() { return &___tween_5; }
	inline void set_tween_5(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___tween_5 = value;
		Il2CppCodeGenWriteBarrier((&___tween_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___target_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_6() const { return ___target_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_targetPos_7() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targetPos_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPos_7() const { return ___targetPos_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPos_7() { return &___targetPos_7; }
	inline void set_targetPos_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPos_7 = value;
	}

	inline static int32_t get_offset_of_origin_8() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___origin_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_origin_8() const { return ___origin_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_origin_8() { return &___origin_8; }
	inline void set_origin_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___origin_8 = value;
	}

	inline static int32_t get_offset_of_targeBehindBallCube_9() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targeBehindBallCube_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targeBehindBallCube_9() const { return ___targeBehindBallCube_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targeBehindBallCube_9() { return &___targeBehindBallCube_9; }
	inline void set_targeBehindBallCube_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targeBehindBallCube_9 = value;
		Il2CppCodeGenWriteBarrier((&___targeBehindBallCube_9), value);
	}

	inline static int32_t get_offset_of_targetHoleCube_10() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targetHoleCube_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetHoleCube_10() const { return ___targetHoleCube_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetHoleCube_10() { return &___targetHoleCube_10; }
	inline void set_targetHoleCube_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetHoleCube_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetHoleCube_10), value);
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_puttRythmLineVector_12() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___puttRythmLineVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_puttRythmLineVector_12() const { return ___puttRythmLineVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_puttRythmLineVector_12() { return &___puttRythmLineVector_12; }
	inline void set_puttRythmLineVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___puttRythmLineVector_12 = value;
	}

	inline static int32_t get_offset_of_stanceRightHandedLine_13() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___stanceRightHandedLine_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceRightHandedLine_13() const { return ___stanceRightHandedLine_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceRightHandedLine_13() { return &___stanceRightHandedLine_13; }
	inline void set_stanceRightHandedLine_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceRightHandedLine_13 = value;
		Il2CppCodeGenWriteBarrier((&___stanceRightHandedLine_13), value);
	}

	inline static int32_t get_offset_of_stanceLeftHandedLine_14() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___stanceLeftHandedLine_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceLeftHandedLine_14() const { return ___stanceLeftHandedLine_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceLeftHandedLine_14() { return &___stanceLeftHandedLine_14; }
	inline void set_stanceLeftHandedLine_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceLeftHandedLine_14 = value;
		Il2CppCodeGenWriteBarrier((&___stanceLeftHandedLine_14), value);
	}

	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___direction_15)); }
	inline int32_t get_direction_15() const { return ___direction_15; }
	inline int32_t* get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(int32_t value)
	{
		___direction_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#ifndef PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#define PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttSimulation
struct  PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.Putt PuttSimulation::putt
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * ___putt_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PuttSimulation::puttPointsFull
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsFull_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PuttSimulation::puttPointsHalved
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsHalved_6;

public:
	inline static int32_t get_offset_of_putt_4() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___putt_4)); }
	inline Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * get_putt_4() const { return ___putt_4; }
	inline Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 ** get_address_of_putt_4() { return &___putt_4; }
	inline void set_putt_4(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * value)
	{
		___putt_4 = value;
		Il2CppCodeGenWriteBarrier((&___putt_4), value);
	}

	inline static int32_t get_offset_of_puttPointsFull_5() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___puttPointsFull_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsFull_5() const { return ___puttPointsFull_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsFull_5() { return &___puttPointsFull_5; }
	inline void set_puttPointsFull_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsFull_5 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsFull_5), value);
	}

	inline static int32_t get_offset_of_puttPointsHalved_6() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___puttPointsHalved_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsHalved_6() const { return ___puttPointsHalved_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsHalved_6() { return &___puttPointsHalved_6; }
	inline void set_puttPointsHalved_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsHalved_6 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsHalved_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#ifndef REGIONALSELECTFILTER_T0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_H
#define REGIONALSELECTFILTER_T0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegionalSelectFilter
struct  RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// APIGreenController RegionalSelectFilter::apiGreenController
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C * ___apiGreenController_5;
	// CourseManager RegionalSelectFilter::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_6;
	// UnityEngine.RectTransform RegionalSelectFilter::dataListContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___dataListContainer_7;
	// UnityEngine.GameObject RegionalSelectFilter::dataListBtnPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___dataListBtnPrefab_8;

public:
	inline static int32_t get_offset_of_apiGreenController_5() { return static_cast<int32_t>(offsetof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2, ___apiGreenController_5)); }
	inline APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C * get_apiGreenController_5() const { return ___apiGreenController_5; }
	inline APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C ** get_address_of_apiGreenController_5() { return &___apiGreenController_5; }
	inline void set_apiGreenController_5(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C * value)
	{
		___apiGreenController_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiGreenController_5), value);
	}

	inline static int32_t get_offset_of_courseManager_6() { return static_cast<int32_t>(offsetof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2, ___courseManager_6)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_6() const { return ___courseManager_6; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_6() { return &___courseManager_6; }
	inline void set_courseManager_6(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_6), value);
	}

	inline static int32_t get_offset_of_dataListContainer_7() { return static_cast<int32_t>(offsetof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2, ___dataListContainer_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_dataListContainer_7() const { return ___dataListContainer_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_dataListContainer_7() { return &___dataListContainer_7; }
	inline void set_dataListContainer_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___dataListContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___dataListContainer_7), value);
	}

	inline static int32_t get_offset_of_dataListBtnPrefab_8() { return static_cast<int32_t>(offsetof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2, ___dataListBtnPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_dataListBtnPrefab_8() const { return ___dataListBtnPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_dataListBtnPrefab_8() { return &___dataListBtnPrefab_8; }
	inline void set_dataListBtnPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___dataListBtnPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___dataListBtnPrefab_8), value);
	}
};

struct RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_StaticFields
{
public:
	// System.Action RegionalSelectFilter::LoadRegion
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___LoadRegion_4;

public:
	inline static int32_t get_offset_of_LoadRegion_4() { return static_cast<int32_t>(offsetof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_StaticFields, ___LoadRegion_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_LoadRegion_4() const { return ___LoadRegion_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_LoadRegion_4() { return &___LoadRegion_4; }
	inline void set_LoadRegion_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___LoadRegion_4 = value;
		Il2CppCodeGenWriteBarrier((&___LoadRegion_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONALSELECTFILTER_T0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_H
#ifndef RELOADPREVIOUSGREENPUTTDATA_T37EF93F2A73D7903932996FC677441694D6DC06D_H
#define RELOADPREVIOUSGREENPUTTDATA_T37EF93F2A73D7903932996FC677441694D6DC06D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReloadPreviousGreenPuttData
struct  ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CourseManager ReloadPreviousGreenPuttData::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_5;
	// ProjectorStatusData ReloadPreviousGreenPuttData::projectorStatusData
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * ___projectorStatusData_6;
	// Strackaline.GreenActivity ReloadPreviousGreenPuttData::greenActivity
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * ___greenActivity_7;
	// UnityEngine.GameObject ReloadPreviousGreenPuttData::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_8;

public:
	inline static int32_t get_offset_of_courseManager_5() { return static_cast<int32_t>(offsetof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D, ___courseManager_5)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_5() const { return ___courseManager_5; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_5() { return &___courseManager_5; }
	inline void set_courseManager_5(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_5), value);
	}

	inline static int32_t get_offset_of_projectorStatusData_6() { return static_cast<int32_t>(offsetof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D, ___projectorStatusData_6)); }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * get_projectorStatusData_6() const { return ___projectorStatusData_6; }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD ** get_address_of_projectorStatusData_6() { return &___projectorStatusData_6; }
	inline void set_projectorStatusData_6(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * value)
	{
		___projectorStatusData_6 = value;
		Il2CppCodeGenWriteBarrier((&___projectorStatusData_6), value);
	}

	inline static int32_t get_offset_of_greenActivity_7() { return static_cast<int32_t>(offsetof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D, ___greenActivity_7)); }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * get_greenActivity_7() const { return ___greenActivity_7; }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B ** get_address_of_greenActivity_7() { return &___greenActivity_7; }
	inline void set_greenActivity_7(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * value)
	{
		___greenActivity_7 = value;
		Il2CppCodeGenWriteBarrier((&___greenActivity_7), value);
	}

	inline static int32_t get_offset_of_holeContainer_8() { return static_cast<int32_t>(offsetof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D, ___holeContainer_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_8() const { return ___holeContainer_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_8() { return &___holeContainer_8; }
	inline void set_holeContainer_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_8), value);
	}
};

struct ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D_StaticFields
{
public:
	// System.Action ReloadPreviousGreenPuttData::ReloadPreviousDataComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ReloadPreviousDataComplete_4;

public:
	inline static int32_t get_offset_of_ReloadPreviousDataComplete_4() { return static_cast<int32_t>(offsetof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D_StaticFields, ___ReloadPreviousDataComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ReloadPreviousDataComplete_4() const { return ___ReloadPreviousDataComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ReloadPreviousDataComplete_4() { return &___ReloadPreviousDataComplete_4; }
	inline void set_ReloadPreviousDataComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ReloadPreviousDataComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___ReloadPreviousDataComplete_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELOADPREVIOUSGREENPUTTDATA_T37EF93F2A73D7903932996FC677441694D6DC06D_H
#ifndef SEARCHHOLESMANAGER_TE42AD3DA3189D191AFDB8A85313D3CAF3E47DB14_H
#define SEARCHHOLESMANAGER_TE42AD3DA3189D191AFDB8A85313D3CAF3E47DB14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchHolesManager
struct  SearchHolesManager_tE42AD3DA3189D191AFDB8A85313D3CAF3E47DB14  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHHOLESMANAGER_TE42AD3DA3189D191AFDB8A85313D3CAF3E47DB14_H
#ifndef SEARCHHOLESVIEWCONTROLLER_TE205ACE2901EDCA4FAC9EBC0281CEB8DD43F433D_H
#define SEARCHHOLESVIEWCONTROLLER_TE205ACE2901EDCA4FAC9EBC0281CEB8DD43F433D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchHolesViewController
struct  SearchHolesViewController_tE205ACE2901EDCA4FAC9EBC0281CEB8DD43F433D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHHOLESVIEWCONTROLLER_TE205ACE2901EDCA4FAC9EBC0281CEB8DD43F433D_H
#ifndef SEARCHSCANSVIEWCONTROLLER_T89708B48EB37AFAC10D5AF8C38072BED22899D40_H
#define SEARCHSCANSVIEWCONTROLLER_T89708B48EB37AFAC10D5AF8C38072BED22899D40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchScansViewController
struct  SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button SearchScansViewController::regionBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___regionBtn_4;
	// UnityEngine.UI.Button SearchScansViewController::searchBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___searchBtn_5;
	// UnityEngine.GameObject SearchScansViewController::searchPanelCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___searchPanelCont_6;
	// UnityEngine.GameObject SearchScansViewController::regionPanelCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___regionPanelCont_7;

public:
	inline static int32_t get_offset_of_regionBtn_4() { return static_cast<int32_t>(offsetof(SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40, ___regionBtn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_regionBtn_4() const { return ___regionBtn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_regionBtn_4() { return &___regionBtn_4; }
	inline void set_regionBtn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___regionBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&___regionBtn_4), value);
	}

	inline static int32_t get_offset_of_searchBtn_5() { return static_cast<int32_t>(offsetof(SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40, ___searchBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_searchBtn_5() const { return ___searchBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_searchBtn_5() { return &___searchBtn_5; }
	inline void set_searchBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___searchBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchBtn_5), value);
	}

	inline static int32_t get_offset_of_searchPanelCont_6() { return static_cast<int32_t>(offsetof(SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40, ___searchPanelCont_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_searchPanelCont_6() const { return ___searchPanelCont_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_searchPanelCont_6() { return &___searchPanelCont_6; }
	inline void set_searchPanelCont_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___searchPanelCont_6 = value;
		Il2CppCodeGenWriteBarrier((&___searchPanelCont_6), value);
	}

	inline static int32_t get_offset_of_regionPanelCont_7() { return static_cast<int32_t>(offsetof(SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40, ___regionPanelCont_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_regionPanelCont_7() const { return ___regionPanelCont_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_regionPanelCont_7() { return &___regionPanelCont_7; }
	inline void set_regionPanelCont_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___regionPanelCont_7 = value;
		Il2CppCodeGenWriteBarrier((&___regionPanelCont_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHSCANSVIEWCONTROLLER_T89708B48EB37AFAC10D5AF8C38072BED22899D40_H
#ifndef SELECTGREENSVBTN_TDA0A043620675AA0547AC8EE56397920682812E5_H
#define SELECTGREENSVBTN_TDA0A043620675AA0547AC8EE56397920682812E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectGreenSVBtn
struct  SelectGreenSVBtn_tDA0A043620675AA0547AC8EE56397920682812E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SelectGreenSVBtn::groupIndex
	int32_t ___groupIndex_4;

public:
	inline static int32_t get_offset_of_groupIndex_4() { return static_cast<int32_t>(offsetof(SelectGreenSVBtn_tDA0A043620675AA0547AC8EE56397920682812E5, ___groupIndex_4)); }
	inline int32_t get_groupIndex_4() const { return ___groupIndex_4; }
	inline int32_t* get_address_of_groupIndex_4() { return &___groupIndex_4; }
	inline void set_groupIndex_4(int32_t value)
	{
		___groupIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTGREENSVBTN_TDA0A043620675AA0547AC8EE56397920682812E5_H
#ifndef SELECTGREENVIEWCONTROLLER_T37F1BBDC6955472D676E3155DE51FF1D77776126_H
#define SELECTGREENVIEWCONTROLLER_T37F1BBDC6955472D676E3155DE51FF1D77776126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectGreenViewController
struct  SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126_StaticFields
{
public:
	// System.Action SelectGreenViewController::SelectGreenVCInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___SelectGreenVCInit_4;

public:
	inline static int32_t get_offset_of_SelectGreenVCInit_4() { return static_cast<int32_t>(offsetof(SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126_StaticFields, ___SelectGreenVCInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_SelectGreenVCInit_4() const { return ___SelectGreenVCInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_SelectGreenVCInit_4() { return &___SelectGreenVCInit_4; }
	inline void set_SelectGreenVCInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___SelectGreenVCInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___SelectGreenVCInit_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTGREENVIEWCONTROLLER_T37F1BBDC6955472D676E3155DE51FF1D77776126_H
#ifndef SETHOLEVIEWCONTROLLER_T70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_H
#define SETHOLEVIEWCONTROLLER_T70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetHoleViewController
struct  SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CameraManager SetHoleViewController::cameraManager
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * ___cameraManager_5;
	// HoleController SetHoleViewController::holeController
	HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0 * ___holeController_6;

public:
	inline static int32_t get_offset_of_cameraManager_5() { return static_cast<int32_t>(offsetof(SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0, ___cameraManager_5)); }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * get_cameraManager_5() const { return ___cameraManager_5; }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 ** get_address_of_cameraManager_5() { return &___cameraManager_5; }
	inline void set_cameraManager_5(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * value)
	{
		___cameraManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_5), value);
	}

	inline static int32_t get_offset_of_holeController_6() { return static_cast<int32_t>(offsetof(SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0, ___holeController_6)); }
	inline HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0 * get_holeController_6() const { return ___holeController_6; }
	inline HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0 ** get_address_of_holeController_6() { return &___holeController_6; }
	inline void set_holeController_6(HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0 * value)
	{
		___holeController_6 = value;
		Il2CppCodeGenWriteBarrier((&___holeController_6), value);
	}
};

struct SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_StaticFields
{
public:
	// System.Action SetHoleViewController::SetHoleVCInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___SetHoleVCInit_4;

public:
	inline static int32_t get_offset_of_SetHoleVCInit_4() { return static_cast<int32_t>(offsetof(SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_StaticFields, ___SetHoleVCInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_SetHoleVCInit_4() const { return ___SetHoleVCInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_SetHoleVCInit_4() { return &___SetHoleVCInit_4; }
	inline void set_SetHoleVCInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___SetHoleVCInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___SetHoleVCInit_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETHOLEVIEWCONTROLLER_T70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_H
#ifndef SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#define SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationManager
struct  SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SimulationManager::loopPutt
	bool ___loopPutt_4;
	// SimulationManager_PuttLineSpeed SimulationManager::puttLineSpeed
	int32_t ___puttLineSpeed_5;

public:
	inline static int32_t get_offset_of_loopPutt_4() { return static_cast<int32_t>(offsetof(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9, ___loopPutt_4)); }
	inline bool get_loopPutt_4() const { return ___loopPutt_4; }
	inline bool* get_address_of_loopPutt_4() { return &___loopPutt_4; }
	inline void set_loopPutt_4(bool value)
	{
		___loopPutt_4 = value;
	}

	inline static int32_t get_offset_of_puttLineSpeed_5() { return static_cast<int32_t>(offsetof(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9, ___puttLineSpeed_5)); }
	inline int32_t get_puttLineSpeed_5() const { return ___puttLineSpeed_5; }
	inline int32_t* get_address_of_puttLineSpeed_5() { return &___puttLineSpeed_5; }
	inline void set_puttLineSpeed_5(int32_t value)
	{
		___puttLineSpeed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#ifndef SIMULATIONVIEWCONTROLLER_T905F4C8B64F0FE3582D14878F52E89C31C7FEF53_H
#define SIMULATIONVIEWCONTROLLER_T905F4C8B64F0FE3582D14878F52E89C31C7FEF53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationViewController
struct  SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button SimulationViewController::playPauseBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___playPauseBtn_7;
	// UnityEngine.Sprite SimulationViewController::playSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___playSprite_8;
	// UnityEngine.Sprite SimulationViewController::pauseSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___pauseSprite_9;
	// UnityEngine.UI.Toggle SimulationViewController::loopToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___loopToggle_10;
	// UnityEngine.UI.Button SimulationViewController::slowBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___slowBtn_11;
	// UnityEngine.UI.Button SimulationViewController::normalBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___normalBtn_12;
	// UnityEngine.UI.Button SimulationViewController::fastBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___fastBtn_13;
	// Doozy.Engine.UI.UIButton SimulationViewController::puttNowSubNavBtn
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___puttNowSubNavBtn_14;

public:
	inline static int32_t get_offset_of_playPauseBtn_7() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___playPauseBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_playPauseBtn_7() const { return ___playPauseBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_playPauseBtn_7() { return &___playPauseBtn_7; }
	inline void set_playPauseBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___playPauseBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___playPauseBtn_7), value);
	}

	inline static int32_t get_offset_of_playSprite_8() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___playSprite_8)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_playSprite_8() const { return ___playSprite_8; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_playSprite_8() { return &___playSprite_8; }
	inline void set_playSprite_8(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___playSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___playSprite_8), value);
	}

	inline static int32_t get_offset_of_pauseSprite_9() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___pauseSprite_9)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_pauseSprite_9() const { return ___pauseSprite_9; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_pauseSprite_9() { return &___pauseSprite_9; }
	inline void set_pauseSprite_9(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___pauseSprite_9 = value;
		Il2CppCodeGenWriteBarrier((&___pauseSprite_9), value);
	}

	inline static int32_t get_offset_of_loopToggle_10() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___loopToggle_10)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_loopToggle_10() const { return ___loopToggle_10; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_loopToggle_10() { return &___loopToggle_10; }
	inline void set_loopToggle_10(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___loopToggle_10 = value;
		Il2CppCodeGenWriteBarrier((&___loopToggle_10), value);
	}

	inline static int32_t get_offset_of_slowBtn_11() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___slowBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_slowBtn_11() const { return ___slowBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_slowBtn_11() { return &___slowBtn_11; }
	inline void set_slowBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___slowBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&___slowBtn_11), value);
	}

	inline static int32_t get_offset_of_normalBtn_12() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___normalBtn_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_normalBtn_12() const { return ___normalBtn_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_normalBtn_12() { return &___normalBtn_12; }
	inline void set_normalBtn_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___normalBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&___normalBtn_12), value);
	}

	inline static int32_t get_offset_of_fastBtn_13() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___fastBtn_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_fastBtn_13() const { return ___fastBtn_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_fastBtn_13() { return &___fastBtn_13; }
	inline void set_fastBtn_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___fastBtn_13 = value;
		Il2CppCodeGenWriteBarrier((&___fastBtn_13), value);
	}

	inline static int32_t get_offset_of_puttNowSubNavBtn_14() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53, ___puttNowSubNavBtn_14)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_puttNowSubNavBtn_14() const { return ___puttNowSubNavBtn_14; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_puttNowSubNavBtn_14() { return &___puttNowSubNavBtn_14; }
	inline void set_puttNowSubNavBtn_14(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___puttNowSubNavBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&___puttNowSubNavBtn_14), value);
	}
};

struct SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields
{
public:
	// System.Action SimulationViewController::PlayClicked
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PlayClicked_4;
	// System.Action SimulationViewController::LoopPuttEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___LoopPuttEvent_5;
	// System.Action`1<System.Int32> SimulationViewController::SimulationPuttSpeedSet
	Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * ___SimulationPuttSpeedSet_6;

public:
	inline static int32_t get_offset_of_PlayClicked_4() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields, ___PlayClicked_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PlayClicked_4() const { return ___PlayClicked_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PlayClicked_4() { return &___PlayClicked_4; }
	inline void set_PlayClicked_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PlayClicked_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlayClicked_4), value);
	}

	inline static int32_t get_offset_of_LoopPuttEvent_5() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields, ___LoopPuttEvent_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_LoopPuttEvent_5() const { return ___LoopPuttEvent_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_LoopPuttEvent_5() { return &___LoopPuttEvent_5; }
	inline void set_LoopPuttEvent_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___LoopPuttEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoopPuttEvent_5), value);
	}

	inline static int32_t get_offset_of_SimulationPuttSpeedSet_6() { return static_cast<int32_t>(offsetof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields, ___SimulationPuttSpeedSet_6)); }
	inline Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * get_SimulationPuttSpeedSet_6() const { return ___SimulationPuttSpeedSet_6; }
	inline Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 ** get_address_of_SimulationPuttSpeedSet_6() { return &___SimulationPuttSpeedSet_6; }
	inline void set_SimulationPuttSpeedSet_6(Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * value)
	{
		___SimulationPuttSpeedSet_6 = value;
		Il2CppCodeGenWriteBarrier((&___SimulationPuttSpeedSet_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATIONVIEWCONTROLLER_T905F4C8B64F0FE3582D14878F52E89C31C7FEF53_H
#ifndef SUBNAVMANAGER_T678EBC95DEA6CC6E911930E8E9A91E0709346476_H
#define SUBNAVMANAGER_T678EBC95DEA6CC6E911930E8E9A91E0709346476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubNavManager
struct  SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject SubNavManager::subNav
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___subNav_4;
	// UnityEngine.Vector3 SubNavManager::subNavStart
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___subNavStart_5;

public:
	inline static int32_t get_offset_of_subNav_4() { return static_cast<int32_t>(offsetof(SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476, ___subNav_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_subNav_4() const { return ___subNav_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_subNav_4() { return &___subNav_4; }
	inline void set_subNav_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___subNav_4 = value;
		Il2CppCodeGenWriteBarrier((&___subNav_4), value);
	}

	inline static int32_t get_offset_of_subNavStart_5() { return static_cast<int32_t>(offsetof(SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476, ___subNavStart_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_subNavStart_5() const { return ___subNavStart_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_subNavStart_5() { return &___subNavStart_5; }
	inline void set_subNavStart_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___subNavStart_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBNAVMANAGER_T678EBC95DEA6CC6E911930E8E9A91E0709346476_H
#ifndef POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#define POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_4;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_5;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_6;

public:
	inline static int32_t get_offset_of_supportHDRTextures_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportHDRTextures_4)); }
	inline bool get_supportHDRTextures_4() const { return ___supportHDRTextures_4; }
	inline bool* get_address_of_supportHDRTextures_4() { return &___supportHDRTextures_4; }
	inline void set_supportHDRTextures_4(bool value)
	{
		___supportHDRTextures_4 = value;
	}

	inline static int32_t get_offset_of_supportDX11_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportDX11_5)); }
	inline bool get_supportDX11_5() const { return ___supportDX11_5; }
	inline bool* get_address_of_supportDX11_5() { return &___supportDX11_5; }
	inline void set_supportDX11_5(bool value)
	{
		___supportDX11_5 = value;
	}

	inline static int32_t get_offset_of_isSupported_6() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___isSupported_6)); }
	inline bool get_isSupported_6() const { return ___isSupported_6; }
	inline bool* get_address_of_isSupported_6() { return &___isSupported_6; }
	inline void set_isSupported_6(bool value)
	{
		___isSupported_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#ifndef USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#define USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserController
struct  UserController_tFD917E07B535D607AFD7056993FB736FD366E600  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.User UserController::user
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___user_4;
	// System.String UserController::fileNameForUser
	String_t* ___fileNameForUser_5;
	// System.Boolean UserController::userLoggedIn
	bool ___userLoggedIn_6;

public:
	inline static int32_t get_offset_of_user_4() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___user_4)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_user_4() const { return ___user_4; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_user_4() { return &___user_4; }
	inline void set_user_4(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___user_4 = value;
		Il2CppCodeGenWriteBarrier((&___user_4), value);
	}

	inline static int32_t get_offset_of_fileNameForUser_5() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___fileNameForUser_5)); }
	inline String_t* get_fileNameForUser_5() const { return ___fileNameForUser_5; }
	inline String_t** get_address_of_fileNameForUser_5() { return &___fileNameForUser_5; }
	inline void set_fileNameForUser_5(String_t* value)
	{
		___fileNameForUser_5 = value;
		Il2CppCodeGenWriteBarrier((&___fileNameForUser_5), value);
	}

	inline static int32_t get_offset_of_userLoggedIn_6() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___userLoggedIn_6)); }
	inline bool get_userLoggedIn_6() const { return ___userLoggedIn_6; }
	inline bool* get_address_of_userLoggedIn_6() { return &___userLoggedIn_6; }
	inline void set_userLoggedIn_6(bool value)
	{
		___userLoggedIn_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#ifndef BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H
#define BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized
struct  BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730  : public PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E
{
public:
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::threshold
	float ___threshold_7;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::intensity
	float ___intensity_8;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::blurSize
	float ___blurSize_9;
	// UnityStandardAssets.ImageEffects.BloomOptimized_Resolution UnityStandardAssets.ImageEffects.BloomOptimized::resolution
	int32_t ___resolution_10;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized::blurIterations
	int32_t ___blurIterations_11;
	// UnityStandardAssets.ImageEffects.BloomOptimized_BlurType UnityStandardAssets.ImageEffects.BloomOptimized::blurType
	int32_t ___blurType_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___fastBloomShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fastBloomMaterial_14;

public:
	inline static int32_t get_offset_of_threshold_7() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___threshold_7)); }
	inline float get_threshold_7() const { return ___threshold_7; }
	inline float* get_address_of_threshold_7() { return &___threshold_7; }
	inline void set_threshold_7(float value)
	{
		___threshold_7 = value;
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___intensity_8)); }
	inline float get_intensity_8() const { return ___intensity_8; }
	inline float* get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(float value)
	{
		___intensity_8 = value;
	}

	inline static int32_t get_offset_of_blurSize_9() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurSize_9)); }
	inline float get_blurSize_9() const { return ___blurSize_9; }
	inline float* get_address_of_blurSize_9() { return &___blurSize_9; }
	inline void set_blurSize_9(float value)
	{
		___blurSize_9 = value;
	}

	inline static int32_t get_offset_of_resolution_10() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___resolution_10)); }
	inline int32_t get_resolution_10() const { return ___resolution_10; }
	inline int32_t* get_address_of_resolution_10() { return &___resolution_10; }
	inline void set_resolution_10(int32_t value)
	{
		___resolution_10 = value;
	}

	inline static int32_t get_offset_of_blurIterations_11() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurIterations_11)); }
	inline int32_t get_blurIterations_11() const { return ___blurIterations_11; }
	inline int32_t* get_address_of_blurIterations_11() { return &___blurIterations_11; }
	inline void set_blurIterations_11(int32_t value)
	{
		___blurIterations_11 = value;
	}

	inline static int32_t get_offset_of_blurType_12() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___blurType_12)); }
	inline int32_t get_blurType_12() const { return ___blurType_12; }
	inline int32_t* get_address_of_blurType_12() { return &___blurType_12; }
	inline void set_blurType_12(int32_t value)
	{
		___blurType_12 = value;
	}

	inline static int32_t get_offset_of_fastBloomShader_13() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___fastBloomShader_13)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_fastBloomShader_13() const { return ___fastBloomShader_13; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_fastBloomShader_13() { return &___fastBloomShader_13; }
	inline void set_fastBloomShader_13(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___fastBloomShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomShader_13), value);
	}

	inline static int32_t get_offset_of_fastBloomMaterial_14() { return static_cast<int32_t>(offsetof(BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730, ___fastBloomMaterial_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fastBloomMaterial_14() const { return ___fastBloomMaterial_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fastBloomMaterial_14() { return &___fastBloomMaterial_14; }
	inline void set_fastBloomMaterial_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fastBloomMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMOPTIMIZED_TF196D91797F45F2E2A6B293421FDC3C8721BC730_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (HomeViewController_t85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[1] = 
{
	HomeViewController_t85F4999511EAF7D8BC8B40DAF5839F3ECFA92D79::get_offset_of_userController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[5] = 
{
	JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D::get_offset_of_jackSettingTxt_4(),
	JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D::get_offset_of_jackIDTxt_5(),
	JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D::get_offset_of_index_6(),
	JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D::get_offset_of_elevation_7(),
	JackConfigBtn_t981FA576C2E62D051E054D1C748D67271A6D973D::get_offset_of_offset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[8] = 
{
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineMaterial_4(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineToHoleGO_5(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_indexOfAnimationPoint_6(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_animatedLineToHoleList_7(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_puttPointsFull_8(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_animateLineToHole_9(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineRenderer_10(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_simulationManager_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[7] = 
{
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_pathToTargetMarkerList_4(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_startLinePosition_5(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_endLinePosition_6(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetLineSegmentPrefab_7(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetPrefabInst_8(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetPrefab_9(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_puttSimulation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692), -1, sizeof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3704[6] = 
{
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields::get_offset_of_UserDataSet_4(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_userNameIF_5(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_passwordIF_6(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_submitBtn_7(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_apiLoginController_8(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_user_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[3] = 
{
	LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833::get_offset_of_loginUserNavTxt_4(),
	LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833::get_offset_of_navTxt_5(),
	LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833::get_offset_of_userController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983), -1, sizeof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3706[13] = 
{
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields::get_offset_of_ArrowObjectsCreated_4(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowPrefab_5(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowList_6(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowColor_7(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowPrefColor_8(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowContGO_9(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_isArrowAnimating_10(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowLevel1List_11(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowLevelsList_12(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowsAllLevelsList_13(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowCont_14(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_maxArrowSpeed_15(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_courseManager_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[3] = 
{
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085::get_offset_of_cameraUI_4(),
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085::get_offset_of_cameraGreen_5(),
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085::get_offset_of_holeCamera_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278), -1, sizeof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3708[5] = 
{
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278_StaticFields::get_offset_of_ContoursLoaded_4(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_contourLineList_5(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_contourCont_6(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_lineMat_7(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_courseManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8), -1, sizeof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3709[19] = 
{
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields::get_offset_of_CalenderForCourseLoaded_4(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_StaticFields::get_offset_of_DailyTemplateLoaded_5(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_region_6(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_course_7(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_courseGreens_8(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_green_9(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_courseCalender_10(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_dailyTemplate_11(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currRegion_12(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currCourse_13(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currGreen_14(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_greenModelLoaded_15(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_arrowsData_16(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_contourData_17(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedRegionCode_18(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedCourseID_19(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedGroupID_20(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedGreenID_21(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_greenMAT_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134), -1, sizeof(PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3710[5] = 
{
	PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134_StaticFields::get_offset_of_RemoveBallEvent_4(),
	PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134::get_offset_of_addBallBtn_5(),
	PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134::get_offset_of_removeBallBtn_6(),
	PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134::get_offset_of_ballPrefab_7(),
	PlaceBallsManager_t4C2DF545A77F766939498D1F976E271DEED3E134::get_offset_of_ballList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41), -1, sizeof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3711[4] = 
{
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields::get_offset_of_PuttControlStatusUpdated_4(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields::get_offset_of_isAnimating_5(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields::get_offset_of_isLooping_6(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_StaticFields::get_offset_of_speed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[2] = 
{
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9::get_offset_of_loopPutt_4(),
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9::get_offset_of_puttLineSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3713[4] = 
{
	PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[3] = 
{
	Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9::get_offset_of_jackIndex_0(),
	Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9::get_offset_of_elevation_1(),
	Jack_t5EA54DD23FCFCB263EFFD7260A6753EC86C0ACD9::get_offset_of_offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[2] = 
{
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD::get_offset_of_jackData_0(),
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD::get_offset_of_greenActivity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[3] = 
{
	RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D::get_offset_of_puttTrainerId_0(),
	RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D::get_offset_of_configurationData_1(),
	RoboData_t7E47D9391EA9D88DDDAE974C82B8E7DAC7F9B62D::get_offset_of_configTypeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (RoboJackData_t96D97C9BE56685CFE6AED4237E2B0E55782A3FF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[1] = 
{
	RoboJackData_t96D97C9BE56685CFE6AED4237E2B0E55782A3FF5::get_offset_of_jackList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (RoboPost_t2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3718[1] = 
{
	RoboPost_t2BA45624BD86BF25A42F2BA9FB47F21A6917DEC0::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[2] = 
{
	UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF::get_offset_of_userName_0(),
	UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF::get_offset_of_password_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (OffsetLocalFile_tEFB035EDF2C230390495C009ABD4F499748FF148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[1] = 
{
	OffsetLocalFile_tEFB035EDF2C230390495C009ABD4F499748FF148::get_offset_of_configViewController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[7] = 
{
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttRhythmPrefab_4(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttInteractive_5(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttStanceBoxInst_6(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttRhythmIcon_7(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_stanceRightHandedLine_8(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_stanceLeftHandedLine_9(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_directionOfHoleLocation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60), -1, sizeof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3722[9] = 
{
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_StaticFields::get_offset_of_PuttUpdated_4(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of__puttVariation_5(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_ballDragInput_6(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_holeDragInput_7(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_isActive_8(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_puttSimulationsList_9(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_puttSimulationPrefab_10(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_invalidPuttMaterial_11(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_validPuttMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[12] = 
{
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_puttRythmGO_4(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_tween_5(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_target_6(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targetPos_7(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_origin_8(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targeBehindBallCube_9(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targetHoleCube_10(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_speed_11(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_puttRythmLineVector_12(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_stanceRightHandedLine_13(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_stanceLeftHandedLine_14(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_direction_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3724[3] = 
{
	Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3725[3] = 
{
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_putt_4(),
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_puttPointsFull_5(),
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_puttPointsHalved_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2), -1, sizeof(RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3726[5] = 
{
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2_StaticFields::get_offset_of_LoadRegion_4(),
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2::get_offset_of_apiGreenController_5(),
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2::get_offset_of_courseManager_6(),
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2::get_offset_of_dataListContainer_7(),
	RegionalSelectFilter_t0103D3FA5BD4A75AB62F3289BE75A68CB8E99BA2::get_offset_of_dataListBtnPrefab_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[2] = 
{
	U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass9_0_tBB84438049B445B2A7D56DCD0AD84F8B79E92A4E::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[2] = 
{
	U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass11_0_t8C706C33ED5FA412CB1F3C2760DE58203706F7F2::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[2] = 
{
	U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B::get_offset_of_listBtn_0(),
	U3CU3Ec__DisplayClass13_0_tB0566369EDF4B3232C57A065AA40552CFA2F594B::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[2] = 
{
	U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass14_0_t302499D805D0C2134517491D33971886561A0383::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D), -1, sizeof(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3731[5] = 
{
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D_StaticFields::get_offset_of_ReloadPreviousDataComplete_4(),
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D::get_offset_of_courseManager_5(),
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D::get_offset_of_projectorStatusData_6(),
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D::get_offset_of_greenActivity_7(),
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D::get_offset_of_holeContainer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (SearchHolesManager_tE42AD3DA3189D191AFDB8A85313D3CAF3E47DB14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[2] = 
{
	SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476::get_offset_of_subNav_4(),
	SubNavManager_t678EBC95DEA6CC6E911930E8E9A91E0709346476::get_offset_of_subNavStart_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[4] = 
{
	AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F::get_offset_of_okayBtn_4(),
	AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F::get_offset_of_cancelBtn_5(),
	AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F::get_offset_of_headerTxt_6(),
	AlertPopup_t676CD1E44D14D59932D918F6DEDD73E67231168F::get_offset_of_bodyTxt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (SelectGreenSVBtn_tDA0A043620675AA0547AC8EE56397920682812E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3735[1] = 
{
	SelectGreenSVBtn_tDA0A043620675AA0547AC8EE56397920682812E5::get_offset_of_groupIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (UserController_tFD917E07B535D607AFD7056993FB736FD366E600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[3] = 
{
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600::get_offset_of_user_4(),
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600::get_offset_of_fileNameForUser_5(),
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600::get_offset_of_userLoggedIn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB), -1, sizeof(ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3737[8] = 
{
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB_StaticFields::get_offset_of_ConfigurationViewInit_4(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_greenJackConfigBtnPrefab_5(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_greenJackConfigContainer_6(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_selectedGreenJack_7(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_elevationSlider_8(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_jackSelectedDisplayTxt_9(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_jackConfigBtnList_10(),
	ConfigurationViewController_t62E715E6E2D48ABF16FCB5FDC3D35FD0A47F2FBB::get_offset_of_jackData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3738[2] = 
{
	U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4::get_offset_of_go_0(),
	U3CU3Ec__DisplayClass13_0_tB0CFDF6072ABA24FA5B68A1A2BB8EA71C43E01A4::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[2] = 
{
	U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B::get_offset_of_go_0(),
	U3CU3Ec__DisplayClass14_0_tA0960DE9FA1AC8F0FE6392D56FB1E17A5124F19B::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA), -1, sizeof(ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3740[2] = 
{
	ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA_StaticFields::get_offset_of_ManuallySetHoleViewInit_4(),
	ManuallySetHolesViewController_t29E191CCFAC5CFEDC7FF2AB82D3EA3DFEFA8FAEA::get_offset_of_holeContainer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E), -1, sizeof(PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3741[6] = 
{
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields::get_offset_of_PlaceBallsViewInit_4(),
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E_StaticFields::get_offset_of_PlaceBallsTransmitData_5(),
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E::get_offset_of_cameraManager_6(),
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E::get_offset_of_addPlaceBallsSubNavBtn_7(),
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E::get_offset_of_courseManager_8(),
	PlaceBallsViewController_t2A36E2422A7B3D80CE764E9D0896DF7D1481432E::get_offset_of_reloadDataInitialize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (PreferencesViewController_tAD526B71F173BFC7288C62D614709D306E1F514B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (SearchHolesViewController_tE205ACE2901EDCA4FAC9EBC0281CEB8DD43F433D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[4] = 
{
	SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40::get_offset_of_regionBtn_4(),
	SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40::get_offset_of_searchBtn_5(),
	SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40::get_offset_of_searchPanelCont_6(),
	SearchScansViewController_t89708B48EB37AFAC10D5AF8C38072BED22899D40::get_offset_of_regionPanelCont_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126), -1, sizeof(SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3745[1] = 
{
	SelectGreenViewController_t37F1BBDC6955472D676E3155DE51FF1D77776126_StaticFields::get_offset_of_SelectGreenVCInit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0), -1, sizeof(SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3746[3] = 
{
	SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0_StaticFields::get_offset_of_SetHoleVCInit_4(),
	SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0::get_offset_of_cameraManager_5(),
	SetHoleViewController_t70700C410C33A4B1FCE8B193E652DDD1BF71A4E0::get_offset_of_holeController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53), -1, sizeof(SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3747[11] = 
{
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields::get_offset_of_PlayClicked_4(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields::get_offset_of_LoopPuttEvent_5(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53_StaticFields::get_offset_of_SimulationPuttSpeedSet_6(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_playPauseBtn_7(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_playSprite_8(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_pauseSprite_9(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_loopToggle_10(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_slowBtn_11(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_normalBtn_12(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_fastBtn_13(),
	SimulationViewController_t905F4C8B64F0FE3582D14878F52E89C31C7FEF53::get_offset_of_puttNowSubNavBtn_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[3] = 
{
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_greenId_0(),
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_rotation_1(),
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[5] = 
{
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_stimp_0(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_firmness_1(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_isMetric_2(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_holeLocation_3(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_ballLocation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[2] = 
{
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64::get_offset_of_x_0(),
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[2] = 
{
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42::get_offset_of_x_0(),
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[9] = 
{
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenId_0(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeXPos_1(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeYPos_2(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_isPlaying_3(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_isLooping_4(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_speed_5(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenCameraSettings_6(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeContainerSettings_7(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenPutts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[3] = 
{
	GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF::get_offset_of_ballXPos_0(),
	GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF::get_offset_of_ballYPos_1(),
	GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF::get_offset_of_puttId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3754[5] = 
{
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_xPos_0(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_yPos_1(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_zPos_2(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_size_3(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_yRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[4] = 
{
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_xPos_0(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_yPos_1(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_zPos_2(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_holeRectYRotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[3] = 
{
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_jacks_0(),
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_jackId_1(),
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_location_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[3] = 
{
	Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC::get_offset_of_index_0(),
	Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC::get_offset_of_elevation_1(),
	Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC::get_offset_of_offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[3] = 
{
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows1_0(),
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows2_1(),
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows3_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3759[5] = 
{
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_point_0(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_slope_1(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_rotation_2(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_level_3(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[5] = 
{
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_r_0(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_g_1(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_b_2(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_a_3(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_hex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[3] = 
{
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_x_0(),
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_y_1(),
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[1] = 
{
	Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42::get_offset_of_contourPaths_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3763[1] = 
{
	ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6::get_offset_of_path_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3764[4] = 
{
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_x_0(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_y_1(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_z_2(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_index_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[1] = 
{
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3766[13] = 
{
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_courseId_0(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_sourceId_1(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_name_2(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_address_3(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_city_4(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_region_5(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_postalCode_6(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_countryCode_7(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_profileImage_8(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_latitude_9(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_longitude_10(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_isActive_11(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_settings_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[8] = 
{
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isHoleLocationSoftware_0(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_holoVersion_1(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isLockedFromSale_2(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_customLockMessage_3(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_lastPinsheetTemplateId_4(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isCourseDataLocked_5(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isGreenDataLocked_6(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isGpsDataLocked_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[2] = 
{
	CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA::get_offset_of_settings_0(),
	CourseCalender_tBA5F8CCB22EE4F5F9687C935CFB55CC71E4A86EA::get_offset_of_calendar_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[11] = 
{
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_dailyId_0(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_courseId_1(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_version_2(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isActive_3(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isSunday_4(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isMonday_5(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isTuesday_6(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isWednesday_7(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isThursday_8(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isFriday_9(),
	CourseCalenderSettings_t3648F4699BFE264FFFBF537C2B0655E969B7F6B1::get_offset_of_isSaturday_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3770[4] = 
{
	Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093::get_offset_of_year_0(),
	Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093::get_offset_of_month_1(),
	Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093::get_offset_of_name_2(),
	Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093::get_offset_of_days_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3771[6] = 
{
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_year_0(),
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_month_1(),
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_day_2(),
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_weekday_3(),
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_name_4(),
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA::get_offset_of_dailyTemplate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3772[5] = 
{
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED::get_offset_of_dailyTemplateId_0(),
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED::get_offset_of_dailyTemplateTypeId_1(),
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED::get_offset_of_name_2(),
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED::get_offset_of_isActive_3(),
	DailyTemplate_t5C457F4435E5F57CB697DC34A7CF9B45FF5B1AED::get_offset_of_holeLocations_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3773[6] = 
{
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_dailyTemplateHoleLocationId_0(),
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_greenId_1(),
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_groupId_2(),
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_holeNumber_3(),
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_x_4(),
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF::get_offset_of_y_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3774[2] = 
{
	ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D::get_offset_of_arrowCollectedList_0(),
	ElevationDataForTransfer_tBFB6DD0AA674E6215E04DF677ACD8E09FCFE963D::get_offset_of_contourCollectedList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3775[3] = 
{
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_courseId_0(),
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_name_1(),
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_groups_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3776[6] = 
{
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_groupId_0(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_name_1(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_sortOrder_2(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_stimp_3(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_greens_4(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_greenGroupType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3777[3] = 
{
	GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3778[10] = 
{
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_holeLocations_0(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_name_1(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_rotation_2(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_greenId_3(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_groupId_4(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_number_5(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_latitude_6(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_longitude_7(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_trueNorth_8(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_DXFFile_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3779[5] = 
{
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_id_0(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_greenId_1(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_rotation_2(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_x_3(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_y_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3780[1] = 
{
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[4] = 
{
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_puttTrainerId_0(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_configurationId_1(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_createdOn_2(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_configurationData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[2] = 
{
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31::get_offset_of_value_0(),
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31::get_offset_of_puttVariationID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3783[13] = 
{
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_puttID_0(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_isValidPutt_1(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_message_2(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_ball_3(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_hole_4(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_target_5(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_ballToHoleDistance_6(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_puttingDistance_7(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_correctionAngle_8(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_elevationChange_9(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_speedFactor_10(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_points_11(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_instructions_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (Ball_t094765B02879225F8C595564E40A823B70AEE45E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[3] = 
{
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_x_0(),
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_y_1(),
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[3] = 
{
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_x_0(),
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_y_1(),
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[3] = 
{
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_x_0(),
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_y_1(),
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3787[3] = 
{
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_speed_0(),
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_location_1(),
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_slope_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[3] = 
{
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_x_0(),
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_y_1(),
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[4] = 
{
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_aim_0(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_weight_1(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_toHole_2(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_elevation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[1] = 
{
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3791[4] = 
{
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_name_0(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_regionCode_1(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_countryCode_2(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_courseCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[5] = 
{
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_name_0(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_roboGreenID_1(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_memberId_2(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_token_3(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_memberDetail_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[4] = 
{
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_emailAddress_0(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_roles_1(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_courses_2(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_printers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3794[3] = 
{
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_roleId_0(),
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_name_1(),
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[3] = 
{
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_courseId_0(),
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_sourceId_1(),
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[2] = 
{
	UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838::get_offset_of_printerId_0(),
	UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[8] = 
{
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_threshold_7(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_intensity_8(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_blurSize_9(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_resolution_10(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_blurIterations_11(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_blurType_12(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_fastBloomShader_13(),
	BloomOptimized_tF196D91797F45F2E2A6B293421FDC3C8721BC730::get_offset_of_fastBloomMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (Resolution_t8B57100A723C02C86B12199571699A420CA4B409)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3798[3] = 
{
	Resolution_t8B57100A723C02C86B12199571699A420CA4B409::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3799[3] = 
{
	BlurType_tD22A6B2AD87D6A22FD745EC9E44EEF824400CD44::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
