﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ETCAxis
struct ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9;
// ETCBase
struct ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093;
// ETCButton/OnDownHandler
struct OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD;
// ETCButton/OnPressedHandler
struct OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46;
// ETCButton/OnPressedValueandler
struct OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C;
// ETCButton/OnUPHandler
struct OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD;
// ETCDPad/OnDownDownHandler
struct OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6;
// ETCDPad/OnDownLeftHandler
struct OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D;
// ETCDPad/OnDownRightHandler
struct OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99;
// ETCDPad/OnDownUpHandler
struct OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3;
// ETCDPad/OnMoveEndHandler
struct OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF;
// ETCDPad/OnMoveHandler
struct OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A;
// ETCDPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57;
// ETCDPad/OnMoveStartHandler
struct OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9;
// ETCDPad/OnTouchStartHandler
struct OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27;
// ETCDPad/OnTouchUPHandler
struct OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24;
// ETCJoystick/OnDownDownHandler
struct OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA;
// ETCJoystick/OnDownLeftHandler
struct OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754;
// ETCJoystick/OnDownRightHandler
struct OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289;
// ETCJoystick/OnDownUpHandler
struct OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92;
// ETCJoystick/OnMoveEndHandler
struct OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671;
// ETCJoystick/OnMoveHandler
struct OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D;
// ETCJoystick/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB;
// ETCJoystick/OnMoveStartHandler
struct OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57;
// ETCJoystick/OnTouchStartHandler
struct OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3;
// ETCJoystick/OnTouchUpHandler
struct OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0;
// ETCTouchPad/OnDownDownHandler
struct OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238;
// ETCTouchPad/OnDownLeftHandler
struct OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180;
// ETCTouchPad/OnDownRightHandler
struct OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C;
// ETCTouchPad/OnDownUpHandler
struct OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230;
// ETCTouchPad/OnMoveEndHandler
struct OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0;
// ETCTouchPad/OnMoveHandler
struct OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1;
// ETCTouchPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9;
// ETCTouchPad/OnMoveStartHandler
struct OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F;
// ETCTouchPad/OnTouchStartHandler
struct OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09;
// ETCTouchPad/OnTouchUPHandler
struct OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0;
// FPSPlayerControl
struct FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469;
// System.Action`1<CI.HttpClient.HttpResponseMessage>
struct Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,ETCAxis>
struct Dictionary_2_t4A5B7E37A9402EC23D05A1658456208C6658531C;
// System.Collections.Generic.Dictionary`2<System.String,ETCBase>
struct Dictionary_2_t7AEE0F0C06194E196C14A615E13920AADB41E1AC;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCLEARTEXTU3ED__5_T08105821CD0F6E31FFB7E294A5B808103F134697_H
#define U3CCLEARTEXTU3ED__5_T08105821CD0F6E31FFB7E294A5B808103F134697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonInputUI_<ClearText>d__5
struct  U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697  : public RuntimeObject
{
public:
	// System.Int32 ButtonInputUI_<ClearText>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ButtonInputUI_<ClearText>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Text ButtonInputUI_<ClearText>d__5::textToCLead
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textToCLead_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_textToCLead_2() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697, ___textToCLead_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textToCLead_2() const { return ___textToCLead_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textToCLead_2() { return &___textToCLead_2; }
	inline void set_textToCLead_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textToCLead_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToCLead_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARTEXTU3ED__5_T08105821CD0F6E31FFB7E294A5B808103F134697_H
#ifndef U3CCLEARTEXTU3ED__8_T5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170_H
#define U3CCLEARTEXTU3ED__8_T5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonUIEvent_<ClearText>d__8
struct  U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170  : public RuntimeObject
{
public:
	// System.Int32 ButtonUIEvent_<ClearText>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ButtonUIEvent_<ClearText>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Text ButtonUIEvent_<ClearText>d__8::textToCLead
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textToCLead_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_textToCLead_2() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170, ___textToCLead_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textToCLead_2() const { return ___textToCLead_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textToCLead_2() { return &___textToCLead_2; }
	inline void set_textToCLead_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textToCLead_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToCLead_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARTEXTU3ED__8_T5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170_H
#ifndef COMPONENTEXTENSIONS_TA12A26B2529343C28D98F632D33E53551698E8EA_H
#define COMPONENTEXTENSIONS_TA12A26B2529343C28D98F632D33E53551698E8EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComponentExtensions
struct  ComponentExtensions_tA12A26B2529343C28D98F632D33E53551698E8EA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONS_TA12A26B2529343C28D98F632D33E53551698E8EA_H
#ifndef U3CCLEARTEXTU3ED__33_T11A0AD90DD1DDBA472B127878B4B6A37464DE81B_H
#define U3CCLEARTEXTU3ED__33_T11A0AD90DD1DDBA472B127878B4B6A37464DE81B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIEvent_<ClearText>d__33
struct  U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B  : public RuntimeObject
{
public:
	// System.Int32 ControlUIEvent_<ClearText>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ControlUIEvent_<ClearText>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Text ControlUIEvent_<ClearText>d__33::textToCLead
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textToCLead_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_textToCLead_2() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B, ___textToCLead_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textToCLead_2() const { return ___textToCLead_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textToCLead_2() { return &___textToCLead_2; }
	inline void set_textToCLead_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textToCLead_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToCLead_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARTEXTU3ED__33_T11A0AD90DD1DDBA472B127878B4B6A37464DE81B_H
#ifndef U3CCLEARTEXTU3ED__13_T29239CDB345E703239300479369E3677C82172C8_H
#define U3CCLEARTEXTU3ED__13_T29239CDB345E703239300479369E3677C82172C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIInput_<ClearText>d__13
struct  U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8  : public RuntimeObject
{
public:
	// System.Int32 ControlUIInput_<ClearText>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ControlUIInput_<ClearText>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Text ControlUIInput_<ClearText>d__13::textToCLead
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textToCLead_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_textToCLead_2() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8, ___textToCLead_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textToCLead_2() const { return ___textToCLead_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textToCLead_2() { return &___textToCLead_2; }
	inline void set_textToCLead_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textToCLead_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToCLead_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARTEXTU3ED__13_T29239CDB345E703239300479369E3677C82172C8_H
#ifndef U3CFIXEDUPDATEVIRTUALCONTROLU3ED__79_T83A8865D2EFA7E55C0D4202F0E512F416785C3D8_H
#define U3CFIXEDUPDATEVIRTUALCONTROLU3ED__79_T83A8865D2EFA7E55C0D4202F0E512F416785C3D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_<FixedUpdateVirtualControl>d__79
struct  U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8  : public RuntimeObject
{
public:
	// System.Int32 ETCBase_<FixedUpdateVirtualControl>d__79::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ETCBase_<FixedUpdateVirtualControl>d__79::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ETCBase ETCBase_<FixedUpdateVirtualControl>d__79::<>4__this
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8, ___U3CU3E4__this_2)); }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFIXEDUPDATEVIRTUALCONTROLU3ED__79_T83A8865D2EFA7E55C0D4202F0E512F416785C3D8_H
#ifndef U3CUPDATEVIRTUALCONTROLU3ED__78_T2738E83AB3972E75A712C00B2C7A5A93978C3FE0_H
#define U3CUPDATEVIRTUALCONTROLU3ED__78_T2738E83AB3972E75A712C00B2C7A5A93978C3FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_<UpdateVirtualControl>d__78
struct  U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0  : public RuntimeObject
{
public:
	// System.Int32 ETCBase_<UpdateVirtualControl>d__78::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ETCBase_<UpdateVirtualControl>d__78::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ETCBase ETCBase_<UpdateVirtualControl>d__78::<>4__this
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0, ___U3CU3E4__this_2)); }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEVIRTUALCONTROLU3ED__78_T2738E83AB3972E75A712C00B2C7A5A93978C3FE0_H
#ifndef U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#define U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneManagerController_<>c
struct  U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields
{
public:
	// ExampleSceneManagerController_<>c ExampleSceneManagerController_<>c::<>9
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * ___U3CU3E9_0;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__3_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__3_0_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__6_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__6_0_2;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__7_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__7_0_3;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__8_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__8_0_4;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__9_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__9_0_5;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__10_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__10_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__7_0_3)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__7_0_3() const { return ___U3CU3E9__7_0_3; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__7_0_3() { return &___U3CU3E9__7_0_3; }
	inline void set_U3CU3E9__7_0_3(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__7_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__8_0_4)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__8_0_4() const { return ___U3CU3E9__8_0_4; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__8_0_4() { return &___U3CU3E9__8_0_4; }
	inline void set_U3CU3E9__8_0_4(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__8_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__9_0_5)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__9_0_5() const { return ___U3CU3E9__9_0_5; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__9_0_5() { return &___U3CU3E9__9_0_5; }
	inline void set_U3CU3E9__9_0_5(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__9_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__10_0_6)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__10_0_6() const { return ___U3CU3E9__10_0_6; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__10_0_6() { return &___U3CU3E9__10_0_6; }
	inline void set_U3CU3E9__10_0_6(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__10_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#ifndef U3CFLASHU3ED__18_T5284EE5AA31143EE0DE5403455C0DD039A950419_H
#define U3CFLASHU3ED__18_T5284EE5AA31143EE0DE5403455C0DD039A950419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSPlayerControl_<Flash>d__18
struct  U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419  : public RuntimeObject
{
public:
	// System.Int32 FPSPlayerControl_<Flash>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FPSPlayerControl_<Flash>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FPSPlayerControl FPSPlayerControl_<Flash>d__18::<>4__this
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419, ___U3CU3E4__this_2)); }
	inline FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLASHU3ED__18_T5284EE5AA31143EE0DE5403455C0DD039A950419_H
#ifndef U3CRELOADU3ED__19_TED8AA4F5BF009BD4CD9F6A6B2500555912645188_H
#define U3CRELOADU3ED__19_TED8AA4F5BF009BD4CD9F6A6B2500555912645188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSPlayerControl_<Reload>d__19
struct  U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188  : public RuntimeObject
{
public:
	// System.Int32 FPSPlayerControl_<Reload>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FPSPlayerControl_<Reload>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FPSPlayerControl FPSPlayerControl_<Reload>d__19::<>4__this
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188, ___U3CU3E4__this_2)); }
	inline FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADU3ED__19_TED8AA4F5BF009BD4CD9F6A6B2500555912645188_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CCLEARTEXTU3ED__6_T95E184D5667E1E25F538021C285D826D4FCAC555_H
#define U3CCLEARTEXTU3ED__6_T95E184D5667E1E25F538021C285D826D4FCAC555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchPadUIEvent_<ClearText>d__6
struct  U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555  : public RuntimeObject
{
public:
	// System.Int32 TouchPadUIEvent_<ClearText>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TouchPadUIEvent_<ClearText>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Text TouchPadUIEvent_<ClearText>d__6::textToCLead
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textToCLead_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_textToCLead_2() { return static_cast<int32_t>(offsetof(U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555, ___textToCLead_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textToCLead_2() const { return ___textToCLead_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textToCLead_2() { return &___textToCLead_2; }
	inline void set_textToCLead_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textToCLead_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToCLead_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARTEXTU3ED__6_T95E184D5667E1E25F538021C285D826D4FCAC555_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#define UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AREAPRESET_T034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9_H
#define AREAPRESET_T034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCArea_AreaPreset
struct  AreaPreset_t034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9 
{
public:
	// System.Int32 ETCArea_AreaPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AreaPreset_t034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AREAPRESET_T034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9_H
#ifndef ACTIONON_TF6F8906139D1BE7F77EC53F6C30F311532EDD3B2_H
#define ACTIONON_TF6F8906139D1BE7F77EC53F6C30F311532EDD3B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis_ActionOn
struct  ActionOn_tF6F8906139D1BE7F77EC53F6C30F311532EDD3B2 
{
public:
	// System.Int32 ETCAxis_ActionOn::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionOn_tF6F8906139D1BE7F77EC53F6C30F311532EDD3B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONON_TF6F8906139D1BE7F77EC53F6C30F311532EDD3B2_H
#ifndef AXISINFLUENCED_T96C763AB9711242CF0DD716ADA5241F2C6498DEB_H
#define AXISINFLUENCED_T96C763AB9711242CF0DD716ADA5241F2C6498DEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis_AxisInfluenced
struct  AxisInfluenced_t96C763AB9711242CF0DD716ADA5241F2C6498DEB 
{
public:
	// System.Int32 ETCAxis_AxisInfluenced::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisInfluenced_t96C763AB9711242CF0DD716ADA5241F2C6498DEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINFLUENCED_T96C763AB9711242CF0DD716ADA5241F2C6498DEB_H
#ifndef AXISSTATE_T4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2_H
#define AXISSTATE_T4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis_AxisState
struct  AxisState_t4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2 
{
public:
	// System.Int32 ETCAxis_AxisState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisState_t4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSTATE_T4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2_H
#ifndef AXISVALUEMETHOD_T1C86AE4E7112F90169A034FA65E53A3601BC32CA_H
#define AXISVALUEMETHOD_T1C86AE4E7112F90169A034FA65E53A3601BC32CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis_AxisValueMethod
struct  AxisValueMethod_t1C86AE4E7112F90169A034FA65E53A3601BC32CA 
{
public:
	// System.Int32 ETCAxis_AxisValueMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisValueMethod_t1C86AE4E7112F90169A034FA65E53A3601BC32CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISVALUEMETHOD_T1C86AE4E7112F90169A034FA65E53A3601BC32CA_H
#ifndef DIRECTACTION_T61B43CD6183FE5997A7C37F824B569D2AE06290C_H
#define DIRECTACTION_T61B43CD6183FE5997A7C37F824B569D2AE06290C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis_DirectAction
struct  DirectAction_t61B43CD6183FE5997A7C37F824B569D2AE06290C 
{
public:
	// System.Int32 ETCAxis_DirectAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectAction_t61B43CD6183FE5997A7C37F824B569D2AE06290C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T61B43CD6183FE5997A7C37F824B569D2AE06290C_H
#ifndef CAMERAMODE_TB88394DD8E850D9D3DEB6E93D06316507E591C1F_H
#define CAMERAMODE_TB88394DD8E850D9D3DEB6E93D06316507E591C1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_CameraMode
struct  CameraMode_tB88394DD8E850D9D3DEB6E93D06316507E591C1F 
{
public:
	// System.Int32 ETCBase_CameraMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraMode_tB88394DD8E850D9D3DEB6E93D06316507E591C1F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODE_TB88394DD8E850D9D3DEB6E93D06316507E591C1F_H
#ifndef CAMERATARGETMODE_T1D7A162CEB13387377CACC9D5190AABE90AF176D_H
#define CAMERATARGETMODE_T1D7A162CEB13387377CACC9D5190AABE90AF176D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_CameraTargetMode
struct  CameraTargetMode_t1D7A162CEB13387377CACC9D5190AABE90AF176D 
{
public:
	// System.Int32 ETCBase_CameraTargetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraTargetMode_t1D7A162CEB13387377CACC9D5190AABE90AF176D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATARGETMODE_T1D7A162CEB13387377CACC9D5190AABE90AF176D_H
#ifndef CONTROLTYPE_T2C7A7438CCA278A1A1764500339DDC2A97583F74_H
#define CONTROLTYPE_T2C7A7438CCA278A1A1764500339DDC2A97583F74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_ControlType
struct  ControlType_t2C7A7438CCA278A1A1764500339DDC2A97583F74 
{
public:
	// System.Int32 ETCBase_ControlType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlType_t2C7A7438CCA278A1A1764500339DDC2A97583F74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTYPE_T2C7A7438CCA278A1A1764500339DDC2A97583F74_H
#ifndef DPADAXIS_T043E01EF634A00A308D177ACD6A5C63D401FC217_H
#define DPADAXIS_T043E01EF634A00A308D177ACD6A5C63D401FC217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_DPadAxis
struct  DPadAxis_t043E01EF634A00A308D177ACD6A5C63D401FC217 
{
public:
	// System.Int32 ETCBase_DPadAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DPadAxis_t043E01EF634A00A308D177ACD6A5C63D401FC217, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPADAXIS_T043E01EF634A00A308D177ACD6A5C63D401FC217_H
#ifndef RECTANCHOR_TAF34790C8468DF8E5E5982B2F86D6BA15BB943C2_H
#define RECTANCHOR_TAF34790C8468DF8E5E5982B2F86D6BA15BB943C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase_RectAnchor
struct  RectAnchor_tAF34790C8468DF8E5E5982B2F86D6BA15BB943C2 
{
public:
	// System.Int32 ETCBase_RectAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RectAnchor_tAF34790C8468DF8E5E5982B2F86D6BA15BB943C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANCHOR_TAF34790C8468DF8E5E5982B2F86D6BA15BB943C2_H
#ifndef ONDOWNHANDLER_T3BF646CB3B50D2B1568AD29812CFA5AF62241ECD_H
#define ONDOWNHANDLER_T3BF646CB3B50D2B1568AD29812CFA5AF62241ECD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton_OnDownHandler
struct  OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNHANDLER_T3BF646CB3B50D2B1568AD29812CFA5AF62241ECD_H
#ifndef ONPRESSEDHANDLER_TAACE80106F872BB737F1A63DE93CF90A88518E46_H
#define ONPRESSEDHANDLER_TAACE80106F872BB737F1A63DE93CF90A88518E46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton_OnPressedHandler
struct  OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSEDHANDLER_TAACE80106F872BB737F1A63DE93CF90A88518E46_H
#ifndef ONPRESSEDVALUEANDLER_TE434F6E6375463F07A22F2F133D6E4D863A19C8C_H
#define ONPRESSEDVALUEANDLER_TE434F6E6375463F07A22F2F133D6E4D863A19C8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton_OnPressedValueandler
struct  OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSEDVALUEANDLER_TE434F6E6375463F07A22F2F133D6E4D863A19C8C_H
#ifndef ONUPHANDLER_T5E09F875FD5F20532E1604E545DD1081A51BB6BD_H
#define ONUPHANDLER_T5E09F875FD5F20532E1604E545DD1081A51BB6BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton_OnUPHandler
struct  OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPHANDLER_T5E09F875FD5F20532E1604E545DD1081A51BB6BD_H
#ifndef ONDOWNDOWNHANDLER_TA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6_H
#define ONDOWNDOWNHANDLER_TA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnDownDownHandler
struct  OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_TA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6_H
#ifndef ONDOWNLEFTHANDLER_T7E32D2BEBB53C437DA1A698E56ACDA764957164D_H
#define ONDOWNLEFTHANDLER_T7E32D2BEBB53C437DA1A698E56ACDA764957164D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnDownLeftHandler
struct  OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T7E32D2BEBB53C437DA1A698E56ACDA764957164D_H
#ifndef ONDOWNRIGHTHANDLER_TEBFF6E41C7963CBCDAE16698D596E3057E5A3F99_H
#define ONDOWNRIGHTHANDLER_TEBFF6E41C7963CBCDAE16698D596E3057E5A3F99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnDownRightHandler
struct  OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_TEBFF6E41C7963CBCDAE16698D596E3057E5A3F99_H
#ifndef ONDOWNUPHANDLER_T2398088B051F3862749014C92250C3B138A120D3_H
#define ONDOWNUPHANDLER_T2398088B051F3862749014C92250C3B138A120D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnDownUpHandler
struct  OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_T2398088B051F3862749014C92250C3B138A120D3_H
#ifndef ONMOVEENDHANDLER_T3C6D36AF08ABF808E58B32604A4476968D845FAF_H
#define ONMOVEENDHANDLER_T3C6D36AF08ABF808E58B32604A4476968D845FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnMoveEndHandler
struct  OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_T3C6D36AF08ABF808E58B32604A4476968D845FAF_H
#ifndef ONMOVEHANDLER_TE90F89A5EF87D5519185F00FDBC4984560643A6A_H
#define ONMOVEHANDLER_TE90F89A5EF87D5519185F00FDBC4984560643A6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnMoveHandler
struct  OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_TE90F89A5EF87D5519185F00FDBC4984560643A6A_H
#ifndef ONMOVESPEEDHANDLER_T59F42533F55D150D683041ECB2F2CB33E8897B57_H
#define ONMOVESPEEDHANDLER_T59F42533F55D150D683041ECB2F2CB33E8897B57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnMoveSpeedHandler
struct  OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_T59F42533F55D150D683041ECB2F2CB33E8897B57_H
#ifndef ONMOVESTARTHANDLER_T348E0BECE9C131ABDB6405E3375FACBEDD7834F9_H
#define ONMOVESTARTHANDLER_T348E0BECE9C131ABDB6405E3375FACBEDD7834F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnMoveStartHandler
struct  OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T348E0BECE9C131ABDB6405E3375FACBEDD7834F9_H
#ifndef ONPRESSDOWNHANDLER_TE5E9A24BAC75D8565FF6D7F5FFD0E328C8C91F02_H
#define ONPRESSDOWNHANDLER_TE5E9A24BAC75D8565FF6D7F5FFD0E328C8C91F02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnPressDownHandler
struct  OnPressDownHandler_tE5E9A24BAC75D8565FF6D7F5FFD0E328C8C91F02  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_TE5E9A24BAC75D8565FF6D7F5FFD0E328C8C91F02_H
#ifndef ONPRESSLEFTHANDLER_TD6AD232FE778E7A6E0A5A6D4A7F541C494EC85AF_H
#define ONPRESSLEFTHANDLER_TD6AD232FE778E7A6E0A5A6D4A7F541C494EC85AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnPressLeftHandler
struct  OnPressLeftHandler_tD6AD232FE778E7A6E0A5A6D4A7F541C494EC85AF  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_TD6AD232FE778E7A6E0A5A6D4A7F541C494EC85AF_H
#ifndef ONPRESSRIGHTHANDLER_TE4E38BBEF2C4F0A6293D340E4D18282DC391BBCD_H
#define ONPRESSRIGHTHANDLER_TE4E38BBEF2C4F0A6293D340E4D18282DC391BBCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnPressRightHandler
struct  OnPressRightHandler_tE4E38BBEF2C4F0A6293D340E4D18282DC391BBCD  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_TE4E38BBEF2C4F0A6293D340E4D18282DC391BBCD_H
#ifndef ONPRESSUPHANDLER_T5EE87DF384558AAFE245813FD586DA9818A33CE3_H
#define ONPRESSUPHANDLER_T5EE87DF384558AAFE245813FD586DA9818A33CE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnPressUpHandler
struct  OnPressUpHandler_t5EE87DF384558AAFE245813FD586DA9818A33CE3  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_T5EE87DF384558AAFE245813FD586DA9818A33CE3_H
#ifndef ONTOUCHSTARTHANDLER_T76BF5BC87655858404EDBC8DE0D3ED8C502C8D27_H
#define ONTOUCHSTARTHANDLER_T76BF5BC87655858404EDBC8DE0D3ED8C502C8D27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnTouchStartHandler
struct  OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T76BF5BC87655858404EDBC8DE0D3ED8C502C8D27_H
#ifndef ONTOUCHUPHANDLER_T1D7253E44E150B5F82C580FC7686BEFB9EF2AA24_H
#define ONTOUCHUPHANDLER_T1D7253E44E150B5F82C580FC7686BEFB9EF2AA24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad_OnTouchUPHandler
struct  OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_T1D7253E44E150B5F82C580FC7686BEFB9EF2AA24_H
#ifndef JOYSTICKAREA_T2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431_H
#define JOYSTICKAREA_T2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_JoystickArea
struct  JoystickArea_t2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431 
{
public:
	// System.Int32 ETCJoystick_JoystickArea::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickArea_t2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKAREA_T2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431_H
#ifndef JOYSTICKTYPE_T2C849C7AC45C129E428F8CAC18E82EBFF0908789_H
#define JOYSTICKTYPE_T2C849C7AC45C129E428F8CAC18E82EBFF0908789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_JoystickType
struct  JoystickType_t2C849C7AC45C129E428F8CAC18E82EBFF0908789 
{
public:
	// System.Int32 ETCJoystick_JoystickType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickType_t2C849C7AC45C129E428F8CAC18E82EBFF0908789, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKTYPE_T2C849C7AC45C129E428F8CAC18E82EBFF0908789_H
#ifndef ONDOWNDOWNHANDLER_T332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA_H
#define ONDOWNDOWNHANDLER_T332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnDownDownHandler
struct  OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_T332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA_H
#ifndef ONDOWNLEFTHANDLER_T3102AEBE0635BC6F724C1206DAA445D4E77C7754_H
#define ONDOWNLEFTHANDLER_T3102AEBE0635BC6F724C1206DAA445D4E77C7754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnDownLeftHandler
struct  OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T3102AEBE0635BC6F724C1206DAA445D4E77C7754_H
#ifndef ONDOWNRIGHTHANDLER_T89C4C65DE698BF06140FC071F94FADE8007BF289_H
#define ONDOWNRIGHTHANDLER_T89C4C65DE698BF06140FC071F94FADE8007BF289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnDownRightHandler
struct  OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_T89C4C65DE698BF06140FC071F94FADE8007BF289_H
#ifndef ONDOWNUPHANDLER_TC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92_H
#define ONDOWNUPHANDLER_TC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnDownUpHandler
struct  OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_TC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92_H
#ifndef ONMOVEENDHANDLER_T5FE43EFCB773CDAED76BFA1153F06E4F47EAD671_H
#define ONMOVEENDHANDLER_T5FE43EFCB773CDAED76BFA1153F06E4F47EAD671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnMoveEndHandler
struct  OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_T5FE43EFCB773CDAED76BFA1153F06E4F47EAD671_H
#ifndef ONMOVEHANDLER_T8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D_H
#define ONMOVEHANDLER_T8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnMoveHandler
struct  OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_T8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D_H
#ifndef ONMOVESPEEDHANDLER_T4FA4A5C082FB5B461963C2AE7F1374093C8654EB_H
#define ONMOVESPEEDHANDLER_T4FA4A5C082FB5B461963C2AE7F1374093C8654EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnMoveSpeedHandler
struct  OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_T4FA4A5C082FB5B461963C2AE7F1374093C8654EB_H
#ifndef ONMOVESTARTHANDLER_T93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57_H
#define ONMOVESTARTHANDLER_T93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnMoveStartHandler
struct  OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57_H
#ifndef ONPRESSDOWNHANDLER_T199761A0C6A223F754D8F1003519D886F2452F4F_H
#define ONPRESSDOWNHANDLER_T199761A0C6A223F754D8F1003519D886F2452F4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnPressDownHandler
struct  OnPressDownHandler_t199761A0C6A223F754D8F1003519D886F2452F4F  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_T199761A0C6A223F754D8F1003519D886F2452F4F_H
#ifndef ONPRESSLEFTHANDLER_T2D68FB3D052175567C04A15DF35F95D52F3F9CE9_H
#define ONPRESSLEFTHANDLER_T2D68FB3D052175567C04A15DF35F95D52F3F9CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnPressLeftHandler
struct  OnPressLeftHandler_t2D68FB3D052175567C04A15DF35F95D52F3F9CE9  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_T2D68FB3D052175567C04A15DF35F95D52F3F9CE9_H
#ifndef ONPRESSRIGHTHANDLER_T570DBE2FD74FE8A4A70D6F52A9E7AD058AE91B8B_H
#define ONPRESSRIGHTHANDLER_T570DBE2FD74FE8A4A70D6F52A9E7AD058AE91B8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnPressRightHandler
struct  OnPressRightHandler_t570DBE2FD74FE8A4A70D6F52A9E7AD058AE91B8B  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_T570DBE2FD74FE8A4A70D6F52A9E7AD058AE91B8B_H
#ifndef ONPRESSUPHANDLER_T2DA53FA695B966CC1205F67A4AD0A47447FE4A0C_H
#define ONPRESSUPHANDLER_T2DA53FA695B966CC1205F67A4AD0A47447FE4A0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnPressUpHandler
struct  OnPressUpHandler_t2DA53FA695B966CC1205F67A4AD0A47447FE4A0C  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_T2DA53FA695B966CC1205F67A4AD0A47447FE4A0C_H
#ifndef ONTOUCHSTARTHANDLER_T27951F79DE8A476D11870053691271C6BE9883C3_H
#define ONTOUCHSTARTHANDLER_T27951F79DE8A476D11870053691271C6BE9883C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnTouchStartHandler
struct  OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T27951F79DE8A476D11870053691271C6BE9883C3_H
#ifndef ONTOUCHUPHANDLER_T108B17FD19F7EE3EA4F053FFAACE81D93CF806A0_H
#define ONTOUCHUPHANDLER_T108B17FD19F7EE3EA4F053FFAACE81D93CF806A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_OnTouchUpHandler
struct  OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_T108B17FD19F7EE3EA4F053FFAACE81D93CF806A0_H
#ifndef RADIUSBASE_T8E1E477A54EAA92094A6719E049B7A945A4ED46B_H
#define RADIUSBASE_T8E1E477A54EAA92094A6719E049B7A945A4ED46B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick_RadiusBase
struct  RadiusBase_t8E1E477A54EAA92094A6719E049B7A945A4ED46B 
{
public:
	// System.Int32 ETCJoystick_RadiusBase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RadiusBase_t8E1E477A54EAA92094A6719E049B7A945A4ED46B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIUSBASE_T8E1E477A54EAA92094A6719E049B7A945A4ED46B_H
#ifndef ONDOWNDOWNHANDLER_T2333AB8FF412905EEDE71B4606A4E0ED11607238_H
#define ONDOWNDOWNHANDLER_T2333AB8FF412905EEDE71B4606A4E0ED11607238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnDownDownHandler
struct  OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_T2333AB8FF412905EEDE71B4606A4E0ED11607238_H
#ifndef ONDOWNLEFTHANDLER_T0A884CFFF54E7772A01938939E367E6F37B04180_H
#define ONDOWNLEFTHANDLER_T0A884CFFF54E7772A01938939E367E6F37B04180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnDownLeftHandler
struct  OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T0A884CFFF54E7772A01938939E367E6F37B04180_H
#ifndef ONDOWNRIGHTHANDLER_T308C5A100EF894D2125DF3DA2CC71647D50A015C_H
#define ONDOWNRIGHTHANDLER_T308C5A100EF894D2125DF3DA2CC71647D50A015C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnDownRightHandler
struct  OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_T308C5A100EF894D2125DF3DA2CC71647D50A015C_H
#ifndef ONDOWNUPHANDLER_TA0188AE5248CC4DBE4D256C3D621BA3A5A028230_H
#define ONDOWNUPHANDLER_TA0188AE5248CC4DBE4D256C3D621BA3A5A028230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnDownUpHandler
struct  OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_TA0188AE5248CC4DBE4D256C3D621BA3A5A028230_H
#ifndef ONMOVEENDHANDLER_TEF560D68C74BB3CA91EC145CA31BFA026F6D63F0_H
#define ONMOVEENDHANDLER_TEF560D68C74BB3CA91EC145CA31BFA026F6D63F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnMoveEndHandler
struct  OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_TEF560D68C74BB3CA91EC145CA31BFA026F6D63F0_H
#ifndef ONMOVEHANDLER_T8A852696E18CD6E61C923170A7A75210923721D1_H
#define ONMOVEHANDLER_T8A852696E18CD6E61C923170A7A75210923721D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnMoveHandler
struct  OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_T8A852696E18CD6E61C923170A7A75210923721D1_H
#ifndef ONMOVESPEEDHANDLER_TB0B9DECF77150E5A04DDF3E31081C277F18AADC9_H
#define ONMOVESPEEDHANDLER_TB0B9DECF77150E5A04DDF3E31081C277F18AADC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnMoveSpeedHandler
struct  OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_TB0B9DECF77150E5A04DDF3E31081C277F18AADC9_H
#ifndef ONMOVESTARTHANDLER_T9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F_H
#define ONMOVESTARTHANDLER_T9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnMoveStartHandler
struct  OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F_H
#ifndef ONPRESSDOWNHANDLER_TF8A3F6CF4DEAE985028EDDAF48E527AD33D8A0EE_H
#define ONPRESSDOWNHANDLER_TF8A3F6CF4DEAE985028EDDAF48E527AD33D8A0EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnPressDownHandler
struct  OnPressDownHandler_tF8A3F6CF4DEAE985028EDDAF48E527AD33D8A0EE  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_TF8A3F6CF4DEAE985028EDDAF48E527AD33D8A0EE_H
#ifndef ONPRESSLEFTHANDLER_TE72C97A205D17B70AA104A82EEEA9F357CC39407_H
#define ONPRESSLEFTHANDLER_TE72C97A205D17B70AA104A82EEEA9F357CC39407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnPressLeftHandler
struct  OnPressLeftHandler_tE72C97A205D17B70AA104A82EEEA9F357CC39407  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_TE72C97A205D17B70AA104A82EEEA9F357CC39407_H
#ifndef ONPRESSRIGHTHANDLER_T2AEBDCCE01BBCF41C292B7D2CE164A4C2B36858E_H
#define ONPRESSRIGHTHANDLER_T2AEBDCCE01BBCF41C292B7D2CE164A4C2B36858E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnPressRightHandler
struct  OnPressRightHandler_t2AEBDCCE01BBCF41C292B7D2CE164A4C2B36858E  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_T2AEBDCCE01BBCF41C292B7D2CE164A4C2B36858E_H
#ifndef ONPRESSUPHANDLER_TE7F29A9AD0AB3C09F62525557D629A8059369D17_H
#define ONPRESSUPHANDLER_TE7F29A9AD0AB3C09F62525557D629A8059369D17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnPressUpHandler
struct  OnPressUpHandler_tE7F29A9AD0AB3C09F62525557D629A8059369D17  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_TE7F29A9AD0AB3C09F62525557D629A8059369D17_H
#ifndef ONTOUCHSTARTHANDLER_T76C519DE2C1AB61047DF39AFD71411888414ED09_H
#define ONTOUCHSTARTHANDLER_T76C519DE2C1AB61047DF39AFD71411888414ED09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnTouchStartHandler
struct  OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T76C519DE2C1AB61047DF39AFD71411888414ED09_H
#ifndef ONTOUCHUPHANDLER_TBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0_H
#define ONTOUCHUPHANDLER_TBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad_OnTouchUPHandler
struct  OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_TBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ETCAXIS_T950EE7D6F878394ACEF29C1F6CF346541A1230E9_H
#define ETCAXIS_T950EE7D6F878394ACEF29C1F6CF346541A1230E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis
struct  ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9  : public RuntimeObject
{
public:
	// System.String ETCAxis::name
	String_t* ___name_0;
	// System.Boolean ETCAxis::autoLinkTagPlayer
	bool ___autoLinkTagPlayer_1;
	// System.String ETCAxis::autoTag
	String_t* ___autoTag_2;
	// UnityEngine.GameObject ETCAxis::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_3;
	// System.Boolean ETCAxis::enable
	bool ___enable_4;
	// System.Boolean ETCAxis::invertedAxis
	bool ___invertedAxis_5;
	// System.Single ETCAxis::speed
	float ___speed_6;
	// System.Single ETCAxis::deadValue
	float ___deadValue_7;
	// ETCAxis_AxisValueMethod ETCAxis::valueMethod
	int32_t ___valueMethod_8;
	// UnityEngine.AnimationCurve ETCAxis::curveValue
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curveValue_9;
	// System.Boolean ETCAxis::isEnertia
	bool ___isEnertia_10;
	// System.Single ETCAxis::inertia
	float ___inertia_11;
	// System.Single ETCAxis::inertiaThreshold
	float ___inertiaThreshold_12;
	// System.Boolean ETCAxis::isAutoStab
	bool ___isAutoStab_13;
	// System.Single ETCAxis::autoStabThreshold
	float ___autoStabThreshold_14;
	// System.Single ETCAxis::autoStabSpeed
	float ___autoStabSpeed_15;
	// System.Single ETCAxis::startAngle
	float ___startAngle_16;
	// System.Boolean ETCAxis::isClampRotation
	bool ___isClampRotation_17;
	// System.Single ETCAxis::maxAngle
	float ___maxAngle_18;
	// System.Single ETCAxis::minAngle
	float ___minAngle_19;
	// System.Boolean ETCAxis::isValueOverTime
	bool ___isValueOverTime_20;
	// System.Single ETCAxis::overTimeStep
	float ___overTimeStep_21;
	// System.Single ETCAxis::maxOverTimeValue
	float ___maxOverTimeValue_22;
	// System.Single ETCAxis::axisValue
	float ___axisValue_23;
	// System.Single ETCAxis::axisSpeedValue
	float ___axisSpeedValue_24;
	// System.Single ETCAxis::axisThreshold
	float ___axisThreshold_25;
	// System.Boolean ETCAxis::isLockinJump
	bool ___isLockinJump_26;
	// UnityEngine.Vector3 ETCAxis::lastMove
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastMove_27;
	// ETCAxis_AxisState ETCAxis::axisState
	int32_t ___axisState_28;
	// UnityEngine.Transform ETCAxis::_directTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____directTransform_29;
	// ETCAxis_DirectAction ETCAxis::directAction
	int32_t ___directAction_30;
	// ETCAxis_AxisInfluenced ETCAxis::axisInfluenced
	int32_t ___axisInfluenced_31;
	// ETCAxis_ActionOn ETCAxis::actionOn
	int32_t ___actionOn_32;
	// UnityEngine.CharacterController ETCAxis::directCharacterController
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___directCharacterController_33;
	// UnityEngine.Rigidbody ETCAxis::directRigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___directRigidBody_34;
	// System.Single ETCAxis::gravity
	float ___gravity_35;
	// System.Single ETCAxis::currentGravity
	float ___currentGravity_36;
	// System.Boolean ETCAxis::isJump
	bool ___isJump_37;
	// System.String ETCAxis::unityAxis
	String_t* ___unityAxis_38;
	// System.Boolean ETCAxis::showGeneralInspector
	bool ___showGeneralInspector_39;
	// System.Boolean ETCAxis::showDirectInspector
	bool ___showDirectInspector_40;
	// System.Boolean ETCAxis::showInertiaInspector
	bool ___showInertiaInspector_41;
	// System.Boolean ETCAxis::showSimulatinInspector
	bool ___showSimulatinInspector_42;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_autoLinkTagPlayer_1() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___autoLinkTagPlayer_1)); }
	inline bool get_autoLinkTagPlayer_1() const { return ___autoLinkTagPlayer_1; }
	inline bool* get_address_of_autoLinkTagPlayer_1() { return &___autoLinkTagPlayer_1; }
	inline void set_autoLinkTagPlayer_1(bool value)
	{
		___autoLinkTagPlayer_1 = value;
	}

	inline static int32_t get_offset_of_autoTag_2() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___autoTag_2)); }
	inline String_t* get_autoTag_2() const { return ___autoTag_2; }
	inline String_t** get_address_of_autoTag_2() { return &___autoTag_2; }
	inline void set_autoTag_2(String_t* value)
	{
		___autoTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___autoTag_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___player_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_3() const { return ___player_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}

	inline static int32_t get_offset_of_enable_4() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___enable_4)); }
	inline bool get_enable_4() const { return ___enable_4; }
	inline bool* get_address_of_enable_4() { return &___enable_4; }
	inline void set_enable_4(bool value)
	{
		___enable_4 = value;
	}

	inline static int32_t get_offset_of_invertedAxis_5() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___invertedAxis_5)); }
	inline bool get_invertedAxis_5() const { return ___invertedAxis_5; }
	inline bool* get_address_of_invertedAxis_5() { return &___invertedAxis_5; }
	inline void set_invertedAxis_5(bool value)
	{
		___invertedAxis_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_deadValue_7() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___deadValue_7)); }
	inline float get_deadValue_7() const { return ___deadValue_7; }
	inline float* get_address_of_deadValue_7() { return &___deadValue_7; }
	inline void set_deadValue_7(float value)
	{
		___deadValue_7 = value;
	}

	inline static int32_t get_offset_of_valueMethod_8() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___valueMethod_8)); }
	inline int32_t get_valueMethod_8() const { return ___valueMethod_8; }
	inline int32_t* get_address_of_valueMethod_8() { return &___valueMethod_8; }
	inline void set_valueMethod_8(int32_t value)
	{
		___valueMethod_8 = value;
	}

	inline static int32_t get_offset_of_curveValue_9() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___curveValue_9)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curveValue_9() const { return ___curveValue_9; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curveValue_9() { return &___curveValue_9; }
	inline void set_curveValue_9(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curveValue_9 = value;
		Il2CppCodeGenWriteBarrier((&___curveValue_9), value);
	}

	inline static int32_t get_offset_of_isEnertia_10() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isEnertia_10)); }
	inline bool get_isEnertia_10() const { return ___isEnertia_10; }
	inline bool* get_address_of_isEnertia_10() { return &___isEnertia_10; }
	inline void set_isEnertia_10(bool value)
	{
		___isEnertia_10 = value;
	}

	inline static int32_t get_offset_of_inertia_11() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___inertia_11)); }
	inline float get_inertia_11() const { return ___inertia_11; }
	inline float* get_address_of_inertia_11() { return &___inertia_11; }
	inline void set_inertia_11(float value)
	{
		___inertia_11 = value;
	}

	inline static int32_t get_offset_of_inertiaThreshold_12() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___inertiaThreshold_12)); }
	inline float get_inertiaThreshold_12() const { return ___inertiaThreshold_12; }
	inline float* get_address_of_inertiaThreshold_12() { return &___inertiaThreshold_12; }
	inline void set_inertiaThreshold_12(float value)
	{
		___inertiaThreshold_12 = value;
	}

	inline static int32_t get_offset_of_isAutoStab_13() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isAutoStab_13)); }
	inline bool get_isAutoStab_13() const { return ___isAutoStab_13; }
	inline bool* get_address_of_isAutoStab_13() { return &___isAutoStab_13; }
	inline void set_isAutoStab_13(bool value)
	{
		___isAutoStab_13 = value;
	}

	inline static int32_t get_offset_of_autoStabThreshold_14() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___autoStabThreshold_14)); }
	inline float get_autoStabThreshold_14() const { return ___autoStabThreshold_14; }
	inline float* get_address_of_autoStabThreshold_14() { return &___autoStabThreshold_14; }
	inline void set_autoStabThreshold_14(float value)
	{
		___autoStabThreshold_14 = value;
	}

	inline static int32_t get_offset_of_autoStabSpeed_15() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___autoStabSpeed_15)); }
	inline float get_autoStabSpeed_15() const { return ___autoStabSpeed_15; }
	inline float* get_address_of_autoStabSpeed_15() { return &___autoStabSpeed_15; }
	inline void set_autoStabSpeed_15(float value)
	{
		___autoStabSpeed_15 = value;
	}

	inline static int32_t get_offset_of_startAngle_16() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___startAngle_16)); }
	inline float get_startAngle_16() const { return ___startAngle_16; }
	inline float* get_address_of_startAngle_16() { return &___startAngle_16; }
	inline void set_startAngle_16(float value)
	{
		___startAngle_16 = value;
	}

	inline static int32_t get_offset_of_isClampRotation_17() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isClampRotation_17)); }
	inline bool get_isClampRotation_17() const { return ___isClampRotation_17; }
	inline bool* get_address_of_isClampRotation_17() { return &___isClampRotation_17; }
	inline void set_isClampRotation_17(bool value)
	{
		___isClampRotation_17 = value;
	}

	inline static int32_t get_offset_of_maxAngle_18() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___maxAngle_18)); }
	inline float get_maxAngle_18() const { return ___maxAngle_18; }
	inline float* get_address_of_maxAngle_18() { return &___maxAngle_18; }
	inline void set_maxAngle_18(float value)
	{
		___maxAngle_18 = value;
	}

	inline static int32_t get_offset_of_minAngle_19() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___minAngle_19)); }
	inline float get_minAngle_19() const { return ___minAngle_19; }
	inline float* get_address_of_minAngle_19() { return &___minAngle_19; }
	inline void set_minAngle_19(float value)
	{
		___minAngle_19 = value;
	}

	inline static int32_t get_offset_of_isValueOverTime_20() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isValueOverTime_20)); }
	inline bool get_isValueOverTime_20() const { return ___isValueOverTime_20; }
	inline bool* get_address_of_isValueOverTime_20() { return &___isValueOverTime_20; }
	inline void set_isValueOverTime_20(bool value)
	{
		___isValueOverTime_20 = value;
	}

	inline static int32_t get_offset_of_overTimeStep_21() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___overTimeStep_21)); }
	inline float get_overTimeStep_21() const { return ___overTimeStep_21; }
	inline float* get_address_of_overTimeStep_21() { return &___overTimeStep_21; }
	inline void set_overTimeStep_21(float value)
	{
		___overTimeStep_21 = value;
	}

	inline static int32_t get_offset_of_maxOverTimeValue_22() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___maxOverTimeValue_22)); }
	inline float get_maxOverTimeValue_22() const { return ___maxOverTimeValue_22; }
	inline float* get_address_of_maxOverTimeValue_22() { return &___maxOverTimeValue_22; }
	inline void set_maxOverTimeValue_22(float value)
	{
		___maxOverTimeValue_22 = value;
	}

	inline static int32_t get_offset_of_axisValue_23() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___axisValue_23)); }
	inline float get_axisValue_23() const { return ___axisValue_23; }
	inline float* get_address_of_axisValue_23() { return &___axisValue_23; }
	inline void set_axisValue_23(float value)
	{
		___axisValue_23 = value;
	}

	inline static int32_t get_offset_of_axisSpeedValue_24() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___axisSpeedValue_24)); }
	inline float get_axisSpeedValue_24() const { return ___axisSpeedValue_24; }
	inline float* get_address_of_axisSpeedValue_24() { return &___axisSpeedValue_24; }
	inline void set_axisSpeedValue_24(float value)
	{
		___axisSpeedValue_24 = value;
	}

	inline static int32_t get_offset_of_axisThreshold_25() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___axisThreshold_25)); }
	inline float get_axisThreshold_25() const { return ___axisThreshold_25; }
	inline float* get_address_of_axisThreshold_25() { return &___axisThreshold_25; }
	inline void set_axisThreshold_25(float value)
	{
		___axisThreshold_25 = value;
	}

	inline static int32_t get_offset_of_isLockinJump_26() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isLockinJump_26)); }
	inline bool get_isLockinJump_26() const { return ___isLockinJump_26; }
	inline bool* get_address_of_isLockinJump_26() { return &___isLockinJump_26; }
	inline void set_isLockinJump_26(bool value)
	{
		___isLockinJump_26 = value;
	}

	inline static int32_t get_offset_of_lastMove_27() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___lastMove_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastMove_27() const { return ___lastMove_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastMove_27() { return &___lastMove_27; }
	inline void set_lastMove_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastMove_27 = value;
	}

	inline static int32_t get_offset_of_axisState_28() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___axisState_28)); }
	inline int32_t get_axisState_28() const { return ___axisState_28; }
	inline int32_t* get_address_of_axisState_28() { return &___axisState_28; }
	inline void set_axisState_28(int32_t value)
	{
		___axisState_28 = value;
	}

	inline static int32_t get_offset_of__directTransform_29() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ____directTransform_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__directTransform_29() const { return ____directTransform_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__directTransform_29() { return &____directTransform_29; }
	inline void set__directTransform_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____directTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&____directTransform_29), value);
	}

	inline static int32_t get_offset_of_directAction_30() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___directAction_30)); }
	inline int32_t get_directAction_30() const { return ___directAction_30; }
	inline int32_t* get_address_of_directAction_30() { return &___directAction_30; }
	inline void set_directAction_30(int32_t value)
	{
		___directAction_30 = value;
	}

	inline static int32_t get_offset_of_axisInfluenced_31() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___axisInfluenced_31)); }
	inline int32_t get_axisInfluenced_31() const { return ___axisInfluenced_31; }
	inline int32_t* get_address_of_axisInfluenced_31() { return &___axisInfluenced_31; }
	inline void set_axisInfluenced_31(int32_t value)
	{
		___axisInfluenced_31 = value;
	}

	inline static int32_t get_offset_of_actionOn_32() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___actionOn_32)); }
	inline int32_t get_actionOn_32() const { return ___actionOn_32; }
	inline int32_t* get_address_of_actionOn_32() { return &___actionOn_32; }
	inline void set_actionOn_32(int32_t value)
	{
		___actionOn_32 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_33() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___directCharacterController_33)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_directCharacterController_33() const { return ___directCharacterController_33; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_directCharacterController_33() { return &___directCharacterController_33; }
	inline void set_directCharacterController_33(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___directCharacterController_33 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_33), value);
	}

	inline static int32_t get_offset_of_directRigidBody_34() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___directRigidBody_34)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_directRigidBody_34() const { return ___directRigidBody_34; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_directRigidBody_34() { return &___directRigidBody_34; }
	inline void set_directRigidBody_34(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___directRigidBody_34 = value;
		Il2CppCodeGenWriteBarrier((&___directRigidBody_34), value);
	}

	inline static int32_t get_offset_of_gravity_35() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___gravity_35)); }
	inline float get_gravity_35() const { return ___gravity_35; }
	inline float* get_address_of_gravity_35() { return &___gravity_35; }
	inline void set_gravity_35(float value)
	{
		___gravity_35 = value;
	}

	inline static int32_t get_offset_of_currentGravity_36() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___currentGravity_36)); }
	inline float get_currentGravity_36() const { return ___currentGravity_36; }
	inline float* get_address_of_currentGravity_36() { return &___currentGravity_36; }
	inline void set_currentGravity_36(float value)
	{
		___currentGravity_36 = value;
	}

	inline static int32_t get_offset_of_isJump_37() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___isJump_37)); }
	inline bool get_isJump_37() const { return ___isJump_37; }
	inline bool* get_address_of_isJump_37() { return &___isJump_37; }
	inline void set_isJump_37(bool value)
	{
		___isJump_37 = value;
	}

	inline static int32_t get_offset_of_unityAxis_38() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___unityAxis_38)); }
	inline String_t* get_unityAxis_38() const { return ___unityAxis_38; }
	inline String_t** get_address_of_unityAxis_38() { return &___unityAxis_38; }
	inline void set_unityAxis_38(String_t* value)
	{
		___unityAxis_38 = value;
		Il2CppCodeGenWriteBarrier((&___unityAxis_38), value);
	}

	inline static int32_t get_offset_of_showGeneralInspector_39() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___showGeneralInspector_39)); }
	inline bool get_showGeneralInspector_39() const { return ___showGeneralInspector_39; }
	inline bool* get_address_of_showGeneralInspector_39() { return &___showGeneralInspector_39; }
	inline void set_showGeneralInspector_39(bool value)
	{
		___showGeneralInspector_39 = value;
	}

	inline static int32_t get_offset_of_showDirectInspector_40() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___showDirectInspector_40)); }
	inline bool get_showDirectInspector_40() const { return ___showDirectInspector_40; }
	inline bool* get_address_of_showDirectInspector_40() { return &___showDirectInspector_40; }
	inline void set_showDirectInspector_40(bool value)
	{
		___showDirectInspector_40 = value;
	}

	inline static int32_t get_offset_of_showInertiaInspector_41() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___showInertiaInspector_41)); }
	inline bool get_showInertiaInspector_41() const { return ___showInertiaInspector_41; }
	inline bool* get_address_of_showInertiaInspector_41() { return &___showInertiaInspector_41; }
	inline void set_showInertiaInspector_41(bool value)
	{
		___showInertiaInspector_41 = value;
	}

	inline static int32_t get_offset_of_showSimulatinInspector_42() { return static_cast<int32_t>(offsetof(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9, ___showSimulatinInspector_42)); }
	inline bool get_showSimulatinInspector_42() const { return ___showSimulatinInspector_42; }
	inline bool* get_address_of_showSimulatinInspector_42() { return &___showSimulatinInspector_42; }
	inline void set_showSimulatinInspector_42(bool value)
	{
		___showSimulatinInspector_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCAXIS_T950EE7D6F878394ACEF29C1F6CF346541A1230E9_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AXISXUI_TCF6461EE4911A3FF6095AD59A726B38F362A681E_H
#define AXISXUI_TCF6461EE4911A3FF6095AD59A726B38F362A681E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AxisXUi
struct  AxisXUi_tCF6461EE4911A3FF6095AD59A726B38F362A681E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISXUI_TCF6461EE4911A3FF6095AD59A726B38F362A681E_H
#ifndef BUTTONINPUTUI_T4297693945A2DA852F81C92011A08E398C3C8796_H
#define BUTTONINPUTUI_T4297693945A2DA852F81C92011A08E398C3C8796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonInputUI
struct  ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ButtonInputUI::getButtonDownText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getButtonDownText_4;
	// UnityEngine.UI.Text ButtonInputUI::getButtonText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getButtonText_5;
	// UnityEngine.UI.Text ButtonInputUI::getButtonTimeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getButtonTimeText_6;
	// UnityEngine.UI.Text ButtonInputUI::getButtonUpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getButtonUpText_7;

public:
	inline static int32_t get_offset_of_getButtonDownText_4() { return static_cast<int32_t>(offsetof(ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796, ___getButtonDownText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getButtonDownText_4() const { return ___getButtonDownText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getButtonDownText_4() { return &___getButtonDownText_4; }
	inline void set_getButtonDownText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getButtonDownText_4 = value;
		Il2CppCodeGenWriteBarrier((&___getButtonDownText_4), value);
	}

	inline static int32_t get_offset_of_getButtonText_5() { return static_cast<int32_t>(offsetof(ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796, ___getButtonText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getButtonText_5() const { return ___getButtonText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getButtonText_5() { return &___getButtonText_5; }
	inline void set_getButtonText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getButtonText_5 = value;
		Il2CppCodeGenWriteBarrier((&___getButtonText_5), value);
	}

	inline static int32_t get_offset_of_getButtonTimeText_6() { return static_cast<int32_t>(offsetof(ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796, ___getButtonTimeText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getButtonTimeText_6() const { return ___getButtonTimeText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getButtonTimeText_6() { return &___getButtonTimeText_6; }
	inline void set_getButtonTimeText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getButtonTimeText_6 = value;
		Il2CppCodeGenWriteBarrier((&___getButtonTimeText_6), value);
	}

	inline static int32_t get_offset_of_getButtonUpText_7() { return static_cast<int32_t>(offsetof(ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796, ___getButtonUpText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getButtonUpText_7() const { return ___getButtonUpText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getButtonUpText_7() { return &___getButtonUpText_7; }
	inline void set_getButtonUpText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getButtonUpText_7 = value;
		Il2CppCodeGenWriteBarrier((&___getButtonUpText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINPUTUI_T4297693945A2DA852F81C92011A08E398C3C8796_H
#ifndef BUTTONUIEVENT_T5DB4F86C8A6911F40300C554D3BAD5E8793987EF_H
#define BUTTONUIEVENT_T5DB4F86C8A6911F40300C554D3BAD5E8793987EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonUIEvent
struct  ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ButtonUIEvent::downText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downText_4;
	// UnityEngine.UI.Text ButtonUIEvent::pressText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___pressText_5;
	// UnityEngine.UI.Text ButtonUIEvent::pressValueText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___pressValueText_6;
	// UnityEngine.UI.Text ButtonUIEvent::upText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___upText_7;

public:
	inline static int32_t get_offset_of_downText_4() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF, ___downText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downText_4() const { return ___downText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downText_4() { return &___downText_4; }
	inline void set_downText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downText_4 = value;
		Il2CppCodeGenWriteBarrier((&___downText_4), value);
	}

	inline static int32_t get_offset_of_pressText_5() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF, ___pressText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_pressText_5() const { return ___pressText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_pressText_5() { return &___pressText_5; }
	inline void set_pressText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___pressText_5 = value;
		Il2CppCodeGenWriteBarrier((&___pressText_5), value);
	}

	inline static int32_t get_offset_of_pressValueText_6() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF, ___pressValueText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_pressValueText_6() const { return ___pressValueText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_pressValueText_6() { return &___pressValueText_6; }
	inline void set_pressValueText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___pressValueText_6 = value;
		Il2CppCodeGenWriteBarrier((&___pressValueText_6), value);
	}

	inline static int32_t get_offset_of_upText_7() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF, ___upText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_upText_7() const { return ___upText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_upText_7() { return &___upText_7; }
	inline void set_upText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___upText_7 = value;
		Il2CppCodeGenWriteBarrier((&___upText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONUIEVENT_T5DB4F86C8A6911F40300C554D3BAD5E8793987EF_H
#ifndef CHARACTERANIMATION_T492D59F02F07753618C579A8DA6A5D65EB8D5F95_H
#define CHARACTERANIMATION_T492D59F02F07753618C579A8DA6A5D65EB8D5F95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAnimation
struct  CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CharacterController CharacterAnimation::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_4;
	// UnityEngine.Animation CharacterAnimation::anim
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ___anim_5;

public:
	inline static int32_t get_offset_of_cc_4() { return static_cast<int32_t>(offsetof(CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95, ___cc_4)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_4() const { return ___cc_4; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_4() { return &___cc_4; }
	inline void set_cc_4(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_4 = value;
		Il2CppCodeGenWriteBarrier((&___cc_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95, ___anim_5)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get_anim_5() const { return ___anim_5; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERANIMATION_T492D59F02F07753618C579A8DA6A5D65EB8D5F95_H
#ifndef CHARACTERANIMATIONDUNGEON_T7CE92C537930F9782D0A91E4BA001EAF2ABB65E5_H
#define CHARACTERANIMATIONDUNGEON_T7CE92C537930F9782D0A91E4BA001EAF2ABB65E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAnimationDungeon
struct  CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CharacterController CharacterAnimationDungeon::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_4;
	// UnityEngine.Animation CharacterAnimationDungeon::anim
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ___anim_5;

public:
	inline static int32_t get_offset_of_cc_4() { return static_cast<int32_t>(offsetof(CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5, ___cc_4)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_4() const { return ___cc_4; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_4() { return &___cc_4; }
	inline void set_cc_4(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_4 = value;
		Il2CppCodeGenWriteBarrier((&___cc_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5, ___anim_5)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get_anim_5() const { return ___anim_5; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERANIMATIONDUNGEON_T7CE92C537930F9782D0A91E4BA001EAF2ABB65E5_H
#ifndef CONTROLUIEVENT_TF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C_H
#define CONTROLUIEVENT_TF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIEvent
struct  ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ControlUIEvent::moveStartText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___moveStartText_4;
	// UnityEngine.UI.Text ControlUIEvent::moveText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___moveText_5;
	// UnityEngine.UI.Text ControlUIEvent::moveSpeedText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___moveSpeedText_6;
	// UnityEngine.UI.Text ControlUIEvent::moveEndText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___moveEndText_7;
	// UnityEngine.UI.Text ControlUIEvent::touchStartText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___touchStartText_8;
	// UnityEngine.UI.Text ControlUIEvent::touchUpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___touchUpText_9;
	// UnityEngine.UI.Text ControlUIEvent::downRightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downRightText_10;
	// UnityEngine.UI.Text ControlUIEvent::downDownText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downDownText_11;
	// UnityEngine.UI.Text ControlUIEvent::downLeftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downLeftText_12;
	// UnityEngine.UI.Text ControlUIEvent::downUpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downUpText_13;
	// UnityEngine.UI.Text ControlUIEvent::rightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___rightText_14;
	// UnityEngine.UI.Text ControlUIEvent::downText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downText_15;
	// UnityEngine.UI.Text ControlUIEvent::leftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___leftText_16;
	// UnityEngine.UI.Text ControlUIEvent::upText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___upText_17;
	// System.Boolean ControlUIEvent::isDown
	bool ___isDown_18;
	// System.Boolean ControlUIEvent::isLeft
	bool ___isLeft_19;
	// System.Boolean ControlUIEvent::isUp
	bool ___isUp_20;
	// System.Boolean ControlUIEvent::isRight
	bool ___isRight_21;

public:
	inline static int32_t get_offset_of_moveStartText_4() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___moveStartText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_moveStartText_4() const { return ___moveStartText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_moveStartText_4() { return &___moveStartText_4; }
	inline void set_moveStartText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___moveStartText_4 = value;
		Il2CppCodeGenWriteBarrier((&___moveStartText_4), value);
	}

	inline static int32_t get_offset_of_moveText_5() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___moveText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_moveText_5() const { return ___moveText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_moveText_5() { return &___moveText_5; }
	inline void set_moveText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___moveText_5 = value;
		Il2CppCodeGenWriteBarrier((&___moveText_5), value);
	}

	inline static int32_t get_offset_of_moveSpeedText_6() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___moveSpeedText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_moveSpeedText_6() const { return ___moveSpeedText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_moveSpeedText_6() { return &___moveSpeedText_6; }
	inline void set_moveSpeedText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___moveSpeedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___moveSpeedText_6), value);
	}

	inline static int32_t get_offset_of_moveEndText_7() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___moveEndText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_moveEndText_7() const { return ___moveEndText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_moveEndText_7() { return &___moveEndText_7; }
	inline void set_moveEndText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___moveEndText_7 = value;
		Il2CppCodeGenWriteBarrier((&___moveEndText_7), value);
	}

	inline static int32_t get_offset_of_touchStartText_8() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___touchStartText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_touchStartText_8() const { return ___touchStartText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_touchStartText_8() { return &___touchStartText_8; }
	inline void set_touchStartText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___touchStartText_8 = value;
		Il2CppCodeGenWriteBarrier((&___touchStartText_8), value);
	}

	inline static int32_t get_offset_of_touchUpText_9() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___touchUpText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_touchUpText_9() const { return ___touchUpText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_touchUpText_9() { return &___touchUpText_9; }
	inline void set_touchUpText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___touchUpText_9 = value;
		Il2CppCodeGenWriteBarrier((&___touchUpText_9), value);
	}

	inline static int32_t get_offset_of_downRightText_10() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___downRightText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downRightText_10() const { return ___downRightText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downRightText_10() { return &___downRightText_10; }
	inline void set_downRightText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downRightText_10 = value;
		Il2CppCodeGenWriteBarrier((&___downRightText_10), value);
	}

	inline static int32_t get_offset_of_downDownText_11() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___downDownText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downDownText_11() const { return ___downDownText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downDownText_11() { return &___downDownText_11; }
	inline void set_downDownText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downDownText_11 = value;
		Il2CppCodeGenWriteBarrier((&___downDownText_11), value);
	}

	inline static int32_t get_offset_of_downLeftText_12() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___downLeftText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downLeftText_12() const { return ___downLeftText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downLeftText_12() { return &___downLeftText_12; }
	inline void set_downLeftText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downLeftText_12 = value;
		Il2CppCodeGenWriteBarrier((&___downLeftText_12), value);
	}

	inline static int32_t get_offset_of_downUpText_13() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___downUpText_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downUpText_13() const { return ___downUpText_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downUpText_13() { return &___downUpText_13; }
	inline void set_downUpText_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downUpText_13 = value;
		Il2CppCodeGenWriteBarrier((&___downUpText_13), value);
	}

	inline static int32_t get_offset_of_rightText_14() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___rightText_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_rightText_14() const { return ___rightText_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_rightText_14() { return &___rightText_14; }
	inline void set_rightText_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___rightText_14 = value;
		Il2CppCodeGenWriteBarrier((&___rightText_14), value);
	}

	inline static int32_t get_offset_of_downText_15() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___downText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downText_15() const { return ___downText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downText_15() { return &___downText_15; }
	inline void set_downText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downText_15 = value;
		Il2CppCodeGenWriteBarrier((&___downText_15), value);
	}

	inline static int32_t get_offset_of_leftText_16() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___leftText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_leftText_16() const { return ___leftText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_leftText_16() { return &___leftText_16; }
	inline void set_leftText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___leftText_16 = value;
		Il2CppCodeGenWriteBarrier((&___leftText_16), value);
	}

	inline static int32_t get_offset_of_upText_17() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___upText_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_upText_17() const { return ___upText_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_upText_17() { return &___upText_17; }
	inline void set_upText_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___upText_17 = value;
		Il2CppCodeGenWriteBarrier((&___upText_17), value);
	}

	inline static int32_t get_offset_of_isDown_18() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___isDown_18)); }
	inline bool get_isDown_18() const { return ___isDown_18; }
	inline bool* get_address_of_isDown_18() { return &___isDown_18; }
	inline void set_isDown_18(bool value)
	{
		___isDown_18 = value;
	}

	inline static int32_t get_offset_of_isLeft_19() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___isLeft_19)); }
	inline bool get_isLeft_19() const { return ___isLeft_19; }
	inline bool* get_address_of_isLeft_19() { return &___isLeft_19; }
	inline void set_isLeft_19(bool value)
	{
		___isLeft_19 = value;
	}

	inline static int32_t get_offset_of_isUp_20() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___isUp_20)); }
	inline bool get_isUp_20() const { return ___isUp_20; }
	inline bool* get_address_of_isUp_20() { return &___isUp_20; }
	inline void set_isUp_20(bool value)
	{
		___isUp_20 = value;
	}

	inline static int32_t get_offset_of_isRight_21() { return static_cast<int32_t>(offsetof(ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C, ___isRight_21)); }
	inline bool get_isRight_21() const { return ___isRight_21; }
	inline bool* get_address_of_isRight_21() { return &___isRight_21; }
	inline void set_isRight_21(bool value)
	{
		___isRight_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLUIEVENT_TF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C_H
#ifndef CONTROLUIINPUT_T319CEA9A7317BB312F913159C0F0243649B53627_H
#define CONTROLUIINPUT_T319CEA9A7317BB312F913159C0F0243649B53627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIInput
struct  ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ControlUIInput::getAxisText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getAxisText_4;
	// UnityEngine.UI.Text ControlUIInput::getAxisSpeedText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getAxisSpeedText_5;
	// UnityEngine.UI.Text ControlUIInput::getAxisYText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getAxisYText_6;
	// UnityEngine.UI.Text ControlUIInput::getAxisYSpeedText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___getAxisYSpeedText_7;
	// UnityEngine.UI.Text ControlUIInput::downRightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downRightText_8;
	// UnityEngine.UI.Text ControlUIInput::downDownText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downDownText_9;
	// UnityEngine.UI.Text ControlUIInput::downLeftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downLeftText_10;
	// UnityEngine.UI.Text ControlUIInput::downUpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downUpText_11;
	// UnityEngine.UI.Text ControlUIInput::rightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___rightText_12;
	// UnityEngine.UI.Text ControlUIInput::downText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___downText_13;
	// UnityEngine.UI.Text ControlUIInput::leftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___leftText_14;
	// UnityEngine.UI.Text ControlUIInput::upText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___upText_15;

public:
	inline static int32_t get_offset_of_getAxisText_4() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___getAxisText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getAxisText_4() const { return ___getAxisText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getAxisText_4() { return &___getAxisText_4; }
	inline void set_getAxisText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getAxisText_4 = value;
		Il2CppCodeGenWriteBarrier((&___getAxisText_4), value);
	}

	inline static int32_t get_offset_of_getAxisSpeedText_5() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___getAxisSpeedText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getAxisSpeedText_5() const { return ___getAxisSpeedText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getAxisSpeedText_5() { return &___getAxisSpeedText_5; }
	inline void set_getAxisSpeedText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getAxisSpeedText_5 = value;
		Il2CppCodeGenWriteBarrier((&___getAxisSpeedText_5), value);
	}

	inline static int32_t get_offset_of_getAxisYText_6() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___getAxisYText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getAxisYText_6() const { return ___getAxisYText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getAxisYText_6() { return &___getAxisYText_6; }
	inline void set_getAxisYText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getAxisYText_6 = value;
		Il2CppCodeGenWriteBarrier((&___getAxisYText_6), value);
	}

	inline static int32_t get_offset_of_getAxisYSpeedText_7() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___getAxisYSpeedText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_getAxisYSpeedText_7() const { return ___getAxisYSpeedText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_getAxisYSpeedText_7() { return &___getAxisYSpeedText_7; }
	inline void set_getAxisYSpeedText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___getAxisYSpeedText_7 = value;
		Il2CppCodeGenWriteBarrier((&___getAxisYSpeedText_7), value);
	}

	inline static int32_t get_offset_of_downRightText_8() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___downRightText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downRightText_8() const { return ___downRightText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downRightText_8() { return &___downRightText_8; }
	inline void set_downRightText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downRightText_8 = value;
		Il2CppCodeGenWriteBarrier((&___downRightText_8), value);
	}

	inline static int32_t get_offset_of_downDownText_9() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___downDownText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downDownText_9() const { return ___downDownText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downDownText_9() { return &___downDownText_9; }
	inline void set_downDownText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downDownText_9 = value;
		Il2CppCodeGenWriteBarrier((&___downDownText_9), value);
	}

	inline static int32_t get_offset_of_downLeftText_10() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___downLeftText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downLeftText_10() const { return ___downLeftText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downLeftText_10() { return &___downLeftText_10; }
	inline void set_downLeftText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downLeftText_10 = value;
		Il2CppCodeGenWriteBarrier((&___downLeftText_10), value);
	}

	inline static int32_t get_offset_of_downUpText_11() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___downUpText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downUpText_11() const { return ___downUpText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downUpText_11() { return &___downUpText_11; }
	inline void set_downUpText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downUpText_11 = value;
		Il2CppCodeGenWriteBarrier((&___downUpText_11), value);
	}

	inline static int32_t get_offset_of_rightText_12() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___rightText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_rightText_12() const { return ___rightText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_rightText_12() { return &___rightText_12; }
	inline void set_rightText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___rightText_12 = value;
		Il2CppCodeGenWriteBarrier((&___rightText_12), value);
	}

	inline static int32_t get_offset_of_downText_13() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___downText_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_downText_13() const { return ___downText_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_downText_13() { return &___downText_13; }
	inline void set_downText_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___downText_13 = value;
		Il2CppCodeGenWriteBarrier((&___downText_13), value);
	}

	inline static int32_t get_offset_of_leftText_14() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___leftText_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_leftText_14() const { return ___leftText_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_leftText_14() { return &___leftText_14; }
	inline void set_leftText_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___leftText_14 = value;
		Il2CppCodeGenWriteBarrier((&___leftText_14), value);
	}

	inline static int32_t get_offset_of_upText_15() { return static_cast<int32_t>(offsetof(ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627, ___upText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_upText_15() const { return ___upText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_upText_15() { return &___upText_15; }
	inline void set_upText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___upText_15 = value;
		Il2CppCodeGenWriteBarrier((&___upText_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLUIINPUT_T319CEA9A7317BB312F913159C0F0243649B53627_H
#ifndef DPADPARAMETERUI_TF90103620860837B0B7E919E9919C8E893FBCE8A_H
#define DPADPARAMETERUI_TF90103620860837B0B7E919E9919C8E893FBCE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DPadParameterUI
struct  DPadParameterUI_tF90103620860837B0B7E919E9919C8E893FBCE8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPADPARAMETERUI_TF90103620860837B0B7E919E9919C8E893FBCE8A_H
#ifndef ETCAREA_TAEB2B810ED837AB23170D5BB0B2E0E425E2654B7_H
#define ETCAREA_TAEB2B810ED837AB23170D5BB0B2E0E425E2654B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCArea
struct  ETCArea_tAEB2B810ED837AB23170D5BB0B2E0E425E2654B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean ETCArea::show
	bool ___show_4;

public:
	inline static int32_t get_offset_of_show_4() { return static_cast<int32_t>(offsetof(ETCArea_tAEB2B810ED837AB23170D5BB0B2E0E425E2654B7, ___show_4)); }
	inline bool get_show_4() const { return ___show_4; }
	inline bool* get_address_of_show_4() { return &___show_4; }
	inline void set_show_4(bool value)
	{
		___show_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCAREA_TAEB2B810ED837AB23170D5BB0B2E0E425E2654B7_H
#ifndef ETCBASE_T6A678D4A7F7A841B76025801594E4EAFFFB1B093_H
#define ETCBASE_T6A678D4A7F7A841B76025801594E4EAFFFB1B093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase
struct  ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform ETCBase::cachedRectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___cachedRectTransform_4;
	// UnityEngine.Canvas ETCBase::cachedRootCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___cachedRootCanvas_5;
	// System.Boolean ETCBase::isUnregisterAtDisable
	bool ___isUnregisterAtDisable_6;
	// System.Boolean ETCBase::visibleAtStart
	bool ___visibleAtStart_7;
	// System.Boolean ETCBase::activatedAtStart
	bool ___activatedAtStart_8;
	// ETCBase_RectAnchor ETCBase::_anchor
	int32_t ____anchor_9;
	// UnityEngine.Vector2 ETCBase::_anchorOffet
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____anchorOffet_10;
	// System.Boolean ETCBase::_visible
	bool ____visible_11;
	// System.Boolean ETCBase::_activated
	bool ____activated_12;
	// System.Boolean ETCBase::enableCamera
	bool ___enableCamera_13;
	// ETCBase_CameraMode ETCBase::cameraMode
	int32_t ___cameraMode_14;
	// System.String ETCBase::camTargetTag
	String_t* ___camTargetTag_15;
	// System.Boolean ETCBase::autoLinkTagCam
	bool ___autoLinkTagCam_16;
	// System.String ETCBase::autoCamTag
	String_t* ___autoCamTag_17;
	// UnityEngine.Transform ETCBase::cameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform_18;
	// ETCBase_CameraTargetMode ETCBase::cameraTargetMode
	int32_t ___cameraTargetMode_19;
	// System.Boolean ETCBase::enableWallDetection
	bool ___enableWallDetection_20;
	// UnityEngine.LayerMask ETCBase::wallLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___wallLayer_21;
	// UnityEngine.Transform ETCBase::cameraLookAt
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraLookAt_22;
	// UnityEngine.CharacterController ETCBase::cameraLookAtCC
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cameraLookAtCC_23;
	// UnityEngine.Vector3 ETCBase::followOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___followOffset_24;
	// System.Single ETCBase::followDistance
	float ___followDistance_25;
	// System.Single ETCBase::followHeight
	float ___followHeight_26;
	// System.Single ETCBase::followRotationDamping
	float ___followRotationDamping_27;
	// System.Single ETCBase::followHeightDamping
	float ___followHeightDamping_28;
	// System.Int32 ETCBase::pointId
	int32_t ___pointId_29;
	// System.Boolean ETCBase::enableKeySimulation
	bool ___enableKeySimulation_30;
	// System.Boolean ETCBase::allowSimulationStandalone
	bool ___allowSimulationStandalone_31;
	// System.Boolean ETCBase::visibleOnStandalone
	bool ___visibleOnStandalone_32;
	// ETCBase_DPadAxis ETCBase::dPadAxisCount
	int32_t ___dPadAxisCount_33;
	// System.Boolean ETCBase::useFixedUpdate
	bool ___useFixedUpdate_34;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> ETCBase::uiRaycastResultCache
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___uiRaycastResultCache_35;
	// UnityEngine.EventSystems.PointerEventData ETCBase::uiPointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___uiPointerEventData_36;
	// UnityEngine.EventSystems.EventSystem ETCBase::uiEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___uiEventSystem_37;
	// System.Boolean ETCBase::isOnDrag
	bool ___isOnDrag_38;
	// System.Boolean ETCBase::isSwipeIn
	bool ___isSwipeIn_39;
	// System.Boolean ETCBase::isSwipeOut
	bool ___isSwipeOut_40;
	// System.Boolean ETCBase::showPSInspector
	bool ___showPSInspector_41;
	// System.Boolean ETCBase::showSpriteInspector
	bool ___showSpriteInspector_42;
	// System.Boolean ETCBase::showEventInspector
	bool ___showEventInspector_43;
	// System.Boolean ETCBase::showBehaviourInspector
	bool ___showBehaviourInspector_44;
	// System.Boolean ETCBase::showAxesInspector
	bool ___showAxesInspector_45;
	// System.Boolean ETCBase::showTouchEventInspector
	bool ___showTouchEventInspector_46;
	// System.Boolean ETCBase::showDownEventInspector
	bool ___showDownEventInspector_47;
	// System.Boolean ETCBase::showPressEventInspector
	bool ___showPressEventInspector_48;
	// System.Boolean ETCBase::showCameraInspector
	bool ___showCameraInspector_49;

public:
	inline static int32_t get_offset_of_cachedRectTransform_4() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cachedRectTransform_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_cachedRectTransform_4() const { return ___cachedRectTransform_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_cachedRectTransform_4() { return &___cachedRectTransform_4; }
	inline void set_cachedRectTransform_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___cachedRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_4), value);
	}

	inline static int32_t get_offset_of_cachedRootCanvas_5() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cachedRootCanvas_5)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_cachedRootCanvas_5() const { return ___cachedRootCanvas_5; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_cachedRootCanvas_5() { return &___cachedRootCanvas_5; }
	inline void set_cachedRootCanvas_5(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___cachedRootCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRootCanvas_5), value);
	}

	inline static int32_t get_offset_of_isUnregisterAtDisable_6() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___isUnregisterAtDisable_6)); }
	inline bool get_isUnregisterAtDisable_6() const { return ___isUnregisterAtDisable_6; }
	inline bool* get_address_of_isUnregisterAtDisable_6() { return &___isUnregisterAtDisable_6; }
	inline void set_isUnregisterAtDisable_6(bool value)
	{
		___isUnregisterAtDisable_6 = value;
	}

	inline static int32_t get_offset_of_visibleAtStart_7() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___visibleAtStart_7)); }
	inline bool get_visibleAtStart_7() const { return ___visibleAtStart_7; }
	inline bool* get_address_of_visibleAtStart_7() { return &___visibleAtStart_7; }
	inline void set_visibleAtStart_7(bool value)
	{
		___visibleAtStart_7 = value;
	}

	inline static int32_t get_offset_of_activatedAtStart_8() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___activatedAtStart_8)); }
	inline bool get_activatedAtStart_8() const { return ___activatedAtStart_8; }
	inline bool* get_address_of_activatedAtStart_8() { return &___activatedAtStart_8; }
	inline void set_activatedAtStart_8(bool value)
	{
		___activatedAtStart_8 = value;
	}

	inline static int32_t get_offset_of__anchor_9() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ____anchor_9)); }
	inline int32_t get__anchor_9() const { return ____anchor_9; }
	inline int32_t* get_address_of__anchor_9() { return &____anchor_9; }
	inline void set__anchor_9(int32_t value)
	{
		____anchor_9 = value;
	}

	inline static int32_t get_offset_of__anchorOffet_10() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ____anchorOffet_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__anchorOffet_10() const { return ____anchorOffet_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__anchorOffet_10() { return &____anchorOffet_10; }
	inline void set__anchorOffet_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____anchorOffet_10 = value;
	}

	inline static int32_t get_offset_of__visible_11() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ____visible_11)); }
	inline bool get__visible_11() const { return ____visible_11; }
	inline bool* get_address_of__visible_11() { return &____visible_11; }
	inline void set__visible_11(bool value)
	{
		____visible_11 = value;
	}

	inline static int32_t get_offset_of__activated_12() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ____activated_12)); }
	inline bool get__activated_12() const { return ____activated_12; }
	inline bool* get_address_of__activated_12() { return &____activated_12; }
	inline void set__activated_12(bool value)
	{
		____activated_12 = value;
	}

	inline static int32_t get_offset_of_enableCamera_13() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___enableCamera_13)); }
	inline bool get_enableCamera_13() const { return ___enableCamera_13; }
	inline bool* get_address_of_enableCamera_13() { return &___enableCamera_13; }
	inline void set_enableCamera_13(bool value)
	{
		___enableCamera_13 = value;
	}

	inline static int32_t get_offset_of_cameraMode_14() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cameraMode_14)); }
	inline int32_t get_cameraMode_14() const { return ___cameraMode_14; }
	inline int32_t* get_address_of_cameraMode_14() { return &___cameraMode_14; }
	inline void set_cameraMode_14(int32_t value)
	{
		___cameraMode_14 = value;
	}

	inline static int32_t get_offset_of_camTargetTag_15() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___camTargetTag_15)); }
	inline String_t* get_camTargetTag_15() const { return ___camTargetTag_15; }
	inline String_t** get_address_of_camTargetTag_15() { return &___camTargetTag_15; }
	inline void set_camTargetTag_15(String_t* value)
	{
		___camTargetTag_15 = value;
		Il2CppCodeGenWriteBarrier((&___camTargetTag_15), value);
	}

	inline static int32_t get_offset_of_autoLinkTagCam_16() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___autoLinkTagCam_16)); }
	inline bool get_autoLinkTagCam_16() const { return ___autoLinkTagCam_16; }
	inline bool* get_address_of_autoLinkTagCam_16() { return &___autoLinkTagCam_16; }
	inline void set_autoLinkTagCam_16(bool value)
	{
		___autoLinkTagCam_16 = value;
	}

	inline static int32_t get_offset_of_autoCamTag_17() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___autoCamTag_17)); }
	inline String_t* get_autoCamTag_17() const { return ___autoCamTag_17; }
	inline String_t** get_address_of_autoCamTag_17() { return &___autoCamTag_17; }
	inline void set_autoCamTag_17(String_t* value)
	{
		___autoCamTag_17 = value;
		Il2CppCodeGenWriteBarrier((&___autoCamTag_17), value);
	}

	inline static int32_t get_offset_of_cameraTransform_18() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cameraTransform_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTransform_18() const { return ___cameraTransform_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTransform_18() { return &___cameraTransform_18; }
	inline void set_cameraTransform_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTransform_18 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_18), value);
	}

	inline static int32_t get_offset_of_cameraTargetMode_19() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cameraTargetMode_19)); }
	inline int32_t get_cameraTargetMode_19() const { return ___cameraTargetMode_19; }
	inline int32_t* get_address_of_cameraTargetMode_19() { return &___cameraTargetMode_19; }
	inline void set_cameraTargetMode_19(int32_t value)
	{
		___cameraTargetMode_19 = value;
	}

	inline static int32_t get_offset_of_enableWallDetection_20() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___enableWallDetection_20)); }
	inline bool get_enableWallDetection_20() const { return ___enableWallDetection_20; }
	inline bool* get_address_of_enableWallDetection_20() { return &___enableWallDetection_20; }
	inline void set_enableWallDetection_20(bool value)
	{
		___enableWallDetection_20 = value;
	}

	inline static int32_t get_offset_of_wallLayer_21() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___wallLayer_21)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_wallLayer_21() const { return ___wallLayer_21; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_wallLayer_21() { return &___wallLayer_21; }
	inline void set_wallLayer_21(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___wallLayer_21 = value;
	}

	inline static int32_t get_offset_of_cameraLookAt_22() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cameraLookAt_22)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraLookAt_22() const { return ___cameraLookAt_22; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraLookAt_22() { return &___cameraLookAt_22; }
	inline void set_cameraLookAt_22(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraLookAt_22 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAt_22), value);
	}

	inline static int32_t get_offset_of_cameraLookAtCC_23() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___cameraLookAtCC_23)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cameraLookAtCC_23() const { return ___cameraLookAtCC_23; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cameraLookAtCC_23() { return &___cameraLookAtCC_23; }
	inline void set_cameraLookAtCC_23(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cameraLookAtCC_23 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAtCC_23), value);
	}

	inline static int32_t get_offset_of_followOffset_24() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___followOffset_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_followOffset_24() const { return ___followOffset_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_followOffset_24() { return &___followOffset_24; }
	inline void set_followOffset_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___followOffset_24 = value;
	}

	inline static int32_t get_offset_of_followDistance_25() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___followDistance_25)); }
	inline float get_followDistance_25() const { return ___followDistance_25; }
	inline float* get_address_of_followDistance_25() { return &___followDistance_25; }
	inline void set_followDistance_25(float value)
	{
		___followDistance_25 = value;
	}

	inline static int32_t get_offset_of_followHeight_26() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___followHeight_26)); }
	inline float get_followHeight_26() const { return ___followHeight_26; }
	inline float* get_address_of_followHeight_26() { return &___followHeight_26; }
	inline void set_followHeight_26(float value)
	{
		___followHeight_26 = value;
	}

	inline static int32_t get_offset_of_followRotationDamping_27() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___followRotationDamping_27)); }
	inline float get_followRotationDamping_27() const { return ___followRotationDamping_27; }
	inline float* get_address_of_followRotationDamping_27() { return &___followRotationDamping_27; }
	inline void set_followRotationDamping_27(float value)
	{
		___followRotationDamping_27 = value;
	}

	inline static int32_t get_offset_of_followHeightDamping_28() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___followHeightDamping_28)); }
	inline float get_followHeightDamping_28() const { return ___followHeightDamping_28; }
	inline float* get_address_of_followHeightDamping_28() { return &___followHeightDamping_28; }
	inline void set_followHeightDamping_28(float value)
	{
		___followHeightDamping_28 = value;
	}

	inline static int32_t get_offset_of_pointId_29() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___pointId_29)); }
	inline int32_t get_pointId_29() const { return ___pointId_29; }
	inline int32_t* get_address_of_pointId_29() { return &___pointId_29; }
	inline void set_pointId_29(int32_t value)
	{
		___pointId_29 = value;
	}

	inline static int32_t get_offset_of_enableKeySimulation_30() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___enableKeySimulation_30)); }
	inline bool get_enableKeySimulation_30() const { return ___enableKeySimulation_30; }
	inline bool* get_address_of_enableKeySimulation_30() { return &___enableKeySimulation_30; }
	inline void set_enableKeySimulation_30(bool value)
	{
		___enableKeySimulation_30 = value;
	}

	inline static int32_t get_offset_of_allowSimulationStandalone_31() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___allowSimulationStandalone_31)); }
	inline bool get_allowSimulationStandalone_31() const { return ___allowSimulationStandalone_31; }
	inline bool* get_address_of_allowSimulationStandalone_31() { return &___allowSimulationStandalone_31; }
	inline void set_allowSimulationStandalone_31(bool value)
	{
		___allowSimulationStandalone_31 = value;
	}

	inline static int32_t get_offset_of_visibleOnStandalone_32() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___visibleOnStandalone_32)); }
	inline bool get_visibleOnStandalone_32() const { return ___visibleOnStandalone_32; }
	inline bool* get_address_of_visibleOnStandalone_32() { return &___visibleOnStandalone_32; }
	inline void set_visibleOnStandalone_32(bool value)
	{
		___visibleOnStandalone_32 = value;
	}

	inline static int32_t get_offset_of_dPadAxisCount_33() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___dPadAxisCount_33)); }
	inline int32_t get_dPadAxisCount_33() const { return ___dPadAxisCount_33; }
	inline int32_t* get_address_of_dPadAxisCount_33() { return &___dPadAxisCount_33; }
	inline void set_dPadAxisCount_33(int32_t value)
	{
		___dPadAxisCount_33 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_34() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___useFixedUpdate_34)); }
	inline bool get_useFixedUpdate_34() const { return ___useFixedUpdate_34; }
	inline bool* get_address_of_useFixedUpdate_34() { return &___useFixedUpdate_34; }
	inline void set_useFixedUpdate_34(bool value)
	{
		___useFixedUpdate_34 = value;
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_35() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___uiRaycastResultCache_35)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_uiRaycastResultCache_35() const { return ___uiRaycastResultCache_35; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_uiRaycastResultCache_35() { return &___uiRaycastResultCache_35; }
	inline void set_uiRaycastResultCache_35(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___uiRaycastResultCache_35 = value;
		Il2CppCodeGenWriteBarrier((&___uiRaycastResultCache_35), value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_36() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___uiPointerEventData_36)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_uiPointerEventData_36() const { return ___uiPointerEventData_36; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_uiPointerEventData_36() { return &___uiPointerEventData_36; }
	inline void set_uiPointerEventData_36(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___uiPointerEventData_36 = value;
		Il2CppCodeGenWriteBarrier((&___uiPointerEventData_36), value);
	}

	inline static int32_t get_offset_of_uiEventSystem_37() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___uiEventSystem_37)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_uiEventSystem_37() const { return ___uiEventSystem_37; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_uiEventSystem_37() { return &___uiEventSystem_37; }
	inline void set_uiEventSystem_37(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___uiEventSystem_37 = value;
		Il2CppCodeGenWriteBarrier((&___uiEventSystem_37), value);
	}

	inline static int32_t get_offset_of_isOnDrag_38() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___isOnDrag_38)); }
	inline bool get_isOnDrag_38() const { return ___isOnDrag_38; }
	inline bool* get_address_of_isOnDrag_38() { return &___isOnDrag_38; }
	inline void set_isOnDrag_38(bool value)
	{
		___isOnDrag_38 = value;
	}

	inline static int32_t get_offset_of_isSwipeIn_39() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___isSwipeIn_39)); }
	inline bool get_isSwipeIn_39() const { return ___isSwipeIn_39; }
	inline bool* get_address_of_isSwipeIn_39() { return &___isSwipeIn_39; }
	inline void set_isSwipeIn_39(bool value)
	{
		___isSwipeIn_39 = value;
	}

	inline static int32_t get_offset_of_isSwipeOut_40() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___isSwipeOut_40)); }
	inline bool get_isSwipeOut_40() const { return ___isSwipeOut_40; }
	inline bool* get_address_of_isSwipeOut_40() { return &___isSwipeOut_40; }
	inline void set_isSwipeOut_40(bool value)
	{
		___isSwipeOut_40 = value;
	}

	inline static int32_t get_offset_of_showPSInspector_41() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showPSInspector_41)); }
	inline bool get_showPSInspector_41() const { return ___showPSInspector_41; }
	inline bool* get_address_of_showPSInspector_41() { return &___showPSInspector_41; }
	inline void set_showPSInspector_41(bool value)
	{
		___showPSInspector_41 = value;
	}

	inline static int32_t get_offset_of_showSpriteInspector_42() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showSpriteInspector_42)); }
	inline bool get_showSpriteInspector_42() const { return ___showSpriteInspector_42; }
	inline bool* get_address_of_showSpriteInspector_42() { return &___showSpriteInspector_42; }
	inline void set_showSpriteInspector_42(bool value)
	{
		___showSpriteInspector_42 = value;
	}

	inline static int32_t get_offset_of_showEventInspector_43() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showEventInspector_43)); }
	inline bool get_showEventInspector_43() const { return ___showEventInspector_43; }
	inline bool* get_address_of_showEventInspector_43() { return &___showEventInspector_43; }
	inline void set_showEventInspector_43(bool value)
	{
		___showEventInspector_43 = value;
	}

	inline static int32_t get_offset_of_showBehaviourInspector_44() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showBehaviourInspector_44)); }
	inline bool get_showBehaviourInspector_44() const { return ___showBehaviourInspector_44; }
	inline bool* get_address_of_showBehaviourInspector_44() { return &___showBehaviourInspector_44; }
	inline void set_showBehaviourInspector_44(bool value)
	{
		___showBehaviourInspector_44 = value;
	}

	inline static int32_t get_offset_of_showAxesInspector_45() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showAxesInspector_45)); }
	inline bool get_showAxesInspector_45() const { return ___showAxesInspector_45; }
	inline bool* get_address_of_showAxesInspector_45() { return &___showAxesInspector_45; }
	inline void set_showAxesInspector_45(bool value)
	{
		___showAxesInspector_45 = value;
	}

	inline static int32_t get_offset_of_showTouchEventInspector_46() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showTouchEventInspector_46)); }
	inline bool get_showTouchEventInspector_46() const { return ___showTouchEventInspector_46; }
	inline bool* get_address_of_showTouchEventInspector_46() { return &___showTouchEventInspector_46; }
	inline void set_showTouchEventInspector_46(bool value)
	{
		___showTouchEventInspector_46 = value;
	}

	inline static int32_t get_offset_of_showDownEventInspector_47() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showDownEventInspector_47)); }
	inline bool get_showDownEventInspector_47() const { return ___showDownEventInspector_47; }
	inline bool* get_address_of_showDownEventInspector_47() { return &___showDownEventInspector_47; }
	inline void set_showDownEventInspector_47(bool value)
	{
		___showDownEventInspector_47 = value;
	}

	inline static int32_t get_offset_of_showPressEventInspector_48() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showPressEventInspector_48)); }
	inline bool get_showPressEventInspector_48() const { return ___showPressEventInspector_48; }
	inline bool* get_address_of_showPressEventInspector_48() { return &___showPressEventInspector_48; }
	inline void set_showPressEventInspector_48(bool value)
	{
		___showPressEventInspector_48 = value;
	}

	inline static int32_t get_offset_of_showCameraInspector_49() { return static_cast<int32_t>(offsetof(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093, ___showCameraInspector_49)); }
	inline bool get_showCameraInspector_49() const { return ___showCameraInspector_49; }
	inline bool* get_address_of_showCameraInspector_49() { return &___showCameraInspector_49; }
	inline void set_showCameraInspector_49(bool value)
	{
		___showCameraInspector_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCBASE_T6A678D4A7F7A841B76025801594E4EAFFFB1B093_H
#ifndef ETCINPUT_TFE969393152B3F3A4F73F8B503BABD0919EE0494_H
#define ETCINPUT_TFE969393152B3F3A4F73F8B503BABD0919EE0494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCInput
struct  ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ETCAxis> ETCInput::axes
	Dictionary_2_t4A5B7E37A9402EC23D05A1658456208C6658531C * ___axes_5;
	// System.Collections.Generic.Dictionary`2<System.String,ETCBase> ETCInput::controls
	Dictionary_2_t7AEE0F0C06194E196C14A615E13920AADB41E1AC * ___controls_6;

public:
	inline static int32_t get_offset_of_axes_5() { return static_cast<int32_t>(offsetof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494, ___axes_5)); }
	inline Dictionary_2_t4A5B7E37A9402EC23D05A1658456208C6658531C * get_axes_5() const { return ___axes_5; }
	inline Dictionary_2_t4A5B7E37A9402EC23D05A1658456208C6658531C ** get_address_of_axes_5() { return &___axes_5; }
	inline void set_axes_5(Dictionary_2_t4A5B7E37A9402EC23D05A1658456208C6658531C * value)
	{
		___axes_5 = value;
		Il2CppCodeGenWriteBarrier((&___axes_5), value);
	}

	inline static int32_t get_offset_of_controls_6() { return static_cast<int32_t>(offsetof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494, ___controls_6)); }
	inline Dictionary_2_t7AEE0F0C06194E196C14A615E13920AADB41E1AC * get_controls_6() const { return ___controls_6; }
	inline Dictionary_2_t7AEE0F0C06194E196C14A615E13920AADB41E1AC ** get_address_of_controls_6() { return &___controls_6; }
	inline void set_controls_6(Dictionary_2_t7AEE0F0C06194E196C14A615E13920AADB41E1AC * value)
	{
		___controls_6 = value;
		Il2CppCodeGenWriteBarrier((&___controls_6), value);
	}
};

struct ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields
{
public:
	// ETCInput ETCInput::_instance
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494 * ____instance_4;
	// ETCBase ETCInput::control
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * ___control_7;
	// ETCAxis ETCInput::axis
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axis_8;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields, ____instance_4)); }
	inline ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494 * get__instance_4() const { return ____instance_4; }
	inline ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of_control_7() { return static_cast<int32_t>(offsetof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields, ___control_7)); }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * get_control_7() const { return ___control_7; }
	inline ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 ** get_address_of_control_7() { return &___control_7; }
	inline void set_control_7(ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093 * value)
	{
		___control_7 = value;
		Il2CppCodeGenWriteBarrier((&___control_7), value);
	}

	inline static int32_t get_offset_of_axis_8() { return static_cast<int32_t>(offsetof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields, ___axis_8)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axis_8() const { return ___axis_8; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axis_8() { return &___axis_8; }
	inline void set_axis_8(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axis_8 = value;
		Il2CppCodeGenWriteBarrier((&___axis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCINPUT_TFE969393152B3F3A4F73F8B503BABD0919EE0494_H
#ifndef ETCSETDIRECTACTIONTRANSFORM_T8A44B1C167AAEC953FCAD4565BF383B28AE1FECC_H
#define ETCSETDIRECTACTIONTRANSFORM_T8A44B1C167AAEC953FCAD4565BF383B28AE1FECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCSetDirectActionTransform
struct  ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String ETCSetDirectActionTransform::axisName1
	String_t* ___axisName1_4;
	// System.String ETCSetDirectActionTransform::axisName2
	String_t* ___axisName2_5;

public:
	inline static int32_t get_offset_of_axisName1_4() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC, ___axisName1_4)); }
	inline String_t* get_axisName1_4() const { return ___axisName1_4; }
	inline String_t** get_address_of_axisName1_4() { return &___axisName1_4; }
	inline void set_axisName1_4(String_t* value)
	{
		___axisName1_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName1_4), value);
	}

	inline static int32_t get_offset_of_axisName2_5() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC, ___axisName2_5)); }
	inline String_t* get_axisName2_5() const { return ___axisName2_5; }
	inline String_t** get_address_of_axisName2_5() { return &___axisName2_5; }
	inline void set_axisName2_5(String_t* value)
	{
		___axisName2_5 = value;
		Il2CppCodeGenWriteBarrier((&___axisName2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCSETDIRECTACTIONTRANSFORM_T8A44B1C167AAEC953FCAD4565BF383B28AE1FECC_H
#ifndef EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#define EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneManagerController
struct  ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ExampleSceneManagerController::LeftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___LeftText_4;
	// UnityEngine.UI.Text ExampleSceneManagerController::RightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___RightText_5;
	// UnityEngine.UI.Slider ExampleSceneManagerController::ProgressSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___ProgressSlider_6;

public:
	inline static int32_t get_offset_of_LeftText_4() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___LeftText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_LeftText_4() const { return ___LeftText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_LeftText_4() { return &___LeftText_4; }
	inline void set_LeftText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___LeftText_4 = value;
		Il2CppCodeGenWriteBarrier((&___LeftText_4), value);
	}

	inline static int32_t get_offset_of_RightText_5() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___RightText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_RightText_5() const { return ___RightText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_RightText_5() { return &___RightText_5; }
	inline void set_RightText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___RightText_5 = value;
		Il2CppCodeGenWriteBarrier((&___RightText_5), value);
	}

	inline static int32_t get_offset_of_ProgressSlider_6() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___ProgressSlider_6)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_ProgressSlider_6() const { return ___ProgressSlider_6; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_ProgressSlider_6() { return &___ProgressSlider_6; }
	inline void set_ProgressSlider_6(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___ProgressSlider_6 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressSlider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#ifndef FPSPLAYERCONTROL_TB4DE35E5767B673B41FB5CDF03199FE427E5A469_H
#define FPSPLAYERCONTROL_TB4DE35E5767B673B41FB5CDF03199FE427E5A469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSPlayerControl
struct  FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip FPSPlayerControl::gunSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___gunSound_4;
	// UnityEngine.AudioClip FPSPlayerControl::reload
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___reload_5;
	// UnityEngine.AudioClip FPSPlayerControl::needReload
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___needReload_6;
	// UnityEngine.ParticleSystem FPSPlayerControl::shellParticle
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___shellParticle_7;
	// UnityEngine.GameObject FPSPlayerControl::muzzleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___muzzleEffect_8;
	// UnityEngine.GameObject FPSPlayerControl::impactEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___impactEffect_9;
	// UnityEngine.UI.Text FPSPlayerControl::armoText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___armoText_10;
	// System.Boolean FPSPlayerControl::inFire
	bool ___inFire_11;
	// System.Boolean FPSPlayerControl::inReload
	bool ___inReload_12;
	// UnityEngine.Animator FPSPlayerControl::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_13;
	// System.Int32 FPSPlayerControl::armoCount
	int32_t ___armoCount_14;
	// UnityEngine.AudioSource FPSPlayerControl::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_15;

public:
	inline static int32_t get_offset_of_gunSound_4() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___gunSound_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_gunSound_4() const { return ___gunSound_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_gunSound_4() { return &___gunSound_4; }
	inline void set_gunSound_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___gunSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___gunSound_4), value);
	}

	inline static int32_t get_offset_of_reload_5() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___reload_5)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_reload_5() const { return ___reload_5; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_reload_5() { return &___reload_5; }
	inline void set_reload_5(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___reload_5 = value;
		Il2CppCodeGenWriteBarrier((&___reload_5), value);
	}

	inline static int32_t get_offset_of_needReload_6() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___needReload_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_needReload_6() const { return ___needReload_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_needReload_6() { return &___needReload_6; }
	inline void set_needReload_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___needReload_6 = value;
		Il2CppCodeGenWriteBarrier((&___needReload_6), value);
	}

	inline static int32_t get_offset_of_shellParticle_7() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___shellParticle_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_shellParticle_7() const { return ___shellParticle_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_shellParticle_7() { return &___shellParticle_7; }
	inline void set_shellParticle_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___shellParticle_7 = value;
		Il2CppCodeGenWriteBarrier((&___shellParticle_7), value);
	}

	inline static int32_t get_offset_of_muzzleEffect_8() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___muzzleEffect_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_muzzleEffect_8() const { return ___muzzleEffect_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_muzzleEffect_8() { return &___muzzleEffect_8; }
	inline void set_muzzleEffect_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___muzzleEffect_8 = value;
		Il2CppCodeGenWriteBarrier((&___muzzleEffect_8), value);
	}

	inline static int32_t get_offset_of_impactEffect_9() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___impactEffect_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_impactEffect_9() const { return ___impactEffect_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_impactEffect_9() { return &___impactEffect_9; }
	inline void set_impactEffect_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___impactEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___impactEffect_9), value);
	}

	inline static int32_t get_offset_of_armoText_10() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___armoText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_armoText_10() const { return ___armoText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_armoText_10() { return &___armoText_10; }
	inline void set_armoText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___armoText_10 = value;
		Il2CppCodeGenWriteBarrier((&___armoText_10), value);
	}

	inline static int32_t get_offset_of_inFire_11() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___inFire_11)); }
	inline bool get_inFire_11() const { return ___inFire_11; }
	inline bool* get_address_of_inFire_11() { return &___inFire_11; }
	inline void set_inFire_11(bool value)
	{
		___inFire_11 = value;
	}

	inline static int32_t get_offset_of_inReload_12() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___inReload_12)); }
	inline bool get_inReload_12() const { return ___inReload_12; }
	inline bool* get_address_of_inReload_12() { return &___inReload_12; }
	inline void set_inReload_12(bool value)
	{
		___inReload_12 = value;
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___anim_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_13() const { return ___anim_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier((&___anim_13), value);
	}

	inline static int32_t get_offset_of_armoCount_14() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___armoCount_14)); }
	inline int32_t get_armoCount_14() const { return ___armoCount_14; }
	inline int32_t* get_address_of_armoCount_14() { return &___armoCount_14; }
	inline void set_armoCount_14(int32_t value)
	{
		___armoCount_14 = value;
	}

	inline static int32_t get_offset_of_audioSource_15() { return static_cast<int32_t>(offsetof(FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469, ___audioSource_15)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_15() const { return ___audioSource_15; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_15() { return &___audioSource_15; }
	inline void set_audioSource_15(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_15 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSPLAYERCONTROL_TB4DE35E5767B673B41FB5CDF03199FE427E5A469_H
#ifndef IMPACTEFFECT_TBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65_H
#define IMPACTEFFECT_TBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImpactEffect
struct  ImpactEffect_tBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem ImpactEffect::ps
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ps_4;

public:
	inline static int32_t get_offset_of_ps_4() { return static_cast<int32_t>(offsetof(ImpactEffect_tBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65, ___ps_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_ps_4() const { return ___ps_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_ps_4() { return &___ps_4; }
	inline void set_ps_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___ps_4 = value;
		Il2CppCodeGenWriteBarrier((&___ps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPACTEFFECT_TBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65_H
#ifndef LOADLEVELSCRIPT_TBC3C9CC048DF5685C6D2D8EFFE90A042A9096C90_H
#define LOADLEVELSCRIPT_TBC3C9CC048DF5685C6D2D8EFFE90A042A9096C90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLevelScript
struct  LoadLevelScript_tBC3C9CC048DF5685C6D2D8EFFE90A042A9096C90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLEVELSCRIPT_TBC3C9CC048DF5685C6D2D8EFFE90A042A9096C90_H
#ifndef RTS_NEWSYNTAXE_TC444D2DBC6DBC0E49EA524447CA37095AC3502CB_H
#define RTS_NEWSYNTAXE_TC444D2DBC6DBC0E49EA524447CA37095AC3502CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RTS_NewSyntaxe
struct  RTS_NewSyntaxe_tC444D2DBC6DBC0E49EA524447CA37095AC3502CB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject RTS_NewSyntaxe::cube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube_4;

public:
	inline static int32_t get_offset_of_cube_4() { return static_cast<int32_t>(offsetof(RTS_NewSyntaxe_tC444D2DBC6DBC0E49EA524447CA37095AC3502CB, ___cube_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube_4() const { return ___cube_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube_4() { return &___cube_4; }
	inline void set_cube_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube_4 = value;
		Il2CppCodeGenWriteBarrier((&___cube_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTS_NEWSYNTAXE_TC444D2DBC6DBC0E49EA524447CA37095AC3502CB_H
#ifndef SIMPLEACTIONEXAMPLE_TE656934E06820C74D77F690A6F25B42A1403990D_H
#define SIMPLEACTIONEXAMPLE_TE656934E06820C74D77F690A6F25B42A1403990D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleActionExample
struct  SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh SimpleActionExample::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Vector3 SimpleActionExample::startScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startScale_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startScale_5() { return static_cast<int32_t>(offsetof(SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D, ___startScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startScale_5() const { return ___startScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startScale_5() { return &___startScale_5; }
	inline void set_startScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIONEXAMPLE_TE656934E06820C74D77F690A6F25B42A1403990D_H
#ifndef SLIDERTEXT_T702B179573E67454D7EEBDD5FECC66EBCF3AB9FC_H
#define SLIDERTEXT_T702B179573E67454D7EEBDD5FECC66EBCF3AB9FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderText
struct  SliderText_t702B179573E67454D7EEBDD5FECC66EBCF3AB9FC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDERTEXT_T702B179573E67454D7EEBDD5FECC66EBCF3AB9FC_H
#ifndef TOUCHPADUIEVENT_T9B574F5909675FA40EA12E9E33D985417A4E71D2_H
#define TOUCHPADUIEVENT_T9B574F5909675FA40EA12E9E33D985417A4E71D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchPadUIEvent
struct  TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text TouchPadUIEvent::touchDownText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___touchDownText_4;
	// UnityEngine.UI.Text TouchPadUIEvent::touchText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___touchText_5;
	// UnityEngine.UI.Text TouchPadUIEvent::touchUpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___touchUpText_6;

public:
	inline static int32_t get_offset_of_touchDownText_4() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2, ___touchDownText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_touchDownText_4() const { return ___touchDownText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_touchDownText_4() { return &___touchDownText_4; }
	inline void set_touchDownText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___touchDownText_4 = value;
		Il2CppCodeGenWriteBarrier((&___touchDownText_4), value);
	}

	inline static int32_t get_offset_of_touchText_5() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2, ___touchText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_touchText_5() const { return ___touchText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_touchText_5() { return &___touchText_5; }
	inline void set_touchText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___touchText_5 = value;
		Il2CppCodeGenWriteBarrier((&___touchText_5), value);
	}

	inline static int32_t get_offset_of_touchUpText_6() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2, ___touchUpText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_touchUpText_6() const { return ___touchUpText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_touchUpText_6() { return &___touchUpText_6; }
	inline void set_touchUpText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___touchUpText_6 = value;
		Il2CppCodeGenWriteBarrier((&___touchUpText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPADUIEVENT_T9B574F5909675FA40EA12E9E33D985417A4E71D2_H
#ifndef UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#define UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrag
struct  UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UIDrag::fingerId
	int32_t ___fingerId_4;
	// System.Boolean UIDrag::drag
	bool ___drag_5;

public:
	inline static int32_t get_offset_of_fingerId_4() { return static_cast<int32_t>(offsetof(UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D, ___fingerId_4)); }
	inline int32_t get_fingerId_4() const { return ___fingerId_4; }
	inline int32_t* get_address_of_fingerId_4() { return &___fingerId_4; }
	inline void set_fingerId_4(int32_t value)
	{
		___fingerId_4 = value;
	}

	inline static int32_t get_offset_of_drag_5() { return static_cast<int32_t>(offsetof(UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D, ___drag_5)); }
	inline bool get_drag_5() const { return ___drag_5; }
	inline bool* get_address_of_drag_5() { return &___drag_5; }
	inline void set_drag_5(bool value)
	{
		___drag_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAG_T1B25857A28B4AB83D3E93A8C8F2F0956756EF98D_H
#ifndef UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#define UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPinch
struct  UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPINCH_TDA165F2BB20E871B82303F3E4BEAECD7554275C4_H
#ifndef UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#define UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITwist
struct  UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWIST_T48BEFE9487BC4195F36C655F4CC9C434A06840F0_H
#ifndef UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#define UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWindow
struct  UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOW_TB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090_H
#ifndef ETCBUTTON_T220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A_H
#define ETCBUTTON_T220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton
struct  ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A  : public ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093
{
public:
	// ETCButton_OnDownHandler ETCButton::onDown
	OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD * ___onDown_50;
	// ETCButton_OnPressedHandler ETCButton::onPressed
	OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46 * ___onPressed_51;
	// ETCButton_OnPressedValueandler ETCButton::onPressedValue
	OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C * ___onPressedValue_52;
	// ETCButton_OnUPHandler ETCButton::onUp
	OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD * ___onUp_53;
	// ETCAxis ETCButton::axis
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axis_54;
	// UnityEngine.Sprite ETCButton::normalSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___normalSprite_55;
	// UnityEngine.Color ETCButton::normalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___normalColor_56;
	// UnityEngine.Sprite ETCButton::pressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___pressedSprite_57;
	// UnityEngine.Color ETCButton::pressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___pressedColor_58;
	// UnityEngine.UI.Image ETCButton::cachedImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___cachedImage_59;
	// System.Boolean ETCButton::isOnPress
	bool ___isOnPress_60;
	// UnityEngine.GameObject ETCButton::previousDargObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___previousDargObject_61;
	// System.Boolean ETCButton::isOnTouch
	bool ___isOnTouch_62;

public:
	inline static int32_t get_offset_of_onDown_50() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___onDown_50)); }
	inline OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD * get_onDown_50() const { return ___onDown_50; }
	inline OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD ** get_address_of_onDown_50() { return &___onDown_50; }
	inline void set_onDown_50(OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD * value)
	{
		___onDown_50 = value;
		Il2CppCodeGenWriteBarrier((&___onDown_50), value);
	}

	inline static int32_t get_offset_of_onPressed_51() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___onPressed_51)); }
	inline OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46 * get_onPressed_51() const { return ___onPressed_51; }
	inline OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46 ** get_address_of_onPressed_51() { return &___onPressed_51; }
	inline void set_onPressed_51(OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46 * value)
	{
		___onPressed_51 = value;
		Il2CppCodeGenWriteBarrier((&___onPressed_51), value);
	}

	inline static int32_t get_offset_of_onPressedValue_52() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___onPressedValue_52)); }
	inline OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C * get_onPressedValue_52() const { return ___onPressedValue_52; }
	inline OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C ** get_address_of_onPressedValue_52() { return &___onPressedValue_52; }
	inline void set_onPressedValue_52(OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C * value)
	{
		___onPressedValue_52 = value;
		Il2CppCodeGenWriteBarrier((&___onPressedValue_52), value);
	}

	inline static int32_t get_offset_of_onUp_53() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___onUp_53)); }
	inline OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD * get_onUp_53() const { return ___onUp_53; }
	inline OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD ** get_address_of_onUp_53() { return &___onUp_53; }
	inline void set_onUp_53(OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD * value)
	{
		___onUp_53 = value;
		Il2CppCodeGenWriteBarrier((&___onUp_53), value);
	}

	inline static int32_t get_offset_of_axis_54() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___axis_54)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axis_54() const { return ___axis_54; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axis_54() { return &___axis_54; }
	inline void set_axis_54(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axis_54 = value;
		Il2CppCodeGenWriteBarrier((&___axis_54), value);
	}

	inline static int32_t get_offset_of_normalSprite_55() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___normalSprite_55)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_normalSprite_55() const { return ___normalSprite_55; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_normalSprite_55() { return &___normalSprite_55; }
	inline void set_normalSprite_55(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___normalSprite_55 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_55), value);
	}

	inline static int32_t get_offset_of_normalColor_56() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___normalColor_56)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_normalColor_56() const { return ___normalColor_56; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_normalColor_56() { return &___normalColor_56; }
	inline void set_normalColor_56(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___normalColor_56 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_57() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___pressedSprite_57)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_pressedSprite_57() const { return ___pressedSprite_57; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_pressedSprite_57() { return &___pressedSprite_57; }
	inline void set_pressedSprite_57(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___pressedSprite_57 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_57), value);
	}

	inline static int32_t get_offset_of_pressedColor_58() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___pressedColor_58)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_pressedColor_58() const { return ___pressedColor_58; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_pressedColor_58() { return &___pressedColor_58; }
	inline void set_pressedColor_58(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___pressedColor_58 = value;
	}

	inline static int32_t get_offset_of_cachedImage_59() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___cachedImage_59)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_cachedImage_59() const { return ___cachedImage_59; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_cachedImage_59() { return &___cachedImage_59; }
	inline void set_cachedImage_59(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___cachedImage_59 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_59), value);
	}

	inline static int32_t get_offset_of_isOnPress_60() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___isOnPress_60)); }
	inline bool get_isOnPress_60() const { return ___isOnPress_60; }
	inline bool* get_address_of_isOnPress_60() { return &___isOnPress_60; }
	inline void set_isOnPress_60(bool value)
	{
		___isOnPress_60 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_61() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___previousDargObject_61)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_previousDargObject_61() const { return ___previousDargObject_61; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_previousDargObject_61() { return &___previousDargObject_61; }
	inline void set_previousDargObject_61(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___previousDargObject_61 = value;
		Il2CppCodeGenWriteBarrier((&___previousDargObject_61), value);
	}

	inline static int32_t get_offset_of_isOnTouch_62() { return static_cast<int32_t>(offsetof(ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A, ___isOnTouch_62)); }
	inline bool get_isOnTouch_62() const { return ___isOnTouch_62; }
	inline bool* get_address_of_isOnTouch_62() { return &___isOnTouch_62; }
	inline void set_isOnTouch_62(bool value)
	{
		___isOnTouch_62 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCBUTTON_T220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A_H
#ifndef ETCDPAD_TBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343_H
#define ETCDPAD_TBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad
struct  ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343  : public ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093
{
public:
	// ETCDPad_OnMoveStartHandler ETCDPad::onMoveStart
	OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9 * ___onMoveStart_50;
	// ETCDPad_OnMoveHandler ETCDPad::onMove
	OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A * ___onMove_51;
	// ETCDPad_OnMoveSpeedHandler ETCDPad::onMoveSpeed
	OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57 * ___onMoveSpeed_52;
	// ETCDPad_OnMoveEndHandler ETCDPad::onMoveEnd
	OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF * ___onMoveEnd_53;
	// ETCDPad_OnTouchStartHandler ETCDPad::onTouchStart
	OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27 * ___onTouchStart_54;
	// ETCDPad_OnTouchUPHandler ETCDPad::onTouchUp
	OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24 * ___onTouchUp_55;
	// ETCDPad_OnDownUpHandler ETCDPad::OnDownUp
	OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * ___OnDownUp_56;
	// ETCDPad_OnDownDownHandler ETCDPad::OnDownDown
	OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * ___OnDownDown_57;
	// ETCDPad_OnDownLeftHandler ETCDPad::OnDownLeft
	OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * ___OnDownLeft_58;
	// ETCDPad_OnDownRightHandler ETCDPad::OnDownRight
	OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * ___OnDownRight_59;
	// ETCDPad_OnDownUpHandler ETCDPad::OnPressUp
	OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * ___OnPressUp_60;
	// ETCDPad_OnDownDownHandler ETCDPad::OnPressDown
	OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * ___OnPressDown_61;
	// ETCDPad_OnDownLeftHandler ETCDPad::OnPressLeft
	OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * ___OnPressLeft_62;
	// ETCDPad_OnDownRightHandler ETCDPad::OnPressRight
	OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * ___OnPressRight_63;
	// ETCAxis ETCDPad::axisX
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisX_64;
	// ETCAxis ETCDPad::axisY
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisY_65;
	// UnityEngine.Sprite ETCDPad::normalSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___normalSprite_66;
	// UnityEngine.Color ETCDPad::normalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___normalColor_67;
	// UnityEngine.Sprite ETCDPad::pressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___pressedSprite_68;
	// UnityEngine.Color ETCDPad::pressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___pressedColor_69;
	// UnityEngine.Vector2 ETCDPad::tmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___tmpAxis_70;
	// UnityEngine.Vector2 ETCDPad::OldTmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___OldTmpAxis_71;
	// System.Boolean ETCDPad::isOnTouch
	bool ___isOnTouch_72;
	// UnityEngine.UI.Image ETCDPad::cachedImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___cachedImage_73;
	// System.Single ETCDPad::buttonSizeCoef
	float ___buttonSizeCoef_74;

public:
	inline static int32_t get_offset_of_onMoveStart_50() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onMoveStart_50)); }
	inline OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9 * get_onMoveStart_50() const { return ___onMoveStart_50; }
	inline OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9 ** get_address_of_onMoveStart_50() { return &___onMoveStart_50; }
	inline void set_onMoveStart_50(OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9 * value)
	{
		___onMoveStart_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_50), value);
	}

	inline static int32_t get_offset_of_onMove_51() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onMove_51)); }
	inline OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A * get_onMove_51() const { return ___onMove_51; }
	inline OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A ** get_address_of_onMove_51() { return &___onMove_51; }
	inline void set_onMove_51(OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A * value)
	{
		___onMove_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_51), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_52() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onMoveSpeed_52)); }
	inline OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57 * get_onMoveSpeed_52() const { return ___onMoveSpeed_52; }
	inline OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57 ** get_address_of_onMoveSpeed_52() { return &___onMoveSpeed_52; }
	inline void set_onMoveSpeed_52(OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57 * value)
	{
		___onMoveSpeed_52 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_52), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_53() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onMoveEnd_53)); }
	inline OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF * get_onMoveEnd_53() const { return ___onMoveEnd_53; }
	inline OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF ** get_address_of_onMoveEnd_53() { return &___onMoveEnd_53; }
	inline void set_onMoveEnd_53(OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF * value)
	{
		___onMoveEnd_53 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_53), value);
	}

	inline static int32_t get_offset_of_onTouchStart_54() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onTouchStart_54)); }
	inline OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27 * get_onTouchStart_54() const { return ___onTouchStart_54; }
	inline OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27 ** get_address_of_onTouchStart_54() { return &___onTouchStart_54; }
	inline void set_onTouchStart_54(OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27 * value)
	{
		___onTouchStart_54 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_54), value);
	}

	inline static int32_t get_offset_of_onTouchUp_55() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___onTouchUp_55)); }
	inline OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24 * get_onTouchUp_55() const { return ___onTouchUp_55; }
	inline OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24 ** get_address_of_onTouchUp_55() { return &___onTouchUp_55; }
	inline void set_onTouchUp_55(OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24 * value)
	{
		___onTouchUp_55 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_55), value);
	}

	inline static int32_t get_offset_of_OnDownUp_56() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnDownUp_56)); }
	inline OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * get_OnDownUp_56() const { return ___OnDownUp_56; }
	inline OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 ** get_address_of_OnDownUp_56() { return &___OnDownUp_56; }
	inline void set_OnDownUp_56(OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * value)
	{
		___OnDownUp_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_56), value);
	}

	inline static int32_t get_offset_of_OnDownDown_57() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnDownDown_57)); }
	inline OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * get_OnDownDown_57() const { return ___OnDownDown_57; }
	inline OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 ** get_address_of_OnDownDown_57() { return &___OnDownDown_57; }
	inline void set_OnDownDown_57(OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * value)
	{
		___OnDownDown_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_57), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_58() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnDownLeft_58)); }
	inline OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * get_OnDownLeft_58() const { return ___OnDownLeft_58; }
	inline OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D ** get_address_of_OnDownLeft_58() { return &___OnDownLeft_58; }
	inline void set_OnDownLeft_58(OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * value)
	{
		___OnDownLeft_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_58), value);
	}

	inline static int32_t get_offset_of_OnDownRight_59() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnDownRight_59)); }
	inline OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * get_OnDownRight_59() const { return ___OnDownRight_59; }
	inline OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 ** get_address_of_OnDownRight_59() { return &___OnDownRight_59; }
	inline void set_OnDownRight_59(OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * value)
	{
		___OnDownRight_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_59), value);
	}

	inline static int32_t get_offset_of_OnPressUp_60() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnPressUp_60)); }
	inline OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * get_OnPressUp_60() const { return ___OnPressUp_60; }
	inline OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 ** get_address_of_OnPressUp_60() { return &___OnPressUp_60; }
	inline void set_OnPressUp_60(OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3 * value)
	{
		___OnPressUp_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_60), value);
	}

	inline static int32_t get_offset_of_OnPressDown_61() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnPressDown_61)); }
	inline OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * get_OnPressDown_61() const { return ___OnPressDown_61; }
	inline OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 ** get_address_of_OnPressDown_61() { return &___OnPressDown_61; }
	inline void set_OnPressDown_61(OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6 * value)
	{
		___OnPressDown_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_61), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_62() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnPressLeft_62)); }
	inline OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * get_OnPressLeft_62() const { return ___OnPressLeft_62; }
	inline OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D ** get_address_of_OnPressLeft_62() { return &___OnPressLeft_62; }
	inline void set_OnPressLeft_62(OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D * value)
	{
		___OnPressLeft_62 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_62), value);
	}

	inline static int32_t get_offset_of_OnPressRight_63() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OnPressRight_63)); }
	inline OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * get_OnPressRight_63() const { return ___OnPressRight_63; }
	inline OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 ** get_address_of_OnPressRight_63() { return &___OnPressRight_63; }
	inline void set_OnPressRight_63(OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99 * value)
	{
		___OnPressRight_63 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_63), value);
	}

	inline static int32_t get_offset_of_axisX_64() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___axisX_64)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisX_64() const { return ___axisX_64; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisX_64() { return &___axisX_64; }
	inline void set_axisX_64(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisX_64 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_64), value);
	}

	inline static int32_t get_offset_of_axisY_65() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___axisY_65)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisY_65() const { return ___axisY_65; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisY_65() { return &___axisY_65; }
	inline void set_axisY_65(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisY_65 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_65), value);
	}

	inline static int32_t get_offset_of_normalSprite_66() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___normalSprite_66)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_normalSprite_66() const { return ___normalSprite_66; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_normalSprite_66() { return &___normalSprite_66; }
	inline void set_normalSprite_66(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___normalSprite_66 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_66), value);
	}

	inline static int32_t get_offset_of_normalColor_67() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___normalColor_67)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_normalColor_67() const { return ___normalColor_67; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_normalColor_67() { return &___normalColor_67; }
	inline void set_normalColor_67(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___normalColor_67 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_68() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___pressedSprite_68)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_pressedSprite_68() const { return ___pressedSprite_68; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_pressedSprite_68() { return &___pressedSprite_68; }
	inline void set_pressedSprite_68(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___pressedSprite_68 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_68), value);
	}

	inline static int32_t get_offset_of_pressedColor_69() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___pressedColor_69)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_pressedColor_69() const { return ___pressedColor_69; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_pressedColor_69() { return &___pressedColor_69; }
	inline void set_pressedColor_69(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___pressedColor_69 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_70() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___tmpAxis_70)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_tmpAxis_70() const { return ___tmpAxis_70; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_tmpAxis_70() { return &___tmpAxis_70; }
	inline void set_tmpAxis_70(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___tmpAxis_70 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_71() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___OldTmpAxis_71)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_OldTmpAxis_71() const { return ___OldTmpAxis_71; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_OldTmpAxis_71() { return &___OldTmpAxis_71; }
	inline void set_OldTmpAxis_71(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___OldTmpAxis_71 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_72() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___isOnTouch_72)); }
	inline bool get_isOnTouch_72() const { return ___isOnTouch_72; }
	inline bool* get_address_of_isOnTouch_72() { return &___isOnTouch_72; }
	inline void set_isOnTouch_72(bool value)
	{
		___isOnTouch_72 = value;
	}

	inline static int32_t get_offset_of_cachedImage_73() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___cachedImage_73)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_cachedImage_73() const { return ___cachedImage_73; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_cachedImage_73() { return &___cachedImage_73; }
	inline void set_cachedImage_73(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___cachedImage_73 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_73), value);
	}

	inline static int32_t get_offset_of_buttonSizeCoef_74() { return static_cast<int32_t>(offsetof(ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343, ___buttonSizeCoef_74)); }
	inline float get_buttonSizeCoef_74() const { return ___buttonSizeCoef_74; }
	inline float* get_address_of_buttonSizeCoef_74() { return &___buttonSizeCoef_74; }
	inline void set_buttonSizeCoef_74(float value)
	{
		___buttonSizeCoef_74 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCDPAD_TBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343_H
#ifndef ETCJOYSTICK_TC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9_H
#define ETCJOYSTICK_TC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick
struct  ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9  : public ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093
{
public:
	// ETCJoystick_OnMoveStartHandler ETCJoystick::onMoveStart
	OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57 * ___onMoveStart_50;
	// ETCJoystick_OnMoveHandler ETCJoystick::onMove
	OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D * ___onMove_51;
	// ETCJoystick_OnMoveSpeedHandler ETCJoystick::onMoveSpeed
	OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB * ___onMoveSpeed_52;
	// ETCJoystick_OnMoveEndHandler ETCJoystick::onMoveEnd
	OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671 * ___onMoveEnd_53;
	// ETCJoystick_OnTouchStartHandler ETCJoystick::onTouchStart
	OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3 * ___onTouchStart_54;
	// ETCJoystick_OnTouchUpHandler ETCJoystick::onTouchUp
	OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0 * ___onTouchUp_55;
	// ETCJoystick_OnDownUpHandler ETCJoystick::OnDownUp
	OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * ___OnDownUp_56;
	// ETCJoystick_OnDownDownHandler ETCJoystick::OnDownDown
	OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * ___OnDownDown_57;
	// ETCJoystick_OnDownLeftHandler ETCJoystick::OnDownLeft
	OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * ___OnDownLeft_58;
	// ETCJoystick_OnDownRightHandler ETCJoystick::OnDownRight
	OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * ___OnDownRight_59;
	// ETCJoystick_OnDownUpHandler ETCJoystick::OnPressUp
	OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * ___OnPressUp_60;
	// ETCJoystick_OnDownDownHandler ETCJoystick::OnPressDown
	OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * ___OnPressDown_61;
	// ETCJoystick_OnDownLeftHandler ETCJoystick::OnPressLeft
	OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * ___OnPressLeft_62;
	// ETCJoystick_OnDownRightHandler ETCJoystick::OnPressRight
	OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * ___OnPressRight_63;
	// ETCJoystick_JoystickType ETCJoystick::joystickType
	int32_t ___joystickType_64;
	// System.Boolean ETCJoystick::allowJoystickOverTouchPad
	bool ___allowJoystickOverTouchPad_65;
	// ETCJoystick_RadiusBase ETCJoystick::radiusBase
	int32_t ___radiusBase_66;
	// System.Single ETCJoystick::radiusBaseValue
	float ___radiusBaseValue_67;
	// ETCAxis ETCJoystick::axisX
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisX_68;
	// ETCAxis ETCJoystick::axisY
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisY_69;
	// UnityEngine.RectTransform ETCJoystick::thumb
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___thumb_70;
	// ETCJoystick_JoystickArea ETCJoystick::joystickArea
	int32_t ___joystickArea_71;
	// UnityEngine.RectTransform ETCJoystick::userArea
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___userArea_72;
	// System.Boolean ETCJoystick::isTurnAndMove
	bool ___isTurnAndMove_73;
	// System.Single ETCJoystick::tmSpeed
	float ___tmSpeed_74;
	// System.Single ETCJoystick::tmAdditionnalRotation
	float ___tmAdditionnalRotation_75;
	// UnityEngine.AnimationCurve ETCJoystick::tmMoveCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___tmMoveCurve_76;
	// System.Boolean ETCJoystick::tmLockInJump
	bool ___tmLockInJump_77;
	// UnityEngine.Vector3 ETCJoystick::tmLastMove
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___tmLastMove_78;
	// UnityEngine.Vector2 ETCJoystick::thumbPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___thumbPosition_79;
	// System.Boolean ETCJoystick::isDynamicActif
	bool ___isDynamicActif_80;
	// UnityEngine.Vector2 ETCJoystick::tmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___tmpAxis_81;
	// UnityEngine.Vector2 ETCJoystick::OldTmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___OldTmpAxis_82;
	// System.Boolean ETCJoystick::isOnTouch
	bool ___isOnTouch_83;
	// System.Boolean ETCJoystick::isNoReturnThumb
	bool ___isNoReturnThumb_84;
	// UnityEngine.Vector2 ETCJoystick::noReturnPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___noReturnPosition_85;
	// UnityEngine.Vector2 ETCJoystick::noReturnOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___noReturnOffset_86;
	// System.Boolean ETCJoystick::isNoOffsetThumb
	bool ___isNoOffsetThumb_87;

public:
	inline static int32_t get_offset_of_onMoveStart_50() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onMoveStart_50)); }
	inline OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57 * get_onMoveStart_50() const { return ___onMoveStart_50; }
	inline OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57 ** get_address_of_onMoveStart_50() { return &___onMoveStart_50; }
	inline void set_onMoveStart_50(OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57 * value)
	{
		___onMoveStart_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_50), value);
	}

	inline static int32_t get_offset_of_onMove_51() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onMove_51)); }
	inline OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D * get_onMove_51() const { return ___onMove_51; }
	inline OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D ** get_address_of_onMove_51() { return &___onMove_51; }
	inline void set_onMove_51(OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D * value)
	{
		___onMove_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_51), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_52() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onMoveSpeed_52)); }
	inline OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB * get_onMoveSpeed_52() const { return ___onMoveSpeed_52; }
	inline OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB ** get_address_of_onMoveSpeed_52() { return &___onMoveSpeed_52; }
	inline void set_onMoveSpeed_52(OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB * value)
	{
		___onMoveSpeed_52 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_52), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_53() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onMoveEnd_53)); }
	inline OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671 * get_onMoveEnd_53() const { return ___onMoveEnd_53; }
	inline OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671 ** get_address_of_onMoveEnd_53() { return &___onMoveEnd_53; }
	inline void set_onMoveEnd_53(OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671 * value)
	{
		___onMoveEnd_53 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_53), value);
	}

	inline static int32_t get_offset_of_onTouchStart_54() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onTouchStart_54)); }
	inline OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3 * get_onTouchStart_54() const { return ___onTouchStart_54; }
	inline OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3 ** get_address_of_onTouchStart_54() { return &___onTouchStart_54; }
	inline void set_onTouchStart_54(OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3 * value)
	{
		___onTouchStart_54 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_54), value);
	}

	inline static int32_t get_offset_of_onTouchUp_55() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___onTouchUp_55)); }
	inline OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0 * get_onTouchUp_55() const { return ___onTouchUp_55; }
	inline OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0 ** get_address_of_onTouchUp_55() { return &___onTouchUp_55; }
	inline void set_onTouchUp_55(OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0 * value)
	{
		___onTouchUp_55 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_55), value);
	}

	inline static int32_t get_offset_of_OnDownUp_56() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnDownUp_56)); }
	inline OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * get_OnDownUp_56() const { return ___OnDownUp_56; }
	inline OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 ** get_address_of_OnDownUp_56() { return &___OnDownUp_56; }
	inline void set_OnDownUp_56(OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * value)
	{
		___OnDownUp_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_56), value);
	}

	inline static int32_t get_offset_of_OnDownDown_57() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnDownDown_57)); }
	inline OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * get_OnDownDown_57() const { return ___OnDownDown_57; }
	inline OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA ** get_address_of_OnDownDown_57() { return &___OnDownDown_57; }
	inline void set_OnDownDown_57(OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * value)
	{
		___OnDownDown_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_57), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_58() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnDownLeft_58)); }
	inline OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * get_OnDownLeft_58() const { return ___OnDownLeft_58; }
	inline OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 ** get_address_of_OnDownLeft_58() { return &___OnDownLeft_58; }
	inline void set_OnDownLeft_58(OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * value)
	{
		___OnDownLeft_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_58), value);
	}

	inline static int32_t get_offset_of_OnDownRight_59() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnDownRight_59)); }
	inline OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * get_OnDownRight_59() const { return ___OnDownRight_59; }
	inline OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 ** get_address_of_OnDownRight_59() { return &___OnDownRight_59; }
	inline void set_OnDownRight_59(OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * value)
	{
		___OnDownRight_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_59), value);
	}

	inline static int32_t get_offset_of_OnPressUp_60() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnPressUp_60)); }
	inline OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * get_OnPressUp_60() const { return ___OnPressUp_60; }
	inline OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 ** get_address_of_OnPressUp_60() { return &___OnPressUp_60; }
	inline void set_OnPressUp_60(OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92 * value)
	{
		___OnPressUp_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_60), value);
	}

	inline static int32_t get_offset_of_OnPressDown_61() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnPressDown_61)); }
	inline OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * get_OnPressDown_61() const { return ___OnPressDown_61; }
	inline OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA ** get_address_of_OnPressDown_61() { return &___OnPressDown_61; }
	inline void set_OnPressDown_61(OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA * value)
	{
		___OnPressDown_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_61), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_62() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnPressLeft_62)); }
	inline OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * get_OnPressLeft_62() const { return ___OnPressLeft_62; }
	inline OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 ** get_address_of_OnPressLeft_62() { return &___OnPressLeft_62; }
	inline void set_OnPressLeft_62(OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754 * value)
	{
		___OnPressLeft_62 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_62), value);
	}

	inline static int32_t get_offset_of_OnPressRight_63() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OnPressRight_63)); }
	inline OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * get_OnPressRight_63() const { return ___OnPressRight_63; }
	inline OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 ** get_address_of_OnPressRight_63() { return &___OnPressRight_63; }
	inline void set_OnPressRight_63(OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289 * value)
	{
		___OnPressRight_63 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_63), value);
	}

	inline static int32_t get_offset_of_joystickType_64() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___joystickType_64)); }
	inline int32_t get_joystickType_64() const { return ___joystickType_64; }
	inline int32_t* get_address_of_joystickType_64() { return &___joystickType_64; }
	inline void set_joystickType_64(int32_t value)
	{
		___joystickType_64 = value;
	}

	inline static int32_t get_offset_of_allowJoystickOverTouchPad_65() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___allowJoystickOverTouchPad_65)); }
	inline bool get_allowJoystickOverTouchPad_65() const { return ___allowJoystickOverTouchPad_65; }
	inline bool* get_address_of_allowJoystickOverTouchPad_65() { return &___allowJoystickOverTouchPad_65; }
	inline void set_allowJoystickOverTouchPad_65(bool value)
	{
		___allowJoystickOverTouchPad_65 = value;
	}

	inline static int32_t get_offset_of_radiusBase_66() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___radiusBase_66)); }
	inline int32_t get_radiusBase_66() const { return ___radiusBase_66; }
	inline int32_t* get_address_of_radiusBase_66() { return &___radiusBase_66; }
	inline void set_radiusBase_66(int32_t value)
	{
		___radiusBase_66 = value;
	}

	inline static int32_t get_offset_of_radiusBaseValue_67() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___radiusBaseValue_67)); }
	inline float get_radiusBaseValue_67() const { return ___radiusBaseValue_67; }
	inline float* get_address_of_radiusBaseValue_67() { return &___radiusBaseValue_67; }
	inline void set_radiusBaseValue_67(float value)
	{
		___radiusBaseValue_67 = value;
	}

	inline static int32_t get_offset_of_axisX_68() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___axisX_68)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisX_68() const { return ___axisX_68; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisX_68() { return &___axisX_68; }
	inline void set_axisX_68(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisX_68 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_68), value);
	}

	inline static int32_t get_offset_of_axisY_69() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___axisY_69)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisY_69() const { return ___axisY_69; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisY_69() { return &___axisY_69; }
	inline void set_axisY_69(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisY_69 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_69), value);
	}

	inline static int32_t get_offset_of_thumb_70() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___thumb_70)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_thumb_70() const { return ___thumb_70; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_thumb_70() { return &___thumb_70; }
	inline void set_thumb_70(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___thumb_70 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_70), value);
	}

	inline static int32_t get_offset_of_joystickArea_71() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___joystickArea_71)); }
	inline int32_t get_joystickArea_71() const { return ___joystickArea_71; }
	inline int32_t* get_address_of_joystickArea_71() { return &___joystickArea_71; }
	inline void set_joystickArea_71(int32_t value)
	{
		___joystickArea_71 = value;
	}

	inline static int32_t get_offset_of_userArea_72() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___userArea_72)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_userArea_72() const { return ___userArea_72; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_userArea_72() { return &___userArea_72; }
	inline void set_userArea_72(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___userArea_72 = value;
		Il2CppCodeGenWriteBarrier((&___userArea_72), value);
	}

	inline static int32_t get_offset_of_isTurnAndMove_73() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___isTurnAndMove_73)); }
	inline bool get_isTurnAndMove_73() const { return ___isTurnAndMove_73; }
	inline bool* get_address_of_isTurnAndMove_73() { return &___isTurnAndMove_73; }
	inline void set_isTurnAndMove_73(bool value)
	{
		___isTurnAndMove_73 = value;
	}

	inline static int32_t get_offset_of_tmSpeed_74() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmSpeed_74)); }
	inline float get_tmSpeed_74() const { return ___tmSpeed_74; }
	inline float* get_address_of_tmSpeed_74() { return &___tmSpeed_74; }
	inline void set_tmSpeed_74(float value)
	{
		___tmSpeed_74 = value;
	}

	inline static int32_t get_offset_of_tmAdditionnalRotation_75() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmAdditionnalRotation_75)); }
	inline float get_tmAdditionnalRotation_75() const { return ___tmAdditionnalRotation_75; }
	inline float* get_address_of_tmAdditionnalRotation_75() { return &___tmAdditionnalRotation_75; }
	inline void set_tmAdditionnalRotation_75(float value)
	{
		___tmAdditionnalRotation_75 = value;
	}

	inline static int32_t get_offset_of_tmMoveCurve_76() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmMoveCurve_76)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_tmMoveCurve_76() const { return ___tmMoveCurve_76; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_tmMoveCurve_76() { return &___tmMoveCurve_76; }
	inline void set_tmMoveCurve_76(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___tmMoveCurve_76 = value;
		Il2CppCodeGenWriteBarrier((&___tmMoveCurve_76), value);
	}

	inline static int32_t get_offset_of_tmLockInJump_77() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmLockInJump_77)); }
	inline bool get_tmLockInJump_77() const { return ___tmLockInJump_77; }
	inline bool* get_address_of_tmLockInJump_77() { return &___tmLockInJump_77; }
	inline void set_tmLockInJump_77(bool value)
	{
		___tmLockInJump_77 = value;
	}

	inline static int32_t get_offset_of_tmLastMove_78() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmLastMove_78)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_tmLastMove_78() const { return ___tmLastMove_78; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_tmLastMove_78() { return &___tmLastMove_78; }
	inline void set_tmLastMove_78(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___tmLastMove_78 = value;
	}

	inline static int32_t get_offset_of_thumbPosition_79() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___thumbPosition_79)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_thumbPosition_79() const { return ___thumbPosition_79; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_thumbPosition_79() { return &___thumbPosition_79; }
	inline void set_thumbPosition_79(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___thumbPosition_79 = value;
	}

	inline static int32_t get_offset_of_isDynamicActif_80() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___isDynamicActif_80)); }
	inline bool get_isDynamicActif_80() const { return ___isDynamicActif_80; }
	inline bool* get_address_of_isDynamicActif_80() { return &___isDynamicActif_80; }
	inline void set_isDynamicActif_80(bool value)
	{
		___isDynamicActif_80 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_81() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___tmpAxis_81)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_tmpAxis_81() const { return ___tmpAxis_81; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_tmpAxis_81() { return &___tmpAxis_81; }
	inline void set_tmpAxis_81(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___tmpAxis_81 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_82() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___OldTmpAxis_82)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_OldTmpAxis_82() const { return ___OldTmpAxis_82; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_OldTmpAxis_82() { return &___OldTmpAxis_82; }
	inline void set_OldTmpAxis_82(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___OldTmpAxis_82 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_83() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___isOnTouch_83)); }
	inline bool get_isOnTouch_83() const { return ___isOnTouch_83; }
	inline bool* get_address_of_isOnTouch_83() { return &___isOnTouch_83; }
	inline void set_isOnTouch_83(bool value)
	{
		___isOnTouch_83 = value;
	}

	inline static int32_t get_offset_of_isNoReturnThumb_84() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___isNoReturnThumb_84)); }
	inline bool get_isNoReturnThumb_84() const { return ___isNoReturnThumb_84; }
	inline bool* get_address_of_isNoReturnThumb_84() { return &___isNoReturnThumb_84; }
	inline void set_isNoReturnThumb_84(bool value)
	{
		___isNoReturnThumb_84 = value;
	}

	inline static int32_t get_offset_of_noReturnPosition_85() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___noReturnPosition_85)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_noReturnPosition_85() const { return ___noReturnPosition_85; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_noReturnPosition_85() { return &___noReturnPosition_85; }
	inline void set_noReturnPosition_85(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___noReturnPosition_85 = value;
	}

	inline static int32_t get_offset_of_noReturnOffset_86() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___noReturnOffset_86)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_noReturnOffset_86() const { return ___noReturnOffset_86; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_noReturnOffset_86() { return &___noReturnOffset_86; }
	inline void set_noReturnOffset_86(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___noReturnOffset_86 = value;
	}

	inline static int32_t get_offset_of_isNoOffsetThumb_87() { return static_cast<int32_t>(offsetof(ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9, ___isNoOffsetThumb_87)); }
	inline bool get_isNoOffsetThumb_87() const { return ___isNoOffsetThumb_87; }
	inline bool* get_address_of_isNoOffsetThumb_87() { return &___isNoOffsetThumb_87; }
	inline void set_isNoOffsetThumb_87(bool value)
	{
		___isNoOffsetThumb_87 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCJOYSTICK_TC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9_H
#ifndef ETCTOUCHPAD_T305D6BFF501C77B8BBEA31220C3D5AE1E91C3062_H
#define ETCTOUCHPAD_T305D6BFF501C77B8BBEA31220C3D5AE1E91C3062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad
struct  ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062  : public ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093
{
public:
	// ETCTouchPad_OnMoveStartHandler ETCTouchPad::onMoveStart
	OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F * ___onMoveStart_50;
	// ETCTouchPad_OnMoveHandler ETCTouchPad::onMove
	OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1 * ___onMove_51;
	// ETCTouchPad_OnMoveSpeedHandler ETCTouchPad::onMoveSpeed
	OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9 * ___onMoveSpeed_52;
	// ETCTouchPad_OnMoveEndHandler ETCTouchPad::onMoveEnd
	OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0 * ___onMoveEnd_53;
	// ETCTouchPad_OnTouchStartHandler ETCTouchPad::onTouchStart
	OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09 * ___onTouchStart_54;
	// ETCTouchPad_OnTouchUPHandler ETCTouchPad::onTouchUp
	OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0 * ___onTouchUp_55;
	// ETCTouchPad_OnDownUpHandler ETCTouchPad::OnDownUp
	OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * ___OnDownUp_56;
	// ETCTouchPad_OnDownDownHandler ETCTouchPad::OnDownDown
	OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * ___OnDownDown_57;
	// ETCTouchPad_OnDownLeftHandler ETCTouchPad::OnDownLeft
	OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * ___OnDownLeft_58;
	// ETCTouchPad_OnDownRightHandler ETCTouchPad::OnDownRight
	OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * ___OnDownRight_59;
	// ETCTouchPad_OnDownUpHandler ETCTouchPad::OnPressUp
	OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * ___OnPressUp_60;
	// ETCTouchPad_OnDownDownHandler ETCTouchPad::OnPressDown
	OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * ___OnPressDown_61;
	// ETCTouchPad_OnDownLeftHandler ETCTouchPad::OnPressLeft
	OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * ___OnPressLeft_62;
	// ETCTouchPad_OnDownRightHandler ETCTouchPad::OnPressRight
	OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * ___OnPressRight_63;
	// ETCAxis ETCTouchPad::axisX
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisX_64;
	// ETCAxis ETCTouchPad::axisY
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * ___axisY_65;
	// System.Boolean ETCTouchPad::isDPI
	bool ___isDPI_66;
	// UnityEngine.UI.Image ETCTouchPad::cachedImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___cachedImage_67;
	// UnityEngine.Vector2 ETCTouchPad::tmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___tmpAxis_68;
	// UnityEngine.Vector2 ETCTouchPad::OldTmpAxis
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___OldTmpAxis_69;
	// UnityEngine.GameObject ETCTouchPad::previousDargObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___previousDargObject_70;
	// System.Boolean ETCTouchPad::isOut
	bool ___isOut_71;
	// System.Boolean ETCTouchPad::isOnTouch
	bool ___isOnTouch_72;
	// System.Boolean ETCTouchPad::cachedVisible
	bool ___cachedVisible_73;

public:
	inline static int32_t get_offset_of_onMoveStart_50() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onMoveStart_50)); }
	inline OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F * get_onMoveStart_50() const { return ___onMoveStart_50; }
	inline OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F ** get_address_of_onMoveStart_50() { return &___onMoveStart_50; }
	inline void set_onMoveStart_50(OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F * value)
	{
		___onMoveStart_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_50), value);
	}

	inline static int32_t get_offset_of_onMove_51() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onMove_51)); }
	inline OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1 * get_onMove_51() const { return ___onMove_51; }
	inline OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1 ** get_address_of_onMove_51() { return &___onMove_51; }
	inline void set_onMove_51(OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1 * value)
	{
		___onMove_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_51), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_52() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onMoveSpeed_52)); }
	inline OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9 * get_onMoveSpeed_52() const { return ___onMoveSpeed_52; }
	inline OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9 ** get_address_of_onMoveSpeed_52() { return &___onMoveSpeed_52; }
	inline void set_onMoveSpeed_52(OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9 * value)
	{
		___onMoveSpeed_52 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_52), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_53() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onMoveEnd_53)); }
	inline OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0 * get_onMoveEnd_53() const { return ___onMoveEnd_53; }
	inline OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0 ** get_address_of_onMoveEnd_53() { return &___onMoveEnd_53; }
	inline void set_onMoveEnd_53(OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0 * value)
	{
		___onMoveEnd_53 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_53), value);
	}

	inline static int32_t get_offset_of_onTouchStart_54() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onTouchStart_54)); }
	inline OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09 * get_onTouchStart_54() const { return ___onTouchStart_54; }
	inline OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09 ** get_address_of_onTouchStart_54() { return &___onTouchStart_54; }
	inline void set_onTouchStart_54(OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09 * value)
	{
		___onTouchStart_54 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_54), value);
	}

	inline static int32_t get_offset_of_onTouchUp_55() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___onTouchUp_55)); }
	inline OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0 * get_onTouchUp_55() const { return ___onTouchUp_55; }
	inline OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0 ** get_address_of_onTouchUp_55() { return &___onTouchUp_55; }
	inline void set_onTouchUp_55(OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0 * value)
	{
		___onTouchUp_55 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_55), value);
	}

	inline static int32_t get_offset_of_OnDownUp_56() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnDownUp_56)); }
	inline OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * get_OnDownUp_56() const { return ___OnDownUp_56; }
	inline OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 ** get_address_of_OnDownUp_56() { return &___OnDownUp_56; }
	inline void set_OnDownUp_56(OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * value)
	{
		___OnDownUp_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_56), value);
	}

	inline static int32_t get_offset_of_OnDownDown_57() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnDownDown_57)); }
	inline OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * get_OnDownDown_57() const { return ___OnDownDown_57; }
	inline OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 ** get_address_of_OnDownDown_57() { return &___OnDownDown_57; }
	inline void set_OnDownDown_57(OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * value)
	{
		___OnDownDown_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_57), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_58() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnDownLeft_58)); }
	inline OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * get_OnDownLeft_58() const { return ___OnDownLeft_58; }
	inline OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 ** get_address_of_OnDownLeft_58() { return &___OnDownLeft_58; }
	inline void set_OnDownLeft_58(OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * value)
	{
		___OnDownLeft_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_58), value);
	}

	inline static int32_t get_offset_of_OnDownRight_59() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnDownRight_59)); }
	inline OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * get_OnDownRight_59() const { return ___OnDownRight_59; }
	inline OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C ** get_address_of_OnDownRight_59() { return &___OnDownRight_59; }
	inline void set_OnDownRight_59(OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * value)
	{
		___OnDownRight_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_59), value);
	}

	inline static int32_t get_offset_of_OnPressUp_60() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnPressUp_60)); }
	inline OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * get_OnPressUp_60() const { return ___OnPressUp_60; }
	inline OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 ** get_address_of_OnPressUp_60() { return &___OnPressUp_60; }
	inline void set_OnPressUp_60(OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230 * value)
	{
		___OnPressUp_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_60), value);
	}

	inline static int32_t get_offset_of_OnPressDown_61() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnPressDown_61)); }
	inline OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * get_OnPressDown_61() const { return ___OnPressDown_61; }
	inline OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 ** get_address_of_OnPressDown_61() { return &___OnPressDown_61; }
	inline void set_OnPressDown_61(OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238 * value)
	{
		___OnPressDown_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_61), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_62() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnPressLeft_62)); }
	inline OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * get_OnPressLeft_62() const { return ___OnPressLeft_62; }
	inline OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 ** get_address_of_OnPressLeft_62() { return &___OnPressLeft_62; }
	inline void set_OnPressLeft_62(OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180 * value)
	{
		___OnPressLeft_62 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_62), value);
	}

	inline static int32_t get_offset_of_OnPressRight_63() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OnPressRight_63)); }
	inline OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * get_OnPressRight_63() const { return ___OnPressRight_63; }
	inline OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C ** get_address_of_OnPressRight_63() { return &___OnPressRight_63; }
	inline void set_OnPressRight_63(OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C * value)
	{
		___OnPressRight_63 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_63), value);
	}

	inline static int32_t get_offset_of_axisX_64() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___axisX_64)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisX_64() const { return ___axisX_64; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisX_64() { return &___axisX_64; }
	inline void set_axisX_64(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisX_64 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_64), value);
	}

	inline static int32_t get_offset_of_axisY_65() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___axisY_65)); }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * get_axisY_65() const { return ___axisY_65; }
	inline ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 ** get_address_of_axisY_65() { return &___axisY_65; }
	inline void set_axisY_65(ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9 * value)
	{
		___axisY_65 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_65), value);
	}

	inline static int32_t get_offset_of_isDPI_66() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___isDPI_66)); }
	inline bool get_isDPI_66() const { return ___isDPI_66; }
	inline bool* get_address_of_isDPI_66() { return &___isDPI_66; }
	inline void set_isDPI_66(bool value)
	{
		___isDPI_66 = value;
	}

	inline static int32_t get_offset_of_cachedImage_67() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___cachedImage_67)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_cachedImage_67() const { return ___cachedImage_67; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_cachedImage_67() { return &___cachedImage_67; }
	inline void set_cachedImage_67(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___cachedImage_67 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_67), value);
	}

	inline static int32_t get_offset_of_tmpAxis_68() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___tmpAxis_68)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_tmpAxis_68() const { return ___tmpAxis_68; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_tmpAxis_68() { return &___tmpAxis_68; }
	inline void set_tmpAxis_68(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___tmpAxis_68 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_69() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___OldTmpAxis_69)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_OldTmpAxis_69() const { return ___OldTmpAxis_69; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_OldTmpAxis_69() { return &___OldTmpAxis_69; }
	inline void set_OldTmpAxis_69(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___OldTmpAxis_69 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_70() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___previousDargObject_70)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_previousDargObject_70() const { return ___previousDargObject_70; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_previousDargObject_70() { return &___previousDargObject_70; }
	inline void set_previousDargObject_70(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___previousDargObject_70 = value;
		Il2CppCodeGenWriteBarrier((&___previousDargObject_70), value);
	}

	inline static int32_t get_offset_of_isOut_71() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___isOut_71)); }
	inline bool get_isOut_71() const { return ___isOut_71; }
	inline bool* get_address_of_isOut_71() { return &___isOut_71; }
	inline void set_isOut_71(bool value)
	{
		___isOut_71 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_72() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___isOnTouch_72)); }
	inline bool get_isOnTouch_72() const { return ___isOnTouch_72; }
	inline bool* get_address_of_isOnTouch_72() { return &___isOnTouch_72; }
	inline void set_isOnTouch_72(bool value)
	{
		___isOnTouch_72 = value;
	}

	inline static int32_t get_offset_of_cachedVisible_73() { return static_cast<int32_t>(offsetof(ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062, ___cachedVisible_73)); }
	inline bool get_cachedVisible_73() const { return ___cachedVisible_73; }
	inline bool* get_address_of_cachedVisible_73() { return &___cachedVisible_73; }
	inline void set_cachedVisible_73(bool value)
	{
		___cachedVisible_73 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCTOUCHPAD_T305D6BFF501C77B8BBEA31220C3D5AE1E91C3062_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (UIWindow_tB7DDFF2C4C87FBF0F1E6FEFDA3441CBD9857F090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[2] = 
{
	UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D::get_offset_of_fingerId_4(),
	UIDrag_t1B25857A28B4AB83D3E93A8C8F2F0956756EF98D::get_offset_of_drag_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (UIPinch_tDA165F2BB20E871B82303F3E4BEAECD7554275C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (UITwist_t48BEFE9487BC4195F36C655F4CC9C434A06840F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (RTS_NewSyntaxe_tC444D2DBC6DBC0E49EA524447CA37095AC3502CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[1] = 
{
	RTS_NewSyntaxe_tC444D2DBC6DBC0E49EA524447CA37095AC3502CB::get_offset_of_cube_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[2] = 
{
	SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D::get_offset_of_textMesh_4(),
	SimpleActionExample_tE656934E06820C74D77F690A6F25B42A1403990D::get_offset_of_startScale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3506[2] = 
{
	ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC::get_offset_of_axisName1_4(),
	ETCSetDirectActionTransform_t8A44B1C167AAEC953FCAD4565BF383B28AE1FECC::get_offset_of_axisName2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (SliderText_t702B179573E67454D7EEBDD5FECC66EBCF3AB9FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3508[4] = 
{
	ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796::get_offset_of_getButtonDownText_4(),
	ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796::get_offset_of_getButtonText_5(),
	ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796::get_offset_of_getButtonTimeText_6(),
	ButtonInputUI_t4297693945A2DA852F81C92011A08E398C3C8796::get_offset_of_getButtonUpText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3509[3] = 
{
	U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697::get_offset_of_U3CU3E1__state_0(),
	U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697::get_offset_of_U3CU3E2__current_1(),
	U3CClearTextU3Ed__5_t08105821CD0F6E31FFB7E294A5B808103F134697::get_offset_of_textToCLead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[4] = 
{
	ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF::get_offset_of_downText_4(),
	ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF::get_offset_of_pressText_5(),
	ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF::get_offset_of_pressValueText_6(),
	ButtonUIEvent_t5DB4F86C8A6911F40300C554D3BAD5E8793987EF::get_offset_of_upText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[3] = 
{
	U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170::get_offset_of_U3CU3E1__state_0(),
	U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170::get_offset_of_U3CU3E2__current_1(),
	U3CClearTextU3Ed__8_t5A06B3144CA1DDDF52A7B2F47BE45EAB7DBF8170::get_offset_of_textToCLead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3512[18] = 
{
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_moveStartText_4(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_moveText_5(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_moveSpeedText_6(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_moveEndText_7(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_touchStartText_8(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_touchUpText_9(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_downRightText_10(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_downDownText_11(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_downLeftText_12(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_downUpText_13(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_rightText_14(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_downText_15(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_leftText_16(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_upText_17(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_isDown_18(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_isLeft_19(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_isUp_20(),
	ControlUIEvent_tF7EE2D4FB7DF33E73665FB79356DDBC86AB9BA4C::get_offset_of_isRight_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3513[3] = 
{
	U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B::get_offset_of_U3CU3E1__state_0(),
	U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B::get_offset_of_U3CU3E2__current_1(),
	U3CClearTextU3Ed__33_t11A0AD90DD1DDBA472B127878B4B6A37464DE81B::get_offset_of_textToCLead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[12] = 
{
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_getAxisText_4(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_getAxisSpeedText_5(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_getAxisYText_6(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_getAxisYSpeedText_7(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_downRightText_8(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_downDownText_9(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_downLeftText_10(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_downUpText_11(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_rightText_12(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_downText_13(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_leftText_14(),
	ControlUIInput_t319CEA9A7317BB312F913159C0F0243649B53627::get_offset_of_upText_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[3] = 
{
	U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8::get_offset_of_U3CU3E1__state_0(),
	U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8::get_offset_of_U3CU3E2__current_1(),
	U3CClearTextU3Ed__13_t29239CDB345E703239300479369E3677C82172C8::get_offset_of_textToCLead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (DPadParameterUI_tF90103620860837B0B7E919E9919C8E893FBCE8A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3517[12] = 
{
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_gunSound_4(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_reload_5(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_needReload_6(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_shellParticle_7(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_muzzleEffect_8(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_impactEffect_9(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_armoText_10(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_inFire_11(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_inReload_12(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_anim_13(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_armoCount_14(),
	FPSPlayerControl_tB4DE35E5767B673B41FB5CDF03199FE427E5A469::get_offset_of_audioSource_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3518[3] = 
{
	U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419::get_offset_of_U3CU3E1__state_0(),
	U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419::get_offset_of_U3CU3E2__current_1(),
	U3CFlashU3Ed__18_t5284EE5AA31143EE0DE5403455C0DD039A950419::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[3] = 
{
	U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188::get_offset_of_U3CU3E1__state_0(),
	U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188::get_offset_of_U3CU3E2__current_1(),
	U3CReloadU3Ed__19_tED8AA4F5BF009BD4CD9F6A6B2500555912645188::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (ImpactEffect_tBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3520[1] = 
{
	ImpactEffect_tBAA1FC1B9F1D5F5786C10CD751ADDC2DE99E4A65::get_offset_of_ps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (AxisXUi_tCF6461EE4911A3FF6095AD59A726B38F362A681E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (LoadLevelScript_tBC3C9CC048DF5685C6D2D8EFFE90A042A9096C90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[3] = 
{
	TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2::get_offset_of_touchDownText_4(),
	TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2::get_offset_of_touchText_5(),
	TouchPadUIEvent_t9B574F5909675FA40EA12E9E33D985417A4E71D2::get_offset_of_touchUpText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[3] = 
{
	U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555::get_offset_of_U3CU3E1__state_0(),
	U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555::get_offset_of_U3CU3E2__current_1(),
	U3CClearTextU3Ed__6_t95E184D5667E1E25F538021C285D826D4FCAC555::get_offset_of_textToCLead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[2] = 
{
	CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95::get_offset_of_cc_4(),
	CharacterAnimation_t492D59F02F07753618C579A8DA6A5D65EB8D5F95::get_offset_of_anim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3526[2] = 
{
	CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5::get_offset_of_cc_4(),
	CharacterAnimationDungeon_t7CE92C537930F9782D0A91E4BA001EAF2ABB65E5::get_offset_of_anim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (ComponentExtensions_tA12A26B2529343C28D98F632D33E53551698E8EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (ETCArea_tAEB2B810ED837AB23170D5BB0B2E0E425E2654B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[1] = 
{
	ETCArea_tAEB2B810ED837AB23170D5BB0B2E0E425E2654B7::get_offset_of_show_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (AreaPreset_t034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3529[6] = 
{
	AreaPreset_t034EE0E84BED677BA372A6D47E6D1BBCEBAE0FC9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[43] = 
{
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_name_0(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_autoLinkTagPlayer_1(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_autoTag_2(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_player_3(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_enable_4(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_invertedAxis_5(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_speed_6(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_deadValue_7(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_valueMethod_8(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_curveValue_9(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isEnertia_10(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_inertia_11(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_inertiaThreshold_12(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isAutoStab_13(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_autoStabThreshold_14(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_autoStabSpeed_15(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_startAngle_16(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isClampRotation_17(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_maxAngle_18(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_minAngle_19(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isValueOverTime_20(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_overTimeStep_21(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_maxOverTimeValue_22(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_axisValue_23(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_axisSpeedValue_24(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_axisThreshold_25(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isLockinJump_26(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_lastMove_27(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_axisState_28(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of__directTransform_29(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_directAction_30(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_axisInfluenced_31(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_actionOn_32(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_directCharacterController_33(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_directRigidBody_34(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_gravity_35(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_currentGravity_36(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_isJump_37(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_unityAxis_38(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_showGeneralInspector_39(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_showDirectInspector_40(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_showInertiaInspector_41(),
	ETCAxis_t950EE7D6F878394ACEF29C1F6CF346541A1230E9::get_offset_of_showSimulatinInspector_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (DirectAction_t61B43CD6183FE5997A7C37F824B569D2AE06290C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3531[11] = 
{
	DirectAction_t61B43CD6183FE5997A7C37F824B569D2AE06290C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (AxisInfluenced_t96C763AB9711242CF0DD716ADA5241F2C6498DEB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3532[4] = 
{
	AxisInfluenced_t96C763AB9711242CF0DD716ADA5241F2C6498DEB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (AxisValueMethod_t1C86AE4E7112F90169A034FA65E53A3601BC32CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3533[3] = 
{
	AxisValueMethod_t1C86AE4E7112F90169A034FA65E53A3601BC32CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (AxisState_t4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3534[13] = 
{
	AxisState_t4C73853AF268BD24937FEAB13DEE4B18B6FE0AD2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (ActionOn_tF6F8906139D1BE7F77EC53F6C30F311532EDD3B2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3535[3] = 
{
	ActionOn_tF6F8906139D1BE7F77EC53F6C30F311532EDD3B2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3536[46] = 
{
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cachedRectTransform_4(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cachedRootCanvas_5(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_isUnregisterAtDisable_6(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_visibleAtStart_7(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_activatedAtStart_8(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of__anchor_9(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of__anchorOffet_10(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of__visible_11(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of__activated_12(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_enableCamera_13(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cameraMode_14(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_camTargetTag_15(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_autoLinkTagCam_16(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_autoCamTag_17(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cameraTransform_18(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cameraTargetMode_19(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_enableWallDetection_20(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_wallLayer_21(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cameraLookAt_22(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_cameraLookAtCC_23(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_followOffset_24(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_followDistance_25(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_followHeight_26(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_followRotationDamping_27(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_followHeightDamping_28(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_pointId_29(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_enableKeySimulation_30(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_allowSimulationStandalone_31(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_visibleOnStandalone_32(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_dPadAxisCount_33(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_useFixedUpdate_34(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_uiRaycastResultCache_35(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_uiPointerEventData_36(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_uiEventSystem_37(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_isOnDrag_38(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_isSwipeIn_39(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_isSwipeOut_40(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showPSInspector_41(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showSpriteInspector_42(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showEventInspector_43(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showBehaviourInspector_44(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showAxesInspector_45(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showTouchEventInspector_46(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showDownEventInspector_47(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showPressEventInspector_48(),
	ETCBase_t6A678D4A7F7A841B76025801594E4EAFFFB1B093::get_offset_of_showCameraInspector_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (ControlType_t2C7A7438CCA278A1A1764500339DDC2A97583F74)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3537[5] = 
{
	ControlType_t2C7A7438CCA278A1A1764500339DDC2A97583F74::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (RectAnchor_tAF34790C8468DF8E5E5982B2F86D6BA15BB943C2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3538[11] = 
{
	RectAnchor_tAF34790C8468DF8E5E5982B2F86D6BA15BB943C2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (DPadAxis_t043E01EF634A00A308D177ACD6A5C63D401FC217)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3539[3] = 
{
	DPadAxis_t043E01EF634A00A308D177ACD6A5C63D401FC217::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (CameraMode_tB88394DD8E850D9D3DEB6E93D06316507E591C1F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3540[3] = 
{
	CameraMode_tB88394DD8E850D9D3DEB6E93D06316507E591C1F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (CameraTargetMode_t1D7A162CEB13387377CACC9D5190AABE90AF176D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3541[5] = 
{
	CameraTargetMode_t1D7A162CEB13387377CACC9D5190AABE90AF176D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[3] = 
{
	U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateVirtualControlU3Ed__78_t2738E83AB3972E75A712C00B2C7A5A93978C3FE0::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[3] = 
{
	U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8::get_offset_of_U3CU3E1__state_0(),
	U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8::get_offset_of_U3CU3E2__current_1(),
	U3CFixedUpdateVirtualControlU3Ed__79_t83A8865D2EFA7E55C0D4202F0E512F416785C3D8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3544[13] = 
{
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_onDown_50(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_onPressed_51(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_onPressedValue_52(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_onUp_53(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_axis_54(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_normalSprite_55(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_normalColor_56(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_pressedSprite_57(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_pressedColor_58(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_cachedImage_59(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_isOnPress_60(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_previousDargObject_61(),
	ETCButton_t220E9F9FF4D3B35989D779EF5EEA6BF8A1F80D0A::get_offset_of_isOnTouch_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (OnDownHandler_t3BF646CB3B50D2B1568AD29812CFA5AF62241ECD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (OnPressedHandler_tAACE80106F872BB737F1A63DE93CF90A88518E46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (OnPressedValueandler_tE434F6E6375463F07A22F2F133D6E4D863A19C8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (OnUPHandler_t5E09F875FD5F20532E1604E545DD1081A51BB6BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[25] = 
{
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onMoveStart_50(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onMove_51(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onMoveSpeed_52(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onMoveEnd_53(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onTouchStart_54(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_onTouchUp_55(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnDownUp_56(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnDownDown_57(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnDownLeft_58(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnDownRight_59(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnPressUp_60(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnPressDown_61(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnPressLeft_62(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OnPressRight_63(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_axisX_64(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_axisY_65(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_normalSprite_66(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_normalColor_67(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_pressedSprite_68(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_pressedColor_69(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_tmpAxis_70(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_OldTmpAxis_71(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_isOnTouch_72(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_cachedImage_73(),
	ETCDPad_tBA16D9B62F4D8E1A55CE498CAED81EF8B55F5343::get_offset_of_buttonSizeCoef_74(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (OnMoveStartHandler_t348E0BECE9C131ABDB6405E3375FACBEDD7834F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (OnMoveHandler_tE90F89A5EF87D5519185F00FDBC4984560643A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (OnMoveSpeedHandler_t59F42533F55D150D683041ECB2F2CB33E8897B57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (OnMoveEndHandler_t3C6D36AF08ABF808E58B32604A4476968D845FAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (OnTouchStartHandler_t76BF5BC87655858404EDBC8DE0D3ED8C502C8D27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (OnTouchUPHandler_t1D7253E44E150B5F82C580FC7686BEFB9EF2AA24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (OnDownUpHandler_t2398088B051F3862749014C92250C3B138A120D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (OnDownDownHandler_tA3F8EDD0CB70D3029D50D9D09914126A17B4D1F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (OnDownLeftHandler_t7E32D2BEBB53C437DA1A698E56ACDA764957164D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (OnDownRightHandler_tEBFF6E41C7963CBCDAE16698D596E3057E5A3F99), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (OnPressUpHandler_t5EE87DF384558AAFE245813FD586DA9818A33CE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (OnPressDownHandler_tE5E9A24BAC75D8565FF6D7F5FFD0E328C8C91F02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (OnPressLeftHandler_tD6AD232FE778E7A6E0A5A6D4A7F541C494EC85AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (OnPressRightHandler_tE4E38BBEF2C4F0A6293D340E4D18282DC391BBCD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494), -1, sizeof(ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3564[5] = 
{
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields::get_offset_of__instance_4(),
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494::get_offset_of_axes_5(),
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494::get_offset_of_controls_6(),
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields::get_offset_of_control_7(),
	ETCInput_tFE969393152B3F3A4F73F8B503BABD0919EE0494_StaticFields::get_offset_of_axis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3565[38] = 
{
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onMoveStart_50(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onMove_51(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onMoveSpeed_52(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onMoveEnd_53(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onTouchStart_54(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_onTouchUp_55(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnDownUp_56(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnDownDown_57(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnDownLeft_58(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnDownRight_59(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnPressUp_60(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnPressDown_61(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnPressLeft_62(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OnPressRight_63(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_joystickType_64(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_allowJoystickOverTouchPad_65(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_radiusBase_66(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_radiusBaseValue_67(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_axisX_68(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_axisY_69(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_thumb_70(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_joystickArea_71(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_userArea_72(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_isTurnAndMove_73(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmSpeed_74(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmAdditionnalRotation_75(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmMoveCurve_76(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmLockInJump_77(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmLastMove_78(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_thumbPosition_79(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_isDynamicActif_80(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_tmpAxis_81(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_OldTmpAxis_82(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_isOnTouch_83(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_isNoReturnThumb_84(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_noReturnPosition_85(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_noReturnOffset_86(),
	ETCJoystick_tC1FCFDAEE87BBD9AF0105D42A445EC3FC8EE41B9::get_offset_of_isNoOffsetThumb_87(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (OnMoveStartHandler_t93BF7CB7A5EDB9BC99F87769DD387DBD4A084F57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (OnMoveSpeedHandler_t4FA4A5C082FB5B461963C2AE7F1374093C8654EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (OnMoveHandler_t8BD7F30988E4F8AC4B38272B98ACBD1740A32E6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (OnMoveEndHandler_t5FE43EFCB773CDAED76BFA1153F06E4F47EAD671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (OnTouchStartHandler_t27951F79DE8A476D11870053691271C6BE9883C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (OnTouchUpHandler_t108B17FD19F7EE3EA4F053FFAACE81D93CF806A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (OnDownUpHandler_tC3D2AD64ADB8815FECA54F04C2B089BEEBB75E92), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (OnDownDownHandler_t332DBEEFEBCE884F11CBF292F47D8CB8EFDC6BAA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (OnDownLeftHandler_t3102AEBE0635BC6F724C1206DAA445D4E77C7754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (OnDownRightHandler_t89C4C65DE698BF06140FC071F94FADE8007BF289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (OnPressUpHandler_t2DA53FA695B966CC1205F67A4AD0A47447FE4A0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (OnPressDownHandler_t199761A0C6A223F754D8F1003519D886F2452F4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (OnPressLeftHandler_t2D68FB3D052175567C04A15DF35F95D52F3F9CE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (OnPressRightHandler_t570DBE2FD74FE8A4A70D6F52A9E7AD058AE91B8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (JoystickArea_t2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3580[11] = 
{
	JoystickArea_t2FA9A5262BDDA3F35A71FBBFEB0BB5332A619431::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (JoystickType_t2C849C7AC45C129E428F8CAC18E82EBFF0908789)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3581[3] = 
{
	JoystickType_t2C849C7AC45C129E428F8CAC18E82EBFF0908789::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (RadiusBase_t8E1E477A54EAA92094A6719E049B7A945A4ED46B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3582[4] = 
{
	RadiusBase_t8E1E477A54EAA92094A6719E049B7A945A4ED46B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[24] = 
{
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onMoveStart_50(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onMove_51(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onMoveSpeed_52(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onMoveEnd_53(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onTouchStart_54(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_onTouchUp_55(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnDownUp_56(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnDownDown_57(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnDownLeft_58(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnDownRight_59(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnPressUp_60(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnPressDown_61(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnPressLeft_62(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OnPressRight_63(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_axisX_64(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_axisY_65(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_isDPI_66(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_cachedImage_67(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_tmpAxis_68(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_OldTmpAxis_69(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_previousDargObject_70(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_isOut_71(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_isOnTouch_72(),
	ETCTouchPad_t305D6BFF501C77B8BBEA31220C3D5AE1E91C3062::get_offset_of_cachedVisible_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (OnMoveStartHandler_t9F196BE5AE08304C6D7D1B20B485E2FCD8E1C52F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (OnMoveHandler_t8A852696E18CD6E61C923170A7A75210923721D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (OnMoveSpeedHandler_tB0B9DECF77150E5A04DDF3E31081C277F18AADC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (OnMoveEndHandler_tEF560D68C74BB3CA91EC145CA31BFA026F6D63F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (OnTouchStartHandler_t76C519DE2C1AB61047DF39AFD71411888414ED09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (OnTouchUPHandler_tBD1C74ED5BCCE1BF84A6443C0B69469D632BC0D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (OnDownUpHandler_tA0188AE5248CC4DBE4D256C3D621BA3A5A028230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (OnDownDownHandler_t2333AB8FF412905EEDE71B4606A4E0ED11607238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (OnDownLeftHandler_t0A884CFFF54E7772A01938939E367E6F37B04180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (OnDownRightHandler_t308C5A100EF894D2125DF3DA2CC71647D50A015C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (OnPressUpHandler_tE7F29A9AD0AB3C09F62525557D629A8059369D17), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (OnPressDownHandler_tF8A3F6CF4DEAE985028EDDAF48E527AD33D8A0EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (OnPressLeftHandler_tE72C97A205D17B70AA104A82EEEA9F357CC39407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (OnPressRightHandler_t2AEBDCCE01BBCF41C292B7D2CE164A4C2B36858E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[3] = 
{
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_LeftText_4(),
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_RightText_5(),
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_ProgressSlider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86), -1, sizeof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3599[7] = 
{
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__7_0_3(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__8_0_4(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__9_0_5(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__10_0_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
