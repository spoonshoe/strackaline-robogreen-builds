﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BitBenderGames.DemoController
struct DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7;
// BitBenderGames.MobilePickingController
struct MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23;
// BitBenderGames.MobileTouchCamera
struct MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA;
// BitBenderGames.MobileTouchPickable
struct MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0;
// BitBenderGames.PinchUpdateData
struct PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461;
// BitBenderGames.TouchInputController
struct TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F;
// BitBenderGames.TouchInputController/DragStopDelegate
struct DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C;
// BitBenderGames.TouchInputController/DragUpdateDelegate
struct DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537;
// BitBenderGames.TouchInputController/Input1PositionDelegate
struct Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4;
// BitBenderGames.TouchInputController/InputClickDelegate
struct InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97;
// BitBenderGames.TouchInputController/InputDragStartDelegate
struct InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16;
// BitBenderGames.TouchInputController/InputLongTapProgress
struct InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE;
// BitBenderGames.TouchInputController/PinchStartDelegate
struct PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC;
// BitBenderGames.TouchInputController/PinchUpdateDelegate
struct PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89;
// BitBenderGames.TouchInputController/PinchUpdateExtendedDelegate
struct PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705;
// BitBenderGames.UnityEventWithPickableSelected
struct UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065;
// BitBenderGames.UnityEventWithPositionAndTransform
struct UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A;
// BitBenderGames.UnityEventWithRaycastHit
struct UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592;
// BitBenderGames.UnityEventWithRaycastHit2D
struct UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3;
// BitBenderGames.UnityEventWithTransform
struct UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC;
// CI.HttpClient.Core.HttpBase
struct HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F;
// CI.HttpClient.Core.IDispatcher
struct IDispatcher_t30A8FD2364C1606B2C0508ACCA9B1BD3FF8A084A;
// CI.HttpClient.HttpClient
struct HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B;
// CI.HttpClient.IHttpContent
struct IHttpContent_t88F10297CB253C6A49CFE371FDD268ACE7977D87;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95;
// HedgehogTeam.EasyTouch.QuickDrag/OnDrag
struct OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE;
// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd
struct OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE;
// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart
struct OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter
struct OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit
struct OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver
struct OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015;
// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap
struct OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E;
// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction
struct OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E;
// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction
struct OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2;
// HedgehogTeam.EasyTouch.QuickTap/OnTap
struct OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<CI.HttpClient.HttpResponseMessage>
struct Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0;
// System.Action`1<CI.HttpClient.UploadStatusMessage>
struct Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009;
// System.Action`2<System.String,System.String>
struct Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<UnityEngine.Component,UnityEngine.Vector3>
struct Dictionary_2_tB4607538378A82F04E3A9BB44C33B416BD7AA06C;
// System.Collections.Generic.Dictionary`2<UnityEngine.Renderer,System.Collections.Generic.List`1<UnityEngine.Color>>
struct Dictionary_2_tCE66378231179DDC525266B160E19AFCF7719737;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_tA30D1CD06BEF2372970C5F484BC3C035A3B71355;
// System.Collections.Generic.IList`1<CI.HttpClient.IHttpContent>
struct IList_1_tDB34EA179775000BCCEF3527239FFEA6241BD703;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver>
struct List_1_tFD5BDF82231676F2B42789D5056A999700B83F12;
// System.Collections.Generic.List`1<System.Net.HttpWebRequest>
struct List_1_tEFFCE55296BDD4933906280D907683221CBA2DF0;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.CookieContainer
struct CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73;
// System.Net.HttpWebRequest
struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0;
// System.Net.HttpWebResponse
struct HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TriLib.Samples.URIDialog
struct URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.TerrainCollider
struct TerrainCollider_t43B44EB62E016F4B076CF3A49D0C3F0EA71398C6;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF_H
#define U3CU3EC__DISPLAYCLASS21_0_TF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.DemoController_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF  : public RuntimeObject
{
public:
	// UnityEngine.Transform BitBenderGames.DemoController_<>c__DisplayClass21_0::pickableTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pickableTransform_0;

public:
	inline static int32_t get_offset_of_pickableTransform_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF, ___pickableTransform_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pickableTransform_0() const { return ___pickableTransform_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pickableTransform_0() { return &___pickableTransform_0; }
	inline void set_pickableTransform_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pickableTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___pickableTransform_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF_H
#ifndef U3CANIMATESCALEFORSELECTIONU3ED__22_TCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99_H
#define U3CANIMATESCALEFORSELECTIONU3ED__22_TCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.DemoController_<AnimateScaleForSelection>d__22
struct  U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99  : public RuntimeObject
{
public:
	// System.Int32 BitBenderGames.DemoController_<AnimateScaleForSelection>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BitBenderGames.DemoController_<AnimateScaleForSelection>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Transform BitBenderGames.DemoController_<AnimateScaleForSelection>d__22::pickableTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pickableTransform_2;
	// System.Single BitBenderGames.DemoController_<AnimateScaleForSelection>d__22::<timeAtStart>5__2
	float ___U3CtimeAtStartU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_pickableTransform_2() { return static_cast<int32_t>(offsetof(U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99, ___pickableTransform_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pickableTransform_2() const { return ___pickableTransform_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pickableTransform_2() { return &___pickableTransform_2; }
	inline void set_pickableTransform_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pickableTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___pickableTransform_2), value);
	}

	inline static int32_t get_offset_of_U3CtimeAtStartU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99, ___U3CtimeAtStartU3E5__2_3)); }
	inline float get_U3CtimeAtStartU3E5__2_3() const { return ___U3CtimeAtStartU3E5__2_3; }
	inline float* get_address_of_U3CtimeAtStartU3E5__2_3() { return &___U3CtimeAtStartU3E5__2_3; }
	inline void set_U3CtimeAtStartU3E5__2_3(float value)
	{
		___U3CtimeAtStartU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATESCALEFORSELECTIONU3ED__22_TCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99_H
#ifndef U3CHIDEINFOTEXTU3ED__36_T2BB943315A4790FC5478F0CEA4A13929D77DF119_H
#define U3CHIDEINFOTEXTU3ED__36_T2BB943315A4790FC5478F0CEA4A13929D77DF119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.DemoController_<HideInfoText>d__36
struct  U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119  : public RuntimeObject
{
public:
	// System.Int32 BitBenderGames.DemoController_<HideInfoText>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BitBenderGames.DemoController_<HideInfoText>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BitBenderGames.DemoController BitBenderGames.DemoController_<HideInfoText>d__36::<>4__this
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7 * ___U3CU3E4__this_2;
	// System.Single BitBenderGames.DemoController_<HideInfoText>d__36::delay
	float ___delay_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119, ___U3CU3E4__this_2)); }
	inline DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEINFOTEXTU3ED__36_T2BB943315A4790FC5478F0CEA4A13929D77DF119_H
#ifndef U3CU3EC_T896EB3C776F16747440A65415640F84FFA063C07_H
#define U3CU3EC_T896EB3C776F16747440A65415640F84FFA063C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobilePickingController_<>c
struct  U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields
{
public:
	// BitBenderGames.MobilePickingController_<>c BitBenderGames.MobilePickingController_<>c::<>9
	U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07 * ___U3CU3E9_0;
	// System.Predicate`1<UnityEngine.Component> BitBenderGames.MobilePickingController_<>c::<>9__67_0
	Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * ___U3CU3E9__67_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__67_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields, ___U3CU3E9__67_0_1)); }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * get_U3CU3E9__67_0_1() const { return ___U3CU3E9__67_0_1; }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 ** get_address_of_U3CU3E9__67_0_1() { return &___U3CU3E9__67_0_1; }
	inline void set_U3CU3E9__67_0_1(Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * value)
	{
		___U3CU3E9__67_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__67_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T896EB3C776F16747440A65415640F84FFA063C07_H
#ifndef U3CINITCAMBOUNDARIESDELAYEDU3ED__193_T75681A79FF8ED5CB7408E5044E84A1D60C7E622C_H
#define U3CINITCAMBOUNDARIESDELAYEDU3ED__193_T75681A79FF8ED5CB7408E5044E84A1D60C7E622C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobileTouchCamera_<InitCamBoundariesDelayed>d__193
struct  U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C  : public RuntimeObject
{
public:
	// System.Int32 BitBenderGames.MobileTouchCamera_<InitCamBoundariesDelayed>d__193::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BitBenderGames.MobileTouchCamera_<InitCamBoundariesDelayed>d__193::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BitBenderGames.MobileTouchCamera BitBenderGames.MobileTouchCamera_<InitCamBoundariesDelayed>d__193::<>4__this
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C, ___U3CU3E4__this_2)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITCAMBOUNDARIESDELAYEDU3ED__193_T75681A79FF8ED5CB7408E5044E84A1D60C7E622C_H
#ifndef U3CZOOMTOTARGETVALUECOROUTINEU3ED__230_TA833CF0CF20715DE3DB39A235AD3A368CD4A29D0_H
#define U3CZOOMTOTARGETVALUECOROUTINEU3ED__230_TA833CF0CF20715DE3DB39A235AD3A368CD4A29D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230
struct  U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0  : public RuntimeObject
{
public:
	// System.Int32 BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::target
	float ___target_2;
	// BitBenderGames.MobileTouchCamera BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::<>4__this
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___U3CU3E4__this_3;
	// System.Single BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::<startValue>5__2
	float ___U3CstartValueU3E5__2_4;
	// System.Single BitBenderGames.MobileTouchCamera_<ZoomToTargetValueCoroutine>d__230::<timeStart>5__3
	float ___U3CtimeStartU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___U3CU3E4__this_3)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CstartValueU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___U3CstartValueU3E5__2_4)); }
	inline float get_U3CstartValueU3E5__2_4() const { return ___U3CstartValueU3E5__2_4; }
	inline float* get_address_of_U3CstartValueU3E5__2_4() { return &___U3CstartValueU3E5__2_4; }
	inline void set_U3CstartValueU3E5__2_4(float value)
	{
		___U3CstartValueU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimeStartU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0, ___U3CtimeStartU3E5__3_5)); }
	inline float get_U3CtimeStartU3E5__3_5() const { return ___U3CtimeStartU3E5__3_5; }
	inline float* get_address_of_U3CtimeStartU3E5__3_5() { return &___U3CtimeStartU3E5__3_5; }
	inline void set_U3CtimeStartU3E5__3_5(float value)
	{
		___U3CtimeStartU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CZOOMTOTARGETVALUECOROUTINEU3ED__230_TA833CF0CF20715DE3DB39A235AD3A368CD4A29D0_H
#ifndef PICKABLESELECTEDDATA_TA21FF7FB7F227F172BD42758B2B00E4670FD7380_H
#define PICKABLESELECTEDDATA_TA21FF7FB7F227F172BD42758B2B00E4670FD7380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.PickableSelectedData
struct  PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380  : public RuntimeObject
{
public:
	// UnityEngine.Transform BitBenderGames.PickableSelectedData::<SelectedTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CSelectedTransformU3Ek__BackingField_0;
	// System.Boolean BitBenderGames.PickableSelectedData::<IsDoubleClick>k__BackingField
	bool ___U3CIsDoubleClickU3Ek__BackingField_1;
	// System.Boolean BitBenderGames.PickableSelectedData::<IsLongTap>k__BackingField
	bool ___U3CIsLongTapU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CSelectedTransformU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380, ___U3CSelectedTransformU3Ek__BackingField_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CSelectedTransformU3Ek__BackingField_0() const { return ___U3CSelectedTransformU3Ek__BackingField_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CSelectedTransformU3Ek__BackingField_0() { return &___U3CSelectedTransformU3Ek__BackingField_0; }
	inline void set_U3CSelectedTransformU3Ek__BackingField_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CSelectedTransformU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedTransformU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsDoubleClickU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380, ___U3CIsDoubleClickU3Ek__BackingField_1)); }
	inline bool get_U3CIsDoubleClickU3Ek__BackingField_1() const { return ___U3CIsDoubleClickU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsDoubleClickU3Ek__BackingField_1() { return &___U3CIsDoubleClickU3Ek__BackingField_1; }
	inline void set_U3CIsDoubleClickU3Ek__BackingField_1(bool value)
	{
		___U3CIsDoubleClickU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsLongTapU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380, ___U3CIsLongTapU3Ek__BackingField_2)); }
	inline bool get_U3CIsLongTapU3Ek__BackingField_2() const { return ___U3CIsLongTapU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsLongTapU3Ek__BackingField_2() { return &___U3CIsLongTapU3Ek__BackingField_2; }
	inline void set_U3CIsLongTapU3Ek__BackingField_2(bool value)
	{
		___U3CIsLongTapU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKABLESELECTEDDATA_TA21FF7FB7F227F172BD42758B2B00E4670FD7380_H
#ifndef TOUCHWRAPPER_T735E6415B5EDF72E835232B021FD08DA64D5C6AE_H
#define TOUCHWRAPPER_T735E6415B5EDF72E835232B021FD08DA64D5C6AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchWrapper
struct  TouchWrapper_t735E6415B5EDF72E835232B021FD08DA64D5C6AE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHWRAPPER_T735E6415B5EDF72E835232B021FD08DA64D5C6AE_H
#ifndef BYTEARRAYCONTENT_TAC3F769431D8BAB745350A09B4EA71FB8FBC28A6_H
#define BYTEARRAYCONTENT_TAC3F769431D8BAB745350A09B4EA71FB8FBC28A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.ByteArrayContent
struct  ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6  : public RuntimeObject
{
public:
	// System.Byte[] CI.HttpClient.ByteArrayContent::_content
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____content_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.ByteArrayContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__content_0() { return static_cast<int32_t>(offsetof(ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6, ____content_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__content_0() const { return ____content_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__content_0() { return &____content_0; }
	inline void set__content_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____content_0 = value;
		Il2CppCodeGenWriteBarrier((&____content_0), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEARRAYCONTENT_TAC3F769431D8BAB745350A09B4EA71FB8FBC28A6_H
#ifndef HTTPBASE_T134D4F33D21270D2318F2A25C285CBBF7980B92F_H
#define HTTPBASE_T134D4F33D21270D2318F2A25C285CBBF7980B92F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase
struct  HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest CI.HttpClient.Core.HttpBase::_request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ____request_0;
	// System.Net.HttpWebResponse CI.HttpClient.Core.HttpBase::_response
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * ____response_1;
	// CI.HttpClient.Core.IDispatcher CI.HttpClient.Core.HttpBase::_dispatcher
	RuntimeObject* ____dispatcher_2;
	// System.Boolean CI.HttpClient.Core.HttpBase::_isAborted
	bool ____isAborted_3;

public:
	inline static int32_t get_offset_of__request_0() { return static_cast<int32_t>(offsetof(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F, ____request_0)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get__request_0() const { return ____request_0; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of__request_0() { return &____request_0; }
	inline void set__request_0(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		____request_0 = value;
		Il2CppCodeGenWriteBarrier((&____request_0), value);
	}

	inline static int32_t get_offset_of__response_1() { return static_cast<int32_t>(offsetof(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F, ____response_1)); }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * get__response_1() const { return ____response_1; }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 ** get_address_of__response_1() { return &____response_1; }
	inline void set__response_1(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * value)
	{
		____response_1 = value;
		Il2CppCodeGenWriteBarrier((&____response_1), value);
	}

	inline static int32_t get_offset_of__dispatcher_2() { return static_cast<int32_t>(offsetof(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F, ____dispatcher_2)); }
	inline RuntimeObject* get__dispatcher_2() const { return ____dispatcher_2; }
	inline RuntimeObject** get_address_of__dispatcher_2() { return &____dispatcher_2; }
	inline void set__dispatcher_2(RuntimeObject* value)
	{
		____dispatcher_2 = value;
		Il2CppCodeGenWriteBarrier((&____dispatcher_2), value);
	}

	inline static int32_t get_offset_of__isAborted_3() { return static_cast<int32_t>(offsetof(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F, ____isAborted_3)); }
	inline bool get__isAborted_3() const { return ____isAborted_3; }
	inline bool* get_address_of__isAborted_3() { return &____isAborted_3; }
	inline void set__isAborted_3(bool value)
	{
		____isAborted_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASE_T134D4F33D21270D2318F2A25C285CBBF7980B92F_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T68597394AB3AF08DD76D930222D63E455AEB944B_H
#define U3CU3EC__DISPLAYCLASS10_0_T68597394AB3AF08DD76D930222D63E455AEB944B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B  : public RuntimeObject
{
public:
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.Core.HttpBase_<>c__DisplayClass10_0::uploadStatusCallback
	Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * ___uploadStatusCallback_0;
	// System.Int64 CI.HttpClient.Core.HttpBase_<>c__DisplayClass10_0::contentLength
	int64_t ___contentLength_1;
	// System.Int64 CI.HttpClient.Core.HttpBase_<>c__DisplayClass10_0::contentUploadedThisRound
	int64_t ___contentUploadedThisRound_2;
	// System.Int64 CI.HttpClient.Core.HttpBase_<>c__DisplayClass10_0::totalContentUploaded
	int64_t ___totalContentUploaded_3;

public:
	inline static int32_t get_offset_of_uploadStatusCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B, ___uploadStatusCallback_0)); }
	inline Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * get_uploadStatusCallback_0() const { return ___uploadStatusCallback_0; }
	inline Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 ** get_address_of_uploadStatusCallback_0() { return &___uploadStatusCallback_0; }
	inline void set_uploadStatusCallback_0(Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * value)
	{
		___uploadStatusCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_0), value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_contentUploadedThisRound_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B, ___contentUploadedThisRound_2)); }
	inline int64_t get_contentUploadedThisRound_2() const { return ___contentUploadedThisRound_2; }
	inline int64_t* get_address_of_contentUploadedThisRound_2() { return &___contentUploadedThisRound_2; }
	inline void set_contentUploadedThisRound_2(int64_t value)
	{
		___contentUploadedThisRound_2 = value;
	}

	inline static int32_t get_offset_of_totalContentUploaded_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B, ___totalContentUploaded_3)); }
	inline int64_t get_totalContentUploaded_3() const { return ___totalContentUploaded_3; }
	inline int64_t* get_address_of_totalContentUploaded_3() { return &___totalContentUploaded_3; }
	inline void set_totalContentUploaded_3(int64_t value)
	{
		___totalContentUploaded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T68597394AB3AF08DD76D930222D63E455AEB944B_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_TCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B_H
#define U3CU3EC__DISPLAYCLASS11_0_TCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B  : public RuntimeObject
{
public:
	// System.Action`1<CI.HttpClient.HttpResponseMessage> CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0::responseCallback
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___responseCallback_0;
	// System.Byte[] CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_1;
	// CI.HttpClient.Core.HttpBase CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0::<>4__this
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * ___U3CU3E4__this_2;
	// System.Int64 CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0::contentReadThisRound
	int64_t ___contentReadThisRound_3;
	// System.Int64 CI.HttpClient.Core.HttpBase_<>c__DisplayClass11_0::totalContentRead
	int64_t ___totalContentRead_4;

public:
	inline static int32_t get_offset_of_responseCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B, ___responseCallback_0)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_responseCallback_0() const { return ___responseCallback_0; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_responseCallback_0() { return &___responseCallback_0; }
	inline void set_responseCallback_0(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___responseCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B, ___data_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B, ___U3CU3E4__this_2)); }
	inline HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_contentReadThisRound_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B, ___contentReadThisRound_3)); }
	inline int64_t get_contentReadThisRound_3() const { return ___contentReadThisRound_3; }
	inline int64_t* get_address_of_contentReadThisRound_3() { return &___contentReadThisRound_3; }
	inline void set_contentReadThisRound_3(int64_t value)
	{
		___contentReadThisRound_3 = value;
	}

	inline static int32_t get_offset_of_totalContentRead_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B, ___totalContentRead_4)); }
	inline int64_t get_totalContentRead_4() const { return ___totalContentRead_4; }
	inline int64_t* get_address_of_totalContentRead_4() { return &___totalContentRead_4; }
	inline void set_totalContentRead_4(int64_t value)
	{
		___totalContentRead_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_TCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T5D7374647380736D7769A35259F24248C46B27BC_H
#define U3CU3EC__DISPLAYCLASS12_0_T5D7374647380736D7769A35259F24248C46B27BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC  : public RuntimeObject
{
public:
	// System.Action`1<CI.HttpClient.HttpResponseMessage> CI.HttpClient.Core.HttpBase_<>c__DisplayClass12_0::action
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___action_0;
	// CI.HttpClient.Core.HttpBase CI.HttpClient.Core.HttpBase_<>c__DisplayClass12_0::<>4__this
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * ___U3CU3E4__this_1;
	// System.Exception CI.HttpClient.Core.HttpBase_<>c__DisplayClass12_0::exception
	Exception_t * ___exception_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC, ___action_0)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_action_0() const { return ___action_0; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC, ___U3CU3E4__this_1)); }
	inline HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC, ___exception_2)); }
	inline Exception_t * get_exception_2() const { return ___exception_2; }
	inline Exception_t ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___exception_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T5D7374647380736D7769A35259F24248C46B27BC_H
#ifndef FORMURLENCODEDCONTENT_TA24D8893148AC8DD0134C620D07B07C81ACA063A_H
#define FORMURLENCODEDCONTENT_TA24D8893148AC8DD0134C620D07B07C81ACA063A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.FormUrlEncodedContent
struct  FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A  : public RuntimeObject
{
public:
	// System.Byte[] CI.HttpClient.FormUrlEncodedContent::_content
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____content_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.FormUrlEncodedContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__content_0() { return static_cast<int32_t>(offsetof(FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A, ____content_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__content_0() const { return ____content_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__content_0() { return &____content_0; }
	inline void set__content_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____content_0 = value;
		Il2CppCodeGenWriteBarrier((&____content_0), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMURLENCODEDCONTENT_TA24D8893148AC8DD0134C620D07B07C81ACA063A_H
#ifndef AUTHHELPER_T79E62E96D3A645A3E6F32CF77FC1F4629DB01027_H
#define AUTHHELPER_T79E62E96D3A645A3E6F32CF77FC1F4629DB01027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Helpers.AuthHelper
struct  AuthHelper_t79E62E96D3A645A3E6F32CF77FC1F4629DB01027  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHHELPER_T79E62E96D3A645A3E6F32CF77FC1F4629DB01027_H
#ifndef HTTPCLIENT_TE71BC23E214AF27FD5297528E116E86A36F6124B_H
#define HTTPCLIENT_TE71BC23E214AF27FD5297528E116E86A36F6124B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient
struct  HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B  : public RuntimeObject
{
public:
	// System.Int32 CI.HttpClient.HttpClient::<DownloadBlockSize>k__BackingField
	int32_t ___U3CDownloadBlockSizeU3Ek__BackingField_5;
	// System.Int32 CI.HttpClient.HttpClient::<UploadBlockSize>k__BackingField
	int32_t ___U3CUploadBlockSizeU3Ek__BackingField_6;
	// System.Int32 CI.HttpClient.HttpClient::<Timeout>k__BackingField
	int32_t ___U3CTimeoutU3Ek__BackingField_7;
	// System.Int32 CI.HttpClient.HttpClient::<ReadWriteTimeout>k__BackingField
	int32_t ___U3CReadWriteTimeoutU3Ek__BackingField_8;
	// System.Net.Cache.RequestCachePolicy CI.HttpClient.HttpClient::<Cache>k__BackingField
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___U3CCacheU3Ek__BackingField_9;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection CI.HttpClient.HttpClient::<Certificates>k__BackingField
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___U3CCertificatesU3Ek__BackingField_10;
	// System.Net.CookieContainer CI.HttpClient.HttpClient::<Cookies>k__BackingField
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * ___U3CCookiesU3Ek__BackingField_11;
	// System.Net.ICredentials CI.HttpClient.HttpClient::<Credentials>k__BackingField
	RuntimeObject* ___U3CCredentialsU3Ek__BackingField_12;
	// System.Boolean CI.HttpClient.HttpClient::<KeepAlive>k__BackingField
	bool ___U3CKeepAliveU3Ek__BackingField_13;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.HttpClient::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_14;
	// System.Net.IWebProxy CI.HttpClient.HttpClient::<Proxy>k__BackingField
	RuntimeObject* ___U3CProxyU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<System.Net.HttpWebRequest> CI.HttpClient.HttpClient::_requests
	List_1_tEFFCE55296BDD4933906280D907683221CBA2DF0 * ____requests_16;
	// System.Object CI.HttpClient.HttpClient::_lock
	RuntimeObject * ____lock_17;

public:
	inline static int32_t get_offset_of_U3CDownloadBlockSizeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CDownloadBlockSizeU3Ek__BackingField_5)); }
	inline int32_t get_U3CDownloadBlockSizeU3Ek__BackingField_5() const { return ___U3CDownloadBlockSizeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CDownloadBlockSizeU3Ek__BackingField_5() { return &___U3CDownloadBlockSizeU3Ek__BackingField_5; }
	inline void set_U3CDownloadBlockSizeU3Ek__BackingField_5(int32_t value)
	{
		___U3CDownloadBlockSizeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CUploadBlockSizeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CUploadBlockSizeU3Ek__BackingField_6)); }
	inline int32_t get_U3CUploadBlockSizeU3Ek__BackingField_6() const { return ___U3CUploadBlockSizeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CUploadBlockSizeU3Ek__BackingField_6() { return &___U3CUploadBlockSizeU3Ek__BackingField_6; }
	inline void set_U3CUploadBlockSizeU3Ek__BackingField_6(int32_t value)
	{
		___U3CUploadBlockSizeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CTimeoutU3Ek__BackingField_7)); }
	inline int32_t get_U3CTimeoutU3Ek__BackingField_7() const { return ___U3CTimeoutU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CTimeoutU3Ek__BackingField_7() { return &___U3CTimeoutU3Ek__BackingField_7; }
	inline void set_U3CTimeoutU3Ek__BackingField_7(int32_t value)
	{
		___U3CTimeoutU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CReadWriteTimeoutU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CReadWriteTimeoutU3Ek__BackingField_8)); }
	inline int32_t get_U3CReadWriteTimeoutU3Ek__BackingField_8() const { return ___U3CReadWriteTimeoutU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CReadWriteTimeoutU3Ek__BackingField_8() { return &___U3CReadWriteTimeoutU3Ek__BackingField_8; }
	inline void set_U3CReadWriteTimeoutU3Ek__BackingField_8(int32_t value)
	{
		___U3CReadWriteTimeoutU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCacheU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CCacheU3Ek__BackingField_9)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_U3CCacheU3Ek__BackingField_9() const { return ___U3CCacheU3Ek__BackingField_9; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_U3CCacheU3Ek__BackingField_9() { return &___U3CCacheU3Ek__BackingField_9; }
	inline void set_U3CCacheU3Ek__BackingField_9(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___U3CCacheU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCertificatesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CCertificatesU3Ek__BackingField_10)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_U3CCertificatesU3Ek__BackingField_10() const { return ___U3CCertificatesU3Ek__BackingField_10; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_U3CCertificatesU3Ek__BackingField_10() { return &___U3CCertificatesU3Ek__BackingField_10; }
	inline void set_U3CCertificatesU3Ek__BackingField_10(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___U3CCertificatesU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCertificatesU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CCookiesU3Ek__BackingField_11)); }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * get_U3CCookiesU3Ek__BackingField_11() const { return ___U3CCookiesU3Ek__BackingField_11; }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 ** get_address_of_U3CCookiesU3Ek__BackingField_11() { return &___U3CCookiesU3Ek__BackingField_11; }
	inline void set_U3CCookiesU3Ek__BackingField_11(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * value)
	{
		___U3CCookiesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CCredentialsU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CCredentialsU3Ek__BackingField_12() const { return ___U3CCredentialsU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CCredentialsU3Ek__BackingField_12() { return &___U3CCredentialsU3Ek__BackingField_12; }
	inline void set_U3CCredentialsU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CCredentialsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CKeepAliveU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CKeepAliveU3Ek__BackingField_13)); }
	inline bool get_U3CKeepAliveU3Ek__BackingField_13() const { return ___U3CKeepAliveU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CKeepAliveU3Ek__BackingField_13() { return &___U3CKeepAliveU3Ek__BackingField_13; }
	inline void set_U3CKeepAliveU3Ek__BackingField_13(bool value)
	{
		___U3CKeepAliveU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CHeadersU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_14() const { return ___U3CHeadersU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_14() { return &___U3CHeadersU3Ek__BackingField_14; }
	inline void set_U3CHeadersU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ___U3CProxyU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3CProxyU3Ek__BackingField_15() const { return ___U3CProxyU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3CProxyU3Ek__BackingField_15() { return &___U3CProxyU3Ek__BackingField_15; }
	inline void set_U3CProxyU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3CProxyU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of__requests_16() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ____requests_16)); }
	inline List_1_tEFFCE55296BDD4933906280D907683221CBA2DF0 * get__requests_16() const { return ____requests_16; }
	inline List_1_tEFFCE55296BDD4933906280D907683221CBA2DF0 ** get_address_of__requests_16() { return &____requests_16; }
	inline void set__requests_16(List_1_tEFFCE55296BDD4933906280D907683221CBA2DF0 * value)
	{
		____requests_16 = value;
		Il2CppCodeGenWriteBarrier((&____requests_16), value);
	}

	inline static int32_t get_offset_of__lock_17() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B, ____lock_17)); }
	inline RuntimeObject * get__lock_17() const { return ____lock_17; }
	inline RuntimeObject ** get_address_of__lock_17() { return &____lock_17; }
	inline void set__lock_17(RuntimeObject * value)
	{
		____lock_17 = value;
		Il2CppCodeGenWriteBarrier((&____lock_17), value);
	}
};

struct HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B_StaticFields
{
public:
	// CI.HttpClient.Core.IDispatcher CI.HttpClient.HttpClient::_dispatcher
	RuntimeObject* ____dispatcher_18;

public:
	inline static int32_t get_offset_of__dispatcher_18() { return static_cast<int32_t>(offsetof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B_StaticFields, ____dispatcher_18)); }
	inline RuntimeObject* get__dispatcher_18() const { return ____dispatcher_18; }
	inline RuntimeObject** get_address_of__dispatcher_18() { return &____dispatcher_18; }
	inline void set__dispatcher_18(RuntimeObject* value)
	{
		____dispatcher_18 = value;
		Il2CppCodeGenWriteBarrier((&____dispatcher_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENT_TE71BC23E214AF27FD5297528E116E86A36F6124B_H
#ifndef U3CU3EC__DISPLAYCLASS76_0_T621FB1FD67CC27819717BD3488E02A2DE7D5BD64_H
#define U3CU3EC__DISPLAYCLASS76_0_T621FB1FD67CC27819717BD3488E02A2DE7D5BD64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient_<>c__DisplayClass76_0
struct  U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64  : public RuntimeObject
{
public:
	// System.Action`1<CI.HttpClient.HttpResponseMessage> CI.HttpClient.HttpClient_<>c__DisplayClass76_0::action
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___action_0;
	// System.Exception CI.HttpClient.HttpClient_<>c__DisplayClass76_0::exception
	Exception_t * ___exception_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64, ___action_0)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_action_0() const { return ___action_0; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64, ___exception_1)); }
	inline Exception_t * get_exception_1() const { return ___exception_1; }
	inline Exception_t ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(Exception_t * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS76_0_T621FB1FD67CC27819717BD3488E02A2DE7D5BD64_H
#ifndef MULTIPARTCONTENT_TF49F293164C17C3D21F57F1778AD94B5E5556DF2_H
#define MULTIPARTCONTENT_TF49F293164C17C3D21F57F1778AD94B5E5556DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.MultipartContent
struct  MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<CI.HttpClient.IHttpContent> CI.HttpClient.MultipartContent::_content
	RuntimeObject* ____content_1;
	// System.String CI.HttpClient.MultipartContent::_boundary
	String_t* ____boundary_2;
	// System.Int64 CI.HttpClient.MultipartContent::_contentLength
	int64_t ____contentLength_3;
	// System.Byte[] CI.HttpClient.MultipartContent::<BoundaryStartBytes>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CBoundaryStartBytesU3Ek__BackingField_4;
	// System.Byte[] CI.HttpClient.MultipartContent::<BoundaryEndBytes>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CBoundaryEndBytesU3Ek__BackingField_5;
	// System.Byte[] CI.HttpClient.MultipartContent::<CRLFBytes>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CCRLFBytesU3Ek__BackingField_6;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.MultipartContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ____content_1)); }
	inline RuntimeObject* get__content_1() const { return ____content_1; }
	inline RuntimeObject** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(RuntimeObject* value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of__boundary_2() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ____boundary_2)); }
	inline String_t* get__boundary_2() const { return ____boundary_2; }
	inline String_t** get_address_of__boundary_2() { return &____boundary_2; }
	inline void set__boundary_2(String_t* value)
	{
		____boundary_2 = value;
		Il2CppCodeGenWriteBarrier((&____boundary_2), value);
	}

	inline static int32_t get_offset_of__contentLength_3() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ____contentLength_3)); }
	inline int64_t get__contentLength_3() const { return ____contentLength_3; }
	inline int64_t* get_address_of__contentLength_3() { return &____contentLength_3; }
	inline void set__contentLength_3(int64_t value)
	{
		____contentLength_3 = value;
	}

	inline static int32_t get_offset_of_U3CBoundaryStartBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ___U3CBoundaryStartBytesU3Ek__BackingField_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CBoundaryStartBytesU3Ek__BackingField_4() const { return ___U3CBoundaryStartBytesU3Ek__BackingField_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CBoundaryStartBytesU3Ek__BackingField_4() { return &___U3CBoundaryStartBytesU3Ek__BackingField_4; }
	inline void set_U3CBoundaryStartBytesU3Ek__BackingField_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CBoundaryStartBytesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundaryStartBytesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBoundaryEndBytesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ___U3CBoundaryEndBytesU3Ek__BackingField_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CBoundaryEndBytesU3Ek__BackingField_5() const { return ___U3CBoundaryEndBytesU3Ek__BackingField_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CBoundaryEndBytesU3Ek__BackingField_5() { return &___U3CBoundaryEndBytesU3Ek__BackingField_5; }
	inline void set_U3CBoundaryEndBytesU3Ek__BackingField_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CBoundaryEndBytesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundaryEndBytesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCRLFBytesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ___U3CCRLFBytesU3Ek__BackingField_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CCRLFBytesU3Ek__BackingField_6() const { return ___U3CCRLFBytesU3Ek__BackingField_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CCRLFBytesU3Ek__BackingField_6() { return &___U3CCRLFBytesU3Ek__BackingField_6; }
	inline void set_U3CCRLFBytesU3Ek__BackingField_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CCRLFBytesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCRLFBytesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2, ___U3CHeadersU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_7() const { return ___U3CHeadersU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_7() { return &___U3CHeadersU3Ek__BackingField_7; }
	inline void set_U3CHeadersU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPARTCONTENT_TF49F293164C17C3D21F57F1778AD94B5E5556DF2_H
#ifndef STREAMCONTENT_T487A9BB71FFA9BF5B967E712D896E8FD3BA873F8_H
#define STREAMCONTENT_T487A9BB71FFA9BF5B967E712D896E8FD3BA873F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.StreamContent
struct  StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8  : public RuntimeObject
{
public:
	// System.IO.Stream CI.HttpClient.StreamContent::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.StreamContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__stream_0() { return static_cast<int32_t>(offsetof(StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8, ____stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_0() const { return ____stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_0() { return &____stream_0; }
	inline void set__stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_0 = value;
		Il2CppCodeGenWriteBarrier((&____stream_0), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMCONTENT_T487A9BB71FFA9BF5B967E712D896E8FD3BA873F8_H
#ifndef STRINGCONTENT_TD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC_H
#define STRINGCONTENT_TD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.StringContent
struct  StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC  : public RuntimeObject
{
public:
	// System.Byte[] CI.HttpClient.StringContent::_content
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____content_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.StringContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC, ____content_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__content_1() const { return ____content_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC, ___U3CHeadersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_2() const { return ___U3CHeadersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_2() { return &___U3CHeadersU3Ek__BackingField_2; }
	inline void set_U3CHeadersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCONTENT_TD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC_H
#ifndef UPLOADSTATUSMESSAGE_T6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB_H
#define UPLOADSTATUSMESSAGE_T6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.UploadStatusMessage
struct  UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB  : public RuntimeObject
{
public:
	// System.Int64 CI.HttpClient.UploadStatusMessage::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_0;
	// System.Int64 CI.HttpClient.UploadStatusMessage::<TotalContentUploaded>k__BackingField
	int64_t ___U3CTotalContentUploadedU3Ek__BackingField_1;
	// System.Int64 CI.HttpClient.UploadStatusMessage::<ContentUploadedThisRound>k__BackingField
	int64_t ___U3CContentUploadedThisRoundU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB, ___U3CContentLengthU3Ek__BackingField_0)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_0() const { return ___U3CContentLengthU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_0() { return &___U3CContentLengthU3Ek__BackingField_0; }
	inline void set_U3CContentLengthU3Ek__BackingField_0(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTotalContentUploadedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB, ___U3CTotalContentUploadedU3Ek__BackingField_1)); }
	inline int64_t get_U3CTotalContentUploadedU3Ek__BackingField_1() const { return ___U3CTotalContentUploadedU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CTotalContentUploadedU3Ek__BackingField_1() { return &___U3CTotalContentUploadedU3Ek__BackingField_1; }
	inline void set_U3CTotalContentUploadedU3Ek__BackingField_1(int64_t value)
	{
		___U3CTotalContentUploadedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CContentUploadedThisRoundU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB, ___U3CContentUploadedThisRoundU3Ek__BackingField_2)); }
	inline int64_t get_U3CContentUploadedThisRoundU3Ek__BackingField_2() const { return ___U3CContentUploadedThisRoundU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CContentUploadedThisRoundU3Ek__BackingField_2() { return &___U3CContentUploadedThisRoundU3Ek__BackingField_2; }
	inline void set_U3CContentUploadedThisRoundU3Ek__BackingField_2(int64_t value)
	{
		___U3CContentUploadedThisRoundU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTATUSMESSAGE_T6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_TF89E810CD63A5639A152C00F97A83128C2ED8C68_H
#define U3CU3EC__DISPLAYCLASS52_0_TF89E810CD63A5639A152C00F97A83128C2ED8C68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_tF89E810CD63A5639A152C00F97A83128C2ED8C68  : public RuntimeObject
{
public:
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass52_0::triggerName
	String_t* ___triggerName_0;

public:
	inline static int32_t get_offset_of_triggerName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_tF89E810CD63A5639A152C00F97A83128C2ED8C68, ___triggerName_0)); }
	inline String_t* get_triggerName_0() const { return ___triggerName_0; }
	inline String_t** get_address_of_triggerName_0() { return &___triggerName_0; }
	inline void set_triggerName_0(String_t* value)
	{
		___triggerName_0 = value;
		Il2CppCodeGenWriteBarrier((&___triggerName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_TF89E810CD63A5639A152C00F97A83128C2ED8C68_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5_H
#define U3CU3EC__DISPLAYCLASS2_0_T3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.PersistentDataPathLoadSample_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5  : public RuntimeObject
{
public:
	// System.String TriLib.Samples.PersistentDataPathLoadSample_<>c__DisplayClass2_0::filter
	String_t* ___filter_0;

public:
	inline static int32_t get_offset_of_filter_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5, ___filter_0)); }
	inline String_t* get_filter_0() const { return ___filter_0; }
	inline String_t** get_address_of_filter_0() { return &___filter_0; }
	inline void set_filter_0(String_t* value)
	{
		___filter_0 = value;
		Il2CppCodeGenWriteBarrier((&___filter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_TCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB_H
#define U3CU3EC__DISPLAYCLASS16_0_TCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.URIDialog_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB  : public RuntimeObject
{
public:
	// System.Action`2<System.String,System.String> TriLib.Samples.URIDialog_<>c__DisplayClass16_0::onOk
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___onOk_0;
	// TriLib.Samples.URIDialog TriLib.Samples.URIDialog_<>c__DisplayClass16_0::<>4__this
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_onOk_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB, ___onOk_0)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_onOk_0() const { return ___onOk_0; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_onOk_0() { return &___onOk_0; }
	inline void set_onOk_0(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___onOk_0 = value;
		Il2CppCodeGenWriteBarrier((&___onOk_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB, ___U3CU3E4__this_1)); }
	inline URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_TCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef HTTPREQUEST_TCED431E733C32C305084D914993CF094C22A4C63_H
#define HTTPREQUEST_TCED431E733C32C305084D914993CF094C22A4C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpRequest
struct  HttpRequest_tCED431E733C32C305084D914993CF094C22A4C63  : public HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUEST_TCED431E733C32C305084D914993CF094C22A4C63_H
#ifndef MULTIPARTFORMDATACONTENT_TB1E525F0861AC6129F54CF976155B26BA5024A78_H
#define MULTIPARTFORMDATACONTENT_TB1E525F0861AC6129F54CF976155B26BA5024A78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.MultipartFormDataContent
struct  MultipartFormDataContent_tB1E525F0861AC6129F54CF976155B26BA5024A78  : public MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPARTFORMDATACONTENT_TB1E525F0861AC6129F54CF976155B26BA5024A78_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef UNITYEVENT_1_T15D85F02DC931150953C279163175D0946178A23_H
#define UNITYEVENT_1_T15D85F02DC931150953C279163175D0946178A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<BitBenderGames.PickableSelectedData>
struct  UnityEvent_1_t15D85F02DC931150953C279163175D0946178A23  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t15D85F02DC931150953C279163175D0946178A23, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T15D85F02DC931150953C279163175D0946178A23_H
#ifndef UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#define UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<HedgehogTeam.EasyTouch.Gesture>
struct  UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#ifndef UNITYEVENT_1_T9BB97BC300C0C50E6D35C14B78396DEBE0A4E536_H
#define UNITYEVENT_1_T9BB97BC300C0C50E6D35C14B78396DEBE0A4E536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.RaycastHit2D>
struct  UnityEvent_1_t9BB97BC300C0C50E6D35C14B78396DEBE0A4E536  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t9BB97BC300C0C50E6D35C14B78396DEBE0A4E536, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T9BB97BC300C0C50E6D35C14B78396DEBE0A4E536_H
#ifndef UNITYEVENT_1_T384DA615688578ABCF8BDBDB4C86807E866A8DE2_H
#define UNITYEVENT_1_T384DA615688578ABCF8BDBDB4C86807E866A8DE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.RaycastHit>
struct  UnityEvent_1_t384DA615688578ABCF8BDBDB4C86807E866A8DE2  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t384DA615688578ABCF8BDBDB4C86807E866A8DE2, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T384DA615688578ABCF8BDBDB4C86807E866A8DE2_H
#ifndef UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#define UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Transform>
struct  UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifndef UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#define UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct  UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#ifndef UNITYEVENT_2_TC667BD040665DEBC80BFF9B378153D5D83B31067_H
#define UNITYEVENT_2_TC667BD040665DEBC80BFF9B378153D5D83B31067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.Vector3,UnityEngine.Transform>
struct  UnityEvent_2_tC667BD040665DEBC80BFF9B378153D5D83B31067  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_tC667BD040665DEBC80BFF9B378153D5D83B31067, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_TC667BD040665DEBC80BFF9B378153D5D83B31067_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AUTOSCROLLDAMPMODE_T9F01A0E501AD211C7A5BF17A2603F45936F4A32C_H
#define AUTOSCROLLDAMPMODE_T9F01A0E501AD211C7A5BF17A2603F45936F4A32C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.AutoScrollDampMode
struct  AutoScrollDampMode_t9F01A0E501AD211C7A5BF17A2603F45936F4A32C 
{
public:
	// System.Int32 BitBenderGames.AutoScrollDampMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoScrollDampMode_t9F01A0E501AD211C7A5BF17A2603F45936F4A32C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSCROLLDAMPMODE_T9F01A0E501AD211C7A5BF17A2603F45936F4A32C_H
#ifndef CAMERAPLANEAXES_TF96575D4C05B3634CB666A4AC15AAF3D5BA05722_H
#define CAMERAPLANEAXES_TF96575D4C05B3634CB666A4AC15AAF3D5BA05722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.CameraPlaneAxes
struct  CameraPlaneAxes_tF96575D4C05B3634CB666A4AC15AAF3D5BA05722 
{
public:
	// System.Int32 BitBenderGames.CameraPlaneAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraPlaneAxes_tF96575D4C05B3634CB666A4AC15AAF3D5BA05722, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPLANEAXES_TF96575D4C05B3634CB666A4AC15AAF3D5BA05722_H
#ifndef SELECTIONACTION_T9FB56D0010219A67C1BE52DA01004D577C57F84E_H
#define SELECTIONACTION_T9FB56D0010219A67C1BE52DA01004D577C57F84E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobilePickingController_SelectionAction
struct  SelectionAction_t9FB56D0010219A67C1BE52DA01004D577C57F84E 
{
public:
	// System.Int32 BitBenderGames.MobilePickingController_SelectionAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionAction_t9FB56D0010219A67C1BE52DA01004D577C57F84E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONACTION_T9FB56D0010219A67C1BE52DA01004D577C57F84E_H
#ifndef PERSPECTIVEZOOMMODE_T43EB411EFFC8F799FA908FBD5E73847A5DF31170_H
#define PERSPECTIVEZOOMMODE_T43EB411EFFC8F799FA908FBD5E73847A5DF31170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.PerspectiveZoomMode
struct  PerspectiveZoomMode_t43EB411EFFC8F799FA908FBD5E73847A5DF31170 
{
public:
	// System.Int32 BitBenderGames.PerspectiveZoomMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PerspectiveZoomMode_t43EB411EFFC8F799FA908FBD5E73847A5DF31170, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSPECTIVEZOOMMODE_T43EB411EFFC8F799FA908FBD5E73847A5DF31170_H
#ifndef PINCHUPDATEDATA_T14D520537978BF81B94B44BBAFA898D35B3C8461_H
#define PINCHUPDATEDATA_T14D520537978BF81B94B44BBAFA898D35B3C8461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.PinchUpdateData
struct  PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 BitBenderGames.PinchUpdateData::pinchCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchCenter_0;
	// System.Single BitBenderGames.PinchUpdateData::pinchDistance
	float ___pinchDistance_1;
	// System.Single BitBenderGames.PinchUpdateData::pinchStartDistance
	float ___pinchStartDistance_2;
	// System.Single BitBenderGames.PinchUpdateData::pinchAngleDelta
	float ___pinchAngleDelta_3;
	// System.Single BitBenderGames.PinchUpdateData::pinchAngleDeltaNormalized
	float ___pinchAngleDeltaNormalized_4;
	// System.Single BitBenderGames.PinchUpdateData::pinchTiltDelta
	float ___pinchTiltDelta_5;
	// System.Single BitBenderGames.PinchUpdateData::pinchTotalFingerMovement
	float ___pinchTotalFingerMovement_6;

public:
	inline static int32_t get_offset_of_pinchCenter_0() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchCenter_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchCenter_0() const { return ___pinchCenter_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchCenter_0() { return &___pinchCenter_0; }
	inline void set_pinchCenter_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchCenter_0 = value;
	}

	inline static int32_t get_offset_of_pinchDistance_1() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchDistance_1)); }
	inline float get_pinchDistance_1() const { return ___pinchDistance_1; }
	inline float* get_address_of_pinchDistance_1() { return &___pinchDistance_1; }
	inline void set_pinchDistance_1(float value)
	{
		___pinchDistance_1 = value;
	}

	inline static int32_t get_offset_of_pinchStartDistance_2() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchStartDistance_2)); }
	inline float get_pinchStartDistance_2() const { return ___pinchStartDistance_2; }
	inline float* get_address_of_pinchStartDistance_2() { return &___pinchStartDistance_2; }
	inline void set_pinchStartDistance_2(float value)
	{
		___pinchStartDistance_2 = value;
	}

	inline static int32_t get_offset_of_pinchAngleDelta_3() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchAngleDelta_3)); }
	inline float get_pinchAngleDelta_3() const { return ___pinchAngleDelta_3; }
	inline float* get_address_of_pinchAngleDelta_3() { return &___pinchAngleDelta_3; }
	inline void set_pinchAngleDelta_3(float value)
	{
		___pinchAngleDelta_3 = value;
	}

	inline static int32_t get_offset_of_pinchAngleDeltaNormalized_4() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchAngleDeltaNormalized_4)); }
	inline float get_pinchAngleDeltaNormalized_4() const { return ___pinchAngleDeltaNormalized_4; }
	inline float* get_address_of_pinchAngleDeltaNormalized_4() { return &___pinchAngleDeltaNormalized_4; }
	inline void set_pinchAngleDeltaNormalized_4(float value)
	{
		___pinchAngleDeltaNormalized_4 = value;
	}

	inline static int32_t get_offset_of_pinchTiltDelta_5() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchTiltDelta_5)); }
	inline float get_pinchTiltDelta_5() const { return ___pinchTiltDelta_5; }
	inline float* get_address_of_pinchTiltDelta_5() { return &___pinchTiltDelta_5; }
	inline void set_pinchTiltDelta_5(float value)
	{
		___pinchTiltDelta_5 = value;
	}

	inline static int32_t get_offset_of_pinchTotalFingerMovement_6() { return static_cast<int32_t>(offsetof(PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461, ___pinchTotalFingerMovement_6)); }
	inline float get_pinchTotalFingerMovement_6() const { return ___pinchTotalFingerMovement_6; }
	inline float* get_address_of_pinchTotalFingerMovement_6() { return &___pinchTotalFingerMovement_6; }
	inline void set_pinchTotalFingerMovement_6(float value)
	{
		___pinchTotalFingerMovement_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHUPDATEDATA_T14D520537978BF81B94B44BBAFA898D35B3C8461_H
#ifndef SNAPANGLE_TAB59888ECAA0E909F67C357D9855D6B46646CFE7_H
#define SNAPANGLE_TAB59888ECAA0E909F67C357D9855D6B46646CFE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.SnapAngle
struct  SnapAngle_tAB59888ECAA0E909F67C357D9855D6B46646CFE7 
{
public:
	// System.Int32 BitBenderGames.SnapAngle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SnapAngle_tAB59888ECAA0E909F67C357D9855D6B46646CFE7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPANGLE_TAB59888ECAA0E909F67C357D9855D6B46646CFE7_H
#ifndef UNITYEVENTWITHPICKABLESELECTED_TE36560AEAF1EE51B726C764AB3D3D664F6873065_H
#define UNITYEVENTWITHPICKABLESELECTED_TE36560AEAF1EE51B726C764AB3D3D664F6873065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithPickableSelected
struct  UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065  : public UnityEvent_1_t15D85F02DC931150953C279163175D0946178A23
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHPICKABLESELECTED_TE36560AEAF1EE51B726C764AB3D3D664F6873065_H
#ifndef UNITYEVENTWITHPOSITION_T4413F53299D18ABD6AD10EC21BAF530093DC17AD_H
#define UNITYEVENTWITHPOSITION_T4413F53299D18ABD6AD10EC21BAF530093DC17AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithPosition
struct  UnityEventWithPosition_t4413F53299D18ABD6AD10EC21BAF530093DC17AD  : public UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHPOSITION_T4413F53299D18ABD6AD10EC21BAF530093DC17AD_H
#ifndef UNITYEVENTWITHPOSITIONANDTRANSFORM_T5A134C4683FF1B0C634507E103728B4EB88FA68A_H
#define UNITYEVENTWITHPOSITIONANDTRANSFORM_T5A134C4683FF1B0C634507E103728B4EB88FA68A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithPositionAndTransform
struct  UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A  : public UnityEvent_2_tC667BD040665DEBC80BFF9B378153D5D83B31067
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHPOSITIONANDTRANSFORM_T5A134C4683FF1B0C634507E103728B4EB88FA68A_H
#ifndef UNITYEVENTWITHRAYCASTHIT_TDDC5434A72791FF90F808456B4D7E75CDBCF2592_H
#define UNITYEVENTWITHRAYCASTHIT_TDDC5434A72791FF90F808456B4D7E75CDBCF2592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithRaycastHit
struct  UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592  : public UnityEvent_1_t384DA615688578ABCF8BDBDB4C86807E866A8DE2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHRAYCASTHIT_TDDC5434A72791FF90F808456B4D7E75CDBCF2592_H
#ifndef UNITYEVENTWITHRAYCASTHIT2D_TA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3_H
#define UNITYEVENTWITHRAYCASTHIT2D_TA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithRaycastHit2D
struct  UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3  : public UnityEvent_1_t9BB97BC300C0C50E6D35C14B78396DEBE0A4E536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHRAYCASTHIT2D_TA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3_H
#ifndef UNITYEVENTWITHTRANSFORM_T474E1B585A08145BCC41B1D77A4993500FA3DFBC_H
#define UNITYEVENTWITHTRANSFORM_T474E1B585A08145BCC41B1D77A4993500FA3DFBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.UnityEventWithTransform
struct  UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC  : public UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTWITHTRANSFORM_T474E1B585A08145BCC41B1D77A4993500FA3DFBC_H
#ifndef WRAPPEDTOUCH_TD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5_H
#define WRAPPEDTOUCH_TD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.WrappedTouch
struct  WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 BitBenderGames.WrappedTouch::<Position>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionU3Ek__BackingField_0;
	// System.Int32 BitBenderGames.WrappedTouch::<FingerId>k__BackingField
	int32_t ___U3CFingerIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5, ___U3CPositionU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionU3Ek__BackingField_0() const { return ___U3CPositionU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionU3Ek__BackingField_0() { return &___U3CPositionU3Ek__BackingField_0; }
	inline void set_U3CPositionU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFingerIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5, ___U3CFingerIdU3Ek__BackingField_1)); }
	inline int32_t get_U3CFingerIdU3Ek__BackingField_1() const { return ___U3CFingerIdU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CFingerIdU3Ek__BackingField_1() { return &___U3CFingerIdU3Ek__BackingField_1; }
	inline void set_U3CFingerIdU3Ek__BackingField_1(int32_t value)
	{
		___U3CFingerIdU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPEDTOUCH_TD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5_H
#ifndef CONTENTREADACTION_TEBE208A49881B95D2B1F43A50183E53F3823A8B8_H
#define CONTENTREADACTION_TEBE208A49881B95D2B1F43A50183E53F3823A8B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.ContentReadAction
struct  ContentReadAction_tEBE208A49881B95D2B1F43A50183E53F3823A8B8 
{
public:
	// System.Int32 CI.HttpClient.ContentReadAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentReadAction_tEBE208A49881B95D2B1F43A50183E53F3823A8B8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTREADACTION_TEBE208A49881B95D2B1F43A50183E53F3823A8B8_H
#ifndef HTTPACTION_T90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33_H
#define HTTPACTION_T90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpAction
struct  HttpAction_t90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33 
{
public:
	// System.Int32 CI.HttpClient.HttpAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpAction_t90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPACTION_T90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33_H
#ifndef HTTPCOMPLETIONOPTION_T3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A_H
#define HTTPCOMPLETIONOPTION_T3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpCompletionOption
struct  HttpCompletionOption_t3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A 
{
public:
	// System.Int32 CI.HttpClient.HttpCompletionOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpCompletionOption_t3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCOMPLETIONOPTION_T3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A_H
#ifndef EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#define EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_EvtType
struct  EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_EvtType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifndef ETTPARAMETER_TA7CEE31494A885F86856295E4F565A16444048D5_H
#define ETTPARAMETER_TA7CEE31494A885F86856295E4F565A16444048D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTParameter
struct  ETTParameter_tA7CEE31494A885F86856295E4F565A16444048D5 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTParameter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ETTParameter_tA7CEE31494A885F86856295E4F565A16444048D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETTPARAMETER_TA7CEE31494A885F86856295E4F565A16444048D5_H
#ifndef ETTTYPE_T1AAC6C2F14C4C027796AB307B6567174BA3C810C_H
#define ETTTYPE_T1AAC6C2F14C4C027796AB307B6567174BA3C810C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTType
struct  ETTType_t1AAC6C2F14C4C027796AB307B6567174BA3C810C 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ETTType_t1AAC6C2F14C4C027796AB307B6567174BA3C810C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETTTYPE_T1AAC6C2F14C4C027796AB307B6567174BA3C810C_H
#ifndef AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#define AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction
struct  AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#ifndef DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#define DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_DirectAction
struct  DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_DirectAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#ifndef GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#define GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_GameObjectType
struct  GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_GameObjectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#ifndef ONDRAG_T5744CBA4D301B97D65C67AC86B6C79417163A7AE_H
#define ONDRAG_T5744CBA4D301B97D65C67AC86B6C79417163A7AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag_OnDrag
struct  OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAG_T5744CBA4D301B97D65C67AC86B6C79417163A7AE_H
#ifndef ONDRAGEND_T667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE_H
#define ONDRAGEND_T667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag_OnDragEnd
struct  OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGEND_T667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE_H
#ifndef ONDRAGSTART_T0F16E4F16CF819945A7EFB4AC7FD99554C39D780_H
#define ONDRAGSTART_T0F16E4F16CF819945A7EFB4AC7FD99554C39D780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag_OnDragStart
struct  OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGSTART_T0F16E4F16CF819945A7EFB4AC7FD99554C39D780_H
#ifndef ONTOUCHENTER_T11475E4F03EFF70EBD9A46545ABD66C549E72A13_H
#define ONTOUCHENTER_T11475E4F03EFF70EBD9A46545ABD66C549E72A13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchEnter
struct  OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHENTER_T11475E4F03EFF70EBD9A46545ABD66C549E72A13_H
#ifndef ONTOUCHEXIT_TD9FC8D0EA2607B6F6D48585337B09D1F29A367BF_H
#define ONTOUCHEXIT_TD9FC8D0EA2607B6F6D48585337B09D1F29A367BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchExit
struct  OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHEXIT_TD9FC8D0EA2607B6F6D48585337B09D1F29A367BF_H
#ifndef ONTOUCHOVER_T5BB647ED24F68374C13CC58F8C31E890994E4015_H
#define ONTOUCHOVER_T5BB647ED24F68374C13CC58F8C31E890994E4015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchOver
struct  OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHOVER_T5BB647ED24F68374C13CC58F8C31E890994E4015_H
#ifndef ACTIONTRIGGERING_TD6CFD0829085966F2AB0EA03654FD5AB98695A03_H
#define ACTIONTRIGGERING_TD6CFD0829085966F2AB0EA03654FD5AB98695A03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap_ActionTriggering
struct  ActionTriggering_tD6CFD0829085966F2AB0EA03654FD5AB98695A03 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickLongTap_ActionTriggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTriggering_tD6CFD0829085966F2AB0EA03654FD5AB98695A03, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_TD6CFD0829085966F2AB0EA03654FD5AB98695A03_H
#ifndef ONLONGTAP_T6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E_H
#define ONLONGTAP_T6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap_OnLongTap
struct  OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONLONGTAP_T6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E_H
#ifndef ACTIONPINCHDIRECTION_T252F13A292F55C11A79DE3DD185EA11F005CE205_H
#define ACTIONPINCHDIRECTION_T252F13A292F55C11A79DE3DD185EA11F005CE205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch_ActionPinchDirection
struct  ActionPinchDirection_t252F13A292F55C11A79DE3DD185EA11F005CE205 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickPinch_ActionPinchDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionPinchDirection_t252F13A292F55C11A79DE3DD185EA11F005CE205, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONPINCHDIRECTION_T252F13A292F55C11A79DE3DD185EA11F005CE205_H
#ifndef ACTIONTIGGERING_T4E8FC47EF05E529F01227F41B3D518343F2AE83B_H
#define ACTIONTIGGERING_T4E8FC47EF05E529F01227F41B3D518343F2AE83B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch_ActionTiggering
struct  ActionTiggering_t4E8FC47EF05E529F01227F41B3D518343F2AE83B 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickPinch_ActionTiggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTiggering_t4E8FC47EF05E529F01227F41B3D518343F2AE83B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTIGGERING_T4E8FC47EF05E529F01227F41B3D518343F2AE83B_H
#ifndef ONPINCHACTION_T58779DCC888555EC219734341FDC7BF0FA65CE9E_H
#define ONPINCHACTION_T58779DCC888555EC219734341FDC7BF0FA65CE9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch_OnPinchAction
struct  OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPINCHACTION_T58779DCC888555EC219734341FDC7BF0FA65CE9E_H
#ifndef ACTIONTRIGGERING_TF4F78C81B3285CD6653C4506E7114BB1CAFFBD55_H
#define ACTIONTRIGGERING_TF4F78C81B3285CD6653C4506E7114BB1CAFFBD55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe_ActionTriggering
struct  ActionTriggering_tF4F78C81B3285CD6653C4506E7114BB1CAFFBD55 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe_ActionTriggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTriggering_tF4F78C81B3285CD6653C4506E7114BB1CAFFBD55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_TF4F78C81B3285CD6653C4506E7114BB1CAFFBD55_H
#ifndef ONSWIPEACTION_T11487A29B7C37B95564F396860E38777EB15DCA2_H
#define ONSWIPEACTION_T11487A29B7C37B95564F396860E38777EB15DCA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe_OnSwipeAction
struct  OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSWIPEACTION_T11487A29B7C37B95564F396860E38777EB15DCA2_H
#ifndef SWIPEDIRECTION_TF235422ED045B6A596630C0B48DEB2DB3FBE4831_H
#define SWIPEDIRECTION_TF235422ED045B6A596630C0B48DEB2DB3FBE4831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe_SwipeDirection
struct  SwipeDirection_tF235422ED045B6A596630C0B48DEB2DB3FBE4831 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe_SwipeDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeDirection_tF235422ED045B6A596630C0B48DEB2DB3FBE4831, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_TF235422ED045B6A596630C0B48DEB2DB3FBE4831_H
#ifndef ACTIONTRIGGERING_T15EB815254A99EBF572283FC6C7C58EDEEC8FF7D_H
#define ACTIONTRIGGERING_T15EB815254A99EBF572283FC6C7C58EDEEC8FF7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap_ActionTriggering
struct  ActionTriggering_t15EB815254A99EBF572283FC6C7C58EDEEC8FF7D 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTap_ActionTriggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTriggering_t15EB815254A99EBF572283FC6C7C58EDEEC8FF7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T15EB815254A99EBF572283FC6C7C58EDEEC8FF7D_H
#ifndef ONTAP_T219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8_H
#define ONTAP_T219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap_OnTap
struct  OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTAP_T219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#define HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#define PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Normal_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#ifndef U3CU3EC__DISPLAYCLASS61_0_T772E221EFE096B0C2FE6867432EDD0BAF8463EA1_H
#define U3CU3EC__DISPLAYCLASS61_0_T772E221EFE096B0C2FE6867432EDD0BAF8463EA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient_<>c__DisplayClass61_0
struct  U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1  : public RuntimeObject
{
public:
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient_<>c__DisplayClass61_0::<>4__this
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * ___U3CU3E4__this_0;
	// System.Uri CI.HttpClient.HttpClient_<>c__DisplayClass61_0::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_1;
	// CI.HttpClient.HttpAction CI.HttpClient.HttpClient_<>c__DisplayClass61_0::httpAction
	int32_t ___httpAction_2;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient_<>c__DisplayClass61_0::httpCompletionOption
	int32_t ___httpCompletionOption_3;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> CI.HttpClient.HttpClient_<>c__DisplayClass61_0::responseCallback
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___responseCallback_4;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1, ___U3CU3E4__this_0)); }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1, ___uri_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_1() const { return ___uri_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_httpAction_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1, ___httpAction_2)); }
	inline int32_t get_httpAction_2() const { return ___httpAction_2; }
	inline int32_t* get_address_of_httpAction_2() { return &___httpAction_2; }
	inline void set_httpAction_2(int32_t value)
	{
		___httpAction_2 = value;
	}

	inline static int32_t get_offset_of_httpCompletionOption_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1, ___httpCompletionOption_3)); }
	inline int32_t get_httpCompletionOption_3() const { return ___httpCompletionOption_3; }
	inline int32_t* get_address_of_httpCompletionOption_3() { return &___httpCompletionOption_3; }
	inline void set_httpCompletionOption_3(int32_t value)
	{
		___httpCompletionOption_3 = value;
	}

	inline static int32_t get_offset_of_responseCallback_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1, ___responseCallback_4)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_responseCallback_4() const { return ___responseCallback_4; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_responseCallback_4() { return &___responseCallback_4; }
	inline void set_responseCallback_4(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___responseCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS61_0_T772E221EFE096B0C2FE6867432EDD0BAF8463EA1_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T4E2E873F287B2240382B2877ACDFB62098D4B354_H
#define U3CU3EC__DISPLAYCLASS62_0_T4E2E873F287B2240382B2877ACDFB62098D4B354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient_<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354  : public RuntimeObject
{
public:
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient_<>c__DisplayClass62_0::<>4__this
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * ___U3CU3E4__this_0;
	// System.Uri CI.HttpClient.HttpClient_<>c__DisplayClass62_0::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_1;
	// CI.HttpClient.HttpAction CI.HttpClient.HttpClient_<>c__DisplayClass62_0::httpAction
	int32_t ___httpAction_2;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient_<>c__DisplayClass62_0::httpContent
	RuntimeObject* ___httpContent_3;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient_<>c__DisplayClass62_0::httpCompletionOption
	int32_t ___httpCompletionOption_4;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> CI.HttpClient.HttpClient_<>c__DisplayClass62_0::responseCallback
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___responseCallback_5;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient_<>c__DisplayClass62_0::uploadStatusCallback
	Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * ___uploadStatusCallback_6;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___U3CU3E4__this_0)); }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___uri_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_1() const { return ___uri_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_httpAction_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___httpAction_2)); }
	inline int32_t get_httpAction_2() const { return ___httpAction_2; }
	inline int32_t* get_address_of_httpAction_2() { return &___httpAction_2; }
	inline void set_httpAction_2(int32_t value)
	{
		___httpAction_2 = value;
	}

	inline static int32_t get_offset_of_httpContent_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___httpContent_3)); }
	inline RuntimeObject* get_httpContent_3() const { return ___httpContent_3; }
	inline RuntimeObject** get_address_of_httpContent_3() { return &___httpContent_3; }
	inline void set_httpContent_3(RuntimeObject* value)
	{
		___httpContent_3 = value;
		Il2CppCodeGenWriteBarrier((&___httpContent_3), value);
	}

	inline static int32_t get_offset_of_httpCompletionOption_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___httpCompletionOption_4)); }
	inline int32_t get_httpCompletionOption_4() const { return ___httpCompletionOption_4; }
	inline int32_t* get_address_of_httpCompletionOption_4() { return &___httpCompletionOption_4; }
	inline void set_httpCompletionOption_4(int32_t value)
	{
		___httpCompletionOption_4 = value;
	}

	inline static int32_t get_offset_of_responseCallback_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___responseCallback_5)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_responseCallback_5() const { return ___responseCallback_5; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_responseCallback_5() { return &___responseCallback_5; }
	inline void set_responseCallback_5(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___responseCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_5), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354, ___uploadStatusCallback_6)); }
	inline Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * get_uploadStatusCallback_6() const { return ___uploadStatusCallback_6; }
	inline Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 ** get_address_of_uploadStatusCallback_6() { return &___uploadStatusCallback_6; }
	inline void set_uploadStatusCallback_6(Action_1_t6C741B5DE2F5C8D2FB9C89DF1B6D1D164480B009 * value)
	{
		___uploadStatusCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T4E2E873F287B2240382B2877ACDFB62098D4B354_H
#ifndef HTTPRESPONSEMESSAGE_T641F1D1CE60AB20D41014DE899199E10673FA048_H
#define HTTPRESPONSEMESSAGE_T641F1D1CE60AB20D41014DE899199E10673FA048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpResponseMessage
struct  HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest CI.HttpClient.HttpResponseMessage::<OriginalRequest>k__BackingField
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___U3COriginalRequestU3Ek__BackingField_0;
	// System.Net.HttpWebResponse CI.HttpClient.HttpResponseMessage::<OriginalResponse>k__BackingField
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * ___U3COriginalResponseU3Ek__BackingField_1;
	// System.Int64 CI.HttpClient.HttpResponseMessage::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_2;
	// System.Int64 CI.HttpClient.HttpResponseMessage::<TotalContentRead>k__BackingField
	int64_t ___U3CTotalContentReadU3Ek__BackingField_3;
	// System.Int64 CI.HttpClient.HttpResponseMessage::<ContentReadThisRound>k__BackingField
	int64_t ___U3CContentReadThisRoundU3Ek__BackingField_4;
	// System.Net.HttpStatusCode CI.HttpClient.HttpResponseMessage::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_5;
	// System.String CI.HttpClient.HttpResponseMessage::<ReasonPhrase>k__BackingField
	String_t* ___U3CReasonPhraseU3Ek__BackingField_6;
	// System.Exception CI.HttpClient.HttpResponseMessage::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_7;
	// System.Byte[] CI.HttpClient.HttpResponseMessage::_responseData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____responseData_8;

public:
	inline static int32_t get_offset_of_U3COriginalRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3COriginalRequestU3Ek__BackingField_0)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_U3COriginalRequestU3Ek__BackingField_0() const { return ___U3COriginalRequestU3Ek__BackingField_0; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_U3COriginalRequestU3Ek__BackingField_0() { return &___U3COriginalRequestU3Ek__BackingField_0; }
	inline void set_U3COriginalRequestU3Ek__BackingField_0(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___U3COriginalRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalRequestU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COriginalResponseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3COriginalResponseU3Ek__BackingField_1)); }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * get_U3COriginalResponseU3Ek__BackingField_1() const { return ___U3COriginalResponseU3Ek__BackingField_1; }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 ** get_address_of_U3COriginalResponseU3Ek__BackingField_1() { return &___U3COriginalResponseU3Ek__BackingField_1; }
	inline void set_U3COriginalResponseU3Ek__BackingField_1(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * value)
	{
		___U3COriginalResponseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalResponseU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CContentLengthU3Ek__BackingField_2)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_2() const { return ___U3CContentLengthU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_2() { return &___U3CContentLengthU3Ek__BackingField_2; }
	inline void set_U3CContentLengthU3Ek__BackingField_2(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTotalContentReadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CTotalContentReadU3Ek__BackingField_3)); }
	inline int64_t get_U3CTotalContentReadU3Ek__BackingField_3() const { return ___U3CTotalContentReadU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CTotalContentReadU3Ek__BackingField_3() { return &___U3CTotalContentReadU3Ek__BackingField_3; }
	inline void set_U3CTotalContentReadU3Ek__BackingField_3(int64_t value)
	{
		___U3CTotalContentReadU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CContentReadThisRoundU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CContentReadThisRoundU3Ek__BackingField_4)); }
	inline int64_t get_U3CContentReadThisRoundU3Ek__BackingField_4() const { return ___U3CContentReadThisRoundU3Ek__BackingField_4; }
	inline int64_t* get_address_of_U3CContentReadThisRoundU3Ek__BackingField_4() { return &___U3CContentReadThisRoundU3Ek__BackingField_4; }
	inline void set_U3CContentReadThisRoundU3Ek__BackingField_4(int64_t value)
	{
		___U3CContentReadThisRoundU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CStatusCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_5() const { return ___U3CStatusCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_5() { return &___U3CStatusCodeU3Ek__BackingField_5; }
	inline void set_U3CStatusCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CReasonPhraseU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CReasonPhraseU3Ek__BackingField_6)); }
	inline String_t* get_U3CReasonPhraseU3Ek__BackingField_6() const { return ___U3CReasonPhraseU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CReasonPhraseU3Ek__BackingField_6() { return &___U3CReasonPhraseU3Ek__BackingField_6; }
	inline void set_U3CReasonPhraseU3Ek__BackingField_6(String_t* value)
	{
		___U3CReasonPhraseU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReasonPhraseU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ___U3CExceptionU3Ek__BackingField_7)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_7() const { return ___U3CExceptionU3Ek__BackingField_7; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_7() { return &___U3CExceptionU3Ek__BackingField_7; }
	inline void set_U3CExceptionU3Ek__BackingField_7(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of__responseData_8() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048, ____responseData_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__responseData_8() const { return ____responseData_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__responseData_8() { return &____responseData_8; }
	inline void set__responseData_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____responseData_8 = value;
		Il2CppCodeGenWriteBarrier((&____responseData_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEMESSAGE_T641F1D1CE60AB20D41014DE899199E10673FA048_H
#ifndef U3CU3EC__DISPLAYCLASS51_0_TBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315_H
#define U3CU3EC__DISPLAYCLASS51_0_TBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass51_0
struct  U3CU3Ec__DisplayClass51_0_tBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass51_0::evnt
	int32_t ___evnt_0;

public:
	inline static int32_t get_offset_of_evnt_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_tBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315, ___evnt_0)); }
	inline int32_t get_evnt_0() const { return ___evnt_0; }
	inline int32_t* get_address_of_evnt_0() { return &___evnt_0; }
	inline void set_evnt_0(int32_t value)
	{
		___evnt_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_0_TBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315_H
#ifndef EASYTOUCHRECEIVER_T112D590291D8B19809918CE0E2A0F23DA76CF9B2_H
#define EASYTOUCHRECEIVER_T112D590291D8B19809918CE0E2A0F23DA76CF9B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver
struct  EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2  : public RuntimeObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::enable
	bool ___enable_0;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTType HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::triggerType
	int32_t ___triggerType_1;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::name
	String_t* ___name_2;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::restricted
	bool ___restricted_3;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_4;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::otherReceiver
	bool ___otherReceiver_5;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::gameObjectReceiver
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObjectReceiver_6;
	// HedgehogTeam.EasyTouch.EasyTouch_EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::eventName
	int32_t ___eventName_7;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::methodName
	String_t* ___methodName_8;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger_ETTParameter HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::parameter
	int32_t ___parameter_9;

public:
	inline static int32_t get_offset_of_enable_0() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___enable_0)); }
	inline bool get_enable_0() const { return ___enable_0; }
	inline bool* get_address_of_enable_0() { return &___enable_0; }
	inline void set_enable_0(bool value)
	{
		___enable_0 = value;
	}

	inline static int32_t get_offset_of_triggerType_1() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___triggerType_1)); }
	inline int32_t get_triggerType_1() const { return ___triggerType_1; }
	inline int32_t* get_address_of_triggerType_1() { return &___triggerType_1; }
	inline void set_triggerType_1(int32_t value)
	{
		___triggerType_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_restricted_3() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___restricted_3)); }
	inline bool get_restricted_3() const { return ___restricted_3; }
	inline bool* get_address_of_restricted_3() { return &___restricted_3; }
	inline void set_restricted_3(bool value)
	{
		___restricted_3 = value;
	}

	inline static int32_t get_offset_of_gameObject_4() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___gameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_4() const { return ___gameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_4() { return &___gameObject_4; }
	inline void set_gameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_4), value);
	}

	inline static int32_t get_offset_of_otherReceiver_5() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___otherReceiver_5)); }
	inline bool get_otherReceiver_5() const { return ___otherReceiver_5; }
	inline bool* get_address_of_otherReceiver_5() { return &___otherReceiver_5; }
	inline void set_otherReceiver_5(bool value)
	{
		___otherReceiver_5 = value;
	}

	inline static int32_t get_offset_of_gameObjectReceiver_6() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___gameObjectReceiver_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObjectReceiver_6() const { return ___gameObjectReceiver_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObjectReceiver_6() { return &___gameObjectReceiver_6; }
	inline void set_gameObjectReceiver_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObjectReceiver_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectReceiver_6), value);
	}

	inline static int32_t get_offset_of_eventName_7() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___eventName_7)); }
	inline int32_t get_eventName_7() const { return ___eventName_7; }
	inline int32_t* get_address_of_eventName_7() { return &___eventName_7; }
	inline void set_eventName_7(int32_t value)
	{
		___eventName_7 = value;
	}

	inline static int32_t get_offset_of_methodName_8() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___methodName_8)); }
	inline String_t* get_methodName_8() const { return ___methodName_8; }
	inline String_t** get_address_of_methodName_8() { return &___methodName_8; }
	inline void set_methodName_8(String_t* value)
	{
		___methodName_8 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_8), value);
	}

	inline static int32_t get_offset_of_parameter_9() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2, ___parameter_9)); }
	inline int32_t get_parameter_9() const { return ___parameter_9; }
	inline int32_t* get_address_of_parameter_9() { return &___parameter_9; }
	inline void set_parameter_9(int32_t value)
	{
		___parameter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHRECEIVER_T112D590291D8B19809918CE0E2A0F23DA76CF9B2_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef DRAGSTOPDELEGATE_T16A1112266381CFD6AAA96BEE6B822F175B8746C_H
#define DRAGSTOPDELEGATE_T16A1112266381CFD6AAA96BEE6B822F175B8746C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_DragStopDelegate
struct  DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTOPDELEGATE_T16A1112266381CFD6AAA96BEE6B822F175B8746C_H
#ifndef DRAGUPDATEDELEGATE_TD967552922861FA20056601230CDE6123279F537_H
#define DRAGUPDATEDELEGATE_TD967552922861FA20056601230CDE6123279F537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_DragUpdateDelegate
struct  DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGUPDATEDELEGATE_TD967552922861FA20056601230CDE6123279F537_H
#ifndef INPUT1POSITIONDELEGATE_T22E991C6B2947150FE4D989AC84D147BCD8DE7E4_H
#define INPUT1POSITIONDELEGATE_T22E991C6B2947150FE4D989AC84D147BCD8DE7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_Input1PositionDelegate
struct  Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT1POSITIONDELEGATE_T22E991C6B2947150FE4D989AC84D147BCD8DE7E4_H
#ifndef INPUTCLICKDELEGATE_T5C42A7A098B870B3119F9A297A19D171EB1A0A97_H
#define INPUTCLICKDELEGATE_T5C42A7A098B870B3119F9A297A19D171EB1A0A97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_InputClickDelegate
struct  InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCLICKDELEGATE_T5C42A7A098B870B3119F9A297A19D171EB1A0A97_H
#ifndef INPUTDRAGSTARTDELEGATE_TBE096D32B580C003B19074979FC3EDC03CED4C16_H
#define INPUTDRAGSTARTDELEGATE_TBE096D32B580C003B19074979FC3EDC03CED4C16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_InputDragStartDelegate
struct  InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTDRAGSTARTDELEGATE_TBE096D32B580C003B19074979FC3EDC03CED4C16_H
#ifndef INPUTLONGTAPPROGRESS_T3A3325EB0C9A0EF2878BF5422F12413E7E2976EE_H
#define INPUTLONGTAPPROGRESS_T3A3325EB0C9A0EF2878BF5422F12413E7E2976EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_InputLongTapProgress
struct  InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTLONGTAPPROGRESS_T3A3325EB0C9A0EF2878BF5422F12413E7E2976EE_H
#ifndef PINCHSTARTDELEGATE_TD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC_H
#define PINCHSTARTDELEGATE_TD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_PinchStartDelegate
struct  PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHSTARTDELEGATE_TD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC_H
#ifndef PINCHUPDATEDELEGATE_TF07698247DFFC91E8AD87A86383DD0CDB357AE89_H
#define PINCHUPDATEDELEGATE_TF07698247DFFC91E8AD87A86383DD0CDB357AE89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_PinchUpdateDelegate
struct  PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHUPDATEDELEGATE_TF07698247DFFC91E8AD87A86383DD0CDB357AE89_H
#ifndef PINCHUPDATEEXTENDEDDELEGATE_T9D4584B65110D350046FB428CA104A641F32A705_H
#define PINCHUPDATEEXTENDEDDELEGATE_T9D4584B65110D350046FB428CA104A641F32A705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController_PinchUpdateExtendedDelegate
struct  PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHUPDATEEXTENDEDDELEGATE_T9D4584B65110D350046FB428CA104A641F32A705_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DEMOCONTROLLER_T35B229880F71530728AC85176AD73B8EA0A212E7_H
#define DEMOCONTROLLER_T35B229880F71530728AC85176AD73B8EA0A212E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.DemoController
struct  DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BitBenderGames.DemoController::textInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textInfo_4;
	// UnityEngine.UI.Text BitBenderGames.DemoController::textDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textDetail_5;
	// BitBenderGames.TouchInputController BitBenderGames.DemoController::touchInputController
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * ___touchInputController_6;
	// BitBenderGames.MobileTouchCamera BitBenderGames.DemoController::mobileTouchCamera
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___mobileTouchCamera_7;
	// BitBenderGames.MobilePickingController BitBenderGames.DemoController::mobilePickingController
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23 * ___mobilePickingController_8;
	// UnityEngine.Camera BitBenderGames.DemoController::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_9;
	// UnityEngine.Coroutine BitBenderGames.DemoController::coroutineHideInfoText
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___coroutineHideInfoText_10;
	// UnityEngine.Transform BitBenderGames.DemoController::selectedPickableTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___selectedPickableTransform_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Renderer,System.Collections.Generic.List`1<UnityEngine.Color>> BitBenderGames.DemoController::originalItemColorCache
	Dictionary_2_tCE66378231179DDC525266B160E19AFCF7719737 * ___originalItemColorCache_12;
	// System.Single BitBenderGames.DemoController::introTextOnScreenTime
	float ___introTextOnScreenTime_13;

public:
	inline static int32_t get_offset_of_textInfo_4() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___textInfo_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textInfo_4() const { return ___textInfo_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textInfo_4() { return &___textInfo_4; }
	inline void set_textInfo_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_4), value);
	}

	inline static int32_t get_offset_of_textDetail_5() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___textDetail_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textDetail_5() const { return ___textDetail_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textDetail_5() { return &___textDetail_5; }
	inline void set_textDetail_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textDetail_5 = value;
		Il2CppCodeGenWriteBarrier((&___textDetail_5), value);
	}

	inline static int32_t get_offset_of_touchInputController_6() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___touchInputController_6)); }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * get_touchInputController_6() const { return ___touchInputController_6; }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F ** get_address_of_touchInputController_6() { return &___touchInputController_6; }
	inline void set_touchInputController_6(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * value)
	{
		___touchInputController_6 = value;
		Il2CppCodeGenWriteBarrier((&___touchInputController_6), value);
	}

	inline static int32_t get_offset_of_mobileTouchCamera_7() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___mobileTouchCamera_7)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_mobileTouchCamera_7() const { return ___mobileTouchCamera_7; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_mobileTouchCamera_7() { return &___mobileTouchCamera_7; }
	inline void set_mobileTouchCamera_7(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___mobileTouchCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mobileTouchCamera_7), value);
	}

	inline static int32_t get_offset_of_mobilePickingController_8() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___mobilePickingController_8)); }
	inline MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23 * get_mobilePickingController_8() const { return ___mobilePickingController_8; }
	inline MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23 ** get_address_of_mobilePickingController_8() { return &___mobilePickingController_8; }
	inline void set_mobilePickingController_8(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23 * value)
	{
		___mobilePickingController_8 = value;
		Il2CppCodeGenWriteBarrier((&___mobilePickingController_8), value);
	}

	inline static int32_t get_offset_of_cam_9() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___cam_9)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_9() const { return ___cam_9; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_9() { return &___cam_9; }
	inline void set_cam_9(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_9 = value;
		Il2CppCodeGenWriteBarrier((&___cam_9), value);
	}

	inline static int32_t get_offset_of_coroutineHideInfoText_10() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___coroutineHideInfoText_10)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_coroutineHideInfoText_10() const { return ___coroutineHideInfoText_10; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_coroutineHideInfoText_10() { return &___coroutineHideInfoText_10; }
	inline void set_coroutineHideInfoText_10(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___coroutineHideInfoText_10 = value;
		Il2CppCodeGenWriteBarrier((&___coroutineHideInfoText_10), value);
	}

	inline static int32_t get_offset_of_selectedPickableTransform_11() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___selectedPickableTransform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_selectedPickableTransform_11() const { return ___selectedPickableTransform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_selectedPickableTransform_11() { return &___selectedPickableTransform_11; }
	inline void set_selectedPickableTransform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___selectedPickableTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___selectedPickableTransform_11), value);
	}

	inline static int32_t get_offset_of_originalItemColorCache_12() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___originalItemColorCache_12)); }
	inline Dictionary_2_tCE66378231179DDC525266B160E19AFCF7719737 * get_originalItemColorCache_12() const { return ___originalItemColorCache_12; }
	inline Dictionary_2_tCE66378231179DDC525266B160E19AFCF7719737 ** get_address_of_originalItemColorCache_12() { return &___originalItemColorCache_12; }
	inline void set_originalItemColorCache_12(Dictionary_2_tCE66378231179DDC525266B160E19AFCF7719737 * value)
	{
		___originalItemColorCache_12 = value;
		Il2CppCodeGenWriteBarrier((&___originalItemColorCache_12), value);
	}

	inline static int32_t get_offset_of_introTextOnScreenTime_13() { return static_cast<int32_t>(offsetof(DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7, ___introTextOnScreenTime_13)); }
	inline float get_introTextOnScreenTime_13() const { return ___introTextOnScreenTime_13; }
	inline float* get_address_of_introTextOnScreenTime_13() { return &___introTextOnScreenTime_13; }
	inline void set_introTextOnScreenTime_13(float value)
	{
		___introTextOnScreenTime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOCONTROLLER_T35B229880F71530728AC85176AD73B8EA0A212E7_H
#ifndef MOBILEPICKINGCONTROLLER_T4E149E133DA10FBBB1FBC8012678A860E7C42C23_H
#define MOBILEPICKINGCONTROLLER_T4E149E133DA10FBBB1FBC8012678A860E7C42C23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobilePickingController
struct  MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean BitBenderGames.MobilePickingController::snapToGrid
	bool ___snapToGrid_4;
	// System.Single BitBenderGames.MobilePickingController::snapUnitSize
	float ___snapUnitSize_5;
	// UnityEngine.Vector2 BitBenderGames.MobilePickingController::snapOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___snapOffset_6;
	// BitBenderGames.SnapAngle BitBenderGames.MobilePickingController::snapAngle
	int32_t ___snapAngle_7;
	// System.Boolean BitBenderGames.MobilePickingController::isMultiSelectionEnabled
	bool ___isMultiSelectionEnabled_8;
	// System.Boolean BitBenderGames.MobilePickingController::requireLongTapForMove
	bool ___requireLongTapForMove_9;
	// BitBenderGames.UnityEventWithTransform BitBenderGames.MobilePickingController::OnPickableTransformSelected
	UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * ___OnPickableTransformSelected_10;
	// BitBenderGames.UnityEventWithPickableSelected BitBenderGames.MobilePickingController::OnPickableTransformSelectedExtended
	UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065 * ___OnPickableTransformSelectedExtended_11;
	// BitBenderGames.UnityEventWithTransform BitBenderGames.MobilePickingController::OnPickableTransformDeselected
	UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * ___OnPickableTransformDeselected_12;
	// BitBenderGames.UnityEventWithTransform BitBenderGames.MobilePickingController::OnPickableTransformMoveStarted
	UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * ___OnPickableTransformMoveStarted_13;
	// BitBenderGames.UnityEventWithTransform BitBenderGames.MobilePickingController::OnPickableTransformMoved
	UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * ___OnPickableTransformMoved_14;
	// BitBenderGames.UnityEventWithPositionAndTransform BitBenderGames.MobilePickingController::OnPickableTransformMoveEnded
	UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A * ___OnPickableTransformMoveEnded_15;
	// System.Boolean BitBenderGames.MobilePickingController::expertModeEnabled
	bool ___expertModeEnabled_16;
	// System.Boolean BitBenderGames.MobilePickingController::deselectPreviousColliderOnClick
	bool ___deselectPreviousColliderOnClick_17;
	// System.Boolean BitBenderGames.MobilePickingController::repeatEventSelectedOnClick
	bool ___repeatEventSelectedOnClick_18;
	// System.Boolean BitBenderGames.MobilePickingController::useLegacyTransformMovedEventOrder
	bool ___useLegacyTransformMovedEventOrder_19;
	// BitBenderGames.TouchInputController BitBenderGames.MobilePickingController::touchInputController
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * ___touchInputController_20;
	// BitBenderGames.MobileTouchCamera BitBenderGames.MobilePickingController::mobileTouchCam
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___mobileTouchCam_21;
	// System.Collections.Generic.List`1<UnityEngine.Component> BitBenderGames.MobilePickingController::<SelectedColliders>k__BackingField
	List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * ___U3CSelectedCollidersU3Ek__BackingField_22;
	// System.Boolean BitBenderGames.MobilePickingController::isSelectedViaLongTap
	bool ___isSelectedViaLongTap_23;
	// BitBenderGames.MobileTouchPickable BitBenderGames.MobilePickingController::<CurrentlyDraggedPickable>k__BackingField
	MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0 * ___U3CCurrentlyDraggedPickableU3Ek__BackingField_24;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::draggedTransformOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___draggedTransformOffset_25;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::draggedTransformHeightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___draggedTransformHeightOffset_26;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::draggedItemCustomOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___draggedItemCustomOffset_27;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::currentlyDraggedTransformPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentlyDraggedTransformPosition_29;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::currentDragStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentDragStartPos_31;
	// System.Boolean BitBenderGames.MobilePickingController::invokeMoveStartedOnDrag
	bool ___invokeMoveStartedOnDrag_32;
	// System.Boolean BitBenderGames.MobilePickingController::invokeMoveEndedOnDrag
	bool ___invokeMoveEndedOnDrag_33;
	// UnityEngine.Vector3 BitBenderGames.MobilePickingController::itemInitialDragOffsetWorld
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___itemInitialDragOffsetWorld_34;
	// System.Boolean BitBenderGames.MobilePickingController::isManualSelectionRequest
	bool ___isManualSelectionRequest_35;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Component,UnityEngine.Vector3> BitBenderGames.MobilePickingController::selectionPositionOffsets
	Dictionary_2_tB4607538378A82F04E3A9BB44C33B416BD7AA06C * ___selectionPositionOffsets_36;

public:
	inline static int32_t get_offset_of_snapToGrid_4() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___snapToGrid_4)); }
	inline bool get_snapToGrid_4() const { return ___snapToGrid_4; }
	inline bool* get_address_of_snapToGrid_4() { return &___snapToGrid_4; }
	inline void set_snapToGrid_4(bool value)
	{
		___snapToGrid_4 = value;
	}

	inline static int32_t get_offset_of_snapUnitSize_5() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___snapUnitSize_5)); }
	inline float get_snapUnitSize_5() const { return ___snapUnitSize_5; }
	inline float* get_address_of_snapUnitSize_5() { return &___snapUnitSize_5; }
	inline void set_snapUnitSize_5(float value)
	{
		___snapUnitSize_5 = value;
	}

	inline static int32_t get_offset_of_snapOffset_6() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___snapOffset_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_snapOffset_6() const { return ___snapOffset_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_snapOffset_6() { return &___snapOffset_6; }
	inline void set_snapOffset_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___snapOffset_6 = value;
	}

	inline static int32_t get_offset_of_snapAngle_7() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___snapAngle_7)); }
	inline int32_t get_snapAngle_7() const { return ___snapAngle_7; }
	inline int32_t* get_address_of_snapAngle_7() { return &___snapAngle_7; }
	inline void set_snapAngle_7(int32_t value)
	{
		___snapAngle_7 = value;
	}

	inline static int32_t get_offset_of_isMultiSelectionEnabled_8() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___isMultiSelectionEnabled_8)); }
	inline bool get_isMultiSelectionEnabled_8() const { return ___isMultiSelectionEnabled_8; }
	inline bool* get_address_of_isMultiSelectionEnabled_8() { return &___isMultiSelectionEnabled_8; }
	inline void set_isMultiSelectionEnabled_8(bool value)
	{
		___isMultiSelectionEnabled_8 = value;
	}

	inline static int32_t get_offset_of_requireLongTapForMove_9() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___requireLongTapForMove_9)); }
	inline bool get_requireLongTapForMove_9() const { return ___requireLongTapForMove_9; }
	inline bool* get_address_of_requireLongTapForMove_9() { return &___requireLongTapForMove_9; }
	inline void set_requireLongTapForMove_9(bool value)
	{
		___requireLongTapForMove_9 = value;
	}

	inline static int32_t get_offset_of_OnPickableTransformSelected_10() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformSelected_10)); }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * get_OnPickableTransformSelected_10() const { return ___OnPickableTransformSelected_10; }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC ** get_address_of_OnPickableTransformSelected_10() { return &___OnPickableTransformSelected_10; }
	inline void set_OnPickableTransformSelected_10(UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * value)
	{
		___OnPickableTransformSelected_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformSelected_10), value);
	}

	inline static int32_t get_offset_of_OnPickableTransformSelectedExtended_11() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformSelectedExtended_11)); }
	inline UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065 * get_OnPickableTransformSelectedExtended_11() const { return ___OnPickableTransformSelectedExtended_11; }
	inline UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065 ** get_address_of_OnPickableTransformSelectedExtended_11() { return &___OnPickableTransformSelectedExtended_11; }
	inline void set_OnPickableTransformSelectedExtended_11(UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065 * value)
	{
		___OnPickableTransformSelectedExtended_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformSelectedExtended_11), value);
	}

	inline static int32_t get_offset_of_OnPickableTransformDeselected_12() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformDeselected_12)); }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * get_OnPickableTransformDeselected_12() const { return ___OnPickableTransformDeselected_12; }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC ** get_address_of_OnPickableTransformDeselected_12() { return &___OnPickableTransformDeselected_12; }
	inline void set_OnPickableTransformDeselected_12(UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * value)
	{
		___OnPickableTransformDeselected_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformDeselected_12), value);
	}

	inline static int32_t get_offset_of_OnPickableTransformMoveStarted_13() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformMoveStarted_13)); }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * get_OnPickableTransformMoveStarted_13() const { return ___OnPickableTransformMoveStarted_13; }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC ** get_address_of_OnPickableTransformMoveStarted_13() { return &___OnPickableTransformMoveStarted_13; }
	inline void set_OnPickableTransformMoveStarted_13(UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * value)
	{
		___OnPickableTransformMoveStarted_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformMoveStarted_13), value);
	}

	inline static int32_t get_offset_of_OnPickableTransformMoved_14() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformMoved_14)); }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * get_OnPickableTransformMoved_14() const { return ___OnPickableTransformMoved_14; }
	inline UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC ** get_address_of_OnPickableTransformMoved_14() { return &___OnPickableTransformMoved_14; }
	inline void set_OnPickableTransformMoved_14(UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC * value)
	{
		___OnPickableTransformMoved_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformMoved_14), value);
	}

	inline static int32_t get_offset_of_OnPickableTransformMoveEnded_15() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___OnPickableTransformMoveEnded_15)); }
	inline UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A * get_OnPickableTransformMoveEnded_15() const { return ___OnPickableTransformMoveEnded_15; }
	inline UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A ** get_address_of_OnPickableTransformMoveEnded_15() { return &___OnPickableTransformMoveEnded_15; }
	inline void set_OnPickableTransformMoveEnded_15(UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A * value)
	{
		___OnPickableTransformMoveEnded_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickableTransformMoveEnded_15), value);
	}

	inline static int32_t get_offset_of_expertModeEnabled_16() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___expertModeEnabled_16)); }
	inline bool get_expertModeEnabled_16() const { return ___expertModeEnabled_16; }
	inline bool* get_address_of_expertModeEnabled_16() { return &___expertModeEnabled_16; }
	inline void set_expertModeEnabled_16(bool value)
	{
		___expertModeEnabled_16 = value;
	}

	inline static int32_t get_offset_of_deselectPreviousColliderOnClick_17() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___deselectPreviousColliderOnClick_17)); }
	inline bool get_deselectPreviousColliderOnClick_17() const { return ___deselectPreviousColliderOnClick_17; }
	inline bool* get_address_of_deselectPreviousColliderOnClick_17() { return &___deselectPreviousColliderOnClick_17; }
	inline void set_deselectPreviousColliderOnClick_17(bool value)
	{
		___deselectPreviousColliderOnClick_17 = value;
	}

	inline static int32_t get_offset_of_repeatEventSelectedOnClick_18() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___repeatEventSelectedOnClick_18)); }
	inline bool get_repeatEventSelectedOnClick_18() const { return ___repeatEventSelectedOnClick_18; }
	inline bool* get_address_of_repeatEventSelectedOnClick_18() { return &___repeatEventSelectedOnClick_18; }
	inline void set_repeatEventSelectedOnClick_18(bool value)
	{
		___repeatEventSelectedOnClick_18 = value;
	}

	inline static int32_t get_offset_of_useLegacyTransformMovedEventOrder_19() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___useLegacyTransformMovedEventOrder_19)); }
	inline bool get_useLegacyTransformMovedEventOrder_19() const { return ___useLegacyTransformMovedEventOrder_19; }
	inline bool* get_address_of_useLegacyTransformMovedEventOrder_19() { return &___useLegacyTransformMovedEventOrder_19; }
	inline void set_useLegacyTransformMovedEventOrder_19(bool value)
	{
		___useLegacyTransformMovedEventOrder_19 = value;
	}

	inline static int32_t get_offset_of_touchInputController_20() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___touchInputController_20)); }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * get_touchInputController_20() const { return ___touchInputController_20; }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F ** get_address_of_touchInputController_20() { return &___touchInputController_20; }
	inline void set_touchInputController_20(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * value)
	{
		___touchInputController_20 = value;
		Il2CppCodeGenWriteBarrier((&___touchInputController_20), value);
	}

	inline static int32_t get_offset_of_mobileTouchCam_21() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___mobileTouchCam_21)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_mobileTouchCam_21() const { return ___mobileTouchCam_21; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_mobileTouchCam_21() { return &___mobileTouchCam_21; }
	inline void set_mobileTouchCam_21(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___mobileTouchCam_21 = value;
		Il2CppCodeGenWriteBarrier((&___mobileTouchCam_21), value);
	}

	inline static int32_t get_offset_of_U3CSelectedCollidersU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___U3CSelectedCollidersU3Ek__BackingField_22)); }
	inline List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * get_U3CSelectedCollidersU3Ek__BackingField_22() const { return ___U3CSelectedCollidersU3Ek__BackingField_22; }
	inline List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 ** get_address_of_U3CSelectedCollidersU3Ek__BackingField_22() { return &___U3CSelectedCollidersU3Ek__BackingField_22; }
	inline void set_U3CSelectedCollidersU3Ek__BackingField_22(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * value)
	{
		___U3CSelectedCollidersU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedCollidersU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_isSelectedViaLongTap_23() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___isSelectedViaLongTap_23)); }
	inline bool get_isSelectedViaLongTap_23() const { return ___isSelectedViaLongTap_23; }
	inline bool* get_address_of_isSelectedViaLongTap_23() { return &___isSelectedViaLongTap_23; }
	inline void set_isSelectedViaLongTap_23(bool value)
	{
		___isSelectedViaLongTap_23 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentlyDraggedPickableU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___U3CCurrentlyDraggedPickableU3Ek__BackingField_24)); }
	inline MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0 * get_U3CCurrentlyDraggedPickableU3Ek__BackingField_24() const { return ___U3CCurrentlyDraggedPickableU3Ek__BackingField_24; }
	inline MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0 ** get_address_of_U3CCurrentlyDraggedPickableU3Ek__BackingField_24() { return &___U3CCurrentlyDraggedPickableU3Ek__BackingField_24; }
	inline void set_U3CCurrentlyDraggedPickableU3Ek__BackingField_24(MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0 * value)
	{
		___U3CCurrentlyDraggedPickableU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentlyDraggedPickableU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_draggedTransformOffset_25() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___draggedTransformOffset_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_draggedTransformOffset_25() const { return ___draggedTransformOffset_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_draggedTransformOffset_25() { return &___draggedTransformOffset_25; }
	inline void set_draggedTransformOffset_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___draggedTransformOffset_25 = value;
	}

	inline static int32_t get_offset_of_draggedTransformHeightOffset_26() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___draggedTransformHeightOffset_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_draggedTransformHeightOffset_26() const { return ___draggedTransformHeightOffset_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_draggedTransformHeightOffset_26() { return &___draggedTransformHeightOffset_26; }
	inline void set_draggedTransformHeightOffset_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___draggedTransformHeightOffset_26 = value;
	}

	inline static int32_t get_offset_of_draggedItemCustomOffset_27() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___draggedItemCustomOffset_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_draggedItemCustomOffset_27() const { return ___draggedItemCustomOffset_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_draggedItemCustomOffset_27() { return &___draggedItemCustomOffset_27; }
	inline void set_draggedItemCustomOffset_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___draggedItemCustomOffset_27 = value;
	}

	inline static int32_t get_offset_of_currentlyDraggedTransformPosition_29() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___currentlyDraggedTransformPosition_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentlyDraggedTransformPosition_29() const { return ___currentlyDraggedTransformPosition_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentlyDraggedTransformPosition_29() { return &___currentlyDraggedTransformPosition_29; }
	inline void set_currentlyDraggedTransformPosition_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentlyDraggedTransformPosition_29 = value;
	}

	inline static int32_t get_offset_of_currentDragStartPos_31() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___currentDragStartPos_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentDragStartPos_31() const { return ___currentDragStartPos_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentDragStartPos_31() { return &___currentDragStartPos_31; }
	inline void set_currentDragStartPos_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentDragStartPos_31 = value;
	}

	inline static int32_t get_offset_of_invokeMoveStartedOnDrag_32() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___invokeMoveStartedOnDrag_32)); }
	inline bool get_invokeMoveStartedOnDrag_32() const { return ___invokeMoveStartedOnDrag_32; }
	inline bool* get_address_of_invokeMoveStartedOnDrag_32() { return &___invokeMoveStartedOnDrag_32; }
	inline void set_invokeMoveStartedOnDrag_32(bool value)
	{
		___invokeMoveStartedOnDrag_32 = value;
	}

	inline static int32_t get_offset_of_invokeMoveEndedOnDrag_33() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___invokeMoveEndedOnDrag_33)); }
	inline bool get_invokeMoveEndedOnDrag_33() const { return ___invokeMoveEndedOnDrag_33; }
	inline bool* get_address_of_invokeMoveEndedOnDrag_33() { return &___invokeMoveEndedOnDrag_33; }
	inline void set_invokeMoveEndedOnDrag_33(bool value)
	{
		___invokeMoveEndedOnDrag_33 = value;
	}

	inline static int32_t get_offset_of_itemInitialDragOffsetWorld_34() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___itemInitialDragOffsetWorld_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_itemInitialDragOffsetWorld_34() const { return ___itemInitialDragOffsetWorld_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_itemInitialDragOffsetWorld_34() { return &___itemInitialDragOffsetWorld_34; }
	inline void set_itemInitialDragOffsetWorld_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___itemInitialDragOffsetWorld_34 = value;
	}

	inline static int32_t get_offset_of_isManualSelectionRequest_35() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___isManualSelectionRequest_35)); }
	inline bool get_isManualSelectionRequest_35() const { return ___isManualSelectionRequest_35; }
	inline bool* get_address_of_isManualSelectionRequest_35() { return &___isManualSelectionRequest_35; }
	inline void set_isManualSelectionRequest_35(bool value)
	{
		___isManualSelectionRequest_35 = value;
	}

	inline static int32_t get_offset_of_selectionPositionOffsets_36() { return static_cast<int32_t>(offsetof(MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23, ___selectionPositionOffsets_36)); }
	inline Dictionary_2_tB4607538378A82F04E3A9BB44C33B416BD7AA06C * get_selectionPositionOffsets_36() const { return ___selectionPositionOffsets_36; }
	inline Dictionary_2_tB4607538378A82F04E3A9BB44C33B416BD7AA06C ** get_address_of_selectionPositionOffsets_36() { return &___selectionPositionOffsets_36; }
	inline void set_selectionPositionOffsets_36(Dictionary_2_tB4607538378A82F04E3A9BB44C33B416BD7AA06C * value)
	{
		___selectionPositionOffsets_36 = value;
		Il2CppCodeGenWriteBarrier((&___selectionPositionOffsets_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEPICKINGCONTROLLER_T4E149E133DA10FBBB1FBC8012678A860E7C42C23_H
#ifndef MOBILETOUCHPICKABLE_TF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_H
#define MOBILETOUCHPICKABLE_TF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobileTouchPickable
struct  MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform BitBenderGames.MobileTouchPickable::pickableTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pickableTransform_5;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchPickable::localSnapOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___localSnapOffset_6;

public:
	inline static int32_t get_offset_of_pickableTransform_5() { return static_cast<int32_t>(offsetof(MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0, ___pickableTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pickableTransform_5() const { return ___pickableTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pickableTransform_5() { return &___pickableTransform_5; }
	inline void set_pickableTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pickableTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___pickableTransform_5), value);
	}

	inline static int32_t get_offset_of_localSnapOffset_6() { return static_cast<int32_t>(offsetof(MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0, ___localSnapOffset_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_localSnapOffset_6() const { return ___localSnapOffset_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_localSnapOffset_6() { return &___localSnapOffset_6; }
	inline void set_localSnapOffset_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___localSnapOffset_6 = value;
	}
};

struct MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_StaticFields
{
public:
	// BitBenderGames.MobileTouchCamera BitBenderGames.MobileTouchPickable::mobileTouchCam
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___mobileTouchCam_4;

public:
	inline static int32_t get_offset_of_mobileTouchCam_4() { return static_cast<int32_t>(offsetof(MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_StaticFields, ___mobileTouchCam_4)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_mobileTouchCam_4() const { return ___mobileTouchCam_4; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_mobileTouchCam_4() { return &___mobileTouchCam_4; }
	inline void set_mobileTouchCam_4(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___mobileTouchCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___mobileTouchCam_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILETOUCHPICKABLE_TF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_H
#ifndef MONOBEHAVIOURWRAPPED_T123979EA34849E9323414C01CB9C7B96AB5BBC02_H
#define MONOBEHAVIOURWRAPPED_T123979EA34849E9323414C01CB9C7B96AB5BBC02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MonoBehaviourWrapped
struct  MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform BitBenderGames.MonoBehaviourWrapped::cachedTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cachedTransform_4;
	// UnityEngine.GameObject BitBenderGames.MonoBehaviourWrapped::cachedGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cachedGO_5;

public:
	inline static int32_t get_offset_of_cachedTransform_4() { return static_cast<int32_t>(offsetof(MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02, ___cachedTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cachedTransform_4() const { return ___cachedTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cachedTransform_4() { return &___cachedTransform_4; }
	inline void set_cachedTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cachedTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_4), value);
	}

	inline static int32_t get_offset_of_cachedGO_5() { return static_cast<int32_t>(offsetof(MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02, ___cachedGO_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cachedGO_5() const { return ___cachedGO_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cachedGO_5() { return &___cachedGO_5; }
	inline void set_cachedGO_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cachedGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGO_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURWRAPPED_T123979EA34849E9323414C01CB9C7B96AB5BBC02_H
#ifndef SETBOUNDARYFROMCOLLIDER_T6EF27C0922E2C08F3085C1216561346AA9FB56EB_H
#define SETBOUNDARYFROMCOLLIDER_T6EF27C0922E2C08F3085C1216561346AA9FB56EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.SetBoundaryFromCollider
struct  SetBoundaryFromCollider_t6EF27C0922E2C08F3085C1216561346AA9FB56EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.BoxCollider BitBenderGames.SetBoundaryFromCollider::boxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___boxCollider_4;

public:
	inline static int32_t get_offset_of_boxCollider_4() { return static_cast<int32_t>(offsetof(SetBoundaryFromCollider_t6EF27C0922E2C08F3085C1216561346AA9FB56EB, ___boxCollider_4)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_boxCollider_4() const { return ___boxCollider_4; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_boxCollider_4() { return &___boxCollider_4; }
	inline void set_boxCollider_4(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___boxCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___boxCollider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETBOUNDARYFROMCOLLIDER_T6EF27C0922E2C08F3085C1216561346AA9FB56EB_H
#ifndef TOUCHINPUTCONTROLLER_T4A8C8C170434264465AFFEEC42C00CB66609331F_H
#define TOUCHINPUTCONTROLLER_T4A8C8C170434264465AFFEEC42C00CB66609331F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.TouchInputController
struct  TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean BitBenderGames.TouchInputController::expertModeEnabled
	bool ___expertModeEnabled_4;
	// System.Single BitBenderGames.TouchInputController::clickDurationThreshold
	float ___clickDurationThreshold_5;
	// System.Single BitBenderGames.TouchInputController::doubleclickDurationThreshold
	float ___doubleclickDurationThreshold_6;
	// System.Single BitBenderGames.TouchInputController::tiltMoveDotTreshold
	float ___tiltMoveDotTreshold_7;
	// System.Single BitBenderGames.TouchInputController::tiltHorizontalDotThreshold
	float ___tiltHorizontalDotThreshold_8;
	// System.Single BitBenderGames.TouchInputController::dragStartDistanceThresholdRelative
	float ___dragStartDistanceThresholdRelative_9;
	// System.Boolean BitBenderGames.TouchInputController::longTapStartsDrag
	bool ___longTapStartsDrag_10;
	// System.Single BitBenderGames.TouchInputController::lastFingerDownTimeReal
	float ___lastFingerDownTimeReal_11;
	// System.Single BitBenderGames.TouchInputController::lastClickTimeReal
	float ___lastClickTimeReal_12;
	// System.Boolean BitBenderGames.TouchInputController::wasFingerDownLastFrame
	bool ___wasFingerDownLastFrame_13;
	// UnityEngine.Vector3 BitBenderGames.TouchInputController::lastFinger0DownPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastFinger0DownPos_14;
	// System.Boolean BitBenderGames.TouchInputController::isDragging
	bool ___isDragging_16;
	// UnityEngine.Vector3 BitBenderGames.TouchInputController::dragStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dragStartPos_17;
	// UnityEngine.Vector3 BitBenderGames.TouchInputController::dragStartOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dragStartOffset_18;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BitBenderGames.TouchInputController::<DragFinalMomentumVector>k__BackingField
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___U3CDragFinalMomentumVectorU3Ek__BackingField_19;
	// System.Single BitBenderGames.TouchInputController::pinchStartDistance
	float ___pinchStartDistance_21;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BitBenderGames.TouchInputController::pinchStartPositions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___pinchStartPositions_22;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BitBenderGames.TouchInputController::touchPositionLastFrame
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___touchPositionLastFrame_23;
	// UnityEngine.Vector3 BitBenderGames.TouchInputController::pinchRotationVectorStart
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchRotationVectorStart_24;
	// UnityEngine.Vector3 BitBenderGames.TouchInputController::pinchVectorLastFrame
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchVectorLastFrame_25;
	// System.Single BitBenderGames.TouchInputController::totalFingerMovement
	float ___totalFingerMovement_26;
	// System.Boolean BitBenderGames.TouchInputController::wasDraggingLastFrame
	bool ___wasDraggingLastFrame_27;
	// System.Boolean BitBenderGames.TouchInputController::wasPinchingLastFrame
	bool ___wasPinchingLastFrame_28;
	// System.Boolean BitBenderGames.TouchInputController::isPinching
	bool ___isPinching_29;
	// System.Boolean BitBenderGames.TouchInputController::isInputOnLockedArea
	bool ___isInputOnLockedArea_30;
	// System.Single BitBenderGames.TouchInputController::timeSinceDragStart
	float ___timeSinceDragStart_31;
	// BitBenderGames.TouchInputController_InputDragStartDelegate BitBenderGames.TouchInputController::OnDragStart
	InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16 * ___OnDragStart_32;
	// BitBenderGames.TouchInputController_Input1PositionDelegate BitBenderGames.TouchInputController::OnFingerDown
	Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4 * ___OnFingerDown_33;
	// System.Action BitBenderGames.TouchInputController::OnFingerUp
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnFingerUp_34;
	// BitBenderGames.TouchInputController_DragUpdateDelegate BitBenderGames.TouchInputController::OnDragUpdate
	DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537 * ___OnDragUpdate_35;
	// BitBenderGames.TouchInputController_DragStopDelegate BitBenderGames.TouchInputController::OnDragStop
	DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C * ___OnDragStop_36;
	// BitBenderGames.TouchInputController_PinchStartDelegate BitBenderGames.TouchInputController::OnPinchStart
	PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC * ___OnPinchStart_37;
	// BitBenderGames.TouchInputController_PinchUpdateDelegate BitBenderGames.TouchInputController::OnPinchUpdate
	PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89 * ___OnPinchUpdate_38;
	// BitBenderGames.TouchInputController_PinchUpdateExtendedDelegate BitBenderGames.TouchInputController::OnPinchUpdateExtended
	PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705 * ___OnPinchUpdateExtended_39;
	// System.Action BitBenderGames.TouchInputController::OnPinchStop
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnPinchStop_40;
	// BitBenderGames.TouchInputController_InputLongTapProgress BitBenderGames.TouchInputController::OnLongTapProgress
	InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE * ___OnLongTapProgress_41;
	// System.Boolean BitBenderGames.TouchInputController::isClickPrevented
	bool ___isClickPrevented_42;
	// BitBenderGames.TouchInputController_InputClickDelegate BitBenderGames.TouchInputController::OnInputClick
	InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97 * ___OnInputClick_43;
	// System.Boolean BitBenderGames.TouchInputController::isFingerDown
	bool ___isFingerDown_44;

public:
	inline static int32_t get_offset_of_expertModeEnabled_4() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___expertModeEnabled_4)); }
	inline bool get_expertModeEnabled_4() const { return ___expertModeEnabled_4; }
	inline bool* get_address_of_expertModeEnabled_4() { return &___expertModeEnabled_4; }
	inline void set_expertModeEnabled_4(bool value)
	{
		___expertModeEnabled_4 = value;
	}

	inline static int32_t get_offset_of_clickDurationThreshold_5() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___clickDurationThreshold_5)); }
	inline float get_clickDurationThreshold_5() const { return ___clickDurationThreshold_5; }
	inline float* get_address_of_clickDurationThreshold_5() { return &___clickDurationThreshold_5; }
	inline void set_clickDurationThreshold_5(float value)
	{
		___clickDurationThreshold_5 = value;
	}

	inline static int32_t get_offset_of_doubleclickDurationThreshold_6() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___doubleclickDurationThreshold_6)); }
	inline float get_doubleclickDurationThreshold_6() const { return ___doubleclickDurationThreshold_6; }
	inline float* get_address_of_doubleclickDurationThreshold_6() { return &___doubleclickDurationThreshold_6; }
	inline void set_doubleclickDurationThreshold_6(float value)
	{
		___doubleclickDurationThreshold_6 = value;
	}

	inline static int32_t get_offset_of_tiltMoveDotTreshold_7() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___tiltMoveDotTreshold_7)); }
	inline float get_tiltMoveDotTreshold_7() const { return ___tiltMoveDotTreshold_7; }
	inline float* get_address_of_tiltMoveDotTreshold_7() { return &___tiltMoveDotTreshold_7; }
	inline void set_tiltMoveDotTreshold_7(float value)
	{
		___tiltMoveDotTreshold_7 = value;
	}

	inline static int32_t get_offset_of_tiltHorizontalDotThreshold_8() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___tiltHorizontalDotThreshold_8)); }
	inline float get_tiltHorizontalDotThreshold_8() const { return ___tiltHorizontalDotThreshold_8; }
	inline float* get_address_of_tiltHorizontalDotThreshold_8() { return &___tiltHorizontalDotThreshold_8; }
	inline void set_tiltHorizontalDotThreshold_8(float value)
	{
		___tiltHorizontalDotThreshold_8 = value;
	}

	inline static int32_t get_offset_of_dragStartDistanceThresholdRelative_9() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___dragStartDistanceThresholdRelative_9)); }
	inline float get_dragStartDistanceThresholdRelative_9() const { return ___dragStartDistanceThresholdRelative_9; }
	inline float* get_address_of_dragStartDistanceThresholdRelative_9() { return &___dragStartDistanceThresholdRelative_9; }
	inline void set_dragStartDistanceThresholdRelative_9(float value)
	{
		___dragStartDistanceThresholdRelative_9 = value;
	}

	inline static int32_t get_offset_of_longTapStartsDrag_10() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___longTapStartsDrag_10)); }
	inline bool get_longTapStartsDrag_10() const { return ___longTapStartsDrag_10; }
	inline bool* get_address_of_longTapStartsDrag_10() { return &___longTapStartsDrag_10; }
	inline void set_longTapStartsDrag_10(bool value)
	{
		___longTapStartsDrag_10 = value;
	}

	inline static int32_t get_offset_of_lastFingerDownTimeReal_11() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___lastFingerDownTimeReal_11)); }
	inline float get_lastFingerDownTimeReal_11() const { return ___lastFingerDownTimeReal_11; }
	inline float* get_address_of_lastFingerDownTimeReal_11() { return &___lastFingerDownTimeReal_11; }
	inline void set_lastFingerDownTimeReal_11(float value)
	{
		___lastFingerDownTimeReal_11 = value;
	}

	inline static int32_t get_offset_of_lastClickTimeReal_12() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___lastClickTimeReal_12)); }
	inline float get_lastClickTimeReal_12() const { return ___lastClickTimeReal_12; }
	inline float* get_address_of_lastClickTimeReal_12() { return &___lastClickTimeReal_12; }
	inline void set_lastClickTimeReal_12(float value)
	{
		___lastClickTimeReal_12 = value;
	}

	inline static int32_t get_offset_of_wasFingerDownLastFrame_13() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___wasFingerDownLastFrame_13)); }
	inline bool get_wasFingerDownLastFrame_13() const { return ___wasFingerDownLastFrame_13; }
	inline bool* get_address_of_wasFingerDownLastFrame_13() { return &___wasFingerDownLastFrame_13; }
	inline void set_wasFingerDownLastFrame_13(bool value)
	{
		___wasFingerDownLastFrame_13 = value;
	}

	inline static int32_t get_offset_of_lastFinger0DownPos_14() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___lastFinger0DownPos_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastFinger0DownPos_14() const { return ___lastFinger0DownPos_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastFinger0DownPos_14() { return &___lastFinger0DownPos_14; }
	inline void set_lastFinger0DownPos_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastFinger0DownPos_14 = value;
	}

	inline static int32_t get_offset_of_isDragging_16() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___isDragging_16)); }
	inline bool get_isDragging_16() const { return ___isDragging_16; }
	inline bool* get_address_of_isDragging_16() { return &___isDragging_16; }
	inline void set_isDragging_16(bool value)
	{
		___isDragging_16 = value;
	}

	inline static int32_t get_offset_of_dragStartPos_17() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___dragStartPos_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dragStartPos_17() const { return ___dragStartPos_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dragStartPos_17() { return &___dragStartPos_17; }
	inline void set_dragStartPos_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dragStartPos_17 = value;
	}

	inline static int32_t get_offset_of_dragStartOffset_18() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___dragStartOffset_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dragStartOffset_18() const { return ___dragStartOffset_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dragStartOffset_18() { return &___dragStartOffset_18; }
	inline void set_dragStartOffset_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dragStartOffset_18 = value;
	}

	inline static int32_t get_offset_of_U3CDragFinalMomentumVectorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___U3CDragFinalMomentumVectorU3Ek__BackingField_19)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_U3CDragFinalMomentumVectorU3Ek__BackingField_19() const { return ___U3CDragFinalMomentumVectorU3Ek__BackingField_19; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_U3CDragFinalMomentumVectorU3Ek__BackingField_19() { return &___U3CDragFinalMomentumVectorU3Ek__BackingField_19; }
	inline void set_U3CDragFinalMomentumVectorU3Ek__BackingField_19(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___U3CDragFinalMomentumVectorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDragFinalMomentumVectorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_pinchStartDistance_21() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___pinchStartDistance_21)); }
	inline float get_pinchStartDistance_21() const { return ___pinchStartDistance_21; }
	inline float* get_address_of_pinchStartDistance_21() { return &___pinchStartDistance_21; }
	inline void set_pinchStartDistance_21(float value)
	{
		___pinchStartDistance_21 = value;
	}

	inline static int32_t get_offset_of_pinchStartPositions_22() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___pinchStartPositions_22)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_pinchStartPositions_22() const { return ___pinchStartPositions_22; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_pinchStartPositions_22() { return &___pinchStartPositions_22; }
	inline void set_pinchStartPositions_22(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___pinchStartPositions_22 = value;
		Il2CppCodeGenWriteBarrier((&___pinchStartPositions_22), value);
	}

	inline static int32_t get_offset_of_touchPositionLastFrame_23() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___touchPositionLastFrame_23)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_touchPositionLastFrame_23() const { return ___touchPositionLastFrame_23; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_touchPositionLastFrame_23() { return &___touchPositionLastFrame_23; }
	inline void set_touchPositionLastFrame_23(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___touchPositionLastFrame_23 = value;
		Il2CppCodeGenWriteBarrier((&___touchPositionLastFrame_23), value);
	}

	inline static int32_t get_offset_of_pinchRotationVectorStart_24() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___pinchRotationVectorStart_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchRotationVectorStart_24() const { return ___pinchRotationVectorStart_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchRotationVectorStart_24() { return &___pinchRotationVectorStart_24; }
	inline void set_pinchRotationVectorStart_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchRotationVectorStart_24 = value;
	}

	inline static int32_t get_offset_of_pinchVectorLastFrame_25() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___pinchVectorLastFrame_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchVectorLastFrame_25() const { return ___pinchVectorLastFrame_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchVectorLastFrame_25() { return &___pinchVectorLastFrame_25; }
	inline void set_pinchVectorLastFrame_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchVectorLastFrame_25 = value;
	}

	inline static int32_t get_offset_of_totalFingerMovement_26() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___totalFingerMovement_26)); }
	inline float get_totalFingerMovement_26() const { return ___totalFingerMovement_26; }
	inline float* get_address_of_totalFingerMovement_26() { return &___totalFingerMovement_26; }
	inline void set_totalFingerMovement_26(float value)
	{
		___totalFingerMovement_26 = value;
	}

	inline static int32_t get_offset_of_wasDraggingLastFrame_27() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___wasDraggingLastFrame_27)); }
	inline bool get_wasDraggingLastFrame_27() const { return ___wasDraggingLastFrame_27; }
	inline bool* get_address_of_wasDraggingLastFrame_27() { return &___wasDraggingLastFrame_27; }
	inline void set_wasDraggingLastFrame_27(bool value)
	{
		___wasDraggingLastFrame_27 = value;
	}

	inline static int32_t get_offset_of_wasPinchingLastFrame_28() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___wasPinchingLastFrame_28)); }
	inline bool get_wasPinchingLastFrame_28() const { return ___wasPinchingLastFrame_28; }
	inline bool* get_address_of_wasPinchingLastFrame_28() { return &___wasPinchingLastFrame_28; }
	inline void set_wasPinchingLastFrame_28(bool value)
	{
		___wasPinchingLastFrame_28 = value;
	}

	inline static int32_t get_offset_of_isPinching_29() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___isPinching_29)); }
	inline bool get_isPinching_29() const { return ___isPinching_29; }
	inline bool* get_address_of_isPinching_29() { return &___isPinching_29; }
	inline void set_isPinching_29(bool value)
	{
		___isPinching_29 = value;
	}

	inline static int32_t get_offset_of_isInputOnLockedArea_30() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___isInputOnLockedArea_30)); }
	inline bool get_isInputOnLockedArea_30() const { return ___isInputOnLockedArea_30; }
	inline bool* get_address_of_isInputOnLockedArea_30() { return &___isInputOnLockedArea_30; }
	inline void set_isInputOnLockedArea_30(bool value)
	{
		___isInputOnLockedArea_30 = value;
	}

	inline static int32_t get_offset_of_timeSinceDragStart_31() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___timeSinceDragStart_31)); }
	inline float get_timeSinceDragStart_31() const { return ___timeSinceDragStart_31; }
	inline float* get_address_of_timeSinceDragStart_31() { return &___timeSinceDragStart_31; }
	inline void set_timeSinceDragStart_31(float value)
	{
		___timeSinceDragStart_31 = value;
	}

	inline static int32_t get_offset_of_OnDragStart_32() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnDragStart_32)); }
	inline InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16 * get_OnDragStart_32() const { return ___OnDragStart_32; }
	inline InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16 ** get_address_of_OnDragStart_32() { return &___OnDragStart_32; }
	inline void set_OnDragStart_32(InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16 * value)
	{
		___OnDragStart_32 = value;
		Il2CppCodeGenWriteBarrier((&___OnDragStart_32), value);
	}

	inline static int32_t get_offset_of_OnFingerDown_33() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnFingerDown_33)); }
	inline Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4 * get_OnFingerDown_33() const { return ___OnFingerDown_33; }
	inline Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4 ** get_address_of_OnFingerDown_33() { return &___OnFingerDown_33; }
	inline void set_OnFingerDown_33(Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4 * value)
	{
		___OnFingerDown_33 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerDown_33), value);
	}

	inline static int32_t get_offset_of_OnFingerUp_34() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnFingerUp_34)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnFingerUp_34() const { return ___OnFingerUp_34; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnFingerUp_34() { return &___OnFingerUp_34; }
	inline void set_OnFingerUp_34(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnFingerUp_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerUp_34), value);
	}

	inline static int32_t get_offset_of_OnDragUpdate_35() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnDragUpdate_35)); }
	inline DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537 * get_OnDragUpdate_35() const { return ___OnDragUpdate_35; }
	inline DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537 ** get_address_of_OnDragUpdate_35() { return &___OnDragUpdate_35; }
	inline void set_OnDragUpdate_35(DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537 * value)
	{
		___OnDragUpdate_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnDragUpdate_35), value);
	}

	inline static int32_t get_offset_of_OnDragStop_36() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnDragStop_36)); }
	inline DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C * get_OnDragStop_36() const { return ___OnDragStop_36; }
	inline DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C ** get_address_of_OnDragStop_36() { return &___OnDragStop_36; }
	inline void set_OnDragStop_36(DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C * value)
	{
		___OnDragStop_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnDragStop_36), value);
	}

	inline static int32_t get_offset_of_OnPinchStart_37() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnPinchStart_37)); }
	inline PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC * get_OnPinchStart_37() const { return ___OnPinchStart_37; }
	inline PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC ** get_address_of_OnPinchStart_37() { return &___OnPinchStart_37; }
	inline void set_OnPinchStart_37(PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC * value)
	{
		___OnPinchStart_37 = value;
		Il2CppCodeGenWriteBarrier((&___OnPinchStart_37), value);
	}

	inline static int32_t get_offset_of_OnPinchUpdate_38() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnPinchUpdate_38)); }
	inline PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89 * get_OnPinchUpdate_38() const { return ___OnPinchUpdate_38; }
	inline PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89 ** get_address_of_OnPinchUpdate_38() { return &___OnPinchUpdate_38; }
	inline void set_OnPinchUpdate_38(PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89 * value)
	{
		___OnPinchUpdate_38 = value;
		Il2CppCodeGenWriteBarrier((&___OnPinchUpdate_38), value);
	}

	inline static int32_t get_offset_of_OnPinchUpdateExtended_39() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnPinchUpdateExtended_39)); }
	inline PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705 * get_OnPinchUpdateExtended_39() const { return ___OnPinchUpdateExtended_39; }
	inline PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705 ** get_address_of_OnPinchUpdateExtended_39() { return &___OnPinchUpdateExtended_39; }
	inline void set_OnPinchUpdateExtended_39(PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705 * value)
	{
		___OnPinchUpdateExtended_39 = value;
		Il2CppCodeGenWriteBarrier((&___OnPinchUpdateExtended_39), value);
	}

	inline static int32_t get_offset_of_OnPinchStop_40() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnPinchStop_40)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnPinchStop_40() const { return ___OnPinchStop_40; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnPinchStop_40() { return &___OnPinchStop_40; }
	inline void set_OnPinchStop_40(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnPinchStop_40 = value;
		Il2CppCodeGenWriteBarrier((&___OnPinchStop_40), value);
	}

	inline static int32_t get_offset_of_OnLongTapProgress_41() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnLongTapProgress_41)); }
	inline InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE * get_OnLongTapProgress_41() const { return ___OnLongTapProgress_41; }
	inline InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE ** get_address_of_OnLongTapProgress_41() { return &___OnLongTapProgress_41; }
	inline void set_OnLongTapProgress_41(InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE * value)
	{
		___OnLongTapProgress_41 = value;
		Il2CppCodeGenWriteBarrier((&___OnLongTapProgress_41), value);
	}

	inline static int32_t get_offset_of_isClickPrevented_42() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___isClickPrevented_42)); }
	inline bool get_isClickPrevented_42() const { return ___isClickPrevented_42; }
	inline bool* get_address_of_isClickPrevented_42() { return &___isClickPrevented_42; }
	inline void set_isClickPrevented_42(bool value)
	{
		___isClickPrevented_42 = value;
	}

	inline static int32_t get_offset_of_OnInputClick_43() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___OnInputClick_43)); }
	inline InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97 * get_OnInputClick_43() const { return ___OnInputClick_43; }
	inline InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97 ** get_address_of_OnInputClick_43() { return &___OnInputClick_43; }
	inline void set_OnInputClick_43(InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97 * value)
	{
		___OnInputClick_43 = value;
		Il2CppCodeGenWriteBarrier((&___OnInputClick_43), value);
	}

	inline static int32_t get_offset_of_isFingerDown_44() { return static_cast<int32_t>(offsetof(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F, ___isFingerDown_44)); }
	inline bool get_isFingerDown_44() const { return ___isFingerDown_44; }
	inline bool* get_address_of_isFingerDown_44() { return &___isFingerDown_44; }
	inline void set_isFingerDown_44(bool value)
	{
		___isFingerDown_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINPUTCONTROLLER_T4A8C8C170434264465AFFEEC42C00CB66609331F_H
#ifndef DISPATCHER_T8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_H
#define DISPATCHER_T8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.Dispatcher
struct  Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> CI.HttpClient.Core.Dispatcher::_queue
	Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * ____queue_4;
	// System.Object CI.HttpClient.Core.Dispatcher::_lock
	RuntimeObject * ____lock_5;

public:
	inline static int32_t get_offset_of__queue_4() { return static_cast<int32_t>(offsetof(Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields, ____queue_4)); }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * get__queue_4() const { return ____queue_4; }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA ** get_address_of__queue_4() { return &____queue_4; }
	inline void set__queue_4(Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * value)
	{
		____queue_4 = value;
		Il2CppCodeGenWriteBarrier((&____queue_4), value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((&____lock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_T8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_H
#ifndef EASYTOUCHTRIGGER_TE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61_H
#define EASYTOUCHTRIGGER_TE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger
struct  EasyTouchTrigger_tE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver> HedgehogTeam.EasyTouch.EasyTouchTrigger::receivers
	List_1_tFD5BDF82231676F2B42789D5056A999700B83F12 * ___receivers_4;

public:
	inline static int32_t get_offset_of_receivers_4() { return static_cast<int32_t>(offsetof(EasyTouchTrigger_tE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61, ___receivers_4)); }
	inline List_1_tFD5BDF82231676F2B42789D5056A999700B83F12 * get_receivers_4() const { return ___receivers_4; }
	inline List_1_tFD5BDF82231676F2B42789D5056A999700B83F12 ** get_address_of_receivers_4() { return &___receivers_4; }
	inline void set_receivers_4(List_1_tFD5BDF82231676F2B42789D5056A999700B83F12 * value)
	{
		___receivers_4 = value;
		Il2CppCodeGenWriteBarrier((&___receivers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHTRIGGER_TE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61_H
#ifndef QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#define QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase
struct  QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HedgehogTeam.EasyTouch.QuickBase::quickActionName
	String_t* ___quickActionName_4;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isMultiTouch
	bool ___isMultiTouch_5;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::is2Finger
	bool ___is2Finger_6;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isOnTouch
	bool ___isOnTouch_7;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::enablePickOverUI
	bool ___enablePickOverUI_8;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::resetPhysic
	bool ___resetPhysic_9;
	// HedgehogTeam.EasyTouch.QuickBase_DirectAction HedgehogTeam.EasyTouch.QuickBase::directAction
	int32_t ___directAction_10;
	// HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction HedgehogTeam.EasyTouch.QuickBase::axesAction
	int32_t ___axesAction_11;
	// System.Single HedgehogTeam.EasyTouch.QuickBase::sensibility
	float ___sensibility_12;
	// UnityEngine.CharacterController HedgehogTeam.EasyTouch.QuickBase::directCharacterController
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___directCharacterController_13;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::inverseAxisValue
	bool ___inverseAxisValue_14;
	// UnityEngine.Rigidbody HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___cachedRigidBody_15;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic
	bool ___isKinematic_16;
	// UnityEngine.Rigidbody2D HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody2D
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___cachedRigidBody2D_17;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic2D
	bool ___isKinematic2D_18;
	// HedgehogTeam.EasyTouch.QuickBase_GameObjectType HedgehogTeam.EasyTouch.QuickBase::realType
	int32_t ___realType_19;
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase::fingerIndex
	int32_t ___fingerIndex_20;

public:
	inline static int32_t get_offset_of_quickActionName_4() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___quickActionName_4)); }
	inline String_t* get_quickActionName_4() const { return ___quickActionName_4; }
	inline String_t** get_address_of_quickActionName_4() { return &___quickActionName_4; }
	inline void set_quickActionName_4(String_t* value)
	{
		___quickActionName_4 = value;
		Il2CppCodeGenWriteBarrier((&___quickActionName_4), value);
	}

	inline static int32_t get_offset_of_isMultiTouch_5() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isMultiTouch_5)); }
	inline bool get_isMultiTouch_5() const { return ___isMultiTouch_5; }
	inline bool* get_address_of_isMultiTouch_5() { return &___isMultiTouch_5; }
	inline void set_isMultiTouch_5(bool value)
	{
		___isMultiTouch_5 = value;
	}

	inline static int32_t get_offset_of_is2Finger_6() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___is2Finger_6)); }
	inline bool get_is2Finger_6() const { return ___is2Finger_6; }
	inline bool* get_address_of_is2Finger_6() { return &___is2Finger_6; }
	inline void set_is2Finger_6(bool value)
	{
		___is2Finger_6 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_7() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isOnTouch_7)); }
	inline bool get_isOnTouch_7() const { return ___isOnTouch_7; }
	inline bool* get_address_of_isOnTouch_7() { return &___isOnTouch_7; }
	inline void set_isOnTouch_7(bool value)
	{
		___isOnTouch_7 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_8() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___enablePickOverUI_8)); }
	inline bool get_enablePickOverUI_8() const { return ___enablePickOverUI_8; }
	inline bool* get_address_of_enablePickOverUI_8() { return &___enablePickOverUI_8; }
	inline void set_enablePickOverUI_8(bool value)
	{
		___enablePickOverUI_8 = value;
	}

	inline static int32_t get_offset_of_resetPhysic_9() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___resetPhysic_9)); }
	inline bool get_resetPhysic_9() const { return ___resetPhysic_9; }
	inline bool* get_address_of_resetPhysic_9() { return &___resetPhysic_9; }
	inline void set_resetPhysic_9(bool value)
	{
		___resetPhysic_9 = value;
	}

	inline static int32_t get_offset_of_directAction_10() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___directAction_10)); }
	inline int32_t get_directAction_10() const { return ___directAction_10; }
	inline int32_t* get_address_of_directAction_10() { return &___directAction_10; }
	inline void set_directAction_10(int32_t value)
	{
		___directAction_10 = value;
	}

	inline static int32_t get_offset_of_axesAction_11() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___axesAction_11)); }
	inline int32_t get_axesAction_11() const { return ___axesAction_11; }
	inline int32_t* get_address_of_axesAction_11() { return &___axesAction_11; }
	inline void set_axesAction_11(int32_t value)
	{
		___axesAction_11 = value;
	}

	inline static int32_t get_offset_of_sensibility_12() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___sensibility_12)); }
	inline float get_sensibility_12() const { return ___sensibility_12; }
	inline float* get_address_of_sensibility_12() { return &___sensibility_12; }
	inline void set_sensibility_12(float value)
	{
		___sensibility_12 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_13() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___directCharacterController_13)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_directCharacterController_13() const { return ___directCharacterController_13; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_directCharacterController_13() { return &___directCharacterController_13; }
	inline void set_directCharacterController_13(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___directCharacterController_13 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_13), value);
	}

	inline static int32_t get_offset_of_inverseAxisValue_14() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___inverseAxisValue_14)); }
	inline bool get_inverseAxisValue_14() const { return ___inverseAxisValue_14; }
	inline bool* get_address_of_inverseAxisValue_14() { return &___inverseAxisValue_14; }
	inline void set_inverseAxisValue_14(bool value)
	{
		___inverseAxisValue_14 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody_15() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___cachedRigidBody_15)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_cachedRigidBody_15() const { return ___cachedRigidBody_15; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_cachedRigidBody_15() { return &___cachedRigidBody_15; }
	inline void set_cachedRigidBody_15(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___cachedRigidBody_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody_15), value);
	}

	inline static int32_t get_offset_of_isKinematic_16() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isKinematic_16)); }
	inline bool get_isKinematic_16() const { return ___isKinematic_16; }
	inline bool* get_address_of_isKinematic_16() { return &___isKinematic_16; }
	inline void set_isKinematic_16(bool value)
	{
		___isKinematic_16 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody2D_17() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___cachedRigidBody2D_17)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_cachedRigidBody2D_17() const { return ___cachedRigidBody2D_17; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_cachedRigidBody2D_17() { return &___cachedRigidBody2D_17; }
	inline void set_cachedRigidBody2D_17(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___cachedRigidBody2D_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody2D_17), value);
	}

	inline static int32_t get_offset_of_isKinematic2D_18() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isKinematic2D_18)); }
	inline bool get_isKinematic2D_18() const { return ___isKinematic2D_18; }
	inline bool* get_address_of_isKinematic2D_18() { return &___isKinematic2D_18; }
	inline void set_isKinematic2D_18(bool value)
	{
		___isKinematic2D_18 = value;
	}

	inline static int32_t get_offset_of_realType_19() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___realType_19)); }
	inline int32_t get_realType_19() const { return ___realType_19; }
	inline int32_t* get_address_of_realType_19() { return &___realType_19; }
	inline void set_realType_19(int32_t value)
	{
		___realType_19 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_20() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___fingerIndex_20)); }
	inline int32_t get_fingerIndex_20() const { return ___fingerIndex_20; }
	inline int32_t* get_address_of_fingerIndex_20() { return &___fingerIndex_20; }
	inline void set_fingerIndex_20(int32_t value)
	{
		___fingerIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#ifndef PERSISTENTDATAPATHLOADSAMPLE_TC5BF685056EAC52A39AB583839E63B5A67E22FCE_H
#define PERSISTENTDATAPATHLOADSAMPLE_TC5BF685056EAC52A39AB583839E63B5A67E22FCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.PersistentDataPathLoadSample
struct  PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] TriLib.Samples.PersistentDataPathLoadSample::_files
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____files_4;
	// UnityEngine.GameObject TriLib.Samples.PersistentDataPathLoadSample::_loadedGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____loadedGameObject_5;

public:
	inline static int32_t get_offset_of__files_4() { return static_cast<int32_t>(offsetof(PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE, ____files_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__files_4() const { return ____files_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__files_4() { return &____files_4; }
	inline void set__files_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____files_4 = value;
		Il2CppCodeGenWriteBarrier((&____files_4), value);
	}

	inline static int32_t get_offset_of__loadedGameObject_5() { return static_cast<int32_t>(offsetof(PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE, ____loadedGameObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__loadedGameObject_5() const { return ____loadedGameObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__loadedGameObject_5() { return &____loadedGameObject_5; }
	inline void set__loadedGameObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____loadedGameObject_5 = value;
		Il2CppCodeGenWriteBarrier((&____loadedGameObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTDATAPATHLOADSAMPLE_TC5BF685056EAC52A39AB583839E63B5A67E22FCE_H
#ifndef PROGRESSHANDLINGSAMPLE_TAEF539D60538FF937B7F403CDD4C354FBB02D2FF_H
#define PROGRESSHANDLINGSAMPLE_TAEF539D60538FF937B7F403CDD4C354FBB02D2FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.ProgressHandlingSample
struct  ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GUIStyle TriLib.Samples.ProgressHandlingSample::_centeredStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ____centeredStyle_4;
	// System.Single TriLib.Samples.ProgressHandlingSample::_assetLoadingProgress
	float ____assetLoadingProgress_5;
	// System.Boolean TriLib.Samples.ProgressHandlingSample::_assetLoaded
	bool ____assetLoaded_6;
	// System.String TriLib.Samples.ProgressHandlingSample::_error
	String_t* ____error_7;
	// UnityEngine.Texture2D TriLib.Samples.ProgressHandlingSample::_progressBarEmptyTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____progressBarEmptyTexture_8;
	// UnityEngine.Texture2D TriLib.Samples.ProgressHandlingSample::_progressBarFilledTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____progressBarFilledTexture_9;

public:
	inline static int32_t get_offset_of__centeredStyle_4() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____centeredStyle_4)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get__centeredStyle_4() const { return ____centeredStyle_4; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of__centeredStyle_4() { return &____centeredStyle_4; }
	inline void set__centeredStyle_4(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		____centeredStyle_4 = value;
		Il2CppCodeGenWriteBarrier((&____centeredStyle_4), value);
	}

	inline static int32_t get_offset_of__assetLoadingProgress_5() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____assetLoadingProgress_5)); }
	inline float get__assetLoadingProgress_5() const { return ____assetLoadingProgress_5; }
	inline float* get_address_of__assetLoadingProgress_5() { return &____assetLoadingProgress_5; }
	inline void set__assetLoadingProgress_5(float value)
	{
		____assetLoadingProgress_5 = value;
	}

	inline static int32_t get_offset_of__assetLoaded_6() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____assetLoaded_6)); }
	inline bool get__assetLoaded_6() const { return ____assetLoaded_6; }
	inline bool* get_address_of__assetLoaded_6() { return &____assetLoaded_6; }
	inline void set__assetLoaded_6(bool value)
	{
		____assetLoaded_6 = value;
	}

	inline static int32_t get_offset_of__error_7() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____error_7)); }
	inline String_t* get__error_7() const { return ____error_7; }
	inline String_t** get_address_of__error_7() { return &____error_7; }
	inline void set__error_7(String_t* value)
	{
		____error_7 = value;
		Il2CppCodeGenWriteBarrier((&____error_7), value);
	}

	inline static int32_t get_offset_of__progressBarEmptyTexture_8() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____progressBarEmptyTexture_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__progressBarEmptyTexture_8() const { return ____progressBarEmptyTexture_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__progressBarEmptyTexture_8() { return &____progressBarEmptyTexture_8; }
	inline void set__progressBarEmptyTexture_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____progressBarEmptyTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&____progressBarEmptyTexture_8), value);
	}

	inline static int32_t get_offset_of__progressBarFilledTexture_9() { return static_cast<int32_t>(offsetof(ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF, ____progressBarFilledTexture_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__progressBarFilledTexture_9() const { return ____progressBarFilledTexture_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__progressBarFilledTexture_9() { return &____progressBarFilledTexture_9; }
	inline void set__progressBarFilledTexture_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____progressBarFilledTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&____progressBarFilledTexture_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSHANDLINGSAMPLE_TAEF539D60538FF937B7F403CDD4C354FBB02D2FF_H
#ifndef SIMPLEROTATOR_T58EECB5DEB87D980FB1E81554C611718BF2D9051_H
#define SIMPLEROTATOR_T58EECB5DEB87D980FB1E81554C611718BF2D9051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.SimpleRotator
struct  SimpleRotator_t58EECB5DEB87D980FB1E81554C611718BF2D9051  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEROTATOR_T58EECB5DEB87D980FB1E81554C611718BF2D9051_H
#ifndef URIDIALOG_T177278A7CF46FBFED6EAF0966B666D74DB91F9FF_H
#define URIDIALOG_T177278A7CF46FBFED6EAF0966B666D74DB91F9FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.URIDialog
struct  URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button TriLib.Samples.URIDialog::_okButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____okButton_5;
	// UnityEngine.UI.Button TriLib.Samples.URIDialog::_cancelButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____cancelButton_6;
	// UnityEngine.UI.InputField TriLib.Samples.URIDialog::_uriText
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____uriText_7;
	// UnityEngine.UI.InputField TriLib.Samples.URIDialog::_extensionText
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____extensionText_8;
	// UnityEngine.GameObject TriLib.Samples.URIDialog::_rendererGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____rendererGameObject_9;

public:
	inline static int32_t get_offset_of__okButton_5() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF, ____okButton_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__okButton_5() const { return ____okButton_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__okButton_5() { return &____okButton_5; }
	inline void set__okButton_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____okButton_5 = value;
		Il2CppCodeGenWriteBarrier((&____okButton_5), value);
	}

	inline static int32_t get_offset_of__cancelButton_6() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF, ____cancelButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__cancelButton_6() const { return ____cancelButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__cancelButton_6() { return &____cancelButton_6; }
	inline void set__cancelButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____cancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((&____cancelButton_6), value);
	}

	inline static int32_t get_offset_of__uriText_7() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF, ____uriText_7)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__uriText_7() const { return ____uriText_7; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__uriText_7() { return &____uriText_7; }
	inline void set__uriText_7(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____uriText_7 = value;
		Il2CppCodeGenWriteBarrier((&____uriText_7), value);
	}

	inline static int32_t get_offset_of__extensionText_8() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF, ____extensionText_8)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__extensionText_8() const { return ____extensionText_8; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__extensionText_8() { return &____extensionText_8; }
	inline void set__extensionText_8(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____extensionText_8 = value;
		Il2CppCodeGenWriteBarrier((&____extensionText_8), value);
	}

	inline static int32_t get_offset_of__rendererGameObject_9() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF, ____rendererGameObject_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__rendererGameObject_9() const { return ____rendererGameObject_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__rendererGameObject_9() { return &____rendererGameObject_9; }
	inline void set__rendererGameObject_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____rendererGameObject_9 = value;
		Il2CppCodeGenWriteBarrier((&____rendererGameObject_9), value);
	}
};

struct URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF_StaticFields
{
public:
	// TriLib.Samples.URIDialog TriLib.Samples.URIDialog::<Instance>k__BackingField
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIDIALOG_T177278A7CF46FBFED6EAF0966B666D74DB91F9FF_H
#ifndef FOCUSCAMERAONITEM_TDE1C85F927B27015660C8A8F0F7295BD002BFC0A_H
#define FOCUSCAMERAONITEM_TDE1C85F927B27015660C8A8F0F7295BD002BFC0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.FocusCameraOnItem
struct  FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A  : public MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02
{
public:
	// System.Single BitBenderGames.FocusCameraOnItem::transitionDuration
	float ___transitionDuration_6;
	// UnityEngine.AnimationCurve BitBenderGames.FocusCameraOnItem::transitionCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___transitionCurve_7;
	// BitBenderGames.MobileTouchCamera BitBenderGames.FocusCameraOnItem::<MobileTouchCamera>k__BackingField
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * ___U3CMobileTouchCameraU3Ek__BackingField_8;
	// UnityEngine.Vector3 BitBenderGames.FocusCameraOnItem::posTransitionStart
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___posTransitionStart_9;
	// UnityEngine.Vector3 BitBenderGames.FocusCameraOnItem::posTransitionEnd
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___posTransitionEnd_10;
	// UnityEngine.Quaternion BitBenderGames.FocusCameraOnItem::rotTransitionStart
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotTransitionStart_11;
	// UnityEngine.Quaternion BitBenderGames.FocusCameraOnItem::rotTransitionEnd
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotTransitionEnd_12;
	// System.Single BitBenderGames.FocusCameraOnItem::zoomTransitionStart
	float ___zoomTransitionStart_13;
	// System.Single BitBenderGames.FocusCameraOnItem::zoomTransitionEnd
	float ___zoomTransitionEnd_14;
	// System.Single BitBenderGames.FocusCameraOnItem::timeTransitionStart
	float ___timeTransitionStart_15;
	// System.Boolean BitBenderGames.FocusCameraOnItem::isTransitionStarted
	bool ___isTransitionStarted_16;

public:
	inline static int32_t get_offset_of_transitionDuration_6() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___transitionDuration_6)); }
	inline float get_transitionDuration_6() const { return ___transitionDuration_6; }
	inline float* get_address_of_transitionDuration_6() { return &___transitionDuration_6; }
	inline void set_transitionDuration_6(float value)
	{
		___transitionDuration_6 = value;
	}

	inline static int32_t get_offset_of_transitionCurve_7() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___transitionCurve_7)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_transitionCurve_7() const { return ___transitionCurve_7; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_transitionCurve_7() { return &___transitionCurve_7; }
	inline void set_transitionCurve_7(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___transitionCurve_7 = value;
		Il2CppCodeGenWriteBarrier((&___transitionCurve_7), value);
	}

	inline static int32_t get_offset_of_U3CMobileTouchCameraU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___U3CMobileTouchCameraU3Ek__BackingField_8)); }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * get_U3CMobileTouchCameraU3Ek__BackingField_8() const { return ___U3CMobileTouchCameraU3Ek__BackingField_8; }
	inline MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA ** get_address_of_U3CMobileTouchCameraU3Ek__BackingField_8() { return &___U3CMobileTouchCameraU3Ek__BackingField_8; }
	inline void set_U3CMobileTouchCameraU3Ek__BackingField_8(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA * value)
	{
		___U3CMobileTouchCameraU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMobileTouchCameraU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_posTransitionStart_9() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___posTransitionStart_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_posTransitionStart_9() const { return ___posTransitionStart_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_posTransitionStart_9() { return &___posTransitionStart_9; }
	inline void set_posTransitionStart_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___posTransitionStart_9 = value;
	}

	inline static int32_t get_offset_of_posTransitionEnd_10() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___posTransitionEnd_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_posTransitionEnd_10() const { return ___posTransitionEnd_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_posTransitionEnd_10() { return &___posTransitionEnd_10; }
	inline void set_posTransitionEnd_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___posTransitionEnd_10 = value;
	}

	inline static int32_t get_offset_of_rotTransitionStart_11() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___rotTransitionStart_11)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotTransitionStart_11() const { return ___rotTransitionStart_11; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotTransitionStart_11() { return &___rotTransitionStart_11; }
	inline void set_rotTransitionStart_11(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotTransitionStart_11 = value;
	}

	inline static int32_t get_offset_of_rotTransitionEnd_12() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___rotTransitionEnd_12)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotTransitionEnd_12() const { return ___rotTransitionEnd_12; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotTransitionEnd_12() { return &___rotTransitionEnd_12; }
	inline void set_rotTransitionEnd_12(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotTransitionEnd_12 = value;
	}

	inline static int32_t get_offset_of_zoomTransitionStart_13() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___zoomTransitionStart_13)); }
	inline float get_zoomTransitionStart_13() const { return ___zoomTransitionStart_13; }
	inline float* get_address_of_zoomTransitionStart_13() { return &___zoomTransitionStart_13; }
	inline void set_zoomTransitionStart_13(float value)
	{
		___zoomTransitionStart_13 = value;
	}

	inline static int32_t get_offset_of_zoomTransitionEnd_14() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___zoomTransitionEnd_14)); }
	inline float get_zoomTransitionEnd_14() const { return ___zoomTransitionEnd_14; }
	inline float* get_address_of_zoomTransitionEnd_14() { return &___zoomTransitionEnd_14; }
	inline void set_zoomTransitionEnd_14(float value)
	{
		___zoomTransitionEnd_14 = value;
	}

	inline static int32_t get_offset_of_timeTransitionStart_15() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___timeTransitionStart_15)); }
	inline float get_timeTransitionStart_15() const { return ___timeTransitionStart_15; }
	inline float* get_address_of_timeTransitionStart_15() { return &___timeTransitionStart_15; }
	inline void set_timeTransitionStart_15(float value)
	{
		___timeTransitionStart_15 = value;
	}

	inline static int32_t get_offset_of_isTransitionStarted_16() { return static_cast<int32_t>(offsetof(FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A, ___isTransitionStarted_16)); }
	inline bool get_isTransitionStarted_16() const { return ___isTransitionStarted_16; }
	inline bool* get_address_of_isTransitionStarted_16() { return &___isTransitionStarted_16; }
	inline void set_isTransitionStarted_16(bool value)
	{
		___isTransitionStarted_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSCAMERAONITEM_TDE1C85F927B27015660C8A8F0F7295BD002BFC0A_H
#ifndef MOBILETOUCHCAMERA_T88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA_H
#define MOBILETOUCHCAMERA_T88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BitBenderGames.MobileTouchCamera
struct  MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA  : public MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02
{
public:
	// BitBenderGames.CameraPlaneAxes BitBenderGames.MobileTouchCamera::cameraAxes
	int32_t ___cameraAxes_6;
	// BitBenderGames.PerspectiveZoomMode BitBenderGames.MobileTouchCamera::perspectiveZoomMode
	int32_t ___perspectiveZoomMode_7;
	// System.Single BitBenderGames.MobileTouchCamera::camZoomMin
	float ___camZoomMin_8;
	// System.Single BitBenderGames.MobileTouchCamera::camZoomMax
	float ___camZoomMax_9;
	// System.Single BitBenderGames.MobileTouchCamera::camOverzoomMargin
	float ___camOverzoomMargin_10;
	// System.Single BitBenderGames.MobileTouchCamera::camOverdragMargin
	float ___camOverdragMargin_11;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchCamera::boundaryMin
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___boundaryMin_12;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchCamera::boundaryMax
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___boundaryMax_13;
	// System.Single BitBenderGames.MobileTouchCamera::camFollowFactor
	float ___camFollowFactor_14;
	// BitBenderGames.AutoScrollDampMode BitBenderGames.MobileTouchCamera::autoScrollDampMode
	int32_t ___autoScrollDampMode_15;
	// System.Single BitBenderGames.MobileTouchCamera::autoScrollDamp
	float ___autoScrollDamp_16;
	// UnityEngine.AnimationCurve BitBenderGames.MobileTouchCamera::autoScrollDampCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___autoScrollDampCurve_17;
	// System.Single BitBenderGames.MobileTouchCamera::groundLevelOffset
	float ___groundLevelOffset_18;
	// System.Boolean BitBenderGames.MobileTouchCamera::enableRotation
	bool ___enableRotation_19;
	// System.Boolean BitBenderGames.MobileTouchCamera::enableTilt
	bool ___enableTilt_20;
	// System.Single BitBenderGames.MobileTouchCamera::tiltAngleMin
	float ___tiltAngleMin_21;
	// System.Single BitBenderGames.MobileTouchCamera::tiltAngleMax
	float ___tiltAngleMax_22;
	// System.Boolean BitBenderGames.MobileTouchCamera::enableZoomTilt
	bool ___enableZoomTilt_23;
	// System.Single BitBenderGames.MobileTouchCamera::zoomTiltAngleMin
	float ___zoomTiltAngleMin_24;
	// System.Single BitBenderGames.MobileTouchCamera::zoomTiltAngleMax
	float ___zoomTiltAngleMax_25;
	// BitBenderGames.UnityEventWithRaycastHit BitBenderGames.MobileTouchCamera::OnPickItem
	UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * ___OnPickItem_26;
	// BitBenderGames.UnityEventWithRaycastHit2D BitBenderGames.MobileTouchCamera::OnPickItem2D
	UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * ___OnPickItem2D_27;
	// BitBenderGames.UnityEventWithRaycastHit BitBenderGames.MobileTouchCamera::OnPickItemDoubleClick
	UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * ___OnPickItemDoubleClick_28;
	// BitBenderGames.UnityEventWithRaycastHit2D BitBenderGames.MobileTouchCamera::OnPickItem2DDoubleClick
	UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * ___OnPickItem2DDoubleClick_29;
	// BitBenderGames.TouchInputController BitBenderGames.MobileTouchCamera::touchInputController
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * ___touchInputController_30;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::dragStartCamPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dragStartCamPos_31;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::cameraScrollVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraScrollVelocity_32;
	// System.Single BitBenderGames.MobileTouchCamera::pinchStartCamZoomSize
	float ___pinchStartCamZoomSize_33;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::pinchStartIntersectionCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchStartIntersectionCenter_34;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::pinchCenterCurrent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchCenterCurrent_35;
	// System.Single BitBenderGames.MobileTouchCamera::pinchDistanceCurrent
	float ___pinchDistanceCurrent_36;
	// System.Single BitBenderGames.MobileTouchCamera::pinchAngleCurrent
	float ___pinchAngleCurrent_37;
	// System.Single BitBenderGames.MobileTouchCamera::pinchDistanceStart
	float ___pinchDistanceStart_38;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::pinchCenterCurrentLerp
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pinchCenterCurrentLerp_39;
	// System.Single BitBenderGames.MobileTouchCamera::pinchDistanceCurrentLerp
	float ___pinchDistanceCurrentLerp_40;
	// System.Single BitBenderGames.MobileTouchCamera::pinchAngleCurrentLerp
	float ___pinchAngleCurrentLerp_41;
	// System.Boolean BitBenderGames.MobileTouchCamera::isRotationLock
	bool ___isRotationLock_42;
	// System.Boolean BitBenderGames.MobileTouchCamera::isRotationActivated
	bool ___isRotationActivated_43;
	// System.Single BitBenderGames.MobileTouchCamera::pinchAngleLastFrame
	float ___pinchAngleLastFrame_44;
	// System.Single BitBenderGames.MobileTouchCamera::pinchTiltCurrent
	float ___pinchTiltCurrent_45;
	// System.Single BitBenderGames.MobileTouchCamera::pinchTiltAccumulated
	float ___pinchTiltAccumulated_46;
	// System.Boolean BitBenderGames.MobileTouchCamera::isTiltModeEvaluated
	bool ___isTiltModeEvaluated_47;
	// System.Single BitBenderGames.MobileTouchCamera::pinchTiltLastFrame
	float ___pinchTiltLastFrame_48;
	// System.Boolean BitBenderGames.MobileTouchCamera::isPinchTiltMode
	bool ___isPinchTiltMode_49;
	// System.Single BitBenderGames.MobileTouchCamera::timeRealDragStop
	float ___timeRealDragStop_50;
	// System.Boolean BitBenderGames.MobileTouchCamera::<IsPinching>k__BackingField
	bool ___U3CIsPinchingU3Ek__BackingField_51;
	// System.Boolean BitBenderGames.MobileTouchCamera::<IsDragging>k__BackingField
	bool ___U3CIsDraggingU3Ek__BackingField_52;
	// System.Boolean BitBenderGames.MobileTouchCamera::expertModeEnabled
	bool ___expertModeEnabled_53;
	// System.Single BitBenderGames.MobileTouchCamera::zoomBackSpringFactor
	float ___zoomBackSpringFactor_54;
	// System.Single BitBenderGames.MobileTouchCamera::dragBackSpringFactor
	float ___dragBackSpringFactor_55;
	// System.Single BitBenderGames.MobileTouchCamera::autoScrollVelocityMax
	float ___autoScrollVelocityMax_56;
	// System.Single BitBenderGames.MobileTouchCamera::dampFactorTimeMultiplier
	float ___dampFactorTimeMultiplier_57;
	// System.Boolean BitBenderGames.MobileTouchCamera::isPinchModeExclusive
	bool ___isPinchModeExclusive_58;
	// System.Single BitBenderGames.MobileTouchCamera::customZoomSensitivity
	float ___customZoomSensitivity_59;
	// UnityEngine.TerrainCollider BitBenderGames.MobileTouchCamera::terrainCollider
	TerrainCollider_t43B44EB62E016F4B076CF3A49D0C3F0EA71398C6 * ___terrainCollider_60;
	// UnityEngine.Transform BitBenderGames.MobileTouchCamera::cameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform_61;
	// System.Boolean BitBenderGames.MobileTouchCamera::is2dOverdragMarginEnabled
	bool ___is2dOverdragMarginEnabled_62;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchCamera::camOverdragMargin2d
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___camOverdragMargin2d_63;
	// System.Single BitBenderGames.MobileTouchCamera::rotationDetectionDeltaThreshold
	float ___rotationDetectionDeltaThreshold_64;
	// System.Single BitBenderGames.MobileTouchCamera::rotationMinPinchDistance
	float ___rotationMinPinchDistance_65;
	// System.Single BitBenderGames.MobileTouchCamera::rotationLockThreshold
	float ___rotationLockThreshold_66;
	// System.Single BitBenderGames.MobileTouchCamera::pinchModeDetectionMoveTreshold
	float ___pinchModeDetectionMoveTreshold_67;
	// System.Single BitBenderGames.MobileTouchCamera::pinchTiltModeThreshold
	float ___pinchTiltModeThreshold_68;
	// System.Single BitBenderGames.MobileTouchCamera::pinchTiltSpeed
	float ___pinchTiltSpeed_69;
	// System.Boolean BitBenderGames.MobileTouchCamera::isStarted
	bool ___isStarted_70;
	// UnityEngine.Camera BitBenderGames.MobileTouchCamera::<Cam>k__BackingField
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___U3CCamU3Ek__BackingField_71;
	// System.Boolean BitBenderGames.MobileTouchCamera::isDraggingSceneObject
	bool ___isDraggingSceneObject_72;
	// UnityEngine.Plane BitBenderGames.MobileTouchCamera::refPlaneXY
	Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___refPlaneXY_73;
	// UnityEngine.Plane BitBenderGames.MobileTouchCamera::refPlaneXZ
	Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___refPlaneXZ_74;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BitBenderGames.MobileTouchCamera::<DragCameraMoveVector>k__BackingField
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___U3CDragCameraMoveVectorU3Ek__BackingField_75;
	// UnityEngine.Vector3 BitBenderGames.MobileTouchCamera::targetPositionClamped
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPositionClamped_79;
	// System.Boolean BitBenderGames.MobileTouchCamera::<IsSmoothingEnabled>k__BackingField
	bool ___U3CIsSmoothingEnabledU3Ek__BackingField_80;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchCamera::<CamPosMin>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CCamPosMinU3Ek__BackingField_81;
	// UnityEngine.Vector2 BitBenderGames.MobileTouchCamera::<CamPosMax>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CCamPosMaxU3Ek__BackingField_82;
	// System.Boolean BitBenderGames.MobileTouchCamera::showHorizonError
	bool ___showHorizonError_83;
	// System.Boolean BitBenderGames.MobileTouchCamera::enableOvertiltSpring
	bool ___enableOvertiltSpring_84;
	// System.Single BitBenderGames.MobileTouchCamera::camOvertiltMargin
	float ___camOvertiltMargin_85;
	// System.Single BitBenderGames.MobileTouchCamera::tiltBackSpringFactor
	float ___tiltBackSpringFactor_86;
	// System.Single BitBenderGames.MobileTouchCamera::minOvertiltSpringPositionThreshold
	float ___minOvertiltSpringPositionThreshold_87;
	// System.Boolean BitBenderGames.MobileTouchCamera::useUntransformedCamBoundary
	bool ___useUntransformedCamBoundary_88;
	// System.Single BitBenderGames.MobileTouchCamera::maxHorizonFallbackDistance
	float ___maxHorizonFallbackDistance_89;

public:
	inline static int32_t get_offset_of_cameraAxes_6() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___cameraAxes_6)); }
	inline int32_t get_cameraAxes_6() const { return ___cameraAxes_6; }
	inline int32_t* get_address_of_cameraAxes_6() { return &___cameraAxes_6; }
	inline void set_cameraAxes_6(int32_t value)
	{
		___cameraAxes_6 = value;
	}

	inline static int32_t get_offset_of_perspectiveZoomMode_7() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___perspectiveZoomMode_7)); }
	inline int32_t get_perspectiveZoomMode_7() const { return ___perspectiveZoomMode_7; }
	inline int32_t* get_address_of_perspectiveZoomMode_7() { return &___perspectiveZoomMode_7; }
	inline void set_perspectiveZoomMode_7(int32_t value)
	{
		___perspectiveZoomMode_7 = value;
	}

	inline static int32_t get_offset_of_camZoomMin_8() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camZoomMin_8)); }
	inline float get_camZoomMin_8() const { return ___camZoomMin_8; }
	inline float* get_address_of_camZoomMin_8() { return &___camZoomMin_8; }
	inline void set_camZoomMin_8(float value)
	{
		___camZoomMin_8 = value;
	}

	inline static int32_t get_offset_of_camZoomMax_9() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camZoomMax_9)); }
	inline float get_camZoomMax_9() const { return ___camZoomMax_9; }
	inline float* get_address_of_camZoomMax_9() { return &___camZoomMax_9; }
	inline void set_camZoomMax_9(float value)
	{
		___camZoomMax_9 = value;
	}

	inline static int32_t get_offset_of_camOverzoomMargin_10() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camOverzoomMargin_10)); }
	inline float get_camOverzoomMargin_10() const { return ___camOverzoomMargin_10; }
	inline float* get_address_of_camOverzoomMargin_10() { return &___camOverzoomMargin_10; }
	inline void set_camOverzoomMargin_10(float value)
	{
		___camOverzoomMargin_10 = value;
	}

	inline static int32_t get_offset_of_camOverdragMargin_11() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camOverdragMargin_11)); }
	inline float get_camOverdragMargin_11() const { return ___camOverdragMargin_11; }
	inline float* get_address_of_camOverdragMargin_11() { return &___camOverdragMargin_11; }
	inline void set_camOverdragMargin_11(float value)
	{
		___camOverdragMargin_11 = value;
	}

	inline static int32_t get_offset_of_boundaryMin_12() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___boundaryMin_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_boundaryMin_12() const { return ___boundaryMin_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_boundaryMin_12() { return &___boundaryMin_12; }
	inline void set_boundaryMin_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___boundaryMin_12 = value;
	}

	inline static int32_t get_offset_of_boundaryMax_13() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___boundaryMax_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_boundaryMax_13() const { return ___boundaryMax_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_boundaryMax_13() { return &___boundaryMax_13; }
	inline void set_boundaryMax_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___boundaryMax_13 = value;
	}

	inline static int32_t get_offset_of_camFollowFactor_14() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camFollowFactor_14)); }
	inline float get_camFollowFactor_14() const { return ___camFollowFactor_14; }
	inline float* get_address_of_camFollowFactor_14() { return &___camFollowFactor_14; }
	inline void set_camFollowFactor_14(float value)
	{
		___camFollowFactor_14 = value;
	}

	inline static int32_t get_offset_of_autoScrollDampMode_15() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___autoScrollDampMode_15)); }
	inline int32_t get_autoScrollDampMode_15() const { return ___autoScrollDampMode_15; }
	inline int32_t* get_address_of_autoScrollDampMode_15() { return &___autoScrollDampMode_15; }
	inline void set_autoScrollDampMode_15(int32_t value)
	{
		___autoScrollDampMode_15 = value;
	}

	inline static int32_t get_offset_of_autoScrollDamp_16() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___autoScrollDamp_16)); }
	inline float get_autoScrollDamp_16() const { return ___autoScrollDamp_16; }
	inline float* get_address_of_autoScrollDamp_16() { return &___autoScrollDamp_16; }
	inline void set_autoScrollDamp_16(float value)
	{
		___autoScrollDamp_16 = value;
	}

	inline static int32_t get_offset_of_autoScrollDampCurve_17() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___autoScrollDampCurve_17)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_autoScrollDampCurve_17() const { return ___autoScrollDampCurve_17; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_autoScrollDampCurve_17() { return &___autoScrollDampCurve_17; }
	inline void set_autoScrollDampCurve_17(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___autoScrollDampCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___autoScrollDampCurve_17), value);
	}

	inline static int32_t get_offset_of_groundLevelOffset_18() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___groundLevelOffset_18)); }
	inline float get_groundLevelOffset_18() const { return ___groundLevelOffset_18; }
	inline float* get_address_of_groundLevelOffset_18() { return &___groundLevelOffset_18; }
	inline void set_groundLevelOffset_18(float value)
	{
		___groundLevelOffset_18 = value;
	}

	inline static int32_t get_offset_of_enableRotation_19() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___enableRotation_19)); }
	inline bool get_enableRotation_19() const { return ___enableRotation_19; }
	inline bool* get_address_of_enableRotation_19() { return &___enableRotation_19; }
	inline void set_enableRotation_19(bool value)
	{
		___enableRotation_19 = value;
	}

	inline static int32_t get_offset_of_enableTilt_20() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___enableTilt_20)); }
	inline bool get_enableTilt_20() const { return ___enableTilt_20; }
	inline bool* get_address_of_enableTilt_20() { return &___enableTilt_20; }
	inline void set_enableTilt_20(bool value)
	{
		___enableTilt_20 = value;
	}

	inline static int32_t get_offset_of_tiltAngleMin_21() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___tiltAngleMin_21)); }
	inline float get_tiltAngleMin_21() const { return ___tiltAngleMin_21; }
	inline float* get_address_of_tiltAngleMin_21() { return &___tiltAngleMin_21; }
	inline void set_tiltAngleMin_21(float value)
	{
		___tiltAngleMin_21 = value;
	}

	inline static int32_t get_offset_of_tiltAngleMax_22() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___tiltAngleMax_22)); }
	inline float get_tiltAngleMax_22() const { return ___tiltAngleMax_22; }
	inline float* get_address_of_tiltAngleMax_22() { return &___tiltAngleMax_22; }
	inline void set_tiltAngleMax_22(float value)
	{
		___tiltAngleMax_22 = value;
	}

	inline static int32_t get_offset_of_enableZoomTilt_23() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___enableZoomTilt_23)); }
	inline bool get_enableZoomTilt_23() const { return ___enableZoomTilt_23; }
	inline bool* get_address_of_enableZoomTilt_23() { return &___enableZoomTilt_23; }
	inline void set_enableZoomTilt_23(bool value)
	{
		___enableZoomTilt_23 = value;
	}

	inline static int32_t get_offset_of_zoomTiltAngleMin_24() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___zoomTiltAngleMin_24)); }
	inline float get_zoomTiltAngleMin_24() const { return ___zoomTiltAngleMin_24; }
	inline float* get_address_of_zoomTiltAngleMin_24() { return &___zoomTiltAngleMin_24; }
	inline void set_zoomTiltAngleMin_24(float value)
	{
		___zoomTiltAngleMin_24 = value;
	}

	inline static int32_t get_offset_of_zoomTiltAngleMax_25() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___zoomTiltAngleMax_25)); }
	inline float get_zoomTiltAngleMax_25() const { return ___zoomTiltAngleMax_25; }
	inline float* get_address_of_zoomTiltAngleMax_25() { return &___zoomTiltAngleMax_25; }
	inline void set_zoomTiltAngleMax_25(float value)
	{
		___zoomTiltAngleMax_25 = value;
	}

	inline static int32_t get_offset_of_OnPickItem_26() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___OnPickItem_26)); }
	inline UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * get_OnPickItem_26() const { return ___OnPickItem_26; }
	inline UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 ** get_address_of_OnPickItem_26() { return &___OnPickItem_26; }
	inline void set_OnPickItem_26(UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * value)
	{
		___OnPickItem_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickItem_26), value);
	}

	inline static int32_t get_offset_of_OnPickItem2D_27() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___OnPickItem2D_27)); }
	inline UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * get_OnPickItem2D_27() const { return ___OnPickItem2D_27; }
	inline UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 ** get_address_of_OnPickItem2D_27() { return &___OnPickItem2D_27; }
	inline void set_OnPickItem2D_27(UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * value)
	{
		___OnPickItem2D_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickItem2D_27), value);
	}

	inline static int32_t get_offset_of_OnPickItemDoubleClick_28() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___OnPickItemDoubleClick_28)); }
	inline UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * get_OnPickItemDoubleClick_28() const { return ___OnPickItemDoubleClick_28; }
	inline UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 ** get_address_of_OnPickItemDoubleClick_28() { return &___OnPickItemDoubleClick_28; }
	inline void set_OnPickItemDoubleClick_28(UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592 * value)
	{
		___OnPickItemDoubleClick_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickItemDoubleClick_28), value);
	}

	inline static int32_t get_offset_of_OnPickItem2DDoubleClick_29() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___OnPickItem2DDoubleClick_29)); }
	inline UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * get_OnPickItem2DDoubleClick_29() const { return ___OnPickItem2DDoubleClick_29; }
	inline UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 ** get_address_of_OnPickItem2DDoubleClick_29() { return &___OnPickItem2DDoubleClick_29; }
	inline void set_OnPickItem2DDoubleClick_29(UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3 * value)
	{
		___OnPickItem2DDoubleClick_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickItem2DDoubleClick_29), value);
	}

	inline static int32_t get_offset_of_touchInputController_30() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___touchInputController_30)); }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * get_touchInputController_30() const { return ___touchInputController_30; }
	inline TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F ** get_address_of_touchInputController_30() { return &___touchInputController_30; }
	inline void set_touchInputController_30(TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F * value)
	{
		___touchInputController_30 = value;
		Il2CppCodeGenWriteBarrier((&___touchInputController_30), value);
	}

	inline static int32_t get_offset_of_dragStartCamPos_31() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___dragStartCamPos_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dragStartCamPos_31() const { return ___dragStartCamPos_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dragStartCamPos_31() { return &___dragStartCamPos_31; }
	inline void set_dragStartCamPos_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dragStartCamPos_31 = value;
	}

	inline static int32_t get_offset_of_cameraScrollVelocity_32() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___cameraScrollVelocity_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraScrollVelocity_32() const { return ___cameraScrollVelocity_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraScrollVelocity_32() { return &___cameraScrollVelocity_32; }
	inline void set_cameraScrollVelocity_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraScrollVelocity_32 = value;
	}

	inline static int32_t get_offset_of_pinchStartCamZoomSize_33() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchStartCamZoomSize_33)); }
	inline float get_pinchStartCamZoomSize_33() const { return ___pinchStartCamZoomSize_33; }
	inline float* get_address_of_pinchStartCamZoomSize_33() { return &___pinchStartCamZoomSize_33; }
	inline void set_pinchStartCamZoomSize_33(float value)
	{
		___pinchStartCamZoomSize_33 = value;
	}

	inline static int32_t get_offset_of_pinchStartIntersectionCenter_34() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchStartIntersectionCenter_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchStartIntersectionCenter_34() const { return ___pinchStartIntersectionCenter_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchStartIntersectionCenter_34() { return &___pinchStartIntersectionCenter_34; }
	inline void set_pinchStartIntersectionCenter_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchStartIntersectionCenter_34 = value;
	}

	inline static int32_t get_offset_of_pinchCenterCurrent_35() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchCenterCurrent_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchCenterCurrent_35() const { return ___pinchCenterCurrent_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchCenterCurrent_35() { return &___pinchCenterCurrent_35; }
	inline void set_pinchCenterCurrent_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchCenterCurrent_35 = value;
	}

	inline static int32_t get_offset_of_pinchDistanceCurrent_36() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchDistanceCurrent_36)); }
	inline float get_pinchDistanceCurrent_36() const { return ___pinchDistanceCurrent_36; }
	inline float* get_address_of_pinchDistanceCurrent_36() { return &___pinchDistanceCurrent_36; }
	inline void set_pinchDistanceCurrent_36(float value)
	{
		___pinchDistanceCurrent_36 = value;
	}

	inline static int32_t get_offset_of_pinchAngleCurrent_37() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchAngleCurrent_37)); }
	inline float get_pinchAngleCurrent_37() const { return ___pinchAngleCurrent_37; }
	inline float* get_address_of_pinchAngleCurrent_37() { return &___pinchAngleCurrent_37; }
	inline void set_pinchAngleCurrent_37(float value)
	{
		___pinchAngleCurrent_37 = value;
	}

	inline static int32_t get_offset_of_pinchDistanceStart_38() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchDistanceStart_38)); }
	inline float get_pinchDistanceStart_38() const { return ___pinchDistanceStart_38; }
	inline float* get_address_of_pinchDistanceStart_38() { return &___pinchDistanceStart_38; }
	inline void set_pinchDistanceStart_38(float value)
	{
		___pinchDistanceStart_38 = value;
	}

	inline static int32_t get_offset_of_pinchCenterCurrentLerp_39() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchCenterCurrentLerp_39)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pinchCenterCurrentLerp_39() const { return ___pinchCenterCurrentLerp_39; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pinchCenterCurrentLerp_39() { return &___pinchCenterCurrentLerp_39; }
	inline void set_pinchCenterCurrentLerp_39(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pinchCenterCurrentLerp_39 = value;
	}

	inline static int32_t get_offset_of_pinchDistanceCurrentLerp_40() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchDistanceCurrentLerp_40)); }
	inline float get_pinchDistanceCurrentLerp_40() const { return ___pinchDistanceCurrentLerp_40; }
	inline float* get_address_of_pinchDistanceCurrentLerp_40() { return &___pinchDistanceCurrentLerp_40; }
	inline void set_pinchDistanceCurrentLerp_40(float value)
	{
		___pinchDistanceCurrentLerp_40 = value;
	}

	inline static int32_t get_offset_of_pinchAngleCurrentLerp_41() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchAngleCurrentLerp_41)); }
	inline float get_pinchAngleCurrentLerp_41() const { return ___pinchAngleCurrentLerp_41; }
	inline float* get_address_of_pinchAngleCurrentLerp_41() { return &___pinchAngleCurrentLerp_41; }
	inline void set_pinchAngleCurrentLerp_41(float value)
	{
		___pinchAngleCurrentLerp_41 = value;
	}

	inline static int32_t get_offset_of_isRotationLock_42() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isRotationLock_42)); }
	inline bool get_isRotationLock_42() const { return ___isRotationLock_42; }
	inline bool* get_address_of_isRotationLock_42() { return &___isRotationLock_42; }
	inline void set_isRotationLock_42(bool value)
	{
		___isRotationLock_42 = value;
	}

	inline static int32_t get_offset_of_isRotationActivated_43() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isRotationActivated_43)); }
	inline bool get_isRotationActivated_43() const { return ___isRotationActivated_43; }
	inline bool* get_address_of_isRotationActivated_43() { return &___isRotationActivated_43; }
	inline void set_isRotationActivated_43(bool value)
	{
		___isRotationActivated_43 = value;
	}

	inline static int32_t get_offset_of_pinchAngleLastFrame_44() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchAngleLastFrame_44)); }
	inline float get_pinchAngleLastFrame_44() const { return ___pinchAngleLastFrame_44; }
	inline float* get_address_of_pinchAngleLastFrame_44() { return &___pinchAngleLastFrame_44; }
	inline void set_pinchAngleLastFrame_44(float value)
	{
		___pinchAngleLastFrame_44 = value;
	}

	inline static int32_t get_offset_of_pinchTiltCurrent_45() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchTiltCurrent_45)); }
	inline float get_pinchTiltCurrent_45() const { return ___pinchTiltCurrent_45; }
	inline float* get_address_of_pinchTiltCurrent_45() { return &___pinchTiltCurrent_45; }
	inline void set_pinchTiltCurrent_45(float value)
	{
		___pinchTiltCurrent_45 = value;
	}

	inline static int32_t get_offset_of_pinchTiltAccumulated_46() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchTiltAccumulated_46)); }
	inline float get_pinchTiltAccumulated_46() const { return ___pinchTiltAccumulated_46; }
	inline float* get_address_of_pinchTiltAccumulated_46() { return &___pinchTiltAccumulated_46; }
	inline void set_pinchTiltAccumulated_46(float value)
	{
		___pinchTiltAccumulated_46 = value;
	}

	inline static int32_t get_offset_of_isTiltModeEvaluated_47() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isTiltModeEvaluated_47)); }
	inline bool get_isTiltModeEvaluated_47() const { return ___isTiltModeEvaluated_47; }
	inline bool* get_address_of_isTiltModeEvaluated_47() { return &___isTiltModeEvaluated_47; }
	inline void set_isTiltModeEvaluated_47(bool value)
	{
		___isTiltModeEvaluated_47 = value;
	}

	inline static int32_t get_offset_of_pinchTiltLastFrame_48() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchTiltLastFrame_48)); }
	inline float get_pinchTiltLastFrame_48() const { return ___pinchTiltLastFrame_48; }
	inline float* get_address_of_pinchTiltLastFrame_48() { return &___pinchTiltLastFrame_48; }
	inline void set_pinchTiltLastFrame_48(float value)
	{
		___pinchTiltLastFrame_48 = value;
	}

	inline static int32_t get_offset_of_isPinchTiltMode_49() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isPinchTiltMode_49)); }
	inline bool get_isPinchTiltMode_49() const { return ___isPinchTiltMode_49; }
	inline bool* get_address_of_isPinchTiltMode_49() { return &___isPinchTiltMode_49; }
	inline void set_isPinchTiltMode_49(bool value)
	{
		___isPinchTiltMode_49 = value;
	}

	inline static int32_t get_offset_of_timeRealDragStop_50() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___timeRealDragStop_50)); }
	inline float get_timeRealDragStop_50() const { return ___timeRealDragStop_50; }
	inline float* get_address_of_timeRealDragStop_50() { return &___timeRealDragStop_50; }
	inline void set_timeRealDragStop_50(float value)
	{
		___timeRealDragStop_50 = value;
	}

	inline static int32_t get_offset_of_U3CIsPinchingU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CIsPinchingU3Ek__BackingField_51)); }
	inline bool get_U3CIsPinchingU3Ek__BackingField_51() const { return ___U3CIsPinchingU3Ek__BackingField_51; }
	inline bool* get_address_of_U3CIsPinchingU3Ek__BackingField_51() { return &___U3CIsPinchingU3Ek__BackingField_51; }
	inline void set_U3CIsPinchingU3Ek__BackingField_51(bool value)
	{
		___U3CIsPinchingU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CIsDraggingU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CIsDraggingU3Ek__BackingField_52)); }
	inline bool get_U3CIsDraggingU3Ek__BackingField_52() const { return ___U3CIsDraggingU3Ek__BackingField_52; }
	inline bool* get_address_of_U3CIsDraggingU3Ek__BackingField_52() { return &___U3CIsDraggingU3Ek__BackingField_52; }
	inline void set_U3CIsDraggingU3Ek__BackingField_52(bool value)
	{
		___U3CIsDraggingU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_expertModeEnabled_53() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___expertModeEnabled_53)); }
	inline bool get_expertModeEnabled_53() const { return ___expertModeEnabled_53; }
	inline bool* get_address_of_expertModeEnabled_53() { return &___expertModeEnabled_53; }
	inline void set_expertModeEnabled_53(bool value)
	{
		___expertModeEnabled_53 = value;
	}

	inline static int32_t get_offset_of_zoomBackSpringFactor_54() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___zoomBackSpringFactor_54)); }
	inline float get_zoomBackSpringFactor_54() const { return ___zoomBackSpringFactor_54; }
	inline float* get_address_of_zoomBackSpringFactor_54() { return &___zoomBackSpringFactor_54; }
	inline void set_zoomBackSpringFactor_54(float value)
	{
		___zoomBackSpringFactor_54 = value;
	}

	inline static int32_t get_offset_of_dragBackSpringFactor_55() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___dragBackSpringFactor_55)); }
	inline float get_dragBackSpringFactor_55() const { return ___dragBackSpringFactor_55; }
	inline float* get_address_of_dragBackSpringFactor_55() { return &___dragBackSpringFactor_55; }
	inline void set_dragBackSpringFactor_55(float value)
	{
		___dragBackSpringFactor_55 = value;
	}

	inline static int32_t get_offset_of_autoScrollVelocityMax_56() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___autoScrollVelocityMax_56)); }
	inline float get_autoScrollVelocityMax_56() const { return ___autoScrollVelocityMax_56; }
	inline float* get_address_of_autoScrollVelocityMax_56() { return &___autoScrollVelocityMax_56; }
	inline void set_autoScrollVelocityMax_56(float value)
	{
		___autoScrollVelocityMax_56 = value;
	}

	inline static int32_t get_offset_of_dampFactorTimeMultiplier_57() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___dampFactorTimeMultiplier_57)); }
	inline float get_dampFactorTimeMultiplier_57() const { return ___dampFactorTimeMultiplier_57; }
	inline float* get_address_of_dampFactorTimeMultiplier_57() { return &___dampFactorTimeMultiplier_57; }
	inline void set_dampFactorTimeMultiplier_57(float value)
	{
		___dampFactorTimeMultiplier_57 = value;
	}

	inline static int32_t get_offset_of_isPinchModeExclusive_58() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isPinchModeExclusive_58)); }
	inline bool get_isPinchModeExclusive_58() const { return ___isPinchModeExclusive_58; }
	inline bool* get_address_of_isPinchModeExclusive_58() { return &___isPinchModeExclusive_58; }
	inline void set_isPinchModeExclusive_58(bool value)
	{
		___isPinchModeExclusive_58 = value;
	}

	inline static int32_t get_offset_of_customZoomSensitivity_59() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___customZoomSensitivity_59)); }
	inline float get_customZoomSensitivity_59() const { return ___customZoomSensitivity_59; }
	inline float* get_address_of_customZoomSensitivity_59() { return &___customZoomSensitivity_59; }
	inline void set_customZoomSensitivity_59(float value)
	{
		___customZoomSensitivity_59 = value;
	}

	inline static int32_t get_offset_of_terrainCollider_60() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___terrainCollider_60)); }
	inline TerrainCollider_t43B44EB62E016F4B076CF3A49D0C3F0EA71398C6 * get_terrainCollider_60() const { return ___terrainCollider_60; }
	inline TerrainCollider_t43B44EB62E016F4B076CF3A49D0C3F0EA71398C6 ** get_address_of_terrainCollider_60() { return &___terrainCollider_60; }
	inline void set_terrainCollider_60(TerrainCollider_t43B44EB62E016F4B076CF3A49D0C3F0EA71398C6 * value)
	{
		___terrainCollider_60 = value;
		Il2CppCodeGenWriteBarrier((&___terrainCollider_60), value);
	}

	inline static int32_t get_offset_of_cameraTransform_61() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___cameraTransform_61)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTransform_61() const { return ___cameraTransform_61; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTransform_61() { return &___cameraTransform_61; }
	inline void set_cameraTransform_61(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTransform_61 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_61), value);
	}

	inline static int32_t get_offset_of_is2dOverdragMarginEnabled_62() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___is2dOverdragMarginEnabled_62)); }
	inline bool get_is2dOverdragMarginEnabled_62() const { return ___is2dOverdragMarginEnabled_62; }
	inline bool* get_address_of_is2dOverdragMarginEnabled_62() { return &___is2dOverdragMarginEnabled_62; }
	inline void set_is2dOverdragMarginEnabled_62(bool value)
	{
		___is2dOverdragMarginEnabled_62 = value;
	}

	inline static int32_t get_offset_of_camOverdragMargin2d_63() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camOverdragMargin2d_63)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_camOverdragMargin2d_63() const { return ___camOverdragMargin2d_63; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_camOverdragMargin2d_63() { return &___camOverdragMargin2d_63; }
	inline void set_camOverdragMargin2d_63(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___camOverdragMargin2d_63 = value;
	}

	inline static int32_t get_offset_of_rotationDetectionDeltaThreshold_64() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___rotationDetectionDeltaThreshold_64)); }
	inline float get_rotationDetectionDeltaThreshold_64() const { return ___rotationDetectionDeltaThreshold_64; }
	inline float* get_address_of_rotationDetectionDeltaThreshold_64() { return &___rotationDetectionDeltaThreshold_64; }
	inline void set_rotationDetectionDeltaThreshold_64(float value)
	{
		___rotationDetectionDeltaThreshold_64 = value;
	}

	inline static int32_t get_offset_of_rotationMinPinchDistance_65() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___rotationMinPinchDistance_65)); }
	inline float get_rotationMinPinchDistance_65() const { return ___rotationMinPinchDistance_65; }
	inline float* get_address_of_rotationMinPinchDistance_65() { return &___rotationMinPinchDistance_65; }
	inline void set_rotationMinPinchDistance_65(float value)
	{
		___rotationMinPinchDistance_65 = value;
	}

	inline static int32_t get_offset_of_rotationLockThreshold_66() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___rotationLockThreshold_66)); }
	inline float get_rotationLockThreshold_66() const { return ___rotationLockThreshold_66; }
	inline float* get_address_of_rotationLockThreshold_66() { return &___rotationLockThreshold_66; }
	inline void set_rotationLockThreshold_66(float value)
	{
		___rotationLockThreshold_66 = value;
	}

	inline static int32_t get_offset_of_pinchModeDetectionMoveTreshold_67() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchModeDetectionMoveTreshold_67)); }
	inline float get_pinchModeDetectionMoveTreshold_67() const { return ___pinchModeDetectionMoveTreshold_67; }
	inline float* get_address_of_pinchModeDetectionMoveTreshold_67() { return &___pinchModeDetectionMoveTreshold_67; }
	inline void set_pinchModeDetectionMoveTreshold_67(float value)
	{
		___pinchModeDetectionMoveTreshold_67 = value;
	}

	inline static int32_t get_offset_of_pinchTiltModeThreshold_68() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchTiltModeThreshold_68)); }
	inline float get_pinchTiltModeThreshold_68() const { return ___pinchTiltModeThreshold_68; }
	inline float* get_address_of_pinchTiltModeThreshold_68() { return &___pinchTiltModeThreshold_68; }
	inline void set_pinchTiltModeThreshold_68(float value)
	{
		___pinchTiltModeThreshold_68 = value;
	}

	inline static int32_t get_offset_of_pinchTiltSpeed_69() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___pinchTiltSpeed_69)); }
	inline float get_pinchTiltSpeed_69() const { return ___pinchTiltSpeed_69; }
	inline float* get_address_of_pinchTiltSpeed_69() { return &___pinchTiltSpeed_69; }
	inline void set_pinchTiltSpeed_69(float value)
	{
		___pinchTiltSpeed_69 = value;
	}

	inline static int32_t get_offset_of_isStarted_70() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isStarted_70)); }
	inline bool get_isStarted_70() const { return ___isStarted_70; }
	inline bool* get_address_of_isStarted_70() { return &___isStarted_70; }
	inline void set_isStarted_70(bool value)
	{
		___isStarted_70 = value;
	}

	inline static int32_t get_offset_of_U3CCamU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CCamU3Ek__BackingField_71)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_U3CCamU3Ek__BackingField_71() const { return ___U3CCamU3Ek__BackingField_71; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_U3CCamU3Ek__BackingField_71() { return &___U3CCamU3Ek__BackingField_71; }
	inline void set_U3CCamU3Ek__BackingField_71(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___U3CCamU3Ek__BackingField_71 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamU3Ek__BackingField_71), value);
	}

	inline static int32_t get_offset_of_isDraggingSceneObject_72() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___isDraggingSceneObject_72)); }
	inline bool get_isDraggingSceneObject_72() const { return ___isDraggingSceneObject_72; }
	inline bool* get_address_of_isDraggingSceneObject_72() { return &___isDraggingSceneObject_72; }
	inline void set_isDraggingSceneObject_72(bool value)
	{
		___isDraggingSceneObject_72 = value;
	}

	inline static int32_t get_offset_of_refPlaneXY_73() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___refPlaneXY_73)); }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  get_refPlaneXY_73() const { return ___refPlaneXY_73; }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * get_address_of_refPlaneXY_73() { return &___refPlaneXY_73; }
	inline void set_refPlaneXY_73(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  value)
	{
		___refPlaneXY_73 = value;
	}

	inline static int32_t get_offset_of_refPlaneXZ_74() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___refPlaneXZ_74)); }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  get_refPlaneXZ_74() const { return ___refPlaneXZ_74; }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * get_address_of_refPlaneXZ_74() { return &___refPlaneXZ_74; }
	inline void set_refPlaneXZ_74(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  value)
	{
		___refPlaneXZ_74 = value;
	}

	inline static int32_t get_offset_of_U3CDragCameraMoveVectorU3Ek__BackingField_75() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CDragCameraMoveVectorU3Ek__BackingField_75)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_U3CDragCameraMoveVectorU3Ek__BackingField_75() const { return ___U3CDragCameraMoveVectorU3Ek__BackingField_75; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_U3CDragCameraMoveVectorU3Ek__BackingField_75() { return &___U3CDragCameraMoveVectorU3Ek__BackingField_75; }
	inline void set_U3CDragCameraMoveVectorU3Ek__BackingField_75(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___U3CDragCameraMoveVectorU3Ek__BackingField_75 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDragCameraMoveVectorU3Ek__BackingField_75), value);
	}

	inline static int32_t get_offset_of_targetPositionClamped_79() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___targetPositionClamped_79)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPositionClamped_79() const { return ___targetPositionClamped_79; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPositionClamped_79() { return &___targetPositionClamped_79; }
	inline void set_targetPositionClamped_79(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPositionClamped_79 = value;
	}

	inline static int32_t get_offset_of_U3CIsSmoothingEnabledU3Ek__BackingField_80() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CIsSmoothingEnabledU3Ek__BackingField_80)); }
	inline bool get_U3CIsSmoothingEnabledU3Ek__BackingField_80() const { return ___U3CIsSmoothingEnabledU3Ek__BackingField_80; }
	inline bool* get_address_of_U3CIsSmoothingEnabledU3Ek__BackingField_80() { return &___U3CIsSmoothingEnabledU3Ek__BackingField_80; }
	inline void set_U3CIsSmoothingEnabledU3Ek__BackingField_80(bool value)
	{
		___U3CIsSmoothingEnabledU3Ek__BackingField_80 = value;
	}

	inline static int32_t get_offset_of_U3CCamPosMinU3Ek__BackingField_81() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CCamPosMinU3Ek__BackingField_81)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CCamPosMinU3Ek__BackingField_81() const { return ___U3CCamPosMinU3Ek__BackingField_81; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CCamPosMinU3Ek__BackingField_81() { return &___U3CCamPosMinU3Ek__BackingField_81; }
	inline void set_U3CCamPosMinU3Ek__BackingField_81(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CCamPosMinU3Ek__BackingField_81 = value;
	}

	inline static int32_t get_offset_of_U3CCamPosMaxU3Ek__BackingField_82() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___U3CCamPosMaxU3Ek__BackingField_82)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CCamPosMaxU3Ek__BackingField_82() const { return ___U3CCamPosMaxU3Ek__BackingField_82; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CCamPosMaxU3Ek__BackingField_82() { return &___U3CCamPosMaxU3Ek__BackingField_82; }
	inline void set_U3CCamPosMaxU3Ek__BackingField_82(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CCamPosMaxU3Ek__BackingField_82 = value;
	}

	inline static int32_t get_offset_of_showHorizonError_83() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___showHorizonError_83)); }
	inline bool get_showHorizonError_83() const { return ___showHorizonError_83; }
	inline bool* get_address_of_showHorizonError_83() { return &___showHorizonError_83; }
	inline void set_showHorizonError_83(bool value)
	{
		___showHorizonError_83 = value;
	}

	inline static int32_t get_offset_of_enableOvertiltSpring_84() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___enableOvertiltSpring_84)); }
	inline bool get_enableOvertiltSpring_84() const { return ___enableOvertiltSpring_84; }
	inline bool* get_address_of_enableOvertiltSpring_84() { return &___enableOvertiltSpring_84; }
	inline void set_enableOvertiltSpring_84(bool value)
	{
		___enableOvertiltSpring_84 = value;
	}

	inline static int32_t get_offset_of_camOvertiltMargin_85() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___camOvertiltMargin_85)); }
	inline float get_camOvertiltMargin_85() const { return ___camOvertiltMargin_85; }
	inline float* get_address_of_camOvertiltMargin_85() { return &___camOvertiltMargin_85; }
	inline void set_camOvertiltMargin_85(float value)
	{
		___camOvertiltMargin_85 = value;
	}

	inline static int32_t get_offset_of_tiltBackSpringFactor_86() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___tiltBackSpringFactor_86)); }
	inline float get_tiltBackSpringFactor_86() const { return ___tiltBackSpringFactor_86; }
	inline float* get_address_of_tiltBackSpringFactor_86() { return &___tiltBackSpringFactor_86; }
	inline void set_tiltBackSpringFactor_86(float value)
	{
		___tiltBackSpringFactor_86 = value;
	}

	inline static int32_t get_offset_of_minOvertiltSpringPositionThreshold_87() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___minOvertiltSpringPositionThreshold_87)); }
	inline float get_minOvertiltSpringPositionThreshold_87() const { return ___minOvertiltSpringPositionThreshold_87; }
	inline float* get_address_of_minOvertiltSpringPositionThreshold_87() { return &___minOvertiltSpringPositionThreshold_87; }
	inline void set_minOvertiltSpringPositionThreshold_87(float value)
	{
		___minOvertiltSpringPositionThreshold_87 = value;
	}

	inline static int32_t get_offset_of_useUntransformedCamBoundary_88() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___useUntransformedCamBoundary_88)); }
	inline bool get_useUntransformedCamBoundary_88() const { return ___useUntransformedCamBoundary_88; }
	inline bool* get_address_of_useUntransformedCamBoundary_88() { return &___useUntransformedCamBoundary_88; }
	inline void set_useUntransformedCamBoundary_88(bool value)
	{
		___useUntransformedCamBoundary_88 = value;
	}

	inline static int32_t get_offset_of_maxHorizonFallbackDistance_89() { return static_cast<int32_t>(offsetof(MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA, ___maxHorizonFallbackDistance_89)); }
	inline float get_maxHorizonFallbackDistance_89() const { return ___maxHorizonFallbackDistance_89; }
	inline float* get_address_of_maxHorizonFallbackDistance_89() { return &___maxHorizonFallbackDistance_89; }
	inline void set_maxHorizonFallbackDistance_89(float value)
	{
		___maxHorizonFallbackDistance_89 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILETOUCHCAMERA_T88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA_H
#ifndef QUICKDRAG_T84C22B93C7A128A18DF62BC5DE05643F67AD549E_H
#define QUICKDRAG_T84C22B93C7A128A18DF62BC5DE05643F67AD549E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag
struct  QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickDrag_OnDragStart HedgehogTeam.EasyTouch.QuickDrag::onDragStart
	OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780 * ___onDragStart_21;
	// HedgehogTeam.EasyTouch.QuickDrag_OnDrag HedgehogTeam.EasyTouch.QuickDrag::onDrag
	OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE * ___onDrag_22;
	// HedgehogTeam.EasyTouch.QuickDrag_OnDragEnd HedgehogTeam.EasyTouch.QuickDrag::onDragEnd
	OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE * ___onDragEnd_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isStopOncollisionEnter
	bool ___isStopOncollisionEnter_24;
	// UnityEngine.Vector3 HedgehogTeam.EasyTouch.QuickDrag::deltaPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deltaPosition_25;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isOnDrag
	bool ___isOnDrag_26;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickDrag::lastGesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___lastGesture_27;

public:
	inline static int32_t get_offset_of_onDragStart_21() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___onDragStart_21)); }
	inline OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780 * get_onDragStart_21() const { return ___onDragStart_21; }
	inline OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780 ** get_address_of_onDragStart_21() { return &___onDragStart_21; }
	inline void set_onDragStart_21(OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780 * value)
	{
		___onDragStart_21 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_21), value);
	}

	inline static int32_t get_offset_of_onDrag_22() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___onDrag_22)); }
	inline OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE * get_onDrag_22() const { return ___onDrag_22; }
	inline OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE ** get_address_of_onDrag_22() { return &___onDrag_22; }
	inline void set_onDrag_22(OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE * value)
	{
		___onDrag_22 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_22), value);
	}

	inline static int32_t get_offset_of_onDragEnd_23() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___onDragEnd_23)); }
	inline OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE * get_onDragEnd_23() const { return ___onDragEnd_23; }
	inline OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE ** get_address_of_onDragEnd_23() { return &___onDragEnd_23; }
	inline void set_onDragEnd_23(OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE * value)
	{
		___onDragEnd_23 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_23), value);
	}

	inline static int32_t get_offset_of_isStopOncollisionEnter_24() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___isStopOncollisionEnter_24)); }
	inline bool get_isStopOncollisionEnter_24() const { return ___isStopOncollisionEnter_24; }
	inline bool* get_address_of_isStopOncollisionEnter_24() { return &___isStopOncollisionEnter_24; }
	inline void set_isStopOncollisionEnter_24(bool value)
	{
		___isStopOncollisionEnter_24 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_25() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___deltaPosition_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deltaPosition_25() const { return ___deltaPosition_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deltaPosition_25() { return &___deltaPosition_25; }
	inline void set_deltaPosition_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deltaPosition_25 = value;
	}

	inline static int32_t get_offset_of_isOnDrag_26() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___isOnDrag_26)); }
	inline bool get_isOnDrag_26() const { return ___isOnDrag_26; }
	inline bool* get_address_of_isOnDrag_26() { return &___isOnDrag_26; }
	inline void set_isOnDrag_26(bool value)
	{
		___isOnDrag_26 = value;
	}

	inline static int32_t get_offset_of_lastGesture_27() { return static_cast<int32_t>(offsetof(QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E, ___lastGesture_27)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get_lastGesture_27() const { return ___lastGesture_27; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of_lastGesture_27() { return &___lastGesture_27; }
	inline void set_lastGesture_27(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		___lastGesture_27 = value;
		Il2CppCodeGenWriteBarrier((&___lastGesture_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKDRAG_T84C22B93C7A128A18DF62BC5DE05643F67AD549E_H
#ifndef QUICKENTEROVEREXIST_T8E9956CB50FB84413EB886F8EDA4C5A3399C5E40_H
#define QUICKENTEROVEREXIST_T8E9956CB50FB84413EB886F8EDA4C5A3399C5E40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist
struct  QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchEnter HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchEnter
	OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13 * ___onTouchEnter_21;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchOver HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchOver
	OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015 * ___onTouchOver_22;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchExit HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchExit
	OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF * ___onTouchExit_23;
	// System.Boolean[] HedgehogTeam.EasyTouch.QuickEnterOverExist::fingerOver
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___fingerOver_24;

public:
	inline static int32_t get_offset_of_onTouchEnter_21() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40, ___onTouchEnter_21)); }
	inline OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13 * get_onTouchEnter_21() const { return ___onTouchEnter_21; }
	inline OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13 ** get_address_of_onTouchEnter_21() { return &___onTouchEnter_21; }
	inline void set_onTouchEnter_21(OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13 * value)
	{
		___onTouchEnter_21 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchEnter_21), value);
	}

	inline static int32_t get_offset_of_onTouchOver_22() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40, ___onTouchOver_22)); }
	inline OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015 * get_onTouchOver_22() const { return ___onTouchOver_22; }
	inline OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015 ** get_address_of_onTouchOver_22() { return &___onTouchOver_22; }
	inline void set_onTouchOver_22(OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015 * value)
	{
		___onTouchOver_22 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchOver_22), value);
	}

	inline static int32_t get_offset_of_onTouchExit_23() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40, ___onTouchExit_23)); }
	inline OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF * get_onTouchExit_23() const { return ___onTouchExit_23; }
	inline OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF ** get_address_of_onTouchExit_23() { return &___onTouchExit_23; }
	inline void set_onTouchExit_23(OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF * value)
	{
		___onTouchExit_23 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchExit_23), value);
	}

	inline static int32_t get_offset_of_fingerOver_24() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40, ___fingerOver_24)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_fingerOver_24() const { return ___fingerOver_24; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_fingerOver_24() { return &___fingerOver_24; }
	inline void set_fingerOver_24(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___fingerOver_24 = value;
		Il2CppCodeGenWriteBarrier((&___fingerOver_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKENTEROVEREXIST_T8E9956CB50FB84413EB886F8EDA4C5A3399C5E40_H
#ifndef QUICKLONGTAP_T0905B696E7CEAAF70AB28A74C6AE089EE27C1B30_H
#define QUICKLONGTAP_T0905B696E7CEAAF70AB28A74C6AE089EE27C1B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap
struct  QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickLongTap_OnLongTap HedgehogTeam.EasyTouch.QuickLongTap::onLongTap
	OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E * ___onLongTap_21;
	// HedgehogTeam.EasyTouch.QuickLongTap_ActionTriggering HedgehogTeam.EasyTouch.QuickLongTap::actionTriggering
	int32_t ___actionTriggering_22;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickLongTap::currentGesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___currentGesture_23;

public:
	inline static int32_t get_offset_of_onLongTap_21() { return static_cast<int32_t>(offsetof(QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30, ___onLongTap_21)); }
	inline OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E * get_onLongTap_21() const { return ___onLongTap_21; }
	inline OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E ** get_address_of_onLongTap_21() { return &___onLongTap_21; }
	inline void set_onLongTap_21(OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E * value)
	{
		___onLongTap_21 = value;
		Il2CppCodeGenWriteBarrier((&___onLongTap_21), value);
	}

	inline static int32_t get_offset_of_actionTriggering_22() { return static_cast<int32_t>(offsetof(QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30, ___actionTriggering_22)); }
	inline int32_t get_actionTriggering_22() const { return ___actionTriggering_22; }
	inline int32_t* get_address_of_actionTriggering_22() { return &___actionTriggering_22; }
	inline void set_actionTriggering_22(int32_t value)
	{
		___actionTriggering_22 = value;
	}

	inline static int32_t get_offset_of_currentGesture_23() { return static_cast<int32_t>(offsetof(QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30, ___currentGesture_23)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get_currentGesture_23() const { return ___currentGesture_23; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of_currentGesture_23() { return &___currentGesture_23; }
	inline void set_currentGesture_23(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		___currentGesture_23 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKLONGTAP_T0905B696E7CEAAF70AB28A74C6AE089EE27C1B30_H
#ifndef QUICKPINCH_TE6F1368F669ACB269BCB7BF16271B5C24CF19945_H
#define QUICKPINCH_TE6F1368F669ACB269BCB7BF16271B5C24CF19945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch
struct  QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickPinch_OnPinchAction HedgehogTeam.EasyTouch.QuickPinch::onPinchAction
	OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E * ___onPinchAction_21;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::isGestureOnMe
	bool ___isGestureOnMe_22;
	// HedgehogTeam.EasyTouch.QuickPinch_ActionTiggering HedgehogTeam.EasyTouch.QuickPinch::actionTriggering
	int32_t ___actionTriggering_23;
	// HedgehogTeam.EasyTouch.QuickPinch_ActionPinchDirection HedgehogTeam.EasyTouch.QuickPinch::pinchDirection
	int32_t ___pinchDirection_24;
	// System.Single HedgehogTeam.EasyTouch.QuickPinch::axisActionValue
	float ___axisActionValue_25;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::enableSimpleAction
	bool ___enableSimpleAction_26;

public:
	inline static int32_t get_offset_of_onPinchAction_21() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___onPinchAction_21)); }
	inline OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E * get_onPinchAction_21() const { return ___onPinchAction_21; }
	inline OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E ** get_address_of_onPinchAction_21() { return &___onPinchAction_21; }
	inline void set_onPinchAction_21(OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E * value)
	{
		___onPinchAction_21 = value;
		Il2CppCodeGenWriteBarrier((&___onPinchAction_21), value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_22() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___isGestureOnMe_22)); }
	inline bool get_isGestureOnMe_22() const { return ___isGestureOnMe_22; }
	inline bool* get_address_of_isGestureOnMe_22() { return &___isGestureOnMe_22; }
	inline void set_isGestureOnMe_22(bool value)
	{
		___isGestureOnMe_22 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_23() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___actionTriggering_23)); }
	inline int32_t get_actionTriggering_23() const { return ___actionTriggering_23; }
	inline int32_t* get_address_of_actionTriggering_23() { return &___actionTriggering_23; }
	inline void set_actionTriggering_23(int32_t value)
	{
		___actionTriggering_23 = value;
	}

	inline static int32_t get_offset_of_pinchDirection_24() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___pinchDirection_24)); }
	inline int32_t get_pinchDirection_24() const { return ___pinchDirection_24; }
	inline int32_t* get_address_of_pinchDirection_24() { return &___pinchDirection_24; }
	inline void set_pinchDirection_24(int32_t value)
	{
		___pinchDirection_24 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_25() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___axisActionValue_25)); }
	inline float get_axisActionValue_25() const { return ___axisActionValue_25; }
	inline float* get_address_of_axisActionValue_25() { return &___axisActionValue_25; }
	inline void set_axisActionValue_25(float value)
	{
		___axisActionValue_25 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_26() { return static_cast<int32_t>(offsetof(QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945, ___enableSimpleAction_26)); }
	inline bool get_enableSimpleAction_26() const { return ___enableSimpleAction_26; }
	inline bool* get_address_of_enableSimpleAction_26() { return &___enableSimpleAction_26; }
	inline void set_enableSimpleAction_26(bool value)
	{
		___enableSimpleAction_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKPINCH_TE6F1368F669ACB269BCB7BF16271B5C24CF19945_H
#ifndef QUICKSWIPE_TD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4_H
#define QUICKSWIPE_TD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe
struct  QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickSwipe_OnSwipeAction HedgehogTeam.EasyTouch.QuickSwipe::onSwipeAction
	OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2 * ___onSwipeAction_21;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::allowSwipeStartOverMe
	bool ___allowSwipeStartOverMe_22;
	// HedgehogTeam.EasyTouch.QuickSwipe_ActionTriggering HedgehogTeam.EasyTouch.QuickSwipe::actionTriggering
	int32_t ___actionTriggering_23;
	// HedgehogTeam.EasyTouch.QuickSwipe_SwipeDirection HedgehogTeam.EasyTouch.QuickSwipe::swipeDirection
	int32_t ___swipeDirection_24;
	// System.Single HedgehogTeam.EasyTouch.QuickSwipe::axisActionValue
	float ___axisActionValue_25;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::enableSimpleAction
	bool ___enableSimpleAction_26;

public:
	inline static int32_t get_offset_of_onSwipeAction_21() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___onSwipeAction_21)); }
	inline OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2 * get_onSwipeAction_21() const { return ___onSwipeAction_21; }
	inline OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2 ** get_address_of_onSwipeAction_21() { return &___onSwipeAction_21; }
	inline void set_onSwipeAction_21(OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2 * value)
	{
		___onSwipeAction_21 = value;
		Il2CppCodeGenWriteBarrier((&___onSwipeAction_21), value);
	}

	inline static int32_t get_offset_of_allowSwipeStartOverMe_22() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___allowSwipeStartOverMe_22)); }
	inline bool get_allowSwipeStartOverMe_22() const { return ___allowSwipeStartOverMe_22; }
	inline bool* get_address_of_allowSwipeStartOverMe_22() { return &___allowSwipeStartOverMe_22; }
	inline void set_allowSwipeStartOverMe_22(bool value)
	{
		___allowSwipeStartOverMe_22 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_23() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___actionTriggering_23)); }
	inline int32_t get_actionTriggering_23() const { return ___actionTriggering_23; }
	inline int32_t* get_address_of_actionTriggering_23() { return &___actionTriggering_23; }
	inline void set_actionTriggering_23(int32_t value)
	{
		___actionTriggering_23 = value;
	}

	inline static int32_t get_offset_of_swipeDirection_24() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___swipeDirection_24)); }
	inline int32_t get_swipeDirection_24() const { return ___swipeDirection_24; }
	inline int32_t* get_address_of_swipeDirection_24() { return &___swipeDirection_24; }
	inline void set_swipeDirection_24(int32_t value)
	{
		___swipeDirection_24 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_25() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___axisActionValue_25)); }
	inline float get_axisActionValue_25() const { return ___axisActionValue_25; }
	inline float* get_address_of_axisActionValue_25() { return &___axisActionValue_25; }
	inline void set_axisActionValue_25(float value)
	{
		___axisActionValue_25 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_26() { return static_cast<int32_t>(offsetof(QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4, ___enableSimpleAction_26)); }
	inline bool get_enableSimpleAction_26() const { return ___enableSimpleAction_26; }
	inline bool* get_address_of_enableSimpleAction_26() { return &___enableSimpleAction_26; }
	inline void set_enableSimpleAction_26(bool value)
	{
		___enableSimpleAction_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKSWIPE_TD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4_H
#ifndef QUICKTAP_TA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C_H
#define QUICKTAP_TA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap
struct  QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickTap_OnTap HedgehogTeam.EasyTouch.QuickTap::onTap
	OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8 * ___onTap_21;
	// HedgehogTeam.EasyTouch.QuickTap_ActionTriggering HedgehogTeam.EasyTouch.QuickTap::actionTriggering
	int32_t ___actionTriggering_22;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTap::currentGesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___currentGesture_23;

public:
	inline static int32_t get_offset_of_onTap_21() { return static_cast<int32_t>(offsetof(QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C, ___onTap_21)); }
	inline OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8 * get_onTap_21() const { return ___onTap_21; }
	inline OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8 ** get_address_of_onTap_21() { return &___onTap_21; }
	inline void set_onTap_21(OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8 * value)
	{
		___onTap_21 = value;
		Il2CppCodeGenWriteBarrier((&___onTap_21), value);
	}

	inline static int32_t get_offset_of_actionTriggering_22() { return static_cast<int32_t>(offsetof(QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C, ___actionTriggering_22)); }
	inline int32_t get_actionTriggering_22() const { return ___actionTriggering_22; }
	inline int32_t* get_address_of_actionTriggering_22() { return &___actionTriggering_22; }
	inline void set_actionTriggering_22(int32_t value)
	{
		___actionTriggering_22 = value;
	}

	inline static int32_t get_offset_of_currentGesture_23() { return static_cast<int32_t>(offsetof(QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C, ___currentGesture_23)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get_currentGesture_23() const { return ___currentGesture_23; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of_currentGesture_23() { return &___currentGesture_23; }
	inline void set_currentGesture_23(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		___currentGesture_23 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTAP_TA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[2] = 
{
	PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE::get_offset_of__files_4(),
	PersistentDataPathLoadSample_tC5BF685056EAC52A39AB583839E63B5A67E22FCE::get_offset_of__loadedGameObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (U3CU3Ec__DisplayClass2_0_t3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3901[1] = 
{
	U3CU3Ec__DisplayClass2_0_t3129DCCD5EB93D2AC5B811B3D69DDC1BEB6C35E5::get_offset_of_filter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[9] = 
{
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__centeredStyle_4(),
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__assetLoadingProgress_5(),
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__assetLoaded_6(),
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__error_7(),
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__progressBarEmptyTexture_8(),
	ProgressHandlingSample_tAEF539D60538FF937B7F403CDD4C354FBB02D2FF::get_offset_of__progressBarFilledTexture_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (SimpleRotator_t58EECB5DEB87D980FB1E81554C611718BF2D9051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF), -1, sizeof(URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3904[6] = 
{
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF::get_offset_of__okButton_5(),
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF::get_offset_of__cancelButton_6(),
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF::get_offset_of__uriText_7(),
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF::get_offset_of__extensionText_8(),
	URIDialog_t177278A7CF46FBFED6EAF0966B666D74DB91F9FF::get_offset_of__rendererGameObject_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[2] = 
{
	U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB::get_offset_of_onOk_0(),
	U3CU3Ec__DisplayClass16_0_tCDB4B8A3F8FA45F0BC224E65610A68DC2AAA9BEB::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (AutoScrollDampMode_t9F01A0E501AD211C7A5BF17A2603F45936F4A32C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3906[4] = 
{
	AutoScrollDampMode_t9F01A0E501AD211C7A5BF17A2603F45936F4A32C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (CameraPlaneAxes_tF96575D4C05B3634CB666A4AC15AAF3D5BA05722)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3907[3] = 
{
	CameraPlaneAxes_tF96575D4C05B3634CB666A4AC15AAF3D5BA05722::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[10] = 
{
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_textInfo_4(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_textDetail_5(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_touchInputController_6(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_mobileTouchCamera_7(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_mobilePickingController_8(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_cam_9(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_coroutineHideInfoText_10(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_selectedPickableTransform_11(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_originalItemColorCache_12(),
	DemoController_t35B229880F71530728AC85176AD73B8EA0A212E7::get_offset_of_introTextOnScreenTime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (U3CU3Ec__DisplayClass21_0_tF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[1] = 
{
	U3CU3Ec__DisplayClass21_0_tF177F0E92F5117BEECF3E6C6F9064E1D710D7FBF::get_offset_of_pickableTransform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3910[4] = 
{
	U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99::get_offset_of_pickableTransform_2(),
	U3CAnimateScaleForSelectionU3Ed__22_tCDEBB40B26BFD6D97D2A3A0A271D8F53DA743B99::get_offset_of_U3CtimeAtStartU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3911[4] = 
{
	U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119::get_offset_of_U3CU3E1__state_0(),
	U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119::get_offset_of_U3CU3E2__current_1(),
	U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119::get_offset_of_U3CU3E4__this_2(),
	U3CHideInfoTextU3Ed__36_t2BB943315A4790FC5478F0CEA4A13929D77DF119::get_offset_of_delay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3912[33] = 
{
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_snapToGrid_4(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_snapUnitSize_5(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_snapOffset_6(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_snapAngle_7(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_isMultiSelectionEnabled_8(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_requireLongTapForMove_9(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformSelected_10(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformSelectedExtended_11(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformDeselected_12(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformMoveStarted_13(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformMoved_14(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_OnPickableTransformMoveEnded_15(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_expertModeEnabled_16(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_deselectPreviousColliderOnClick_17(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_repeatEventSelectedOnClick_18(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_useLegacyTransformMovedEventOrder_19(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_touchInputController_20(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_mobileTouchCam_21(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_U3CSelectedCollidersU3Ek__BackingField_22(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_isSelectedViaLongTap_23(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_U3CCurrentlyDraggedPickableU3Ek__BackingField_24(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_draggedTransformOffset_25(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_draggedTransformHeightOffset_26(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_draggedItemCustomOffset_27(),
	0,
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_currentlyDraggedTransformPosition_29(),
	0,
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_currentDragStartPos_31(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_invokeMoveStartedOnDrag_32(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_invokeMoveEndedOnDrag_33(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_itemInitialDragOffsetWorld_34(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_isManualSelectionRequest_35(),
	MobilePickingController_t4E149E133DA10FBBB1FBC8012678A860E7C42C23::get_offset_of_selectionPositionOffsets_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (SelectionAction_t9FB56D0010219A67C1BE52DA01004D577C57F84E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3913[3] = 
{
	SelectionAction_t9FB56D0010219A67C1BE52DA01004D577C57F84E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07), -1, sizeof(U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3914[2] = 
{
	U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t896EB3C776F16747440A65415640F84FFA063C07_StaticFields::get_offset_of_U3CU3E9__67_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3915[84] = 
{
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_cameraAxes_6(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_perspectiveZoomMode_7(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camZoomMin_8(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camZoomMax_9(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camOverzoomMargin_10(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camOverdragMargin_11(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_boundaryMin_12(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_boundaryMax_13(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camFollowFactor_14(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_autoScrollDampMode_15(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_autoScrollDamp_16(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_autoScrollDampCurve_17(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_groundLevelOffset_18(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_enableRotation_19(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_enableTilt_20(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_tiltAngleMin_21(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_tiltAngleMax_22(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_enableZoomTilt_23(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_zoomTiltAngleMin_24(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_zoomTiltAngleMax_25(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_OnPickItem_26(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_OnPickItem2D_27(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_OnPickItemDoubleClick_28(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_OnPickItem2DDoubleClick_29(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_touchInputController_30(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_dragStartCamPos_31(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_cameraScrollVelocity_32(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchStartCamZoomSize_33(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchStartIntersectionCenter_34(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchCenterCurrent_35(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchDistanceCurrent_36(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchAngleCurrent_37(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchDistanceStart_38(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchCenterCurrentLerp_39(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchDistanceCurrentLerp_40(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchAngleCurrentLerp_41(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isRotationLock_42(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isRotationActivated_43(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchAngleLastFrame_44(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchTiltCurrent_45(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchTiltAccumulated_46(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isTiltModeEvaluated_47(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchTiltLastFrame_48(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isPinchTiltMode_49(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_timeRealDragStop_50(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CIsPinchingU3Ek__BackingField_51(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CIsDraggingU3Ek__BackingField_52(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_expertModeEnabled_53(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_zoomBackSpringFactor_54(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_dragBackSpringFactor_55(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_autoScrollVelocityMax_56(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_dampFactorTimeMultiplier_57(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isPinchModeExclusive_58(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_customZoomSensitivity_59(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_terrainCollider_60(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_cameraTransform_61(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_is2dOverdragMarginEnabled_62(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camOverdragMargin2d_63(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_rotationDetectionDeltaThreshold_64(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_rotationMinPinchDistance_65(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_rotationLockThreshold_66(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchModeDetectionMoveTreshold_67(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchTiltModeThreshold_68(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_pinchTiltSpeed_69(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isStarted_70(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CCamU3Ek__BackingField_71(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_isDraggingSceneObject_72(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_refPlaneXY_73(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_refPlaneXZ_74(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CDragCameraMoveVectorU3Ek__BackingField_75(),
	0,
	0,
	0,
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_targetPositionClamped_79(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CIsSmoothingEnabledU3Ek__BackingField_80(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CCamPosMinU3Ek__BackingField_81(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_U3CCamPosMaxU3Ek__BackingField_82(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_showHorizonError_83(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_enableOvertiltSpring_84(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_camOvertiltMargin_85(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_tiltBackSpringFactor_86(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_minOvertiltSpringPositionThreshold_87(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_useUntransformedCamBoundary_88(),
	MobileTouchCamera_t88CE3B980C2CBC208BCA390DC0A92AD48AE2D6EA::get_offset_of_maxHorizonFallbackDistance_89(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[3] = 
{
	U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C::get_offset_of_U3CU3E1__state_0(),
	U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C::get_offset_of_U3CU3E2__current_1(),
	U3CInitCamBoundariesDelayedU3Ed__193_t75681A79FF8ED5CB7408E5044E84A1D60C7E622C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[6] = 
{
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_U3CU3E1__state_0(),
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_U3CU3E2__current_1(),
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_target_2(),
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_U3CU3E4__this_3(),
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_U3CstartValueU3E5__2_4(),
	U3CZoomToTargetValueCoroutineU3Ed__230_tA833CF0CF20715DE3DB39A235AD3A368CD4A29D0::get_offset_of_U3CtimeStartU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0), -1, sizeof(MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3918[3] = 
{
	MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0_StaticFields::get_offset_of_mobileTouchCam_4(),
	MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0::get_offset_of_pickableTransform_5(),
	MobileTouchPickable_tF0F60EB42FEDADC55FB3B014E805A1B6206A61D0::get_offset_of_localSnapOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3919[2] = 
{
	MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02::get_offset_of_cachedTransform_4(),
	MonoBehaviourWrapped_t123979EA34849E9323414C01CB9C7B96AB5BBC02::get_offset_of_cachedGO_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (PerspectiveZoomMode_t43EB411EFFC8F799FA908FBD5E73847A5DF31170)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3920[3] = 
{
	PerspectiveZoomMode_t43EB411EFFC8F799FA908FBD5E73847A5DF31170::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[3] = 
{
	PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380::get_offset_of_U3CSelectedTransformU3Ek__BackingField_0(),
	PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380::get_offset_of_U3CIsDoubleClickU3Ek__BackingField_1(),
	PickableSelectedData_tA21FF7FB7F227F172BD42758B2B00E4670FD7380::get_offset_of_U3CIsLongTapU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[7] = 
{
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchCenter_0(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchDistance_1(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchStartDistance_2(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchAngleDelta_3(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchAngleDeltaNormalized_4(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchTiltDelta_5(),
	PinchUpdateData_t14D520537978BF81B94B44BBAFA898D35B3C8461::get_offset_of_pinchTotalFingerMovement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (SnapAngle_tAB59888ECAA0E909F67C357D9855D6B46646CFE7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3923[3] = 
{
	SnapAngle_tAB59888ECAA0E909F67C357D9855D6B46646CFE7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[11] = 
{
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_transitionDuration_6(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_transitionCurve_7(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_U3CMobileTouchCameraU3Ek__BackingField_8(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_posTransitionStart_9(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_posTransitionEnd_10(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_rotTransitionStart_11(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_rotTransitionEnd_12(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_zoomTransitionStart_13(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_zoomTransitionEnd_14(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_timeTransitionStart_15(),
	FocusCameraOnItem_tDE1C85F927B27015660C8A8F0F7295BD002BFC0A::get_offset_of_isTransitionStarted_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (SetBoundaryFromCollider_t6EF27C0922E2C08F3085C1216561346AA9FB56EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[1] = 
{
	SetBoundaryFromCollider_t6EF27C0922E2C08F3085C1216561346AA9FB56EB::get_offset_of_boxCollider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[41] = 
{
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_expertModeEnabled_4(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_clickDurationThreshold_5(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_doubleclickDurationThreshold_6(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_tiltMoveDotTreshold_7(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_tiltHorizontalDotThreshold_8(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_dragStartDistanceThresholdRelative_9(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_longTapStartsDrag_10(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_lastFingerDownTimeReal_11(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_lastClickTimeReal_12(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_wasFingerDownLastFrame_13(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_lastFinger0DownPos_14(),
	0,
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_isDragging_16(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_dragStartPos_17(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_dragStartOffset_18(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_U3CDragFinalMomentumVectorU3Ek__BackingField_19(),
	0,
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_pinchStartDistance_21(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_pinchStartPositions_22(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_touchPositionLastFrame_23(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_pinchRotationVectorStart_24(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_pinchVectorLastFrame_25(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_totalFingerMovement_26(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_wasDraggingLastFrame_27(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_wasPinchingLastFrame_28(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_isPinching_29(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_isInputOnLockedArea_30(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_timeSinceDragStart_31(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnDragStart_32(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnFingerDown_33(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnFingerUp_34(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnDragUpdate_35(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnDragStop_36(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnPinchStart_37(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnPinchUpdate_38(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnPinchUpdateExtended_39(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnPinchStop_40(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnLongTapProgress_41(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_isClickPrevented_42(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_OnInputClick_43(),
	TouchInputController_t4A8C8C170434264465AFFEEC42C00CB66609331F::get_offset_of_isFingerDown_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (InputDragStartDelegate_tBE096D32B580C003B19074979FC3EDC03CED4C16), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (Input1PositionDelegate_t22E991C6B2947150FE4D989AC84D147BCD8DE7E4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (DragUpdateDelegate_tD967552922861FA20056601230CDE6123279F537), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (DragStopDelegate_t16A1112266381CFD6AAA96BEE6B822F175B8746C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (PinchStartDelegate_tD01633B4A79C3D510ACF9881A5F9BDAAB3D5B9AC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (PinchUpdateDelegate_tF07698247DFFC91E8AD87A86383DD0CDB357AE89), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (PinchUpdateExtendedDelegate_t9D4584B65110D350046FB428CA104A641F32A705), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (InputLongTapProgress_t3A3325EB0C9A0EF2878BF5422F12413E7E2976EE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (InputClickDelegate_t5C42A7A098B870B3119F9A297A19D171EB1A0A97), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (TouchWrapper_t735E6415B5EDF72E835232B021FD08DA64D5C6AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (UnityEventWithPickableSelected_tE36560AEAF1EE51B726C764AB3D3D664F6873065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (UnityEventWithPosition_t4413F53299D18ABD6AD10EC21BAF530093DC17AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (UnityEventWithPositionAndTransform_t5A134C4683FF1B0C634507E103728B4EB88FA68A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (UnityEventWithRaycastHit_tDDC5434A72791FF90F808456B4D7E75CDBCF2592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (UnityEventWithRaycastHit2D_tA01F20B0B6AAD69A9A0CA43D711358CB5E07E9F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (UnityEventWithTransform_t474E1B585A08145BCC41B1D77A4993500FA3DFBC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3943[2] = 
{
	WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5::get_offset_of_U3CPositionU3Ek__BackingField_0(),
	WrappedTouch_tD1242AC9129A001A16F4EF3E94B8968FE5DAA4B5::get_offset_of_U3CFingerIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (ContentReadAction_tEBE208A49881B95D2B1F43A50183E53F3823A8B8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3944[3] = 
{
	ContentReadAction_tEBE208A49881B95D2B1F43A50183E53F3823A8B8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (HttpAction_t90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3945[6] = 
{
	HttpAction_t90025BDB0FC0244513368F4A4C4A5E0D8FDCDB33::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (HttpCompletionOption_t3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3946[3] = 
{
	HttpCompletionOption_t3CBA0C26AE0533634D5CEEF45FCCEBFE8210232A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B), -1, sizeof(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3947[19] = 
{
	0,
	0,
	0,
	0,
	0,
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CDownloadBlockSizeU3Ek__BackingField_5(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CUploadBlockSizeU3Ek__BackingField_6(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CTimeoutU3Ek__BackingField_7(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CReadWriteTimeoutU3Ek__BackingField_8(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CCacheU3Ek__BackingField_9(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CCertificatesU3Ek__BackingField_10(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CCookiesU3Ek__BackingField_11(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CCredentialsU3Ek__BackingField_12(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CKeepAliveU3Ek__BackingField_13(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CHeadersU3Ek__BackingField_14(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of_U3CProxyU3Ek__BackingField_15(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of__requests_16(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B::get_offset_of__lock_17(),
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B_StaticFields::get_offset_of__dispatcher_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[5] = 
{
	U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1::get_offset_of_uri_1(),
	U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1::get_offset_of_httpAction_2(),
	U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1::get_offset_of_httpCompletionOption_3(),
	U3CU3Ec__DisplayClass61_0_t772E221EFE096B0C2FE6867432EDD0BAF8463EA1::get_offset_of_responseCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3949[7] = 
{
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_uri_1(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_httpAction_2(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_httpContent_3(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_httpCompletionOption_4(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_responseCallback_5(),
	U3CU3Ec__DisplayClass62_0_t4E2E873F287B2240382B2877ACDFB62098D4B354::get_offset_of_uploadStatusCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3950[2] = 
{
	U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64::get_offset_of_action_0(),
	U3CU3Ec__DisplayClass76_0_t621FB1FD67CC27819717BD3488E02A2DE7D5BD64::get_offset_of_exception_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3951[2] = 
{
	ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6::get_offset_of__content_0(),
	ByteArrayContent_tAC3F769431D8BAB745350A09B4EA71FB8FBC28A6::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3952[2] = 
{
	FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A::get_offset_of__content_0(),
	FormUrlEncodedContent_tA24D8893148AC8DD0134C620D07B07C81ACA063A::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3954[8] = 
{
	0,
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of__content_1(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of__boundary_2(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of__contentLength_3(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of_U3CBoundaryStartBytesU3Ek__BackingField_4(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of_U3CBoundaryEndBytesU3Ek__BackingField_5(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of_U3CCRLFBytesU3Ek__BackingField_6(),
	MultipartContent_tF49F293164C17C3D21F57F1778AD94B5E5556DF2::get_offset_of_U3CHeadersU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (MultipartFormDataContent_tB1E525F0861AC6129F54CF976155B26BA5024A78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[2] = 
{
	StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8::get_offset_of__stream_0(),
	StreamContent_t487A9BB71FFA9BF5B967E712D896E8FD3BA873F8::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[3] = 
{
	0,
	StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC::get_offset_of__content_1(),
	StringContent_tD30D86D7DCFE3B2811C510BADDE99CE84F3E7FCC::get_offset_of_U3CHeadersU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[9] = 
{
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3COriginalRequestU3Ek__BackingField_0(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3COriginalResponseU3Ek__BackingField_1(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CContentLengthU3Ek__BackingField_2(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CTotalContentReadU3Ek__BackingField_3(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CContentReadThisRoundU3Ek__BackingField_4(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CStatusCodeU3Ek__BackingField_5(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CReasonPhraseU3Ek__BackingField_6(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of_U3CExceptionU3Ek__BackingField_7(),
	HttpResponseMessage_t641F1D1CE60AB20D41014DE899199E10673FA048::get_offset_of__responseData_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[3] = 
{
	UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB::get_offset_of_U3CContentLengthU3Ek__BackingField_0(),
	UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB::get_offset_of_U3CTotalContentUploadedU3Ek__BackingField_1(),
	UploadStatusMessage_t6E6EF0CD2179DCE0F63A185AC605CE22D3757EDB::get_offset_of_U3CContentUploadedThisRoundU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (AuthHelper_t79E62E96D3A645A3E6F32CF77FC1F4629DB01027), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61), -1, sizeof(Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3961[2] = 
{
	Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields::get_offset_of__queue_4(),
	Dispatcher_t8226CCC3ED53958A2A0BA3DDA90F30BEB2F51E61_StaticFields::get_offset_of__lock_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3963[4] = 
{
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F::get_offset_of__request_0(),
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F::get_offset_of__response_1(),
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F::get_offset_of__dispatcher_2(),
	HttpBase_t134D4F33D21270D2318F2A25C285CBBF7980B92F::get_offset_of__isAborted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3964[4] = 
{
	U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B::get_offset_of_uploadStatusCallback_0(),
	U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B::get_offset_of_contentLength_1(),
	U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B::get_offset_of_contentUploadedThisRound_2(),
	U3CU3Ec__DisplayClass10_0_t68597394AB3AF08DD76D930222D63E455AEB944B::get_offset_of_totalContentUploaded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[5] = 
{
	U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B::get_offset_of_responseCallback_0(),
	U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B::get_offset_of_data_1(),
	U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B::get_offset_of_contentReadThisRound_3(),
	U3CU3Ec__DisplayClass11_0_tCB40E76EB0FDBEBC3565A2D1251F0E6112DA883B::get_offset_of_totalContentRead_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3966[3] = 
{
	U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC::get_offset_of_action_0(),
	U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass12_0_t5D7374647380736D7769A35259F24248C46B27BC::get_offset_of_exception_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (HttpRequest_tCED431E733C32C305084D914993CF094C22A4C63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (EasyTouchTrigger_tE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[1] = 
{
	EasyTouchTrigger_tE0AA8B8DF51168EB4D7739DD4F7B5C018ED1FF61::get_offset_of_receivers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (ETTParameter_tA7CEE31494A885F86856295E4F565A16444048D5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3969[17] = 
{
	ETTParameter_tA7CEE31494A885F86856295E4F565A16444048D5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (ETTType_t1AAC6C2F14C4C027796AB307B6567174BA3C810C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3970[3] = 
{
	ETTType_t1AAC6C2F14C4C027796AB307B6567174BA3C810C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3971[10] = 
{
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_enable_0(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_triggerType_1(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_name_2(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_restricted_3(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_gameObject_4(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_otherReceiver_5(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_gameObjectReceiver_6(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_eventName_7(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_methodName_8(),
	EasyTouchReceiver_t112D590291D8B19809918CE0E2A0F23DA76CF9B2::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (U3CU3Ec__DisplayClass51_0_tBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3972[1] = 
{
	U3CU3Ec__DisplayClass51_0_tBC261BE0F545F6DCBA84E6F828B4BFFA2D6F2315::get_offset_of_evnt_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (U3CU3Ec__DisplayClass52_0_tF89E810CD63A5639A152C00F97A83128C2ED8C68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[1] = 
{
	U3CU3Ec__DisplayClass52_0_tF89E810CD63A5639A152C00F97A83128C2ED8C68::get_offset_of_triggerName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3974[17] = 
{
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_quickActionName_4(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_isMultiTouch_5(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_is2Finger_6(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_isOnTouch_7(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_enablePickOverUI_8(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_resetPhysic_9(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_directAction_10(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_axesAction_11(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_sensibility_12(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_directCharacterController_13(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_inverseAxisValue_14(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_cachedRigidBody_15(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_isKinematic_16(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_cachedRigidBody2D_17(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_isKinematic2D_18(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_realType_19(),
	QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850::get_offset_of_fingerIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3975[5] = 
{
	GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3976[7] = 
{
	DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3977[8] = 
{
	AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3978[7] = 
{
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_onDragStart_21(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_onDrag_22(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_onDragEnd_23(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_isStopOncollisionEnter_24(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_deltaPosition_25(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_isOnDrag_26(),
	QuickDrag_t84C22B93C7A128A18DF62BC5DE05643F67AD549E::get_offset_of_lastGesture_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (OnDragStart_t0F16E4F16CF819945A7EFB4AC7FD99554C39D780), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (OnDrag_t5744CBA4D301B97D65C67AC86B6C79417163A7AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (OnDragEnd_t667CF847B4B5D8CC2E2D1AF63A3EE8E3191C55AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[4] = 
{
	QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40::get_offset_of_onTouchEnter_21(),
	QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40::get_offset_of_onTouchOver_22(),
	QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40::get_offset_of_onTouchExit_23(),
	QuickEnterOverExist_t8E9956CB50FB84413EB886F8EDA4C5A3399C5E40::get_offset_of_fingerOver_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (OnTouchEnter_t11475E4F03EFF70EBD9A46545ABD66C549E72A13), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (OnTouchOver_t5BB647ED24F68374C13CC58F8C31E890994E4015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (OnTouchExit_tD9FC8D0EA2607B6F6D48585337B09D1F29A367BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[3] = 
{
	QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30::get_offset_of_onLongTap_21(),
	QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30::get_offset_of_actionTriggering_22(),
	QuickLongTap_t0905B696E7CEAAF70AB28A74C6AE089EE27C1B30::get_offset_of_currentGesture_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (OnLongTap_t6C61FBCF4E25A10BFE67DBBA80C34F0EDDD8DB8E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (ActionTriggering_tD6CFD0829085966F2AB0EA03654FD5AB98695A03)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3988[4] = 
{
	ActionTriggering_tD6CFD0829085966F2AB0EA03654FD5AB98695A03::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[6] = 
{
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_onPinchAction_21(),
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_isGestureOnMe_22(),
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_actionTriggering_23(),
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_pinchDirection_24(),
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_axisActionValue_25(),
	QuickPinch_tE6F1368F669ACB269BCB7BF16271B5C24CF19945::get_offset_of_enableSimpleAction_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (OnPinchAction_t58779DCC888555EC219734341FDC7BF0FA65CE9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (ActionTiggering_t4E8FC47EF05E529F01227F41B3D518343F2AE83B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3991[3] = 
{
	ActionTiggering_t4E8FC47EF05E529F01227F41B3D518343F2AE83B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (ActionPinchDirection_t252F13A292F55C11A79DE3DD185EA11F005CE205)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3992[4] = 
{
	ActionPinchDirection_t252F13A292F55C11A79DE3DD185EA11F005CE205::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3993[6] = 
{
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_onSwipeAction_21(),
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_allowSwipeStartOverMe_22(),
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_actionTriggering_23(),
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_swipeDirection_24(),
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_axisActionValue_25(),
	QuickSwipe_tD3E1BE2FA95922F8BF1FA5FF5967AB8474CA20A4::get_offset_of_enableSimpleAction_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (OnSwipeAction_t11487A29B7C37B95564F396860E38777EB15DCA2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (ActionTriggering_tF4F78C81B3285CD6653C4506E7114BB1CAFFBD55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3995[3] = 
{
	ActionTriggering_tF4F78C81B3285CD6653C4506E7114BB1CAFFBD55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (SwipeDirection_tF235422ED045B6A596630C0B48DEB2DB3FBE4831)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3996[14] = 
{
	SwipeDirection_tF235422ED045B6A596630C0B48DEB2DB3FBE4831::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[3] = 
{
	QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C::get_offset_of_onTap_21(),
	QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C::get_offset_of_actionTriggering_22(),
	QuickTap_tA4DA5B1DE2DF9892C39AEAC7DB740B711B143C6C::get_offset_of_currentGesture_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (OnTap_t219D6EAD2A98030C7B88B2D117B8FE9AECCE2FC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (ActionTriggering_t15EB815254A99EBF572283FC6C7C58EDEEC8FF7D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3999[3] = 
{
	ActionTriggering_t15EB815254A99EBF572283FC6C7C58EDEEC8FF7D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
