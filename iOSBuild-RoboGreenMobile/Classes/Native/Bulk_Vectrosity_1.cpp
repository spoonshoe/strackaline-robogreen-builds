﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,UnityEngine.Mesh>[]
struct EntryU5BU5D_t8F54265119F36E2FB7C40EFC6499244AF80AA099;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.Mesh>
struct KeyCollection_t948BD8ADE1CA3A82FA067772974AD94A26578AE4;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.Mesh>
struct ValueCollection_tC0E942B13F248100B06FFFE76E1709DE4716DE01;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>
struct Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0;
// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo>
struct Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t85CDFDE61E9ED5EC59179D4466DF210DD727EEEC;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.List`1<Vectrosity.RefInt>
struct List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE;
// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// Vectrosity.BrightnessControl
struct BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97;
// Vectrosity.IVectorObject
struct IVectorObject_t6A320644FF8ACBE266FD9BA1E8D31DED52FAE201;
// Vectrosity.LineManager
struct LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776;
// Vectrosity.RefInt
struct RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2;
// Vectrosity.RefInt[]
struct RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87;
// Vectrosity.VectorLine
struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F;
// Vectrosity.VectorLine[]
struct VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC;
// Vectrosity.VectorObject2D
struct VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650;
// Vectrosity.VectorObject3D
struct VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE;
// Vectrosity.VisibilityControl
struct VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA;
// Vectrosity.VisibilityControl/<OnBecameInvisible>c__Iterator3
struct U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73;
// Vectrosity.VisibilityControl/<OnBecameVisible>c__Iterator2
struct U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403;
// Vectrosity.VisibilityControl/<VisibilityTest>c__Iterator1
struct U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48;
// Vectrosity.VisibilityControlAlways
struct VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E;
// Vectrosity.VisibilityControlStatic
struct VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B;
// Vectrosity.VisibilityControlStatic/<WaitCheck>c__Iterator4
struct U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7;

extern RuntimeClass* BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_il2cpp_TypeInfo_var;
extern RuntimeClass* MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_il2cpp_TypeInfo_var;
extern RuntimeClass* Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_il2cpp_TypeInfo_var;
extern RuntimeClass* U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
extern RuntimeClass* VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var;
extern RuntimeClass* VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var;
extern RuntimeClass* VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_il2cpp_TypeInfo_var;
extern RuntimeClass* VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_il2cpp_TypeInfo_var;
extern RuntimeClass* VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_il2cpp_TypeInfo_var;
extern RuntimeClass* VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral50FEA8B87017C4C0C3E95EDBFF1E6EA64C8180CE;
extern String_t* _stringLiteral5E99782E75827888115C4B84F118BE713DD3CF07;
extern String_t* _stringLiteral63D2F2B40CD52CB916885FD6798EB2CA97387C35;
extern String_t* _stringLiteralB312E0EE41A9D97F429E491684D5D351E5F14FDF;
extern String_t* _stringLiteralEE80E905BA9EAAC3B0371212824CA6B7C66EB9DD;
extern const RuntimeMethod* Component_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mF3F89565A9CEFF85AA1FB27C6EC64BE590DC386B_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2010B8DB8B89AF9E3AB5184F256BB89CC201EF1C_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m299D4D802C5BC1E197E7B4B41CBBEE85EE9EE0C0_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m8EA95790B7EF7A3D250BF84526344E6A5FCCA549_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mD1BA4FFEB800AB3D102141CD0A0ECE237EA70FB4_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mFB43D5458906C4005145640D4396FDE5853AFA3A_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mFE084338629BB4ECDD74DD412713A0FF01A1A08E_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_mF372089EAC48892D8A5DB1C9721B8D816EB92F2E_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mAB4C08A6D18053E4C3BE30A489615645071526A8_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m6FBF36E821E762256C887A96718EC1B49B67EA8D_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547_RuntimeMethod_var;
extern const RuntimeMethod* U3COnBecameInvisibleU3Ec__Iterator3_Reset_m8B1D3B560DBA62FFE3D062FD35CE0A6B28A2922C_RuntimeMethod_var;
extern const RuntimeMethod* U3COnBecameVisibleU3Ec__Iterator2_Reset_mADF361215367F92DB67EC9536CE303728FE50109_RuntimeMethod_var;
extern const RuntimeMethod* U3CVisibilityTestU3Ec__Iterator1_Reset_mCA3C372AA2A7B0614FB0B92347A91F927906D1BA_RuntimeMethod_var;
extern const RuntimeMethod* U3CWaitCheckU3Ec__Iterator4_Reset_m50206552E5B267F8BCDDE793EEC585799F2B4CC4_RuntimeMethod_var;
extern const RuntimeType* BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_0_0_0_var;
extern const RuntimeType* MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_0_0_0_var;
extern const RuntimeType* MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_0_0_0_var;
extern const RuntimeType* VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_0_0_0_var;
extern const RuntimeType* VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_0_0_0_var;
extern const RuntimeType* VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_0_0_0_var;
extern const uint32_t U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_m15387FC79C3F9A8D5AB103D28917158A1F0DF429_MetadataUsageId;
extern const uint32_t U3COnBecameInvisibleU3Ec__Iterator3_Reset_m8B1D3B560DBA62FFE3D062FD35CE0A6B28A2922C_MetadataUsageId;
extern const uint32_t U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m74638B33EA17EBA2F3D1076555DD7DE2C3902F49_MetadataUsageId;
extern const uint32_t U3COnBecameVisibleU3Ec__Iterator2_Reset_mADF361215367F92DB67EC9536CE303728FE50109_MetadataUsageId;
extern const uint32_t U3CVisibilityTestU3Ec__Iterator1_MoveNext_m4BD218B997001D0C4892B312666283E5D35633FA_MetadataUsageId;
extern const uint32_t U3CVisibilityTestU3Ec__Iterator1_Reset_mCA3C372AA2A7B0614FB0B92347A91F927906D1BA_MetadataUsageId;
extern const uint32_t U3CWaitCheckU3Ec__Iterator4_MoveNext_m826855185D28E61E3923A2C8AECD434C586AB207_MetadataUsageId;
extern const uint32_t U3CWaitCheckU3Ec__Iterator4_Reset_m50206552E5B267F8BCDDE793EEC585799F2B4CC4_MetadataUsageId;
extern const uint32_t VectorManager_CheckDistanceSetup_m5BBDE4F9EEB39C177D0419D37A6BCF74BAA74BFC_MetadataUsageId;
extern const uint32_t VectorManager_CheckDistance_m174A9BB070CD50AC6AE7AF33B7D5578B0092362A_MetadataUsageId;
extern const uint32_t VectorManager_DistanceRemove_mF080E1C53F1DAD28C7FE19F63CF6DE695CE8CD35_MetadataUsageId;
extern const uint32_t VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19_MetadataUsageId;
extern const uint32_t VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081_MetadataUsageId;
extern const uint32_t VectorManager_DrawArrayLines2_m79729DDA6145FAF9A5EB2A9BCA9A5AF48A5FE5FE_MetadataUsageId;
extern const uint32_t VectorManager_DrawArrayLines_mE37BD49B9937B4DB6C8EFB1D62E77A9789D0C58F_MetadataUsageId;
extern const uint32_t VectorManager_GetBounds_m81458FB6B7B1F622EF00C2DF03E83416BA7AA4D7_MetadataUsageId;
extern const uint32_t VectorManager_GetBounds_mE824FB050B62C410AFCEB570F5351824839335C4_MetadataUsageId;
extern const uint32_t VectorManager_GetBrightnessValue_m9AD38BBC736EBBB0BD805028EAC9FBEF852FB5C3_MetadataUsageId;
extern const uint32_t VectorManager_MakeBoundsMesh_m050BEFC2B6263AF23741435C1F7C579298366E1C_MetadataUsageId;
extern const uint32_t VectorManager_ObjectSetup_m95FA247CA782490727E4AFF27971CA7D6BEC9506_MetadataUsageId;
extern const uint32_t VectorManager_ObjectSetup_mBE9DB9235D4177C91832964336FC68925FEDF55C_MetadataUsageId;
extern const uint32_t VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2_MetadataUsageId;
extern const uint32_t VectorManager_SetDistanceColor_m8EC2BBE45FD2A12808FC0978168D4C85DD0D1439_MetadataUsageId;
extern const uint32_t VectorManager_SetOldDistance_m7C0E3CD2180C2A4A1192AABC9DF4D0A54D332D59_MetadataUsageId;
extern const uint32_t VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF_MetadataUsageId;
extern const uint32_t VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B_MetadataUsageId;
extern const uint32_t VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED_MetadataUsageId;
extern const uint32_t VectorManager_VisibilityStaticRemove_m62842272CFC8F10526322B4F60F7966532455867_MetadataUsageId;
extern const uint32_t VectorManager_VisibilityStaticSetup_m1FA651D0D2A47D01C99369F33B95C88DEC9BB4B1_MetadataUsageId;
extern const uint32_t VectorManager__cctor_m3AE1C5FEAC6053455ECDD21BC5F826FE393CA16C_MetadataUsageId;
extern const uint32_t VectorManager_get_arrayCount2_mEBCE3A36B705F2968A0AEEC0088D3483D7A5010C_MetadataUsageId;
extern const uint32_t VectorManager_get_arrayCount_mDC24274981CD046960F76077C6D52B73059B5E86_MetadataUsageId;
extern const uint32_t VectorObject2D_ClearMesh_mBAE450E7E0F5E7F8637A7FBC87B8532DA97CC720_MetadataUsageId;
extern const uint32_t VectorObject2D_DestroyNow_m04978A2F39767BB28D87D023F54DB59D94EA78C9_MetadataUsageId;
extern const uint32_t VectorObject2D_Destroy_mE4F502240BE03574D081D7C9C6F63EAE90E2C871_MetadataUsageId;
extern const uint32_t VectorObject2D_Enable_mBA5E8E1A00A6D5402E119E40DAA313DB0611EEA9_MetadataUsageId;
extern const uint32_t VectorObject2D_OnPopulateMesh_mEE597AD595E63AC648FE57B77EBF2E270F03FE06_MetadataUsageId;
extern const uint32_t VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0_MetadataUsageId;
extern const uint32_t VectorObject2D_SetName_m457760C14940A4B030043B8D05EA4AF9D5FC1050_MetadataUsageId;
extern const uint32_t VectorObject2D_SetupMesh_m65B0C77C52B6ABCDF96C84107E658188DEDDD310_MetadataUsageId;
extern const uint32_t VectorObject2D_UpdateGeometry_mD3E3AE525460790DA8EBDA46A1EDC21F9ADA7061_MetadataUsageId;
extern const uint32_t VectorObject2D_UpdateMeshAttributes_m48C0D2AF88A3E054FE2EDF81A285A67CBC8092A9_MetadataUsageId;
extern const uint32_t VectorObject2D__cctor_m8B463B1188B0C71FC838DB81D829AFE4BA354C4A_MetadataUsageId;
extern const uint32_t VectorObject3D_ClearMesh_m76390F9DE3A878CC447E02E207BB04A7655D4F82_MetadataUsageId;
extern const uint32_t VectorObject3D_Destroy_m8A31175145711DDCDBA6E5C853D5BE441077FAF6_MetadataUsageId;
extern const uint32_t VectorObject3D_Enable_m6931189D0D171B44CFBFFE9DF4871019FEF650BF_MetadataUsageId;
extern const uint32_t VectorObject3D_SetMaterial_m6DDAC1E086DE38C332BA1BD402695A1D6B66E62A_MetadataUsageId;
extern const uint32_t VectorObject3D_SetName_mD2F94D73915D5DA6D18C5F86F2C3B4A7BBD80178_MetadataUsageId;
extern const uint32_t VectorObject3D_SetTexture_mE863674CF2DEC0B387DFF906BE3B477AAB73B5AA_MetadataUsageId;
extern const uint32_t VectorObject3D_SetVectorLine_mE235B13331769EF2B6BB656E404951FD4593764D_MetadataUsageId;
extern const uint32_t VectorObject3D_SetupMesh_m527507F5C9C7A8B91385F6F8D8F883BC41C1295C_MetadataUsageId;
extern const uint32_t VisibilityControlAlways_OnDestroy_m79041C8FF29732B0C7A2641FA02565FE092B572E_MetadataUsageId;
extern const uint32_t VisibilityControlAlways_Setup_m5619CA19EBF41DCFB139E1DE25633C14E432E7DF_MetadataUsageId;
extern const uint32_t VisibilityControlStatic_OnBecameVisible_m60D7992FE0785281EB6A710922B23680BC533943_MetadataUsageId;
extern const uint32_t VisibilityControlStatic_OnDestroy_mCD23F86B2DC562834816E4E48EAF01827FE39293_MetadataUsageId;
extern const uint32_t VisibilityControlStatic_Setup_m8DF1DC3A026ADB970451E19CCD65782BA960FDC0_MetadataUsageId;
extern const uint32_t VisibilityControlStatic_WaitCheck_m789C29BB85C9EB189F9AB3E8C97E2470CF3F24DC_MetadataUsageId;
extern const uint32_t VisibilityControl_OnBecameInvisible_mA07DF85339DABD246ACF5A68AE089BFD99ADE1E7_MetadataUsageId;
extern const uint32_t VisibilityControl_OnBecameVisible_m69CBE26331CECCD0F2C503ECBE4FD8FE38D591AF_MetadataUsageId;
extern const uint32_t VisibilityControl_OnDestroy_m194DC050E90D34281385066A59F37A55923BD3F9_MetadataUsageId;
extern const uint32_t VisibilityControl_Setup_mD333A77ED1FAC412903F1B213F1F532997042C76_MetadataUsageId;
extern const uint32_t VisibilityControl_VisibilityTest_m754BD51A9F8FEDCCAED123D8EEA0E2400267EB2F_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_TE1F10B46B17B98B8216962ABCCB2327B0D5278B0_H
#define DICTIONARY_2_TE1F10B46B17B98B8216962ABCCB2327B0D5278B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>
struct  Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t8F54265119F36E2FB7C40EFC6499244AF80AA099* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t948BD8ADE1CA3A82FA067772974AD94A26578AE4 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tC0E942B13F248100B06FFFE76E1709DE4716DE01 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___entries_1)); }
	inline EntryU5BU5D_t8F54265119F36E2FB7C40EFC6499244AF80AA099* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t8F54265119F36E2FB7C40EFC6499244AF80AA099** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t8F54265119F36E2FB7C40EFC6499244AF80AA099* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___keys_7)); }
	inline KeyCollection_t948BD8ADE1CA3A82FA067772974AD94A26578AE4 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t948BD8ADE1CA3A82FA067772974AD94A26578AE4 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t948BD8ADE1CA3A82FA067772974AD94A26578AE4 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ___values_8)); }
	inline ValueCollection_tC0E942B13F248100B06FFFE76E1709DE4716DE01 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tC0E942B13F248100B06FFFE76E1709DE4716DE01 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tC0E942B13F248100B06FFFE76E1709DE4716DE01 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_TE1F10B46B17B98B8216962ABCCB2327B0D5278B0_H
#ifndef LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#define LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifndef LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#define LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____items_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_StaticFields, ____emptyArray_5)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#ifndef LIST_1_T1863EF4EE1FDEED14D460C85AF61BE0850892F6D_H
#define LIST_1_T1863EF4EE1FDEED14D460C85AF61BE0850892F6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D, ____items_1)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1863EF4EE1FDEED14D460C85AF61BE0850892F6D_H
#ifndef LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#define LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifndef LIST_1_T5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_H
#define LIST_1_T5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Vectrosity.RefInt>
struct  List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE, ____items_1)); }
	inline RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* get__items_1() const { return ____items_1; }
	inline RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_StaticFields, ____emptyArray_5)); }
	inline RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RefIntU5BU5D_t6C02EBC0E3B63BA5524DFD64EC8B0F1A1B12CC87* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_H
#ifndef LIST_1_T8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_H
#define LIST_1_T8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct  List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980, ____items_1)); }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* get__items_1() const { return ____items_1; }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_StaticFields, ____emptyArray_5)); }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* get__emptyArray_5() const { return ____emptyArray_5; }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#define YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifndef REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#define REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.RefInt
struct  RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.RefInt::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#ifndef U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#define U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3
struct  U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#ifndef U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#define U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2
struct  U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#ifndef U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#define U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1
struct  U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#ifndef U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#define U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4
struct  U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControlStatic Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::<>f__this
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U3CU3Ef__this_2)); }
	inline VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef WAITFORENDOFFRAME_T75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_H
#define WAITFORENDOFFRAME_T75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORENDOFFRAME_T75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_H
#ifndef NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#define NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#define COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifndef HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#define HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#define BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Brightness
struct  Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4 
{
public:
	// System.Int32 Vectrosity.Brightness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifndef CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#define CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.CanvasState
struct  CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6 
{
public:
	// System.Int32 Vectrosity.CanvasState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifndef ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#define ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.EndCap
struct  EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F 
{
public:
	// System.Int32 Vectrosity.EndCap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifndef JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#define JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Joins
struct  Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F 
{
public:
	// System.Int32 Vectrosity.Joins::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifndef LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#define LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.LineType
struct  LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1 
{
public:
	// System.Int32 Vectrosity.LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifndef FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#define FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine_FunctionName
struct  FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9 
{
public:
	// System.Int32 Vectrosity.VectorLine_FunctionName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#ifndef VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#define VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorManager
struct  VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993  : public RuntimeObject
{
public:

public:
};

struct VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields
{
public:
	// System.Single Vectrosity.VectorManager::minBrightnessDistance
	float ___minBrightnessDistance_0;
	// System.Single Vectrosity.VectorManager::maxBrightnessDistance
	float ___maxBrightnessDistance_1;
	// System.Int32 Vectrosity.VectorManager::brightnessLevels
	int32_t ___brightnessLevels_2;
	// System.Single Vectrosity.VectorManager::distanceCheckFrequency
	float ___distanceCheckFrequency_3;
	// UnityEngine.Color Vectrosity.VectorManager::fogColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___fogColor_4;
	// System.Boolean Vectrosity.VectorManager::useDraw3D
	bool ___useDraw3D_5;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines_6;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers_7;
	// System.Int32 Vectrosity.VectorManager::_arrayCount
	int32_t ____arrayCount_8;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines2
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines2_9;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers2
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers2_10;
	// System.Int32 Vectrosity.VectorManager::_arrayCount2
	int32_t ____arrayCount2_11;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Vectrosity.VectorManager::transforms3
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___transforms3_12;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines3
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines3_13;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorManager::oldDistances
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___oldDistances_14;
	// System.Collections.Generic.List`1<UnityEngine.Color> Vectrosity.VectorManager::colors
	List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * ___colors_15;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers3
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers3_16;
	// System.Int32 Vectrosity.VectorManager::_arrayCount3
	int32_t ____arrayCount3_17;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh> Vectrosity.VectorManager::meshTable
	Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * ___meshTable_18;

public:
	inline static int32_t get_offset_of_minBrightnessDistance_0() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___minBrightnessDistance_0)); }
	inline float get_minBrightnessDistance_0() const { return ___minBrightnessDistance_0; }
	inline float* get_address_of_minBrightnessDistance_0() { return &___minBrightnessDistance_0; }
	inline void set_minBrightnessDistance_0(float value)
	{
		___minBrightnessDistance_0 = value;
	}

	inline static int32_t get_offset_of_maxBrightnessDistance_1() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___maxBrightnessDistance_1)); }
	inline float get_maxBrightnessDistance_1() const { return ___maxBrightnessDistance_1; }
	inline float* get_address_of_maxBrightnessDistance_1() { return &___maxBrightnessDistance_1; }
	inline void set_maxBrightnessDistance_1(float value)
	{
		___maxBrightnessDistance_1 = value;
	}

	inline static int32_t get_offset_of_brightnessLevels_2() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___brightnessLevels_2)); }
	inline int32_t get_brightnessLevels_2() const { return ___brightnessLevels_2; }
	inline int32_t* get_address_of_brightnessLevels_2() { return &___brightnessLevels_2; }
	inline void set_brightnessLevels_2(int32_t value)
	{
		___brightnessLevels_2 = value;
	}

	inline static int32_t get_offset_of_distanceCheckFrequency_3() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___distanceCheckFrequency_3)); }
	inline float get_distanceCheckFrequency_3() const { return ___distanceCheckFrequency_3; }
	inline float* get_address_of_distanceCheckFrequency_3() { return &___distanceCheckFrequency_3; }
	inline void set_distanceCheckFrequency_3(float value)
	{
		___distanceCheckFrequency_3 = value;
	}

	inline static int32_t get_offset_of_fogColor_4() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___fogColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_fogColor_4() const { return ___fogColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_fogColor_4() { return &___fogColor_4; }
	inline void set_fogColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___fogColor_4 = value;
	}

	inline static int32_t get_offset_of_useDraw3D_5() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___useDraw3D_5)); }
	inline bool get_useDraw3D_5() const { return ___useDraw3D_5; }
	inline bool* get_address_of_useDraw3D_5() { return &___useDraw3D_5; }
	inline void set_useDraw3D_5(bool value)
	{
		___useDraw3D_5 = value;
	}

	inline static int32_t get_offset_of_vectorLines_6() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines_6)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines_6() const { return ___vectorLines_6; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines_6() { return &___vectorLines_6; }
	inline void set_vectorLines_6(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines_6 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines_6), value);
	}

	inline static int32_t get_offset_of_objectNumbers_7() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers_7)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers_7() const { return ___objectNumbers_7; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers_7() { return &___objectNumbers_7; }
	inline void set_objectNumbers_7(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers_7 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers_7), value);
	}

	inline static int32_t get_offset_of__arrayCount_8() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount_8)); }
	inline int32_t get__arrayCount_8() const { return ____arrayCount_8; }
	inline int32_t* get_address_of__arrayCount_8() { return &____arrayCount_8; }
	inline void set__arrayCount_8(int32_t value)
	{
		____arrayCount_8 = value;
	}

	inline static int32_t get_offset_of_vectorLines2_9() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines2_9)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines2_9() const { return ___vectorLines2_9; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines2_9() { return &___vectorLines2_9; }
	inline void set_vectorLines2_9(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines2_9 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines2_9), value);
	}

	inline static int32_t get_offset_of_objectNumbers2_10() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers2_10)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers2_10() const { return ___objectNumbers2_10; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers2_10() { return &___objectNumbers2_10; }
	inline void set_objectNumbers2_10(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers2_10 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers2_10), value);
	}

	inline static int32_t get_offset_of__arrayCount2_11() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount2_11)); }
	inline int32_t get__arrayCount2_11() const { return ____arrayCount2_11; }
	inline int32_t* get_address_of__arrayCount2_11() { return &____arrayCount2_11; }
	inline void set__arrayCount2_11(int32_t value)
	{
		____arrayCount2_11 = value;
	}

	inline static int32_t get_offset_of_transforms3_12() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___transforms3_12)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_transforms3_12() const { return ___transforms3_12; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_transforms3_12() { return &___transforms3_12; }
	inline void set_transforms3_12(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___transforms3_12 = value;
		Il2CppCodeGenWriteBarrier((&___transforms3_12), value);
	}

	inline static int32_t get_offset_of_vectorLines3_13() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines3_13)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines3_13() const { return ___vectorLines3_13; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines3_13() { return &___vectorLines3_13; }
	inline void set_vectorLines3_13(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines3_13 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines3_13), value);
	}

	inline static int32_t get_offset_of_oldDistances_14() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___oldDistances_14)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_oldDistances_14() const { return ___oldDistances_14; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_oldDistances_14() { return &___oldDistances_14; }
	inline void set_oldDistances_14(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___oldDistances_14 = value;
		Il2CppCodeGenWriteBarrier((&___oldDistances_14), value);
	}

	inline static int32_t get_offset_of_colors_15() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___colors_15)); }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * get_colors_15() const { return ___colors_15; }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E ** get_address_of_colors_15() { return &___colors_15; }
	inline void set_colors_15(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * value)
	{
		___colors_15 = value;
		Il2CppCodeGenWriteBarrier((&___colors_15), value);
	}

	inline static int32_t get_offset_of_objectNumbers3_16() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers3_16)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers3_16() const { return ___objectNumbers3_16; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers3_16() { return &___objectNumbers3_16; }
	inline void set_objectNumbers3_16(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers3_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers3_16), value);
	}

	inline static int32_t get_offset_of__arrayCount3_17() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount3_17)); }
	inline int32_t get__arrayCount3_17() const { return ____arrayCount3_17; }
	inline int32_t* get_address_of__arrayCount3_17() { return &____arrayCount3_17; }
	inline void set__arrayCount3_17(int32_t value)
	{
		____arrayCount3_17 = value;
	}

	inline static int32_t get_offset_of_meshTable_18() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___meshTable_18)); }
	inline Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * get_meshTable_18() const { return ___meshTable_18; }
	inline Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 ** get_address_of_meshTable_18() { return &___meshTable_18; }
	inline void set_meshTable_18(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * value)
	{
		___meshTable_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshTable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#ifndef VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#define VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Visibility
struct  Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174 
{
public:
	// System.Int32 Vectrosity.Visibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#define MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifndef MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#define MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#define VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine
struct  VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_lineVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_lineVertices_0;
	// UnityEngine.Vector2[] Vectrosity.VectorLine::m_lineUVs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_lineUVs_1;
	// UnityEngine.Color32[] Vectrosity.VectorLine::m_lineColors
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_lineColors_2;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::m_lineTriangles
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lineTriangles_3;
	// System.Int32 Vectrosity.VectorLine::m_vertexCount
	int32_t ___m_vertexCount_4;
	// UnityEngine.GameObject Vectrosity.VectorLine::m_go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_go_5;
	// UnityEngine.RectTransform Vectrosity.VectorLine::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_6;
	// Vectrosity.IVectorObject Vectrosity.VectorLine::m_vectorObject
	RuntimeObject* ___m_vectorObject_7;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_color_8;
	// Vectrosity.CanvasState Vectrosity.VectorLine::m_canvasState
	int32_t ___m_canvasState_9;
	// System.Boolean Vectrosity.VectorLine::m_is2D
	bool ___m_is2D_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::m_points2
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_points2_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::m_points3
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_points3_12;
	// System.Int32 Vectrosity.VectorLine::m_pointsCount
	int32_t ___m_pointsCount_13;
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_screenPoints
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_screenPoints_14;
	// System.Single[] Vectrosity.VectorLine::m_lineWidths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_lineWidths_15;
	// System.Single Vectrosity.VectorLine::m_lineWidth
	float ___m_lineWidth_16;
	// System.Single Vectrosity.VectorLine::m_maxWeldDistance
	float ___m_maxWeldDistance_17;
	// System.Single[] Vectrosity.VectorLine::m_distances
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_distances_18;
	// System.String Vectrosity.VectorLine::m_name
	String_t* ___m_name_19;
	// UnityEngine.Material Vectrosity.VectorLine::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_20;
	// UnityEngine.Texture Vectrosity.VectorLine::m_originalTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_originalTexture_21;
	// UnityEngine.Texture Vectrosity.VectorLine::m_texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_texture_22;
	// System.Boolean Vectrosity.VectorLine::m_active
	bool ___m_active_23;
	// Vectrosity.LineType Vectrosity.VectorLine::m_lineType
	int32_t ___m_lineType_24;
	// System.Single Vectrosity.VectorLine::m_capLength
	float ___m_capLength_25;
	// System.Boolean Vectrosity.VectorLine::m_smoothWidth
	bool ___m_smoothWidth_26;
	// System.Boolean Vectrosity.VectorLine::m_smoothColor
	bool ___m_smoothColor_27;
	// Vectrosity.Joins Vectrosity.VectorLine::m_joins
	int32_t ___m_joins_28;
	// System.Boolean Vectrosity.VectorLine::m_isAutoDrawing
	bool ___m_isAutoDrawing_29;
	// System.Int32 Vectrosity.VectorLine::m_drawStart
	int32_t ___m_drawStart_30;
	// System.Int32 Vectrosity.VectorLine::m_drawEnd
	int32_t ___m_drawEnd_31;
	// System.Int32 Vectrosity.VectorLine::m_endPointsUpdate
	int32_t ___m_endPointsUpdate_32;
	// System.Boolean Vectrosity.VectorLine::m_useNormals
	bool ___m_useNormals_33;
	// System.Boolean Vectrosity.VectorLine::m_useTangents
	bool ___m_useTangents_34;
	// System.Boolean Vectrosity.VectorLine::m_normalsCalculated
	bool ___m_normalsCalculated_35;
	// System.Boolean Vectrosity.VectorLine::m_tangentsCalculated
	bool ___m_tangentsCalculated_36;
	// Vectrosity.EndCap Vectrosity.VectorLine::m_capType
	int32_t ___m_capType_37;
	// System.String Vectrosity.VectorLine::m_endCap
	String_t* ___m_endCap_38;
	// System.Boolean Vectrosity.VectorLine::m_useCapColors
	bool ___m_useCapColors_39;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_frontColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_frontColor_40;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_backColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_backColor_41;
	// System.Int32 Vectrosity.VectorLine::m_frontEndCapIndex
	int32_t ___m_frontEndCapIndex_42;
	// System.Int32 Vectrosity.VectorLine::m_backEndCapIndex
	int32_t ___m_backEndCapIndex_43;
	// System.Single Vectrosity.VectorLine::m_lineUVBottom
	float ___m_lineUVBottom_44;
	// System.Single Vectrosity.VectorLine::m_lineUVTop
	float ___m_lineUVTop_45;
	// System.Single Vectrosity.VectorLine::m_frontCapUVBottom
	float ___m_frontCapUVBottom_46;
	// System.Single Vectrosity.VectorLine::m_frontCapUVTop
	float ___m_frontCapUVTop_47;
	// System.Single Vectrosity.VectorLine::m_backCapUVBottom
	float ___m_backCapUVBottom_48;
	// System.Single Vectrosity.VectorLine::m_backCapUVTop
	float ___m_backCapUVTop_49;
	// System.Boolean Vectrosity.VectorLine::m_continuousTexture
	bool ___m_continuousTexture_50;
	// UnityEngine.Transform Vectrosity.VectorLine::m_drawTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_drawTransform_51;
	// System.Boolean Vectrosity.VectorLine::m_viewportDraw
	bool ___m_viewportDraw_52;
	// System.Single Vectrosity.VectorLine::m_textureScale
	float ___m_textureScale_53;
	// System.Boolean Vectrosity.VectorLine::m_useTextureScale
	bool ___m_useTextureScale_54;
	// System.Single Vectrosity.VectorLine::m_textureOffset
	float ___m_textureOffset_55;
	// System.Boolean Vectrosity.VectorLine::m_useMatrix
	bool ___m_useMatrix_56;
	// UnityEngine.Matrix4x4 Vectrosity.VectorLine::m_matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_matrix_57;
	// System.Boolean Vectrosity.VectorLine::m_collider
	bool ___m_collider_58;
	// System.Boolean Vectrosity.VectorLine::m_trigger
	bool ___m_trigger_59;
	// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::m_physicsMaterial
	PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * ___m_physicsMaterial_60;
	// System.Boolean Vectrosity.VectorLine::m_alignOddWidthToPixels
	bool ___m_alignOddWidthToPixels_61;

public:
	inline static int32_t get_offset_of_m_lineVertices_0() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineVertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_lineVertices_0() const { return ___m_lineVertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_lineVertices_0() { return &___m_lineVertices_0; }
	inline void set_m_lineVertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_lineVertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineVertices_0), value);
	}

	inline static int32_t get_offset_of_m_lineUVs_1() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVs_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_lineUVs_1() const { return ___m_lineUVs_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_lineUVs_1() { return &___m_lineUVs_1; }
	inline void set_m_lineUVs_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_lineUVs_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineUVs_1), value);
	}

	inline static int32_t get_offset_of_m_lineColors_2() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineColors_2)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_lineColors_2() const { return ___m_lineColors_2; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_lineColors_2() { return &___m_lineColors_2; }
	inline void set_m_lineColors_2(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_lineColors_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineColors_2), value);
	}

	inline static int32_t get_offset_of_m_lineTriangles_3() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineTriangles_3)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lineTriangles_3() const { return ___m_lineTriangles_3; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lineTriangles_3() { return &___m_lineTriangles_3; }
	inline void set_m_lineTriangles_3(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lineTriangles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineTriangles_3), value);
	}

	inline static int32_t get_offset_of_m_vertexCount_4() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vertexCount_4)); }
	inline int32_t get_m_vertexCount_4() const { return ___m_vertexCount_4; }
	inline int32_t* get_address_of_m_vertexCount_4() { return &___m_vertexCount_4; }
	inline void set_m_vertexCount_4(int32_t value)
	{
		___m_vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_m_go_5() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_go_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_go_5() const { return ___m_go_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_go_5() { return &___m_go_5; }
	inline void set_m_go_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_go_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_go_5), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_6() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_rectTransform_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_6() const { return ___m_rectTransform_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_6() { return &___m_rectTransform_6; }
	inline void set_m_rectTransform_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_6), value);
	}

	inline static int32_t get_offset_of_m_vectorObject_7() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vectorObject_7)); }
	inline RuntimeObject* get_m_vectorObject_7() const { return ___m_vectorObject_7; }
	inline RuntimeObject** get_address_of_m_vectorObject_7() { return &___m_vectorObject_7; }
	inline void set_m_vectorObject_7(RuntimeObject* value)
	{
		___m_vectorObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorObject_7), value);
	}

	inline static int32_t get_offset_of_m_color_8() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_color_8)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_color_8() const { return ___m_color_8; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_color_8() { return &___m_color_8; }
	inline void set_m_color_8(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_color_8 = value;
	}

	inline static int32_t get_offset_of_m_canvasState_9() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_canvasState_9)); }
	inline int32_t get_m_canvasState_9() const { return ___m_canvasState_9; }
	inline int32_t* get_address_of_m_canvasState_9() { return &___m_canvasState_9; }
	inline void set_m_canvasState_9(int32_t value)
	{
		___m_canvasState_9 = value;
	}

	inline static int32_t get_offset_of_m_is2D_10() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_is2D_10)); }
	inline bool get_m_is2D_10() const { return ___m_is2D_10; }
	inline bool* get_address_of_m_is2D_10() { return &___m_is2D_10; }
	inline void set_m_is2D_10(bool value)
	{
		___m_is2D_10 = value;
	}

	inline static int32_t get_offset_of_m_points2_11() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points2_11)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_points2_11() const { return ___m_points2_11; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_points2_11() { return &___m_points2_11; }
	inline void set_m_points2_11(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_points2_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_points2_11), value);
	}

	inline static int32_t get_offset_of_m_points3_12() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points3_12)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_points3_12() const { return ___m_points3_12; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_points3_12() { return &___m_points3_12; }
	inline void set_m_points3_12(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_points3_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_points3_12), value);
	}

	inline static int32_t get_offset_of_m_pointsCount_13() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_pointsCount_13)); }
	inline int32_t get_m_pointsCount_13() const { return ___m_pointsCount_13; }
	inline int32_t* get_address_of_m_pointsCount_13() { return &___m_pointsCount_13; }
	inline void set_m_pointsCount_13(int32_t value)
	{
		___m_pointsCount_13 = value;
	}

	inline static int32_t get_offset_of_m_screenPoints_14() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_screenPoints_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_screenPoints_14() const { return ___m_screenPoints_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_screenPoints_14() { return &___m_screenPoints_14; }
	inline void set_m_screenPoints_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_screenPoints_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_screenPoints_14), value);
	}

	inline static int32_t get_offset_of_m_lineWidths_15() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidths_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_lineWidths_15() const { return ___m_lineWidths_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_lineWidths_15() { return &___m_lineWidths_15; }
	inline void set_m_lineWidths_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_lineWidths_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWidths_15), value);
	}

	inline static int32_t get_offset_of_m_lineWidth_16() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidth_16)); }
	inline float get_m_lineWidth_16() const { return ___m_lineWidth_16; }
	inline float* get_address_of_m_lineWidth_16() { return &___m_lineWidth_16; }
	inline void set_m_lineWidth_16(float value)
	{
		___m_lineWidth_16 = value;
	}

	inline static int32_t get_offset_of_m_maxWeldDistance_17() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_maxWeldDistance_17)); }
	inline float get_m_maxWeldDistance_17() const { return ___m_maxWeldDistance_17; }
	inline float* get_address_of_m_maxWeldDistance_17() { return &___m_maxWeldDistance_17; }
	inline void set_m_maxWeldDistance_17(float value)
	{
		___m_maxWeldDistance_17 = value;
	}

	inline static int32_t get_offset_of_m_distances_18() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_distances_18)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_distances_18() const { return ___m_distances_18; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_distances_18() { return &___m_distances_18; }
	inline void set_m_distances_18(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_distances_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_distances_18), value);
	}

	inline static int32_t get_offset_of_m_name_19() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_name_19)); }
	inline String_t* get_m_name_19() const { return ___m_name_19; }
	inline String_t** get_address_of_m_name_19() { return &___m_name_19; }
	inline void set_m_name_19(String_t* value)
	{
		___m_name_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_19), value);
	}

	inline static int32_t get_offset_of_m_material_20() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_material_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_20() const { return ___m_material_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_20() { return &___m_material_20; }
	inline void set_m_material_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_20), value);
	}

	inline static int32_t get_offset_of_m_originalTexture_21() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_originalTexture_21)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_originalTexture_21() const { return ___m_originalTexture_21; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_originalTexture_21() { return &___m_originalTexture_21; }
	inline void set_m_originalTexture_21(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_originalTexture_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalTexture_21), value);
	}

	inline static int32_t get_offset_of_m_texture_22() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_texture_22)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_texture_22() const { return ___m_texture_22; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_texture_22() { return &___m_texture_22; }
	inline void set_m_texture_22(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_texture_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_texture_22), value);
	}

	inline static int32_t get_offset_of_m_active_23() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_active_23)); }
	inline bool get_m_active_23() const { return ___m_active_23; }
	inline bool* get_address_of_m_active_23() { return &___m_active_23; }
	inline void set_m_active_23(bool value)
	{
		___m_active_23 = value;
	}

	inline static int32_t get_offset_of_m_lineType_24() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineType_24)); }
	inline int32_t get_m_lineType_24() const { return ___m_lineType_24; }
	inline int32_t* get_address_of_m_lineType_24() { return &___m_lineType_24; }
	inline void set_m_lineType_24(int32_t value)
	{
		___m_lineType_24 = value;
	}

	inline static int32_t get_offset_of_m_capLength_25() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capLength_25)); }
	inline float get_m_capLength_25() const { return ___m_capLength_25; }
	inline float* get_address_of_m_capLength_25() { return &___m_capLength_25; }
	inline void set_m_capLength_25(float value)
	{
		___m_capLength_25 = value;
	}

	inline static int32_t get_offset_of_m_smoothWidth_26() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothWidth_26)); }
	inline bool get_m_smoothWidth_26() const { return ___m_smoothWidth_26; }
	inline bool* get_address_of_m_smoothWidth_26() { return &___m_smoothWidth_26; }
	inline void set_m_smoothWidth_26(bool value)
	{
		___m_smoothWidth_26 = value;
	}

	inline static int32_t get_offset_of_m_smoothColor_27() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothColor_27)); }
	inline bool get_m_smoothColor_27() const { return ___m_smoothColor_27; }
	inline bool* get_address_of_m_smoothColor_27() { return &___m_smoothColor_27; }
	inline void set_m_smoothColor_27(bool value)
	{
		___m_smoothColor_27 = value;
	}

	inline static int32_t get_offset_of_m_joins_28() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_joins_28)); }
	inline int32_t get_m_joins_28() const { return ___m_joins_28; }
	inline int32_t* get_address_of_m_joins_28() { return &___m_joins_28; }
	inline void set_m_joins_28(int32_t value)
	{
		___m_joins_28 = value;
	}

	inline static int32_t get_offset_of_m_isAutoDrawing_29() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_isAutoDrawing_29)); }
	inline bool get_m_isAutoDrawing_29() const { return ___m_isAutoDrawing_29; }
	inline bool* get_address_of_m_isAutoDrawing_29() { return &___m_isAutoDrawing_29; }
	inline void set_m_isAutoDrawing_29(bool value)
	{
		___m_isAutoDrawing_29 = value;
	}

	inline static int32_t get_offset_of_m_drawStart_30() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawStart_30)); }
	inline int32_t get_m_drawStart_30() const { return ___m_drawStart_30; }
	inline int32_t* get_address_of_m_drawStart_30() { return &___m_drawStart_30; }
	inline void set_m_drawStart_30(int32_t value)
	{
		___m_drawStart_30 = value;
	}

	inline static int32_t get_offset_of_m_drawEnd_31() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawEnd_31)); }
	inline int32_t get_m_drawEnd_31() const { return ___m_drawEnd_31; }
	inline int32_t* get_address_of_m_drawEnd_31() { return &___m_drawEnd_31; }
	inline void set_m_drawEnd_31(int32_t value)
	{
		___m_drawEnd_31 = value;
	}

	inline static int32_t get_offset_of_m_endPointsUpdate_32() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endPointsUpdate_32)); }
	inline int32_t get_m_endPointsUpdate_32() const { return ___m_endPointsUpdate_32; }
	inline int32_t* get_address_of_m_endPointsUpdate_32() { return &___m_endPointsUpdate_32; }
	inline void set_m_endPointsUpdate_32(int32_t value)
	{
		___m_endPointsUpdate_32 = value;
	}

	inline static int32_t get_offset_of_m_useNormals_33() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useNormals_33)); }
	inline bool get_m_useNormals_33() const { return ___m_useNormals_33; }
	inline bool* get_address_of_m_useNormals_33() { return &___m_useNormals_33; }
	inline void set_m_useNormals_33(bool value)
	{
		___m_useNormals_33 = value;
	}

	inline static int32_t get_offset_of_m_useTangents_34() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTangents_34)); }
	inline bool get_m_useTangents_34() const { return ___m_useTangents_34; }
	inline bool* get_address_of_m_useTangents_34() { return &___m_useTangents_34; }
	inline void set_m_useTangents_34(bool value)
	{
		___m_useTangents_34 = value;
	}

	inline static int32_t get_offset_of_m_normalsCalculated_35() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_normalsCalculated_35)); }
	inline bool get_m_normalsCalculated_35() const { return ___m_normalsCalculated_35; }
	inline bool* get_address_of_m_normalsCalculated_35() { return &___m_normalsCalculated_35; }
	inline void set_m_normalsCalculated_35(bool value)
	{
		___m_normalsCalculated_35 = value;
	}

	inline static int32_t get_offset_of_m_tangentsCalculated_36() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_tangentsCalculated_36)); }
	inline bool get_m_tangentsCalculated_36() const { return ___m_tangentsCalculated_36; }
	inline bool* get_address_of_m_tangentsCalculated_36() { return &___m_tangentsCalculated_36; }
	inline void set_m_tangentsCalculated_36(bool value)
	{
		___m_tangentsCalculated_36 = value;
	}

	inline static int32_t get_offset_of_m_capType_37() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capType_37)); }
	inline int32_t get_m_capType_37() const { return ___m_capType_37; }
	inline int32_t* get_address_of_m_capType_37() { return &___m_capType_37; }
	inline void set_m_capType_37(int32_t value)
	{
		___m_capType_37 = value;
	}

	inline static int32_t get_offset_of_m_endCap_38() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endCap_38)); }
	inline String_t* get_m_endCap_38() const { return ___m_endCap_38; }
	inline String_t** get_address_of_m_endCap_38() { return &___m_endCap_38; }
	inline void set_m_endCap_38(String_t* value)
	{
		___m_endCap_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_endCap_38), value);
	}

	inline static int32_t get_offset_of_m_useCapColors_39() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useCapColors_39)); }
	inline bool get_m_useCapColors_39() const { return ___m_useCapColors_39; }
	inline bool* get_address_of_m_useCapColors_39() { return &___m_useCapColors_39; }
	inline void set_m_useCapColors_39(bool value)
	{
		___m_useCapColors_39 = value;
	}

	inline static int32_t get_offset_of_m_frontColor_40() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontColor_40)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_frontColor_40() const { return ___m_frontColor_40; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_frontColor_40() { return &___m_frontColor_40; }
	inline void set_m_frontColor_40(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_frontColor_40 = value;
	}

	inline static int32_t get_offset_of_m_backColor_41() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backColor_41)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_backColor_41() const { return ___m_backColor_41; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_backColor_41() { return &___m_backColor_41; }
	inline void set_m_backColor_41(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_backColor_41 = value;
	}

	inline static int32_t get_offset_of_m_frontEndCapIndex_42() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontEndCapIndex_42)); }
	inline int32_t get_m_frontEndCapIndex_42() const { return ___m_frontEndCapIndex_42; }
	inline int32_t* get_address_of_m_frontEndCapIndex_42() { return &___m_frontEndCapIndex_42; }
	inline void set_m_frontEndCapIndex_42(int32_t value)
	{
		___m_frontEndCapIndex_42 = value;
	}

	inline static int32_t get_offset_of_m_backEndCapIndex_43() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backEndCapIndex_43)); }
	inline int32_t get_m_backEndCapIndex_43() const { return ___m_backEndCapIndex_43; }
	inline int32_t* get_address_of_m_backEndCapIndex_43() { return &___m_backEndCapIndex_43; }
	inline void set_m_backEndCapIndex_43(int32_t value)
	{
		___m_backEndCapIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_lineUVBottom_44() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVBottom_44)); }
	inline float get_m_lineUVBottom_44() const { return ___m_lineUVBottom_44; }
	inline float* get_address_of_m_lineUVBottom_44() { return &___m_lineUVBottom_44; }
	inline void set_m_lineUVBottom_44(float value)
	{
		___m_lineUVBottom_44 = value;
	}

	inline static int32_t get_offset_of_m_lineUVTop_45() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVTop_45)); }
	inline float get_m_lineUVTop_45() const { return ___m_lineUVTop_45; }
	inline float* get_address_of_m_lineUVTop_45() { return &___m_lineUVTop_45; }
	inline void set_m_lineUVTop_45(float value)
	{
		___m_lineUVTop_45 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVBottom_46() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVBottom_46)); }
	inline float get_m_frontCapUVBottom_46() const { return ___m_frontCapUVBottom_46; }
	inline float* get_address_of_m_frontCapUVBottom_46() { return &___m_frontCapUVBottom_46; }
	inline void set_m_frontCapUVBottom_46(float value)
	{
		___m_frontCapUVBottom_46 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVTop_47() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVTop_47)); }
	inline float get_m_frontCapUVTop_47() const { return ___m_frontCapUVTop_47; }
	inline float* get_address_of_m_frontCapUVTop_47() { return &___m_frontCapUVTop_47; }
	inline void set_m_frontCapUVTop_47(float value)
	{
		___m_frontCapUVTop_47 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVBottom_48() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVBottom_48)); }
	inline float get_m_backCapUVBottom_48() const { return ___m_backCapUVBottom_48; }
	inline float* get_address_of_m_backCapUVBottom_48() { return &___m_backCapUVBottom_48; }
	inline void set_m_backCapUVBottom_48(float value)
	{
		___m_backCapUVBottom_48 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVTop_49() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVTop_49)); }
	inline float get_m_backCapUVTop_49() const { return ___m_backCapUVTop_49; }
	inline float* get_address_of_m_backCapUVTop_49() { return &___m_backCapUVTop_49; }
	inline void set_m_backCapUVTop_49(float value)
	{
		___m_backCapUVTop_49 = value;
	}

	inline static int32_t get_offset_of_m_continuousTexture_50() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_continuousTexture_50)); }
	inline bool get_m_continuousTexture_50() const { return ___m_continuousTexture_50; }
	inline bool* get_address_of_m_continuousTexture_50() { return &___m_continuousTexture_50; }
	inline void set_m_continuousTexture_50(bool value)
	{
		___m_continuousTexture_50 = value;
	}

	inline static int32_t get_offset_of_m_drawTransform_51() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawTransform_51)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_drawTransform_51() const { return ___m_drawTransform_51; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_drawTransform_51() { return &___m_drawTransform_51; }
	inline void set_m_drawTransform_51(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_drawTransform_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_drawTransform_51), value);
	}

	inline static int32_t get_offset_of_m_viewportDraw_52() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_viewportDraw_52)); }
	inline bool get_m_viewportDraw_52() const { return ___m_viewportDraw_52; }
	inline bool* get_address_of_m_viewportDraw_52() { return &___m_viewportDraw_52; }
	inline void set_m_viewportDraw_52(bool value)
	{
		___m_viewportDraw_52 = value;
	}

	inline static int32_t get_offset_of_m_textureScale_53() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureScale_53)); }
	inline float get_m_textureScale_53() const { return ___m_textureScale_53; }
	inline float* get_address_of_m_textureScale_53() { return &___m_textureScale_53; }
	inline void set_m_textureScale_53(float value)
	{
		___m_textureScale_53 = value;
	}

	inline static int32_t get_offset_of_m_useTextureScale_54() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTextureScale_54)); }
	inline bool get_m_useTextureScale_54() const { return ___m_useTextureScale_54; }
	inline bool* get_address_of_m_useTextureScale_54() { return &___m_useTextureScale_54; }
	inline void set_m_useTextureScale_54(bool value)
	{
		___m_useTextureScale_54 = value;
	}

	inline static int32_t get_offset_of_m_textureOffset_55() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureOffset_55)); }
	inline float get_m_textureOffset_55() const { return ___m_textureOffset_55; }
	inline float* get_address_of_m_textureOffset_55() { return &___m_textureOffset_55; }
	inline void set_m_textureOffset_55(float value)
	{
		___m_textureOffset_55 = value;
	}

	inline static int32_t get_offset_of_m_useMatrix_56() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useMatrix_56)); }
	inline bool get_m_useMatrix_56() const { return ___m_useMatrix_56; }
	inline bool* get_address_of_m_useMatrix_56() { return &___m_useMatrix_56; }
	inline void set_m_useMatrix_56(bool value)
	{
		___m_useMatrix_56 = value;
	}

	inline static int32_t get_offset_of_m_matrix_57() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_matrix_57)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_matrix_57() const { return ___m_matrix_57; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_matrix_57() { return &___m_matrix_57; }
	inline void set_m_matrix_57(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_matrix_57 = value;
	}

	inline static int32_t get_offset_of_m_collider_58() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_collider_58)); }
	inline bool get_m_collider_58() const { return ___m_collider_58; }
	inline bool* get_address_of_m_collider_58() { return &___m_collider_58; }
	inline void set_m_collider_58(bool value)
	{
		___m_collider_58 = value;
	}

	inline static int32_t get_offset_of_m_trigger_59() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_trigger_59)); }
	inline bool get_m_trigger_59() const { return ___m_trigger_59; }
	inline bool* get_address_of_m_trigger_59() { return &___m_trigger_59; }
	inline void set_m_trigger_59(bool value)
	{
		___m_trigger_59 = value;
	}

	inline static int32_t get_offset_of_m_physicsMaterial_60() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_physicsMaterial_60)); }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * get_m_physicsMaterial_60() const { return ___m_physicsMaterial_60; }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 ** get_address_of_m_physicsMaterial_60() { return &___m_physicsMaterial_60; }
	inline void set_m_physicsMaterial_60(PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * value)
	{
		___m_physicsMaterial_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_physicsMaterial_60), value);
	}

	inline static int32_t get_offset_of_m_alignOddWidthToPixels_61() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_alignOddWidthToPixels_61)); }
	inline bool get_m_alignOddWidthToPixels_61() const { return ___m_alignOddWidthToPixels_61; }
	inline bool* get_address_of_m_alignOddWidthToPixels_61() { return &___m_alignOddWidthToPixels_61; }
	inline void set_m_alignOddWidthToPixels_61(bool value)
	{
		___m_alignOddWidthToPixels_61 = value;
	}
};

struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields
{
public:
	// UnityEngine.Vector3 Vectrosity.VectorLine::v3zero
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v3zero_62;
	// UnityEngine.Canvas Vectrosity.VectorLine::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_63;
	// UnityEngine.Transform Vectrosity.VectorLine::camTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___camTransform_64;
	// UnityEngine.Camera Vectrosity.VectorLine::cam3D
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam3D_65;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldPosition_66;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldRotation_67;
	// System.Boolean Vectrosity.VectorLine::lineManagerCreated
	bool ___lineManagerCreated_68;
	// Vectrosity.LineManager Vectrosity.VectorLine::m_lineManager
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * ___m_lineManager_69;
	// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo> Vectrosity.VectorLine::capDictionary
	Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * ___capDictionary_70;
	// System.Int32 Vectrosity.VectorLine::endianDiff1
	int32_t ___endianDiff1_71;
	// System.Int32 Vectrosity.VectorLine::endianDiff2
	int32_t ___endianDiff2_72;
	// System.Byte[] Vectrosity.VectorLine::byteBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___byteBlock_73;
	// System.String[] Vectrosity.VectorLine::functionNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___functionNames_74;

public:
	inline static int32_t get_offset_of_v3zero_62() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___v3zero_62)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v3zero_62() const { return ___v3zero_62; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v3zero_62() { return &___v3zero_62; }
	inline void set_v3zero_62(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v3zero_62 = value;
	}

	inline static int32_t get_offset_of_m_canvas_63() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_canvas_63)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_63() const { return ___m_canvas_63; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_63() { return &___m_canvas_63; }
	inline void set_m_canvas_63(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_63), value);
	}

	inline static int32_t get_offset_of_camTransform_64() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___camTransform_64)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_camTransform_64() const { return ___camTransform_64; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_camTransform_64() { return &___camTransform_64; }
	inline void set_camTransform_64(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___camTransform_64 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_64), value);
	}

	inline static int32_t get_offset_of_cam3D_65() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___cam3D_65)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam3D_65() const { return ___cam3D_65; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam3D_65() { return &___cam3D_65; }
	inline void set_cam3D_65(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam3D_65 = value;
		Il2CppCodeGenWriteBarrier((&___cam3D_65), value);
	}

	inline static int32_t get_offset_of_oldPosition_66() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldPosition_66)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldPosition_66() const { return ___oldPosition_66; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldPosition_66() { return &___oldPosition_66; }
	inline void set_oldPosition_66(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldPosition_66 = value;
	}

	inline static int32_t get_offset_of_oldRotation_67() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldRotation_67)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldRotation_67() const { return ___oldRotation_67; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldRotation_67() { return &___oldRotation_67; }
	inline void set_oldRotation_67(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldRotation_67 = value;
	}

	inline static int32_t get_offset_of_lineManagerCreated_68() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___lineManagerCreated_68)); }
	inline bool get_lineManagerCreated_68() const { return ___lineManagerCreated_68; }
	inline bool* get_address_of_lineManagerCreated_68() { return &___lineManagerCreated_68; }
	inline void set_lineManagerCreated_68(bool value)
	{
		___lineManagerCreated_68 = value;
	}

	inline static int32_t get_offset_of_m_lineManager_69() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_lineManager_69)); }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * get_m_lineManager_69() const { return ___m_lineManager_69; }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 ** get_address_of_m_lineManager_69() { return &___m_lineManager_69; }
	inline void set_m_lineManager_69(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * value)
	{
		___m_lineManager_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineManager_69), value);
	}

	inline static int32_t get_offset_of_capDictionary_70() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___capDictionary_70)); }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * get_capDictionary_70() const { return ___capDictionary_70; }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D ** get_address_of_capDictionary_70() { return &___capDictionary_70; }
	inline void set_capDictionary_70(Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * value)
	{
		___capDictionary_70 = value;
		Il2CppCodeGenWriteBarrier((&___capDictionary_70), value);
	}

	inline static int32_t get_offset_of_endianDiff1_71() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff1_71)); }
	inline int32_t get_endianDiff1_71() const { return ___endianDiff1_71; }
	inline int32_t* get_address_of_endianDiff1_71() { return &___endianDiff1_71; }
	inline void set_endianDiff1_71(int32_t value)
	{
		___endianDiff1_71 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_72() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff2_72)); }
	inline int32_t get_endianDiff2_72() const { return ___endianDiff2_72; }
	inline int32_t* get_address_of_endianDiff2_72() { return &___endianDiff2_72; }
	inline void set_endianDiff2_72(int32_t value)
	{
		___endianDiff2_72 = value;
	}

	inline static int32_t get_offset_of_byteBlock_73() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___byteBlock_73)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_byteBlock_73() const { return ___byteBlock_73; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_byteBlock_73() { return &___byteBlock_73; }
	inline void set_byteBlock_73(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___byteBlock_73 = value;
		Il2CppCodeGenWriteBarrier((&___byteBlock_73), value);
	}

	inline static int32_t get_offset_of_functionNames_74() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___functionNames_74)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_functionNames_74() const { return ___functionNames_74; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_functionNames_74() { return &___functionNames_74; }
	inline void set_functionNames_74(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___functionNames_74 = value;
		Il2CppCodeGenWriteBarrier((&___functionNames_74), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#define CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisMaskU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72, ___U3CisMaskU3Ek__BackingField_4)); }
	inline bool get_U3CisMaskU3Ek__BackingField_4() const { return ___U3CisMaskU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisMaskU3Ek__BackingField_4() { return &___U3CisMaskU3Ek__BackingField_4; }
	inline void set_U3CisMaskU3Ek__BackingField_4(bool value)
	{
		___U3CisMaskU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifndef MESHFILTER_T8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_H
#define MESHFILTER_T8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_H
#ifndef RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#define RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#define MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef RECTTRANSFORM_T285CBD8775B25174B75164F10618F8B9728E1B20_H
#define RECTTRANSFORM_T285CBD8775B25174B75164F10618F8B9728E1B20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20  : public Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA
{
public:

public:
};

struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T285CBD8775B25174B75164F10618F8B9728E1B20_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#define BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.BrightnessControl
struct  BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.BrightnessControl::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.BrightnessControl::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.BrightnessControl::m_useLine
	bool ___m_useLine_6;
	// System.Boolean Vectrosity.BrightnessControl::m_destroyed
	bool ___m_destroyed_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_useLine_6() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_useLine_6)); }
	inline bool get_m_useLine_6() const { return ___m_useLine_6; }
	inline bool* get_address_of_m_useLine_6() { return &___m_useLine_6; }
	inline void set_m_useLine_6(bool value)
	{
		___m_useLine_6 = value;
	}

	inline static int32_t get_offset_of_m_destroyed_7() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_destroyed_7)); }
	inline bool get_m_destroyed_7() const { return ___m_destroyed_7; }
	inline bool* get_address_of_m_destroyed_7() { return &___m_destroyed_7; }
	inline void set_m_destroyed_7(bool value)
	{
		___m_destroyed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#ifndef VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#define VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorObject3D
struct  VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Vectrosity.VectorObject3D::m_updateVerts
	bool ___m_updateVerts_4;
	// System.Boolean Vectrosity.VectorObject3D::m_updateUVs
	bool ___m_updateUVs_5;
	// System.Boolean Vectrosity.VectorObject3D::m_updateColors
	bool ___m_updateColors_6;
	// System.Boolean Vectrosity.VectorObject3D::m_updateNormals
	bool ___m_updateNormals_7;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTangents
	bool ___m_updateTangents_8;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTris
	bool ___m_updateTris_9;
	// UnityEngine.Mesh Vectrosity.VectorObject3D::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_10;
	// Vectrosity.VectorLine Vectrosity.VectorObject3D::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_11;
	// UnityEngine.Material Vectrosity.VectorObject3D::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_12;
	// System.Boolean Vectrosity.VectorObject3D::m_useCustomMaterial
	bool ___m_useCustomMaterial_13;

public:
	inline static int32_t get_offset_of_m_updateVerts_4() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateVerts_4)); }
	inline bool get_m_updateVerts_4() const { return ___m_updateVerts_4; }
	inline bool* get_address_of_m_updateVerts_4() { return &___m_updateVerts_4; }
	inline void set_m_updateVerts_4(bool value)
	{
		___m_updateVerts_4 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_5() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateUVs_5)); }
	inline bool get_m_updateUVs_5() const { return ___m_updateUVs_5; }
	inline bool* get_address_of_m_updateUVs_5() { return &___m_updateUVs_5; }
	inline void set_m_updateUVs_5(bool value)
	{
		___m_updateUVs_5 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_6() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateColors_6)); }
	inline bool get_m_updateColors_6() const { return ___m_updateColors_6; }
	inline bool* get_address_of_m_updateColors_6() { return &___m_updateColors_6; }
	inline void set_m_updateColors_6(bool value)
	{
		___m_updateColors_6 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_7() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateNormals_7)); }
	inline bool get_m_updateNormals_7() const { return ___m_updateNormals_7; }
	inline bool* get_address_of_m_updateNormals_7() { return &___m_updateNormals_7; }
	inline void set_m_updateNormals_7(bool value)
	{
		___m_updateNormals_7 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_8() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateTangents_8)); }
	inline bool get_m_updateTangents_8() const { return ___m_updateTangents_8; }
	inline bool* get_address_of_m_updateTangents_8() { return &___m_updateTangents_8; }
	inline void set_m_updateTangents_8(bool value)
	{
		___m_updateTangents_8 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_9() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateTris_9)); }
	inline bool get_m_updateTris_9() const { return ___m_updateTris_9; }
	inline bool* get_address_of_m_updateTris_9() { return &___m_updateTris_9; }
	inline void set_m_updateTris_9(bool value)
	{
		___m_updateTris_9 = value;
	}

	inline static int32_t get_offset_of_m_mesh_10() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_mesh_10)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_10() const { return ___m_mesh_10; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_10() { return &___m_mesh_10; }
	inline void set_m_mesh_10(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_10), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_11() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_vectorLine_11)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_11() const { return ___m_vectorLine_11; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_11() { return &___m_vectorLine_11; }
	inline void set_m_vectorLine_11(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_11), value);
	}

	inline static int32_t get_offset_of_m_material_12() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_material_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_12() const { return ___m_material_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_12() { return &___m_material_12; }
	inline void set_m_material_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_12), value);
	}

	inline static int32_t get_offset_of_m_useCustomMaterial_13() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_useCustomMaterial_13)); }
	inline bool get_m_useCustomMaterial_13() const { return ___m_useCustomMaterial_13; }
	inline bool* get_address_of_m_useCustomMaterial_13() { return &___m_useCustomMaterial_13; }
	inline void set_m_useCustomMaterial_13(bool value)
	{
		___m_useCustomMaterial_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#ifndef VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#define VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl
struct  VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControl::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControl::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControl::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControl::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#ifndef VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#define VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlAlways
struct  VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlAlways::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlAlways::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#ifndef VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#define VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlStatic
struct  VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlStatic::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlStatic::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;
	// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::m_originalMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_originalMatrix_8;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}

	inline static int32_t get_offset_of_m_originalMatrix_8() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_originalMatrix_8)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_originalMatrix_8() const { return ___m_originalMatrix_8; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_originalMatrix_8() { return &___m_originalMatrix_8; }
	inline void set_m_originalMatrix_8(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_originalMatrix_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#define RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Texture_30;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_UVRect_31;

public:
	inline static int32_t get_offset_of_m_Texture_30() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_Texture_30)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Texture_30() const { return ___m_Texture_30; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Texture_30() { return &___m_Texture_30; }
	inline void set_m_Texture_30(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Texture_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_30), value);
	}

	inline static int32_t get_offset_of_m_UVRect_31() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_UVRect_31)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_UVRect_31() const { return ___m_UVRect_31; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_UVRect_31() { return &___m_UVRect_31; }
	inline void set_m_UVRect_31(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_UVRect_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifndef VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H
#define VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorObject2D
struct  VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650  : public RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8
{
public:
	// System.Boolean Vectrosity.VectorObject2D::m_updateVerts
	bool ___m_updateVerts_32;
	// System.Boolean Vectrosity.VectorObject2D::m_updateUVs
	bool ___m_updateUVs_33;
	// System.Boolean Vectrosity.VectorObject2D::m_updateColors
	bool ___m_updateColors_34;
	// System.Boolean Vectrosity.VectorObject2D::m_updateNormals
	bool ___m_updateNormals_35;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTangents
	bool ___m_updateTangents_36;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTris
	bool ___m_updateTris_37;
	// UnityEngine.Mesh Vectrosity.VectorObject2D::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_38;
	// Vectrosity.VectorLine Vectrosity.VectorObject2D::vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___vectorLine_39;

public:
	inline static int32_t get_offset_of_m_updateVerts_32() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateVerts_32)); }
	inline bool get_m_updateVerts_32() const { return ___m_updateVerts_32; }
	inline bool* get_address_of_m_updateVerts_32() { return &___m_updateVerts_32; }
	inline void set_m_updateVerts_32(bool value)
	{
		___m_updateVerts_32 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_33() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateUVs_33)); }
	inline bool get_m_updateUVs_33() const { return ___m_updateUVs_33; }
	inline bool* get_address_of_m_updateUVs_33() { return &___m_updateUVs_33; }
	inline void set_m_updateUVs_33(bool value)
	{
		___m_updateUVs_33 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_34() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateColors_34)); }
	inline bool get_m_updateColors_34() const { return ___m_updateColors_34; }
	inline bool* get_address_of_m_updateColors_34() { return &___m_updateColors_34; }
	inline void set_m_updateColors_34(bool value)
	{
		___m_updateColors_34 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_35() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateNormals_35)); }
	inline bool get_m_updateNormals_35() const { return ___m_updateNormals_35; }
	inline bool* get_address_of_m_updateNormals_35() { return &___m_updateNormals_35; }
	inline void set_m_updateNormals_35(bool value)
	{
		___m_updateNormals_35 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_36() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateTangents_36)); }
	inline bool get_m_updateTangents_36() const { return ___m_updateTangents_36; }
	inline bool* get_address_of_m_updateTangents_36() { return &___m_updateTangents_36; }
	inline void set_m_updateTangents_36(bool value)
	{
		___m_updateTangents_36 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_37() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateTris_37)); }
	inline bool get_m_updateTris_37() const { return ___m_updateTris_37; }
	inline bool* get_address_of_m_updateTris_37() { return &___m_updateTris_37; }
	inline void set_m_updateTris_37(bool value)
	{
		___m_updateTris_37 = value;
	}

	inline static int32_t get_offset_of_m_mesh_38() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_mesh_38)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_38() const { return ___m_mesh_38; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_38() { return &___m_mesh_38; }
	inline void set_m_mesh_38(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_38), value);
	}

	inline static int32_t get_offset_of_vectorLine_39() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___vectorLine_39)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_vectorLine_39() const { return ___vectorLine_39; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_vectorLine_39() { return &___vectorLine_39; }
	inline void set_vectorLine_39(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___vectorLine_39 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLine_39), value);
	}
};

struct VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields
{
public:
	// UnityEngine.UI.VertexHelper Vectrosity.VectorObject2D::vertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___vertexHelper_40;

public:
	inline static int32_t get_offset_of_vertexHelper_40() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields, ___vertexHelper_40)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_vertexHelper_40() const { return ___vertexHelper_40; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_vertexHelper_40() { return &___vertexHelper_40; }
	inline void set_vertexHelper_40(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___vertexHelper_40 = value;
		Il2CppCodeGenWriteBarrier((&___vertexHelper_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  m_Items[1];

public:
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  m_Items[1];

public:
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  m_Items[1];

public:
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4EBC00E16E83DA33851A551757D2B7332D5756B9_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m6625C3BA931A6EE5D6DB46B9E743B40AAA30010B_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, RuntimeObject* p0, const RuntimeMethod* method);

// System.Boolean Vectrosity.VectorLine::get_camTransformExists()
extern "C" IL2CPP_METHOD_ATTR bool VectorLine_get_camTransformExists_m84DBFBBD4ABCFFD9422BAA3A5B8DD8D7C7A5BCAF (const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::SetCamera3D()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_SetCamera3D_m0E50CD711406F5B8DD24811AEB81E8670B4AD472 (const RuntimeMethod* method);
// UnityEngine.Vector3 Vectrosity.VectorLine::get_camTransformPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  VectorLine_get_camTransformPosition_m70168227EF54649B768D3DED01989A44FB231D2A (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31 (float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_m95FA247CA782490727E4AFF27971CA7D6BEC9506 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, int32_t ___visibility2, int32_t ___brightness3, bool ___makeBounds4, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  p0, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" IL2CPP_METHOD_ATTR Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, Type_t * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_DontDestroyLine_mE4BC69DDFE1E76C36033824C37C33A51A3F707F2 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::ResetLinePoints(Vectrosity.VisibilityControlStatic,Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * ___vcs0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlAlways::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways_DontDestroyLine_m0B8DEE28405AEF5E25655615B4EAF7F1F9F0FD32 (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" IL2CPP_METHOD_ATTR Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, Type_t * p0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl_Setup_mD333A77ED1FAC412903F1B213F1F532997042C76 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, bool ___makeBounds1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void Vectrosity.BrightnessControl::SetUseLine(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void BrightnessControl_SetUseLine_m66FFDE4527141BD8775D86DD699FFC308CA60217 (BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * __this, bool ___useLine0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl_DontDestroyLine_m59B23A6E17F5710C41641A7B12C604271F588C86 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_Setup_m8DF1DC3A026ADB970451E19CCD65782BA960FDC0 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, bool ___makeBounds1, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlAlways::Setup(Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways_Setup_m5619CA19EBF41DCFB139E1DE25633C14E432E7DF (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, const RuntimeMethod* method);
// System.Void Vectrosity.BrightnessControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void BrightnessControl_Setup_mA8D49EAAD7EC9F9FBDB480C1A0031C2D5E523B1D (BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, bool ___m_useLine1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::GetMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  VisibilityControlStatic_GetMatrix_m393A81CE4897F455D9006439D4A387AC398606A3 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_inverse_mBD3145C0D7977962E18C8B3BF63DD671F7917166 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::get_points3()
extern "C" IL2CPP_METHOD_ATTR List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, int32_t, const RuntimeMethod*))List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_gshared)(__this, p0, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, int32_t, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , const RuntimeMethod*))List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::.ctor()
inline void List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9 (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::.ctor()
inline void List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75 (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Vectrosity.VectorLine::set_drawTransform(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_set_drawTransform_m38C1334B48A49F7E717D92265934CDE9AD7A9F04 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::Add(!0)
inline void List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void Vectrosity.RefInt::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void RefInt__ctor_m855FAFA703575604693F4F1504775C0A43BCBD24 (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::Add(!0)
inline void List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * __this, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void Vectrosity.VectorLine::LineManagerEnable()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_LineManagerEnable_mEBBC8F2B41594EC8373A0D8F21E46228DAA88A5B (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Vectrosity.VectorLine>::get_Count()
inline int32_t List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634 (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Vectrosity.RefInt>::get_Item(System.Int32)
inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93 (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * (*) (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3 (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, p0, method);
}
// System.Void Vectrosity.VectorLine::LineManagerDisable()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_LineManagerDisable_m4BFBFA223E0EFF00E56649932B68704897873879 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
inline void List_1__ctor_mAB4C08A6D18053E4C3BE30A489615645071526A8 (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
inline void List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, const RuntimeMethod*))List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626_gshared)(__this, method);
}
// System.Void Vectrosity.VectorLine::LineManagerCheckDistance()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_LineManagerCheckDistance_m9325E4EEB3B2D04E6C6435AB1E6B71C305DF9740 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
inline void List_1_Add_mFE084338629BB4ECDD74DD412713A0FF01A1A08E (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D *, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, int32_t, const RuntimeMethod*))List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
inline void List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 , const RuntimeMethod*))List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mF372089EAC48892D8A5DB1C9721B8D816EB92F2E (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4_gshared)(__this, p0, method);
}
// System.Void Vectrosity.VectorManager::SetDistanceColor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_SetDistanceColor_m8EC2BBE45FD2A12808FC0978168D4C85DD0D1439 (int32_t ___i0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, int32_t, int32_t, const RuntimeMethod*))List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925_gshared)(__this, p0, p1, method);
}
// !0 System.Collections.Generic.List`1<Vectrosity.VectorLine>::get_Item(System.Int32)
inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1 (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * (*) (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Boolean Vectrosity.VectorLine::get_active()
extern "C" IL2CPP_METHOD_ATTR bool VectorLine_get_active_m1D98B4F0CA741171227832C0E72E32BD0F0B51A3 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * List_1_get_Item_m6FBF36E821E762256C887A96718EC1B49B67EA8D (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * (*) (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Single Vectrosity.VectorManager::GetBrightnessValue(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float VectorManager_GetBrightnessValue_m9AD38BBC736EBBB0BD805028EAC9FBEF852FB5C3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, int32_t, const RuntimeMethod*))List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, int32_t, const RuntimeMethod*))List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_gshared)(__this, p0, method);
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_SetColor_m9E40451DEC91BF6B567C658CF8612F4AF4DBBF6F (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Draw3D()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_Draw3D_m069CE6FCCFAA45724F1686EBB4BFE615DC73F999 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Draw()
extern "C" IL2CPP_METHOD_ATTR void VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  VectorManager_GetBounds_mE824FB050B62C410AFCEB570F5351824839335C4 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points30, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Bounds_set_min_m5933955F04FCC8E3B372EA72ECCD398BB057C844 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Bounds_set_max_m12B864B082A4A188C7624C1ABEFA34028DD5A603 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * GameObject_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mD1BA4FFEB800AB3D102141CD0A0ECE237EA70FB4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mFB43D5458906C4005145640D4396FDE5853AFA3A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::.ctor()
inline void Dictionary_2__ctor_m8EA95790B7EF7A3D250BF84526344E6A5FCCA549 (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 *, const RuntimeMethod*))Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared)(__this, method);
}
// System.String Vectrosity.VectorLine::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m299D4D802C5BC1E197E7B4B41CBBEE85EE9EE0C0 (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4EBC00E16E83DA33851A551757D2B7332D5756B9_gshared)(__this, p0, method);
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  VectorManager_GetBounds_m81458FB6B7B1F622EF00C2DF03E83416BA7AA4D7 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, const RuntimeMethod* method);
// UnityEngine.Mesh Vectrosity.VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * VectorManager_MakeBoundsMesh_m050BEFC2B6263AF23741435C1F7C579298366E1C (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::Add(!0,!1)
inline void Dictionary_2_Add_m2010B8DB8B89AF9E3AB5184F256BB89CC201EF1C (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * __this, String_t* p0, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 *, String_t*, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*))Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared)(__this, p0, p1, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::get_Item(!0)
inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * (*) (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m6625C3BA931A6EE5D6DB46B9E743B40AAA30010B_gshared)(__this, p0, method);
}
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshFilter_set_mesh_mA18AA96C75CC91CF0917BA1F437626499FAAF496 (MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RawImage__ctor_m9C69814EC2E8E295A3CDEF79789EF5A4C670CF72 (RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetTexture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetTexture_mCF3DBC916A9E526F82662176D592FDCCD13FDECE (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetMaterial_m417027141DC94371E45296532DB56E09ABA910BB (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void RawImage_set_texture_m897BC65663AFF15258A611CC6E3480B078F41D23 (RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetupMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetupMesh_m65B0C77C52B6ABCDF96C84107E658188DEDDD310 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * Graphic_get_rectTransform_m127FCBF38F4F0D4B264D892013A3AD9A507936DE (Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  RectTransform_get_rect_mE5F283FCB99A66403AC1F0607CA49C156D73A15E (RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::get_canvasRenderer()
extern "C" IL2CPP_METHOD_ATTR CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * Graphic_get_canvasRenderer_m3CCAFCDBCEB38A281BAAB89F2832171BB3D348A6 (Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void CanvasRenderer_SetMesh_mC87C841A52339C33E5B1C644C70FC9CC9C560988 (CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" IL2CPP_METHOD_ATTR void Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetMeshBounds()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  p0, const RuntimeMethod* method);
// UnityEngine.Vector3[] Vectrosity.VectorLine::get_lineVertices()
extern "C" IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* VectorLine_get_lineVertices_m57239935B0FD597F75E15E240895FC9BCD0B2205 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// UnityEngine.Vector2[] Vectrosity.VectorLine::get_lineUVs()
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* VectorLine_get_lineUVs_mA513323E5C67007AC265EB355157DB299CEE7451 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::get_vertexCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* p0, const RuntimeMethod* method);
// UnityEngine.Color32[] Vectrosity.VectorLine::get_lineColors()
extern "C" IL2CPP_METHOD_ATTR Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* VectorLine_get_lineColors_mAD82DDEAC5313ED04BAB4C61A251790804FB0917 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_colors32_m29459AFE2843D67E95D60E5A13B6F6AF7670230C (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::get_lineTriangles()
extern "C" IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * VectorLine_get_lineTriangles_mAF5521DCE9BBAF2E4833555194F10E1610A602B5 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Mesh_SetTriangles_m6A43D705DE751C622CCF88EC31C4EF1B53578BE5 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C" IL2CPP_METHOD_ATTR void Mesh_RecalculateNormals_m9F5DF412F81F250419D9887C76F549B692B7D027 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C" IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector4[] Vectrosity.VectorLine::CalculateTangents(UnityEngine.Vector3[])
extern "C" IL2CPP_METHOD_ATTR Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* VectorLine_CalculateTangents_m52E4D000B3E7EE01C0BF57AF951247B65492C5AD (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::Clear()
extern "C" IL2CPP_METHOD_ATTR void Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_sharedMaterial_mC94A354D9B0FCA081754A7CB51AEE5A9AD3946A3 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * p0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject3D::SetupMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetupMesh_m527507F5C9C7A8B91385F6F8D8F883BC41C1295C (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * Renderer_get_sharedMaterial_m2BE9FF3D269968F2E323AC60EFBBCC0B26E7E6F9 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// UnityEngine.Texture Vectrosity.VectorLine::get_texture()
extern "C" IL2CPP_METHOD_ATTR Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * VectorLine_get_texture_mAD80AB37FC67D382596F6BF8F5312B56B41F95D7 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * Component_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mF3F89565A9CEFF85AA1FB27C6EC64BE590DC386B (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void Vectrosity.VectorObject3D::SetVerts()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetVerts_mDE110B09DA7E3DC546E6B86A63549870053D1BEB (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C" IL2CPP_METHOD_ATTR void Mesh_RecalculateBounds_m1BF701FE2CEA4E8E1183FF878B812808ED1EBA49 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,Vectrosity.RefInt&)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___thisTransform0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** ___objectNum2, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::DrawArrayLine2(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19 (int32_t ___i0, const RuntimeMethod* method);
// System.Collections.IEnumerator Vectrosity.VisibilityControl::VisibilityTest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_VisibilityTest_m754BD51A9F8FEDCCAED123D8EEA0E2400267EB2F (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<VisibilityTest>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1__ctor_mEB2B89198D42EC091034E26A99FDCBEF61CE96E6 (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<OnBecameVisible>c__Iterator2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2__ctor_m3D0322E423D96DFFCFBF87D8A93AD700F80A65CA (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<OnBecameInvisible>c__Iterator3::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3__ctor_mD52E94CE8B2DA54CD7488B1255F4648CCA427DF1 (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityRemove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B (int32_t ___objectNumber0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine&)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_Destroy_mDE8EFC54CC11D3D871B74A67DCE1A0A3BC2FC3D8 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** ___line0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::set_active(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
extern "C" IL2CPP_METHOD_ATTR bool Renderer_get_isVisible_mE952393384B74AD7FE85354406B19F1157CB4EC0 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_gshared)(__this, p0, method);
}
// System.Void Vectrosity.VectorLine::set_points3(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR void VectorLine_set_points3_m192AE593418F39E0ECF76FC2BFD5985813AEC7D2 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * __this, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,Vectrosity.RefInt&)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticSetup_m1FA651D0D2A47D01C99369F33B95C88DEC9BB4B1 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** ___objectNum1, const RuntimeMethod* method);
// System.Collections.IEnumerator Vectrosity.VisibilityControlStatic::WaitCheck()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControlStatic_WaitCheck_m789C29BB85C9EB189F9AB3E8C97E2470CF3F24DC (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic/<WaitCheck>c__Iterator4::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4__ctor_m63DDECF3067CF698773C9989C203B980639ACA15 (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::DrawArrayLine(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081 (int32_t ___i0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityStaticRemove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticRemove_m62842272CFC8F10526322B4F60F7966532455867 (int32_t ___objectNumber0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorManager::.cctor()
extern "C" IL2CPP_METHOD_ATTR void VectorManager__cctor_m3AE1C5FEAC6053455ECDD21BC5F826FE393CA16C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager__cctor_m3AE1C5FEAC6053455ECDD21BC5F826FE393CA16C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_minBrightnessDistance_0((500.0f));
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_maxBrightnessDistance_1((250.0f));
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_brightnessLevels_2(((int32_t)32));
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_distanceCheckFrequency_3((0.2f));
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_useDraw3D_5((bool)0);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount_8(0);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount2_11(0);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount3_17(0);
		return;
	}
}
// System.Single Vectrosity.VectorManager::GetBrightnessValue(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float VectorManager_GetBrightnessValue_m9AD38BBC736EBBB0BD805028EAC9FBEF852FB5C3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBrightnessValue_m9AD38BBC736EBBB0BD805028EAC9FBEF852FB5C3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		bool L_0 = VectorLine_get_camTransformExists_m84DBFBBD4ABCFFD9422BAA3A5B8DD8D7C7A5BCAF(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_SetCamera3D_m0E50CD711406F5B8DD24811AEB81E8670B4AD472(/*hidden argument*/NULL);
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		float L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_minBrightnessDistance_0();
		float L_2 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_maxBrightnessDistance_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___pos0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = VectorLine_get_camTransformPosition_m70168227EF54649B768D3DED01989A44FB231D2A(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_7 = Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31(L_1, L_2, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_mBE9DB9235D4177C91832964336FC68925FEDF55C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, int32_t ___visibility2, int32_t ___brightness3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ObjectSetup_mBE9DB9235D4177C91832964336FC68925FEDF55C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___go0;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_1 = ___line1;
		int32_t L_2 = ___visibility2;
		int32_t L_3 = ___brightness3;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_ObjectSetup_m95FA247CA782490727E4AFF27971CA7D6BEC9506(L_0, L_1, L_2, L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_m95FA247CA782490727E4AFF27971CA7D6BEC9506 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, int32_t ___visibility2, int32_t ___brightness3, bool ___makeBounds4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ObjectSetup_m95FA247CA782490727E4AFF27971CA7D6BEC9506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * V_0 = NULL;
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * V_1 = NULL;
	VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * V_2 = NULL;
	BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * V_3 = NULL;
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_1 = { reinterpret_cast<intptr_t> (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_3 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_0, L_2, /*hidden argument*/NULL);
		V_0 = ((VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA *)IsInstClass((RuntimeObject*)L_3, VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_il2cpp_TypeInfo_var));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_0_0_0_var) };
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_7 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_4, L_6, /*hidden argument*/NULL);
		V_1 = ((VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B *)IsInstClass((RuntimeObject*)L_7, VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_il2cpp_TypeInfo_var));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_11 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_8, L_10, /*hidden argument*/NULL);
		V_2 = ((VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E *)IsInstClass((RuntimeObject*)L_11, VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_il2cpp_TypeInfo_var));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_13 = { reinterpret_cast<intptr_t> (BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_0_0_0_var) };
		Type_t * L_14 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_15 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_12, L_14, /*hidden argument*/NULL);
		V_3 = ((BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 *)IsInstClass((RuntimeObject*)L_15, BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_il2cpp_TypeInfo_var));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_17 = { reinterpret_cast<intptr_t> (MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_0_0_0_var) };
		Type_t * L_18 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_19 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_16, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(((MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 *)IsInstSealed((RuntimeObject*)L_19, MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_il2cpp_TypeInfo_var)), (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = ___go0;
		NullCheck(L_21);
		GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4(L_21, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4_RuntimeMethod_var);
	}

IL_007f:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_23 = { reinterpret_cast<intptr_t> (MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_25 = GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151(L_22, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(((MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED *)IsInstClass((RuntimeObject*)L_25, MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_il2cpp_TypeInfo_var)), (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_27 = ___go0;
		NullCheck(L_27);
		GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B(L_27, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B_RuntimeMethod_var);
	}

IL_00a6:
	{
		int32_t L_28 = ___visibility2;
		if (L_28)
		{
			goto IL_0124;
		}
	}
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00ca;
		}
	}
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_31 = V_1;
		NullCheck(L_31);
		VisibilityControlStatic_DontDestroyLine_mE4BC69DDFE1E76C36033824C37C33A51A3F707F2(L_31, /*hidden argument*/NULL);
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_32, /*hidden argument*/NULL);
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_33 = V_1;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_34 = ___line1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00e1;
		}
	}
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_37 = V_2;
		NullCheck(L_37);
		VisibilityControlAlways_DontDestroyLine_m0B8DEE28405AEF5E25655615B4EAF7F1F9F0FD32(L_37, /*hidden argument*/NULL);
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_38 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_38, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_40 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_39, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_011f;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_41 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_42 = { reinterpret_cast<intptr_t> (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_44 = GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70(L_41, L_43, /*hidden argument*/NULL);
		V_0 = ((VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA *)IsInstClass((RuntimeObject*)L_44, VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA_il2cpp_TypeInfo_var));
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_45 = V_0;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_46 = ___line1;
		bool L_47 = ___makeBounds4;
		NullCheck(L_45);
		VisibilityControl_Setup_mD333A77ED1FAC412903F1B213F1F532997042C76(L_45, L_46, L_47, /*hidden argument*/NULL);
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_48 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_48, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_011f;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_50 = V_3;
		NullCheck(L_50);
		BrightnessControl_SetUseLine_m66FFDE4527141BD8775D86DD699FFC308CA60217(L_50, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		goto IL_0214;
	}

IL_0124:
	{
		int32_t L_51 = ___visibility2;
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_019c;
		}
	}
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_52 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_53 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0142;
		}
	}
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_54 = V_0;
		NullCheck(L_54);
		VisibilityControl_DontDestroyLine_m59B23A6E17F5710C41641A7B12C604271F588C86(L_54, /*hidden argument*/NULL);
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_55 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_55, /*hidden argument*/NULL);
	}

IL_0142:
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_56 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_57 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0159;
		}
	}
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_58 = V_2;
		NullCheck(L_58);
		VisibilityControlAlways_DontDestroyLine_m0B8DEE28405AEF5E25655615B4EAF7F1F9F0FD32(L_58, /*hidden argument*/NULL);
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_59 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_59, /*hidden argument*/NULL);
	}

IL_0159:
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_60 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_61 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_60, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0197;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_62 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_63 = { reinterpret_cast<intptr_t> (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_65 = GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70(L_62, L_64, /*hidden argument*/NULL);
		V_1 = ((VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B *)IsInstClass((RuntimeObject*)L_65, VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B_il2cpp_TypeInfo_var));
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_66 = V_1;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_67 = ___line1;
		bool L_68 = ___makeBounds4;
		NullCheck(L_66);
		VisibilityControlStatic_Setup_m8DF1DC3A026ADB970451E19CCD65782BA960FDC0(L_66, L_67, L_68, /*hidden argument*/NULL);
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_69 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_70 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_69, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_0197;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_71 = V_3;
		NullCheck(L_71);
		BrightnessControl_SetUseLine_m66FFDE4527141BD8775D86DD699FFC308CA60217(L_71, (bool)0, /*hidden argument*/NULL);
	}

IL_0197:
	{
		goto IL_0214;
	}

IL_019c:
	{
		int32_t L_72 = ___visibility2;
		if ((!(((uint32_t)L_72) == ((uint32_t)2))))
		{
			goto IL_0214;
		}
	}
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_73 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01ba;
		}
	}
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_75 = V_0;
		NullCheck(L_75);
		VisibilityControl_DontDestroyLine_m59B23A6E17F5710C41641A7B12C604271F588C86(L_75, /*hidden argument*/NULL);
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_76 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_76, /*hidden argument*/NULL);
	}

IL_01ba:
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_77 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_78 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_01d8;
		}
	}
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_79 = V_1;
		NullCheck(L_79);
		VisibilityControlStatic_DontDestroyLine_mE4BC69DDFE1E76C36033824C37C33A51A3F707F2(L_79, /*hidden argument*/NULL);
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_80 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_80, /*hidden argument*/NULL);
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_81 = V_1;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_82 = ___line1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2(L_81, L_82, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_83 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_83, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0214;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_85 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_86 = { reinterpret_cast<intptr_t> (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_88 = GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70(L_85, L_87, /*hidden argument*/NULL);
		V_2 = ((VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E *)IsInstClass((RuntimeObject*)L_88, VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E_il2cpp_TypeInfo_var));
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_89 = V_2;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_90 = ___line1;
		NullCheck(L_89);
		VisibilityControlAlways_Setup_m5619CA19EBF41DCFB139E1DE25633C14E432E7DF(L_89, L_90, /*hidden argument*/NULL);
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_91 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_92 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_91, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0214;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_93 = V_3;
		NullCheck(L_93);
		BrightnessControl_SetUseLine_m66FFDE4527141BD8775D86DD699FFC308CA60217(L_93, (bool)0, /*hidden argument*/NULL);
	}

IL_0214:
	{
		int32_t L_94 = ___brightness3;
		if (L_94)
		{
			goto IL_027a;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_95 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_96 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_95, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_0275;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_97 = ___go0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_98 = { reinterpret_cast<intptr_t> (BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_98, /*hidden argument*/NULL);
		NullCheck(L_97);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_100 = GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70(L_97, L_99, /*hidden argument*/NULL);
		V_3 = ((BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 *)IsInstClass((RuntimeObject*)L_100, BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97_il2cpp_TypeInfo_var));
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_101 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_102 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_101, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_026d;
		}
	}
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_103 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_104 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_103, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_026d;
		}
	}
	{
		VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * L_105 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_106 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_105, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_026d;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_107 = V_3;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_108 = ___line1;
		NullCheck(L_107);
		BrightnessControl_Setup_mA8D49EAAD7EC9F9FBDB480C1A0031C2D5E523B1D(L_107, L_108, (bool)1, /*hidden argument*/NULL);
		goto IL_0275;
	}

IL_026d:
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_109 = V_3;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_110 = ___line1;
		NullCheck(L_109);
		BrightnessControl_Setup_mA8D49EAAD7EC9F9FBDB480C1A0031C2D5E523B1D(L_109, L_110, (bool)0, /*hidden argument*/NULL);
	}

IL_0275:
	{
		goto IL_028b;
	}

IL_027a:
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_111 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_112 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_111, /*hidden argument*/NULL);
		if (!L_112)
		{
			goto IL_028b;
		}
	}
	{
		BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97 * L_113 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_113, /*hidden argument*/NULL);
	}

IL_028b:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::ResetLinePoints(Vectrosity.VisibilityControlStatic,Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * ___vcs0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ResetLinePoints_m5B3A84773AF87490964250D6E74960539377CEB2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_0 = ___vcs0;
		NullCheck(L_0);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_1 = VisibilityControlStatic_GetMatrix_m393A81CE4897F455D9006439D4A387AC398606A3(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_2 = Matrix4x4_get_inverse_mBD3145C0D7977962E18C8B3BF63DD671F7917166((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		V_2 = 0;
		goto IL_0039;
	}

IL_0016:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = ___line1;
		NullCheck(L_3);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_4 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = ___line1;
		NullCheck(L_6);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_7 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_7, L_8, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&V_0), L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547(L_4, L_5, L_10, /*hidden argument*/List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547_RuntimeMethod_var);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_2;
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_13 = ___line1;
		NullCheck(L_13);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_14 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_14, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// System.Int32 Vectrosity.VectorManager::get_arrayCount()
extern "C" IL2CPP_METHOD_ATTR int32_t VectorManager_get_arrayCount_mDC24274981CD046960F76077C6D52B73059B5E86 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_get_arrayCount_mDC24274981CD046960F76077C6D52B73059B5E86_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		return L_0;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,Vectrosity.RefIntU26)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticSetup_m1FA651D0D2A47D01C99369F33B95C88DEC9BB4B1 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** ___objectNum1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityStaticSetup_m1FA651D0D2A47D01C99369F33B95C88DEC9BB4B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *)il2cpp_codegen_object_new(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_il2cpp_TypeInfo_var);
		List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9(L_1, /*hidden argument*/List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_vectorLines_6(L_1);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_2 = (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *)il2cpp_codegen_object_new(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_il2cpp_TypeInfo_var);
		List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75(L_2, /*hidden argument*/List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_objectNumbers_7(L_2);
	}

IL_001e:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = ___line0;
		NullCheck(L_3);
		VectorLine_set_drawTransform_m38C1334B48A49F7E717D92265934CDE9AD7A9F04(L_3, (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_5 = ___line0;
		NullCheck(L_4);
		List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C(L_4, L_5, /*hidden argument*/List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_6 = ___objectNum1;
		int32_t L_7 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		int32_t L_8 = L_7;
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount_8(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_9 = (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 *)il2cpp_codegen_object_new(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_il2cpp_TypeInfo_var);
		RefInt__ctor_m855FAFA703575604693F4F1504775C0A43BCBD24(L_9, L_8, /*hidden argument*/NULL);
		*((RuntimeObject **)L_6) = (RuntimeObject *)L_9;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_6, (RuntimeObject *)L_9);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_10 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_11 = ___objectNum1;
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_12 = *((RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 **)L_11);
		NullCheck(L_10);
		List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF(L_10, L_12, /*hidden argument*/List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_mEBBC8F2B41594EC8373A0D8F21E46228DAA88A5B(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityStaticRemove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticRemove_m62842272CFC8F10526322B4F60F7966532455867 (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityStaticRemove_m62842272CFC8F10526322B4F60F7966532455867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634(L_1, /*hidden argument*/List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteralB312E0EE41A9D97F429E491684D5D351E5F14FDF, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_6 = List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93(L_4, L_5, /*hidden argument*/List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3_RuntimeMethod_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_14 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA(L_14, L_15, /*hidden argument*/List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA_RuntimeMethod_var);
		int32_t L_16 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerDisable_m4BFBFA223E0EFF00E56649932B68704897873879(/*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vectrosity.VectorManager::get_arrayCount2()
extern "C" IL2CPP_METHOD_ATTR int32_t VectorManager_get_arrayCount2_mEBCE3A36B705F2968A0AEEC0088D3483D7A5010C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_get_arrayCount2_mEBCE3A36B705F2968A0AEEC0088D3483D7A5010C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		return L_0;
	}
}
// System.Void Vectrosity.VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,Vectrosity.RefIntU26)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___thisTransform0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** ___objectNum2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *)il2cpp_codegen_object_new(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_il2cpp_TypeInfo_var);
		List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9(L_1, /*hidden argument*/List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_vectorLines2_9(L_1);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_2 = (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *)il2cpp_codegen_object_new(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_il2cpp_TypeInfo_var);
		List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75(L_2, /*hidden argument*/List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_objectNumbers2_10(L_2);
	}

IL_001e:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = ___line1;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___thisTransform0;
		NullCheck(L_3);
		VectorLine_set_drawTransform_m38C1334B48A49F7E717D92265934CDE9AD7A9F04(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_5 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = ___line1;
		NullCheck(L_5);
		List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C(L_5, L_6, /*hidden argument*/List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_7 = ___objectNum2;
		int32_t L_8 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		int32_t L_9 = L_8;
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount2_11(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_10 = (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 *)il2cpp_codegen_object_new(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_il2cpp_TypeInfo_var);
		RefInt__ctor_m855FAFA703575604693F4F1504775C0A43BCBD24(L_10, L_9, /*hidden argument*/NULL);
		*((RuntimeObject **)L_7) = (RuntimeObject *)L_10;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_7, (RuntimeObject *)L_10);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_11 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_12 = ___objectNum2;
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_13 = *((RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 **)L_12);
		NullCheck(L_11);
		List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF(L_11, L_13, /*hidden argument*/List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_mEBBC8F2B41594EC8373A0D8F21E46228DAA88A5B(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityRemove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634(L_1, /*hidden argument*/List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral5E99782E75827888115C4B84F118BE713DD3CF07, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_6 = List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93(L_4, L_5, /*hidden argument*/List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3_RuntimeMethod_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_14 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA(L_14, L_15, /*hidden argument*/List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA_RuntimeMethod_var);
		int32_t L_16 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount2_11(((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerDisable_m4BFBFA223E0EFF00E56649932B68704897873879(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::CheckDistanceSetup(UnityEngine.Transform,Vectrosity.VectorLine,UnityEngine.Color,Vectrosity.RefInt)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_CheckDistanceSetup_m5BBDE4F9EEB39C177D0419D37A6BCF74BAA74BFC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___thisTransform0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color2, RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___objectNum3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_CheckDistanceSetup_m5BBDE4F9EEB39C177D0419D37A6BCF74BAA74BFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_mEBBC8F2B41594EC8373A0D8F21E46228DAA88A5B(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		if (L_0)
		{
			goto IL_0046;
		}
	}
	{
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = (List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 *)il2cpp_codegen_object_new(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980_il2cpp_TypeInfo_var);
		List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9(L_1, /*hidden argument*/List_1__ctor_m95BFAC4A3FBBB6928503E0E27483BC0A668480E9_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_vectorLines3_13(L_1);
		List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * L_2 = (List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D *)il2cpp_codegen_object_new(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D_il2cpp_TypeInfo_var);
		List_1__ctor_mAB4C08A6D18053E4C3BE30A489615645071526A8(L_2, /*hidden argument*/List_1__ctor_mAB4C08A6D18053E4C3BE30A489615645071526A8_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_transforms3_12(L_2);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_3 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)il2cpp_codegen_object_new(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var);
		List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4(L_3, /*hidden argument*/List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_oldDistances_14(L_3);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_4 = (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *)il2cpp_codegen_object_new(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_il2cpp_TypeInfo_var);
		List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626(L_4, /*hidden argument*/List_1__ctor_mA3B28700709CF84961F7F0EBF87726BB69AAB626_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_colors_15(L_4);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_5 = (List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE *)il2cpp_codegen_object_new(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE_il2cpp_TypeInfo_var);
		List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75(L_5, /*hidden argument*/List_1__ctor_m16BF6AF855FFAF80C7F08B07B4719BD36399FA75_RuntimeMethod_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_objectNumbers3_16(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_LineManagerCheckDistance_m9325E4EEB3B2D04E6C6435AB1E6B71C305DF9740(/*hidden argument*/NULL);
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * L_6 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_transforms3_12();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = ___thisTransform0;
		NullCheck(L_6);
		List_1_Add_mFE084338629BB4ECDD74DD412713A0FF01A1A08E(L_6, L_7, /*hidden argument*/List_1_Add_mFE084338629BB4ECDD74DD412713A0FF01A1A08E_RuntimeMethod_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_8 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_9 = ___line1;
		NullCheck(L_8);
		List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C(L_8, L_9, /*hidden argument*/List_1_Add_m75123FFF97336E04D6046E33878E0D803E005F1C_RuntimeMethod_var);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_10 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_oldDistances_14();
		NullCheck(L_10);
		List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771(L_10, (-1), /*hidden argument*/List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_RuntimeMethod_var);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_11 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_colors_15();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_12 = ___color2;
		NullCheck(L_11);
		List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F(L_11, L_12, /*hidden argument*/List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_13 = ___objectNum3;
		int32_t L_14 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		int32_t L_15 = L_14;
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount3_17(((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)));
		NullCheck(L_13);
		L_13->set_i_0(L_15);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_16 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_17 = ___objectNum3;
		NullCheck(L_16);
		List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF(L_16, L_17, /*hidden argument*/List_1_Add_m910FF13A2E124B8C335A08D4C22C3ECD53629BFF_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::DistanceRemove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DistanceRemove_mF080E1C53F1DAD28C7FE19F63CF6DE695CE8CD35 (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DistanceRemove_mF080E1C53F1DAD28C7FE19F63CF6DE695CE8CD35_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634(L_1, /*hidden argument*/List_1_get_Count_m604B19151B23441B8D188D239850D5E19A3D6634_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral63D2F2B40CD52CB916885FD6798EB2CA97387C35, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_6 = List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93(L_4, L_5, /*hidden argument*/List_1_get_Item_m29F6ADDBA6DF7E3C277EAE4AFDA47FB6764AFE93_RuntimeMethod_var);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_transforms3_12();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_mF372089EAC48892D8A5DB1C9721B8D816EB92F2E(L_12, L_13, /*hidden argument*/List_1_RemoveAt_mF372089EAC48892D8A5DB1C9721B8D816EB92F2E_RuntimeMethod_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_14 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3(L_14, L_15, /*hidden argument*/List_1_RemoveAt_m7DDA12FE50C0BDA8709F4B2D7C2EB6F74F917AE3_RuntimeMethod_var);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_16 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_17 = ___objectNumber0;
		NullCheck(L_16);
		List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF(L_16, L_17, /*hidden argument*/List_1_RemoveAt_m46F3350653EC1EC25D6AB6E2A93FEAA964DD90EF_RuntimeMethod_var);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_18 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_colors_15();
		int32_t L_19 = ___objectNumber0;
		NullCheck(L_18);
		List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4(L_18, L_19, /*hidden argument*/List_1_RemoveAt_m9AEF2DFF7E1C68099E13B5D05F8D285C66A208A4_RuntimeMethod_var);
		List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * L_20 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		int32_t L_21 = ___objectNumber0;
		NullCheck(L_20);
		List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA(L_20, L_21, /*hidden argument*/List_1_RemoveAt_mB210FA4A5CBC60348EF65ADA1432DA9B218BD3EA_RuntimeMethod_var);
		int32_t L_22 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set__arrayCount3_17(((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1)));
		return;
	}
}
// System.Void Vectrosity.VectorManager::CheckDistance()
extern "C" IL2CPP_METHOD_ATTR void VectorManager_CheckDistance_m174A9BB070CD50AC6AE7AF33B7D5578B0092362A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_CheckDistance_m174A9BB070CD50AC6AE7AF33B7D5578B0092362A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0011;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_SetDistanceColor_m8EC2BBE45FD2A12808FC0978168D4C85DD0D1439(L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
	}

IL_0011:
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_3 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::SetOldDistance(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_SetOldDistance_m7C0E3CD2180C2A4A1192AABC9DF4D0A54D332D59 (int32_t ___objectNumber0, int32_t ___val1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetOldDistance_m7C0E3CD2180C2A4A1192AABC9DF4D0A54D332D59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_1 = ___objectNumber0;
		int32_t L_2 = ___val1;
		NullCheck(L_0);
		List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925(L_0, L_1, L_2, /*hidden argument*/List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::SetDistanceColor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_SetDistanceColor_m8EC2BBE45FD2A12808FC0978168D4C85DD0D1439 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetDistanceColor_m8EC2BBE45FD2A12808FC0978168D4C85DD0D1439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_0, L_1, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3 = VectorLine_get_active_m1D98B4F0CA741171227832C0E72E32BD0F0B51A3(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_transforms3_12();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = List_1_get_Item_m6FBF36E821E762256C887A96718EC1B49B67EA8D(L_4, L_5, /*hidden argument*/List_1_get_Item_m6FBF36E821E762256C887A96718EC1B49B67EA8D_RuntimeMethod_var);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		float L_8 = VectorManager_GetBrightnessValue_m9AD38BBC736EBBB0BD805028EAC9FBEF852FB5C3(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = V_0;
		int32_t L_10 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_brightnessLevels_2();
		V_1 = (((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)L_9, (float)(((float)((float)L_10))))))));
		int32_t L_11 = V_1;
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_13 = ___i0;
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5(L_12, L_13, /*hidden argument*/List_1_get_Item_mDF3F52C7C1985C572A07CD15F1486A0035D288D5_RuntimeMethod_var);
		if ((((int32_t)L_11) == ((int32_t)L_14)))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_15 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_17 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_15, L_16, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_18 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_fogColor_4();
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_19 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_colors_15();
		int32_t L_20 = ___i0;
		NullCheck(L_19);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_21 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_19, L_20, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		float L_22 = V_0;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_23 = Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46(L_18, L_21, L_22, /*hidden argument*/NULL);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_24 = Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30(L_23, /*hidden argument*/NULL);
		NullCheck(L_17);
		VectorLine_SetColor_m9E40451DEC91BF6B567C658CF8612F4AF4DBBF6F(L_17, L_24, /*hidden argument*/NULL);
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_25 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_26 = ___i0;
		int32_t L_27 = V_1;
		NullCheck(L_25);
		List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925(L_25, L_26, L_27, /*hidden argument*/List_1_set_Item_m2CAC3DFE375C3E2406CEE0942DC175C93F2FF925_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLine(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_1, L_2, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m069CE6FCCFAA45724F1686EBB4BFE615DC73F999(L_3, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_4, L_5, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_6);
		VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723(L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLine2(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_1, L_2, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m069CE6FCCFAA45724F1686EBB4BFE615DC73F999(L_3, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_4 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_4, L_5, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_6);
		VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723(L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLines()
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLines_mE37BD49B9937B4DB6C8EFB1D62E77A9789D0C58F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLines_mE37BD49B9937B4DB6C8EFB1D62E77A9789D0C58F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 0;
		goto IL_0025;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_1, L_2, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m069CE6FCCFAA45724F1686EBB4BFE615DC73F999(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_6 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_005b;
	}

IL_0035:
	{
		V_1 = 0;
		goto IL_0050;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_7 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_9 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_7, L_8, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_9);
		VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723(L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_003c;
		}
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLines2()
extern "C" IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLines2_m79729DDA6145FAF9A5EB2A9BCA9A5AF48A5FE5FE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLines2_m79729DDA6145FAF9A5EB2A9BCA9A5AF48A5FE5FE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 0;
		goto IL_0025;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_1 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_1, L_2, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m069CE6FCCFAA45724F1686EBB4BFE615DC73F999(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_6 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_005b;
	}

IL_0035:
	{
		V_1 = 0;
		goto IL_0050;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * L_7 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_9 = List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1(L_7, L_8, /*hidden argument*/List_1_get_Item_m7D21D0B39EC28851325284D98FAEECB012F3CAE1_RuntimeMethod_var);
		NullCheck(L_9);
		VectorLine_Draw_m9E3A8B2CAFED577A6108FCAD5675F5142A6D6723(L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		int32_t L_12 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_003c;
		}
	}

IL_005b:
	{
		return;
	}
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  VectorManager_GetBounds_m81458FB6B7B1F622EF00C2DF03E83416BA7AA4D7 (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBounds_m81458FB6B7B1F622EF00C2DF03E83416BA7AA4D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_0 = ___line0;
		NullCheck(L_0);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral50FEA8B87017C4C0C3E95EDBFF1E6EA64C8180CE, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 ));
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_2 = V_0;
		return L_2;
	}

IL_001f:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_3 = ___line0;
		NullCheck(L_3);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_4 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_5 = VectorManager_GetBounds_mE824FB050B62C410AFCEB570F5351824839335C4(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  VectorManager_GetBounds_mE824FB050B62C410AFCEB570F5351824839335C4 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points30, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBounds_mE824FB050B62C410AFCEB570F5351824839335C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_16;
	memset(&V_16, 0, sizeof(V_16));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 ));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___points30;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_0, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		V_3 = L_1;
		V_4 = 0;
		goto IL_0196;
	}

IL_0043:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = ___points30;
		int32_t L_3 = V_4;
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_2, L_3, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_5 = L_4;
		float L_5 = (&V_5)->get_x_2();
		float L_6 = (&V_1)->get_x_2();
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_007d;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_7 = ___points30;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_7, L_8, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_6 = L_9;
		float L_10 = (&V_6)->get_x_2();
		(&V_1)->set_x_2(L_10);
		goto IL_00b2;
	}

IL_007d:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_11 = ___points30;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_11, L_12, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_7 = L_13;
		float L_14 = (&V_7)->get_x_2();
		float L_15 = (&V_2)->get_x_2();
		if ((!(((float)L_14) > ((float)L_15))))
		{
			goto IL_00b2;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_16 = ___points30;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_16, L_17, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_8 = L_18;
		float L_19 = (&V_8)->get_x_2();
		(&V_2)->set_x_2(L_19);
	}

IL_00b2:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_20 = ___points30;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_20, L_21, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_9 = L_22;
		float L_23 = (&V_9)->get_y_3();
		float L_24 = (&V_1)->get_y_3();
		if ((!(((float)L_23) < ((float)L_24))))
		{
			goto IL_00ec;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_25 = ___points30;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_25, L_26, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_10 = L_27;
		float L_28 = (&V_10)->get_y_3();
		(&V_1)->set_y_3(L_28);
		goto IL_0121;
	}

IL_00ec:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_29 = ___points30;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_29, L_30, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_11 = L_31;
		float L_32 = (&V_11)->get_y_3();
		float L_33 = (&V_2)->get_y_3();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0121;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_34 = ___points30;
		int32_t L_35 = V_4;
		NullCheck(L_34);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_34, L_35, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_12 = L_36;
		float L_37 = (&V_12)->get_y_3();
		(&V_2)->set_y_3(L_37);
	}

IL_0121:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_38 = ___points30;
		int32_t L_39 = V_4;
		NullCheck(L_38);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_38, L_39, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_13 = L_40;
		float L_41 = (&V_13)->get_z_4();
		float L_42 = (&V_1)->get_z_4();
		if ((!(((float)L_41) < ((float)L_42))))
		{
			goto IL_015b;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_43 = ___points30;
		int32_t L_44 = V_4;
		NullCheck(L_43);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_43, L_44, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_14 = L_45;
		float L_46 = (&V_14)->get_z_4();
		(&V_1)->set_z_4(L_46);
		goto IL_0190;
	}

IL_015b:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_47 = ___points30;
		int32_t L_48 = V_4;
		NullCheck(L_47);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_47, L_48, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_15 = L_49;
		float L_50 = (&V_15)->get_z_4();
		float L_51 = (&V_2)->get_z_4();
		if ((!(((float)L_50) > ((float)L_51))))
		{
			goto IL_0190;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_52 = ___points30;
		int32_t L_53 = V_4;
		NullCheck(L_52);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_52, L_53, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_16 = L_54;
		float L_55 = (&V_16)->get_z_4();
		(&V_2)->set_z_4(L_55);
	}

IL_0190:
	{
		int32_t L_56 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0196:
	{
		int32_t L_57 = V_4;
		int32_t L_58 = V_3;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0043;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_59 = V_1;
		Bounds_set_min_m5933955F04FCC8E3B372EA72ECCD398BB057C844((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), L_59, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = V_2;
		Bounds_set_max_m12B864B082A4A188C7624C1ABEFA34028DD5A603((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), L_60, /*hidden argument*/NULL);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_61 = V_0;
		return L_61;
	}
}
// UnityEngine.Mesh Vectrosity.VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * VectorManager_MakeBoundsMesh_m050BEFC2B6263AF23741435C1F7C579298366E1C (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_MakeBoundsMesh_m050BEFC2B6263AF23741435C1F7C579298366E1C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * V_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_24;
	memset(&V_24, 0, sizeof(V_24));
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *)il2cpp_codegen_object_new(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var);
		Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = V_0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_2 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)8);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_3 = L_2;
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = (&V_3)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_11), ((-L_6)), L_8, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_4, L_11, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_12;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_13 = L_3;
		NullCheck(L_13);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = (&V_4)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = (&V_5)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = (&V_6)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_21), L_16, L_18, L_20, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_14, L_21, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_22;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_23 = L_13;
		NullCheck(L_23);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_7 = L_25;
		float L_26 = (&V_7)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_8 = L_27;
		float L_28 = (&V_8)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_9 = L_29;
		float L_30 = (&V_9)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_31), ((-L_26)), L_28, ((-L_30)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_24, L_31, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_32;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_33 = L_23;
		NullCheck(L_33);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_10 = L_35;
		float L_36 = (&V_10)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_11 = L_37;
		float L_38 = (&V_11)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_12 = L_39;
		float L_40 = (&V_12)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_41), L_36, L_38, ((-L_40)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_34, L_41, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_42;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_43 = L_33;
		NullCheck(L_43);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_13 = L_45;
		float L_46 = (&V_13)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_14 = L_47;
		float L_48 = (&V_14)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_15 = L_49;
		float L_50 = (&V_15)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_51), ((-L_46)), ((-L_48)), L_50, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_44, L_51, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(4))) = L_52;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_53 = L_43;
		NullCheck(L_53);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_16 = L_55;
		float L_56 = (&V_16)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_57 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_17 = L_57;
		float L_58 = (&V_17)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_59 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_18 = L_59;
		float L_60 = (&V_18)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_61), L_56, ((-L_58)), L_60, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_62 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_54, L_61, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_53)->GetAddressAt(static_cast<il2cpp_array_size_t>(5))) = L_62;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_63 = L_53;
		NullCheck(L_63);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_64 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_65 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_19 = L_65;
		float L_66 = (&V_19)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_67 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_20 = L_67;
		float L_68 = (&V_20)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_69 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_21 = L_69;
		float L_70 = (&V_21)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_71), ((-L_66)), ((-L_68)), ((-L_70)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_72 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_64, L_71, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(6))) = L_72;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_73 = L_63;
		NullCheck(L_73);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_74 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_75 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_22 = L_75;
		float L_76 = (&V_22)->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_77 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_23 = L_77;
		float L_78 = (&V_23)->get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_79 = Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds0), /*hidden argument*/NULL);
		V_24 = L_79;
		float L_80 = (&V_24)->get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_81), L_76, ((-L_78)), ((-L_80)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_82 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_74, L_81, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(7))) = L_82;
		NullCheck(L_1);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_1, L_73, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_83 = V_0;
		return L_83;
	}
}
// System.Void Vectrosity.VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * V_0 = NULL;
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * V_1 = NULL;
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___go0;
		NullCheck(L_0);
		MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * L_1 = GameObject_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mD1BA4FFEB800AB3D102141CD0A0ECE237EA70FB4(L_0, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mD1BA4FFEB800AB3D102141CD0A0ECE237EA70FB4_RuntimeMethod_var);
		V_0 = L_1;
		MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ___go0;
		NullCheck(L_4);
		MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * L_5 = GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4(L_4, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4_RuntimeMethod_var);
		V_0 = L_5;
	}

IL_001a:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = ___go0;
		NullCheck(L_6);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_7 = GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mFB43D5458906C4005145640D4396FDE5853AFA3A(L_6, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mFB43D5458906C4005145640D4396FDE5853AFA3A_RuntimeMethod_var);
		V_1 = L_7;
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_8, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = ___go0;
		NullCheck(L_10);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_11 = GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B(L_10, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B_RuntimeMethod_var);
		V_1 = L_11;
	}

IL_0034:
	{
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_12 = V_1;
		NullCheck(L_12);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_12, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_13 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_meshTable_18();
		if (L_13)
		{
			goto IL_004f;
		}
	}
	{
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_14 = (Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 *)il2cpp_codegen_object_new(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8EA95790B7EF7A3D250BF84526344E6A5FCCA549(L_14, /*hidden argument*/Dictionary_2__ctor_m8EA95790B7EF7A3D250BF84526344E6A5FCCA549_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->set_meshTable_18(L_14);
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_15 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_16 = ___line1;
		NullCheck(L_16);
		String_t* L_17 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		bool L_18 = Dictionary_2_ContainsKey_m299D4D802C5BC1E197E7B4B41CBBEE85EE9EE0C0(L_15, L_17, /*hidden argument*/Dictionary_2_ContainsKey_m299D4D802C5BC1E197E7B4B41CBBEE85EE9EE0C0_RuntimeMethod_var);
		if (L_18)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_19 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_20 = ___line1;
		NullCheck(L_20);
		String_t* L_21 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_20, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_22 = ___line1;
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_23 = VectorManager_GetBounds_m81458FB6B7B1F622EF00C2DF03E83416BA7AA4D7(L_22, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_24 = VectorManager_MakeBoundsMesh_m050BEFC2B6263AF23741435C1F7C579298366E1C(L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		Dictionary_2_Add_m2010B8DB8B89AF9E3AB5184F256BB89CC201EF1C(L_19, L_21, L_24, /*hidden argument*/Dictionary_2_Add_m2010B8DB8B89AF9E3AB5184F256BB89CC201EF1C_RuntimeMethod_var);
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_25 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_26 = ___line1;
		NullCheck(L_26);
		String_t* L_27 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_28 = Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA(L_25, L_27, /*hidden argument*/Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA_RuntimeMethod_var);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_29 = ___line1;
		NullCheck(L_29);
		String_t* L_30 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_29, /*hidden argument*/NULL);
		String_t* L_31 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_30, _stringLiteralEE80E905BA9EAAC3B0371212824CA6B7C66EB9DD, /*hidden argument*/NULL);
		NullCheck(L_28);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_28, L_31, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * L_33 = ((VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_34 = ___line1;
		NullCheck(L_34);
		String_t* L_35 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_36 = Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA(L_33, L_35, /*hidden argument*/Dictionary_2_get_Item_m9289727887D4594D6606B98CB8FE7A238542A4CA_RuntimeMethod_var);
		NullCheck(L_32);
		MeshFilter_set_mesh_mA18AA96C75CC91CF0917BA1F437626499FAAF496(L_32, L_36, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorObject2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D__ctor_mEA26F0F7D71A3447EDF722BF4D0D44F7477A8FB5 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_32((bool)1);
		__this->set_m_updateUVs_33((bool)1);
		__this->set_m_updateColors_34((bool)1);
		__this->set_m_updateNormals_35((bool)0);
		__this->set_m_updateTangents_36((bool)0);
		__this->set_m_updateTris_37((bool)1);
		RawImage__ctor_m9C69814EC2E8E295A3CDEF79789EF5A4C670CF72(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::.cctor()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D__cctor_m8B463B1188B0C71FC838DB81D829AFE4BA354C4A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D__cctor_m8B463B1188B0C71FC838DB81D829AFE4BA354C4A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields*)il2cpp_codegen_static_fields_for(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_il2cpp_TypeInfo_var))->set_vertexHelper_40((VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F *)NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetVectorLine_m114F5C683063C126B2470D09B07AE794340AE662 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___vectorLine0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat2, bool ___useCustomMaterial3, const RuntimeMethod* method)
{
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_0 = ___vectorLine0;
		__this->set_vectorLine_39(L_0);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_1 = ___tex1;
		VectorObject2D_SetTexture_mCF3DBC916A9E526F82662176D592FDCCD13FDECE(__this, L_1, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = ___mat2;
		VectorObject2D_SetMaterial_m417027141DC94371E45296532DB56E09ABA910BB(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::Destroy()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_Destroy_mE4F502240BE03574D081D7C9C6F63EAE90E2C871 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_Destroy_mE4F502240BE03574D081D7C9C6F63EAE90E2C871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::DestroyNow()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_DestroyNow_m04978A2F39767BB28D87D023F54DB59D94EA78C9 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_DestroyNow_m04978A2F39767BB28D87D023F54DB59D94EA78C9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::Enable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_Enable_mBA5E8E1A00A6D5402E119E40DAA313DB0611EEA9 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, bool ___enable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_Enable_mBA5E8E1A00A6D5402E119E40DAA313DB0611EEA9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_0 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(__this, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_1 = ___enable0;
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetTexture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetTexture_mCF3DBC916A9E526F82662176D592FDCCD13FDECE (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex0, const RuntimeMethod* method)
{
	{
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_0 = ___tex0;
		RawImage_set_texture_m897BC65663AFF15258A611CC6E3480B078F41D23(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetMaterial_m417027141DC94371E45296532DB56E09ABA910BB (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat0, const RuntimeMethod* method)
{
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = ___mat0;
		VirtActionInvoker1< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, __this, L_0);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateGeometry()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateGeometry_mD3E3AE525460790DA8EBDA46A1EDC21F9ADA7061 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_UpdateGeometry_mD3E3AE525460790DA8EBDA46A1EDC21F9ADA7061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		VectorObject2D_SetupMesh_m65B0C77C52B6ABCDF96C84107E658188DEDDD310(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_2 = Graphic_get_rectTransform_m127FCBF38F4F0D4B264D892013A3AD9A507936DE(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_006d;
		}
	}
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_4 = Graphic_get_rectTransform_m127FCBF38F4F0D4B264D892013A3AD9A507936DE(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_5 = RectTransform_get_rect_mE5F283FCB99A66403AC1F0607CA49C156D73A15E(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_6) >= ((float)(0.0f)))))
		{
			goto IL_006d;
		}
	}
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_7 = Graphic_get_rectTransform_m127FCBF38F4F0D4B264D892013A3AD9A507936DE(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_8 = RectTransform_get_rect_mE5F283FCB99A66403AC1F0607CA49C156D73A15E(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(0.0f)))))
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_il2cpp_TypeInfo_var);
		VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * L_10 = ((VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields*)il2cpp_codegen_static_fields_for(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_il2cpp_TypeInfo_var))->get_vertexHelper_40();
		VirtActionInvoker1< VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * >::Invoke(44 /* System.Void UnityEngine.UI.Graphic::OnPopulateMesh(UnityEngine.UI.VertexHelper) */, __this, L_10);
	}

IL_006d:
	{
		CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * L_11 = Graphic_get_canvasRenderer_m3CCAFCDBCEB38A281BAAB89F2832171BB3D348A6(__this, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_12 = __this->get_m_mesh_38();
		NullCheck(L_11);
		CanvasRenderer_SetMesh_mC87C841A52339C33E5B1C644C70FC9CC9C560988(L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetupMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetupMesh_m65B0C77C52B6ABCDF96C84107E658188DEDDD310 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetupMesh_m65B0C77C52B6ABCDF96C84107E658188DEDDD310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *)il2cpp_codegen_object_new(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var);
		Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4(L_0, /*hidden argument*/NULL);
		__this->set_m_mesh_38(L_0);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = __this->get_vectorLine_39();
		NullCheck(L_2);
		String_t* L_3 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_1, L_3, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_4 = __this->get_m_mesh_38();
		NullCheck(L_4);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_4, ((int32_t)61), /*hidden argument*/NULL);
		VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetMeshBounds()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_38();
		int32_t L_3 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_5), (((float)((float)((int32_t)((int32_t)L_3/(int32_t)2))))), (((float)((float)((int32_t)((int32_t)L_4/(int32_t)2))))), (0.0f), /*hidden argument*/NULL);
		int32_t L_6 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_8), (((float)((float)L_6))), (((float)((float)L_7))), (0.0f), /*hidden argument*/NULL);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D((&L_9), L_5, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6(L_2, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_OnPopulateMesh_mEE597AD595E63AC648FE57B77EBF2E270F03FE06 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___vh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_OnPopulateMesh_mEE597AD595E63AC648FE57B77EBF2E270F03FE06_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_updateVerts_32();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = __this->get_vectorLine_39();
		NullCheck(L_2);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_3 = VectorLine_get_lineVertices_m57239935B0FD597F75E15E240895FC9BCD0B2205(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_1, L_3, /*hidden argument*/NULL);
		__this->set_m_updateVerts_32((bool)0);
	}

IL_0028:
	{
		bool L_4 = __this->get_m_updateUVs_33();
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_5 = __this->get_vectorLine_39();
		NullCheck(L_5);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_6 = VectorLine_get_lineUVs_mA513323E5C67007AC265EB355157DB299CEE7451(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_7 = __this->get_m_mesh_38();
		NullCheck(L_7);
		int32_t L_8 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))) == ((uint32_t)L_8))))
		{
			goto IL_0066;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_9 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_10 = __this->get_vectorLine_39();
		NullCheck(L_10);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_11 = VectorLine_get_lineUVs_mA513323E5C67007AC265EB355157DB299CEE7451(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0066:
	{
		__this->set_m_updateUVs_33((bool)0);
	}

IL_006d:
	{
		bool L_12 = __this->get_m_updateColors_34();
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_13 = __this->get_vectorLine_39();
		NullCheck(L_13);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_14 = VectorLine_get_lineColors_mAD82DDEAC5313ED04BAB4C61A251790804FB0917(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_15 = __this->get_m_mesh_38();
		NullCheck(L_15);
		int32_t L_16 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length))))) == ((uint32_t)L_16))))
		{
			goto IL_00ab;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_17 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_18 = __this->get_vectorLine_39();
		NullCheck(L_18);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_19 = VectorLine_get_lineColors_mAD82DDEAC5313ED04BAB4C61A251790804FB0917(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Mesh_set_colors32_m29459AFE2843D67E95D60E5A13B6F6AF7670230C(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		__this->set_m_updateColors_34((bool)0);
	}

IL_00b2:
	{
		bool L_20 = __this->get_m_updateTris_37();
		if (!L_20)
		{
			goto IL_00e1;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_21 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_22 = __this->get_vectorLine_39();
		NullCheck(L_22);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_23 = VectorLine_get_lineTriangles_mAF5521DCE9BBAF2E4833555194F10E1610A602B5(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Mesh_SetTriangles_m6A43D705DE751C622CCF88EC31C4EF1B53578BE5(L_21, L_23, 0, /*hidden argument*/NULL);
		__this->set_m_updateTris_37((bool)0);
		VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0(__this, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		bool L_24 = __this->get_m_updateNormals_35();
		if (!L_24)
		{
			goto IL_0115;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_25 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_25, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0115;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_27 = __this->get_m_mesh_38();
		NullCheck(L_27);
		Mesh_RecalculateNormals_m9F5DF412F81F250419D9887C76F549B692B7D027(L_27, /*hidden argument*/NULL);
		__this->set_m_updateNormals_35((bool)0);
		VirtActionInvoker0::Invoke(41 /* System.Void UnityEngine.UI.Graphic::UpdateGeometry() */, __this);
	}

IL_0115:
	{
		bool L_28 = __this->get_m_updateTangents_36();
		if (!L_28)
		{
			goto IL_0159;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_29 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_29, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0159;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_31 = __this->get_m_mesh_38();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_32 = __this->get_vectorLine_39();
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_33 = __this->get_m_mesh_38();
		NullCheck(L_33);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_34 = Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* L_35 = VectorLine_CalculateTangents_m52E4D000B3E7EE01C0BF57AF951247B65492C5AD(L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57(L_31, L_35, /*hidden argument*/NULL);
		__this->set_m_updateTangents_36((bool)0);
	}

IL_0159:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetName(System.String)
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_SetName_m457760C14940A4B030043B8D05EA4AF9D5FC1050 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetName_m457760C14940A4B030043B8D05EA4AF9D5FC1050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_38();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateVerts()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateVerts_m31500CDB5C0CBE6C290095B9905C44EAE31CF403 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_32((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateUVs()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateUVs_m28CEA8CCC8CB16481C65815B1A276CF78393104D (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateUVs_33((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateColors()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateColors_m2CD11F33F0A19E11361C4E4C9D6EA76BA5D196BF (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateColors_34((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateNormals()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateNormals_mC720773AA56DDE290F66FBCB50B648F90E6B3FB5 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateNormals_35((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateTangents()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateTangents_m31A29FD8E24B07DA86785420ACB301762C7EDF8E (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTangents_36((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateTris()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateTris_m4B16FDE2A786B4BF2A9F04EC80E6C4C02563EC26 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTris_37((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateMeshAttributes()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_UpdateMeshAttributes_m48C0D2AF88A3E054FE2EDF81A285A67CBC8092A9 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_UpdateMeshAttributes_m48C0D2AF88A3E054FE2EDF81A285A67CBC8092A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_38();
		NullCheck(L_2);
		Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		__this->set_m_updateVerts_32((bool)1);
		__this->set_m_updateUVs_33((bool)1);
		__this->set_m_updateColors_34((bool)1);
		__this->set_m_updateTris_37((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VectorObject2D_SetMeshBounds_mE945A4D3976B50A1FD3F38EB6D40D26B9E51DED0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::ClearMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject2D_ClearMesh_mBAE450E7E0F5E7F8637A7FBC87B8532DA97CC720 (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_ClearMesh_mBAE450E7E0F5E7F8637A7FBC87B8532DA97CC720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_38();
		NullCheck(L_2);
		Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60(L_2, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(41 /* System.Void UnityEngine.UI.Graphic::UpdateGeometry() */, __this);
		return;
	}
}
// System.Int32 Vectrosity.VectorObject2D::VertexCount()
extern "C" IL2CPP_METHOD_ATTR int32_t VectorObject2D_VertexCount_m285EB1269917EE0A77904E6BBA4F773CD022CA8E (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650 * __this, const RuntimeMethod* method)
{
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_38();
		NullCheck(L_0);
		int32_t L_1 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorObject3D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D__ctor_mEA3FD52D48E93F131B8A4FBDDB8CB623B15D0C71 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_4((bool)1);
		__this->set_m_updateUVs_5((bool)1);
		__this->set_m_updateColors_6((bool)1);
		__this->set_m_updateNormals_7((bool)0);
		__this->set_m_updateTangents_8((bool)0);
		__this->set_m_updateTris_9((bool)1);
		__this->set_m_useCustomMaterial_13((bool)0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetVectorLine_mE235B13331769EF2B6BB656E404951FD4593764D (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___vectorLine0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat2, bool ___useCustomMaterial3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetVectorLine_mE235B13331769EF2B6BB656E404951FD4593764D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B(L_0, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_mEA2B6A451EE2482AC1B5CF86FECA86AFB92F168B_RuntimeMethod_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4(L_1, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_m98AEA1EDDC59492203D06FA2912152C37C4164E4_RuntimeMethod_var);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = ___vectorLine0;
		__this->set_m_vectorLine_11(L_2);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = ___mat2;
		__this->set_m_material_12(L_3);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_4 = __this->get_m_material_12();
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_5 = ___tex1;
		NullCheck(L_4);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_4, L_5, /*hidden argument*/NULL);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_6 = Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_7 = __this->get_m_material_12();
		NullCheck(L_6);
		Renderer_set_sharedMaterial_mC94A354D9B0FCA081754A7CB51AEE5A9AD3946A3(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = ___useCustomMaterial3;
		__this->set_m_useCustomMaterial_13(L_8);
		VectorObject3D_SetupMesh_m527507F5C9C7A8B91385F6F8D8F883BC41C1295C(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::Destroy()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_Destroy_m8A31175145711DDCDBA6E5C853D5BE441077FAF6 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_Destroy_m8A31175145711DDCDBA6E5C853D5BE441077FAF6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_m_useCustomMaterial_13();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = __this->get_m_material_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::Enable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_Enable_m6931189D0D171B44CFBFFE9DF4871019FEF650BF (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, bool ___enable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_Enable_m6931189D0D171B44CFBFFE9DF4871019FEF650BF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_0 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(__this, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_1 = Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var);
		bool L_2 = ___enable0;
		NullCheck(L_1);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetTexture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetTexture_mE863674CF2DEC0B387DFF906BE3B477AAB73B5AA (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetTexture_mE863674CF2DEC0B387DFF906BE3B477AAB73B5AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_0 = Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var);
		NullCheck(L_0);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_1 = Renderer_get_sharedMaterial_m2BE9FF3D269968F2E323AC60EFBBCC0B26E7E6F9(L_0, /*hidden argument*/NULL);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_2 = ___tex0;
		NullCheck(L_1);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetMaterial_m6DDAC1E086DE38C332BA1BD402695A1D6B66E62A (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetMaterial_m6DDAC1E086DE38C332BA1BD402695A1D6B66E62A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = ___mat0;
		__this->set_m_material_12(L_0);
		__this->set_m_useCustomMaterial_13((bool)1);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_1 = Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = ___mat0;
		NullCheck(L_1);
		Renderer_set_sharedMaterial_mC94A354D9B0FCA081754A7CB51AEE5A9AD3946A3(L_1, L_2, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = ___mat0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_5 = Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m8196A340B7AB343328492ADCD6AAFFEA179580EA_RuntimeMethod_var);
		NullCheck(L_5);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = Renderer_get_sharedMaterial_m2BE9FF3D269968F2E323AC60EFBBCC0B26E7E6F9(L_5, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_7 = __this->get_m_vectorLine_11();
		NullCheck(L_7);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_8 = VectorLine_get_texture_mAD80AB37FC67D382596F6BF8F5312B56B41F95D7(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_6, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetupMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetupMesh_m527507F5C9C7A8B91385F6F8D8F883BC41C1295C (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetupMesh_m527507F5C9C7A8B91385F6F8D8F883BC41C1295C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *)il2cpp_codegen_object_new(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var);
		Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4(L_0, /*hidden argument*/NULL);
		__this->set_m_mesh_10(L_0);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = __this->get_m_vectorLine_11();
		NullCheck(L_2);
		String_t* L_3 = VectorLine_get_name_mDB8C0609A812F7073381C46690A3D060DC09B5AD(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_1, L_3, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_4 = __this->get_m_mesh_10();
		NullCheck(L_4);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_4, ((int32_t)61), /*hidden argument*/NULL);
		MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * L_5 = Component_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mF3F89565A9CEFF85AA1FB27C6EC64BE590DC386B(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0_mF3F89565A9CEFF85AA1FB27C6EC64BE590DC386B_RuntimeMethod_var);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_6 = __this->get_m_mesh_10();
		NullCheck(L_5);
		MeshFilter_set_mesh_mA18AA96C75CC91CF0917BA1F437626499FAAF496(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::LateUpdate()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_LateUpdate_m4F609E20102927F1EC10608685BAE2D8C8ED813D (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_updateVerts_4();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VectorObject3D_SetVerts_mDE110B09DA7E3DC546E6B86A63549870053D1BEB(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_m_updateUVs_5();
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = __this->get_m_vectorLine_11();
		NullCheck(L_2);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_3 = VectorLine_get_lineUVs_mA513323E5C67007AC265EB355157DB299CEE7451(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_4 = __this->get_m_mesh_10();
		NullCheck(L_4);
		int32_t L_5 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((uint32_t)L_5))))
		{
			goto IL_004f;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_6 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_7 = __this->get_m_vectorLine_11();
		NullCheck(L_7);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_8 = VectorLine_get_lineUVs_mA513323E5C67007AC265EB355157DB299CEE7451(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004f:
	{
		__this->set_m_updateUVs_5((bool)0);
	}

IL_0056:
	{
		bool L_9 = __this->get_m_updateColors_6();
		if (!L_9)
		{
			goto IL_009b;
		}
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_10 = __this->get_m_vectorLine_11();
		NullCheck(L_10);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_11 = VectorLine_get_lineColors_mAD82DDEAC5313ED04BAB4C61A251790804FB0917(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_12 = __this->get_m_mesh_10();
		NullCheck(L_12);
		int32_t L_13 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))) == ((uint32_t)L_13))))
		{
			goto IL_0094;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_14 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_15 = __this->get_m_vectorLine_11();
		NullCheck(L_15);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_16 = VectorLine_get_lineColors_mAD82DDEAC5313ED04BAB4C61A251790804FB0917(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Mesh_set_colors32_m29459AFE2843D67E95D60E5A13B6F6AF7670230C(L_14, L_16, /*hidden argument*/NULL);
	}

IL_0094:
	{
		__this->set_m_updateColors_6((bool)0);
	}

IL_009b:
	{
		bool L_17 = __this->get_m_updateTris_9();
		if (!L_17)
		{
			goto IL_00c4;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_18 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_19 = __this->get_m_vectorLine_11();
		NullCheck(L_19);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_20 = VectorLine_get_lineTriangles_mAF5521DCE9BBAF2E4833555194F10E1610A602B5(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Mesh_SetTriangles_m6A43D705DE751C622CCF88EC31C4EF1B53578BE5(L_18, L_20, 0, /*hidden argument*/NULL);
		__this->set_m_updateTris_9((bool)0);
	}

IL_00c4:
	{
		bool L_21 = __this->get_m_updateNormals_7();
		if (!L_21)
		{
			goto IL_00e1;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_22 = __this->get_m_mesh_10();
		NullCheck(L_22);
		Mesh_RecalculateNormals_m9F5DF412F81F250419D9887C76F549B692B7D027(L_22, /*hidden argument*/NULL);
		__this->set_m_updateNormals_7((bool)0);
	}

IL_00e1:
	{
		bool L_23 = __this->get_m_updateTangents_8();
		if (!L_23)
		{
			goto IL_0114;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_24 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_25 = __this->get_m_vectorLine_11();
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_26 = __this->get_m_mesh_10();
		NullCheck(L_26);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_27 = Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* L_28 = VectorLine_CalculateTangents_m52E4D000B3E7EE01C0BF57AF951247B65492C5AD(L_25, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57(L_24, L_28, /*hidden argument*/NULL);
		__this->set_m_updateTangents_8((bool)0);
	}

IL_0114:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetVerts()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetVerts_mDE110B09DA7E3DC546E6B86A63549870053D1BEB (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_1 = __this->get_m_vectorLine_11();
		NullCheck(L_1);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_2 = VectorLine_get_lineVertices_m57239935B0FD597F75E15E240895FC9BCD0B2205(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_0, L_2, /*hidden argument*/NULL);
		__this->set_m_updateVerts_4((bool)0);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_3 = __this->get_m_mesh_10();
		NullCheck(L_3);
		Mesh_RecalculateBounds_m1BF701FE2CEA4E8E1183FF878B812808ED1EBA49(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetName(System.String)
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_SetName_mD2F94D73915D5DA6D18C5F86F2C3B4A7BBD80178 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetName_mD2F94D73915D5DA6D18C5F86F2C3B4A7BBD80178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_10();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateVerts()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateVerts_m62EA25A2920FCFDD5F21CD7B9FCF86D10EDB71F5 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_4((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateUVs()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateUVs_mE62FDF5DFD6EDB5CF0AEADAAF1EB9E4FC4C0A3B0 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateUVs_5((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateColors()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateColors_m48174CF6823BACB2B42EB44F7E53FA712E3372FF (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateColors_6((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateNormals()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateNormals_m1D15DEF64F129050D40A232688A599C75AE8204F (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateNormals_7((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateTangents()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateTangents_m6295CB3E4929132B9FB0D57EDAC2B84BAA19958D (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTangents_8((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateTris()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateTris_m8A233408352908789F6E879284D83AE80D7718C5 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTris_9((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateMeshAttributes()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_UpdateMeshAttributes_mDE8594C2A067357F077D3ABFFC27462100EBF274 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		NullCheck(L_0);
		Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60(L_0, /*hidden argument*/NULL);
		__this->set_m_updateVerts_4((bool)1);
		__this->set_m_updateUVs_5((bool)1);
		__this->set_m_updateColors_6((bool)1);
		__this->set_m_updateTris_9((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::ClearMesh()
extern "C" IL2CPP_METHOD_ATTR void VectorObject3D_ClearMesh_m76390F9DE3A878CC447E02E207BB04A7655D4F82 (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_ClearMesh_m76390F9DE3A878CC447E02E207BB04A7655D4F82_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = __this->get_m_mesh_10();
		NullCheck(L_2);
		Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vectrosity.VectorObject3D::VertexCount()
extern "C" IL2CPP_METHOD_ATTR int32_t VectorObject3D_VertexCount_m36FDDA7B752558E2F0AD4F3C60A4661587A92CEE (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE * __this, const RuntimeMethod* method)
{
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = __this->get_m_mesh_10();
		NullCheck(L_0);
		int32_t L_1 = Mesh_get_vertexCount_mE6F1153EA724F831AD11F10807ABE664CC02E0AF(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl__ctor_m39EB5EBF3FCA6D57C92CA642B6B54A5EF7FCCE62 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControl::get_objectNumber()
extern "C" IL2CPP_METHOD_ATTR RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * VisibilityControl_get_objectNumber_m861A6C5F6166D86075B377DE990B085373F9B4DC (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	{
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl_Setup_mD333A77ED1FAC412903F1B213F1F532997042C76 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, bool ___makeBounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_Setup_mD333A77ED1FAC412903F1B213F1F532997042C76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___makeBounds1;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = ___line0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_4 = ___line0;
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_5 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED(L_3, L_4, (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 **)L_5, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = ___line0;
		__this->set_m_vectorLine_5(L_6);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_7 = __this->get_m_objectNumber_4();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19(L_8, /*hidden argument*/NULL);
		RuntimeObject* L_9 = VisibilityControl_VisibilityTest_m754BD51A9F8FEDCCAED123D8EEA0E2400267EB2F(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::VisibilityTest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_VisibilityTest_m754BD51A9F8FEDCCAED123D8EEA0E2400267EB2F (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_VisibilityTest_m754BD51A9F8FEDCCAED123D8EEA0E2400267EB2F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * V_0 = NULL;
	{
		U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * L_0 = (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 *)il2cpp_codegen_object_new(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48_il2cpp_TypeInfo_var);
		U3CVisibilityTestU3Ec__Iterator1__ctor_mEB2B89198D42EC091034E26A99FDCBEF61CE96E6(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameVisible()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_OnBecameVisible_m69CBE26331CECCD0F2C503ECBE4FD8FE38D591AF (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnBecameVisible_m69CBE26331CECCD0F2C503ECBE4FD8FE38D591AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * V_0 = NULL;
	{
		U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * L_0 = (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 *)il2cpp_codegen_object_new(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403_il2cpp_TypeInfo_var);
		U3COnBecameVisibleU3Ec__Iterator2__ctor_m3D0322E423D96DFFCFBF87D8A93AD700F80A65CA(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameInvisible()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_OnBecameInvisible_mA07DF85339DABD246ACF5A68AE089BFD99ADE1E7 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnBecameInvisible_mA07DF85339DABD246ACF5A68AE089BFD99ADE1E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * V_0 = NULL;
	{
		U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * L_0 = (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 *)il2cpp_codegen_object_new(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_il2cpp_TypeInfo_var);
		U3COnBecameInvisibleU3Ec__Iterator3__ctor_mD52E94CE8B2DA54CD7488B1255F4648CCA427DF1(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Vectrosity.VisibilityControl::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl_OnDestroy_m194DC050E90D34281385066A59F37A55923BD3F9 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnDestroy_m194DC050E90D34281385066A59F37A55923BD3F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mDE8EFC54CC11D3D871B74A67DCE1A0A3BC2FC3D8((VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControl::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControl_DontDestroyLine_m59B23A6E17F5710C41641A7B12C604271F588C86 (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3__ctor_mD52E94CE8B2DA54CD7488B1255F4648CCA427DF1 (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m24A2DF8AC40EDE3C910D46EF2332B325A19DC7BD (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m929EE4EDD51D287D6781301E37FB9880D2D3926A (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_m15387FC79C3F9A8D5AB103D28917158A1F0DF429 (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_m15387FC79C3F9A8D5AB103D28917158A1F0DF429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_2 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0052;
	}

IL_0038:
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_4 = L_3->get_m_vectorLine_5();
		NullCheck(L_4);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0050:
	{
		return (bool)0;
	}

IL_0052:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3_Dispose_m5185222E3ECEF82179EE43FA62A85ACD2FFCCC05 (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3_Reset_m8B1D3B560DBA62FFE3D062FD35CE0A6B28A2922C (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameInvisibleU3Ec__Iterator3_Reset_m8B1D3B560DBA62FFE3D062FD35CE0A6B28A2922C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3COnBecameInvisibleU3Ec__Iterator3_Reset_m8B1D3B560DBA62FFE3D062FD35CE0A6B28A2922C_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2__ctor_m3D0322E423D96DFFCFBF87D8A93AD700F80A65CA (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameVisibleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7CFE3A75E95EB2BD0F0B4D1AE99FAD9EB2E5D0A7 (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameVisibleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8BC1F79CBC6756CD2064B1608DDAA1A03237CF3A (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m74638B33EA17EBA2F3D1076555DD7DE2C3902F49 (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m74638B33EA17EBA2F3D1076555DD7DE2C3902F49_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_2 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0052;
	}

IL_0038:
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_4 = L_3->get_m_vectorLine_5();
		NullCheck(L_4);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_4, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0050:
	{
		return (bool)0;
	}

IL_0052:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2_Dispose_mB185BC735AC13A3518B41E85ADE354D6F8D2EB1B (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2_Reset_mADF361215367F92DB67EC9536CE303728FE50109 (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameVisibleU3Ec__Iterator2_Reset_mADF361215367F92DB67EC9536CE303728FE50109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3COnBecameVisibleU3Ec__Iterator2_Reset_mADF361215367F92DB67EC9536CE303728FE50109_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1__ctor_mEB2B89198D42EC091034E26A99FDCBEF61CE96E6 (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CVisibilityTestU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mE30989F1FD8F1F3F879A0F977DB8D7161BF4E774 (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CVisibilityTestU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_mB63BCD8656701CAB0C8C162D01599DF58526FE12 (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CVisibilityTestU3Ec__Iterator1_MoveNext_m4BD218B997001D0C4892B312666283E5D35633FA (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CVisibilityTestU3Ec__Iterator1_MoveNext_m4BD218B997001D0C4892B312666283E5D35633FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_004b;
			}
		}
	}
	{
		goto IL_0078;
	}

IL_0025:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_007a;
	}

IL_0038:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(2);
		goto IL_007a;
	}

IL_004b:
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_3 = Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892(L_2, /*hidden argument*/Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892_RuntimeMethod_var);
		NullCheck(L_3);
		bool L_4 = Renderer_get_isVisible_mE952393384B74AD7FE85354406B19F1157CB4EC0(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0071;
		}
	}
	{
		VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_6 = L_5->get_m_vectorLine_5();
		NullCheck(L_6);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0071:
	{
		__this->set_U24PC_0((-1));
	}

IL_0078:
	{
		return (bool)0;
	}

IL_007a:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1_Dispose_m6B8D8E3A9882D877BA73B2D12FF58B74ABB74CDB (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1_Reset_mCA3C372AA2A7B0614FB0B92347A91F927906D1BA (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CVisibilityTestU3Ec__Iterator1_Reset_mCA3C372AA2A7B0614FB0B92347A91F927906D1BA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CVisibilityTestU3Ec__Iterator1_Reset_mCA3C372AA2A7B0614FB0B92347A91F927906D1BA_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlAlways::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways__ctor_mD97DBAB9ED1771430DAB8A5E812B9CC8839EB798 (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControlAlways::get_objectNumber()
extern "C" IL2CPP_METHOD_ATTR RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * VisibilityControlAlways_get_objectNumber_m93FE52569C335C45A754DC26DCC2F242CA293BC0 (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, const RuntimeMethod* method)
{
	{
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::Setup(Vectrosity.VectorLine)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways_Setup_m5619CA19EBF41DCFB139E1DE25633C14E432E7DF (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlAlways_Setup_m5619CA19EBF41DCFB139E1DE25633C14E432E7DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_1 = ___line0;
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_2 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilitySetup_m83491B58937C53E22D40A870334BE09B4C7704ED(L_0, L_1, (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 **)L_2, /*hidden argument*/NULL);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_3 = __this->get_m_objectNumber_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_i_0();
		VectorManager_DrawArrayLine2_m4020E0CD5A3299C39322A2F06C4C439AB8F9DD19(L_4, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_5 = ___line0;
		__this->set_m_vectorLine_5(L_5);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways_OnDestroy_m79041C8FF29732B0C7A2641FA02565FE092B572E (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlAlways_OnDestroy_m79041C8FF29732B0C7A2641FA02565FE092B572E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilityRemove_m5C33261E662DFA4569597949355FC86AD4A7FF5B(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mDE8EFC54CC11D3D871B74A67DCE1A0A3BC2FC3D8((VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlAlways_DontDestroyLine_m0B8DEE28405AEF5E25655615B4EAF7F1F9F0FD32 (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlStatic::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic__ctor_mA3EAEDF5E73F607799FC4E30D825E50CDD2E908D (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControlStatic::get_objectNumber()
extern "C" IL2CPP_METHOD_ATTR RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * VisibilityControlStatic_get_objectNumber_mCE37BDE2A30827F3EE75EEAE2F1220C83C77C0E5 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	{
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_Setup_m8DF1DC3A026ADB970451E19CCD65782BA960FDC0 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line0, bool ___makeBounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_Setup_m8DF1DC3A026ADB970451E19CCD65782BA960FDC0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		bool L_0 = ___makeBounds1;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_2 = ___line0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_SetupBoundsMesh_m001D291D751B469649A1C54477581C92F904BBEF(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_4 = Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED(L_3, /*hidden argument*/NULL);
		__this->set_m_originalMatrix_8(L_4);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_5 = ___line0;
		NullCheck(L_5);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_6 = VectorLine_get_points3_m5C49202A6953FBEABDC8E2198A0C64F370DF0A5A(L_5, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_7 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)il2cpp_codegen_object_new(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var);
		List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827(L_7, L_6, /*hidden argument*/List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_RuntimeMethod_var);
		V_0 = L_7;
		V_1 = 0;
		goto IL_0053;
	}

IL_0036:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_8 = V_0;
		int32_t L_9 = V_1;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * L_10 = __this->get_address_of_m_originalMatrix_8();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_11, L_12, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)L_10, L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547(L_8, L_9, L_14, /*hidden argument*/List_1_set_Item_m47442B73C0885AF91753D0CF04858D53FDCED547_RuntimeMethod_var);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0053:
	{
		int32_t L_16 = V_1;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_17, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0036;
		}
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_19 = ___line0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_20 = V_0;
		NullCheck(L_19);
		VectorLine_set_points3_m192AE593418F39E0ECF76FC2BFD5985813AEC7D2(L_19, L_20, /*hidden argument*/NULL);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_21 = ___line0;
		__this->set_m_vectorLine_5(L_21);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_22 = ___line0;
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** L_23 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilityStaticSetup_m1FA651D0D2A47D01C99369F33B95C88DEC9BB4B1(L_22, (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 **)L_23, /*hidden argument*/NULL);
		RuntimeObject* L_24 = VisibilityControlStatic_WaitCheck_m789C29BB85C9EB189F9AB3E8C97E2470CF3F24DC(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControlStatic::WaitCheck()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControlStatic_WaitCheck_m789C29BB85C9EB189F9AB3E8C97E2470CF3F24DC (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_WaitCheck_m789C29BB85C9EB189F9AB3E8C97E2470CF3F24DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * V_0 = NULL;
	{
		U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * L_0 = (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 *)il2cpp_codegen_object_new(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_il2cpp_TypeInfo_var);
		U3CWaitCheckU3Ec__Iterator4__ctor_m63DDECF3067CF698773C9989C203B980639ACA15(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnBecameVisible()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnBecameVisible_m60D7992FE0785281EB6A710922B23680BC533943 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_OnBecameVisible_m60D7992FE0785281EB6A710922B23680BC533943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_0 = __this->get_m_vectorLine_5();
		NullCheck(L_0);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_0, (bool)1, /*hidden argument*/NULL);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnBecameInvisible()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnBecameInvisible_m29065C22209B83404855E78DA60601E9833BA5F4 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_0 = __this->get_m_vectorLine_5();
		NullCheck(L_0);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnDestroy_mCD23F86B2DC562834816E4E48EAF01827FE39293 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_OnDestroy_mCD23F86B2DC562834816E4E48EAF01827FE39293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_VisibilityStaticRemove_m62842272CFC8F10526322B4F60F7966532455867(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mDE8EFC54CC11D3D871B74A67DCE1A0A3BC2FC3D8((VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::DontDestroyLine()
extern "C" IL2CPP_METHOD_ATTR void VisibilityControlStatic_DontDestroyLine_mE4BC69DDFE1E76C36033824C37C33A51A3F707F2 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::GetMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  VisibilityControlStatic_GetMatrix_m393A81CE4897F455D9006439D4A387AC398606A3 (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * __this, const RuntimeMethod* method)
{
	{
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_0 = __this->get_m_originalMatrix_8();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4__ctor_m63DDECF3067CF698773C9989C203B980639ACA15 (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitCheckU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mD673B0AEE44F87B76677BBBECD0D465CCF760F6A (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitCheckU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m412CCACC063D1CDB8EE48B71BB82F2CA0833686A (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CWaitCheckU3Ec__Iterator4_MoveNext_m826855185D28E61E3923A2C8AECD434C586AB207 (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitCheckU3Ec__Iterator4_MoveNext_m826855185D28E61E3923A2C8AECD434C586AB207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_004d;
			}
			case 2:
			{
				goto IL_0060;
			}
		}
	}
	{
		goto IL_008d;
	}

IL_0025:
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * L_3 = L_2->get_m_objectNumber_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_il2cpp_TypeInfo_var);
		VectorManager_DrawArrayLine_m26127AF61023597307CF2F4C167D25891635E081(L_4, /*hidden argument*/NULL);
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_008f;
	}

IL_004d:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(2);
		goto IL_008f;
	}

IL_0060:
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892(L_5, /*hidden argument*/Component_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m3E0C8F08ADF98436AEF5AE9F4C56A51FF7D0A892_RuntimeMethod_var);
		NullCheck(L_6);
		bool L_7 = Renderer_get_isVisible_mE952393384B74AD7FE85354406B19F1157CB4EC0(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0086;
		}
	}
	{
		VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * L_9 = L_8->get_m_vectorLine_5();
		NullCheck(L_9);
		VectorLine_set_active_m8A738295DFFA9D43612EFA7E02AFEE2E4170F135(L_9, (bool)0, /*hidden argument*/NULL);
	}

IL_0086:
	{
		__this->set_U24PC_0((-1));
	}

IL_008d:
	{
		return (bool)0;
	}

IL_008f:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4_Dispose_m2B33D720511B8EFA6E3CB8AAF16939AAF1CEDEF8 (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4_Reset_m50206552E5B267F8BCDDE793EEC585799F2B4CC4 (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitCheckU3Ec__Iterator4_Reset_m50206552E5B267F8BCDDE793EEC585799F2B4CC4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CWaitCheckU3Ec__Iterator4_Reset_m50206552E5B267F8BCDDE793EEC585799F2B4CC4_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
