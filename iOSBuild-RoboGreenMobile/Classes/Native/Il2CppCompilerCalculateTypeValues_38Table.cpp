﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey>
struct Dictionary_2_tBFEA694CB2465045E38BFFD385B7E0882370CAAF;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.AnimationCurveData>
struct Dictionary_2_t05CB3F002F4907964D7134A8EA98C230E5E350C0;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.AssimpMetadata>
struct Dictionary_2_tEEF08DE05ED9587E8DC4EBB0F61E55FAFCAAE2DC;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.EmbeddedTextureData>
struct Dictionary_2_t65D495315C64780D636CAB3892AA6E08C3ABD701;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.MeshData>
struct Dictionary_2_t5A56F61CDD731FF7DE1B774ECFE2F31750831F70;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t29EF377760EAE2E4C34F7125BB6CDFE3AE8985A1;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3;
// System.Collections.Generic.Dictionary`2<UnityEngine.SkinnedMeshRenderer,System.Collections.Generic.IList`1<System.String>>
struct Dictionary_2_t5F16EB360AF0F2B45D4A8DA0506983605AA37047;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E;
// System.Collections.Generic.List`1<TriLib.AssetAdvancedConfig>
struct List_1_t53DF7B463C92E9A1A63552E2D7B286018D2EACAD;
// System.Collections.Generic.List`1<TriLib.Extras.BoneRelationship>
struct List_1_tA054CCE4EACDF9E7829E35AB1E08F18E71595DBE;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TriLib.AnimationChannelData[]
struct AnimationChannelDataU5BU5D_tCDA1259454054C7B5A14E80302624001981625A7;
// TriLib.AnimationClipCreatedHandle
struct AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20;
// TriLib.AnimationData[]
struct AnimationDataU5BU5D_tCFB8D99D455859922FB47D3B3BC4E589DCBA21E4;
// TriLib.AssetDownloader
struct AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8;
// TriLib.AssetLoaderAsync
struct AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40;
// TriLib.AssetLoaderOptions
struct AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B;
// TriLib.AssetLoaderZip
struct AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0;
// TriLib.AssimpInterop/DataCallback
struct DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD;
// TriLib.AssimpInterop/ExistsCallback
struct ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35;
// TriLib.AssimpInterop/ProgressCallback
struct ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77;
// TriLib.AssimpMetadata[]
struct AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E;
// TriLib.AvatarCreatedHandle
struct AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2;
// TriLib.BlendShapeKeyCreatedHandle
struct BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28;
// TriLib.CameraData[]
struct CameraDataU5BU5D_t64F46362AD8787B81B58757F1766A70643B25125;
// TriLib.ConcurrentList`1<TriLib.FileLoadData>
struct ConcurrentList_1_tCD25539368BCC75EF5F876BFEB5CB322EE10F316;
// TriLib.DataDisposalCallback
struct DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B;
// TriLib.EmbeddedTextureData
struct EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479;
// TriLib.EmbeddedTextureLoadCallback
struct EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213;
// TriLib.Extras.AvatarLoader
struct AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F;
// TriLib.Extras.BoneRelationshipList
struct BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A;
// TriLib.IMaterialProperty[]
struct IMaterialPropertyU5BU5D_t5C53903294256A8B51A41B0A1E7CF0AD4C70F9C6;
// TriLib.LoadTextureDataCallback
struct LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1;
// TriLib.MaterialCreatedHandle
struct MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541;
// TriLib.MaterialData[]
struct MaterialDataU5BU5D_t589D742C36C332ABE56095676D7DC09D7DF1318B;
// TriLib.MeshCreatedHandle
struct MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5;
// TriLib.MeshData[]
struct MeshDataU5BU5D_tC038125A593483F889BD1D9864EE3AE72C2B6123;
// TriLib.MetadataProcessedHandle
struct MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A;
// TriLib.MorphChannelData[]
struct MorphChannelDataU5BU5D_t9735809D7C5CE5DB25DAB909C918F9DAAD12F2EF;
// TriLib.MorphData[]
struct MorphDataU5BU5D_t2C24700282092894F8A150518392735E30F2E518;
// TriLib.NodeData
struct NodeData_t457462F69494965439509DD564F2B853F08F6FF9;
// TriLib.NodeData[]
struct NodeDataU5BU5D_tEA24D9ADF00D08B714D7A50E75160F20155731DD;
// TriLib.ObjectLoadedHandle
struct ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B;
// TriLib.Samples.AnimationText
struct AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB;
// TriLib.Samples.AssetDownloaderZIP
struct AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322;
// TriLib.Samples.AssetLoaderWindow
struct AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B;
// TriLib.Samples.BlendShapeControl
struct BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426;
// TriLib.Samples.DownloadSample
struct DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB;
// TriLib.Samples.FileOpenEventHandle
struct FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0;
// TriLib.Samples.FileText
struct FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9;
// TriLib.TextureLoadHandle
struct TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Avatar
struct Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Networking.UnityWebRequest[]
struct UnityWebRequestU5BU5D_tE3BAE093C0057E3FF8862136282E47343BCADC7C;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef STBIMAGELOADER_TCF4C2C2C8803ACA300A9A5A5563AE4AB6ECFB397_H
#define STBIMAGELOADER_TCF4C2C2C8803ACA300A9A5A5563AE4AB6ECFB397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// STB.STBImageLoader
struct  STBImageLoader_tCF4C2C2C8803ACA300A9A5A5563AE4AB6ECFB397  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STBIMAGELOADER_TCF4C2C2C8803ACA300A9A5A5563AE4AB6ECFB397_H
#ifndef STBIMAGEINTEROP_T7D506B29404C96763AD60B7E0A3129BD18A435F1_H
#define STBIMAGEINTEROP_T7D506B29404C96763AD60B7E0A3129BD18A435F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// STBImage.STBImageInterop
struct  STBImageInterop_t7D506B29404C96763AD60B7E0A3129BD18A435F1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STBIMAGEINTEROP_T7D506B29404C96763AD60B7E0A3129BD18A435F1_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANIMATIONCHANNELDATA_T7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6_H
#define ANIMATIONCHANNELDATA_T7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationChannelData
struct  AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6  : public RuntimeObject
{
public:
	// System.String TriLib.AnimationChannelData::NodeName
	String_t* ___NodeName_0;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.AnimationCurveData> TriLib.AnimationChannelData::CurveData
	Dictionary_2_t05CB3F002F4907964D7134A8EA98C230E5E350C0 * ___CurveData_1;

public:
	inline static int32_t get_offset_of_NodeName_0() { return static_cast<int32_t>(offsetof(AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6, ___NodeName_0)); }
	inline String_t* get_NodeName_0() const { return ___NodeName_0; }
	inline String_t** get_address_of_NodeName_0() { return &___NodeName_0; }
	inline void set_NodeName_0(String_t* value)
	{
		___NodeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_0), value);
	}

	inline static int32_t get_offset_of_CurveData_1() { return static_cast<int32_t>(offsetof(AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6, ___CurveData_1)); }
	inline Dictionary_2_t05CB3F002F4907964D7134A8EA98C230E5E350C0 * get_CurveData_1() const { return ___CurveData_1; }
	inline Dictionary_2_t05CB3F002F4907964D7134A8EA98C230E5E350C0 ** get_address_of_CurveData_1() { return &___CurveData_1; }
	inline void set_CurveData_1(Dictionary_2_t05CB3F002F4907964D7134A8EA98C230E5E350C0 * value)
	{
		___CurveData_1 = value;
		Il2CppCodeGenWriteBarrier((&___CurveData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNELDATA_T7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6_H
#ifndef ANIMATIONCURVEDATA_TB4A0FA315273780FFDC8DBEE9D21A45414B7B429_H
#define ANIMATIONCURVEDATA_TB4A0FA315273780FFDC8DBEE9D21A45414B7B429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationCurveData
struct  AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429  : public RuntimeObject
{
public:
	// UnityEngine.Keyframe[] TriLib.AnimationCurveData::Keyframes
	KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* ___Keyframes_0;
	// System.UInt32 TriLib.AnimationCurveData::_index
	uint32_t ____index_1;
	// UnityEngine.AnimationCurve TriLib.AnimationCurveData::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_2;

public:
	inline static int32_t get_offset_of_Keyframes_0() { return static_cast<int32_t>(offsetof(AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429, ___Keyframes_0)); }
	inline KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* get_Keyframes_0() const { return ___Keyframes_0; }
	inline KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D** get_address_of_Keyframes_0() { return &___Keyframes_0; }
	inline void set_Keyframes_0(KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* value)
	{
		___Keyframes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Keyframes_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429, ____index_1)); }
	inline uint32_t get__index_1() const { return ____index_1; }
	inline uint32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(uint32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_2() { return static_cast<int32_t>(offsetof(AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429, ___AnimationCurve_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_2() const { return ___AnimationCurve_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_2() { return &___AnimationCurve_2; }
	inline void set_AnimationCurve_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEDATA_TB4A0FA315273780FFDC8DBEE9D21A45414B7B429_H
#ifndef ASSETADVANCEDPROPERTYMETADATA_T1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_H
#define ASSETADVANCEDPROPERTYMETADATA_T1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedPropertyMetadata
struct  AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D  : public RuntimeObject
{
public:

public:
};

struct AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_StaticFields
{
public:
	// System.String[] TriLib.AssetAdvancedPropertyMetadata::ConfigKeys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ConfigKeys_1;

public:
	inline static int32_t get_offset_of_ConfigKeys_1() { return static_cast<int32_t>(offsetof(AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_StaticFields, ___ConfigKeys_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ConfigKeys_1() const { return ___ConfigKeys_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ConfigKeys_1() { return &___ConfigKeys_1; }
	inline void set_ConfigKeys_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ConfigKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigKeys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDPROPERTYMETADATA_T1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_H
#ifndef U3CDODOWNLOADASSETU3ED__22_T8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1_H
#define U3CDODOWNLOADASSETU3ED__22_T8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetDownloader_<DoDownloadAsset>d__22
struct  U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1  : public RuntimeObject
{
public:
	// System.Int32 TriLib.AssetDownloader_<DoDownloadAsset>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TriLib.AssetDownloader_<DoDownloadAsset>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TriLib.AssetDownloader TriLib.AssetDownloader_<DoDownloadAsset>d__22::<>4__this
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8 * ___U3CU3E4__this_2;
	// System.String TriLib.AssetDownloader_<DoDownloadAsset>d__22::assetUri
	String_t* ___assetUri_3;
	// System.String TriLib.AssetDownloader_<DoDownloadAsset>d__22::assetExtension
	String_t* ___assetExtension_4;
	// TriLib.AssetLoaderOptions TriLib.AssetDownloader_<DoDownloadAsset>d__22::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_5;
	// UnityEngine.GameObject TriLib.AssetDownloader_<DoDownloadAsset>d__22::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_6;
	// TriLib.ObjectLoadedHandle TriLib.AssetDownloader_<DoDownloadAsset>d__22::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_7;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetDownloader_<DoDownloadAsset>d__22::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___U3CU3E4__this_2)); }
	inline AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_assetUri_3() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___assetUri_3)); }
	inline String_t* get_assetUri_3() const { return ___assetUri_3; }
	inline String_t** get_address_of_assetUri_3() { return &___assetUri_3; }
	inline void set_assetUri_3(String_t* value)
	{
		___assetUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___assetUri_3), value);
	}

	inline static int32_t get_offset_of_assetExtension_4() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___assetExtension_4)); }
	inline String_t* get_assetExtension_4() const { return ___assetExtension_4; }
	inline String_t** get_address_of_assetExtension_4() { return &___assetExtension_4; }
	inline void set_assetExtension_4(String_t* value)
	{
		___assetExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___assetExtension_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___options_5)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_5() const { return ___options_5; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier((&___options_5), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_6() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___wrapperGameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_6() const { return ___wrapperGameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_6() { return &___wrapperGameObject_6; }
	inline void set_wrapperGameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_6), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_7() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___onAssetLoaded_7)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_7() const { return ___onAssetLoaded_7; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_7() { return &___onAssetLoaded_7; }
	inline void set_onAssetLoaded_7(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_7), value);
	}

	inline static int32_t get_offset_of_progressCallback_8() { return static_cast<int32_t>(offsetof(U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1, ___progressCallback_8)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_8() const { return ___progressCallback_8; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_8() { return &___progressCallback_8; }
	inline void set_progressCallback_8(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDODOWNLOADASSETU3ED__22_T8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE_H
#define U3CU3EC__DISPLAYCLASS0_0_T0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderAsync TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::<>4__this
	AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * ___U3CU3E4__this_0;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::filename
	String_t* ___filename_1;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::basePath
	String_t* ___basePath_2;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_3;
	// System.Boolean TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::usesWrapperGameObject
	bool ___usesWrapperGameObject_4;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_5;
	// UnityEngine.GameObject TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_6;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderAsync_<>c__DisplayClass0_0::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_7;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___U3CU3E4__this_0)); }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_filename_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___filename_1)); }
	inline String_t* get_filename_1() const { return ___filename_1; }
	inline String_t** get_address_of_filename_1() { return &___filename_1; }
	inline void set_filename_1(String_t* value)
	{
		___filename_1 = value;
		Il2CppCodeGenWriteBarrier((&___filename_1), value);
	}

	inline static int32_t get_offset_of_basePath_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___basePath_2)); }
	inline String_t* get_basePath_2() const { return ___basePath_2; }
	inline String_t** get_address_of_basePath_2() { return &___basePath_2; }
	inline void set_basePath_2(String_t* value)
	{
		___basePath_2 = value;
		Il2CppCodeGenWriteBarrier((&___basePath_2), value);
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___options_3)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_3() const { return ___options_3; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_3 = value;
		Il2CppCodeGenWriteBarrier((&___options_3), value);
	}

	inline static int32_t get_offset_of_usesWrapperGameObject_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___usesWrapperGameObject_4)); }
	inline bool get_usesWrapperGameObject_4() const { return ___usesWrapperGameObject_4; }
	inline bool* get_address_of_usesWrapperGameObject_4() { return &___usesWrapperGameObject_4; }
	inline void set_usesWrapperGameObject_4(bool value)
	{
		___usesWrapperGameObject_4 = value;
	}

	inline static int32_t get_offset_of_progressCallback_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___progressCallback_5)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_5() const { return ___progressCallback_5; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_5() { return &___progressCallback_5; }
	inline void set_progressCallback_5(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_5), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___wrapperGameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_6() const { return ___wrapperGameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_6() { return &___wrapperGameObject_6; }
	inline void set_wrapperGameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_6), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE, ___onAssetLoaded_7)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_7() const { return ___onAssetLoaded_7; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_7() { return &___onAssetLoaded_7; }
	inline void set_onAssetLoaded_7(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T9A85B327E9E021EA37CF8888B6B841023D3ED93C_H
#define U3CU3EC__DISPLAYCLASS1_0_T9A85B327E9E021EA37CF8888B6B841023D3ED93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderAsync TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::<>4__this
	AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * ___U3CU3E4__this_0;
	// System.Byte[] TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::fileBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fileBytes_1;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::filename
	String_t* ___filename_2;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::basePath
	String_t* ___basePath_3;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_4;
	// System.Boolean TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::usesWrapperGameObject
	bool ___usesWrapperGameObject_5;
	// TriLib.AssimpInterop_DataCallback TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::dataCallback
	DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * ___dataCallback_6;
	// TriLib.AssimpInterop_ExistsCallback TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::existsCallback
	ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * ___existsCallback_7;
	// TriLib.LoadTextureDataCallback TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::loadTextureDataCallback
	LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1 * ___loadTextureDataCallback_8;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_9;
	// UnityEngine.GameObject TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_10;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderAsync_<>c__DisplayClass1_0::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_11;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___U3CU3E4__this_0)); }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_fileBytes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___fileBytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fileBytes_1() const { return ___fileBytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fileBytes_1() { return &___fileBytes_1; }
	inline void set_fileBytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fileBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileBytes_1), value);
	}

	inline static int32_t get_offset_of_filename_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___filename_2)); }
	inline String_t* get_filename_2() const { return ___filename_2; }
	inline String_t** get_address_of_filename_2() { return &___filename_2; }
	inline void set_filename_2(String_t* value)
	{
		___filename_2 = value;
		Il2CppCodeGenWriteBarrier((&___filename_2), value);
	}

	inline static int32_t get_offset_of_basePath_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___basePath_3)); }
	inline String_t* get_basePath_3() const { return ___basePath_3; }
	inline String_t** get_address_of_basePath_3() { return &___basePath_3; }
	inline void set_basePath_3(String_t* value)
	{
		___basePath_3 = value;
		Il2CppCodeGenWriteBarrier((&___basePath_3), value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___options_4)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_4() const { return ___options_4; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_4 = value;
		Il2CppCodeGenWriteBarrier((&___options_4), value);
	}

	inline static int32_t get_offset_of_usesWrapperGameObject_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___usesWrapperGameObject_5)); }
	inline bool get_usesWrapperGameObject_5() const { return ___usesWrapperGameObject_5; }
	inline bool* get_address_of_usesWrapperGameObject_5() { return &___usesWrapperGameObject_5; }
	inline void set_usesWrapperGameObject_5(bool value)
	{
		___usesWrapperGameObject_5 = value;
	}

	inline static int32_t get_offset_of_dataCallback_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___dataCallback_6)); }
	inline DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * get_dataCallback_6() const { return ___dataCallback_6; }
	inline DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD ** get_address_of_dataCallback_6() { return &___dataCallback_6; }
	inline void set_dataCallback_6(DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * value)
	{
		___dataCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___dataCallback_6), value);
	}

	inline static int32_t get_offset_of_existsCallback_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___existsCallback_7)); }
	inline ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * get_existsCallback_7() const { return ___existsCallback_7; }
	inline ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 ** get_address_of_existsCallback_7() { return &___existsCallback_7; }
	inline void set_existsCallback_7(ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * value)
	{
		___existsCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___existsCallback_7), value);
	}

	inline static int32_t get_offset_of_loadTextureDataCallback_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___loadTextureDataCallback_8)); }
	inline LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1 * get_loadTextureDataCallback_8() const { return ___loadTextureDataCallback_8; }
	inline LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1 ** get_address_of_loadTextureDataCallback_8() { return &___loadTextureDataCallback_8; }
	inline void set_loadTextureDataCallback_8(LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1 * value)
	{
		___loadTextureDataCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___loadTextureDataCallback_8), value);
	}

	inline static int32_t get_offset_of_progressCallback_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___progressCallback_9)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_9() const { return ___progressCallback_9; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_9() { return &___progressCallback_9; }
	inline void set_progressCallback_9(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_9), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___wrapperGameObject_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_10() const { return ___wrapperGameObject_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_10() { return &___wrapperGameObject_10; }
	inline void set_wrapperGameObject_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_10), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C, ___onAssetLoaded_11)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_11() const { return ___onAssetLoaded_11; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_11() { return &___onAssetLoaded_11; }
	inline void set_onAssetLoaded_11(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_11 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T9A85B327E9E021EA37CF8888B6B841023D3ED93C_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T414D895D6AB9E83D9C944887ED15188FB5087C2B_H
#define U3CU3EC__DISPLAYCLASS2_0_T414D895D6AB9E83D9C944887ED15188FB5087C2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B  : public RuntimeObject
{
public:
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::filename
	String_t* ___filename_0;
	// TriLib.AssetLoaderAsync TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::<>4__this
	AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * ___U3CU3E4__this_1;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::extension
	String_t* ___extension_2;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::basePath
	String_t* ___basePath_3;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_4;
	// System.Boolean TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::usesWrapperGameObject
	bool ___usesWrapperGameObject_5;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_6;
	// UnityEngine.GameObject TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_7;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderAsync_<>c__DisplayClass2_0::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_8;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___U3CU3E4__this_1)); }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_extension_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___extension_2)); }
	inline String_t* get_extension_2() const { return ___extension_2; }
	inline String_t** get_address_of_extension_2() { return &___extension_2; }
	inline void set_extension_2(String_t* value)
	{
		___extension_2 = value;
		Il2CppCodeGenWriteBarrier((&___extension_2), value);
	}

	inline static int32_t get_offset_of_basePath_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___basePath_3)); }
	inline String_t* get_basePath_3() const { return ___basePath_3; }
	inline String_t** get_address_of_basePath_3() { return &___basePath_3; }
	inline void set_basePath_3(String_t* value)
	{
		___basePath_3 = value;
		Il2CppCodeGenWriteBarrier((&___basePath_3), value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___options_4)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_4() const { return ___options_4; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_4 = value;
		Il2CppCodeGenWriteBarrier((&___options_4), value);
	}

	inline static int32_t get_offset_of_usesWrapperGameObject_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___usesWrapperGameObject_5)); }
	inline bool get_usesWrapperGameObject_5() const { return ___usesWrapperGameObject_5; }
	inline bool* get_address_of_usesWrapperGameObject_5() { return &___usesWrapperGameObject_5; }
	inline void set_usesWrapperGameObject_5(bool value)
	{
		___usesWrapperGameObject_5 = value;
	}

	inline static int32_t get_offset_of_progressCallback_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___progressCallback_6)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_6() const { return ___progressCallback_6; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_6() { return &___progressCallback_6; }
	inline void set_progressCallback_6(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_6), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___wrapperGameObject_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_7() const { return ___wrapperGameObject_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_7() { return &___wrapperGameObject_7; }
	inline void set_wrapperGameObject_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_7), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B, ___onAssetLoaded_8)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_8() const { return ___onAssetLoaded_8; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_8() { return &___onAssetLoaded_8; }
	inline void set_onAssetLoaded_8(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_8 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T414D895D6AB9E83D9C944887ED15188FB5087C2B_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T355CE05A0C57252B59BE03FF5BCBC47B17A86189_H
#define U3CU3EC__DISPLAYCLASS3_0_T355CE05A0C57252B59BE03FF5BCBC47B17A86189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderAsync TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::<>4__this
	AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * ___U3CU3E4__this_0;
	// System.Byte[] TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::fileData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fileData_1;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::assetExtension
	String_t* ___assetExtension_2;
	// System.String TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::basePath
	String_t* ___basePath_3;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_4;
	// System.Boolean TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::usesWrapperGameObject
	bool ___usesWrapperGameObject_5;
	// TriLib.AssimpInterop_DataCallback TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::dataCallback
	DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * ___dataCallback_6;
	// TriLib.AssimpInterop_ExistsCallback TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::existsCallback
	ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * ___existsCallback_7;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_8;
	// UnityEngine.GameObject TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_9;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderAsync_<>c__DisplayClass3_0::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_10;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___U3CU3E4__this_0)); }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_fileData_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___fileData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fileData_1() const { return ___fileData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fileData_1() { return &___fileData_1; }
	inline void set_fileData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fileData_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileData_1), value);
	}

	inline static int32_t get_offset_of_assetExtension_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___assetExtension_2)); }
	inline String_t* get_assetExtension_2() const { return ___assetExtension_2; }
	inline String_t** get_address_of_assetExtension_2() { return &___assetExtension_2; }
	inline void set_assetExtension_2(String_t* value)
	{
		___assetExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___assetExtension_2), value);
	}

	inline static int32_t get_offset_of_basePath_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___basePath_3)); }
	inline String_t* get_basePath_3() const { return ___basePath_3; }
	inline String_t** get_address_of_basePath_3() { return &___basePath_3; }
	inline void set_basePath_3(String_t* value)
	{
		___basePath_3 = value;
		Il2CppCodeGenWriteBarrier((&___basePath_3), value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___options_4)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_4() const { return ___options_4; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_4 = value;
		Il2CppCodeGenWriteBarrier((&___options_4), value);
	}

	inline static int32_t get_offset_of_usesWrapperGameObject_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___usesWrapperGameObject_5)); }
	inline bool get_usesWrapperGameObject_5() const { return ___usesWrapperGameObject_5; }
	inline bool* get_address_of_usesWrapperGameObject_5() { return &___usesWrapperGameObject_5; }
	inline void set_usesWrapperGameObject_5(bool value)
	{
		___usesWrapperGameObject_5 = value;
	}

	inline static int32_t get_offset_of_dataCallback_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___dataCallback_6)); }
	inline DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * get_dataCallback_6() const { return ___dataCallback_6; }
	inline DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD ** get_address_of_dataCallback_6() { return &___dataCallback_6; }
	inline void set_dataCallback_6(DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD * value)
	{
		___dataCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___dataCallback_6), value);
	}

	inline static int32_t get_offset_of_existsCallback_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___existsCallback_7)); }
	inline ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * get_existsCallback_7() const { return ___existsCallback_7; }
	inline ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 ** get_address_of_existsCallback_7() { return &___existsCallback_7; }
	inline void set_existsCallback_7(ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35 * value)
	{
		___existsCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___existsCallback_7), value);
	}

	inline static int32_t get_offset_of_progressCallback_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___progressCallback_8)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_8() const { return ___progressCallback_8; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_8() { return &___progressCallback_8; }
	inline void set_progressCallback_8(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_8), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___wrapperGameObject_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_9() const { return ___wrapperGameObject_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_9() { return &___wrapperGameObject_9; }
	inline void set_wrapperGameObject_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_9 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_9), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189, ___onAssetLoaded_10)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_10() const { return ___onAssetLoaded_10; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_10() { return &___onAssetLoaded_10; }
	inline void set_onAssetLoaded_10(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_10 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T355CE05A0C57252B59BE03FF5BCBC47B17A86189_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T3DEF68C2B5CB4802B96C513865F290644E21A425_H
#define U3CU3EC__DISPLAYCLASS5_0_T3DEF68C2B5CB4802B96C513865F290644E21A425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderZip_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderZip TriLib.AssetLoaderZip_<>c__DisplayClass5_0::<>4__this
	AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0 * ___U3CU3E4__this_0;
	// System.String TriLib.AssetLoaderZip_<>c__DisplayClass5_0::filename
	String_t* ___filename_1;
	// System.String TriLib.AssetLoaderZip_<>c__DisplayClass5_0::basePath
	String_t* ___basePath_2;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderZip_<>c__DisplayClass5_0::options
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ___options_3;
	// System.Boolean TriLib.AssetLoaderZip_<>c__DisplayClass5_0::usesWrapperGameObject
	bool ___usesWrapperGameObject_4;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderZip_<>c__DisplayClass5_0::progressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___progressCallback_5;
	// UnityEngine.GameObject TriLib.AssetLoaderZip_<>c__DisplayClass5_0::wrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wrapperGameObject_6;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderZip_<>c__DisplayClass5_0::onAssetLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___onAssetLoaded_7;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___U3CU3E4__this_0)); }
	inline AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_filename_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___filename_1)); }
	inline String_t* get_filename_1() const { return ___filename_1; }
	inline String_t** get_address_of_filename_1() { return &___filename_1; }
	inline void set_filename_1(String_t* value)
	{
		___filename_1 = value;
		Il2CppCodeGenWriteBarrier((&___filename_1), value);
	}

	inline static int32_t get_offset_of_basePath_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___basePath_2)); }
	inline String_t* get_basePath_2() const { return ___basePath_2; }
	inline String_t** get_address_of_basePath_2() { return &___basePath_2; }
	inline void set_basePath_2(String_t* value)
	{
		___basePath_2 = value;
		Il2CppCodeGenWriteBarrier((&___basePath_2), value);
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___options_3)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get_options_3() const { return ___options_3; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		___options_3 = value;
		Il2CppCodeGenWriteBarrier((&___options_3), value);
	}

	inline static int32_t get_offset_of_usesWrapperGameObject_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___usesWrapperGameObject_4)); }
	inline bool get_usesWrapperGameObject_4() const { return ___usesWrapperGameObject_4; }
	inline bool* get_address_of_usesWrapperGameObject_4() { return &___usesWrapperGameObject_4; }
	inline void set_usesWrapperGameObject_4(bool value)
	{
		___usesWrapperGameObject_4 = value;
	}

	inline static int32_t get_offset_of_progressCallback_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___progressCallback_5)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_progressCallback_5() const { return ___progressCallback_5; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_progressCallback_5() { return &___progressCallback_5; }
	inline void set_progressCallback_5(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___progressCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_5), value);
	}

	inline static int32_t get_offset_of_wrapperGameObject_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___wrapperGameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wrapperGameObject_6() const { return ___wrapperGameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wrapperGameObject_6() { return &___wrapperGameObject_6; }
	inline void set_wrapperGameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wrapperGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___wrapperGameObject_6), value);
	}

	inline static int32_t get_offset_of_onAssetLoaded_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425, ___onAssetLoaded_7)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_onAssetLoaded_7() const { return ___onAssetLoaded_7; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_onAssetLoaded_7() { return &___onAssetLoaded_7; }
	inline void set_onAssetLoaded_7(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___onAssetLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___onAssetLoaded_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T3DEF68C2B5CB4802B96C513865F290644E21A425_H
#ifndef ASSIMPINTEROP_T878374C66C809A09BD6354E36DF6928BB007BEB7_H
#define ASSIMPINTEROP_T878374C66C809A09BD6354E36DF6928BB007BEB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop
struct  AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7  : public RuntimeObject
{
public:

public:
};

struct AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields
{
public:
	// System.Boolean TriLib.AssimpInterop::Is32Bits
	bool ___Is32Bits_3;
	// System.Int32 TriLib.AssimpInterop::IntSize
	int32_t ___IntSize_4;

public:
	inline static int32_t get_offset_of_Is32Bits_3() { return static_cast<int32_t>(offsetof(AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields, ___Is32Bits_3)); }
	inline bool get_Is32Bits_3() const { return ___Is32Bits_3; }
	inline bool* get_address_of_Is32Bits_3() { return &___Is32Bits_3; }
	inline void set_Is32Bits_3(bool value)
	{
		___Is32Bits_3 = value;
	}

	inline static int32_t get_offset_of_IntSize_4() { return static_cast<int32_t>(offsetof(AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields, ___IntSize_4)); }
	inline int32_t get_IntSize_4() const { return ___IntSize_4; }
	inline int32_t* get_address_of_IntSize_4() { return &___IntSize_4; }
	inline void set_IntSize_4(int32_t value)
	{
		___IntSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPINTEROP_T878374C66C809A09BD6354E36DF6928BB007BEB7_H
#ifndef CAMERAEXTENSIONS_T52A82873EA1CF46FD0A2271F3617F2EE91C78F00_H
#define CAMERAEXTENSIONS_T52A82873EA1CF46FD0A2271F3617F2EE91C78F00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.CameraExtensions
struct  CameraExtensions_t52A82873EA1CF46FD0A2271F3617F2EE91C78F00  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEXTENSIONS_T52A82873EA1CF46FD0A2271F3617F2EE91C78F00_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T51CB3797E623374909EB0DB46AD5E061B6997FF1_H
#define U3CU3EC__DISPLAYCLASS8_0_T51CB3797E623374909EB0DB46AD5E061B6997FF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoaderSample_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t51CB3797E623374909EB0DB46AD5E061B6997FF1  : public RuntimeObject
{
public:
	// System.String TriLib.Extras.AvatarLoaderSample_<>c__DisplayClass8_0::supportedExtensions
	String_t* ___supportedExtensions_0;

public:
	inline static int32_t get_offset_of_supportedExtensions_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t51CB3797E623374909EB0DB46AD5E061B6997FF1, ___supportedExtensions_0)); }
	inline String_t* get_supportedExtensions_0() const { return ___supportedExtensions_0; }
	inline String_t** get_address_of_supportedExtensions_0() { return &___supportedExtensions_0; }
	inline void set_supportedExtensions_0(String_t* value)
	{
		___supportedExtensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___supportedExtensions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T51CB3797E623374909EB0DB46AD5E061B6997FF1_H
#ifndef BONERELATIONSHIP_T4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8_H
#define BONERELATIONSHIP_T4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.BoneRelationship
struct  BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8  : public RuntimeObject
{
public:
	// System.String TriLib.Extras.BoneRelationship::HumanBone
	String_t* ___HumanBone_0;
	// System.String TriLib.Extras.BoneRelationship::BoneName
	String_t* ___BoneName_1;
	// System.Boolean TriLib.Extras.BoneRelationship::Optional
	bool ___Optional_2;

public:
	inline static int32_t get_offset_of_HumanBone_0() { return static_cast<int32_t>(offsetof(BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8, ___HumanBone_0)); }
	inline String_t* get_HumanBone_0() const { return ___HumanBone_0; }
	inline String_t** get_address_of_HumanBone_0() { return &___HumanBone_0; }
	inline void set_HumanBone_0(String_t* value)
	{
		___HumanBone_0 = value;
		Il2CppCodeGenWriteBarrier((&___HumanBone_0), value);
	}

	inline static int32_t get_offset_of_BoneName_1() { return static_cast<int32_t>(offsetof(BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8, ___BoneName_1)); }
	inline String_t* get_BoneName_1() const { return ___BoneName_1; }
	inline String_t** get_address_of_BoneName_1() { return &___BoneName_1; }
	inline void set_BoneName_1(String_t* value)
	{
		___BoneName_1 = value;
		Il2CppCodeGenWriteBarrier((&___BoneName_1), value);
	}

	inline static int32_t get_offset_of_Optional_2() { return static_cast<int32_t>(offsetof(BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8, ___Optional_2)); }
	inline bool get_Optional_2() const { return ___Optional_2; }
	inline bool* get_address_of_Optional_2() { return &___Optional_2; }
	inline void set_Optional_2(bool value)
	{
		___Optional_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONERELATIONSHIP_T4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8_H
#ifndef BONERELATIONSHIPLIST_T87AF0272CB6927BDE06500D48A4514AD11EF803A_H
#define BONERELATIONSHIPLIST_T87AF0272CB6927BDE06500D48A4514AD11EF803A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.BoneRelationshipList
struct  BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TriLib.Extras.BoneRelationship> TriLib.Extras.BoneRelationshipList::_relationships
	List_1_tA054CCE4EACDF9E7829E35AB1E08F18E71595DBE * ____relationships_0;

public:
	inline static int32_t get_offset_of__relationships_0() { return static_cast<int32_t>(offsetof(BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A, ____relationships_0)); }
	inline List_1_tA054CCE4EACDF9E7829E35AB1E08F18E71595DBE * get__relationships_0() const { return ____relationships_0; }
	inline List_1_tA054CCE4EACDF9E7829E35AB1E08F18E71595DBE ** get_address_of__relationships_0() { return &____relationships_0; }
	inline void set__relationships_0(List_1_tA054CCE4EACDF9E7829E35AB1E08F18E71595DBE * value)
	{
		____relationships_0 = value;
		Il2CppCodeGenWriteBarrier((&____relationships_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONERELATIONSHIPLIST_T87AF0272CB6927BDE06500D48A4514AD11EF803A_H
#ifndef FILELOADDATA_T1DB464F7A4A2E23D988714D7E50B250E038C36C7_H
#define FILELOADDATA_T1DB464F7A4A2E23D988714D7E50B250E038C36C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileLoadData
struct  FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7  : public RuntimeObject
{
public:
	// System.String TriLib.FileLoadData::Filename
	String_t* ___Filename_0;
	// System.String TriLib.FileLoadData::BasePath
	String_t* ___BasePath_1;

public:
	inline static int32_t get_offset_of_Filename_0() { return static_cast<int32_t>(offsetof(FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7, ___Filename_0)); }
	inline String_t* get_Filename_0() const { return ___Filename_0; }
	inline String_t** get_address_of_Filename_0() { return &___Filename_0; }
	inline void set_Filename_0(String_t* value)
	{
		___Filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___Filename_0), value);
	}

	inline static int32_t get_offset_of_BasePath_1() { return static_cast<int32_t>(offsetof(FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7, ___BasePath_1)); }
	inline String_t* get_BasePath_1() const { return ___BasePath_1; }
	inline String_t** get_address_of_BasePath_1() { return &___BasePath_1; }
	inline void set_BasePath_1(String_t* value)
	{
		___BasePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___BasePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILELOADDATA_T1DB464F7A4A2E23D988714D7E50B250E038C36C7_H
#ifndef FILEUTILS_T526176EC05822EED286A026F0A00CBD6853B9B4A_H
#define FILEUTILS_T526176EC05822EED286A026F0A00CBD6853B9B4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileUtils
struct  FileUtils_t526176EC05822EED286A026F0A00CBD6853B9B4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_T526176EC05822EED286A026F0A00CBD6853B9B4A_H
#ifndef MATRIXEXTENSIONS_T24B98782A6128043B166DE80B05EAF663B3BE019_H
#define MATRIXEXTENSIONS_T24B98782A6128043B166DE80B05EAF663B3BE019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MatrixExtensions
struct  MatrixExtensions_t24B98782A6128043B166DE80B05EAF663B3BE019  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXEXTENSIONS_T24B98782A6128043B166DE80B05EAF663B3BE019_H
#ifndef MESHDATA_T0A760AADBD6148FDA217567E2C0FB4E689531EAD_H
#define MESHDATA_T0A760AADBD6148FDA217567E2C0FB4E689531EAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshData
struct  MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD  : public RuntimeObject
{
public:
	// System.String TriLib.MeshData::Name
	String_t* ___Name_0;
	// System.String TriLib.MeshData::SubMeshName
	String_t* ___SubMeshName_1;
	// UnityEngine.Vector3[] TriLib.MeshData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_2;
	// UnityEngine.Vector3[] TriLib.MeshData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_3;
	// UnityEngine.Vector4[] TriLib.MeshData::Tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___Tangents_4;
	// UnityEngine.Vector4[] TriLib.MeshData::BiTangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___BiTangents_5;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv_6;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv1
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv1_7;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv2_8;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv3
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv3_9;
	// UnityEngine.Color[] TriLib.MeshData::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_10;
	// System.Int32[] TriLib.MeshData::Triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Triangles_11;
	// System.Boolean TriLib.MeshData::HasBoneInfo
	bool ___HasBoneInfo_12;
	// UnityEngine.Matrix4x4[] TriLib.MeshData::BindPoses
	Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* ___BindPoses_13;
	// System.String[] TriLib.MeshData::BoneNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___BoneNames_14;
	// UnityEngine.BoneWeight[] TriLib.MeshData::BoneWeights
	BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* ___BoneWeights_15;
	// System.UInt32 TriLib.MeshData::MaterialIndex
	uint32_t ___MaterialIndex_16;
	// TriLib.MorphData[] TriLib.MeshData::MorphsData
	MorphDataU5BU5D_t2C24700282092894F8A150518392735E30F2E518* ___MorphsData_17;
	// UnityEngine.Mesh TriLib.MeshData::Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_18;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_SubMeshName_1() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___SubMeshName_1)); }
	inline String_t* get_SubMeshName_1() const { return ___SubMeshName_1; }
	inline String_t** get_address_of_SubMeshName_1() { return &___SubMeshName_1; }
	inline void set_SubMeshName_1(String_t* value)
	{
		___SubMeshName_1 = value;
		Il2CppCodeGenWriteBarrier((&___SubMeshName_1), value);
	}

	inline static int32_t get_offset_of_Vertices_2() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Vertices_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_2() const { return ___Vertices_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_2() { return &___Vertices_2; }
	inline void set_Vertices_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_2 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_2), value);
	}

	inline static int32_t get_offset_of_Normals_3() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Normals_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_3() const { return ___Normals_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_3() { return &___Normals_3; }
	inline void set_Normals_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_3 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_3), value);
	}

	inline static int32_t get_offset_of_Tangents_4() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Tangents_4)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_Tangents_4() const { return ___Tangents_4; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_Tangents_4() { return &___Tangents_4; }
	inline void set_Tangents_4(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___Tangents_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_4), value);
	}

	inline static int32_t get_offset_of_BiTangents_5() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___BiTangents_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_BiTangents_5() const { return ___BiTangents_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_BiTangents_5() { return &___BiTangents_5; }
	inline void set_BiTangents_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___BiTangents_5 = value;
		Il2CppCodeGenWriteBarrier((&___BiTangents_5), value);
	}

	inline static int32_t get_offset_of_Uv_6() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Uv_6)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv_6() const { return ___Uv_6; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv_6() { return &___Uv_6; }
	inline void set_Uv_6(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv_6 = value;
		Il2CppCodeGenWriteBarrier((&___Uv_6), value);
	}

	inline static int32_t get_offset_of_Uv1_7() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Uv1_7)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv1_7() const { return ___Uv1_7; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv1_7() { return &___Uv1_7; }
	inline void set_Uv1_7(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv1_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uv1_7), value);
	}

	inline static int32_t get_offset_of_Uv2_8() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Uv2_8)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv2_8() const { return ___Uv2_8; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv2_8() { return &___Uv2_8; }
	inline void set_Uv2_8(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv2_8 = value;
		Il2CppCodeGenWriteBarrier((&___Uv2_8), value);
	}

	inline static int32_t get_offset_of_Uv3_9() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Uv3_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv3_9() const { return ___Uv3_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv3_9() { return &___Uv3_9; }
	inline void set_Uv3_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv3_9 = value;
		Il2CppCodeGenWriteBarrier((&___Uv3_9), value);
	}

	inline static int32_t get_offset_of_Colors_10() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Colors_10)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_10() const { return ___Colors_10; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_10() { return &___Colors_10; }
	inline void set_Colors_10(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_10 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_10), value);
	}

	inline static int32_t get_offset_of_Triangles_11() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Triangles_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Triangles_11() const { return ___Triangles_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Triangles_11() { return &___Triangles_11; }
	inline void set_Triangles_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_11), value);
	}

	inline static int32_t get_offset_of_HasBoneInfo_12() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___HasBoneInfo_12)); }
	inline bool get_HasBoneInfo_12() const { return ___HasBoneInfo_12; }
	inline bool* get_address_of_HasBoneInfo_12() { return &___HasBoneInfo_12; }
	inline void set_HasBoneInfo_12(bool value)
	{
		___HasBoneInfo_12 = value;
	}

	inline static int32_t get_offset_of_BindPoses_13() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___BindPoses_13)); }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* get_BindPoses_13() const { return ___BindPoses_13; }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9** get_address_of_BindPoses_13() { return &___BindPoses_13; }
	inline void set_BindPoses_13(Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* value)
	{
		___BindPoses_13 = value;
		Il2CppCodeGenWriteBarrier((&___BindPoses_13), value);
	}

	inline static int32_t get_offset_of_BoneNames_14() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___BoneNames_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_BoneNames_14() const { return ___BoneNames_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_BoneNames_14() { return &___BoneNames_14; }
	inline void set_BoneNames_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___BoneNames_14 = value;
		Il2CppCodeGenWriteBarrier((&___BoneNames_14), value);
	}

	inline static int32_t get_offset_of_BoneWeights_15() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___BoneWeights_15)); }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* get_BoneWeights_15() const { return ___BoneWeights_15; }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D** get_address_of_BoneWeights_15() { return &___BoneWeights_15; }
	inline void set_BoneWeights_15(BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* value)
	{
		___BoneWeights_15 = value;
		Il2CppCodeGenWriteBarrier((&___BoneWeights_15), value);
	}

	inline static int32_t get_offset_of_MaterialIndex_16() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___MaterialIndex_16)); }
	inline uint32_t get_MaterialIndex_16() const { return ___MaterialIndex_16; }
	inline uint32_t* get_address_of_MaterialIndex_16() { return &___MaterialIndex_16; }
	inline void set_MaterialIndex_16(uint32_t value)
	{
		___MaterialIndex_16 = value;
	}

	inline static int32_t get_offset_of_MorphsData_17() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___MorphsData_17)); }
	inline MorphDataU5BU5D_t2C24700282092894F8A150518392735E30F2E518* get_MorphsData_17() const { return ___MorphsData_17; }
	inline MorphDataU5BU5D_t2C24700282092894F8A150518392735E30F2E518** get_address_of_MorphsData_17() { return &___MorphsData_17; }
	inline void set_MorphsData_17(MorphDataU5BU5D_t2C24700282092894F8A150518392735E30F2E518* value)
	{
		___MorphsData_17 = value;
		Il2CppCodeGenWriteBarrier((&___MorphsData_17), value);
	}

	inline static int32_t get_offset_of_Mesh_18() { return static_cast<int32_t>(offsetof(MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD, ___Mesh_18)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_Mesh_18() const { return ___Mesh_18; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_Mesh_18() { return &___Mesh_18; }
	inline void set_Mesh_18(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___Mesh_18 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T0A760AADBD6148FDA217567E2C0FB4E689531EAD_H
#ifndef MORPHCHANNELDATA_T21955FCBCECB3B96FED28C7326F63691EBD1E819_H
#define MORPHCHANNELDATA_T21955FCBCECB3B96FED28C7326F63691EBD1E819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelData
struct  MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819  : public RuntimeObject
{
public:
	// System.String TriLib.MorphChannelData::NodeName
	String_t* ___NodeName_0;
	// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey> TriLib.MorphChannelData::MorphChannelKeys
	Dictionary_2_tBFEA694CB2465045E38BFFD385B7E0882370CAAF * ___MorphChannelKeys_1;

public:
	inline static int32_t get_offset_of_NodeName_0() { return static_cast<int32_t>(offsetof(MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819, ___NodeName_0)); }
	inline String_t* get_NodeName_0() const { return ___NodeName_0; }
	inline String_t** get_address_of_NodeName_0() { return &___NodeName_0; }
	inline void set_NodeName_0(String_t* value)
	{
		___NodeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_0), value);
	}

	inline static int32_t get_offset_of_MorphChannelKeys_1() { return static_cast<int32_t>(offsetof(MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819, ___MorphChannelKeys_1)); }
	inline Dictionary_2_tBFEA694CB2465045E38BFFD385B7E0882370CAAF * get_MorphChannelKeys_1() const { return ___MorphChannelKeys_1; }
	inline Dictionary_2_tBFEA694CB2465045E38BFFD385B7E0882370CAAF ** get_address_of_MorphChannelKeys_1() { return &___MorphChannelKeys_1; }
	inline void set_MorphChannelKeys_1(Dictionary_2_tBFEA694CB2465045E38BFFD385B7E0882370CAAF * value)
	{
		___MorphChannelKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___MorphChannelKeys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELDATA_T21955FCBCECB3B96FED28C7326F63691EBD1E819_H
#ifndef MORPHCHANNELKEY_TD497F720ADE0509A15B4152420FE66DD2685E3C5_H
#define MORPHCHANNELKEY_TD497F720ADE0509A15B4152420FE66DD2685E3C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelKey
struct  MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5  : public RuntimeObject
{
public:
	// System.UInt32[] TriLib.MorphChannelKey::Indices
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Indices_0;
	// System.Single[] TriLib.MorphChannelKey::Weights
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Weights_1;

public:
	inline static int32_t get_offset_of_Indices_0() { return static_cast<int32_t>(offsetof(MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5, ___Indices_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Indices_0() const { return ___Indices_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Indices_0() { return &___Indices_0; }
	inline void set_Indices_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Indices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_0), value);
	}

	inline static int32_t get_offset_of_Weights_1() { return static_cast<int32_t>(offsetof(MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5, ___Weights_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Weights_1() const { return ___Weights_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Weights_1() { return &___Weights_1; }
	inline void set_Weights_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Weights_1 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELKEY_TD497F720ADE0509A15B4152420FE66DD2685E3C5_H
#ifndef MORPHDATA_TBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4_H
#define MORPHDATA_TBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphData
struct  MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4  : public RuntimeObject
{
public:
	// System.String TriLib.MorphData::Name
	String_t* ___Name_0;
	// UnityEngine.Vector3[] TriLib.MorphData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_1;
	// UnityEngine.Vector3[] TriLib.MorphData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_2;
	// UnityEngine.Vector3[] TriLib.MorphData::Tangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Tangents_3;
	// System.Single TriLib.MorphData::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Vertices_1() { return static_cast<int32_t>(offsetof(MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4, ___Vertices_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_1() const { return ___Vertices_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_1() { return &___Vertices_1; }
	inline void set_Vertices_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_1), value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4, ___Normals_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_2() const { return ___Normals_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_2), value);
	}

	inline static int32_t get_offset_of_Tangents_3() { return static_cast<int32_t>(offsetof(MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4, ___Tangents_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Tangents_3() const { return ___Tangents_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Tangents_3() { return &___Tangents_3; }
	inline void set_Tangents_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Tangents_3 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_3), value);
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHDATA_TBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4_H
#ifndef U3CDOWNLOADFILEU3ED__6_TACEA013908769DA01F76866D839FEFEF01696EBD_H
#define U3CDOWNLOADFILEU3ED__6_TACEA013908769DA01F76866D839FEFEF01696EBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6
struct  U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD  : public RuntimeObject
{
public:
	// System.Int32 TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TriLib.Samples.AssetDownloaderZIP TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::<>4__this
	AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322 * ___U3CU3E4__this_2;
	// System.String TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::url
	String_t* ___url_3;
	// System.String TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::fileExtension
	String_t* ___fileExtension_4;
	// System.String TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::localFilePath
	String_t* ___localFilePath_5;
	// System.String TriLib.Samples.AssetDownloaderZIP_<DownloadFile>d__6::localFilename
	String_t* ___localFilename_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___U3CU3E4__this_2)); }
	inline AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_fileExtension_4() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___fileExtension_4)); }
	inline String_t* get_fileExtension_4() const { return ___fileExtension_4; }
	inline String_t** get_address_of_fileExtension_4() { return &___fileExtension_4; }
	inline void set_fileExtension_4(String_t* value)
	{
		___fileExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileExtension_4), value);
	}

	inline static int32_t get_offset_of_localFilePath_5() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___localFilePath_5)); }
	inline String_t* get_localFilePath_5() const { return ___localFilePath_5; }
	inline String_t** get_address_of_localFilePath_5() { return &___localFilePath_5; }
	inline void set_localFilePath_5(String_t* value)
	{
		___localFilePath_5 = value;
		Il2CppCodeGenWriteBarrier((&___localFilePath_5), value);
	}

	inline static int32_t get_offset_of_localFilename_6() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD, ___localFilename_6)); }
	inline String_t* get_localFilename_6() const { return ___localFilename_6; }
	inline String_t** get_address_of_localFilename_6() { return &___localFilename_6; }
	inline void set_localFilename_6(String_t* value)
	{
		___localFilename_6 = value;
		Il2CppCodeGenWriteBarrier((&___localFilename_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADFILEU3ED__6_TACEA013908769DA01F76866D839FEFEF01696EBD_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_TF933BE86382838062FC4C544882293B0CBF7C0DE_H
#define U3CU3EC__DISPLAYCLASS38_0_TF933BE86382838062FC4C544882293B0CBF7C0DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.AssetLoaderWindow_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderAsync TriLib.Samples.AssetLoaderWindow_<>c__DisplayClass38_0::assetLoader
	AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * ___assetLoader_0;
	// TriLib.Samples.AssetLoaderWindow TriLib.Samples.AssetLoaderWindow_<>c__DisplayClass38_0::<>4__this
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_assetLoader_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE, ___assetLoader_0)); }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * get_assetLoader_0() const { return ___assetLoader_0; }
	inline AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 ** get_address_of_assetLoader_0() { return &___assetLoader_0; }
	inline void set_assetLoader_0(AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40 * value)
	{
		___assetLoader_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetLoader_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE, ___U3CU3E4__this_1)); }
	inline AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_TF933BE86382838062FC4C544882293B0CBF7C0DE_H
#ifndef U3CDOWNLOADFILEU3ED__7_TDCA38118765851B26AA54908C14315389F287B59_H
#define U3CDOWNLOADFILEU3ED__7_TDCA38118765851B26AA54908C14315389F287B59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.DownloadSample_<DownloadFile>d__7
struct  U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59  : public RuntimeObject
{
public:
	// System.Int32 TriLib.Samples.DownloadSample_<DownloadFile>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TriLib.Samples.DownloadSample_<DownloadFile>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TriLib.Samples.DownloadSample TriLib.Samples.DownloadSample_<DownloadFile>d__7::<>4__this
	DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB * ___U3CU3E4__this_2;
	// System.Int32 TriLib.Samples.DownloadSample_<DownloadFile>d__7::index
	int32_t ___index_3;
	// System.String TriLib.Samples.DownloadSample_<DownloadFile>d__7::url
	String_t* ___url_4;
	// System.String TriLib.Samples.DownloadSample_<DownloadFile>d__7::fileExtension
	String_t* ___fileExtension_5;
	// System.String TriLib.Samples.DownloadSample_<DownloadFile>d__7::localFilePath
	String_t* ___localFilePath_6;
	// System.String TriLib.Samples.DownloadSample_<DownloadFile>d__7::localFilename
	String_t* ___localFilename_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___U3CU3E4__this_2)); }
	inline DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier((&___url_4), value);
	}

	inline static int32_t get_offset_of_fileExtension_5() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___fileExtension_5)); }
	inline String_t* get_fileExtension_5() const { return ___fileExtension_5; }
	inline String_t** get_address_of_fileExtension_5() { return &___fileExtension_5; }
	inline void set_fileExtension_5(String_t* value)
	{
		___fileExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___fileExtension_5), value);
	}

	inline static int32_t get_offset_of_localFilePath_6() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___localFilePath_6)); }
	inline String_t* get_localFilePath_6() const { return ___localFilePath_6; }
	inline String_t** get_address_of_localFilePath_6() { return &___localFilePath_6; }
	inline void set_localFilePath_6(String_t* value)
	{
		___localFilePath_6 = value;
		Il2CppCodeGenWriteBarrier((&___localFilePath_6), value);
	}

	inline static int32_t get_offset_of_localFilename_7() { return static_cast<int32_t>(offsetof(U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59, ___localFilename_7)); }
	inline String_t* get_localFilename_7() const { return ___localFilename_7; }
	inline String_t** get_address_of_localFilename_7() { return &___localFilename_7; }
	inline void set_localFilename_7(String_t* value)
	{
		___localFilename_7 = value;
		Il2CppCodeGenWriteBarrier((&___localFilename_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADFILEU3ED__7_TDCA38118765851B26AA54908C14315389F287B59_H
#ifndef U3CU3EC_TADF8CFFC690C095BEE3DC37382E3599D633BCC22_H
#define U3CU3EC_TADF8CFFC690C095BEE3DC37382E3599D633BCC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.LoadSampleAsync_<>c
struct  U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields
{
public:
	// TriLib.Samples.LoadSampleAsync_<>c TriLib.Samples.LoadSampleAsync_<>c::<>9
	U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22 * ___U3CU3E9_0;
	// TriLib.ObjectLoadedHandle TriLib.Samples.LoadSampleAsync_<>c::<>9__0_0
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields, ___U3CU3E9__0_0_1)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TADF8CFFC690C095BEE3DC37382E3599D633BCC22_H
#ifndef STREAMUTILS_TBD2306C4BBE7C954429B4056BC1AD309BE4BF5EF_H
#define STREAMUTILS_TBD2306C4BBE7C954429B4056BC1AD309BE4BF5EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StreamUtils
struct  StreamUtils_tBD2306C4BBE7C954429B4056BC1AD309BE4BF5EF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMUTILS_TBD2306C4BBE7C954429B4056BC1AD309BE4BF5EF_H
#ifndef STRINGUTILS_T456B1DFB8084650B9F77922AB3FE024B7AF365CF_H
#define STRINGUTILS_T456B1DFB8084650B9F77922AB3FE024B7AF365CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StringUtils
struct  StringUtils_t456B1DFB8084650B9F77922AB3FE024B7AF365CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T456B1DFB8084650B9F77922AB3FE024B7AF365CF_H
#ifndef TEXTURE2DUTILS_T113F1FD32D931B6C889F535639B2636DFFBC65DD_H
#define TEXTURE2DUTILS_T113F1FD32D931B6C889F535639B2636DFFBC65DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Texture2DUtils
struct  Texture2DUtils_t113F1FD32D931B6C889F535639B2636DFFBC65DD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2DUTILS_T113F1FD32D931B6C889F535639B2636DFFBC65DD_H
#ifndef THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#define THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils
struct  ThreadUtils_tC54E980B81AFB99EAA5C04548985934AD90B7C49  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADUTILS_TC54E980B81AFB99EAA5C04548985934AD90B7C49_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#define U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7  : public RuntimeObject
{
public:
	// System.Action TriLib.ThreadUtils_<>c__DisplayClass0_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;
	// System.Action TriLib.ThreadUtils_<>c__DisplayClass0_0::onComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7, ___onComplete_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_TEF97B9BE44762A00375B06496F40CDADCE95BCC7_H
#ifndef U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#define U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_<>c__DisplayClass0_1
struct  U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68  : public RuntimeObject
{
public:
	// System.Exception TriLib.ThreadUtils_<>c__DisplayClass0_1::exception
	Exception_t * ___exception_0;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68, ___exception_0)); }
	inline Exception_t * get_exception_0() const { return ___exception_0; }
	inline Exception_t ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier((&___exception_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_1_TE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68_H
#ifndef TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#define TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_tF34DF4728FF207B44EF4E4213DB8F391CD4CAABE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_TF34DF4728FF207B44EF4E4213DB8F391CD4CAABE_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BROWSERLOADDATA_T1DD62A7BC54412F7DEE84D00B45C0B44205863A6_H
#define BROWSERLOADDATA_T1DD62A7BC54412F7DEE84D00B45C0B44205863A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.BrowserLoadData
struct  BrowserLoadData_t1DD62A7BC54412F7DEE84D00B45C0B44205863A6  : public FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7
{
public:
	// System.Int32 TriLib.BrowserLoadData::FilesCount
	int32_t ___FilesCount_2;

public:
	inline static int32_t get_offset_of_FilesCount_2() { return static_cast<int32_t>(offsetof(BrowserLoadData_t1DD62A7BC54412F7DEE84D00B45C0B44205863A6, ___FilesCount_2)); }
	inline int32_t get_FilesCount_2() const { return ___FilesCount_2; }
	inline int32_t* get_address_of_FilesCount_2() { return &___FilesCount_2; }
	inline void set_FilesCount_2(int32_t value)
	{
		___FilesCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROWSERLOADDATA_T1DD62A7BC54412F7DEE84D00B45C0B44205863A6_H
#ifndef GCFILELOADDATA_T4429AD2365C86C04ADF085BF969AD7A2CDBB7485_H
#define GCFILELOADDATA_T4429AD2365C86C04ADF085BF969AD7A2CDBB7485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.GCFileLoadData
struct  GCFileLoadData_t4429AD2365C86C04ADF085BF969AD7A2CDBB7485  : public FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7
{
public:
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> TriLib.GCFileLoadData::_lockedBuffers
	List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * ____lockedBuffers_2;

public:
	inline static int32_t get_offset_of__lockedBuffers_2() { return static_cast<int32_t>(offsetof(GCFileLoadData_t4429AD2365C86C04ADF085BF969AD7A2CDBB7485, ____lockedBuffers_2)); }
	inline List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * get__lockedBuffers_2() const { return ____lockedBuffers_2; }
	inline List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E ** get_address_of__lockedBuffers_2() { return &____lockedBuffers_2; }
	inline void set__lockedBuffers_2(List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * value)
	{
		____lockedBuffers_2 = value;
		Il2CppCodeGenWriteBarrier((&____lockedBuffers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCFILELOADDATA_T4429AD2365C86C04ADF085BF969AD7A2CDBB7485_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#define UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef AICOMPONENT_TB270F1F381D082C09A5DF63FF716C7879BD7DB65_H
#define AICOMPONENT_TB270F1F381D082C09A5DF63FF716C7879BD7DB65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AiComponent
struct  AiComponent_tB270F1F381D082C09A5DF63FF716C7879BD7DB65 
{
public:
	// System.Int32 TriLib.AiComponent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AiComponent_tB270F1F381D082C09A5DF63FF716C7879BD7DB65, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICOMPONENT_TB270F1F381D082C09A5DF63FF716C7879BD7DB65_H
#ifndef AIPRIMITIVETYPE_T495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF_H
#define AIPRIMITIVETYPE_T495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AiPrimitiveType
struct  AiPrimitiveType_t495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF 
{
public:
	// System.Int32 TriLib.AiPrimitiveType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AiPrimitiveType_t495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIPRIMITIVETYPE_T495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF_H
#ifndef AIUVTRANSFORM_T2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF_H
#define AIUVTRANSFORM_T2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AiUVTransform
struct  AiUVTransform_t2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF 
{
public:
	// System.Int32 TriLib.AiUVTransform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AiUVTransform_t2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIUVTRANSFORM_T2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF_H
#ifndef ASSETADVANCEDCONFIG_T96547C7010BD6581D388F25556CADCB259E04B33_H
#define ASSETADVANCEDCONFIG_T96547C7010BD6581D388F25556CADCB259E04B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedConfig
struct  AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33  : public RuntimeObject
{
public:
	// System.String TriLib.AssetAdvancedConfig::Key
	String_t* ___Key_0;
	// System.Int32 TriLib.AssetAdvancedConfig::IntValue
	int32_t ___IntValue_1;
	// System.Single TriLib.AssetAdvancedConfig::FloatValue
	float ___FloatValue_2;
	// System.Boolean TriLib.AssetAdvancedConfig::BoolValue
	bool ___BoolValue_3;
	// System.String TriLib.AssetAdvancedConfig::StringValue
	String_t* ___StringValue_4;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::TranslationValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___TranslationValue_5;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::RotationValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationValue_6;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::ScaleValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ScaleValue_7;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_IntValue_1() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___IntValue_1)); }
	inline int32_t get_IntValue_1() const { return ___IntValue_1; }
	inline int32_t* get_address_of_IntValue_1() { return &___IntValue_1; }
	inline void set_IntValue_1(int32_t value)
	{
		___IntValue_1 = value;
	}

	inline static int32_t get_offset_of_FloatValue_2() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___FloatValue_2)); }
	inline float get_FloatValue_2() const { return ___FloatValue_2; }
	inline float* get_address_of_FloatValue_2() { return &___FloatValue_2; }
	inline void set_FloatValue_2(float value)
	{
		___FloatValue_2 = value;
	}

	inline static int32_t get_offset_of_BoolValue_3() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___BoolValue_3)); }
	inline bool get_BoolValue_3() const { return ___BoolValue_3; }
	inline bool* get_address_of_BoolValue_3() { return &___BoolValue_3; }
	inline void set_BoolValue_3(bool value)
	{
		___BoolValue_3 = value;
	}

	inline static int32_t get_offset_of_StringValue_4() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___StringValue_4)); }
	inline String_t* get_StringValue_4() const { return ___StringValue_4; }
	inline String_t** get_address_of_StringValue_4() { return &___StringValue_4; }
	inline void set_StringValue_4(String_t* value)
	{
		___StringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___StringValue_4), value);
	}

	inline static int32_t get_offset_of_TranslationValue_5() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___TranslationValue_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_TranslationValue_5() const { return ___TranslationValue_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_TranslationValue_5() { return &___TranslationValue_5; }
	inline void set_TranslationValue_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___TranslationValue_5 = value;
	}

	inline static int32_t get_offset_of_RotationValue_6() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___RotationValue_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationValue_6() const { return ___RotationValue_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationValue_6() { return &___RotationValue_6; }
	inline void set_RotationValue_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationValue_6 = value;
	}

	inline static int32_t get_offset_of_ScaleValue_7() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33, ___ScaleValue_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ScaleValue_7() const { return ___ScaleValue_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ScaleValue_7() { return &___ScaleValue_7; }
	inline void set_ScaleValue_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ScaleValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDCONFIG_T96547C7010BD6581D388F25556CADCB259E04B33_H
#ifndef ASSETADVANCEDCONFIGTYPE_T3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5_H
#define ASSETADVANCEDCONFIGTYPE_T3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedConfigType
struct  AssetAdvancedConfigType_t3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5 
{
public:
	// System.Int32 TriLib.AssetAdvancedConfigType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssetAdvancedConfigType_t3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDCONFIGTYPE_T3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5_H
#ifndef ASSETADVANCEDPROPERTYCLASSNAMES_T9BB3EAEF233946A026307A628D5522C0D37148D1_H
#define ASSETADVANCEDPROPERTYCLASSNAMES_T9BB3EAEF233946A026307A628D5522C0D37148D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedPropertyClassNames
struct  AssetAdvancedPropertyClassNames_t9BB3EAEF233946A026307A628D5522C0D37148D1 
{
public:
	// System.Int32 TriLib.AssetAdvancedPropertyClassNames::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssetAdvancedPropertyClassNames_t9BB3EAEF233946A026307A628D5522C0D37148D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDPROPERTYCLASSNAMES_T9BB3EAEF233946A026307A628D5522C0D37148D1_H
#ifndef ASSETLOADERBASE_TF7076F38295F492120A3F5B0F0161C9CC304FA88_H
#define ASSETLOADERBASE_TF7076F38295F492120A3F5B0F0161C9CC304FA88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderBase
struct  AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88  : public RuntimeObject
{
public:
	// TriLib.NodeData TriLib.AssetLoaderBase::RootNodeData
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * ___RootNodeData_1;
	// TriLib.MaterialData[] TriLib.AssetLoaderBase::MaterialData
	MaterialDataU5BU5D_t589D742C36C332ABE56095676D7DC09D7DF1318B* ___MaterialData_2;
	// TriLib.MeshData[] TriLib.AssetLoaderBase::MeshData
	MeshDataU5BU5D_tC038125A593483F889BD1D9864EE3AE72C2B6123* ___MeshData_3;
	// TriLib.AnimationData[] TriLib.AssetLoaderBase::AnimationData
	AnimationDataU5BU5D_tCFB8D99D455859922FB47D3B3BC4E589DCBA21E4* ___AnimationData_4;
	// TriLib.CameraData[] TriLib.AssetLoaderBase::CameraData
	CameraDataU5BU5D_t64F46362AD8787B81B58757F1766A70643B25125* ___CameraData_5;
	// TriLib.AssimpMetadata[] TriLib.AssetLoaderBase::Metadata
	AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* ___Metadata_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TriLib.AssetLoaderBase::NodesPath
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___NodesPath_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> TriLib.AssetLoaderBase::LoadedMaterials
	Dictionary_2_t29EF377760EAE2E4C34F7125BB6CDFE3AE8985A1 * ___LoadedMaterials_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> TriLib.AssetLoaderBase::LoadedTextures
	Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * ___LoadedTextures_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.SkinnedMeshRenderer,System.Collections.Generic.IList`1<System.String>> TriLib.AssetLoaderBase::LoadedBoneNames
	Dictionary_2_t5F16EB360AF0F2B45D4A8DA0506983605AA37047 * ___LoadedBoneNames_10;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.MeshData> TriLib.AssetLoaderBase::MeshDataConnections
	Dictionary_2_t5A56F61CDD731FF7DE1B774ECFE2F31750831F70 * ___MeshDataConnections_11;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.EmbeddedTextureData> TriLib.AssetLoaderBase::EmbeddedTextures
	Dictionary_2_t65D495315C64780D636CAB3892AA6E08C3ABD701 * ___EmbeddedTextures_12;
	// System.UInt32 TriLib.AssetLoaderBase::NodeId
	uint32_t ___NodeId_27;
	// System.Boolean TriLib.AssetLoaderBase::HasBoneInfo
	bool ___HasBoneInfo_28;
	// System.Boolean TriLib.AssetLoaderBase::HasBlendShapes
	bool ___HasBlendShapes_29;
	// System.IntPtr TriLib.AssetLoaderBase::Scene
	intptr_t ___Scene_30;
	// TriLib.EmbeddedTextureLoadCallback TriLib.AssetLoaderBase::EmbeddedTextureLoad
	EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213 * ___EmbeddedTextureLoad_31;
	// TriLib.MeshCreatedHandle TriLib.AssetLoaderBase::OnMeshCreated
	MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5 * ___OnMeshCreated_32;
	// TriLib.MaterialCreatedHandle TriLib.AssetLoaderBase::OnMaterialCreated
	MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541 * ___OnMaterialCreated_33;
	// TriLib.TextureLoadHandle TriLib.AssetLoaderBase::OnTextureLoaded
	TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * ___OnTextureLoaded_34;
	// TriLib.AvatarCreatedHandle TriLib.AssetLoaderBase::OnAvatarCreated
	AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2 * ___OnAvatarCreated_35;
	// TriLib.AnimationClipCreatedHandle TriLib.AssetLoaderBase::OnAnimationClipCreated
	AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20 * ___OnAnimationClipCreated_36;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderBase::OnObjectLoaded
	ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * ___OnObjectLoaded_37;
	// TriLib.MetadataProcessedHandle TriLib.AssetLoaderBase::OnMetadataProcessed
	MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A * ___OnMetadataProcessed_38;
	// TriLib.BlendShapeKeyCreatedHandle TriLib.AssetLoaderBase::OnBlendShapeKeyCreated
	BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28 * ___OnBlendShapeKeyCreated_39;

public:
	inline static int32_t get_offset_of_RootNodeData_1() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___RootNodeData_1)); }
	inline NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * get_RootNodeData_1() const { return ___RootNodeData_1; }
	inline NodeData_t457462F69494965439509DD564F2B853F08F6FF9 ** get_address_of_RootNodeData_1() { return &___RootNodeData_1; }
	inline void set_RootNodeData_1(NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * value)
	{
		___RootNodeData_1 = value;
		Il2CppCodeGenWriteBarrier((&___RootNodeData_1), value);
	}

	inline static int32_t get_offset_of_MaterialData_2() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___MaterialData_2)); }
	inline MaterialDataU5BU5D_t589D742C36C332ABE56095676D7DC09D7DF1318B* get_MaterialData_2() const { return ___MaterialData_2; }
	inline MaterialDataU5BU5D_t589D742C36C332ABE56095676D7DC09D7DF1318B** get_address_of_MaterialData_2() { return &___MaterialData_2; }
	inline void set_MaterialData_2(MaterialDataU5BU5D_t589D742C36C332ABE56095676D7DC09D7DF1318B* value)
	{
		___MaterialData_2 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialData_2), value);
	}

	inline static int32_t get_offset_of_MeshData_3() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___MeshData_3)); }
	inline MeshDataU5BU5D_tC038125A593483F889BD1D9864EE3AE72C2B6123* get_MeshData_3() const { return ___MeshData_3; }
	inline MeshDataU5BU5D_tC038125A593483F889BD1D9864EE3AE72C2B6123** get_address_of_MeshData_3() { return &___MeshData_3; }
	inline void set_MeshData_3(MeshDataU5BU5D_tC038125A593483F889BD1D9864EE3AE72C2B6123* value)
	{
		___MeshData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MeshData_3), value);
	}

	inline static int32_t get_offset_of_AnimationData_4() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___AnimationData_4)); }
	inline AnimationDataU5BU5D_tCFB8D99D455859922FB47D3B3BC4E589DCBA21E4* get_AnimationData_4() const { return ___AnimationData_4; }
	inline AnimationDataU5BU5D_tCFB8D99D455859922FB47D3B3BC4E589DCBA21E4** get_address_of_AnimationData_4() { return &___AnimationData_4; }
	inline void set_AnimationData_4(AnimationDataU5BU5D_tCFB8D99D455859922FB47D3B3BC4E589DCBA21E4* value)
	{
		___AnimationData_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationData_4), value);
	}

	inline static int32_t get_offset_of_CameraData_5() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___CameraData_5)); }
	inline CameraDataU5BU5D_t64F46362AD8787B81B58757F1766A70643B25125* get_CameraData_5() const { return ___CameraData_5; }
	inline CameraDataU5BU5D_t64F46362AD8787B81B58757F1766A70643B25125** get_address_of_CameraData_5() { return &___CameraData_5; }
	inline void set_CameraData_5(CameraDataU5BU5D_t64F46362AD8787B81B58757F1766A70643B25125* value)
	{
		___CameraData_5 = value;
		Il2CppCodeGenWriteBarrier((&___CameraData_5), value);
	}

	inline static int32_t get_offset_of_Metadata_6() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___Metadata_6)); }
	inline AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* get_Metadata_6() const { return ___Metadata_6; }
	inline AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E** get_address_of_Metadata_6() { return &___Metadata_6; }
	inline void set_Metadata_6(AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* value)
	{
		___Metadata_6 = value;
		Il2CppCodeGenWriteBarrier((&___Metadata_6), value);
	}

	inline static int32_t get_offset_of_NodesPath_7() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___NodesPath_7)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_NodesPath_7() const { return ___NodesPath_7; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_NodesPath_7() { return &___NodesPath_7; }
	inline void set_NodesPath_7(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___NodesPath_7 = value;
		Il2CppCodeGenWriteBarrier((&___NodesPath_7), value);
	}

	inline static int32_t get_offset_of_LoadedMaterials_8() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___LoadedMaterials_8)); }
	inline Dictionary_2_t29EF377760EAE2E4C34F7125BB6CDFE3AE8985A1 * get_LoadedMaterials_8() const { return ___LoadedMaterials_8; }
	inline Dictionary_2_t29EF377760EAE2E4C34F7125BB6CDFE3AE8985A1 ** get_address_of_LoadedMaterials_8() { return &___LoadedMaterials_8; }
	inline void set_LoadedMaterials_8(Dictionary_2_t29EF377760EAE2E4C34F7125BB6CDFE3AE8985A1 * value)
	{
		___LoadedMaterials_8 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedMaterials_8), value);
	}

	inline static int32_t get_offset_of_LoadedTextures_9() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___LoadedTextures_9)); }
	inline Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * get_LoadedTextures_9() const { return ___LoadedTextures_9; }
	inline Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 ** get_address_of_LoadedTextures_9() { return &___LoadedTextures_9; }
	inline void set_LoadedTextures_9(Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * value)
	{
		___LoadedTextures_9 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedTextures_9), value);
	}

	inline static int32_t get_offset_of_LoadedBoneNames_10() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___LoadedBoneNames_10)); }
	inline Dictionary_2_t5F16EB360AF0F2B45D4A8DA0506983605AA37047 * get_LoadedBoneNames_10() const { return ___LoadedBoneNames_10; }
	inline Dictionary_2_t5F16EB360AF0F2B45D4A8DA0506983605AA37047 ** get_address_of_LoadedBoneNames_10() { return &___LoadedBoneNames_10; }
	inline void set_LoadedBoneNames_10(Dictionary_2_t5F16EB360AF0F2B45D4A8DA0506983605AA37047 * value)
	{
		___LoadedBoneNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedBoneNames_10), value);
	}

	inline static int32_t get_offset_of_MeshDataConnections_11() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___MeshDataConnections_11)); }
	inline Dictionary_2_t5A56F61CDD731FF7DE1B774ECFE2F31750831F70 * get_MeshDataConnections_11() const { return ___MeshDataConnections_11; }
	inline Dictionary_2_t5A56F61CDD731FF7DE1B774ECFE2F31750831F70 ** get_address_of_MeshDataConnections_11() { return &___MeshDataConnections_11; }
	inline void set_MeshDataConnections_11(Dictionary_2_t5A56F61CDD731FF7DE1B774ECFE2F31750831F70 * value)
	{
		___MeshDataConnections_11 = value;
		Il2CppCodeGenWriteBarrier((&___MeshDataConnections_11), value);
	}

	inline static int32_t get_offset_of_EmbeddedTextures_12() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___EmbeddedTextures_12)); }
	inline Dictionary_2_t65D495315C64780D636CAB3892AA6E08C3ABD701 * get_EmbeddedTextures_12() const { return ___EmbeddedTextures_12; }
	inline Dictionary_2_t65D495315C64780D636CAB3892AA6E08C3ABD701 ** get_address_of_EmbeddedTextures_12() { return &___EmbeddedTextures_12; }
	inline void set_EmbeddedTextures_12(Dictionary_2_t65D495315C64780D636CAB3892AA6E08C3ABD701 * value)
	{
		___EmbeddedTextures_12 = value;
		Il2CppCodeGenWriteBarrier((&___EmbeddedTextures_12), value);
	}

	inline static int32_t get_offset_of_NodeId_27() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___NodeId_27)); }
	inline uint32_t get_NodeId_27() const { return ___NodeId_27; }
	inline uint32_t* get_address_of_NodeId_27() { return &___NodeId_27; }
	inline void set_NodeId_27(uint32_t value)
	{
		___NodeId_27 = value;
	}

	inline static int32_t get_offset_of_HasBoneInfo_28() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___HasBoneInfo_28)); }
	inline bool get_HasBoneInfo_28() const { return ___HasBoneInfo_28; }
	inline bool* get_address_of_HasBoneInfo_28() { return &___HasBoneInfo_28; }
	inline void set_HasBoneInfo_28(bool value)
	{
		___HasBoneInfo_28 = value;
	}

	inline static int32_t get_offset_of_HasBlendShapes_29() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___HasBlendShapes_29)); }
	inline bool get_HasBlendShapes_29() const { return ___HasBlendShapes_29; }
	inline bool* get_address_of_HasBlendShapes_29() { return &___HasBlendShapes_29; }
	inline void set_HasBlendShapes_29(bool value)
	{
		___HasBlendShapes_29 = value;
	}

	inline static int32_t get_offset_of_Scene_30() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___Scene_30)); }
	inline intptr_t get_Scene_30() const { return ___Scene_30; }
	inline intptr_t* get_address_of_Scene_30() { return &___Scene_30; }
	inline void set_Scene_30(intptr_t value)
	{
		___Scene_30 = value;
	}

	inline static int32_t get_offset_of_EmbeddedTextureLoad_31() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___EmbeddedTextureLoad_31)); }
	inline EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213 * get_EmbeddedTextureLoad_31() const { return ___EmbeddedTextureLoad_31; }
	inline EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213 ** get_address_of_EmbeddedTextureLoad_31() { return &___EmbeddedTextureLoad_31; }
	inline void set_EmbeddedTextureLoad_31(EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213 * value)
	{
		___EmbeddedTextureLoad_31 = value;
		Il2CppCodeGenWriteBarrier((&___EmbeddedTextureLoad_31), value);
	}

	inline static int32_t get_offset_of_OnMeshCreated_32() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnMeshCreated_32)); }
	inline MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5 * get_OnMeshCreated_32() const { return ___OnMeshCreated_32; }
	inline MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5 ** get_address_of_OnMeshCreated_32() { return &___OnMeshCreated_32; }
	inline void set_OnMeshCreated_32(MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5 * value)
	{
		___OnMeshCreated_32 = value;
		Il2CppCodeGenWriteBarrier((&___OnMeshCreated_32), value);
	}

	inline static int32_t get_offset_of_OnMaterialCreated_33() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnMaterialCreated_33)); }
	inline MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541 * get_OnMaterialCreated_33() const { return ___OnMaterialCreated_33; }
	inline MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541 ** get_address_of_OnMaterialCreated_33() { return &___OnMaterialCreated_33; }
	inline void set_OnMaterialCreated_33(MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541 * value)
	{
		___OnMaterialCreated_33 = value;
		Il2CppCodeGenWriteBarrier((&___OnMaterialCreated_33), value);
	}

	inline static int32_t get_offset_of_OnTextureLoaded_34() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnTextureLoaded_34)); }
	inline TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * get_OnTextureLoaded_34() const { return ___OnTextureLoaded_34; }
	inline TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C ** get_address_of_OnTextureLoaded_34() { return &___OnTextureLoaded_34; }
	inline void set_OnTextureLoaded_34(TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C * value)
	{
		___OnTextureLoaded_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnTextureLoaded_34), value);
	}

	inline static int32_t get_offset_of_OnAvatarCreated_35() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnAvatarCreated_35)); }
	inline AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2 * get_OnAvatarCreated_35() const { return ___OnAvatarCreated_35; }
	inline AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2 ** get_address_of_OnAvatarCreated_35() { return &___OnAvatarCreated_35; }
	inline void set_OnAvatarCreated_35(AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2 * value)
	{
		___OnAvatarCreated_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnAvatarCreated_35), value);
	}

	inline static int32_t get_offset_of_OnAnimationClipCreated_36() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnAnimationClipCreated_36)); }
	inline AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20 * get_OnAnimationClipCreated_36() const { return ___OnAnimationClipCreated_36; }
	inline AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20 ** get_address_of_OnAnimationClipCreated_36() { return &___OnAnimationClipCreated_36; }
	inline void set_OnAnimationClipCreated_36(AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20 * value)
	{
		___OnAnimationClipCreated_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnAnimationClipCreated_36), value);
	}

	inline static int32_t get_offset_of_OnObjectLoaded_37() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnObjectLoaded_37)); }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * get_OnObjectLoaded_37() const { return ___OnObjectLoaded_37; }
	inline ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B ** get_address_of_OnObjectLoaded_37() { return &___OnObjectLoaded_37; }
	inline void set_OnObjectLoaded_37(ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B * value)
	{
		___OnObjectLoaded_37 = value;
		Il2CppCodeGenWriteBarrier((&___OnObjectLoaded_37), value);
	}

	inline static int32_t get_offset_of_OnMetadataProcessed_38() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnMetadataProcessed_38)); }
	inline MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A * get_OnMetadataProcessed_38() const { return ___OnMetadataProcessed_38; }
	inline MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A ** get_address_of_OnMetadataProcessed_38() { return &___OnMetadataProcessed_38; }
	inline void set_OnMetadataProcessed_38(MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A * value)
	{
		___OnMetadataProcessed_38 = value;
		Il2CppCodeGenWriteBarrier((&___OnMetadataProcessed_38), value);
	}

	inline static int32_t get_offset_of_OnBlendShapeKeyCreated_39() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88, ___OnBlendShapeKeyCreated_39)); }
	inline BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28 * get_OnBlendShapeKeyCreated_39() const { return ___OnBlendShapeKeyCreated_39; }
	inline BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28 ** get_address_of_OnBlendShapeKeyCreated_39() { return &___OnBlendShapeKeyCreated_39; }
	inline void set_OnBlendShapeKeyCreated_39(BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28 * value)
	{
		___OnBlendShapeKeyCreated_39 = value;
		Il2CppCodeGenWriteBarrier((&___OnBlendShapeKeyCreated_39), value);
	}
};

struct AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields
{
public:
	// TriLib.ConcurrentList`1<TriLib.FileLoadData> TriLib.AssetLoaderBase::FilesLoadData
	ConcurrentList_1_tCD25539368BCC75EF5F876BFEB5CB322EE10F316 * ___FilesLoadData_13;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseMaterial_14;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularMaterial_15;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseAlphaMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseAlphaMaterial_16;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularAlphaMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularAlphaMaterial_17;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseCutoutMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseCutoutMaterial_18;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseFadeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseFadeMaterial_19;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularCutoutMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularCutoutMaterial_20;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularFadeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularFadeMaterial_21;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardRoughnessMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardRoughnessMaterial_22;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardRoughnessCutoutMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardRoughnessCutoutMaterial_23;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardRoughnessFadeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardRoughnessFadeMaterial_24;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardRoughnessAlphaMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardRoughnessAlphaMaterial_25;
	// UnityEngine.Texture2D TriLib.AssetLoaderBase::NotFoundTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___NotFoundTexture_26;

public:
	inline static int32_t get_offset_of_FilesLoadData_13() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___FilesLoadData_13)); }
	inline ConcurrentList_1_tCD25539368BCC75EF5F876BFEB5CB322EE10F316 * get_FilesLoadData_13() const { return ___FilesLoadData_13; }
	inline ConcurrentList_1_tCD25539368BCC75EF5F876BFEB5CB322EE10F316 ** get_address_of_FilesLoadData_13() { return &___FilesLoadData_13; }
	inline void set_FilesLoadData_13(ConcurrentList_1_tCD25539368BCC75EF5F876BFEB5CB322EE10F316 * value)
	{
		___FilesLoadData_13 = value;
		Il2CppCodeGenWriteBarrier((&___FilesLoadData_13), value);
	}

	inline static int32_t get_offset_of_StandardBaseMaterial_14() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardBaseMaterial_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseMaterial_14() const { return ___StandardBaseMaterial_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseMaterial_14() { return &___StandardBaseMaterial_14; }
	inline void set_StandardBaseMaterial_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseMaterial_14), value);
	}

	inline static int32_t get_offset_of_StandardSpecularMaterial_15() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardSpecularMaterial_15)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularMaterial_15() const { return ___StandardSpecularMaterial_15; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularMaterial_15() { return &___StandardSpecularMaterial_15; }
	inline void set_StandardSpecularMaterial_15(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularMaterial_15), value);
	}

	inline static int32_t get_offset_of_StandardBaseAlphaMaterial_16() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardBaseAlphaMaterial_16)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseAlphaMaterial_16() const { return ___StandardBaseAlphaMaterial_16; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseAlphaMaterial_16() { return &___StandardBaseAlphaMaterial_16; }
	inline void set_StandardBaseAlphaMaterial_16(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseAlphaMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseAlphaMaterial_16), value);
	}

	inline static int32_t get_offset_of_StandardSpecularAlphaMaterial_17() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardSpecularAlphaMaterial_17)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularAlphaMaterial_17() const { return ___StandardSpecularAlphaMaterial_17; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularAlphaMaterial_17() { return &___StandardSpecularAlphaMaterial_17; }
	inline void set_StandardSpecularAlphaMaterial_17(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularAlphaMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularAlphaMaterial_17), value);
	}

	inline static int32_t get_offset_of_StandardBaseCutoutMaterial_18() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardBaseCutoutMaterial_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseCutoutMaterial_18() const { return ___StandardBaseCutoutMaterial_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseCutoutMaterial_18() { return &___StandardBaseCutoutMaterial_18; }
	inline void set_StandardBaseCutoutMaterial_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseCutoutMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseCutoutMaterial_18), value);
	}

	inline static int32_t get_offset_of_StandardBaseFadeMaterial_19() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardBaseFadeMaterial_19)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseFadeMaterial_19() const { return ___StandardBaseFadeMaterial_19; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseFadeMaterial_19() { return &___StandardBaseFadeMaterial_19; }
	inline void set_StandardBaseFadeMaterial_19(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseFadeMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseFadeMaterial_19), value);
	}

	inline static int32_t get_offset_of_StandardSpecularCutoutMaterial_20() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardSpecularCutoutMaterial_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularCutoutMaterial_20() const { return ___StandardSpecularCutoutMaterial_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularCutoutMaterial_20() { return &___StandardSpecularCutoutMaterial_20; }
	inline void set_StandardSpecularCutoutMaterial_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularCutoutMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularCutoutMaterial_20), value);
	}

	inline static int32_t get_offset_of_StandardSpecularFadeMaterial_21() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardSpecularFadeMaterial_21)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularFadeMaterial_21() const { return ___StandardSpecularFadeMaterial_21; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularFadeMaterial_21() { return &___StandardSpecularFadeMaterial_21; }
	inline void set_StandardSpecularFadeMaterial_21(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularFadeMaterial_21 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularFadeMaterial_21), value);
	}

	inline static int32_t get_offset_of_StandardRoughnessMaterial_22() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardRoughnessMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardRoughnessMaterial_22() const { return ___StandardRoughnessMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardRoughnessMaterial_22() { return &___StandardRoughnessMaterial_22; }
	inline void set_StandardRoughnessMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardRoughnessMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___StandardRoughnessMaterial_22), value);
	}

	inline static int32_t get_offset_of_StandardRoughnessCutoutMaterial_23() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardRoughnessCutoutMaterial_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardRoughnessCutoutMaterial_23() const { return ___StandardRoughnessCutoutMaterial_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardRoughnessCutoutMaterial_23() { return &___StandardRoughnessCutoutMaterial_23; }
	inline void set_StandardRoughnessCutoutMaterial_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardRoughnessCutoutMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___StandardRoughnessCutoutMaterial_23), value);
	}

	inline static int32_t get_offset_of_StandardRoughnessFadeMaterial_24() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardRoughnessFadeMaterial_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardRoughnessFadeMaterial_24() const { return ___StandardRoughnessFadeMaterial_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardRoughnessFadeMaterial_24() { return &___StandardRoughnessFadeMaterial_24; }
	inline void set_StandardRoughnessFadeMaterial_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardRoughnessFadeMaterial_24 = value;
		Il2CppCodeGenWriteBarrier((&___StandardRoughnessFadeMaterial_24), value);
	}

	inline static int32_t get_offset_of_StandardRoughnessAlphaMaterial_25() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___StandardRoughnessAlphaMaterial_25)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardRoughnessAlphaMaterial_25() const { return ___StandardRoughnessAlphaMaterial_25; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardRoughnessAlphaMaterial_25() { return &___StandardRoughnessAlphaMaterial_25; }
	inline void set_StandardRoughnessAlphaMaterial_25(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardRoughnessAlphaMaterial_25 = value;
		Il2CppCodeGenWriteBarrier((&___StandardRoughnessAlphaMaterial_25), value);
	}

	inline static int32_t get_offset_of_NotFoundTexture_26() { return static_cast<int32_t>(offsetof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields, ___NotFoundTexture_26)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_NotFoundTexture_26() const { return ___NotFoundTexture_26; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_NotFoundTexture_26() { return &___NotFoundTexture_26; }
	inline void set_NotFoundTexture_26(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___NotFoundTexture_26 = value;
		Il2CppCodeGenWriteBarrier((&___NotFoundTexture_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERBASE_TF7076F38295F492120A3F5B0F0161C9CC304FA88_H
#ifndef ASSIMPMETADATATYPE_T254B012678393BA926711B6A551A19A27DFCD927_H
#define ASSIMPMETADATATYPE_T254B012678393BA926711B6A551A19A27DFCD927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadataType
struct  AssimpMetadataType_t254B012678393BA926711B6A551A19A27DFCD927 
{
public:
	// System.Int32 TriLib.AssimpMetadataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpMetadataType_t254B012678393BA926711B6A551A19A27DFCD927, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATATYPE_T254B012678393BA926711B6A551A19A27DFCD927_H
#ifndef ASSIMPPOSTPROCESSSTEPS_T320A3541BDF0C114643DD8A61E5A80E85C71E910_H
#define ASSIMPPOSTPROCESSSTEPS_T320A3541BDF0C114643DD8A61E5A80E85C71E910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpPostProcessSteps
struct  AssimpPostProcessSteps_t320A3541BDF0C114643DD8A61E5A80E85C71E910 
{
public:
	// System.Int32 TriLib.AssimpPostProcessSteps::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpPostProcessSteps_t320A3541BDF0C114643DD8A61E5A80E85C71E910, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPOSTPROCESSSTEPS_T320A3541BDF0C114643DD8A61E5A80E85C71E910_H
#ifndef BROWSERFILESLOADEDEVENT_T3D8D983456FC9D5949010D7885E2FE13B476E834_H
#define BROWSERFILESLOADEDEVENT_T3D8D983456FC9D5949010D7885E2FE13B476E834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.BrowserFilesLoadedEvent
struct  BrowserFilesLoadedEvent_t3D8D983456FC9D5949010D7885E2FE13B476E834  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROWSERFILESLOADEDEVENT_T3D8D983456FC9D5949010D7885E2FE13B476E834_H
#ifndef CAMERADATA_TE17C01D01F9B4BF95FE67D6D51607845C65F5D52_H
#define CAMERADATA_TE17C01D01F9B4BF95FE67D6D51607845C65F5D52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.CameraData
struct  CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52  : public RuntimeObject
{
public:
	// System.String TriLib.CameraData::Name
	String_t* ___Name_0;
	// System.Single TriLib.CameraData::Aspect
	float ___Aspect_1;
	// System.Single TriLib.CameraData::NearClipPlane
	float ___NearClipPlane_2;
	// System.Single TriLib.CameraData::FarClipPlane
	float ___FarClipPlane_3;
	// System.Single TriLib.CameraData::FieldOfView
	float ___FieldOfView_4;
	// UnityEngine.Vector3 TriLib.CameraData::LocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___LocalPosition_5;
	// UnityEngine.Vector3 TriLib.CameraData::Forward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_6;
	// UnityEngine.Vector3 TriLib.CameraData::Up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_7;
	// UnityEngine.Camera TriLib.CameraData::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_8;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Aspect_1() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___Aspect_1)); }
	inline float get_Aspect_1() const { return ___Aspect_1; }
	inline float* get_address_of_Aspect_1() { return &___Aspect_1; }
	inline void set_Aspect_1(float value)
	{
		___Aspect_1 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_2() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___NearClipPlane_2)); }
	inline float get_NearClipPlane_2() const { return ___NearClipPlane_2; }
	inline float* get_address_of_NearClipPlane_2() { return &___NearClipPlane_2; }
	inline void set_NearClipPlane_2(float value)
	{
		___NearClipPlane_2 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_3() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___FarClipPlane_3)); }
	inline float get_FarClipPlane_3() const { return ___FarClipPlane_3; }
	inline float* get_address_of_FarClipPlane_3() { return &___FarClipPlane_3; }
	inline void set_FarClipPlane_3(float value)
	{
		___FarClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_4() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___FieldOfView_4)); }
	inline float get_FieldOfView_4() const { return ___FieldOfView_4; }
	inline float* get_address_of_FieldOfView_4() { return &___FieldOfView_4; }
	inline void set_FieldOfView_4(float value)
	{
		___FieldOfView_4 = value;
	}

	inline static int32_t get_offset_of_LocalPosition_5() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___LocalPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_LocalPosition_5() const { return ___LocalPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_LocalPosition_5() { return &___LocalPosition_5; }
	inline void set_LocalPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___LocalPosition_5 = value;
	}

	inline static int32_t get_offset_of_Forward_6() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___Forward_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Forward_6() const { return ___Forward_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Forward_6() { return &___Forward_6; }
	inline void set_Forward_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Forward_6 = value;
	}

	inline static int32_t get_offset_of_Up_7() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___Up_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Up_7() const { return ___Up_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Up_7() { return &___Up_7; }
	inline void set_Up_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Up_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52, ___Camera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_8() const { return ___Camera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADATA_TE17C01D01F9B4BF95FE67D6D51607845C65F5D52_H
#ifndef EMBEDDEDTEXTUREDATA_TCDD936C843A64317194277997B78B728B35F9479_H
#define EMBEDDEDTEXTUREDATA_TCDD936C843A64317194277997B78B728B35F9479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureData
struct  EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479  : public RuntimeObject
{
public:
	// System.Byte[] TriLib.EmbeddedTextureData::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.IntPtr TriLib.EmbeddedTextureData::DataPointer
	intptr_t ___DataPointer_1;
	// System.Int32 TriLib.EmbeddedTextureData::DataLength
	int32_t ___DataLength_2;
	// TriLib.DataDisposalCallback TriLib.EmbeddedTextureData::OnDataDisposal
	DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B * ___OnDataDisposal_3;
	// System.Int32 TriLib.EmbeddedTextureData::Width
	int32_t ___Width_4;
	// System.Int32 TriLib.EmbeddedTextureData::Height
	int32_t ___Height_5;
	// System.Int32 TriLib.EmbeddedTextureData::NumChannels
	int32_t ___NumChannels_6;
	// System.Boolean TriLib.EmbeddedTextureData::IsRawData
	bool ___IsRawData_7;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_DataPointer_1() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___DataPointer_1)); }
	inline intptr_t get_DataPointer_1() const { return ___DataPointer_1; }
	inline intptr_t* get_address_of_DataPointer_1() { return &___DataPointer_1; }
	inline void set_DataPointer_1(intptr_t value)
	{
		___DataPointer_1 = value;
	}

	inline static int32_t get_offset_of_DataLength_2() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___DataLength_2)); }
	inline int32_t get_DataLength_2() const { return ___DataLength_2; }
	inline int32_t* get_address_of_DataLength_2() { return &___DataLength_2; }
	inline void set_DataLength_2(int32_t value)
	{
		___DataLength_2 = value;
	}

	inline static int32_t get_offset_of_OnDataDisposal_3() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___OnDataDisposal_3)); }
	inline DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B * get_OnDataDisposal_3() const { return ___OnDataDisposal_3; }
	inline DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B ** get_address_of_OnDataDisposal_3() { return &___OnDataDisposal_3; }
	inline void set_OnDataDisposal_3(DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B * value)
	{
		___OnDataDisposal_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataDisposal_3), value);
	}

	inline static int32_t get_offset_of_Width_4() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___Width_4)); }
	inline int32_t get_Width_4() const { return ___Width_4; }
	inline int32_t* get_address_of_Width_4() { return &___Width_4; }
	inline void set_Width_4(int32_t value)
	{
		___Width_4 = value;
	}

	inline static int32_t get_offset_of_Height_5() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___Height_5)); }
	inline int32_t get_Height_5() const { return ___Height_5; }
	inline int32_t* get_address_of_Height_5() { return &___Height_5; }
	inline void set_Height_5(int32_t value)
	{
		___Height_5 = value;
	}

	inline static int32_t get_offset_of_NumChannels_6() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___NumChannels_6)); }
	inline int32_t get_NumChannels_6() const { return ___NumChannels_6; }
	inline int32_t* get_address_of_NumChannels_6() { return &___NumChannels_6; }
	inline void set_NumChannels_6(int32_t value)
	{
		___NumChannels_6 = value;
	}

	inline static int32_t get_offset_of_IsRawData_7() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479, ___IsRawData_7)); }
	inline bool get_IsRawData_7() const { return ___IsRawData_7; }
	inline bool* get_address_of_IsRawData_7() { return &___IsRawData_7; }
	inline void set_IsRawData_7(bool value)
	{
		___IsRawData_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTUREDATA_TCDD936C843A64317194277997B78B728B35F9479_H
#ifndef MATERIALSHADINGMODE_T526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB_H
#define MATERIALSHADINGMODE_T526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialShadingMode
struct  MaterialShadingMode_t526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB 
{
public:
	// System.Int32 TriLib.MaterialShadingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialShadingMode_t526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSHADINGMODE_T526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB_H
#ifndef MATERIALTRANSPARENCYMODE_TA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C_H
#define MATERIALTRANSPARENCYMODE_TA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialTransparencyMode
struct  MaterialTransparencyMode_tA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C 
{
public:
	// System.Int32 TriLib.MaterialTransparencyMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialTransparencyMode_tA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALTRANSPARENCYMODE_TA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C_H
#ifndef NODEDATA_T457462F69494965439509DD564F2B853F08F6FF9_H
#define NODEDATA_T457462F69494965439509DD564F2B853F08F6FF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.NodeData
struct  NodeData_t457462F69494965439509DD564F2B853F08F6FF9  : public RuntimeObject
{
public:
	// System.String TriLib.NodeData::Name
	String_t* ___Name_0;
	// System.String TriLib.NodeData::Path
	String_t* ___Path_1;
	// UnityEngine.Matrix4x4 TriLib.NodeData::Matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___Matrix_2;
	// System.UInt32[] TriLib.NodeData::Meshes
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Meshes_3;
	// TriLib.NodeData TriLib.NodeData::Parent
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * ___Parent_4;
	// TriLib.NodeData[] TriLib.NodeData::Children
	NodeDataU5BU5D_tEA24D9ADF00D08B714D7A50E75160F20155731DD* ___Children_5;
	// UnityEngine.GameObject TriLib.NodeData::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_6;
	// TriLib.AssimpMetadata[] TriLib.NodeData::Metadata
	AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* ___Metadata_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Path_1() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Path_1)); }
	inline String_t* get_Path_1() const { return ___Path_1; }
	inline String_t** get_address_of_Path_1() { return &___Path_1; }
	inline void set_Path_1(String_t* value)
	{
		___Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___Path_1), value);
	}

	inline static int32_t get_offset_of_Matrix_2() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Matrix_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_Matrix_2() const { return ___Matrix_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_Matrix_2() { return &___Matrix_2; }
	inline void set_Matrix_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___Matrix_2 = value;
	}

	inline static int32_t get_offset_of_Meshes_3() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Meshes_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Meshes_3() const { return ___Meshes_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Meshes_3() { return &___Meshes_3; }
	inline void set_Meshes_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Meshes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Parent_4)); }
	inline NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * get_Parent_4() const { return ___Parent_4; }
	inline NodeData_t457462F69494965439509DD564F2B853F08F6FF9 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(NodeData_t457462F69494965439509DD564F2B853F08F6FF9 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_Children_5() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Children_5)); }
	inline NodeDataU5BU5D_tEA24D9ADF00D08B714D7A50E75160F20155731DD* get_Children_5() const { return ___Children_5; }
	inline NodeDataU5BU5D_tEA24D9ADF00D08B714D7A50E75160F20155731DD** get_address_of_Children_5() { return &___Children_5; }
	inline void set_Children_5(NodeDataU5BU5D_tEA24D9ADF00D08B714D7A50E75160F20155731DD* value)
	{
		___Children_5 = value;
		Il2CppCodeGenWriteBarrier((&___Children_5), value);
	}

	inline static int32_t get_offset_of_GameObject_6() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___GameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_6() const { return ___GameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_6() { return &___GameObject_6; }
	inline void set_GameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_6), value);
	}

	inline static int32_t get_offset_of_Metadata_7() { return static_cast<int32_t>(offsetof(NodeData_t457462F69494965439509DD564F2B853F08F6FF9, ___Metadata_7)); }
	inline AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* get_Metadata_7() const { return ___Metadata_7; }
	inline AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E** get_address_of_Metadata_7() { return &___Metadata_7; }
	inline void set_Metadata_7(AssimpMetadataU5BU5D_t9BD85EF836F6C1F003225A134EB58B7B8E104D3E* value)
	{
		___Metadata_7 = value;
		Il2CppCodeGenWriteBarrier((&___Metadata_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEDATA_T457462F69494965439509DD564F2B853F08F6FF9_H
#ifndef ITEMTYPE_T38E944E287660D759F6138EABFEBBC16B2E4D036_H
#define ITEMTYPE_T38E944E287660D759F6138EABFEBBC16B2E4D036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.ItemType
struct  ItemType_t38E944E287660D759F6138EABFEBBC16B2E4D036 
{
public:
	// System.Int32 TriLib.Samples.ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_t38E944E287660D759F6138EABFEBBC16B2E4D036, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_T38E944E287660D759F6138EABFEBBC16B2E4D036_H
#ifndef TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#define TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_TBE9654CBF04C930D6B3F935CAA06C2F569A514A5_H
#ifndef AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#define AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.aiPropertyTypeInfo
struct  aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF 
{
public:
	// System.Int32 TriLib.aiPropertyTypeInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIPROPERTYTYPEINFO_T1DC25105933923D11C343BB9EE10D64C4D7235AF_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#define TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifndef WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#define WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ANIMATIONDATA_T77CFD17222FBAB4128D5EEBA5FBB8F20928611C2_H
#define ANIMATIONDATA_T77CFD17222FBAB4128D5EEBA5FBB8F20928611C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationData
struct  AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2  : public RuntimeObject
{
public:
	// System.String TriLib.AnimationData::Name
	String_t* ___Name_0;
	// System.Boolean TriLib.AnimationData::Legacy
	bool ___Legacy_1;
	// System.Single TriLib.AnimationData::Length
	float ___Length_2;
	// System.Single TriLib.AnimationData::FrameRate
	float ___FrameRate_3;
	// UnityEngine.WrapMode TriLib.AnimationData::WrapMode
	int32_t ___WrapMode_4;
	// TriLib.AnimationChannelData[] TriLib.AnimationData::ChannelData
	AnimationChannelDataU5BU5D_tCDA1259454054C7B5A14E80302624001981625A7* ___ChannelData_5;
	// TriLib.MorphChannelData[] TriLib.AnimationData::MorphData
	MorphChannelDataU5BU5D_t9735809D7C5CE5DB25DAB909C918F9DAAD12F2EF* ___MorphData_6;
	// UnityEngine.AnimationClip TriLib.AnimationData::AnimationClip
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___AnimationClip_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Legacy_1() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___Legacy_1)); }
	inline bool get_Legacy_1() const { return ___Legacy_1; }
	inline bool* get_address_of_Legacy_1() { return &___Legacy_1; }
	inline void set_Legacy_1(bool value)
	{
		___Legacy_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___Length_2)); }
	inline float get_Length_2() const { return ___Length_2; }
	inline float* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(float value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_FrameRate_3() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___FrameRate_3)); }
	inline float get_FrameRate_3() const { return ___FrameRate_3; }
	inline float* get_address_of_FrameRate_3() { return &___FrameRate_3; }
	inline void set_FrameRate_3(float value)
	{
		___FrameRate_3 = value;
	}

	inline static int32_t get_offset_of_WrapMode_4() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___WrapMode_4)); }
	inline int32_t get_WrapMode_4() const { return ___WrapMode_4; }
	inline int32_t* get_address_of_WrapMode_4() { return &___WrapMode_4; }
	inline void set_WrapMode_4(int32_t value)
	{
		___WrapMode_4 = value;
	}

	inline static int32_t get_offset_of_ChannelData_5() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___ChannelData_5)); }
	inline AnimationChannelDataU5BU5D_tCDA1259454054C7B5A14E80302624001981625A7* get_ChannelData_5() const { return ___ChannelData_5; }
	inline AnimationChannelDataU5BU5D_tCDA1259454054C7B5A14E80302624001981625A7** get_address_of_ChannelData_5() { return &___ChannelData_5; }
	inline void set_ChannelData_5(AnimationChannelDataU5BU5D_tCDA1259454054C7B5A14E80302624001981625A7* value)
	{
		___ChannelData_5 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelData_5), value);
	}

	inline static int32_t get_offset_of_MorphData_6() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___MorphData_6)); }
	inline MorphChannelDataU5BU5D_t9735809D7C5CE5DB25DAB909C918F9DAAD12F2EF* get_MorphData_6() const { return ___MorphData_6; }
	inline MorphChannelDataU5BU5D_t9735809D7C5CE5DB25DAB909C918F9DAAD12F2EF** get_address_of_MorphData_6() { return &___MorphData_6; }
	inline void set_MorphData_6(MorphChannelDataU5BU5D_t9735809D7C5CE5DB25DAB909C918F9DAAD12F2EF* value)
	{
		___MorphData_6 = value;
		Il2CppCodeGenWriteBarrier((&___MorphData_6), value);
	}

	inline static int32_t get_offset_of_AnimationClip_7() { return static_cast<int32_t>(offsetof(AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2, ___AnimationClip_7)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_AnimationClip_7() const { return ___AnimationClip_7; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_AnimationClip_7() { return &___AnimationClip_7; }
	inline void set_AnimationClip_7(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___AnimationClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationClip_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONDATA_T77CFD17222FBAB4128D5EEBA5FBB8F20928611C2_H
#ifndef ASSETLOADER_T3E59CF528778B559EAF7F55C6E61A31D07DA917E_H
#define ASSETLOADER_T3E59CF528778B559EAF7F55C6E61A31D07DA917E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoader
struct  AssetLoader_t3E59CF528778B559EAF7F55C6E61A31D07DA917E  : public AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADER_T3E59CF528778B559EAF7F55C6E61A31D07DA917E_H
#ifndef ASSETLOADERASYNC_TF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40_H
#define ASSETLOADERASYNC_TF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync
struct  AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40  : public AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERASYNC_TF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40_H
#ifndef ASSETLOADERZIP_T5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0_H
#define ASSETLOADERZIP_T5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderZip
struct  AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0  : public AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERZIP_T5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0_H
#ifndef ASSIMPMETADATA_TB652CF827E7588BB133F5EEDC6D9E158E02756E6_H
#define ASSIMPMETADATA_TB652CF827E7588BB133F5EEDC6D9E158E02756E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadata
struct  AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6  : public RuntimeObject
{
public:
	// TriLib.AssimpMetadataType TriLib.AssimpMetadata::MetadataType
	int32_t ___MetadataType_0;
	// System.UInt32 TriLib.AssimpMetadata::MetadataIndex
	uint32_t ___MetadataIndex_1;
	// System.String TriLib.AssimpMetadata::MetadataKey
	String_t* ___MetadataKey_2;
	// System.Object TriLib.AssimpMetadata::MetadataValue
	RuntimeObject * ___MetadataValue_3;

public:
	inline static int32_t get_offset_of_MetadataType_0() { return static_cast<int32_t>(offsetof(AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6, ___MetadataType_0)); }
	inline int32_t get_MetadataType_0() const { return ___MetadataType_0; }
	inline int32_t* get_address_of_MetadataType_0() { return &___MetadataType_0; }
	inline void set_MetadataType_0(int32_t value)
	{
		___MetadataType_0 = value;
	}

	inline static int32_t get_offset_of_MetadataIndex_1() { return static_cast<int32_t>(offsetof(AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6, ___MetadataIndex_1)); }
	inline uint32_t get_MetadataIndex_1() const { return ___MetadataIndex_1; }
	inline uint32_t* get_address_of_MetadataIndex_1() { return &___MetadataIndex_1; }
	inline void set_MetadataIndex_1(uint32_t value)
	{
		___MetadataIndex_1 = value;
	}

	inline static int32_t get_offset_of_MetadataKey_2() { return static_cast<int32_t>(offsetof(AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6, ___MetadataKey_2)); }
	inline String_t* get_MetadataKey_2() const { return ___MetadataKey_2; }
	inline String_t** get_address_of_MetadataKey_2() { return &___MetadataKey_2; }
	inline void set_MetadataKey_2(String_t* value)
	{
		___MetadataKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataKey_2), value);
	}

	inline static int32_t get_offset_of_MetadataValue_3() { return static_cast<int32_t>(offsetof(AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6, ___MetadataValue_3)); }
	inline RuntimeObject * get_MetadataValue_3() const { return ___MetadataValue_3; }
	inline RuntimeObject ** get_address_of_MetadataValue_3() { return &___MetadataValue_3; }
	inline void set_MetadataValue_3(RuntimeObject * value)
	{
		___MetadataValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATA_TB652CF827E7588BB133F5EEDC6D9E158E02756E6_H
#ifndef ASSIMPPROCESSPRESET_T249D29621F5080E78F1BAAEFFB40AE7B50E067F1_H
#define ASSIMPPROCESSPRESET_T249D29621F5080E78F1BAAEFFB40AE7B50E067F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpProcessPreset
struct  AssimpProcessPreset_t249D29621F5080E78F1BAAEFFB40AE7B50E067F1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPROCESSPRESET_T249D29621F5080E78F1BAAEFFB40AE7B50E067F1_H
#ifndef MATERIALDATA_T26ED92447B6B9FB0B0A638637EC1E44445BBF41A_H
#define MATERIALDATA_T26ED92447B6B9FB0B0A638637EC1E44445BBF41A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialData
struct  MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A  : public RuntimeObject
{
public:
	// System.String TriLib.MaterialData::Name
	String_t* ___Name_0;
	// System.Boolean TriLib.MaterialData::AlphaLoaded
	bool ___AlphaLoaded_1;
	// System.Single TriLib.MaterialData::Alpha
	float ___Alpha_2;
	// System.Boolean TriLib.MaterialData::DiffuseInfoLoaded
	bool ___DiffuseInfoLoaded_3;
	// System.String TriLib.MaterialData::DiffusePath
	String_t* ___DiffusePath_4;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::DiffuseWrapMode
	int32_t ___DiffuseWrapMode_5;
	// System.String TriLib.MaterialData::DiffuseName
	String_t* ___DiffuseName_6;
	// System.Single TriLib.MaterialData::DiffuseBlendMode
	float ___DiffuseBlendMode_7;
	// System.UInt32 TriLib.MaterialData::DiffuseOp
	uint32_t ___DiffuseOp_8;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::DiffuseEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___DiffuseEmbeddedTextureData_9;
	// System.Boolean TriLib.MaterialData::DiffuseColorLoaded
	bool ___DiffuseColorLoaded_10;
	// UnityEngine.Color TriLib.MaterialData::DiffuseColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DiffuseColor_11;
	// System.Boolean TriLib.MaterialData::EmissionInfoLoaded
	bool ___EmissionInfoLoaded_12;
	// System.String TriLib.MaterialData::EmissionPath
	String_t* ___EmissionPath_13;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::EmissionWrapMode
	int32_t ___EmissionWrapMode_14;
	// System.String TriLib.MaterialData::EmissionName
	String_t* ___EmissionName_15;
	// System.Single TriLib.MaterialData::EmissionBlendMode
	float ___EmissionBlendMode_16;
	// System.UInt32 TriLib.MaterialData::EmissionOp
	uint32_t ___EmissionOp_17;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::EmissionEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___EmissionEmbeddedTextureData_18;
	// System.Boolean TriLib.MaterialData::EmissionColorLoaded
	bool ___EmissionColorLoaded_19;
	// UnityEngine.Color TriLib.MaterialData::EmissionColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___EmissionColor_20;
	// System.Boolean TriLib.MaterialData::SpecularInfoLoaded
	bool ___SpecularInfoLoaded_21;
	// System.String TriLib.MaterialData::SpecularPath
	String_t* ___SpecularPath_22;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::SpecularWrapMode
	int32_t ___SpecularWrapMode_23;
	// System.String TriLib.MaterialData::SpecularName
	String_t* ___SpecularName_24;
	// System.Single TriLib.MaterialData::SpecularBlendMode
	float ___SpecularBlendMode_25;
	// System.UInt32 TriLib.MaterialData::SpecularOp
	uint32_t ___SpecularOp_26;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::SpecularEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___SpecularEmbeddedTextureData_27;
	// System.Boolean TriLib.MaterialData::SpecularColorLoaded
	bool ___SpecularColorLoaded_28;
	// UnityEngine.Color TriLib.MaterialData::SpecularColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SpecularColor_29;
	// System.Boolean TriLib.MaterialData::NormalInfoLoaded
	bool ___NormalInfoLoaded_30;
	// System.String TriLib.MaterialData::NormalPath
	String_t* ___NormalPath_31;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::NormalWrapMode
	int32_t ___NormalWrapMode_32;
	// System.String TriLib.MaterialData::NormalName
	String_t* ___NormalName_33;
	// System.Single TriLib.MaterialData::NormalBlendMode
	float ___NormalBlendMode_34;
	// System.UInt32 TriLib.MaterialData::NormalOp
	uint32_t ___NormalOp_35;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::NormalEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___NormalEmbeddedTextureData_36;
	// System.Boolean TriLib.MaterialData::HeightInfoLoaded
	bool ___HeightInfoLoaded_37;
	// System.String TriLib.MaterialData::HeightPath
	String_t* ___HeightPath_38;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::HeightWrapMode
	int32_t ___HeightWrapMode_39;
	// System.String TriLib.MaterialData::HeightName
	String_t* ___HeightName_40;
	// System.Single TriLib.MaterialData::HeightBlendMode
	float ___HeightBlendMode_41;
	// System.UInt32 TriLib.MaterialData::HeightOp
	uint32_t ___HeightOp_42;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::HeightEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___HeightEmbeddedTextureData_43;
	// System.Boolean TriLib.MaterialData::BumpScaleLoaded
	bool ___BumpScaleLoaded_44;
	// System.Single TriLib.MaterialData::BumpScale
	float ___BumpScale_45;
	// System.Boolean TriLib.MaterialData::GlossinessLoaded
	bool ___GlossinessLoaded_46;
	// System.Single TriLib.MaterialData::Glossiness
	float ___Glossiness_47;
	// System.Boolean TriLib.MaterialData::GlossMapScaleLoaded
	bool ___GlossMapScaleLoaded_48;
	// System.Single TriLib.MaterialData::GlossMapScale
	float ___GlossMapScale_49;
	// System.Boolean TriLib.MaterialData::OcclusionInfoLoaded
	bool ___OcclusionInfoLoaded_50;
	// System.String TriLib.MaterialData::OcclusionPath
	String_t* ___OcclusionPath_51;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::OcclusionWrapMode
	int32_t ___OcclusionWrapMode_52;
	// System.String TriLib.MaterialData::OcclusionName
	String_t* ___OcclusionName_53;
	// System.Single TriLib.MaterialData::OcclusionBlendMode
	float ___OcclusionBlendMode_54;
	// System.UInt32 TriLib.MaterialData::OcclusionOp
	uint32_t ___OcclusionOp_55;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::OcclusionEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___OcclusionEmbeddedTextureData_56;
	// System.Boolean TriLib.MaterialData::MetallicInfoLoaded
	bool ___MetallicInfoLoaded_57;
	// System.String TriLib.MaterialData::MetallicPath
	String_t* ___MetallicPath_58;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::MetallicWrapMode
	int32_t ___MetallicWrapMode_59;
	// System.String TriLib.MaterialData::MetallicName
	String_t* ___MetallicName_60;
	// System.Single TriLib.MaterialData::MetallicBlendMode
	float ___MetallicBlendMode_61;
	// System.UInt32 TriLib.MaterialData::MetallicOp
	uint32_t ___MetallicOp_62;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::MetallicEmbeddedTextureData
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * ___MetallicEmbeddedTextureData_63;
	// TriLib.IMaterialProperty[] TriLib.MaterialData::Properties
	IMaterialPropertyU5BU5D_t5C53903294256A8B51A41B0A1E7CF0AD4C70F9C6* ___Properties_64;
	// UnityEngine.Material TriLib.MaterialData::Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_65;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_AlphaLoaded_1() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___AlphaLoaded_1)); }
	inline bool get_AlphaLoaded_1() const { return ___AlphaLoaded_1; }
	inline bool* get_address_of_AlphaLoaded_1() { return &___AlphaLoaded_1; }
	inline void set_AlphaLoaded_1(bool value)
	{
		___AlphaLoaded_1 = value;
	}

	inline static int32_t get_offset_of_Alpha_2() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___Alpha_2)); }
	inline float get_Alpha_2() const { return ___Alpha_2; }
	inline float* get_address_of_Alpha_2() { return &___Alpha_2; }
	inline void set_Alpha_2(float value)
	{
		___Alpha_2 = value;
	}

	inline static int32_t get_offset_of_DiffuseInfoLoaded_3() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseInfoLoaded_3)); }
	inline bool get_DiffuseInfoLoaded_3() const { return ___DiffuseInfoLoaded_3; }
	inline bool* get_address_of_DiffuseInfoLoaded_3() { return &___DiffuseInfoLoaded_3; }
	inline void set_DiffuseInfoLoaded_3(bool value)
	{
		___DiffuseInfoLoaded_3 = value;
	}

	inline static int32_t get_offset_of_DiffusePath_4() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffusePath_4)); }
	inline String_t* get_DiffusePath_4() const { return ___DiffusePath_4; }
	inline String_t** get_address_of_DiffusePath_4() { return &___DiffusePath_4; }
	inline void set_DiffusePath_4(String_t* value)
	{
		___DiffusePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___DiffusePath_4), value);
	}

	inline static int32_t get_offset_of_DiffuseWrapMode_5() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseWrapMode_5)); }
	inline int32_t get_DiffuseWrapMode_5() const { return ___DiffuseWrapMode_5; }
	inline int32_t* get_address_of_DiffuseWrapMode_5() { return &___DiffuseWrapMode_5; }
	inline void set_DiffuseWrapMode_5(int32_t value)
	{
		___DiffuseWrapMode_5 = value;
	}

	inline static int32_t get_offset_of_DiffuseName_6() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseName_6)); }
	inline String_t* get_DiffuseName_6() const { return ___DiffuseName_6; }
	inline String_t** get_address_of_DiffuseName_6() { return &___DiffuseName_6; }
	inline void set_DiffuseName_6(String_t* value)
	{
		___DiffuseName_6 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseName_6), value);
	}

	inline static int32_t get_offset_of_DiffuseBlendMode_7() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseBlendMode_7)); }
	inline float get_DiffuseBlendMode_7() const { return ___DiffuseBlendMode_7; }
	inline float* get_address_of_DiffuseBlendMode_7() { return &___DiffuseBlendMode_7; }
	inline void set_DiffuseBlendMode_7(float value)
	{
		___DiffuseBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_DiffuseOp_8() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseOp_8)); }
	inline uint32_t get_DiffuseOp_8() const { return ___DiffuseOp_8; }
	inline uint32_t* get_address_of_DiffuseOp_8() { return &___DiffuseOp_8; }
	inline void set_DiffuseOp_8(uint32_t value)
	{
		___DiffuseOp_8 = value;
	}

	inline static int32_t get_offset_of_DiffuseEmbeddedTextureData_9() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseEmbeddedTextureData_9)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_DiffuseEmbeddedTextureData_9() const { return ___DiffuseEmbeddedTextureData_9; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_DiffuseEmbeddedTextureData_9() { return &___DiffuseEmbeddedTextureData_9; }
	inline void set_DiffuseEmbeddedTextureData_9(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___DiffuseEmbeddedTextureData_9 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseEmbeddedTextureData_9), value);
	}

	inline static int32_t get_offset_of_DiffuseColorLoaded_10() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseColorLoaded_10)); }
	inline bool get_DiffuseColorLoaded_10() const { return ___DiffuseColorLoaded_10; }
	inline bool* get_address_of_DiffuseColorLoaded_10() { return &___DiffuseColorLoaded_10; }
	inline void set_DiffuseColorLoaded_10(bool value)
	{
		___DiffuseColorLoaded_10 = value;
	}

	inline static int32_t get_offset_of_DiffuseColor_11() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___DiffuseColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DiffuseColor_11() const { return ___DiffuseColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DiffuseColor_11() { return &___DiffuseColor_11; }
	inline void set_DiffuseColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DiffuseColor_11 = value;
	}

	inline static int32_t get_offset_of_EmissionInfoLoaded_12() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionInfoLoaded_12)); }
	inline bool get_EmissionInfoLoaded_12() const { return ___EmissionInfoLoaded_12; }
	inline bool* get_address_of_EmissionInfoLoaded_12() { return &___EmissionInfoLoaded_12; }
	inline void set_EmissionInfoLoaded_12(bool value)
	{
		___EmissionInfoLoaded_12 = value;
	}

	inline static int32_t get_offset_of_EmissionPath_13() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionPath_13)); }
	inline String_t* get_EmissionPath_13() const { return ___EmissionPath_13; }
	inline String_t** get_address_of_EmissionPath_13() { return &___EmissionPath_13; }
	inline void set_EmissionPath_13(String_t* value)
	{
		___EmissionPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionPath_13), value);
	}

	inline static int32_t get_offset_of_EmissionWrapMode_14() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionWrapMode_14)); }
	inline int32_t get_EmissionWrapMode_14() const { return ___EmissionWrapMode_14; }
	inline int32_t* get_address_of_EmissionWrapMode_14() { return &___EmissionWrapMode_14; }
	inline void set_EmissionWrapMode_14(int32_t value)
	{
		___EmissionWrapMode_14 = value;
	}

	inline static int32_t get_offset_of_EmissionName_15() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionName_15)); }
	inline String_t* get_EmissionName_15() const { return ___EmissionName_15; }
	inline String_t** get_address_of_EmissionName_15() { return &___EmissionName_15; }
	inline void set_EmissionName_15(String_t* value)
	{
		___EmissionName_15 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionName_15), value);
	}

	inline static int32_t get_offset_of_EmissionBlendMode_16() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionBlendMode_16)); }
	inline float get_EmissionBlendMode_16() const { return ___EmissionBlendMode_16; }
	inline float* get_address_of_EmissionBlendMode_16() { return &___EmissionBlendMode_16; }
	inline void set_EmissionBlendMode_16(float value)
	{
		___EmissionBlendMode_16 = value;
	}

	inline static int32_t get_offset_of_EmissionOp_17() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionOp_17)); }
	inline uint32_t get_EmissionOp_17() const { return ___EmissionOp_17; }
	inline uint32_t* get_address_of_EmissionOp_17() { return &___EmissionOp_17; }
	inline void set_EmissionOp_17(uint32_t value)
	{
		___EmissionOp_17 = value;
	}

	inline static int32_t get_offset_of_EmissionEmbeddedTextureData_18() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionEmbeddedTextureData_18)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_EmissionEmbeddedTextureData_18() const { return ___EmissionEmbeddedTextureData_18; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_EmissionEmbeddedTextureData_18() { return &___EmissionEmbeddedTextureData_18; }
	inline void set_EmissionEmbeddedTextureData_18(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___EmissionEmbeddedTextureData_18 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionEmbeddedTextureData_18), value);
	}

	inline static int32_t get_offset_of_EmissionColorLoaded_19() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionColorLoaded_19)); }
	inline bool get_EmissionColorLoaded_19() const { return ___EmissionColorLoaded_19; }
	inline bool* get_address_of_EmissionColorLoaded_19() { return &___EmissionColorLoaded_19; }
	inline void set_EmissionColorLoaded_19(bool value)
	{
		___EmissionColorLoaded_19 = value;
	}

	inline static int32_t get_offset_of_EmissionColor_20() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___EmissionColor_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_EmissionColor_20() const { return ___EmissionColor_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_EmissionColor_20() { return &___EmissionColor_20; }
	inline void set_EmissionColor_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___EmissionColor_20 = value;
	}

	inline static int32_t get_offset_of_SpecularInfoLoaded_21() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularInfoLoaded_21)); }
	inline bool get_SpecularInfoLoaded_21() const { return ___SpecularInfoLoaded_21; }
	inline bool* get_address_of_SpecularInfoLoaded_21() { return &___SpecularInfoLoaded_21; }
	inline void set_SpecularInfoLoaded_21(bool value)
	{
		___SpecularInfoLoaded_21 = value;
	}

	inline static int32_t get_offset_of_SpecularPath_22() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularPath_22)); }
	inline String_t* get_SpecularPath_22() const { return ___SpecularPath_22; }
	inline String_t** get_address_of_SpecularPath_22() { return &___SpecularPath_22; }
	inline void set_SpecularPath_22(String_t* value)
	{
		___SpecularPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularPath_22), value);
	}

	inline static int32_t get_offset_of_SpecularWrapMode_23() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularWrapMode_23)); }
	inline int32_t get_SpecularWrapMode_23() const { return ___SpecularWrapMode_23; }
	inline int32_t* get_address_of_SpecularWrapMode_23() { return &___SpecularWrapMode_23; }
	inline void set_SpecularWrapMode_23(int32_t value)
	{
		___SpecularWrapMode_23 = value;
	}

	inline static int32_t get_offset_of_SpecularName_24() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularName_24)); }
	inline String_t* get_SpecularName_24() const { return ___SpecularName_24; }
	inline String_t** get_address_of_SpecularName_24() { return &___SpecularName_24; }
	inline void set_SpecularName_24(String_t* value)
	{
		___SpecularName_24 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularName_24), value);
	}

	inline static int32_t get_offset_of_SpecularBlendMode_25() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularBlendMode_25)); }
	inline float get_SpecularBlendMode_25() const { return ___SpecularBlendMode_25; }
	inline float* get_address_of_SpecularBlendMode_25() { return &___SpecularBlendMode_25; }
	inline void set_SpecularBlendMode_25(float value)
	{
		___SpecularBlendMode_25 = value;
	}

	inline static int32_t get_offset_of_SpecularOp_26() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularOp_26)); }
	inline uint32_t get_SpecularOp_26() const { return ___SpecularOp_26; }
	inline uint32_t* get_address_of_SpecularOp_26() { return &___SpecularOp_26; }
	inline void set_SpecularOp_26(uint32_t value)
	{
		___SpecularOp_26 = value;
	}

	inline static int32_t get_offset_of_SpecularEmbeddedTextureData_27() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularEmbeddedTextureData_27)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_SpecularEmbeddedTextureData_27() const { return ___SpecularEmbeddedTextureData_27; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_SpecularEmbeddedTextureData_27() { return &___SpecularEmbeddedTextureData_27; }
	inline void set_SpecularEmbeddedTextureData_27(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___SpecularEmbeddedTextureData_27 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularEmbeddedTextureData_27), value);
	}

	inline static int32_t get_offset_of_SpecularColorLoaded_28() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularColorLoaded_28)); }
	inline bool get_SpecularColorLoaded_28() const { return ___SpecularColorLoaded_28; }
	inline bool* get_address_of_SpecularColorLoaded_28() { return &___SpecularColorLoaded_28; }
	inline void set_SpecularColorLoaded_28(bool value)
	{
		___SpecularColorLoaded_28 = value;
	}

	inline static int32_t get_offset_of_SpecularColor_29() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___SpecularColor_29)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SpecularColor_29() const { return ___SpecularColor_29; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SpecularColor_29() { return &___SpecularColor_29; }
	inline void set_SpecularColor_29(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SpecularColor_29 = value;
	}

	inline static int32_t get_offset_of_NormalInfoLoaded_30() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalInfoLoaded_30)); }
	inline bool get_NormalInfoLoaded_30() const { return ___NormalInfoLoaded_30; }
	inline bool* get_address_of_NormalInfoLoaded_30() { return &___NormalInfoLoaded_30; }
	inline void set_NormalInfoLoaded_30(bool value)
	{
		___NormalInfoLoaded_30 = value;
	}

	inline static int32_t get_offset_of_NormalPath_31() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalPath_31)); }
	inline String_t* get_NormalPath_31() const { return ___NormalPath_31; }
	inline String_t** get_address_of_NormalPath_31() { return &___NormalPath_31; }
	inline void set_NormalPath_31(String_t* value)
	{
		___NormalPath_31 = value;
		Il2CppCodeGenWriteBarrier((&___NormalPath_31), value);
	}

	inline static int32_t get_offset_of_NormalWrapMode_32() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalWrapMode_32)); }
	inline int32_t get_NormalWrapMode_32() const { return ___NormalWrapMode_32; }
	inline int32_t* get_address_of_NormalWrapMode_32() { return &___NormalWrapMode_32; }
	inline void set_NormalWrapMode_32(int32_t value)
	{
		___NormalWrapMode_32 = value;
	}

	inline static int32_t get_offset_of_NormalName_33() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalName_33)); }
	inline String_t* get_NormalName_33() const { return ___NormalName_33; }
	inline String_t** get_address_of_NormalName_33() { return &___NormalName_33; }
	inline void set_NormalName_33(String_t* value)
	{
		___NormalName_33 = value;
		Il2CppCodeGenWriteBarrier((&___NormalName_33), value);
	}

	inline static int32_t get_offset_of_NormalBlendMode_34() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalBlendMode_34)); }
	inline float get_NormalBlendMode_34() const { return ___NormalBlendMode_34; }
	inline float* get_address_of_NormalBlendMode_34() { return &___NormalBlendMode_34; }
	inline void set_NormalBlendMode_34(float value)
	{
		___NormalBlendMode_34 = value;
	}

	inline static int32_t get_offset_of_NormalOp_35() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalOp_35)); }
	inline uint32_t get_NormalOp_35() const { return ___NormalOp_35; }
	inline uint32_t* get_address_of_NormalOp_35() { return &___NormalOp_35; }
	inline void set_NormalOp_35(uint32_t value)
	{
		___NormalOp_35 = value;
	}

	inline static int32_t get_offset_of_NormalEmbeddedTextureData_36() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___NormalEmbeddedTextureData_36)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_NormalEmbeddedTextureData_36() const { return ___NormalEmbeddedTextureData_36; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_NormalEmbeddedTextureData_36() { return &___NormalEmbeddedTextureData_36; }
	inline void set_NormalEmbeddedTextureData_36(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___NormalEmbeddedTextureData_36 = value;
		Il2CppCodeGenWriteBarrier((&___NormalEmbeddedTextureData_36), value);
	}

	inline static int32_t get_offset_of_HeightInfoLoaded_37() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightInfoLoaded_37)); }
	inline bool get_HeightInfoLoaded_37() const { return ___HeightInfoLoaded_37; }
	inline bool* get_address_of_HeightInfoLoaded_37() { return &___HeightInfoLoaded_37; }
	inline void set_HeightInfoLoaded_37(bool value)
	{
		___HeightInfoLoaded_37 = value;
	}

	inline static int32_t get_offset_of_HeightPath_38() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightPath_38)); }
	inline String_t* get_HeightPath_38() const { return ___HeightPath_38; }
	inline String_t** get_address_of_HeightPath_38() { return &___HeightPath_38; }
	inline void set_HeightPath_38(String_t* value)
	{
		___HeightPath_38 = value;
		Il2CppCodeGenWriteBarrier((&___HeightPath_38), value);
	}

	inline static int32_t get_offset_of_HeightWrapMode_39() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightWrapMode_39)); }
	inline int32_t get_HeightWrapMode_39() const { return ___HeightWrapMode_39; }
	inline int32_t* get_address_of_HeightWrapMode_39() { return &___HeightWrapMode_39; }
	inline void set_HeightWrapMode_39(int32_t value)
	{
		___HeightWrapMode_39 = value;
	}

	inline static int32_t get_offset_of_HeightName_40() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightName_40)); }
	inline String_t* get_HeightName_40() const { return ___HeightName_40; }
	inline String_t** get_address_of_HeightName_40() { return &___HeightName_40; }
	inline void set_HeightName_40(String_t* value)
	{
		___HeightName_40 = value;
		Il2CppCodeGenWriteBarrier((&___HeightName_40), value);
	}

	inline static int32_t get_offset_of_HeightBlendMode_41() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightBlendMode_41)); }
	inline float get_HeightBlendMode_41() const { return ___HeightBlendMode_41; }
	inline float* get_address_of_HeightBlendMode_41() { return &___HeightBlendMode_41; }
	inline void set_HeightBlendMode_41(float value)
	{
		___HeightBlendMode_41 = value;
	}

	inline static int32_t get_offset_of_HeightOp_42() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightOp_42)); }
	inline uint32_t get_HeightOp_42() const { return ___HeightOp_42; }
	inline uint32_t* get_address_of_HeightOp_42() { return &___HeightOp_42; }
	inline void set_HeightOp_42(uint32_t value)
	{
		___HeightOp_42 = value;
	}

	inline static int32_t get_offset_of_HeightEmbeddedTextureData_43() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___HeightEmbeddedTextureData_43)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_HeightEmbeddedTextureData_43() const { return ___HeightEmbeddedTextureData_43; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_HeightEmbeddedTextureData_43() { return &___HeightEmbeddedTextureData_43; }
	inline void set_HeightEmbeddedTextureData_43(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___HeightEmbeddedTextureData_43 = value;
		Il2CppCodeGenWriteBarrier((&___HeightEmbeddedTextureData_43), value);
	}

	inline static int32_t get_offset_of_BumpScaleLoaded_44() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___BumpScaleLoaded_44)); }
	inline bool get_BumpScaleLoaded_44() const { return ___BumpScaleLoaded_44; }
	inline bool* get_address_of_BumpScaleLoaded_44() { return &___BumpScaleLoaded_44; }
	inline void set_BumpScaleLoaded_44(bool value)
	{
		___BumpScaleLoaded_44 = value;
	}

	inline static int32_t get_offset_of_BumpScale_45() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___BumpScale_45)); }
	inline float get_BumpScale_45() const { return ___BumpScale_45; }
	inline float* get_address_of_BumpScale_45() { return &___BumpScale_45; }
	inline void set_BumpScale_45(float value)
	{
		___BumpScale_45 = value;
	}

	inline static int32_t get_offset_of_GlossinessLoaded_46() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___GlossinessLoaded_46)); }
	inline bool get_GlossinessLoaded_46() const { return ___GlossinessLoaded_46; }
	inline bool* get_address_of_GlossinessLoaded_46() { return &___GlossinessLoaded_46; }
	inline void set_GlossinessLoaded_46(bool value)
	{
		___GlossinessLoaded_46 = value;
	}

	inline static int32_t get_offset_of_Glossiness_47() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___Glossiness_47)); }
	inline float get_Glossiness_47() const { return ___Glossiness_47; }
	inline float* get_address_of_Glossiness_47() { return &___Glossiness_47; }
	inline void set_Glossiness_47(float value)
	{
		___Glossiness_47 = value;
	}

	inline static int32_t get_offset_of_GlossMapScaleLoaded_48() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___GlossMapScaleLoaded_48)); }
	inline bool get_GlossMapScaleLoaded_48() const { return ___GlossMapScaleLoaded_48; }
	inline bool* get_address_of_GlossMapScaleLoaded_48() { return &___GlossMapScaleLoaded_48; }
	inline void set_GlossMapScaleLoaded_48(bool value)
	{
		___GlossMapScaleLoaded_48 = value;
	}

	inline static int32_t get_offset_of_GlossMapScale_49() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___GlossMapScale_49)); }
	inline float get_GlossMapScale_49() const { return ___GlossMapScale_49; }
	inline float* get_address_of_GlossMapScale_49() { return &___GlossMapScale_49; }
	inline void set_GlossMapScale_49(float value)
	{
		___GlossMapScale_49 = value;
	}

	inline static int32_t get_offset_of_OcclusionInfoLoaded_50() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionInfoLoaded_50)); }
	inline bool get_OcclusionInfoLoaded_50() const { return ___OcclusionInfoLoaded_50; }
	inline bool* get_address_of_OcclusionInfoLoaded_50() { return &___OcclusionInfoLoaded_50; }
	inline void set_OcclusionInfoLoaded_50(bool value)
	{
		___OcclusionInfoLoaded_50 = value;
	}

	inline static int32_t get_offset_of_OcclusionPath_51() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionPath_51)); }
	inline String_t* get_OcclusionPath_51() const { return ___OcclusionPath_51; }
	inline String_t** get_address_of_OcclusionPath_51() { return &___OcclusionPath_51; }
	inline void set_OcclusionPath_51(String_t* value)
	{
		___OcclusionPath_51 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionPath_51), value);
	}

	inline static int32_t get_offset_of_OcclusionWrapMode_52() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionWrapMode_52)); }
	inline int32_t get_OcclusionWrapMode_52() const { return ___OcclusionWrapMode_52; }
	inline int32_t* get_address_of_OcclusionWrapMode_52() { return &___OcclusionWrapMode_52; }
	inline void set_OcclusionWrapMode_52(int32_t value)
	{
		___OcclusionWrapMode_52 = value;
	}

	inline static int32_t get_offset_of_OcclusionName_53() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionName_53)); }
	inline String_t* get_OcclusionName_53() const { return ___OcclusionName_53; }
	inline String_t** get_address_of_OcclusionName_53() { return &___OcclusionName_53; }
	inline void set_OcclusionName_53(String_t* value)
	{
		___OcclusionName_53 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionName_53), value);
	}

	inline static int32_t get_offset_of_OcclusionBlendMode_54() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionBlendMode_54)); }
	inline float get_OcclusionBlendMode_54() const { return ___OcclusionBlendMode_54; }
	inline float* get_address_of_OcclusionBlendMode_54() { return &___OcclusionBlendMode_54; }
	inline void set_OcclusionBlendMode_54(float value)
	{
		___OcclusionBlendMode_54 = value;
	}

	inline static int32_t get_offset_of_OcclusionOp_55() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionOp_55)); }
	inline uint32_t get_OcclusionOp_55() const { return ___OcclusionOp_55; }
	inline uint32_t* get_address_of_OcclusionOp_55() { return &___OcclusionOp_55; }
	inline void set_OcclusionOp_55(uint32_t value)
	{
		___OcclusionOp_55 = value;
	}

	inline static int32_t get_offset_of_OcclusionEmbeddedTextureData_56() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___OcclusionEmbeddedTextureData_56)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_OcclusionEmbeddedTextureData_56() const { return ___OcclusionEmbeddedTextureData_56; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_OcclusionEmbeddedTextureData_56() { return &___OcclusionEmbeddedTextureData_56; }
	inline void set_OcclusionEmbeddedTextureData_56(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___OcclusionEmbeddedTextureData_56 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionEmbeddedTextureData_56), value);
	}

	inline static int32_t get_offset_of_MetallicInfoLoaded_57() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicInfoLoaded_57)); }
	inline bool get_MetallicInfoLoaded_57() const { return ___MetallicInfoLoaded_57; }
	inline bool* get_address_of_MetallicInfoLoaded_57() { return &___MetallicInfoLoaded_57; }
	inline void set_MetallicInfoLoaded_57(bool value)
	{
		___MetallicInfoLoaded_57 = value;
	}

	inline static int32_t get_offset_of_MetallicPath_58() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicPath_58)); }
	inline String_t* get_MetallicPath_58() const { return ___MetallicPath_58; }
	inline String_t** get_address_of_MetallicPath_58() { return &___MetallicPath_58; }
	inline void set_MetallicPath_58(String_t* value)
	{
		___MetallicPath_58 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicPath_58), value);
	}

	inline static int32_t get_offset_of_MetallicWrapMode_59() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicWrapMode_59)); }
	inline int32_t get_MetallicWrapMode_59() const { return ___MetallicWrapMode_59; }
	inline int32_t* get_address_of_MetallicWrapMode_59() { return &___MetallicWrapMode_59; }
	inline void set_MetallicWrapMode_59(int32_t value)
	{
		___MetallicWrapMode_59 = value;
	}

	inline static int32_t get_offset_of_MetallicName_60() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicName_60)); }
	inline String_t* get_MetallicName_60() const { return ___MetallicName_60; }
	inline String_t** get_address_of_MetallicName_60() { return &___MetallicName_60; }
	inline void set_MetallicName_60(String_t* value)
	{
		___MetallicName_60 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicName_60), value);
	}

	inline static int32_t get_offset_of_MetallicBlendMode_61() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicBlendMode_61)); }
	inline float get_MetallicBlendMode_61() const { return ___MetallicBlendMode_61; }
	inline float* get_address_of_MetallicBlendMode_61() { return &___MetallicBlendMode_61; }
	inline void set_MetallicBlendMode_61(float value)
	{
		___MetallicBlendMode_61 = value;
	}

	inline static int32_t get_offset_of_MetallicOp_62() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicOp_62)); }
	inline uint32_t get_MetallicOp_62() const { return ___MetallicOp_62; }
	inline uint32_t* get_address_of_MetallicOp_62() { return &___MetallicOp_62; }
	inline void set_MetallicOp_62(uint32_t value)
	{
		___MetallicOp_62 = value;
	}

	inline static int32_t get_offset_of_MetallicEmbeddedTextureData_63() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___MetallicEmbeddedTextureData_63)); }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * get_MetallicEmbeddedTextureData_63() const { return ___MetallicEmbeddedTextureData_63; }
	inline EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 ** get_address_of_MetallicEmbeddedTextureData_63() { return &___MetallicEmbeddedTextureData_63; }
	inline void set_MetallicEmbeddedTextureData_63(EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479 * value)
	{
		___MetallicEmbeddedTextureData_63 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicEmbeddedTextureData_63), value);
	}

	inline static int32_t get_offset_of_Properties_64() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___Properties_64)); }
	inline IMaterialPropertyU5BU5D_t5C53903294256A8B51A41B0A1E7CF0AD4C70F9C6* get_Properties_64() const { return ___Properties_64; }
	inline IMaterialPropertyU5BU5D_t5C53903294256A8B51A41B0A1E7CF0AD4C70F9C6** get_address_of_Properties_64() { return &___Properties_64; }
	inline void set_Properties_64(IMaterialPropertyU5BU5D_t5C53903294256A8B51A41B0A1E7CF0AD4C70F9C6* value)
	{
		___Properties_64 = value;
		Il2CppCodeGenWriteBarrier((&___Properties_64), value);
	}

	inline static int32_t get_offset_of_Material_65() { return static_cast<int32_t>(offsetof(MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A, ___Material_65)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Material_65() const { return ___Material_65; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Material_65() { return &___Material_65; }
	inline void set_Material_65(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Material_65 = value;
		Il2CppCodeGenWriteBarrier((&___Material_65), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALDATA_T26ED92447B6B9FB0B0A638637EC1E44445BBF41A_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ANIMATIONCLIPCREATEDHANDLE_T663B09E75EEE053CF2A6BB826C31485A0B69DC20_H
#define ANIMATIONCLIPCREATEDHANDLE_T663B09E75EEE053CF2A6BB826C31485A0B69DC20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationClipCreatedHandle
struct  AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPCREATEDHANDLE_T663B09E75EEE053CF2A6BB826C31485A0B69DC20_H
#ifndef ASSETLOADEROPTIONS_TC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B_H
#define ASSETLOADEROPTIONS_TC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderOptions
struct  AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean TriLib.AssetLoaderOptions::AddAssetUnloader
	bool ___AddAssetUnloader_4;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadAnimations
	bool ___DontLoadAnimations_5;
	// System.Boolean TriLib.AssetLoaderOptions::ForceAnimationComponents
	bool ___ForceAnimationComponents_6;
	// System.Boolean TriLib.AssetLoaderOptions::DontApplyAnimations
	bool ___DontApplyAnimations_7;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadLights
	bool ___DontLoadLights_8;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadCameras
	bool ___DontLoadCameras_9;
	// System.Boolean TriLib.AssetLoaderOptions::AutoPlayAnimations
	bool ___AutoPlayAnimations_10;
	// UnityEngine.WrapMode TriLib.AssetLoaderOptions::AnimationWrapMode
	int32_t ___AnimationWrapMode_11;
	// System.Boolean TriLib.AssetLoaderOptions::UseLegacyAnimations
	bool ___UseLegacyAnimations_12;
	// System.Boolean TriLib.AssetLoaderOptions::EnsureQuaternionContinuity
	bool ___EnsureQuaternionContinuity_13;
	// System.Boolean TriLib.AssetLoaderOptions::UseOriginalPositionRotationAndScale
	bool ___UseOriginalPositionRotationAndScale_14;
	// UnityEngine.RuntimeAnimatorController TriLib.AssetLoaderOptions::AnimatorController
	RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * ___AnimatorController_15;
	// UnityEngine.Avatar TriLib.AssetLoaderOptions::Avatar
	Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * ___Avatar_16;
	// System.Boolean TriLib.AssetLoaderOptions::DontGenerateAvatar
	bool ___DontGenerateAvatar_17;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMetadata
	bool ___DontLoadMetadata_18;
	// System.Boolean TriLib.AssetLoaderOptions::DontAddMetadataCollection
	bool ___DontAddMetadataCollection_19;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMaterials
	bool ___DontLoadMaterials_20;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyColorAlpha
	bool ___ApplyColorAlpha_21;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDiffuseColor
	bool ___ApplyDiffuseColor_22;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyEmissionColor
	bool ___ApplyEmissionColor_23;
	// System.Boolean TriLib.AssetLoaderOptions::ApplySpecularColor
	bool ___ApplySpecularColor_24;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDiffuseTexture
	bool ___ApplyDiffuseTexture_25;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyEmissionTexture
	bool ___ApplyEmissionTexture_26;
	// System.Boolean TriLib.AssetLoaderOptions::ApplySpecularTexture
	bool ___ApplySpecularTexture_27;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyNormalTexture
	bool ___ApplyNormalTexture_28;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDisplacementTexture
	bool ___ApplyDisplacementTexture_29;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyOcclusionTexture
	bool ___ApplyOcclusionTexture_30;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyMetallicTexture
	bool ___ApplyMetallicTexture_31;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyNormalScale
	bool ___ApplyNormalScale_32;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyGlossiness
	bool ___ApplyGlossiness_33;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyGlossinessScale
	bool ___ApplyGlossinessScale_34;
	// System.Boolean TriLib.AssetLoaderOptions::LoadRawMaterialProperties
	bool ___LoadRawMaterialProperties_35;
	// System.Boolean TriLib.AssetLoaderOptions::DisableAlphaMaterials
	bool ___DisableAlphaMaterials_36;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyAlphaMaterials
	bool ___ApplyAlphaMaterials_37;
	// System.Boolean TriLib.AssetLoaderOptions::ScanForAlphaMaterials
	bool ___ScanForAlphaMaterials_38;
	// System.Boolean TriLib.AssetLoaderOptions::UseCutoutMaterials
	bool ___UseCutoutMaterials_39;
	// System.Boolean TriLib.AssetLoaderOptions::UseStandardSpecularMaterial
	bool ___UseStandardSpecularMaterial_40;
	// TriLib.MaterialShadingMode TriLib.AssetLoaderOptions::MaterialShadingMode
	int32_t ___MaterialShadingMode_41;
	// TriLib.MaterialTransparencyMode TriLib.AssetLoaderOptions::MaterialTransparencyMode
	int32_t ___MaterialTransparencyMode_42;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMeshes
	bool ___DontLoadMeshes_43;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadBlendShapes
	bool ___DontLoadBlendShapes_44;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadSkinning
	bool ___DontLoadSkinning_45;
	// System.Boolean TriLib.AssetLoaderOptions::CombineMeshes
	bool ___CombineMeshes_46;
	// System.Boolean TriLib.AssetLoaderOptions::Use32BitsIndexFormat
	bool ___Use32BitsIndexFormat_47;
	// System.Boolean TriLib.AssetLoaderOptions::GenerateMeshColliders
	bool ___GenerateMeshColliders_48;
	// System.Boolean TriLib.AssetLoaderOptions::ConvexMeshColliders
	bool ___ConvexMeshColliders_49;
	// UnityEngine.Vector3 TriLib.AssetLoaderOptions::RotationAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationAngles_50;
	// System.Single TriLib.AssetLoaderOptions::Scale
	float ___Scale_51;
	// TriLib.AssimpPostProcessSteps TriLib.AssetLoaderOptions::PostProcessSteps
	int32_t ___PostProcessSteps_52;
	// TriLib.TextureCompression TriLib.AssetLoaderOptions::TextureCompression
	int32_t ___TextureCompression_53;
	// UnityEngine.FilterMode TriLib.AssetLoaderOptions::TextureFilterMode
	int32_t ___TextureFilterMode_54;
	// System.Boolean TriLib.AssetLoaderOptions::GenerateMipMaps
	bool ___GenerateMipMaps_55;
	// System.Collections.Generic.List`1<TriLib.AssetAdvancedConfig> TriLib.AssetLoaderOptions::AdvancedConfigs
	List_1_t53DF7B463C92E9A1A63552E2D7B286018D2EACAD * ___AdvancedConfigs_56;

public:
	inline static int32_t get_offset_of_AddAssetUnloader_4() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___AddAssetUnloader_4)); }
	inline bool get_AddAssetUnloader_4() const { return ___AddAssetUnloader_4; }
	inline bool* get_address_of_AddAssetUnloader_4() { return &___AddAssetUnloader_4; }
	inline void set_AddAssetUnloader_4(bool value)
	{
		___AddAssetUnloader_4 = value;
	}

	inline static int32_t get_offset_of_DontLoadAnimations_5() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadAnimations_5)); }
	inline bool get_DontLoadAnimations_5() const { return ___DontLoadAnimations_5; }
	inline bool* get_address_of_DontLoadAnimations_5() { return &___DontLoadAnimations_5; }
	inline void set_DontLoadAnimations_5(bool value)
	{
		___DontLoadAnimations_5 = value;
	}

	inline static int32_t get_offset_of_ForceAnimationComponents_6() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ForceAnimationComponents_6)); }
	inline bool get_ForceAnimationComponents_6() const { return ___ForceAnimationComponents_6; }
	inline bool* get_address_of_ForceAnimationComponents_6() { return &___ForceAnimationComponents_6; }
	inline void set_ForceAnimationComponents_6(bool value)
	{
		___ForceAnimationComponents_6 = value;
	}

	inline static int32_t get_offset_of_DontApplyAnimations_7() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontApplyAnimations_7)); }
	inline bool get_DontApplyAnimations_7() const { return ___DontApplyAnimations_7; }
	inline bool* get_address_of_DontApplyAnimations_7() { return &___DontApplyAnimations_7; }
	inline void set_DontApplyAnimations_7(bool value)
	{
		___DontApplyAnimations_7 = value;
	}

	inline static int32_t get_offset_of_DontLoadLights_8() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadLights_8)); }
	inline bool get_DontLoadLights_8() const { return ___DontLoadLights_8; }
	inline bool* get_address_of_DontLoadLights_8() { return &___DontLoadLights_8; }
	inline void set_DontLoadLights_8(bool value)
	{
		___DontLoadLights_8 = value;
	}

	inline static int32_t get_offset_of_DontLoadCameras_9() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadCameras_9)); }
	inline bool get_DontLoadCameras_9() const { return ___DontLoadCameras_9; }
	inline bool* get_address_of_DontLoadCameras_9() { return &___DontLoadCameras_9; }
	inline void set_DontLoadCameras_9(bool value)
	{
		___DontLoadCameras_9 = value;
	}

	inline static int32_t get_offset_of_AutoPlayAnimations_10() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___AutoPlayAnimations_10)); }
	inline bool get_AutoPlayAnimations_10() const { return ___AutoPlayAnimations_10; }
	inline bool* get_address_of_AutoPlayAnimations_10() { return &___AutoPlayAnimations_10; }
	inline void set_AutoPlayAnimations_10(bool value)
	{
		___AutoPlayAnimations_10 = value;
	}

	inline static int32_t get_offset_of_AnimationWrapMode_11() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___AnimationWrapMode_11)); }
	inline int32_t get_AnimationWrapMode_11() const { return ___AnimationWrapMode_11; }
	inline int32_t* get_address_of_AnimationWrapMode_11() { return &___AnimationWrapMode_11; }
	inline void set_AnimationWrapMode_11(int32_t value)
	{
		___AnimationWrapMode_11 = value;
	}

	inline static int32_t get_offset_of_UseLegacyAnimations_12() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___UseLegacyAnimations_12)); }
	inline bool get_UseLegacyAnimations_12() const { return ___UseLegacyAnimations_12; }
	inline bool* get_address_of_UseLegacyAnimations_12() { return &___UseLegacyAnimations_12; }
	inline void set_UseLegacyAnimations_12(bool value)
	{
		___UseLegacyAnimations_12 = value;
	}

	inline static int32_t get_offset_of_EnsureQuaternionContinuity_13() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___EnsureQuaternionContinuity_13)); }
	inline bool get_EnsureQuaternionContinuity_13() const { return ___EnsureQuaternionContinuity_13; }
	inline bool* get_address_of_EnsureQuaternionContinuity_13() { return &___EnsureQuaternionContinuity_13; }
	inline void set_EnsureQuaternionContinuity_13(bool value)
	{
		___EnsureQuaternionContinuity_13 = value;
	}

	inline static int32_t get_offset_of_UseOriginalPositionRotationAndScale_14() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___UseOriginalPositionRotationAndScale_14)); }
	inline bool get_UseOriginalPositionRotationAndScale_14() const { return ___UseOriginalPositionRotationAndScale_14; }
	inline bool* get_address_of_UseOriginalPositionRotationAndScale_14() { return &___UseOriginalPositionRotationAndScale_14; }
	inline void set_UseOriginalPositionRotationAndScale_14(bool value)
	{
		___UseOriginalPositionRotationAndScale_14 = value;
	}

	inline static int32_t get_offset_of_AnimatorController_15() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___AnimatorController_15)); }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * get_AnimatorController_15() const { return ___AnimatorController_15; }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD ** get_address_of_AnimatorController_15() { return &___AnimatorController_15; }
	inline void set_AnimatorController_15(RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * value)
	{
		___AnimatorController_15 = value;
		Il2CppCodeGenWriteBarrier((&___AnimatorController_15), value);
	}

	inline static int32_t get_offset_of_Avatar_16() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___Avatar_16)); }
	inline Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * get_Avatar_16() const { return ___Avatar_16; }
	inline Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B ** get_address_of_Avatar_16() { return &___Avatar_16; }
	inline void set_Avatar_16(Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * value)
	{
		___Avatar_16 = value;
		Il2CppCodeGenWriteBarrier((&___Avatar_16), value);
	}

	inline static int32_t get_offset_of_DontGenerateAvatar_17() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontGenerateAvatar_17)); }
	inline bool get_DontGenerateAvatar_17() const { return ___DontGenerateAvatar_17; }
	inline bool* get_address_of_DontGenerateAvatar_17() { return &___DontGenerateAvatar_17; }
	inline void set_DontGenerateAvatar_17(bool value)
	{
		___DontGenerateAvatar_17 = value;
	}

	inline static int32_t get_offset_of_DontLoadMetadata_18() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadMetadata_18)); }
	inline bool get_DontLoadMetadata_18() const { return ___DontLoadMetadata_18; }
	inline bool* get_address_of_DontLoadMetadata_18() { return &___DontLoadMetadata_18; }
	inline void set_DontLoadMetadata_18(bool value)
	{
		___DontLoadMetadata_18 = value;
	}

	inline static int32_t get_offset_of_DontAddMetadataCollection_19() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontAddMetadataCollection_19)); }
	inline bool get_DontAddMetadataCollection_19() const { return ___DontAddMetadataCollection_19; }
	inline bool* get_address_of_DontAddMetadataCollection_19() { return &___DontAddMetadataCollection_19; }
	inline void set_DontAddMetadataCollection_19(bool value)
	{
		___DontAddMetadataCollection_19 = value;
	}

	inline static int32_t get_offset_of_DontLoadMaterials_20() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadMaterials_20)); }
	inline bool get_DontLoadMaterials_20() const { return ___DontLoadMaterials_20; }
	inline bool* get_address_of_DontLoadMaterials_20() { return &___DontLoadMaterials_20; }
	inline void set_DontLoadMaterials_20(bool value)
	{
		___DontLoadMaterials_20 = value;
	}

	inline static int32_t get_offset_of_ApplyColorAlpha_21() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyColorAlpha_21)); }
	inline bool get_ApplyColorAlpha_21() const { return ___ApplyColorAlpha_21; }
	inline bool* get_address_of_ApplyColorAlpha_21() { return &___ApplyColorAlpha_21; }
	inline void set_ApplyColorAlpha_21(bool value)
	{
		___ApplyColorAlpha_21 = value;
	}

	inline static int32_t get_offset_of_ApplyDiffuseColor_22() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyDiffuseColor_22)); }
	inline bool get_ApplyDiffuseColor_22() const { return ___ApplyDiffuseColor_22; }
	inline bool* get_address_of_ApplyDiffuseColor_22() { return &___ApplyDiffuseColor_22; }
	inline void set_ApplyDiffuseColor_22(bool value)
	{
		___ApplyDiffuseColor_22 = value;
	}

	inline static int32_t get_offset_of_ApplyEmissionColor_23() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyEmissionColor_23)); }
	inline bool get_ApplyEmissionColor_23() const { return ___ApplyEmissionColor_23; }
	inline bool* get_address_of_ApplyEmissionColor_23() { return &___ApplyEmissionColor_23; }
	inline void set_ApplyEmissionColor_23(bool value)
	{
		___ApplyEmissionColor_23 = value;
	}

	inline static int32_t get_offset_of_ApplySpecularColor_24() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplySpecularColor_24)); }
	inline bool get_ApplySpecularColor_24() const { return ___ApplySpecularColor_24; }
	inline bool* get_address_of_ApplySpecularColor_24() { return &___ApplySpecularColor_24; }
	inline void set_ApplySpecularColor_24(bool value)
	{
		___ApplySpecularColor_24 = value;
	}

	inline static int32_t get_offset_of_ApplyDiffuseTexture_25() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyDiffuseTexture_25)); }
	inline bool get_ApplyDiffuseTexture_25() const { return ___ApplyDiffuseTexture_25; }
	inline bool* get_address_of_ApplyDiffuseTexture_25() { return &___ApplyDiffuseTexture_25; }
	inline void set_ApplyDiffuseTexture_25(bool value)
	{
		___ApplyDiffuseTexture_25 = value;
	}

	inline static int32_t get_offset_of_ApplyEmissionTexture_26() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyEmissionTexture_26)); }
	inline bool get_ApplyEmissionTexture_26() const { return ___ApplyEmissionTexture_26; }
	inline bool* get_address_of_ApplyEmissionTexture_26() { return &___ApplyEmissionTexture_26; }
	inline void set_ApplyEmissionTexture_26(bool value)
	{
		___ApplyEmissionTexture_26 = value;
	}

	inline static int32_t get_offset_of_ApplySpecularTexture_27() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplySpecularTexture_27)); }
	inline bool get_ApplySpecularTexture_27() const { return ___ApplySpecularTexture_27; }
	inline bool* get_address_of_ApplySpecularTexture_27() { return &___ApplySpecularTexture_27; }
	inline void set_ApplySpecularTexture_27(bool value)
	{
		___ApplySpecularTexture_27 = value;
	}

	inline static int32_t get_offset_of_ApplyNormalTexture_28() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyNormalTexture_28)); }
	inline bool get_ApplyNormalTexture_28() const { return ___ApplyNormalTexture_28; }
	inline bool* get_address_of_ApplyNormalTexture_28() { return &___ApplyNormalTexture_28; }
	inline void set_ApplyNormalTexture_28(bool value)
	{
		___ApplyNormalTexture_28 = value;
	}

	inline static int32_t get_offset_of_ApplyDisplacementTexture_29() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyDisplacementTexture_29)); }
	inline bool get_ApplyDisplacementTexture_29() const { return ___ApplyDisplacementTexture_29; }
	inline bool* get_address_of_ApplyDisplacementTexture_29() { return &___ApplyDisplacementTexture_29; }
	inline void set_ApplyDisplacementTexture_29(bool value)
	{
		___ApplyDisplacementTexture_29 = value;
	}

	inline static int32_t get_offset_of_ApplyOcclusionTexture_30() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyOcclusionTexture_30)); }
	inline bool get_ApplyOcclusionTexture_30() const { return ___ApplyOcclusionTexture_30; }
	inline bool* get_address_of_ApplyOcclusionTexture_30() { return &___ApplyOcclusionTexture_30; }
	inline void set_ApplyOcclusionTexture_30(bool value)
	{
		___ApplyOcclusionTexture_30 = value;
	}

	inline static int32_t get_offset_of_ApplyMetallicTexture_31() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyMetallicTexture_31)); }
	inline bool get_ApplyMetallicTexture_31() const { return ___ApplyMetallicTexture_31; }
	inline bool* get_address_of_ApplyMetallicTexture_31() { return &___ApplyMetallicTexture_31; }
	inline void set_ApplyMetallicTexture_31(bool value)
	{
		___ApplyMetallicTexture_31 = value;
	}

	inline static int32_t get_offset_of_ApplyNormalScale_32() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyNormalScale_32)); }
	inline bool get_ApplyNormalScale_32() const { return ___ApplyNormalScale_32; }
	inline bool* get_address_of_ApplyNormalScale_32() { return &___ApplyNormalScale_32; }
	inline void set_ApplyNormalScale_32(bool value)
	{
		___ApplyNormalScale_32 = value;
	}

	inline static int32_t get_offset_of_ApplyGlossiness_33() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyGlossiness_33)); }
	inline bool get_ApplyGlossiness_33() const { return ___ApplyGlossiness_33; }
	inline bool* get_address_of_ApplyGlossiness_33() { return &___ApplyGlossiness_33; }
	inline void set_ApplyGlossiness_33(bool value)
	{
		___ApplyGlossiness_33 = value;
	}

	inline static int32_t get_offset_of_ApplyGlossinessScale_34() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyGlossinessScale_34)); }
	inline bool get_ApplyGlossinessScale_34() const { return ___ApplyGlossinessScale_34; }
	inline bool* get_address_of_ApplyGlossinessScale_34() { return &___ApplyGlossinessScale_34; }
	inline void set_ApplyGlossinessScale_34(bool value)
	{
		___ApplyGlossinessScale_34 = value;
	}

	inline static int32_t get_offset_of_LoadRawMaterialProperties_35() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___LoadRawMaterialProperties_35)); }
	inline bool get_LoadRawMaterialProperties_35() const { return ___LoadRawMaterialProperties_35; }
	inline bool* get_address_of_LoadRawMaterialProperties_35() { return &___LoadRawMaterialProperties_35; }
	inline void set_LoadRawMaterialProperties_35(bool value)
	{
		___LoadRawMaterialProperties_35 = value;
	}

	inline static int32_t get_offset_of_DisableAlphaMaterials_36() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DisableAlphaMaterials_36)); }
	inline bool get_DisableAlphaMaterials_36() const { return ___DisableAlphaMaterials_36; }
	inline bool* get_address_of_DisableAlphaMaterials_36() { return &___DisableAlphaMaterials_36; }
	inline void set_DisableAlphaMaterials_36(bool value)
	{
		___DisableAlphaMaterials_36 = value;
	}

	inline static int32_t get_offset_of_ApplyAlphaMaterials_37() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ApplyAlphaMaterials_37)); }
	inline bool get_ApplyAlphaMaterials_37() const { return ___ApplyAlphaMaterials_37; }
	inline bool* get_address_of_ApplyAlphaMaterials_37() { return &___ApplyAlphaMaterials_37; }
	inline void set_ApplyAlphaMaterials_37(bool value)
	{
		___ApplyAlphaMaterials_37 = value;
	}

	inline static int32_t get_offset_of_ScanForAlphaMaterials_38() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ScanForAlphaMaterials_38)); }
	inline bool get_ScanForAlphaMaterials_38() const { return ___ScanForAlphaMaterials_38; }
	inline bool* get_address_of_ScanForAlphaMaterials_38() { return &___ScanForAlphaMaterials_38; }
	inline void set_ScanForAlphaMaterials_38(bool value)
	{
		___ScanForAlphaMaterials_38 = value;
	}

	inline static int32_t get_offset_of_UseCutoutMaterials_39() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___UseCutoutMaterials_39)); }
	inline bool get_UseCutoutMaterials_39() const { return ___UseCutoutMaterials_39; }
	inline bool* get_address_of_UseCutoutMaterials_39() { return &___UseCutoutMaterials_39; }
	inline void set_UseCutoutMaterials_39(bool value)
	{
		___UseCutoutMaterials_39 = value;
	}

	inline static int32_t get_offset_of_UseStandardSpecularMaterial_40() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___UseStandardSpecularMaterial_40)); }
	inline bool get_UseStandardSpecularMaterial_40() const { return ___UseStandardSpecularMaterial_40; }
	inline bool* get_address_of_UseStandardSpecularMaterial_40() { return &___UseStandardSpecularMaterial_40; }
	inline void set_UseStandardSpecularMaterial_40(bool value)
	{
		___UseStandardSpecularMaterial_40 = value;
	}

	inline static int32_t get_offset_of_MaterialShadingMode_41() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___MaterialShadingMode_41)); }
	inline int32_t get_MaterialShadingMode_41() const { return ___MaterialShadingMode_41; }
	inline int32_t* get_address_of_MaterialShadingMode_41() { return &___MaterialShadingMode_41; }
	inline void set_MaterialShadingMode_41(int32_t value)
	{
		___MaterialShadingMode_41 = value;
	}

	inline static int32_t get_offset_of_MaterialTransparencyMode_42() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___MaterialTransparencyMode_42)); }
	inline int32_t get_MaterialTransparencyMode_42() const { return ___MaterialTransparencyMode_42; }
	inline int32_t* get_address_of_MaterialTransparencyMode_42() { return &___MaterialTransparencyMode_42; }
	inline void set_MaterialTransparencyMode_42(int32_t value)
	{
		___MaterialTransparencyMode_42 = value;
	}

	inline static int32_t get_offset_of_DontLoadMeshes_43() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadMeshes_43)); }
	inline bool get_DontLoadMeshes_43() const { return ___DontLoadMeshes_43; }
	inline bool* get_address_of_DontLoadMeshes_43() { return &___DontLoadMeshes_43; }
	inline void set_DontLoadMeshes_43(bool value)
	{
		___DontLoadMeshes_43 = value;
	}

	inline static int32_t get_offset_of_DontLoadBlendShapes_44() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadBlendShapes_44)); }
	inline bool get_DontLoadBlendShapes_44() const { return ___DontLoadBlendShapes_44; }
	inline bool* get_address_of_DontLoadBlendShapes_44() { return &___DontLoadBlendShapes_44; }
	inline void set_DontLoadBlendShapes_44(bool value)
	{
		___DontLoadBlendShapes_44 = value;
	}

	inline static int32_t get_offset_of_DontLoadSkinning_45() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___DontLoadSkinning_45)); }
	inline bool get_DontLoadSkinning_45() const { return ___DontLoadSkinning_45; }
	inline bool* get_address_of_DontLoadSkinning_45() { return &___DontLoadSkinning_45; }
	inline void set_DontLoadSkinning_45(bool value)
	{
		___DontLoadSkinning_45 = value;
	}

	inline static int32_t get_offset_of_CombineMeshes_46() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___CombineMeshes_46)); }
	inline bool get_CombineMeshes_46() const { return ___CombineMeshes_46; }
	inline bool* get_address_of_CombineMeshes_46() { return &___CombineMeshes_46; }
	inline void set_CombineMeshes_46(bool value)
	{
		___CombineMeshes_46 = value;
	}

	inline static int32_t get_offset_of_Use32BitsIndexFormat_47() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___Use32BitsIndexFormat_47)); }
	inline bool get_Use32BitsIndexFormat_47() const { return ___Use32BitsIndexFormat_47; }
	inline bool* get_address_of_Use32BitsIndexFormat_47() { return &___Use32BitsIndexFormat_47; }
	inline void set_Use32BitsIndexFormat_47(bool value)
	{
		___Use32BitsIndexFormat_47 = value;
	}

	inline static int32_t get_offset_of_GenerateMeshColliders_48() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___GenerateMeshColliders_48)); }
	inline bool get_GenerateMeshColliders_48() const { return ___GenerateMeshColliders_48; }
	inline bool* get_address_of_GenerateMeshColliders_48() { return &___GenerateMeshColliders_48; }
	inline void set_GenerateMeshColliders_48(bool value)
	{
		___GenerateMeshColliders_48 = value;
	}

	inline static int32_t get_offset_of_ConvexMeshColliders_49() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___ConvexMeshColliders_49)); }
	inline bool get_ConvexMeshColliders_49() const { return ___ConvexMeshColliders_49; }
	inline bool* get_address_of_ConvexMeshColliders_49() { return &___ConvexMeshColliders_49; }
	inline void set_ConvexMeshColliders_49(bool value)
	{
		___ConvexMeshColliders_49 = value;
	}

	inline static int32_t get_offset_of_RotationAngles_50() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___RotationAngles_50)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationAngles_50() const { return ___RotationAngles_50; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationAngles_50() { return &___RotationAngles_50; }
	inline void set_RotationAngles_50(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationAngles_50 = value;
	}

	inline static int32_t get_offset_of_Scale_51() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___Scale_51)); }
	inline float get_Scale_51() const { return ___Scale_51; }
	inline float* get_address_of_Scale_51() { return &___Scale_51; }
	inline void set_Scale_51(float value)
	{
		___Scale_51 = value;
	}

	inline static int32_t get_offset_of_PostProcessSteps_52() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___PostProcessSteps_52)); }
	inline int32_t get_PostProcessSteps_52() const { return ___PostProcessSteps_52; }
	inline int32_t* get_address_of_PostProcessSteps_52() { return &___PostProcessSteps_52; }
	inline void set_PostProcessSteps_52(int32_t value)
	{
		___PostProcessSteps_52 = value;
	}

	inline static int32_t get_offset_of_TextureCompression_53() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___TextureCompression_53)); }
	inline int32_t get_TextureCompression_53() const { return ___TextureCompression_53; }
	inline int32_t* get_address_of_TextureCompression_53() { return &___TextureCompression_53; }
	inline void set_TextureCompression_53(int32_t value)
	{
		___TextureCompression_53 = value;
	}

	inline static int32_t get_offset_of_TextureFilterMode_54() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___TextureFilterMode_54)); }
	inline int32_t get_TextureFilterMode_54() const { return ___TextureFilterMode_54; }
	inline int32_t* get_address_of_TextureFilterMode_54() { return &___TextureFilterMode_54; }
	inline void set_TextureFilterMode_54(int32_t value)
	{
		___TextureFilterMode_54 = value;
	}

	inline static int32_t get_offset_of_GenerateMipMaps_55() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___GenerateMipMaps_55)); }
	inline bool get_GenerateMipMaps_55() const { return ___GenerateMipMaps_55; }
	inline bool* get_address_of_GenerateMipMaps_55() { return &___GenerateMipMaps_55; }
	inline void set_GenerateMipMaps_55(bool value)
	{
		___GenerateMipMaps_55 = value;
	}

	inline static int32_t get_offset_of_AdvancedConfigs_56() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B, ___AdvancedConfigs_56)); }
	inline List_1_t53DF7B463C92E9A1A63552E2D7B286018D2EACAD * get_AdvancedConfigs_56() const { return ___AdvancedConfigs_56; }
	inline List_1_t53DF7B463C92E9A1A63552E2D7B286018D2EACAD ** get_address_of_AdvancedConfigs_56() { return &___AdvancedConfigs_56; }
	inline void set_AdvancedConfigs_56(List_1_t53DF7B463C92E9A1A63552E2D7B286018D2EACAD * value)
	{
		___AdvancedConfigs_56 = value;
		Il2CppCodeGenWriteBarrier((&___AdvancedConfigs_56), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADEROPTIONS_TC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B_H
#ifndef DATACALLBACK_T61B4A7D4712326A93AD3F640D9BFD006932FA9BD_H
#define DATACALLBACK_T61B4A7D4712326A93AD3F640D9BFD006932FA9BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_DataCallback
struct  DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACALLBACK_T61B4A7D4712326A93AD3F640D9BFD006932FA9BD_H
#ifndef EXISTSCALLBACK_TDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35_H
#define EXISTSCALLBACK_TDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ExistsCallback
struct  ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXISTSCALLBACK_TDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35_H
#ifndef PROGRESSCALLBACK_TF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77_H
#define PROGRESSCALLBACK_TF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ProgressCallback
struct  ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCALLBACK_TF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77_H
#ifndef AVATARCREATEDHANDLE_TBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2_H
#define AVATARCREATEDHANDLE_TBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AvatarCreatedHandle
struct  AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARCREATEDHANDLE_TBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2_H
#ifndef BLENDSHAPEKEYCREATEDHANDLE_T32BAC592C9CF57884413E34D52CEA3E601F44E28_H
#define BLENDSHAPEKEYCREATEDHANDLE_T32BAC592C9CF57884413E34D52CEA3E601F44E28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.BlendShapeKeyCreatedHandle
struct  BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEKEYCREATEDHANDLE_T32BAC592C9CF57884413E34D52CEA3E601F44E28_H
#ifndef DATADISPOSALCALLBACK_TECF4CAD57FA4BBB74F300069101E48B9A53DB06B_H
#define DATADISPOSALCALLBACK_TECF4CAD57FA4BBB74F300069101E48B9A53DB06B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.DataDisposalCallback
struct  DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATADISPOSALCALLBACK_TECF4CAD57FA4BBB74F300069101E48B9A53DB06B_H
#ifndef EMBEDDEDTEXTURELOADCALLBACK_T9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213_H
#define EMBEDDEDTEXTURELOADCALLBACK_T9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureLoadCallback
struct  EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTURELOADCALLBACK_T9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213_H
#ifndef LOADTEXTUREDATACALLBACK_T158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1_H
#define LOADTEXTUREDATACALLBACK_T158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.LoadTextureDataCallback
struct  LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTEXTUREDATACALLBACK_T158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1_H
#ifndef MATERIALCREATEDHANDLE_T4D5947E036CE039B5555795C3600179DD199B541_H
#define MATERIALCREATEDHANDLE_T4D5947E036CE039B5555795C3600179DD199B541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialCreatedHandle
struct  MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCREATEDHANDLE_T4D5947E036CE039B5555795C3600179DD199B541_H
#ifndef MESHCREATEDHANDLE_T44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5_H
#define MESHCREATEDHANDLE_T44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshCreatedHandle
struct  MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCREATEDHANDLE_T44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5_H
#ifndef METADATAPROCESSEDHANDLE_T9CEFC8DFED53FBC773B7886D759B915EFF567E4A_H
#define METADATAPROCESSEDHANDLE_T9CEFC8DFED53FBC773B7886D759B915EFF567E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MetadataProcessedHandle
struct  MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROCESSEDHANDLE_T9CEFC8DFED53FBC773B7886D759B915EFF567E4A_H
#ifndef OBJECTLOADEDHANDLE_TFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B_H
#define OBJECTLOADEDHANDLE_TFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ObjectLoadedHandle
struct  ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTLOADEDHANDLE_TFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B_H
#ifndef FILEOPENEVENTHANDLE_T52FBAD3455607D930E8D983FA25B31D7B10DCCE0_H
#define FILEOPENEVENTHANDLE_T52FBAD3455607D930E8D983FA25B31D7B10DCCE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.FileOpenEventHandle
struct  FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEOPENEVENTHANDLE_T52FBAD3455607D930E8D983FA25B31D7B10DCCE0_H
#ifndef TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#define TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_TD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C_H
#ifndef TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#define TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TexturePreLoadHandle
struct  TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPRELOADHANDLE_T1F4AEFE250AE46F35577FA3B53AFD9D756D41B24_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ASSETDOWNLOADER_T190FFB556B68044A594D822E6A54B7562ED1A6B8_H
#define ASSETDOWNLOADER_T190FFB556B68044A594D822E6A54B7562ED1A6B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetDownloader
struct  AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean TriLib.AssetDownloader::AutoStart
	bool ___AutoStart_4;
	// System.String TriLib.AssetDownloader::AssetURI
	String_t* ___AssetURI_5;
	// System.Int32 TriLib.AssetDownloader::Timeout
	int32_t ___Timeout_6;
	// System.String TriLib.AssetDownloader::AssetExtension
	String_t* ___AssetExtension_7;
	// UnityEngine.GameObject TriLib.AssetDownloader::WrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___WrapperGameObject_8;
	// System.Boolean TriLib.AssetDownloader::ShowProgress
	bool ___ShowProgress_9;
	// System.Boolean TriLib.AssetDownloader::Async
	bool ___Async_10;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetDownloader::ProgressCallback
	ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * ___ProgressCallback_11;
	// UnityEngine.Networking.UnityWebRequest TriLib.AssetDownloader::_unityWebRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ____unityWebRequest_12;
	// UnityEngine.GUIStyle TriLib.AssetDownloader::_centeredStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ____centeredStyle_13;
	// System.String TriLib.AssetDownloader::_error
	String_t* ____error_14;

public:
	inline static int32_t get_offset_of_AutoStart_4() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___AutoStart_4)); }
	inline bool get_AutoStart_4() const { return ___AutoStart_4; }
	inline bool* get_address_of_AutoStart_4() { return &___AutoStart_4; }
	inline void set_AutoStart_4(bool value)
	{
		___AutoStart_4 = value;
	}

	inline static int32_t get_offset_of_AssetURI_5() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___AssetURI_5)); }
	inline String_t* get_AssetURI_5() const { return ___AssetURI_5; }
	inline String_t** get_address_of_AssetURI_5() { return &___AssetURI_5; }
	inline void set_AssetURI_5(String_t* value)
	{
		___AssetURI_5 = value;
		Il2CppCodeGenWriteBarrier((&___AssetURI_5), value);
	}

	inline static int32_t get_offset_of_Timeout_6() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___Timeout_6)); }
	inline int32_t get_Timeout_6() const { return ___Timeout_6; }
	inline int32_t* get_address_of_Timeout_6() { return &___Timeout_6; }
	inline void set_Timeout_6(int32_t value)
	{
		___Timeout_6 = value;
	}

	inline static int32_t get_offset_of_AssetExtension_7() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___AssetExtension_7)); }
	inline String_t* get_AssetExtension_7() const { return ___AssetExtension_7; }
	inline String_t** get_address_of_AssetExtension_7() { return &___AssetExtension_7; }
	inline void set_AssetExtension_7(String_t* value)
	{
		___AssetExtension_7 = value;
		Il2CppCodeGenWriteBarrier((&___AssetExtension_7), value);
	}

	inline static int32_t get_offset_of_WrapperGameObject_8() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___WrapperGameObject_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_WrapperGameObject_8() const { return ___WrapperGameObject_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_WrapperGameObject_8() { return &___WrapperGameObject_8; }
	inline void set_WrapperGameObject_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___WrapperGameObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___WrapperGameObject_8), value);
	}

	inline static int32_t get_offset_of_ShowProgress_9() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___ShowProgress_9)); }
	inline bool get_ShowProgress_9() const { return ___ShowProgress_9; }
	inline bool* get_address_of_ShowProgress_9() { return &___ShowProgress_9; }
	inline void set_ShowProgress_9(bool value)
	{
		___ShowProgress_9 = value;
	}

	inline static int32_t get_offset_of_Async_10() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___Async_10)); }
	inline bool get_Async_10() const { return ___Async_10; }
	inline bool* get_address_of_Async_10() { return &___Async_10; }
	inline void set_Async_10(bool value)
	{
		___Async_10 = value;
	}

	inline static int32_t get_offset_of_ProgressCallback_11() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ___ProgressCallback_11)); }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * get_ProgressCallback_11() const { return ___ProgressCallback_11; }
	inline ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 ** get_address_of_ProgressCallback_11() { return &___ProgressCallback_11; }
	inline void set_ProgressCallback_11(ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77 * value)
	{
		___ProgressCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressCallback_11), value);
	}

	inline static int32_t get_offset_of__unityWebRequest_12() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ____unityWebRequest_12)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get__unityWebRequest_12() const { return ____unityWebRequest_12; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of__unityWebRequest_12() { return &____unityWebRequest_12; }
	inline void set__unityWebRequest_12(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		____unityWebRequest_12 = value;
		Il2CppCodeGenWriteBarrier((&____unityWebRequest_12), value);
	}

	inline static int32_t get_offset_of__centeredStyle_13() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ____centeredStyle_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get__centeredStyle_13() const { return ____centeredStyle_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of__centeredStyle_13() { return &____centeredStyle_13; }
	inline void set__centeredStyle_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		____centeredStyle_13 = value;
		Il2CppCodeGenWriteBarrier((&____centeredStyle_13), value);
	}

	inline static int32_t get_offset_of__error_14() { return static_cast<int32_t>(offsetof(AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8, ____error_14)); }
	inline String_t* get__error_14() const { return ____error_14; }
	inline String_t** get_address_of__error_14() { return &____error_14; }
	inline void set__error_14(String_t* value)
	{
		____error_14 = value;
		Il2CppCodeGenWriteBarrier((&____error_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDOWNLOADER_T190FFB556B68044A594D822E6A54B7562ED1A6B8_H
#ifndef ASSETUNLOADER_TB7875EB167F1DDB05B955E4A7D844F111F2BA350_H
#define ASSETUNLOADER_TB7875EB167F1DDB05B955E4A7D844F111F2BA350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetUnloader
struct  AssetUnloader_tB7875EB167F1DDB05B955E4A7D844F111F2BA350  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETUNLOADER_TB7875EB167F1DDB05B955E4A7D844F111F2BA350_H
#ifndef ASSIMPMETADATACOLLECTION_T06E569CB384D28757282A759E71F881A8C012743_H
#define ASSIMPMETADATACOLLECTION_T06E569CB384D28757282A759E71F881A8C012743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadataCollection
struct  AssimpMetadataCollection_t06E569CB384D28757282A759E71F881A8C012743  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.AssimpMetadata> TriLib.AssimpMetadataCollection::_metadataDictionary
	Dictionary_2_tEEF08DE05ED9587E8DC4EBB0F61E55FAFCAAE2DC * ____metadataDictionary_4;

public:
	inline static int32_t get_offset_of__metadataDictionary_4() { return static_cast<int32_t>(offsetof(AssimpMetadataCollection_t06E569CB384D28757282A759E71F881A8C012743, ____metadataDictionary_4)); }
	inline Dictionary_2_tEEF08DE05ED9587E8DC4EBB0F61E55FAFCAAE2DC * get__metadataDictionary_4() const { return ____metadataDictionary_4; }
	inline Dictionary_2_tEEF08DE05ED9587E8DC4EBB0F61E55FAFCAAE2DC ** get_address_of__metadataDictionary_4() { return &____metadataDictionary_4; }
	inline void set__metadataDictionary_4(Dictionary_2_tEEF08DE05ED9587E8DC4EBB0F61E55FAFCAAE2DC * value)
	{
		____metadataDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&____metadataDictionary_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATACOLLECTION_T06E569CB384D28757282A759E71F881A8C012743_H
#ifndef DISPATCHER_TAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_H
#define DISPATCHER_TAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Dispatcher
struct  Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields
{
public:
	// TriLib.Dispatcher TriLib.Dispatcher::_instance
	Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5 * ____instance_4;
	// System.Boolean TriLib.Dispatcher::_instanceExists
	bool ____instanceExists_5;
	// System.Object TriLib.Dispatcher::LockObject
	RuntimeObject * ___LockObject_6;
	// System.Collections.Generic.Queue`1<System.Action> TriLib.Dispatcher::Actions
	Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * ___Actions_7;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields, ____instance_4)); }
	inline Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5 * get__instance_4() const { return ____instance_4; }
	inline Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__instanceExists_5() { return static_cast<int32_t>(offsetof(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields, ____instanceExists_5)); }
	inline bool get__instanceExists_5() const { return ____instanceExists_5; }
	inline bool* get_address_of__instanceExists_5() { return &____instanceExists_5; }
	inline void set__instanceExists_5(bool value)
	{
		____instanceExists_5 = value;
	}

	inline static int32_t get_offset_of_LockObject_6() { return static_cast<int32_t>(offsetof(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields, ___LockObject_6)); }
	inline RuntimeObject * get_LockObject_6() const { return ___LockObject_6; }
	inline RuntimeObject ** get_address_of_LockObject_6() { return &___LockObject_6; }
	inline void set_LockObject_6(RuntimeObject * value)
	{
		___LockObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___LockObject_6), value);
	}

	inline static int32_t get_offset_of_Actions_7() { return static_cast<int32_t>(offsetof(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields, ___Actions_7)); }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * get_Actions_7() const { return ___Actions_7; }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA ** get_address_of_Actions_7() { return &___Actions_7; }
	inline void set_Actions_7(Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * value)
	{
		___Actions_7 = value;
		Il2CppCodeGenWriteBarrier((&___Actions_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_TAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_H
#ifndef AVATARLOADER_T4CE8105BE55427205855FC930DFF49C0FA3B2A4F_H
#define AVATARLOADER_T4CE8105BE55427205855FC930DFF49C0FA3B2A4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoader
struct  AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject TriLib.Extras.AvatarLoader::CurrentAvatar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___CurrentAvatar_4;
	// UnityEngine.RuntimeAnimatorController TriLib.Extras.AvatarLoader::RuntimeAnimatorController
	RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * ___RuntimeAnimatorController_5;
	// System.Single TriLib.Extras.AvatarLoader::ArmStretch
	float ___ArmStretch_6;
	// System.Single TriLib.Extras.AvatarLoader::FeetSpacing
	float ___FeetSpacing_7;
	// System.Boolean TriLib.Extras.AvatarLoader::HasTranslationDof
	bool ___HasTranslationDof_8;
	// System.Single TriLib.Extras.AvatarLoader::LegStretch
	float ___LegStretch_9;
	// System.Single TriLib.Extras.AvatarLoader::LowerArmTwist
	float ___LowerArmTwist_10;
	// System.Single TriLib.Extras.AvatarLoader::LowerLegTwist
	float ___LowerLegTwist_11;
	// System.Single TriLib.Extras.AvatarLoader::UpperArmTwist
	float ___UpperArmTwist_12;
	// System.Single TriLib.Extras.AvatarLoader::UpperLegTwist
	float ___UpperLegTwist_13;
	// System.Single TriLib.Extras.AvatarLoader::Scale
	float ___Scale_14;
	// System.Single TriLib.Extras.AvatarLoader::HeightOffset
	float ___HeightOffset_15;
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::CustomBoneNames
	BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * ___CustomBoneNames_16;
	// TriLib.AssetLoaderOptions TriLib.Extras.AvatarLoader::_loaderOptions
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * ____loaderOptions_19;

public:
	inline static int32_t get_offset_of_CurrentAvatar_4() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___CurrentAvatar_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_CurrentAvatar_4() const { return ___CurrentAvatar_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_CurrentAvatar_4() { return &___CurrentAvatar_4; }
	inline void set_CurrentAvatar_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___CurrentAvatar_4 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentAvatar_4), value);
	}

	inline static int32_t get_offset_of_RuntimeAnimatorController_5() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___RuntimeAnimatorController_5)); }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * get_RuntimeAnimatorController_5() const { return ___RuntimeAnimatorController_5; }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD ** get_address_of_RuntimeAnimatorController_5() { return &___RuntimeAnimatorController_5; }
	inline void set_RuntimeAnimatorController_5(RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * value)
	{
		___RuntimeAnimatorController_5 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeAnimatorController_5), value);
	}

	inline static int32_t get_offset_of_ArmStretch_6() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___ArmStretch_6)); }
	inline float get_ArmStretch_6() const { return ___ArmStretch_6; }
	inline float* get_address_of_ArmStretch_6() { return &___ArmStretch_6; }
	inline void set_ArmStretch_6(float value)
	{
		___ArmStretch_6 = value;
	}

	inline static int32_t get_offset_of_FeetSpacing_7() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___FeetSpacing_7)); }
	inline float get_FeetSpacing_7() const { return ___FeetSpacing_7; }
	inline float* get_address_of_FeetSpacing_7() { return &___FeetSpacing_7; }
	inline void set_FeetSpacing_7(float value)
	{
		___FeetSpacing_7 = value;
	}

	inline static int32_t get_offset_of_HasTranslationDof_8() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___HasTranslationDof_8)); }
	inline bool get_HasTranslationDof_8() const { return ___HasTranslationDof_8; }
	inline bool* get_address_of_HasTranslationDof_8() { return &___HasTranslationDof_8; }
	inline void set_HasTranslationDof_8(bool value)
	{
		___HasTranslationDof_8 = value;
	}

	inline static int32_t get_offset_of_LegStretch_9() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___LegStretch_9)); }
	inline float get_LegStretch_9() const { return ___LegStretch_9; }
	inline float* get_address_of_LegStretch_9() { return &___LegStretch_9; }
	inline void set_LegStretch_9(float value)
	{
		___LegStretch_9 = value;
	}

	inline static int32_t get_offset_of_LowerArmTwist_10() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___LowerArmTwist_10)); }
	inline float get_LowerArmTwist_10() const { return ___LowerArmTwist_10; }
	inline float* get_address_of_LowerArmTwist_10() { return &___LowerArmTwist_10; }
	inline void set_LowerArmTwist_10(float value)
	{
		___LowerArmTwist_10 = value;
	}

	inline static int32_t get_offset_of_LowerLegTwist_11() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___LowerLegTwist_11)); }
	inline float get_LowerLegTwist_11() const { return ___LowerLegTwist_11; }
	inline float* get_address_of_LowerLegTwist_11() { return &___LowerLegTwist_11; }
	inline void set_LowerLegTwist_11(float value)
	{
		___LowerLegTwist_11 = value;
	}

	inline static int32_t get_offset_of_UpperArmTwist_12() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___UpperArmTwist_12)); }
	inline float get_UpperArmTwist_12() const { return ___UpperArmTwist_12; }
	inline float* get_address_of_UpperArmTwist_12() { return &___UpperArmTwist_12; }
	inline void set_UpperArmTwist_12(float value)
	{
		___UpperArmTwist_12 = value;
	}

	inline static int32_t get_offset_of_UpperLegTwist_13() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___UpperLegTwist_13)); }
	inline float get_UpperLegTwist_13() const { return ___UpperLegTwist_13; }
	inline float* get_address_of_UpperLegTwist_13() { return &___UpperLegTwist_13; }
	inline void set_UpperLegTwist_13(float value)
	{
		___UpperLegTwist_13 = value;
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___Scale_14)); }
	inline float get_Scale_14() const { return ___Scale_14; }
	inline float* get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(float value)
	{
		___Scale_14 = value;
	}

	inline static int32_t get_offset_of_HeightOffset_15() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___HeightOffset_15)); }
	inline float get_HeightOffset_15() const { return ___HeightOffset_15; }
	inline float* get_address_of_HeightOffset_15() { return &___HeightOffset_15; }
	inline void set_HeightOffset_15(float value)
	{
		___HeightOffset_15 = value;
	}

	inline static int32_t get_offset_of_CustomBoneNames_16() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ___CustomBoneNames_16)); }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * get_CustomBoneNames_16() const { return ___CustomBoneNames_16; }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A ** get_address_of_CustomBoneNames_16() { return &___CustomBoneNames_16; }
	inline void set_CustomBoneNames_16(BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * value)
	{
		___CustomBoneNames_16 = value;
		Il2CppCodeGenWriteBarrier((&___CustomBoneNames_16), value);
	}

	inline static int32_t get_offset_of__loaderOptions_19() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F, ____loaderOptions_19)); }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * get__loaderOptions_19() const { return ____loaderOptions_19; }
	inline AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B ** get_address_of__loaderOptions_19() { return &____loaderOptions_19; }
	inline void set__loaderOptions_19(AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B * value)
	{
		____loaderOptions_19 = value;
		Il2CppCodeGenWriteBarrier((&____loaderOptions_19), value);
	}
};

struct AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields
{
public:
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::BipedBoneNames
	BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * ___BipedBoneNames_17;
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::MixamoBoneNames
	BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * ___MixamoBoneNames_18;

public:
	inline static int32_t get_offset_of_BipedBoneNames_17() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields, ___BipedBoneNames_17)); }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * get_BipedBoneNames_17() const { return ___BipedBoneNames_17; }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A ** get_address_of_BipedBoneNames_17() { return &___BipedBoneNames_17; }
	inline void set_BipedBoneNames_17(BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * value)
	{
		___BipedBoneNames_17 = value;
		Il2CppCodeGenWriteBarrier((&___BipedBoneNames_17), value);
	}

	inline static int32_t get_offset_of_MixamoBoneNames_18() { return static_cast<int32_t>(offsetof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields, ___MixamoBoneNames_18)); }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * get_MixamoBoneNames_18() const { return ___MixamoBoneNames_18; }
	inline BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A ** get_address_of_MixamoBoneNames_18() { return &___MixamoBoneNames_18; }
	inline void set_MixamoBoneNames_18(BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A * value)
	{
		___MixamoBoneNames_18 = value;
		Il2CppCodeGenWriteBarrier((&___MixamoBoneNames_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARLOADER_T4CE8105BE55427205855FC930DFF49C0FA3B2A4F_H
#ifndef AVATARLOADERSAMPLE_TE624280BAB9E309FA69447AE96FF213DD9E8130B_H
#define AVATARLOADERSAMPLE_TE624280BAB9E309FA69447AE96FF213DD9E8130B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoaderSample
struct  AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::FreeLookCamPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___FreeLookCamPrefab_4;
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::ThirdPersonControllerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ThirdPersonControllerPrefab_5;
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::ActiveCameraGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ActiveCameraGameObject_6;
	// System.String TriLib.Extras.AvatarLoaderSample::ModelsDirectory
	String_t* ___ModelsDirectory_7;
	// System.String[] TriLib.Extras.AvatarLoaderSample::_files
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____files_8;
	// UnityEngine.Rect TriLib.Extras.AvatarLoaderSample::_windowRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ____windowRect_9;
	// UnityEngine.Vector3 TriLib.Extras.AvatarLoaderSample::_scrollPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____scrollPosition_10;
	// TriLib.Extras.AvatarLoader TriLib.Extras.AvatarLoaderSample::_avatarLoader
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F * ____avatarLoader_11;

public:
	inline static int32_t get_offset_of_FreeLookCamPrefab_4() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ___FreeLookCamPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_FreeLookCamPrefab_4() const { return ___FreeLookCamPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_FreeLookCamPrefab_4() { return &___FreeLookCamPrefab_4; }
	inline void set_FreeLookCamPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___FreeLookCamPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___FreeLookCamPrefab_4), value);
	}

	inline static int32_t get_offset_of_ThirdPersonControllerPrefab_5() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ___ThirdPersonControllerPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ThirdPersonControllerPrefab_5() const { return ___ThirdPersonControllerPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ThirdPersonControllerPrefab_5() { return &___ThirdPersonControllerPrefab_5; }
	inline void set_ThirdPersonControllerPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ThirdPersonControllerPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ThirdPersonControllerPrefab_5), value);
	}

	inline static int32_t get_offset_of_ActiveCameraGameObject_6() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ___ActiveCameraGameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ActiveCameraGameObject_6() const { return ___ActiveCameraGameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ActiveCameraGameObject_6() { return &___ActiveCameraGameObject_6; }
	inline void set_ActiveCameraGameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ActiveCameraGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveCameraGameObject_6), value);
	}

	inline static int32_t get_offset_of_ModelsDirectory_7() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ___ModelsDirectory_7)); }
	inline String_t* get_ModelsDirectory_7() const { return ___ModelsDirectory_7; }
	inline String_t** get_address_of_ModelsDirectory_7() { return &___ModelsDirectory_7; }
	inline void set_ModelsDirectory_7(String_t* value)
	{
		___ModelsDirectory_7 = value;
		Il2CppCodeGenWriteBarrier((&___ModelsDirectory_7), value);
	}

	inline static int32_t get_offset_of__files_8() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ____files_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__files_8() const { return ____files_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__files_8() { return &____files_8; }
	inline void set__files_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____files_8 = value;
		Il2CppCodeGenWriteBarrier((&____files_8), value);
	}

	inline static int32_t get_offset_of__windowRect_9() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ____windowRect_9)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get__windowRect_9() const { return ____windowRect_9; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of__windowRect_9() { return &____windowRect_9; }
	inline void set__windowRect_9(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		____windowRect_9 = value;
	}

	inline static int32_t get_offset_of__scrollPosition_10() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ____scrollPosition_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__scrollPosition_10() const { return ____scrollPosition_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__scrollPosition_10() { return &____scrollPosition_10; }
	inline void set__scrollPosition_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____scrollPosition_10 = value;
	}

	inline static int32_t get_offset_of__avatarLoader_11() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B, ____avatarLoader_11)); }
	inline AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F * get__avatarLoader_11() const { return ____avatarLoader_11; }
	inline AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F ** get_address_of__avatarLoader_11() { return &____avatarLoader_11; }
	inline void set__avatarLoader_11(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F * value)
	{
		____avatarLoader_11 = value;
		Il2CppCodeGenWriteBarrier((&____avatarLoader_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARLOADERSAMPLE_TE624280BAB9E309FA69447AE96FF213DD9E8130B_H
#ifndef JSHELPER_TFEDCEE942A02C639F6F09DD63936CCF45703B913_H
#define JSHELPER_TFEDCEE942A02C639F6F09DD63936CCF45703B913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.JSHelper
struct  JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913_StaticFields
{
public:
	// TriLib.JSHelper TriLib.JSHelper::_instance
	JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913_StaticFields, ____instance_4)); }
	inline JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913 * get__instance_4() const { return ____instance_4; }
	inline JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSHELPER_TFEDCEE942A02C639F6F09DD63936CCF45703B913_H
#ifndef ANIMATIONTEXT_T8AD3C674AA82A4A53A36BC098DB416C14E75A7FB_H
#define ANIMATIONTEXT_T8AD3C674AA82A4A53A36BC098DB416C14E75A7FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.AnimationText
struct  AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTEXT_T8AD3C674AA82A4A53A36BC098DB416C14E75A7FB_H
#ifndef ASSETDOWNLOADERZIP_TEC96A3AA2485E56BAB55B741E2A4A3D769609322_H
#define ASSETDOWNLOADERZIP_TEC96A3AA2485E56BAB55B741E2A4A3D769609322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.AssetDownloaderZIP
struct  AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Networking.UnityWebRequest TriLib.Samples.AssetDownloaderZIP::_fileDownloader
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ____fileDownloader_4;

public:
	inline static int32_t get_offset_of__fileDownloader_4() { return static_cast<int32_t>(offsetof(AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322, ____fileDownloader_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get__fileDownloader_4() const { return ____fileDownloader_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of__fileDownloader_4() { return &____fileDownloader_4; }
	inline void set__fileDownloader_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		____fileDownloader_4 = value;
		Il2CppCodeGenWriteBarrier((&____fileDownloader_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDOWNLOADERZIP_TEC96A3AA2485E56BAB55B741E2A4A3D769609322_H
#ifndef ASSETLOADERWINDOW_T3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_H
#define ASSETLOADERWINDOW_T3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.AssetLoaderWindow
struct  AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean TriLib.Samples.AssetLoaderWindow::Async
	bool ___Async_5;
	// UnityEngine.UI.Button TriLib.Samples.AssetLoaderWindow::_loadLocalAssetButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____loadLocalAssetButton_6;
	// UnityEngine.UI.Button TriLib.Samples.AssetLoaderWindow::_loadRemoteAssetButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____loadRemoteAssetButton_7;
	// UnityEngine.UI.Text TriLib.Samples.AssetLoaderWindow::_spinningText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____spinningText_8;
	// UnityEngine.UI.Dropdown TriLib.Samples.AssetLoaderWindow::_transparencyModeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ____transparencyModeDropdown_9;
	// UnityEngine.UI.Dropdown TriLib.Samples.AssetLoaderWindow::_shadingDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ____shadingDropdown_10;
	// UnityEngine.UI.Toggle TriLib.Samples.AssetLoaderWindow::_spinXToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____spinXToggle_11;
	// UnityEngine.UI.Toggle TriLib.Samples.AssetLoaderWindow::_spinYToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____spinYToggle_12;
	// UnityEngine.UI.Button TriLib.Samples.AssetLoaderWindow::_resetRotationButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____resetRotationButton_13;
	// UnityEngine.UI.Button TriLib.Samples.AssetLoaderWindow::_stopAnimationButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____stopAnimationButton_14;
	// UnityEngine.UI.Text TriLib.Samples.AssetLoaderWindow::_animationsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____animationsText_15;
	// UnityEngine.UI.Text TriLib.Samples.AssetLoaderWindow::_blendShapesText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____blendShapesText_16;
	// UnityEngine.UI.ScrollRect TriLib.Samples.AssetLoaderWindow::_animationsScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____animationsScrollRect_17;
	// UnityEngine.UI.ScrollRect TriLib.Samples.AssetLoaderWindow::_blendShapesScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____blendShapesScrollRect_18;
	// UnityEngine.Transform TriLib.Samples.AssetLoaderWindow::_containerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____containerTransform_19;
	// UnityEngine.Transform TriLib.Samples.AssetLoaderWindow::_blendShapesContainerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____blendShapesContainerTransform_20;
	// TriLib.Samples.AnimationText TriLib.Samples.AssetLoaderWindow::_animationTextPrefab
	AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB * ____animationTextPrefab_21;
	// TriLib.Samples.BlendShapeControl TriLib.Samples.AssetLoaderWindow::_blendShapeControlPrefab
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426 * ____blendShapeControlPrefab_22;
	// UnityEngine.Canvas TriLib.Samples.AssetLoaderWindow::_backgroundCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ____backgroundCanvas_23;
	// UnityEngine.GameObject TriLib.Samples.AssetLoaderWindow::_rootGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____rootGameObject_24;
	// UnityEngine.UI.Text TriLib.Samples.AssetLoaderWindow::_loadingTimeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____loadingTimeText_25;
	// UnityEngine.UI.Text TriLib.Samples.AssetLoaderWindow::_dragAndDropText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____dragAndDropText_26;
	// System.Diagnostics.Stopwatch TriLib.Samples.AssetLoaderWindow::_loadingTimer
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ____loadingTimer_27;

public:
	inline static int32_t get_offset_of_Async_5() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ___Async_5)); }
	inline bool get_Async_5() const { return ___Async_5; }
	inline bool* get_address_of_Async_5() { return &___Async_5; }
	inline void set_Async_5(bool value)
	{
		___Async_5 = value;
	}

	inline static int32_t get_offset_of__loadLocalAssetButton_6() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____loadLocalAssetButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__loadLocalAssetButton_6() const { return ____loadLocalAssetButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__loadLocalAssetButton_6() { return &____loadLocalAssetButton_6; }
	inline void set__loadLocalAssetButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____loadLocalAssetButton_6 = value;
		Il2CppCodeGenWriteBarrier((&____loadLocalAssetButton_6), value);
	}

	inline static int32_t get_offset_of__loadRemoteAssetButton_7() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____loadRemoteAssetButton_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__loadRemoteAssetButton_7() const { return ____loadRemoteAssetButton_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__loadRemoteAssetButton_7() { return &____loadRemoteAssetButton_7; }
	inline void set__loadRemoteAssetButton_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____loadRemoteAssetButton_7 = value;
		Il2CppCodeGenWriteBarrier((&____loadRemoteAssetButton_7), value);
	}

	inline static int32_t get_offset_of__spinningText_8() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____spinningText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__spinningText_8() const { return ____spinningText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__spinningText_8() { return &____spinningText_8; }
	inline void set__spinningText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____spinningText_8 = value;
		Il2CppCodeGenWriteBarrier((&____spinningText_8), value);
	}

	inline static int32_t get_offset_of__transparencyModeDropdown_9() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____transparencyModeDropdown_9)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get__transparencyModeDropdown_9() const { return ____transparencyModeDropdown_9; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of__transparencyModeDropdown_9() { return &____transparencyModeDropdown_9; }
	inline void set__transparencyModeDropdown_9(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		____transparencyModeDropdown_9 = value;
		Il2CppCodeGenWriteBarrier((&____transparencyModeDropdown_9), value);
	}

	inline static int32_t get_offset_of__shadingDropdown_10() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____shadingDropdown_10)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get__shadingDropdown_10() const { return ____shadingDropdown_10; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of__shadingDropdown_10() { return &____shadingDropdown_10; }
	inline void set__shadingDropdown_10(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		____shadingDropdown_10 = value;
		Il2CppCodeGenWriteBarrier((&____shadingDropdown_10), value);
	}

	inline static int32_t get_offset_of__spinXToggle_11() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____spinXToggle_11)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__spinXToggle_11() const { return ____spinXToggle_11; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__spinXToggle_11() { return &____spinXToggle_11; }
	inline void set__spinXToggle_11(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____spinXToggle_11 = value;
		Il2CppCodeGenWriteBarrier((&____spinXToggle_11), value);
	}

	inline static int32_t get_offset_of__spinYToggle_12() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____spinYToggle_12)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__spinYToggle_12() const { return ____spinYToggle_12; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__spinYToggle_12() { return &____spinYToggle_12; }
	inline void set__spinYToggle_12(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____spinYToggle_12 = value;
		Il2CppCodeGenWriteBarrier((&____spinYToggle_12), value);
	}

	inline static int32_t get_offset_of__resetRotationButton_13() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____resetRotationButton_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__resetRotationButton_13() const { return ____resetRotationButton_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__resetRotationButton_13() { return &____resetRotationButton_13; }
	inline void set__resetRotationButton_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____resetRotationButton_13 = value;
		Il2CppCodeGenWriteBarrier((&____resetRotationButton_13), value);
	}

	inline static int32_t get_offset_of__stopAnimationButton_14() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____stopAnimationButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__stopAnimationButton_14() const { return ____stopAnimationButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__stopAnimationButton_14() { return &____stopAnimationButton_14; }
	inline void set__stopAnimationButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____stopAnimationButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____stopAnimationButton_14), value);
	}

	inline static int32_t get_offset_of__animationsText_15() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____animationsText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__animationsText_15() const { return ____animationsText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__animationsText_15() { return &____animationsText_15; }
	inline void set__animationsText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____animationsText_15 = value;
		Il2CppCodeGenWriteBarrier((&____animationsText_15), value);
	}

	inline static int32_t get_offset_of__blendShapesText_16() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____blendShapesText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__blendShapesText_16() const { return ____blendShapesText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__blendShapesText_16() { return &____blendShapesText_16; }
	inline void set__blendShapesText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____blendShapesText_16 = value;
		Il2CppCodeGenWriteBarrier((&____blendShapesText_16), value);
	}

	inline static int32_t get_offset_of__animationsScrollRect_17() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____animationsScrollRect_17)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__animationsScrollRect_17() const { return ____animationsScrollRect_17; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__animationsScrollRect_17() { return &____animationsScrollRect_17; }
	inline void set__animationsScrollRect_17(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____animationsScrollRect_17 = value;
		Il2CppCodeGenWriteBarrier((&____animationsScrollRect_17), value);
	}

	inline static int32_t get_offset_of__blendShapesScrollRect_18() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____blendShapesScrollRect_18)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__blendShapesScrollRect_18() const { return ____blendShapesScrollRect_18; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__blendShapesScrollRect_18() { return &____blendShapesScrollRect_18; }
	inline void set__blendShapesScrollRect_18(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____blendShapesScrollRect_18 = value;
		Il2CppCodeGenWriteBarrier((&____blendShapesScrollRect_18), value);
	}

	inline static int32_t get_offset_of__containerTransform_19() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____containerTransform_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__containerTransform_19() const { return ____containerTransform_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__containerTransform_19() { return &____containerTransform_19; }
	inline void set__containerTransform_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____containerTransform_19 = value;
		Il2CppCodeGenWriteBarrier((&____containerTransform_19), value);
	}

	inline static int32_t get_offset_of__blendShapesContainerTransform_20() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____blendShapesContainerTransform_20)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__blendShapesContainerTransform_20() const { return ____blendShapesContainerTransform_20; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__blendShapesContainerTransform_20() { return &____blendShapesContainerTransform_20; }
	inline void set__blendShapesContainerTransform_20(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____blendShapesContainerTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&____blendShapesContainerTransform_20), value);
	}

	inline static int32_t get_offset_of__animationTextPrefab_21() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____animationTextPrefab_21)); }
	inline AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB * get__animationTextPrefab_21() const { return ____animationTextPrefab_21; }
	inline AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB ** get_address_of__animationTextPrefab_21() { return &____animationTextPrefab_21; }
	inline void set__animationTextPrefab_21(AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB * value)
	{
		____animationTextPrefab_21 = value;
		Il2CppCodeGenWriteBarrier((&____animationTextPrefab_21), value);
	}

	inline static int32_t get_offset_of__blendShapeControlPrefab_22() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____blendShapeControlPrefab_22)); }
	inline BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426 * get__blendShapeControlPrefab_22() const { return ____blendShapeControlPrefab_22; }
	inline BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426 ** get_address_of__blendShapeControlPrefab_22() { return &____blendShapeControlPrefab_22; }
	inline void set__blendShapeControlPrefab_22(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426 * value)
	{
		____blendShapeControlPrefab_22 = value;
		Il2CppCodeGenWriteBarrier((&____blendShapeControlPrefab_22), value);
	}

	inline static int32_t get_offset_of__backgroundCanvas_23() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____backgroundCanvas_23)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get__backgroundCanvas_23() const { return ____backgroundCanvas_23; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of__backgroundCanvas_23() { return &____backgroundCanvas_23; }
	inline void set__backgroundCanvas_23(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		____backgroundCanvas_23 = value;
		Il2CppCodeGenWriteBarrier((&____backgroundCanvas_23), value);
	}

	inline static int32_t get_offset_of__rootGameObject_24() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____rootGameObject_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__rootGameObject_24() const { return ____rootGameObject_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__rootGameObject_24() { return &____rootGameObject_24; }
	inline void set__rootGameObject_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____rootGameObject_24 = value;
		Il2CppCodeGenWriteBarrier((&____rootGameObject_24), value);
	}

	inline static int32_t get_offset_of__loadingTimeText_25() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____loadingTimeText_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__loadingTimeText_25() const { return ____loadingTimeText_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__loadingTimeText_25() { return &____loadingTimeText_25; }
	inline void set__loadingTimeText_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____loadingTimeText_25 = value;
		Il2CppCodeGenWriteBarrier((&____loadingTimeText_25), value);
	}

	inline static int32_t get_offset_of__dragAndDropText_26() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____dragAndDropText_26)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__dragAndDropText_26() const { return ____dragAndDropText_26; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__dragAndDropText_26() { return &____dragAndDropText_26; }
	inline void set__dragAndDropText_26(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____dragAndDropText_26 = value;
		Il2CppCodeGenWriteBarrier((&____dragAndDropText_26), value);
	}

	inline static int32_t get_offset_of__loadingTimer_27() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B, ____loadingTimer_27)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get__loadingTimer_27() const { return ____loadingTimer_27; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of__loadingTimer_27() { return &____loadingTimer_27; }
	inline void set__loadingTimer_27(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		____loadingTimer_27 = value;
		Il2CppCodeGenWriteBarrier((&____loadingTimer_27), value);
	}
};

struct AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_StaticFields
{
public:
	// TriLib.Samples.AssetLoaderWindow TriLib.Samples.AssetLoaderWindow::<Instance>k__BackingField
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERWINDOW_T3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_H
#ifndef BLENDSHAPECONTROL_T12CD6F3F43987A93EFC5FE6F83605F6045202426_H
#define BLENDSHAPECONTROL_T12CD6F3F43987A93EFC5FE6F83605F6045202426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.BlendShapeControl
struct  BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text TriLib.Samples.BlendShapeControl::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_4;
	// UnityEngine.UI.Slider TriLib.Samples.BlendShapeControl::_slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____slider_5;
	// UnityEngine.SkinnedMeshRenderer TriLib.Samples.BlendShapeControl::SkinnedMeshRenderer
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___SkinnedMeshRenderer_6;
	// UnityEngine.Animation TriLib.Samples.BlendShapeControl::_animation
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ____animation_7;
	// System.Int32 TriLib.Samples.BlendShapeControl::BlendShapeIndex
	int32_t ___BlendShapeIndex_8;
	// System.Boolean TriLib.Samples.BlendShapeControl::_ignoreValueChange
	bool ____ignoreValueChange_9;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ____text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_4() const { return ____text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of__slider_5() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ____slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__slider_5() const { return ____slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__slider_5() { return &____slider_5; }
	inline void set__slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____slider_5 = value;
		Il2CppCodeGenWriteBarrier((&____slider_5), value);
	}

	inline static int32_t get_offset_of_SkinnedMeshRenderer_6() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ___SkinnedMeshRenderer_6)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_SkinnedMeshRenderer_6() const { return ___SkinnedMeshRenderer_6; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_SkinnedMeshRenderer_6() { return &___SkinnedMeshRenderer_6; }
	inline void set_SkinnedMeshRenderer_6(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___SkinnedMeshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___SkinnedMeshRenderer_6), value);
	}

	inline static int32_t get_offset_of__animation_7() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ____animation_7)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get__animation_7() const { return ____animation_7; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of__animation_7() { return &____animation_7; }
	inline void set__animation_7(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		____animation_7 = value;
		Il2CppCodeGenWriteBarrier((&____animation_7), value);
	}

	inline static int32_t get_offset_of_BlendShapeIndex_8() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ___BlendShapeIndex_8)); }
	inline int32_t get_BlendShapeIndex_8() const { return ___BlendShapeIndex_8; }
	inline int32_t* get_address_of_BlendShapeIndex_8() { return &___BlendShapeIndex_8; }
	inline void set_BlendShapeIndex_8(int32_t value)
	{
		___BlendShapeIndex_8 = value;
	}

	inline static int32_t get_offset_of__ignoreValueChange_9() { return static_cast<int32_t>(offsetof(BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426, ____ignoreValueChange_9)); }
	inline bool get__ignoreValueChange_9() const { return ____ignoreValueChange_9; }
	inline bool* get_address_of__ignoreValueChange_9() { return &____ignoreValueChange_9; }
	inline void set__ignoreValueChange_9(bool value)
	{
		____ignoreValueChange_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPECONTROL_T12CD6F3F43987A93EFC5FE6F83605F6045202426_H
#ifndef CUSTOMIOLOADSAMPLE_T60F870BA7DC31A1B28797FD7E04A1B45233DB258_H
#define CUSTOMIOLOADSAMPLE_T60F870BA7DC31A1B28797FD7E04A1B45233DB258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.CustomIOLoadSample
struct  CustomIOLoadSample_t60F870BA7DC31A1B28797FD7E04A1B45233DB258  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIOLOADSAMPLE_T60F870BA7DC31A1B28797FD7E04A1B45233DB258_H
#ifndef DOWNLOADSAMPLE_T009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB_H
#define DOWNLOADSAMPLE_T009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.DownloadSample
struct  DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] TriLib.Samples.DownloadSample::urls
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___urls_4;
	// UnityEngine.Networking.UnityWebRequest[] TriLib.Samples.DownloadSample::_fileDownloaders
	UnityWebRequestU5BU5D_tE3BAE093C0057E3FF8862136282E47343BCADC7C* ____fileDownloaders_5;
	// UnityEngine.GameObject TriLib.Samples.DownloadSample::_loadedGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____loadedGameObject_6;

public:
	inline static int32_t get_offset_of_urls_4() { return static_cast<int32_t>(offsetof(DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB, ___urls_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_urls_4() const { return ___urls_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_urls_4() { return &___urls_4; }
	inline void set_urls_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___urls_4 = value;
		Il2CppCodeGenWriteBarrier((&___urls_4), value);
	}

	inline static int32_t get_offset_of__fileDownloaders_5() { return static_cast<int32_t>(offsetof(DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB, ____fileDownloaders_5)); }
	inline UnityWebRequestU5BU5D_tE3BAE093C0057E3FF8862136282E47343BCADC7C* get__fileDownloaders_5() const { return ____fileDownloaders_5; }
	inline UnityWebRequestU5BU5D_tE3BAE093C0057E3FF8862136282E47343BCADC7C** get_address_of__fileDownloaders_5() { return &____fileDownloaders_5; }
	inline void set__fileDownloaders_5(UnityWebRequestU5BU5D_tE3BAE093C0057E3FF8862136282E47343BCADC7C* value)
	{
		____fileDownloaders_5 = value;
		Il2CppCodeGenWriteBarrier((&____fileDownloaders_5), value);
	}

	inline static int32_t get_offset_of__loadedGameObject_6() { return static_cast<int32_t>(offsetof(DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB, ____loadedGameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__loadedGameObject_6() const { return ____loadedGameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__loadedGameObject_6() { return &____loadedGameObject_6; }
	inline void set__loadedGameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____loadedGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&____loadedGameObject_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADSAMPLE_T009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB_H
#ifndef ERRORDIALOG_TD573B10DB6C2610E9622AEED7F96332D1608DAD7_H
#define ERRORDIALOG_TD573B10DB6C2610E9622AEED7F96332D1608DAD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.ErrorDialog
struct  ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button TriLib.Samples.ErrorDialog::_okButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____okButton_5;
	// UnityEngine.UI.InputField TriLib.Samples.ErrorDialog::_errorText
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____errorText_6;
	// UnityEngine.GameObject TriLib.Samples.ErrorDialog::_rendererGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____rendererGameObject_7;

public:
	inline static int32_t get_offset_of__okButton_5() { return static_cast<int32_t>(offsetof(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7, ____okButton_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__okButton_5() const { return ____okButton_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__okButton_5() { return &____okButton_5; }
	inline void set__okButton_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____okButton_5 = value;
		Il2CppCodeGenWriteBarrier((&____okButton_5), value);
	}

	inline static int32_t get_offset_of__errorText_6() { return static_cast<int32_t>(offsetof(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7, ____errorText_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__errorText_6() const { return ____errorText_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__errorText_6() { return &____errorText_6; }
	inline void set__errorText_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____errorText_6 = value;
		Il2CppCodeGenWriteBarrier((&____errorText_6), value);
	}

	inline static int32_t get_offset_of__rendererGameObject_7() { return static_cast<int32_t>(offsetof(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7, ____rendererGameObject_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__rendererGameObject_7() const { return ____rendererGameObject_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__rendererGameObject_7() { return &____rendererGameObject_7; }
	inline void set__rendererGameObject_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____rendererGameObject_7 = value;
		Il2CppCodeGenWriteBarrier((&____rendererGameObject_7), value);
	}
};

struct ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7_StaticFields
{
public:
	// TriLib.Samples.ErrorDialog TriLib.Samples.ErrorDialog::<Instance>k__BackingField
	ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORDIALOG_TD573B10DB6C2610E9622AEED7F96332D1608DAD7_H
#ifndef FILEOPENDIALOG_T153A5E25B2CEB15E10959B525070569343D6A690_H
#define FILEOPENDIALOG_T153A5E25B2CEB15E10959B525070569343D6A690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.FileOpenDialog
struct  FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String TriLib.Samples.FileOpenDialog::Filter
	String_t* ___Filter_5;
	// TriLib.Samples.FileOpenEventHandle TriLib.Samples.FileOpenDialog::OnFileOpen
	FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0 * ___OnFileOpen_6;
	// UnityEngine.Transform TriLib.Samples.FileOpenDialog::_containerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____containerTransform_7;
	// TriLib.Samples.FileText TriLib.Samples.FileOpenDialog::_fileTextPrefab
	FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9 * ____fileTextPrefab_8;
	// UnityEngine.GameObject TriLib.Samples.FileOpenDialog::_fileLoaderRenderer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____fileLoaderRenderer_9;
	// UnityEngine.UI.Button TriLib.Samples.FileOpenDialog::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_10;
	// UnityEngine.UI.Text TriLib.Samples.FileOpenDialog::_headerText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____headerText_11;
	// System.String TriLib.Samples.FileOpenDialog::_directory
	String_t* ____directory_12;

public:
	inline static int32_t get_offset_of_Filter_5() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ___Filter_5)); }
	inline String_t* get_Filter_5() const { return ___Filter_5; }
	inline String_t** get_address_of_Filter_5() { return &___Filter_5; }
	inline void set_Filter_5(String_t* value)
	{
		___Filter_5 = value;
		Il2CppCodeGenWriteBarrier((&___Filter_5), value);
	}

	inline static int32_t get_offset_of_OnFileOpen_6() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ___OnFileOpen_6)); }
	inline FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0 * get_OnFileOpen_6() const { return ___OnFileOpen_6; }
	inline FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0 ** get_address_of_OnFileOpen_6() { return &___OnFileOpen_6; }
	inline void set_OnFileOpen_6(FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0 * value)
	{
		___OnFileOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnFileOpen_6), value);
	}

	inline static int32_t get_offset_of__containerTransform_7() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____containerTransform_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__containerTransform_7() const { return ____containerTransform_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__containerTransform_7() { return &____containerTransform_7; }
	inline void set__containerTransform_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____containerTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&____containerTransform_7), value);
	}

	inline static int32_t get_offset_of__fileTextPrefab_8() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____fileTextPrefab_8)); }
	inline FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9 * get__fileTextPrefab_8() const { return ____fileTextPrefab_8; }
	inline FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9 ** get_address_of__fileTextPrefab_8() { return &____fileTextPrefab_8; }
	inline void set__fileTextPrefab_8(FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9 * value)
	{
		____fileTextPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&____fileTextPrefab_8), value);
	}

	inline static int32_t get_offset_of__fileLoaderRenderer_9() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____fileLoaderRenderer_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__fileLoaderRenderer_9() const { return ____fileLoaderRenderer_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__fileLoaderRenderer_9() { return &____fileLoaderRenderer_9; }
	inline void set__fileLoaderRenderer_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____fileLoaderRenderer_9 = value;
		Il2CppCodeGenWriteBarrier((&____fileLoaderRenderer_9), value);
	}

	inline static int32_t get_offset_of__closeButton_10() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____closeButton_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_10() const { return ____closeButton_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_10() { return &____closeButton_10; }
	inline void set__closeButton_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_10 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_10), value);
	}

	inline static int32_t get_offset_of__headerText_11() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____headerText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__headerText_11() const { return ____headerText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__headerText_11() { return &____headerText_11; }
	inline void set__headerText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____headerText_11 = value;
		Il2CppCodeGenWriteBarrier((&____headerText_11), value);
	}

	inline static int32_t get_offset_of__directory_12() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690, ____directory_12)); }
	inline String_t* get__directory_12() const { return ____directory_12; }
	inline String_t** get_address_of__directory_12() { return &____directory_12; }
	inline void set__directory_12(String_t* value)
	{
		____directory_12 = value;
		Il2CppCodeGenWriteBarrier((&____directory_12), value);
	}
};

struct FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690_StaticFields
{
public:
	// TriLib.Samples.FileOpenDialog TriLib.Samples.FileOpenDialog::<Instance>k__BackingField
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEOPENDIALOG_T153A5E25B2CEB15E10959B525070569343D6A690_H
#ifndef FILETEXT_TE032034CE13EBB5A135F6CD36D7A69C7395D66C9_H
#define FILETEXT_TE032034CE13EBB5A135F6CD36D7A69C7395D66C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.FileText
struct  FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TriLib.Samples.ItemType TriLib.Samples.FileText::<ItemType>k__BackingField
	int32_t ___U3CItemTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CItemTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9, ___U3CItemTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CItemTypeU3Ek__BackingField_4() const { return ___U3CItemTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CItemTypeU3Ek__BackingField_4() { return &___U3CItemTypeU3Ek__BackingField_4; }
	inline void set_U3CItemTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CItemTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILETEXT_TE032034CE13EBB5A135F6CD36D7A69C7395D66C9_H
#ifndef LOADSAMPLE_T170D97D6A8248B38D65A113D89EB542E0E5A9EB5_H
#define LOADSAMPLE_T170D97D6A8248B38D65A113D89EB542E0E5A9EB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.LoadSample
struct  LoadSample_t170D97D6A8248B38D65A113D89EB542E0E5A9EB5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSAMPLE_T170D97D6A8248B38D65A113D89EB542E0E5A9EB5_H
#ifndef LOADSAMPLEASYNC_T8427B705F0B151FBF9CD6CEF3CA7492F55410233_H
#define LOADSAMPLEASYNC_T8427B705F0B151FBF9CD6CEF3CA7492F55410233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Samples.LoadSampleAsync
struct  LoadSampleAsync_t8427B705F0B151FBF9CD6CEF3CA7492F55410233  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSAMPLEASYNC_T8427B705F0B151FBF9CD6CEF3CA7492F55410233_H
#ifndef POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#define POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_4;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_5;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_6;

public:
	inline static int32_t get_offset_of_supportHDRTextures_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportHDRTextures_4)); }
	inline bool get_supportHDRTextures_4() const { return ___supportHDRTextures_4; }
	inline bool* get_address_of_supportHDRTextures_4() { return &___supportHDRTextures_4; }
	inline void set_supportHDRTextures_4(bool value)
	{
		___supportHDRTextures_4 = value;
	}

	inline static int32_t get_offset_of_supportDX11_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___supportDX11_5)); }
	inline bool get_supportDX11_5() const { return ___supportDX11_5; }
	inline bool* get_address_of_supportDX11_5() { return &___supportDX11_5; }
	inline void set_supportDX11_5(bool value)
	{
		___supportDX11_5 = value;
	}

	inline static int32_t get_offset_of_isSupported_6() { return static_cast<int32_t>(offsetof(PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E, ___isSupported_6)); }
	inline bool get_isSupported_6() const { return ___isSupported_6; }
	inline bool* get_address_of_isSupported_6() { return &___isSupported_6; }
	inline void set_isSupported_6(bool value)
	{
		___isSupported_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_TCC43BD8E385DF43317DC6AFE25A14C83B5A0011E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3800[3] = 
{
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E::get_offset_of_supportHDRTextures_4(),
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E::get_offset_of_supportDX11_5(),
	PostEffectsBase_tCC43BD8E385DF43317DC6AFE25A14C83B5A0011E::get_offset_of_isSupported_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (STBImageLoader_tCF4C2C2C8803ACA300A9A5A5563AE4AB6ECFB397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (STBImageInterop_t7D506B29404C96763AD60B7E0A3129BD18A435F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3802[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (AssetAdvancedConfigType_t3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3803[10] = 
{
	AssetAdvancedConfigType_t3CDEA9295BAD6A947C5974BF93B8D329FA6A06F5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (AiPrimitiveType_t495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3804[5] = 
{
	AiPrimitiveType_t495FD7E7BAA0FFAB40FBC0767FF1E60BA5F4D7AF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (AiComponent_tB270F1F381D082C09A5DF63FF716C7879BD7DB65)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3805[12] = 
{
	AiComponent_tB270F1F381D082C09A5DF63FF716C7879BD7DB65::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (AiUVTransform_t2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3806[4] = 
{
	AiUVTransform_t2F5E61F2C5F138A9AA22A9C7D2BB7222358A38EF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (AssetAdvancedPropertyClassNames_t9BB3EAEF233946A026307A628D5522C0D37148D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3807[67] = 
{
	AssetAdvancedPropertyClassNames_t9BB3EAEF233946A026307A628D5522C0D37148D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D), -1, sizeof(AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3808[2] = 
{
	0,
	AssetAdvancedPropertyMetadata_t1E296B2BEEE4EE6CC3E4891637AFD9A9A6E80B2D_StaticFields::get_offset_of_ConfigKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3809[8] = 
{
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_Key_0(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_IntValue_1(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_FloatValue_2(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_BoolValue_3(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_StringValue_4(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_TranslationValue_5(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_RotationValue_6(),
	AssetAdvancedConfig_t96547C7010BD6581D388F25556CADCB259E04B33::get_offset_of_ScaleValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3810[11] = 
{
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_AutoStart_4(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_AssetURI_5(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_Timeout_6(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_AssetExtension_7(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_WrapperGameObject_8(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_ShowProgress_9(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_Async_10(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of_ProgressCallback_11(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of__unityWebRequest_12(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of__centeredStyle_13(),
	AssetDownloader_t190FFB556B68044A594D822E6A54B7562ED1A6B8::get_offset_of__error_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[9] = 
{
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_U3CU3E1__state_0(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_U3CU3E2__current_1(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_U3CU3E4__this_2(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_assetUri_3(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_assetExtension_4(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_options_5(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_wrapperGameObject_6(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_onAssetLoaded_7(),
	U3CDoDownloadAssetU3Ed__22_t8DA5910B3ABC7EE040E040D8770AF91BDF94A5D1::get_offset_of_progressCallback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (AssetLoader_t3E59CF528778B559EAF7F55C6E61A31D07DA917E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (AssetLoaderAsync_tF198DE6AB2FC2E41AE06C2B5E31AFD94C4A3AF40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[8] = 
{
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_filename_1(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_basePath_2(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_options_3(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_usesWrapperGameObject_4(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_progressCallback_5(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_wrapperGameObject_6(),
	U3CU3Ec__DisplayClass0_0_t0E65EF1CB609FD8B7ADE4F22B7DADE8172C528DE::get_offset_of_onAssetLoaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[12] = 
{
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_fileBytes_1(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_filename_2(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_basePath_3(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_options_4(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_usesWrapperGameObject_5(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_dataCallback_6(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_existsCallback_7(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_loadTextureDataCallback_8(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_progressCallback_9(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_wrapperGameObject_10(),
	U3CU3Ec__DisplayClass1_0_t9A85B327E9E021EA37CF8888B6B841023D3ED93C::get_offset_of_onAssetLoaded_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3816[9] = 
{
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_filename_0(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_extension_2(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_basePath_3(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_options_4(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_usesWrapperGameObject_5(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_progressCallback_6(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_wrapperGameObject_7(),
	U3CU3Ec__DisplayClass2_0_t414D895D6AB9E83D9C944887ED15188FB5087C2B::get_offset_of_onAssetLoaded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3817[11] = 
{
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_fileData_1(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_assetExtension_2(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_basePath_3(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_options_4(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_usesWrapperGameObject_5(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_dataCallback_6(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_existsCallback_7(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_progressCallback_8(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_wrapperGameObject_9(),
	U3CU3Ec__DisplayClass3_0_t355CE05A0C57252B59BE03FF5BCBC47B17A86189::get_offset_of_onAssetLoaded_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (ObjectLoadedHandle_tFC78DA10EF9DC4FD267AAD49CBDE0512FFC9789B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (MeshCreatedHandle_t44A4BFA9BEA02EBBA0FDFF8D7C41DEF9A14F7BF5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (MaterialCreatedHandle_t4D5947E036CE039B5555795C3600179DD199B541), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (AvatarCreatedHandle_tBE0BFF7A3E2A6C8EE6EE830761D973871026C9F2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (AnimationClipCreatedHandle_t663B09E75EEE053CF2A6BB826C31485A0B69DC20), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (MetadataProcessedHandle_t9CEFC8DFED53FBC773B7886D759B915EFF567E4A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (BlendShapeKeyCreatedHandle_t32BAC592C9CF57884413E34D52CEA3E601F44E28), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (LoadTextureDataCallback_t158DEEE02D9DF7A554468AB4FE3D395AF3AF17F1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (EmbeddedTextureLoadCallback_t9AD05D737F8039AD7FB3A2F2E9AA08BB9B2E2213), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88), -1, sizeof(AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3827[40] = 
{
	0,
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_RootNodeData_1(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_MaterialData_2(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_MeshData_3(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_AnimationData_4(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_CameraData_5(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_Metadata_6(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_NodesPath_7(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_LoadedMaterials_8(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_LoadedTextures_9(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_LoadedBoneNames_10(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_MeshDataConnections_11(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_EmbeddedTextures_12(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_FilesLoadData_13(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardBaseMaterial_14(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardSpecularMaterial_15(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardBaseAlphaMaterial_16(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardSpecularAlphaMaterial_17(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardBaseCutoutMaterial_18(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardBaseFadeMaterial_19(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardSpecularCutoutMaterial_20(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardSpecularFadeMaterial_21(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardRoughnessMaterial_22(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardRoughnessCutoutMaterial_23(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardRoughnessFadeMaterial_24(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_StandardRoughnessAlphaMaterial_25(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88_StaticFields::get_offset_of_NotFoundTexture_26(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_NodeId_27(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_HasBoneInfo_28(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_HasBlendShapes_29(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_Scene_30(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_EmbeddedTextureLoad_31(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnMeshCreated_32(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnMaterialCreated_33(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnTextureLoaded_34(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnAvatarCreated_35(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnAnimationClipCreated_36(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnObjectLoaded_37(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnMetadataProcessed_38(),
	AssetLoaderBase_tF7076F38295F492120A3F5B0F0161C9CC304FA88::get_offset_of_OnBlendShapeKeyCreated_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[53] = 
{
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_AddAssetUnloader_4(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadAnimations_5(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ForceAnimationComponents_6(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontApplyAnimations_7(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadLights_8(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadCameras_9(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_AutoPlayAnimations_10(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_AnimationWrapMode_11(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_UseLegacyAnimations_12(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_EnsureQuaternionContinuity_13(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_UseOriginalPositionRotationAndScale_14(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_AnimatorController_15(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_Avatar_16(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontGenerateAvatar_17(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadMetadata_18(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontAddMetadataCollection_19(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadMaterials_20(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyColorAlpha_21(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyDiffuseColor_22(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyEmissionColor_23(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplySpecularColor_24(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyDiffuseTexture_25(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyEmissionTexture_26(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplySpecularTexture_27(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyNormalTexture_28(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyDisplacementTexture_29(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyOcclusionTexture_30(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyMetallicTexture_31(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyNormalScale_32(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyGlossiness_33(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyGlossinessScale_34(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_LoadRawMaterialProperties_35(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DisableAlphaMaterials_36(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ApplyAlphaMaterials_37(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ScanForAlphaMaterials_38(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_UseCutoutMaterials_39(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_UseStandardSpecularMaterial_40(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_MaterialShadingMode_41(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_MaterialTransparencyMode_42(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadMeshes_43(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadBlendShapes_44(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_DontLoadSkinning_45(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_CombineMeshes_46(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_Use32BitsIndexFormat_47(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_GenerateMeshColliders_48(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_ConvexMeshColliders_49(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_RotationAngles_50(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_Scale_51(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_PostProcessSteps_52(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_TextureCompression_53(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_TextureFilterMode_54(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_GenerateMipMaps_55(),
	AssetLoaderOptions_tC563B6E9A16FB6D2C826B13E6B5B130C2D6ECD6B::get_offset_of_AdvancedConfigs_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (AssetLoaderZip_t5FA917BD7F15D2BB818B96E6F6C64D1C2FB9D5E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[8] = 
{
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_filename_1(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_basePath_2(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_options_3(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_usesWrapperGameObject_4(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_progressCallback_5(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_wrapperGameObject_6(),
	U3CU3Ec__DisplayClass5_0_t3DEF68C2B5CB4802B96C513865F290644E21A425::get_offset_of_onAssetLoaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (AssetUnloader_tB7875EB167F1DDB05B955E4A7D844F111F2BA350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (AssimpPostProcessSteps_t320A3541BDF0C114643DD8A61E5A80E85C71E910)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3832[28] = 
{
	AssimpPostProcessSteps_t320A3541BDF0C114643DD8A61E5A80E85C71E910::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (AssimpProcessPreset_t249D29621F5080E78F1BAAEFFB40AE7B50E067F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3833[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7), -1, sizeof(AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3834[5] = 
{
	0,
	0,
	0,
	AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields::get_offset_of_Is32Bits_3(),
	AssimpInterop_t878374C66C809A09BD6354E36DF6928BB007BEB7_StaticFields::get_offset_of_IntSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (DataCallback_t61B4A7D4712326A93AD3F640D9BFD006932FA9BD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (ExistsCallback_tDF03E0AE214CB8BF9CC5640A3DF6EAAA34E84B35), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (ProgressCallback_tF2DD2577DB92078BE5DCAF16C1E1EEE17693CB77), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (AssimpMetadataCollection_t06E569CB384D28757282A759E71F881A8C012743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3838[1] = 
{
	AssimpMetadataCollection_t06E569CB384D28757282A759E71F881A8C012743::get_offset_of__metadataDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[2] = 
{
	AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6::get_offset_of_NodeName_0(),
	AnimationChannelData_t7A6B0148C5A8368611393B2324A4FAF0EFC6D8C6::get_offset_of_CurveData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[3] = 
{
	AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429::get_offset_of_Keyframes_0(),
	AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429::get_offset_of__index_1(),
	AnimationCurveData_tB4A0FA315273780FFDC8DBEE9D21A45414B7B429::get_offset_of_AnimationCurve_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[8] = 
{
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_Name_0(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_Legacy_1(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_Length_2(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_FrameRate_3(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_WrapMode_4(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_ChannelData_5(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_MorphData_6(),
	AnimationData_t77CFD17222FBAB4128D5EEBA5FBB8F20928611C2::get_offset_of_AnimationClip_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (AssimpMetadataType_t254B012678393BA926711B6A551A19A27DFCD927)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3842[8] = 
{
	AssimpMetadataType_t254B012678393BA926711B6A551A19A27DFCD927::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[4] = 
{
	AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6::get_offset_of_MetadataType_0(),
	AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6::get_offset_of_MetadataIndex_1(),
	AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6::get_offset_of_MetadataKey_2(),
	AssimpMetadata_tB652CF827E7588BB133F5EEDC6D9E158E02756E6::get_offset_of_MetadataValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[9] = 
{
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_Name_0(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_Aspect_1(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_NearClipPlane_2(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_FarClipPlane_3(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_FieldOfView_4(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_LocalPosition_5(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_Forward_6(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_Up_7(),
	CameraData_tE17C01D01F9B4BF95FE67D6D51607845C65F5D52::get_offset_of_Camera_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3845[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (DataDisposalCallback_tECF4CAD57FA4BBB74F300069101E48B9A53DB06B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[8] = 
{
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_Data_0(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_DataPointer_1(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_DataLength_2(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_OnDataDisposal_3(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_Width_4(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_Height_5(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_NumChannels_6(),
	EmbeddedTextureData_tCDD936C843A64317194277997B78B728B35F9479::get_offset_of_IsRawData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (BrowserLoadData_t1DD62A7BC54412F7DEE84D00B45C0B44205863A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3848[1] = 
{
	BrowserLoadData_t1DD62A7BC54412F7DEE84D00B45C0B44205863A6::get_offset_of_FilesCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (GCFileLoadData_t4429AD2365C86C04ADF085BF969AD7A2CDBB7485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3849[1] = 
{
	GCFileLoadData_t4429AD2365C86C04ADF085BF969AD7A2CDBB7485::get_offset_of__lockedBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[2] = 
{
	FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7::get_offset_of_Filename_0(),
	FileLoadData_t1DB464F7A4A2E23D988714D7E50B250E038C36C7::get_offset_of_BasePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3851[6] = 
{
	aiPropertyTypeInfo_t1DC25105933923D11C343BB9EE10D64C4D7235AF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[66] = 
{
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_Name_0(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_AlphaLoaded_1(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_Alpha_2(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseInfoLoaded_3(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffusePath_4(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseWrapMode_5(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseName_6(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseBlendMode_7(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseOp_8(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseEmbeddedTextureData_9(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseColorLoaded_10(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_DiffuseColor_11(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionInfoLoaded_12(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionPath_13(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionWrapMode_14(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionName_15(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionBlendMode_16(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionOp_17(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionEmbeddedTextureData_18(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionColorLoaded_19(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_EmissionColor_20(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularInfoLoaded_21(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularPath_22(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularWrapMode_23(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularName_24(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularBlendMode_25(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularOp_26(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularEmbeddedTextureData_27(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularColorLoaded_28(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_SpecularColor_29(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalInfoLoaded_30(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalPath_31(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalWrapMode_32(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalName_33(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalBlendMode_34(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalOp_35(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_NormalEmbeddedTextureData_36(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightInfoLoaded_37(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightPath_38(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightWrapMode_39(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightName_40(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightBlendMode_41(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightOp_42(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_HeightEmbeddedTextureData_43(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_BumpScaleLoaded_44(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_BumpScale_45(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_GlossinessLoaded_46(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_Glossiness_47(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_GlossMapScaleLoaded_48(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_GlossMapScale_49(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionInfoLoaded_50(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionPath_51(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionWrapMode_52(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionName_53(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionBlendMode_54(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionOp_55(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_OcclusionEmbeddedTextureData_56(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicInfoLoaded_57(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicPath_58(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicWrapMode_59(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicName_60(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicBlendMode_61(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicOp_62(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_MetallicEmbeddedTextureData_63(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_Properties_64(),
	MaterialData_t26ED92447B6B9FB0B0A638637EC1E44445BBF41A::get_offset_of_Material_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[19] = 
{
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Name_0(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_SubMeshName_1(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Vertices_2(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Normals_3(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Tangents_4(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_BiTangents_5(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Uv_6(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Uv1_7(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Uv2_8(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Uv3_9(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Colors_10(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Triangles_11(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_HasBoneInfo_12(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_BindPoses_13(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_BoneNames_14(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_BoneWeights_15(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_MaterialIndex_16(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_MorphsData_17(),
	MeshData_t0A760AADBD6148FDA217567E2C0FB4E689531EAD::get_offset_of_Mesh_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[2] = 
{
	MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819::get_offset_of_NodeName_0(),
	MorphChannelData_t21955FCBCECB3B96FED28C7326F63691EBD1E819::get_offset_of_MorphChannelKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3857[2] = 
{
	MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5::get_offset_of_Indices_0(),
	MorphChannelKey_tD497F720ADE0509A15B4152420FE66DD2685E3C5::get_offset_of_Weights_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3858[5] = 
{
	MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4::get_offset_of_Name_0(),
	MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4::get_offset_of_Vertices_1(),
	MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4::get_offset_of_Normals_2(),
	MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4::get_offset_of_Tangents_3(),
	MorphData_tBE7F729F0DB98D50E1D135AEC1E3A1806E5618A4::get_offset_of_Weight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (NodeData_t457462F69494965439509DD564F2B853F08F6FF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3859[8] = 
{
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Name_0(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Path_1(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Matrix_2(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Meshes_3(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Parent_4(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Children_5(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_GameObject_6(),
	NodeData_t457462F69494965439509DD564F2B853F08F6FF9::get_offset_of_Metadata_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (MaterialShadingMode_t526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3860[4] = 
{
	MaterialShadingMode_t526BB04E457CFF6BCCBEEA75E32F60C8C937D3DB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (MaterialTransparencyMode_tA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3861[4] = 
{
	MaterialTransparencyMode_tA5263604AB8B4A0F0D54564D29FCE1ABAF534F5C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (CameraExtensions_t52A82873EA1CF46FD0A2271F3617F2EE91C78F00), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5), -1, sizeof(Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3863[4] = 
{
	Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields::get_offset_of__instance_4(),
	Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields::get_offset_of__instanceExists_5(),
	Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields::get_offset_of_LockObject_6(),
	Dispatcher_tAF1179DE6A762B560B21E145F6DFF2F37AA23CD5_StaticFields::get_offset_of_Actions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (FileUtils_t526176EC05822EED286A026F0A00CBD6853B9B4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (BrowserFilesLoadedEvent_t3D8D983456FC9D5949010D7885E2FE13B476E834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913), -1, sizeof(JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3866[1] = 
{
	JSHelper_tFEDCEE942A02C639F6F09DD63936CCF45703B913_StaticFields::get_offset_of__instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (MatrixExtensions_t24B98782A6128043B166DE80B05EAF663B3BE019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (StreamUtils_tBD2306C4BBE7C954429B4056BC1AD309BE4BF5EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (StringUtils_t456B1DFB8084650B9F77922AB3FE024B7AF365CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3870[4] = 
{
	TextureCompression_tBE9654CBF04C930D6B3F935CAA06C2F569A514A5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (TextureLoadHandle_tD0A09C67FE71DB866F00A2D53129F6DAE0C4F72C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (TexturePreLoadHandle_t1F4AEFE250AE46F35577FA3B53AFD9D756D41B24), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (Texture2DUtils_t113F1FD32D931B6C889F535639B2636DFFBC65DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (ThreadUtils_tC54E980B81AFB99EAA5C04548985934AD90B7C49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[2] = 
{
	U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7::get_offset_of_action_0(),
	U3CU3Ec__DisplayClass0_0_tEF97B9BE44762A00375B06496F40CDADCE95BCC7::get_offset_of_onComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[1] = 
{
	U3CU3Ec__DisplayClass0_1_tE4017F41B9BDB0274A9E042BA8930B0EFEFB5A68::get_offset_of_exception_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (TransformExtensions_tF34DF4728FF207B44EF4E4213DB8F391CD4CAABE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[8] = 
{
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of_FreeLookCamPrefab_4(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of_ThirdPersonControllerPrefab_5(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of_ActiveCameraGameObject_6(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of_ModelsDirectory_7(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of__files_8(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of__windowRect_9(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of__scrollPosition_10(),
	AvatarLoaderSample_tE624280BAB9E309FA69447AE96FF213DD9E8130B::get_offset_of__avatarLoader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (U3CU3Ec__DisplayClass8_0_t51CB3797E623374909EB0DB46AD5E061B6997FF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[1] = 
{
	U3CU3Ec__DisplayClass8_0_t51CB3797E623374909EB0DB46AD5E061B6997FF1::get_offset_of_supportedExtensions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F), -1, sizeof(AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3880[16] = 
{
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_CurrentAvatar_4(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_RuntimeAnimatorController_5(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_ArmStretch_6(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_FeetSpacing_7(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_HasTranslationDof_8(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_LegStretch_9(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_LowerArmTwist_10(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_LowerLegTwist_11(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_UpperArmTwist_12(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_UpperLegTwist_13(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_Scale_14(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_HeightOffset_15(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of_CustomBoneNames_16(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields::get_offset_of_BipedBoneNames_17(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F_StaticFields::get_offset_of_MixamoBoneNames_18(),
	AvatarLoader_t4CE8105BE55427205855FC930DFF49C0FA3B2A4F::get_offset_of__loaderOptions_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[3] = 
{
	BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8::get_offset_of_HumanBone_0(),
	BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8::get_offset_of_BoneName_1(),
	BoneRelationship_t4B2A41D4311115E4428E32E5C2BCE6664DDBA9A8::get_offset_of_Optional_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[1] = 
{
	BoneRelationshipList_t87AF0272CB6927BDE06500D48A4514AD11EF803A::get_offset_of__relationships_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (AnimationText_t8AD3C674AA82A4A53A36BC098DB416C14E75A7FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[1] = 
{
	AssetDownloaderZIP_tEC96A3AA2485E56BAB55B741E2A4A3D769609322::get_offset_of__fileDownloader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[7] = 
{
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_url_3(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_fileExtension_4(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_localFilePath_5(),
	U3CDownloadFileU3Ed__6_tACEA013908769DA01F76866D839FEFEF01696EBD::get_offset_of_localFilename_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B), -1, sizeof(AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3886[24] = 
{
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of_Async_5(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__loadLocalAssetButton_6(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__loadRemoteAssetButton_7(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__spinningText_8(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__transparencyModeDropdown_9(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__shadingDropdown_10(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__spinXToggle_11(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__spinYToggle_12(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__resetRotationButton_13(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__stopAnimationButton_14(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__animationsText_15(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__blendShapesText_16(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__animationsScrollRect_17(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__blendShapesScrollRect_18(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__containerTransform_19(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__blendShapesContainerTransform_20(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__animationTextPrefab_21(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__blendShapeControlPrefab_22(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__backgroundCanvas_23(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__rootGameObject_24(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__loadingTimeText_25(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__dragAndDropText_26(),
	AssetLoaderWindow_t3DB17EE5B298CA2D18DD11692BB54C1079B6C57B::get_offset_of__loadingTimer_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[2] = 
{
	U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE::get_offset_of_assetLoader_0(),
	U3CU3Ec__DisplayClass38_0_tF933BE86382838062FC4C544882293B0CBF7C0DE::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[6] = 
{
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of__text_4(),
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of__slider_5(),
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of_SkinnedMeshRenderer_6(),
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of__animation_7(),
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of_BlendShapeIndex_8(),
	BlendShapeControl_t12CD6F3F43987A93EFC5FE6F83605F6045202426::get_offset_of__ignoreValueChange_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (CustomIOLoadSample_t60F870BA7DC31A1B28797FD7E04A1B45233DB258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[3] = 
{
	DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB::get_offset_of_urls_4(),
	DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB::get_offset_of__fileDownloaders_5(),
	DownloadSample_t009F01A92EC234B32F5ACD3F84767D1E7C7EBAEB::get_offset_of__loadedGameObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3891[8] = 
{
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_index_3(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_url_4(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_fileExtension_5(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_localFilePath_6(),
	U3CDownloadFileU3Ed__7_tDCA38118765851B26AA54908C14315389F287B59::get_offset_of_localFilename_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7), -1, sizeof(ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3892[4] = 
{
	ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7::get_offset_of__okButton_5(),
	ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7::get_offset_of__errorText_6(),
	ErrorDialog_tD573B10DB6C2610E9622AEED7F96332D1608DAD7::get_offset_of__rendererGameObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (FileOpenEventHandle_t52FBAD3455607D930E8D983FA25B31D7B10DCCE0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (ItemType_t38E944E287660D759F6138EABFEBBC16B2E4D036)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[4] = 
{
	ItemType_t38E944E287660D759F6138EABFEBBC16B2E4D036::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690), -1, sizeof(FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3895[9] = 
{
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of_Filter_5(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of_OnFileOpen_6(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__containerTransform_7(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__fileTextPrefab_8(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__fileLoaderRenderer_9(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__closeButton_10(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__headerText_11(),
	FileOpenDialog_t153A5E25B2CEB15E10959B525070569343D6A690::get_offset_of__directory_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3896[1] = 
{
	FileText_tE032034CE13EBB5A135F6CD36D7A69C7395D66C9::get_offset_of_U3CItemTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (LoadSample_t170D97D6A8248B38D65A113D89EB542E0E5A9EB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (LoadSampleAsync_t8427B705F0B151FBF9CD6CEF3CA7492F55410233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22), -1, sizeof(U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3899[2] = 
{
	U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tADF8CFFC690C095BEE3DC37382E3599D633BCC22_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
