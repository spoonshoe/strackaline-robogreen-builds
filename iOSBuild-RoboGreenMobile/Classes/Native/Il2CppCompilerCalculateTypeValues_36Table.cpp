﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// APIConfigurationController
struct APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B;
// APIDXFController
struct APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7;
// APIElevationDataTransferController
struct APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630;
// APIGetConfigurationController
struct APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A;
// APIGreenController/CourseDataEvent
struct CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901;
// APILoginController
struct APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05;
// ArrowManager
struct ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983;
// CI.HttpClient.HttpClient
struct HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B;
// CalendarController
struct CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338;
// CameraManager
struct CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085;
// CourseManager
struct CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8;
// CustomCollider
struct CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E;
// DailyTemplateHole/DailyTemplateHolevent
struct DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10;
// Doozy.Engine.UI.UIButton
struct UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527;
// Doozy.Engine.UI.UIPopup
struct UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228;
// DrawBox
struct DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C;
// DrawPath
struct DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519;
// Highlight
struct Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD;
// JackManager
struct JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F;
// ProjectorStatusData
struct ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD;
// ReloadPreviousGreenPuttData
struct ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D;
// RotateRectController/RotateRectControllerEvent
struct RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E;
// SelectionBox
struct SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176;
// SplineFollow2D
struct SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB;
// SplineFollow3D
struct SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55;
// Strackaline.Arrow
struct Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520;
// Strackaline.Calendar
struct Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093;
// Strackaline.CalendarDay
struct CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA;
// Strackaline.ProjectorData
struct ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88;
// Strackaline.TemplateHoleLocation
struct TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<CI.HttpClient.HttpResponseMessage>
struct Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.List`1<Jack>
struct List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD;
// System.Collections.Generic.List`1<JackController>
struct List_1_tCB910459F316FBBEEBD292307AE8406532EB60BE;
// System.Collections.Generic.List`1<Strackaline.Arrow>
struct List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Net.Http.HttpClient
struct HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t9F5D984DC2777DA3C6E2BEC6CE9D0F1C3D2E851B;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// UserController
struct UserController_tFD917E07B535D607AFD7056993FB736FD366E600;
// Vectrosity.VectorLine
struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F;
// Vectrosity.VectorLine[]
struct VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITFORPROJECTODATAU3ED__4_TBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E_H
#define U3CWAITFORPROJECTODATAU3ED__4_TBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIConfigurationController_<WaitForProjectoData>d__4
struct  U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E  : public RuntimeObject
{
public:
	// System.Int32 APIConfigurationController_<WaitForProjectoData>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIConfigurationController_<WaitForProjectoData>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIConfigurationController APIConfigurationController_<WaitForProjectoData>d__4::<>4__this
	APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIConfigurationController_<WaitForProjectoData>d__4::<>7__wrap1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E, ___U3CU3E4__this_2)); }
	inline APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E, ___U3CU3E7__wrap1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPROJECTODATAU3ED__4_TBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E_H
#ifndef U3CU3EC_TB09D592FE943F06E23805710E2711C9A9B736B2A_H
#define U3CU3EC_TB09D592FE943F06E23805710E2711C9A9B736B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APICourseCalendarController_<>c
struct  U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields
{
public:
	// APICourseCalendarController_<>c APICourseCalendarController_<>c::<>9
	U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A * ___U3CU3E9_0;
	// System.Net.Security.RemoteCertificateValidationCallback APICourseCalendarController_<>c::<>9__2_0
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields, ___U3CU3E9__2_0_1)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB09D592FE943F06E23805710E2711C9A9B736B2A_H
#ifndef U3CGETDXFMODELFROMAPIU3ED__4_T512887611CD7AA6D61D5AC0C35C3670B07F63AF9_H
#define U3CGETDXFMODELFROMAPIU3ED__4_T512887611CD7AA6D61D5AC0C35C3670B07F63AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIDXFController_<GetDXFModelFromAPI>d__4
struct  U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9  : public RuntimeObject
{
public:
	// System.Int32 APIDXFController_<GetDXFModelFromAPI>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIDXFController_<GetDXFModelFromAPI>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 APIDXFController_<GetDXFModelFromAPI>d__4::_greenID
	int32_t ____greenID_2;
	// System.Int32 APIDXFController_<GetDXFModelFromAPI>d__4::_typeId
	int32_t ____typeId_3;
	// APIDXFController APIDXFController_<GetDXFModelFromAPI>d__4::<>4__this
	APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7 * ___U3CU3E4__this_4;
	// UnityEngine.WWW APIDXFController_<GetDXFModelFromAPI>d__4::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of__greenID_2() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ____greenID_2)); }
	inline int32_t get__greenID_2() const { return ____greenID_2; }
	inline int32_t* get_address_of__greenID_2() { return &____greenID_2; }
	inline void set__greenID_2(int32_t value)
	{
		____greenID_2 = value;
	}

	inline static int32_t get_offset_of__typeId_3() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ____typeId_3)); }
	inline int32_t get__typeId_3() const { return ____typeId_3; }
	inline int32_t* get_address_of__typeId_3() { return &____typeId_3; }
	inline void set__typeId_3(int32_t value)
	{
		____typeId_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ___U3CU3E4__this_4)); }
	inline APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9, ___U3CwwwU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDXFMODELFROMAPIU3ED__4_T512887611CD7AA6D61D5AC0C35C3670B07F63AF9_H
#ifndef U3CWAITFORELEVATIONTRANSFERDATAU3ED__4_T454674913F8BDFC04EF8A95F225843F2A5F843D3_H
#define U3CWAITFORELEVATIONTRANSFERDATAU3ED__4_T454674913F8BDFC04EF8A95F225843F2A5F843D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIElevationDataTransferController_<WaitForElevationTransferData>d__4
struct  U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3  : public RuntimeObject
{
public:
	// System.Int32 APIElevationDataTransferController_<WaitForElevationTransferData>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIElevationDataTransferController_<WaitForElevationTransferData>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIElevationDataTransferController APIElevationDataTransferController_<WaitForElevationTransferData>d__4::<>4__this
	APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630 * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIElevationDataTransferController_<WaitForElevationTransferData>d__4::<>7__wrap1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3, ___U3CU3E4__this_2)); }
	inline APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3, ___U3CU3E7__wrap1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORELEVATIONTRANSFERDATAU3ED__4_T454674913F8BDFC04EF8A95F225843F2A5F843D3_H
#ifndef U3CWAITFORCONFIGURATIONDATAU3ED__10_TEEFBC4CF6F071708602EC9FE2C35B3F1116B6438_H
#define U3CWAITFORCONFIGURATIONDATAU3ED__10_TEEFBC4CF6F071708602EC9FE2C35B3F1116B6438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGetConfigurationController_<WaitForConfigurationData>d__10
struct  U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438  : public RuntimeObject
{
public:
	// System.Int32 APIGetConfigurationController_<WaitForConfigurationData>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIGetConfigurationController_<WaitForConfigurationData>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIGetConfigurationController APIGetConfigurationController_<WaitForConfigurationData>d__10::<>4__this
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIGetConfigurationController_<WaitForConfigurationData>d__10::<>7__wrap1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438, ___U3CU3E4__this_2)); }
	inline APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438, ___U3CU3E7__wrap1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORCONFIGURATIONDATAU3ED__10_TEEFBC4CF6F071708602EC9FE2C35B3F1116B6438_H
#ifndef U3CU3EC_T69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_H
#define U3CU3EC_T69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGreenController_<>c
struct  U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields
{
public:
	// APIGreenController_<>c APIGreenController_<>c::<>9
	U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8 * ___U3CU3E9_0;
	// System.Net.Security.RemoteCertificateValidationCallback APIGreenController_<>c::<>9__17_0
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___U3CU3E9__17_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields, ___U3CU3E9__17_0_1)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_H
#ifndef U3CU3EC_TE049D2F79AFDC77CC44DD153BD80780466E61993_H
#define U3CU3EC_TE049D2F79AFDC77CC44DD153BD80780466E61993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGreenElevationDataController_<>c
struct  U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields
{
public:
	// APIGreenElevationDataController_<>c APIGreenElevationDataController_<>c::<>9
	U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993 * ___U3CU3E9_0;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> APIGreenElevationDataController_<>c::<>9__6_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE049D2F79AFDC77CC44DD153BD80780466E61993_H
#ifndef U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#define U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController_<>c
struct  U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields
{
public:
	// APILoginController_<>c APILoginController_<>c::<>9
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * ___U3CU3E9_0;
	// System.Net.Security.RemoteCertificateValidationCallback APILoginController_<>c::<>9__6_0
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields, ___U3CU3E9__6_0_1)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#ifndef U3CLOGINREQUESTU3ED__8_T286EE8C57C961623E0E2098657F72DB3E401DB30_H
#define U3CLOGINREQUESTU3ED__8_T286EE8C57C961623E0E2098657F72DB3E401DB30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController_<LoginRequest>d__8
struct  U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30  : public RuntimeObject
{
public:
	// System.Int32 APILoginController_<LoginRequest>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APILoginController_<LoginRequest>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APILoginController APILoginController_<LoginRequest>d__8::<>4__this
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APILoginController_<LoginRequest>d__8::<>7__wrap1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30, ___U3CU3E4__this_2)); }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30, ___U3CU3E7__wrap1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGINREQUESTU3ED__8_T286EE8C57C961623E0E2098657F72DB3E401DB30_H
#ifndef U3CWAITFORPROJECTODATAU3ED__11_T0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4_H
#define U3CWAITFORPROJECTODATAU3ED__11_T0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIProjectorDataController_<WaitForProjectoData>d__11
struct  U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4  : public RuntimeObject
{
public:
	// System.Int32 APIProjectorDataController_<WaitForProjectoData>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIProjectorDataController_<WaitForProjectoData>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Networking.UnityWebRequest APIProjectorDataController_<WaitForProjectoData>d__11::_webRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ____webRequest_2;
	// UnityEngine.Networking.UnityWebRequest APIProjectorDataController_<WaitForProjectoData>d__11::<>7__wrap1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of__webRequest_2() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4, ____webRequest_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get__webRequest_2() const { return ____webRequest_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of__webRequest_2() { return &____webRequest_2; }
	inline void set__webRequest_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		____webRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&____webRequest_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4, ___U3CU3E7__wrap1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPROJECTODATAU3ED__11_T0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4_H
#ifndef U3CU3EC_T3AB132847052CED2D0E9F15F12163B529E3E7D64_H
#define U3CU3EC_T3AB132847052CED2D0E9F15F12163B529E3E7D64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttController_<>c
struct  U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields
{
public:
	// APIPuttController_<>c APIPuttController_<>c::<>9
	U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64 * ___U3CU3E9_0;
	// System.Net.Security.RemoteCertificateValidationCallback APIPuttController_<>c::<>9__7_0
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields, ___U3CU3E9__7_0_1)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3AB132847052CED2D0E9F15F12163B529E3E7D64_H
#ifndef U3CWAITFORGETPUTTU3ED__9_T945D70A7877F27EFCB7F908F6027C3AC94E5ABD7_H
#define U3CWAITFORGETPUTTU3ED__9_T945D70A7877F27EFCB7F908F6027C3AC94E5ABD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttController_<WaitForGetPutt>d__9
struct  U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7  : public RuntimeObject
{
public:
	// System.Int32 APIPuttController_<WaitForGetPutt>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIPuttController_<WaitForGetPutt>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WWW APIPuttController_<WaitForGetPutt>d__9::data
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___data_2;
	// UnityEngine.GameObject APIPuttController_<WaitForGetPutt>d__9::_puttInteractive
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____puttInteractive_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7, ___data_2)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_data_2() const { return ___data_2; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}

	inline static int32_t get_offset_of__puttInteractive_3() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7, ____puttInteractive_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__puttInteractive_3() const { return ____puttInteractive_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__puttInteractive_3() { return &____puttInteractive_3; }
	inline void set__puttInteractive_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____puttInteractive_3 = value;
		Il2CppCodeGenWriteBarrier((&____puttInteractive_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORGETPUTTU3ED__9_T945D70A7877F27EFCB7F908F6027C3AC94E5ABD7_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T1D02706154F17F7E780B584C3E8A5381A149BB34_H
#define U3CU3EC__DISPLAYCLASS8_0_T1D02706154F17F7E780B584C3E8A5381A149BB34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalendarController_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34  : public RuntimeObject
{
public:
	// Strackaline.Calendar CalendarController_<>c__DisplayClass8_0::item
	Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093 * ___item_0;
	// CalendarController CalendarController_<>c__DisplayClass8_0::<>4__this
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34, ___item_0)); }
	inline Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093 * get_item_0() const { return ___item_0; }
	inline Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Calendar_tD7C76FE9D351E82FCD7746A2C72220767BF93093 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34, ___U3CU3E4__this_1)); }
	inline CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T1D02706154F17F7E780B584C3E8A5381A149BB34_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T22C76A9F1226B1BCDFCAC0BB870837B79832F3A1_H
#define U3CU3EC__DISPLAYCLASS9_0_T22C76A9F1226B1BCDFCAC0BB870837B79832F3A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalendarController_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1  : public RuntimeObject
{
public:
	// Strackaline.CalendarDay CalendarController_<>c__DisplayClass9_0::item
	CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA * ___item_0;
	// CalendarController CalendarController_<>c__DisplayClass9_0::<>4__this
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1, ___item_0)); }
	inline CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA * get_item_0() const { return ___item_0; }
	inline CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(CalendarDay_t7DA31A0F3CD6385D90898E4DF03BF90496743CEA * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1, ___U3CU3E4__this_1)); }
	inline CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T22C76A9F1226B1BCDFCAC0BB870837B79832F3A1_H
#ifndef CONFIGROBOGREEN_TC89B896171249FAF5C285A85224E453E7E13930F_H
#define CONFIGROBOGREEN_TC89B896171249FAF5C285A85224E453E7E13930F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigRoboGreen
struct  ConfigRoboGreen_tC89B896171249FAF5C285A85224E453E7E13930F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Jack> ConfigRoboGreen::jackList
	List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * ___jackList_0;

public:
	inline static int32_t get_offset_of_jackList_0() { return static_cast<int32_t>(offsetof(ConfigRoboGreen_tC89B896171249FAF5C285A85224E453E7E13930F, ___jackList_0)); }
	inline List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * get_jackList_0() const { return ___jackList_0; }
	inline List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD ** get_address_of_jackList_0() { return &___jackList_0; }
	inline void set_jackList_0(List_1_t5F83BF35CE91953E7284F751A62DD8FF49F92EAD * value)
	{
		___jackList_0 = value;
		Il2CppCodeGenWriteBarrier((&___jackList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGROBOGREEN_TC89B896171249FAF5C285A85224E453E7E13930F_H
#ifndef U3CSTARTU3ED__7_TE11A570AFAD9602C994C1F904115052D61726850_H
#define U3CSTARTU3ED__7_TE11A570AFAD9602C994C1F904115052D61726850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawBox_<Start>d__7
struct  U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850  : public RuntimeObject
{
public:
	// System.Int32 DrawBox_<Start>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DrawBox_<Start>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DrawBox DrawBox_<Start>d__7::<>4__this
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850, ___U3CU3E4__this_2)); }
	inline DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__7_TE11A570AFAD9602C994C1F904115052D61726850_H
#ifndef U3CSAMPLEPOINTSU3ED__11_TA6E94A341AE69E60D93A4EFE238EC24991DD4451_H
#define U3CSAMPLEPOINTSU3ED__11_TA6E94A341AE69E60D93A4EFE238EC24991DD4451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawPath_<SamplePoints>d__11
struct  U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451  : public RuntimeObject
{
public:
	// System.Int32 DrawPath_<SamplePoints>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DrawPath_<SamplePoints>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DrawPath DrawPath_<SamplePoints>d__11::<>4__this
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519 * ___U3CU3E4__this_2;
	// UnityEngine.Transform DrawPath_<SamplePoints>d__11::thisTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___thisTransform_3;
	// System.Boolean DrawPath_<SamplePoints>d__11::<running>5__2
	bool ___U3CrunningU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451, ___U3CU3E4__this_2)); }
	inline DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_thisTransform_3() { return static_cast<int32_t>(offsetof(U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451, ___thisTransform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_thisTransform_3() const { return ___thisTransform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_thisTransform_3() { return &___thisTransform_3; }
	inline void set_thisTransform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___thisTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_3), value);
	}

	inline static int32_t get_offset_of_U3CrunningU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451, ___U3CrunningU3E5__2_4)); }
	inline bool get_U3CrunningU3E5__2_4() const { return ___U3CrunningU3E5__2_4; }
	inline bool* get_address_of_U3CrunningU3E5__2_4() { return &___U3CrunningU3E5__2_4; }
	inline void set_U3CrunningU3E5__2_4(bool value)
	{
		___U3CrunningU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAMPLEPOINTSU3ED__11_TA6E94A341AE69E60D93A4EFE238EC24991DD4451_H
#ifndef EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#define EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtensionMethods
struct  ExtensionMethods_t9C246E3C94E8653839FE7FBBE6549E8030A5B14B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#ifndef U3CCYCLECOLORU3ED__6_T8EB25DFD392E5F8365F1E48D49501ACC8B553E27_H
#define U3CCYCLECOLORU3ED__6_T8EB25DFD392E5F8365F1E48D49501ACC8B553E27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionBox_<CycleColor>d__6
struct  U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27  : public RuntimeObject
{
public:
	// System.Int32 SelectionBox_<CycleColor>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SelectionBox_<CycleColor>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SelectionBox SelectionBox_<CycleColor>d__6::<>4__this
	SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27, ___U3CU3E4__this_2)); }
	inline SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCYCLECOLORU3ED__6_T8EB25DFD392E5F8365F1E48D49501ACC8B553E27_H
#ifndef U3CSTARTU3ED__4_TC42A5F1439C73FF835A1CDA996E878E8347EF1C1_H
#define U3CSTARTU3ED__4_TC42A5F1439C73FF835A1CDA996E878E8347EF1C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineFollow2D_<Start>d__4
struct  U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1  : public RuntimeObject
{
public:
	// System.Int32 SplineFollow2D_<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SplineFollow2D_<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SplineFollow2D SplineFollow2D_<Start>d__4::<>4__this
	SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB * ___U3CU3E4__this_2;
	// Vectrosity.VectorLine SplineFollow2D_<Start>d__4::<line>5__2
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___U3ClineU3E5__2_3;
	// System.Single SplineFollow2D_<Start>d__4::<dist>5__3
	float ___U3CdistU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1, ___U3CU3E4__this_2)); }
	inline SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3ClineU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1, ___U3ClineU3E5__2_3)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_U3ClineU3E5__2_3() const { return ___U3ClineU3E5__2_3; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_U3ClineU3E5__2_3() { return &___U3ClineU3E5__2_3; }
	inline void set_U3ClineU3E5__2_3(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___U3ClineU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CdistU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1, ___U3CdistU3E5__3_4)); }
	inline float get_U3CdistU3E5__3_4() const { return ___U3CdistU3E5__3_4; }
	inline float* get_address_of_U3CdistU3E5__3_4() { return &___U3CdistU3E5__3_4; }
	inline void set_U3CdistU3E5__3_4(float value)
	{
		___U3CdistU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_TC42A5F1439C73FF835A1CDA996E878E8347EF1C1_H
#ifndef U3CSTARTU3ED__4_T057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455_H
#define U3CSTARTU3ED__4_T057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineFollow3D_<Start>d__4
struct  U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455  : public RuntimeObject
{
public:
	// System.Int32 SplineFollow3D_<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SplineFollow3D_<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SplineFollow3D SplineFollow3D_<Start>d__4::<>4__this
	SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55 * ___U3CU3E4__this_2;
	// Vectrosity.VectorLine SplineFollow3D_<Start>d__4::<line>5__2
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___U3ClineU3E5__2_3;
	// System.Single SplineFollow3D_<Start>d__4::<dist>5__3
	float ___U3CdistU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455, ___U3CU3E4__this_2)); }
	inline SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3ClineU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455, ___U3ClineU3E5__2_3)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_U3ClineU3E5__2_3() const { return ___U3ClineU3E5__2_3; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_U3ClineU3E5__2_3() { return &___U3ClineU3E5__2_3; }
	inline void set_U3ClineU3E5__2_3(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___U3ClineU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CdistU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455, ___U3CdistU3E5__3_4)); }
	inline float get_U3CdistU3E5__3_4() const { return ___U3CdistU3E5__3_4; }
	inline float* get_address_of_U3CdistU3E5__3_4() { return &___U3CdistU3E5__3_4; }
	inline void set_U3CdistU3E5__3_4(float value)
	{
		___U3CdistU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef COURSECALENDARDAY_TC84B10CAB8C8E39932384D10C308A77D4DAB24F2_H
#define COURSECALENDARDAY_TC84B10CAB8C8E39932384D10C308A77D4DAB24F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalendarController_CourseCalendarDay
struct  CourseCalendarDay_tC84B10CAB8C8E39932384D10C308A77D4DAB24F2 
{
public:
	// System.Int32 CalendarController_CourseCalendarDay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CourseCalendarDay_tC84B10CAB8C8E39932384D10C308A77D4DAB24F2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSECALENDARDAY_TC84B10CAB8C8E39932384D10C308A77D4DAB24F2_H
#ifndef U3CFADECOLORU3ED__22_TAF497C7701CB339968BB75FECDE2E606992FF577_H
#define U3CFADECOLORU3ED__22_TAF497C7701CB339968BB75FECDE2E606992FF577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Highlight_<FadeColor>d__22
struct  U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577  : public RuntimeObject
{
public:
	// System.Int32 Highlight_<FadeColor>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Highlight_<FadeColor>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Highlight_<FadeColor>d__22::instantFade
	bool ___instantFade_2;
	// Highlight Highlight_<FadeColor>d__22::<>4__this
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD * ___U3CU3E4__this_3;
	// UnityEngine.Color Highlight_<FadeColor>d__22::<startColor>5__2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CstartColorU3E5__2_4;
	// System.Int32 Highlight_<FadeColor>d__22::<thisIndex>5__3
	int32_t ___U3CthisIndexU3E5__3_5;
	// System.Single Highlight_<FadeColor>d__22::<t>5__4
	float ___U3CtU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantFade_2() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___instantFade_2)); }
	inline bool get_instantFade_2() const { return ___instantFade_2; }
	inline bool* get_address_of_instantFade_2() { return &___instantFade_2; }
	inline void set_instantFade_2(bool value)
	{
		___instantFade_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CU3E4__this_3)); }
	inline Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CstartColorU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CstartColorU3E5__2_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CstartColorU3E5__2_4() const { return ___U3CstartColorU3E5__2_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CstartColorU3E5__2_4() { return &___U3CstartColorU3E5__2_4; }
	inline void set_U3CstartColorU3E5__2_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CstartColorU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CthisIndexU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CthisIndexU3E5__3_5)); }
	inline int32_t get_U3CthisIndexU3E5__3_5() const { return ___U3CthisIndexU3E5__3_5; }
	inline int32_t* get_address_of_U3CthisIndexU3E5__3_5() { return &___U3CthisIndexU3E5__3_5; }
	inline void set_U3CthisIndexU3E5__3_5(int32_t value)
	{
		___U3CthisIndexU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577, ___U3CtU3E5__4_6)); }
	inline float get_U3CtU3E5__4_6() const { return ___U3CtU3E5__4_6; }
	inline float* get_address_of_U3CtU3E5__4_6() { return &___U3CtU3E5__4_6; }
	inline void set_U3CtU3E5__4_6(float value)
	{
		___U3CtU3E5__4_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADECOLORU3ED__22_TAF497C7701CB339968BB75FECDE2E606992FF577_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#define SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorObject_Shape
struct  Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB 
{
public:
	// System.Int32 VectorObject_Shape::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPE_T6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#define TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t806752C775BA713A91B6588A07CA98417CABC003 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifndef COURSEDATAEVENT_T8E7637E2129D80FA7D871BC563CB4327F9E54901_H
#define COURSEDATAEVENT_T8E7637E2129D80FA7D871BC563CB4327F9E54901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGreenController_CourseDataEvent
struct  CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEDATAEVENT_T8E7637E2129D80FA7D871BC563CB4327F9E54901_H
#ifndef DAILYTEMPLATEHOLEVENT_T297F3EF6EC1735B01FE448B6D3639F55933F3F10_H
#define DAILYTEMPLATEHOLEVENT_T297F3EF6EC1735B01FE448B6D3639F55933F3F10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyTemplateHole_DailyTemplateHolevent
struct  DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAILYTEMPLATEHOLEVENT_T297F3EF6EC1735B01FE448B6D3639F55933F3F10_H
#ifndef ROTATERECTCONTROLLEREVENT_T2699905974A56D173A88FB96BF0F0756A74A9D0E_H
#define ROTATERECTCONTROLLEREVENT_T2699905974A56D173A88FB96BF0F0756A74A9D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateRectController_RotateRectControllerEvent
struct  RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATERECTCONTROLLEREVENT_T2699905974A56D173A88FB96BF0F0756A74A9D0E_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef APICONFIGURATIONCONTROLLER_TE8F2ED6E76DED60D8DCC80378972FB6B3850D83B_H
#define APICONFIGURATIONCONTROLLER_TE8F2ED6E76DED60D8DCC80378972FB6B3850D83B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIConfigurationController
struct  APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIConfigurationController::apiBaseURL
	String_t* ___apiBaseURL_4;
	// System.String APIConfigurationController::permanentToken
	String_t* ___permanentToken_5;
	// UnityEngine.Networking.UnityWebRequest APIConfigurationController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_6;

public:
	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_4), value);
	}

	inline static int32_t get_offset_of_permanentToken_5() { return static_cast<int32_t>(offsetof(APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B, ___permanentToken_5)); }
	inline String_t* get_permanentToken_5() const { return ___permanentToken_5; }
	inline String_t** get_address_of_permanentToken_5() { return &___permanentToken_5; }
	inline void set_permanentToken_5(String_t* value)
	{
		___permanentToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___permanentToken_5), value);
	}

	inline static int32_t get_offset_of_apiRequest_6() { return static_cast<int32_t>(offsetof(APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B, ___apiRequest_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_6() const { return ___apiRequest_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_6() { return &___apiRequest_6; }
	inline void set_apiRequest_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_6 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICONFIGURATIONCONTROLLER_TE8F2ED6E76DED60D8DCC80378972FB6B3850D83B_H
#ifndef APICOURSECALENDARCONTROLLER_TC06934C155FBA856B1495D7021C4B113FC09DBE6_H
#define APICOURSECALENDARCONTROLLER_TC06934C155FBA856B1495D7021C4B113FC09DBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APICourseCalendarController
struct  APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> APICourseCalendarController::postHeaderDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___postHeaderDict_4;
	// CourseManager APICourseCalendarController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_5;

public:
	inline static int32_t get_offset_of_postHeaderDict_4() { return static_cast<int32_t>(offsetof(APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6, ___postHeaderDict_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_postHeaderDict_4() const { return ___postHeaderDict_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_postHeaderDict_4() { return &___postHeaderDict_4; }
	inline void set_postHeaderDict_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___postHeaderDict_4 = value;
		Il2CppCodeGenWriteBarrier((&___postHeaderDict_4), value);
	}

	inline static int32_t get_offset_of_courseManager_5() { return static_cast<int32_t>(offsetof(APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6, ___courseManager_5)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_5() const { return ___courseManager_5; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_5() { return &___courseManager_5; }
	inline void set_courseManager_5(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICOURSECALENDARCONTROLLER_TC06934C155FBA856B1495D7021C4B113FC09DBE6_H
#ifndef APIDXFCONTROLLER_T2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_H
#define APIDXFCONTROLLER_T2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIDXFController
struct  APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_StaticFields
{
public:
	// System.Action APIDXFController::DXFLoadedComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DXFLoadedComplete_4;

public:
	inline static int32_t get_offset_of_DXFLoadedComplete_4() { return static_cast<int32_t>(offsetof(APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_StaticFields, ___DXFLoadedComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DXFLoadedComplete_4() const { return ___DXFLoadedComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DXFLoadedComplete_4() { return &___DXFLoadedComplete_4; }
	inline void set_DXFLoadedComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DXFLoadedComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___DXFLoadedComplete_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIDXFCONTROLLER_T2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_H
#ifndef APIELEVATIONDATATRANSFERCONTROLLER_TEE52CF62B640E345F993777E434B794B8E955630_H
#define APIELEVATIONDATATRANSFERCONTROLLER_TEE52CF62B640E345F993777E434B794B8E955630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIElevationDataTransferController
struct  APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIElevationDataTransferController::apiBaseURL
	String_t* ___apiBaseURL_4;
	// UnityEngine.Networking.UnityWebRequest APIElevationDataTransferController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_5;

public:
	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_4), value);
	}

	inline static int32_t get_offset_of_apiRequest_5() { return static_cast<int32_t>(offsetof(APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630, ___apiRequest_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_5() const { return ___apiRequest_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_5() { return &___apiRequest_5; }
	inline void set_apiRequest_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIELEVATIONDATATRANSFERCONTROLLER_TEE52CF62B640E345F993777E434B794B8E955630_H
#ifndef APIGETCONFIGURATIONCONTROLLER_TFE15AE0BE552D54DE1113DA0DC118284A20B214A_H
#define APIGETCONFIGURATIONCONTROLLER_TFE15AE0BE552D54DE1113DA0DC118284A20B214A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGetConfigurationController
struct  APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIGetConfigurationController::apiBaseURL
	String_t* ___apiBaseURL_5;
	// UnityEngine.Networking.UnityWebRequest APIGetConfigurationController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_6;
	// Strackaline.ProjectorData APIGetConfigurationController::projectorData
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * ___projectorData_7;
	// ReloadPreviousGreenPuttData APIGetConfigurationController::reloadPreviousGreen
	ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D * ___reloadPreviousGreen_8;
	// CourseManager APIGetConfigurationController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_9;

public:
	inline static int32_t get_offset_of_apiBaseURL_5() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A, ___apiBaseURL_5)); }
	inline String_t* get_apiBaseURL_5() const { return ___apiBaseURL_5; }
	inline String_t** get_address_of_apiBaseURL_5() { return &___apiBaseURL_5; }
	inline void set_apiBaseURL_5(String_t* value)
	{
		___apiBaseURL_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_5), value);
	}

	inline static int32_t get_offset_of_apiRequest_6() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A, ___apiRequest_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_6() const { return ___apiRequest_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_6() { return &___apiRequest_6; }
	inline void set_apiRequest_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_6 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_6), value);
	}

	inline static int32_t get_offset_of_projectorData_7() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A, ___projectorData_7)); }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * get_projectorData_7() const { return ___projectorData_7; }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 ** get_address_of_projectorData_7() { return &___projectorData_7; }
	inline void set_projectorData_7(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * value)
	{
		___projectorData_7 = value;
		Il2CppCodeGenWriteBarrier((&___projectorData_7), value);
	}

	inline static int32_t get_offset_of_reloadPreviousGreen_8() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A, ___reloadPreviousGreen_8)); }
	inline ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D * get_reloadPreviousGreen_8() const { return ___reloadPreviousGreen_8; }
	inline ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D ** get_address_of_reloadPreviousGreen_8() { return &___reloadPreviousGreen_8; }
	inline void set_reloadPreviousGreen_8(ReloadPreviousGreenPuttData_t37EF93F2A73D7903932996FC677441694D6DC06D * value)
	{
		___reloadPreviousGreen_8 = value;
		Il2CppCodeGenWriteBarrier((&___reloadPreviousGreen_8), value);
	}

	inline static int32_t get_offset_of_courseManager_9() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A, ___courseManager_9)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_9() const { return ___courseManager_9; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_9() { return &___courseManager_9; }
	inline void set_courseManager_9(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_9), value);
	}
};

struct APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A_StaticFields
{
public:
	// System.Action APIGetConfigurationController::APIGetConfigurationDataComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___APIGetConfigurationDataComplete_4;

public:
	inline static int32_t get_offset_of_APIGetConfigurationDataComplete_4() { return static_cast<int32_t>(offsetof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A_StaticFields, ___APIGetConfigurationDataComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_APIGetConfigurationDataComplete_4() const { return ___APIGetConfigurationDataComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_APIGetConfigurationDataComplete_4() { return &___APIGetConfigurationDataComplete_4; }
	inline void set_APIGetConfigurationDataComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___APIGetConfigurationDataComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___APIGetConfigurationDataComplete_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIGETCONFIGURATIONCONTROLLER_TFE15AE0BE552D54DE1113DA0DC118284A20B214A_H
#ifndef APIGREENCONTROLLER_T822F4A90519A3A01081E8CBA458F740113FED13C_H
#define APIGREENCONTROLLER_T822F4A90519A3A01081E8CBA458F740113FED13C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGreenController
struct  APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> APIGreenController::postHeaderDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___postHeaderDict_10;
	// CourseManager APIGreenController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_11;
	// System.String APIGreenController::courseIDToLoad
	String_t* ___courseIDToLoad_12;
	// System.String APIGreenController::regionCodeToLoad
	String_t* ___regionCodeToLoad_13;

public:
	inline static int32_t get_offset_of_postHeaderDict_10() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C, ___postHeaderDict_10)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_postHeaderDict_10() const { return ___postHeaderDict_10; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_postHeaderDict_10() { return &___postHeaderDict_10; }
	inline void set_postHeaderDict_10(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___postHeaderDict_10 = value;
		Il2CppCodeGenWriteBarrier((&___postHeaderDict_10), value);
	}

	inline static int32_t get_offset_of_courseManager_11() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C, ___courseManager_11)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_11() const { return ___courseManager_11; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_11() { return &___courseManager_11; }
	inline void set_courseManager_11(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_11), value);
	}

	inline static int32_t get_offset_of_courseIDToLoad_12() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C, ___courseIDToLoad_12)); }
	inline String_t* get_courseIDToLoad_12() const { return ___courseIDToLoad_12; }
	inline String_t** get_address_of_courseIDToLoad_12() { return &___courseIDToLoad_12; }
	inline void set_courseIDToLoad_12(String_t* value)
	{
		___courseIDToLoad_12 = value;
		Il2CppCodeGenWriteBarrier((&___courseIDToLoad_12), value);
	}

	inline static int32_t get_offset_of_regionCodeToLoad_13() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C, ___regionCodeToLoad_13)); }
	inline String_t* get_regionCodeToLoad_13() const { return ___regionCodeToLoad_13; }
	inline String_t** get_address_of_regionCodeToLoad_13() { return &___regionCodeToLoad_13; }
	inline void set_regionCodeToLoad_13(String_t* value)
	{
		___regionCodeToLoad_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionCodeToLoad_13), value);
	}
};

struct APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields
{
public:
	// APIGreenController_CourseDataEvent APIGreenController::GreenLoaded
	CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901 * ___GreenLoaded_4;
	// System.Action APIGreenController::RegionsLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___RegionsLoaded_5;
	// System.Action APIGreenController::CoursesInRegionLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CoursesInRegionLoaded_6;
	// System.Action APIGreenController::GreenGroupsInCourseLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___GreenGroupsInCourseLoaded_7;
	// System.String APIGreenController::apiBaseURL
	String_t* ___apiBaseURL_8;
	// System.String APIGreenController::permanentToken
	String_t* ___permanentToken_9;

public:
	inline static int32_t get_offset_of_GreenLoaded_4() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___GreenLoaded_4)); }
	inline CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901 * get_GreenLoaded_4() const { return ___GreenLoaded_4; }
	inline CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901 ** get_address_of_GreenLoaded_4() { return &___GreenLoaded_4; }
	inline void set_GreenLoaded_4(CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901 * value)
	{
		___GreenLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___GreenLoaded_4), value);
	}

	inline static int32_t get_offset_of_RegionsLoaded_5() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___RegionsLoaded_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_RegionsLoaded_5() const { return ___RegionsLoaded_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_RegionsLoaded_5() { return &___RegionsLoaded_5; }
	inline void set_RegionsLoaded_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___RegionsLoaded_5 = value;
		Il2CppCodeGenWriteBarrier((&___RegionsLoaded_5), value);
	}

	inline static int32_t get_offset_of_CoursesInRegionLoaded_6() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___CoursesInRegionLoaded_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CoursesInRegionLoaded_6() const { return ___CoursesInRegionLoaded_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CoursesInRegionLoaded_6() { return &___CoursesInRegionLoaded_6; }
	inline void set_CoursesInRegionLoaded_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CoursesInRegionLoaded_6 = value;
		Il2CppCodeGenWriteBarrier((&___CoursesInRegionLoaded_6), value);
	}

	inline static int32_t get_offset_of_GreenGroupsInCourseLoaded_7() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___GreenGroupsInCourseLoaded_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_GreenGroupsInCourseLoaded_7() const { return ___GreenGroupsInCourseLoaded_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_GreenGroupsInCourseLoaded_7() { return &___GreenGroupsInCourseLoaded_7; }
	inline void set_GreenGroupsInCourseLoaded_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___GreenGroupsInCourseLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___GreenGroupsInCourseLoaded_7), value);
	}

	inline static int32_t get_offset_of_apiBaseURL_8() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___apiBaseURL_8)); }
	inline String_t* get_apiBaseURL_8() const { return ___apiBaseURL_8; }
	inline String_t** get_address_of_apiBaseURL_8() { return &___apiBaseURL_8; }
	inline void set_apiBaseURL_8(String_t* value)
	{
		___apiBaseURL_8 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_8), value);
	}

	inline static int32_t get_offset_of_permanentToken_9() { return static_cast<int32_t>(offsetof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields, ___permanentToken_9)); }
	inline String_t* get_permanentToken_9() const { return ___permanentToken_9; }
	inline String_t** get_address_of_permanentToken_9() { return &___permanentToken_9; }
	inline void set_permanentToken_9(String_t* value)
	{
		___permanentToken_9 = value;
		Il2CppCodeGenWriteBarrier((&___permanentToken_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIGREENCONTROLLER_T822F4A90519A3A01081E8CBA458F740113FED13C_H
#ifndef APIGREENELEVATIONDATACONTROLLER_T36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_H
#define APIGREENELEVATIONDATACONTROLLER_T36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIGreenElevationDataController
struct  APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CI.HttpClient.HttpClient APIGreenElevationDataController::client
	HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * ___client_5;

public:
	inline static int32_t get_offset_of_client_5() { return static_cast<int32_t>(offsetof(APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8, ___client_5)); }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * get_client_5() const { return ___client_5; }
	inline HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B ** get_address_of_client_5() { return &___client_5; }
	inline void set_client_5(HttpClient_tE71BC23E214AF27FD5297528E116E86A36F6124B * value)
	{
		___client_5 = value;
		Il2CppCodeGenWriteBarrier((&___client_5), value);
	}
};

struct APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_StaticFields
{
public:
	// System.Action APIGreenElevationDataController::GreenElevationDataLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___GreenElevationDataLoaded_4;

public:
	inline static int32_t get_offset_of_GreenElevationDataLoaded_4() { return static_cast<int32_t>(offsetof(APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_StaticFields, ___GreenElevationDataLoaded_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_GreenElevationDataLoaded_4() const { return ___GreenElevationDataLoaded_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_GreenElevationDataLoaded_4() { return &___GreenElevationDataLoaded_4; }
	inline void set_GreenElevationDataLoaded_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___GreenElevationDataLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___GreenElevationDataLoaded_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIGREENELEVATIONDATACONTROLLER_T36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_H
#ifndef APIJACKCONTROLLER_T3C67DF98CDEEDEB36426B098DCAD1B0C9B44B70E_H
#define APIJACKCONTROLLER_T3C67DF98CDEEDEB36426B098DCAD1B0C9B44B70E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIJackController
struct  APIJackController_t3C67DF98CDEEDEB36426B098DCAD1B0C9B44B70E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIJACKCONTROLLER_T3C67DF98CDEEDEB36426B098DCAD1B0C9B44B70E_H
#ifndef APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#define APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController
struct  APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APILoginController::PostLoginURL
	String_t* ___PostLoginURL_4;
	// UnityEngine.Networking.UnityWebRequest APILoginController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_5;
	// System.String APILoginController::userNameForTesting
	String_t* ___userNameForTesting_6;
	// System.String APILoginController::passwordForTesting
	String_t* ___passwordForTesting_7;
	// System.Boolean APILoginController::testingLOGIN
	bool ___testingLOGIN_8;
	// UserController APILoginController::userController
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * ___userController_9;

public:
	inline static int32_t get_offset_of_PostLoginURL_4() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___PostLoginURL_4)); }
	inline String_t* get_PostLoginURL_4() const { return ___PostLoginURL_4; }
	inline String_t** get_address_of_PostLoginURL_4() { return &___PostLoginURL_4; }
	inline void set_PostLoginURL_4(String_t* value)
	{
		___PostLoginURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___PostLoginURL_4), value);
	}

	inline static int32_t get_offset_of_apiRequest_5() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___apiRequest_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_5() const { return ___apiRequest_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_5() { return &___apiRequest_5; }
	inline void set_apiRequest_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_5), value);
	}

	inline static int32_t get_offset_of_userNameForTesting_6() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___userNameForTesting_6)); }
	inline String_t* get_userNameForTesting_6() const { return ___userNameForTesting_6; }
	inline String_t** get_address_of_userNameForTesting_6() { return &___userNameForTesting_6; }
	inline void set_userNameForTesting_6(String_t* value)
	{
		___userNameForTesting_6 = value;
		Il2CppCodeGenWriteBarrier((&___userNameForTesting_6), value);
	}

	inline static int32_t get_offset_of_passwordForTesting_7() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___passwordForTesting_7)); }
	inline String_t* get_passwordForTesting_7() const { return ___passwordForTesting_7; }
	inline String_t** get_address_of_passwordForTesting_7() { return &___passwordForTesting_7; }
	inline void set_passwordForTesting_7(String_t* value)
	{
		___passwordForTesting_7 = value;
		Il2CppCodeGenWriteBarrier((&___passwordForTesting_7), value);
	}

	inline static int32_t get_offset_of_testingLOGIN_8() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___testingLOGIN_8)); }
	inline bool get_testingLOGIN_8() const { return ___testingLOGIN_8; }
	inline bool* get_address_of_testingLOGIN_8() { return &___testingLOGIN_8; }
	inline void set_testingLOGIN_8(bool value)
	{
		___testingLOGIN_8 = value;
	}

	inline static int32_t get_offset_of_userController_9() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___userController_9)); }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * get_userController_9() const { return ___userController_9; }
	inline UserController_tFD917E07B535D607AFD7056993FB736FD366E600 ** get_address_of_userController_9() { return &___userController_9; }
	inline void set_userController_9(UserController_tFD917E07B535D607AFD7056993FB736FD366E600 * value)
	{
		___userController_9 = value;
		Il2CppCodeGenWriteBarrier((&___userController_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#ifndef APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#define APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIProjectorDataController
struct  APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIProjectorDataController::apiBaseURL
	String_t* ___apiBaseURL_4;
	// System.String APIProjectorDataController::permanentToken
	String_t* ___permanentToken_5;
	// UnityEngine.Networking.UnityWebRequest APIProjectorDataController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_6;
	// CourseManager APIProjectorDataController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_7;
	// UnityEngine.GameObject APIProjectorDataController::holeDragInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeDragInput_8;
	// System.Single APIProjectorDataController::holeRectRot
	float ___holeRectRot_9;
	// UnityEngine.GameObject APIProjectorDataController::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_10;
	// ProjectorStatusData APIProjectorDataController::projectorStatusData
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * ___projectorStatusData_11;
	// System.Int32 APIProjectorDataController::puttsUpdated
	int32_t ___puttsUpdated_12;

public:
	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_4), value);
	}

	inline static int32_t get_offset_of_permanentToken_5() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___permanentToken_5)); }
	inline String_t* get_permanentToken_5() const { return ___permanentToken_5; }
	inline String_t** get_address_of_permanentToken_5() { return &___permanentToken_5; }
	inline void set_permanentToken_5(String_t* value)
	{
		___permanentToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___permanentToken_5), value);
	}

	inline static int32_t get_offset_of_apiRequest_6() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___apiRequest_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_6() const { return ___apiRequest_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_6() { return &___apiRequest_6; }
	inline void set_apiRequest_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_6 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_6), value);
	}

	inline static int32_t get_offset_of_courseManager_7() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___courseManager_7)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_7() const { return ___courseManager_7; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_7() { return &___courseManager_7; }
	inline void set_courseManager_7(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_7), value);
	}

	inline static int32_t get_offset_of_holeDragInput_8() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___holeDragInput_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeDragInput_8() const { return ___holeDragInput_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeDragInput_8() { return &___holeDragInput_8; }
	inline void set_holeDragInput_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeDragInput_8 = value;
		Il2CppCodeGenWriteBarrier((&___holeDragInput_8), value);
	}

	inline static int32_t get_offset_of_holeRectRot_9() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___holeRectRot_9)); }
	inline float get_holeRectRot_9() const { return ___holeRectRot_9; }
	inline float* get_address_of_holeRectRot_9() { return &___holeRectRot_9; }
	inline void set_holeRectRot_9(float value)
	{
		___holeRectRot_9 = value;
	}

	inline static int32_t get_offset_of_holeContainer_10() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___holeContainer_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_10() const { return ___holeContainer_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_10() { return &___holeContainer_10; }
	inline void set_holeContainer_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_10 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_10), value);
	}

	inline static int32_t get_offset_of_projectorStatusData_11() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___projectorStatusData_11)); }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * get_projectorStatusData_11() const { return ___projectorStatusData_11; }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD ** get_address_of_projectorStatusData_11() { return &___projectorStatusData_11; }
	inline void set_projectorStatusData_11(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * value)
	{
		___projectorStatusData_11 = value;
		Il2CppCodeGenWriteBarrier((&___projectorStatusData_11), value);
	}

	inline static int32_t get_offset_of_puttsUpdated_12() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___puttsUpdated_12)); }
	inline int32_t get_puttsUpdated_12() const { return ___puttsUpdated_12; }
	inline int32_t* get_address_of_puttsUpdated_12() { return &___puttsUpdated_12; }
	inline void set_puttsUpdated_12(int32_t value)
	{
		___puttsUpdated_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#ifndef APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#define APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttController
struct  APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIPuttController::apiBaseURL
	String_t* ___apiBaseURL_4;
	// System.String APIPuttController::permanentToken
	String_t* ___permanentToken_5;
	// UnityEngine.GameObject APIPuttController::preloader
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___preloader_6;
	// UnityEngine.WWW APIPuttController::apiRequest
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___apiRequest_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> APIPuttController::postHeaderDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___postHeaderDict_8;
	// System.Net.Http.HttpClient APIPuttController::client
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * ___client_9;
	// CourseManager APIPuttController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_10;

public:
	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_4), value);
	}

	inline static int32_t get_offset_of_permanentToken_5() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___permanentToken_5)); }
	inline String_t* get_permanentToken_5() const { return ___permanentToken_5; }
	inline String_t** get_address_of_permanentToken_5() { return &___permanentToken_5; }
	inline void set_permanentToken_5(String_t* value)
	{
		___permanentToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___permanentToken_5), value);
	}

	inline static int32_t get_offset_of_preloader_6() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___preloader_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_preloader_6() const { return ___preloader_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_preloader_6() { return &___preloader_6; }
	inline void set_preloader_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___preloader_6 = value;
		Il2CppCodeGenWriteBarrier((&___preloader_6), value);
	}

	inline static int32_t get_offset_of_apiRequest_7() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___apiRequest_7)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_apiRequest_7() const { return ___apiRequest_7; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_apiRequest_7() { return &___apiRequest_7; }
	inline void set_apiRequest_7(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___apiRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_7), value);
	}

	inline static int32_t get_offset_of_postHeaderDict_8() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___postHeaderDict_8)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_postHeaderDict_8() const { return ___postHeaderDict_8; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_postHeaderDict_8() { return &___postHeaderDict_8; }
	inline void set_postHeaderDict_8(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___postHeaderDict_8 = value;
		Il2CppCodeGenWriteBarrier((&___postHeaderDict_8), value);
	}

	inline static int32_t get_offset_of_client_9() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___client_9)); }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * get_client_9() const { return ___client_9; }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 ** get_address_of_client_9() { return &___client_9; }
	inline void set_client_9(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * value)
	{
		___client_9 = value;
		Il2CppCodeGenWriteBarrier((&___client_9), value);
	}

	inline static int32_t get_offset_of_courseManager_10() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___courseManager_10)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_10() const { return ___courseManager_10; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_10() { return &___courseManager_10; }
	inline void set_courseManager_10(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_10 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#ifndef ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#define ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlertPopupController
struct  AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String AlertPopupController::popupName
	String_t* ___popupName_5;
	// Doozy.Engine.UI.UIPopup AlertPopupController::alertPopup
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___alertPopup_6;

public:
	inline static int32_t get_offset_of_popupName_5() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC, ___popupName_5)); }
	inline String_t* get_popupName_5() const { return ___popupName_5; }
	inline String_t** get_address_of_popupName_5() { return &___popupName_5; }
	inline void set_popupName_5(String_t* value)
	{
		___popupName_5 = value;
		Il2CppCodeGenWriteBarrier((&___popupName_5), value);
	}

	inline static int32_t get_offset_of_alertPopup_6() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC, ___alertPopup_6)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_alertPopup_6() const { return ___alertPopup_6; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_alertPopup_6() { return &___alertPopup_6; }
	inline void set_alertPopup_6(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___alertPopup_6 = value;
		Il2CppCodeGenWriteBarrier((&___alertPopup_6), value);
	}
};

struct AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields
{
public:
	// AlertPopupController AlertPopupController::inst
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * ___inst_4;

public:
	inline static int32_t get_offset_of_inst_4() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields, ___inst_4)); }
	inline AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * get_inst_4() const { return ___inst_4; }
	inline AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC ** get_address_of_inst_4() { return &___inst_4; }
	inline void set_inst_4(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * value)
	{
		___inst_4 = value;
		Il2CppCodeGenWriteBarrier((&___inst_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#ifndef ANIMATEPARTIALLINE_T8353382416A0859D7A0B39A20F0C062BD97610B6_H
#define ANIMATEPARTIALLINE_T8353382416A0859D7A0B39A20F0C062BD97610B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatePartialLine
struct  AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture AnimatePartialLine::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Int32 AnimatePartialLine::segments
	int32_t ___segments_5;
	// System.Int32 AnimatePartialLine::visibleLineSegments
	int32_t ___visibleLineSegments_6;
	// System.Single AnimatePartialLine::speed
	float ___speed_7;
	// System.Single AnimatePartialLine::startIndex
	float ___startIndex_8;
	// System.Single AnimatePartialLine::endIndex
	float ___endIndex_9;
	// Vectrosity.VectorLine AnimatePartialLine::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_10;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_segments_5() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___segments_5)); }
	inline int32_t get_segments_5() const { return ___segments_5; }
	inline int32_t* get_address_of_segments_5() { return &___segments_5; }
	inline void set_segments_5(int32_t value)
	{
		___segments_5 = value;
	}

	inline static int32_t get_offset_of_visibleLineSegments_6() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___visibleLineSegments_6)); }
	inline int32_t get_visibleLineSegments_6() const { return ___visibleLineSegments_6; }
	inline int32_t* get_address_of_visibleLineSegments_6() { return &___visibleLineSegments_6; }
	inline void set_visibleLineSegments_6(int32_t value)
	{
		___visibleLineSegments_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}

	inline static int32_t get_offset_of_startIndex_8() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___startIndex_8)); }
	inline float get_startIndex_8() const { return ___startIndex_8; }
	inline float* get_address_of_startIndex_8() { return &___startIndex_8; }
	inline void set_startIndex_8(float value)
	{
		___startIndex_8 = value;
	}

	inline static int32_t get_offset_of_endIndex_9() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___endIndex_9)); }
	inline float get_endIndex_9() const { return ___endIndex_9; }
	inline float* get_address_of_endIndex_9() { return &___endIndex_9; }
	inline void set_endIndex_9(float value)
	{
		___endIndex_9 = value;
	}

	inline static int32_t get_offset_of_line_10() { return static_cast<int32_t>(offsetof(AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6, ___line_10)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_10() const { return ___line_10; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_10() { return &___line_10; }
	inline void set_line_10(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_10 = value;
		Il2CppCodeGenWriteBarrier((&___line_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEPARTIALLINE_T8353382416A0859D7A0B39A20F0C062BD97610B6_H
#ifndef ARROWCOLLECTOR_TB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_H
#define ARROWCOLLECTOR_TB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowCollector
struct  ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Strackaline.Arrow> ArrowCollector::arrowsGathersInHoleContainer
	List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * ___arrowsGathersInHoleContainer_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrowCollector::arrowsForRef
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___arrowsForRef_6;
	// UnityEngine.Collider ArrowCollector::arrowCollectorCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___arrowCollectorCollider_7;
	// ArrowManager ArrowCollector::arrowManager
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983 * ___arrowManager_8;
	// ArrowCollector ArrowCollector::arrowCollector
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08 * ___arrowCollector_9;

public:
	inline static int32_t get_offset_of_arrowsGathersInHoleContainer_5() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08, ___arrowsGathersInHoleContainer_5)); }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * get_arrowsGathersInHoleContainer_5() const { return ___arrowsGathersInHoleContainer_5; }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 ** get_address_of_arrowsGathersInHoleContainer_5() { return &___arrowsGathersInHoleContainer_5; }
	inline void set_arrowsGathersInHoleContainer_5(List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * value)
	{
		___arrowsGathersInHoleContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsGathersInHoleContainer_5), value);
	}

	inline static int32_t get_offset_of_arrowsForRef_6() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08, ___arrowsForRef_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_arrowsForRef_6() const { return ___arrowsForRef_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_arrowsForRef_6() { return &___arrowsForRef_6; }
	inline void set_arrowsForRef_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___arrowsForRef_6 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsForRef_6), value);
	}

	inline static int32_t get_offset_of_arrowCollectorCollider_7() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08, ___arrowCollectorCollider_7)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_arrowCollectorCollider_7() const { return ___arrowCollectorCollider_7; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_arrowCollectorCollider_7() { return &___arrowCollectorCollider_7; }
	inline void set_arrowCollectorCollider_7(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___arrowCollectorCollider_7 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCollectorCollider_7), value);
	}

	inline static int32_t get_offset_of_arrowManager_8() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08, ___arrowManager_8)); }
	inline ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983 * get_arrowManager_8() const { return ___arrowManager_8; }
	inline ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983 ** get_address_of_arrowManager_8() { return &___arrowManager_8; }
	inline void set_arrowManager_8(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983 * value)
	{
		___arrowManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___arrowManager_8), value);
	}

	inline static int32_t get_offset_of_arrowCollector_9() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08, ___arrowCollector_9)); }
	inline ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08 * get_arrowCollector_9() const { return ___arrowCollector_9; }
	inline ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08 ** get_address_of_arrowCollector_9() { return &___arrowCollector_9; }
	inline void set_arrowCollector_9(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08 * value)
	{
		___arrowCollector_9 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCollector_9), value);
	}
};

struct ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_StaticFields
{
public:
	// System.Action ArrowCollector::ArrowsCollectionComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ArrowsCollectionComplete_4;

public:
	inline static int32_t get_offset_of_ArrowsCollectionComplete_4() { return static_cast<int32_t>(offsetof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_StaticFields, ___ArrowsCollectionComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ArrowsCollectionComplete_4() const { return ___ArrowsCollectionComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ArrowsCollectionComplete_4() { return &___ArrowsCollectionComplete_4; }
	inline void set_ArrowsCollectionComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ArrowsCollectionComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrowsCollectionComplete_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWCOLLECTOR_TB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_H
#ifndef ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#define ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowPrefabController
struct  ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ArrowPrefabController::speed
	float ___speed_4;
	// System.Boolean ArrowPrefabController::isDoneFading
	bool ___isDoneFading_5;
	// System.Boolean ArrowPrefabController::isDoneAnimating
	bool ___isDoneAnimating_6;
	// System.Single ArrowPrefabController::slope
	float ___slope_7;
	// UnityEngine.Color ArrowPrefabController::slopeColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___slopeColor_8;
	// System.String ArrowPrefabController::hex
	String_t* ___hex_9;
	// Strackaline.Arrow ArrowPrefabController::arrow
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520 * ___arrow_10;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_isDoneFading_5() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___isDoneFading_5)); }
	inline bool get_isDoneFading_5() const { return ___isDoneFading_5; }
	inline bool* get_address_of_isDoneFading_5() { return &___isDoneFading_5; }
	inline void set_isDoneFading_5(bool value)
	{
		___isDoneFading_5 = value;
	}

	inline static int32_t get_offset_of_isDoneAnimating_6() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___isDoneAnimating_6)); }
	inline bool get_isDoneAnimating_6() const { return ___isDoneAnimating_6; }
	inline bool* get_address_of_isDoneAnimating_6() { return &___isDoneAnimating_6; }
	inline void set_isDoneAnimating_6(bool value)
	{
		___isDoneAnimating_6 = value;
	}

	inline static int32_t get_offset_of_slope_7() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___slope_7)); }
	inline float get_slope_7() const { return ___slope_7; }
	inline float* get_address_of_slope_7() { return &___slope_7; }
	inline void set_slope_7(float value)
	{
		___slope_7 = value;
	}

	inline static int32_t get_offset_of_slopeColor_8() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___slopeColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_slopeColor_8() const { return ___slopeColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_slopeColor_8() { return &___slopeColor_8; }
	inline void set_slopeColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___slopeColor_8 = value;
	}

	inline static int32_t get_offset_of_hex_9() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___hex_9)); }
	inline String_t* get_hex_9() const { return ___hex_9; }
	inline String_t** get_address_of_hex_9() { return &___hex_9; }
	inline void set_hex_9(String_t* value)
	{
		___hex_9 = value;
		Il2CppCodeGenWriteBarrier((&___hex_9), value);
	}

	inline static int32_t get_offset_of_arrow_10() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___arrow_10)); }
	inline Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520 * get_arrow_10() const { return ___arrow_10; }
	inline Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520 ** get_address_of_arrow_10() { return &___arrow_10; }
	inline void set_arrow_10(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520 * value)
	{
		___arrow_10 = value;
		Il2CppCodeGenWriteBarrier((&___arrow_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#ifndef BALLDRAG_T98CB96C6069713A0429A7AC98A108AED42299166_H
#define BALLDRAG_T98CB96C6069713A0429A7AC98A108AED42299166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallDrag
struct  BallDrag_t98CB96C6069713A0429A7AC98A108AED42299166  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera BallDrag::holeCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___holeCamera_4;

public:
	inline static int32_t get_offset_of_holeCamera_4() { return static_cast<int32_t>(offsetof(BallDrag_t98CB96C6069713A0429A7AC98A108AED42299166, ___holeCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_holeCamera_4() const { return ___holeCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_holeCamera_4() { return &___holeCamera_4; }
	inline void set_holeCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___holeCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___holeCamera_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLDRAG_T98CB96C6069713A0429A7AC98A108AED42299166_H
#ifndef CALENDARCONTROLLER_TF48490BCBA0FD0DAAC6B0303E20302A057212338_H
#define CALENDARCONTROLLER_TF48490BCBA0FD0DAAC6B0303E20302A057212338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalendarController
struct  CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform CalendarController::scrollViewCont
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___scrollViewCont_4;
	// UnityEngine.GameObject CalendarController::listBtnPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___listBtnPrefab_5;
	// CourseManager CalendarController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_6;
	// UnityEngine.GameObject CalendarController::dailyTemplateHoleBtnPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___dailyTemplateHoleBtnPrefab_7;
	// JackManager CalendarController::jackManager
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F * ___jackManager_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CalendarController::holeLocationPrefabList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___holeLocationPrefabList_9;

public:
	inline static int32_t get_offset_of_scrollViewCont_4() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___scrollViewCont_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_scrollViewCont_4() const { return ___scrollViewCont_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_scrollViewCont_4() { return &___scrollViewCont_4; }
	inline void set_scrollViewCont_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___scrollViewCont_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollViewCont_4), value);
	}

	inline static int32_t get_offset_of_listBtnPrefab_5() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___listBtnPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_listBtnPrefab_5() const { return ___listBtnPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_listBtnPrefab_5() { return &___listBtnPrefab_5; }
	inline void set_listBtnPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___listBtnPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___listBtnPrefab_5), value);
	}

	inline static int32_t get_offset_of_courseManager_6() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___courseManager_6)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_6() const { return ___courseManager_6; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_6() { return &___courseManager_6; }
	inline void set_courseManager_6(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_6), value);
	}

	inline static int32_t get_offset_of_dailyTemplateHoleBtnPrefab_7() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___dailyTemplateHoleBtnPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_dailyTemplateHoleBtnPrefab_7() const { return ___dailyTemplateHoleBtnPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_dailyTemplateHoleBtnPrefab_7() { return &___dailyTemplateHoleBtnPrefab_7; }
	inline void set_dailyTemplateHoleBtnPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___dailyTemplateHoleBtnPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___dailyTemplateHoleBtnPrefab_7), value);
	}

	inline static int32_t get_offset_of_jackManager_8() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___jackManager_8)); }
	inline JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F * get_jackManager_8() const { return ___jackManager_8; }
	inline JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F ** get_address_of_jackManager_8() { return &___jackManager_8; }
	inline void set_jackManager_8(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F * value)
	{
		___jackManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___jackManager_8), value);
	}

	inline static int32_t get_offset_of_holeLocationPrefabList_9() { return static_cast<int32_t>(offsetof(CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338, ___holeLocationPrefabList_9)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_holeLocationPrefabList_9() const { return ___holeLocationPrefabList_9; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_holeLocationPrefabList_9() { return &___holeLocationPrefabList_9; }
	inline void set_holeLocationPrefabList_9(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___holeLocationPrefabList_9 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocationPrefabList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALENDARCONTROLLER_TF48490BCBA0FD0DAAC6B0303E20302A057212338_H
#ifndef CAMERAZOOM_TD8CB4E12EE033A7224A82B1849DFC88378E6479B_H
#define CAMERAZOOM_TD8CB4E12EE033A7224A82B1849DFC88378E6479B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraZoom
struct  CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CameraZoom::zoomSpeed
	float ___zoomSpeed_4;
	// System.Single CameraZoom::keyZoomSpeed
	float ___keyZoomSpeed_5;

public:
	inline static int32_t get_offset_of_zoomSpeed_4() { return static_cast<int32_t>(offsetof(CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B, ___zoomSpeed_4)); }
	inline float get_zoomSpeed_4() const { return ___zoomSpeed_4; }
	inline float* get_address_of_zoomSpeed_4() { return &___zoomSpeed_4; }
	inline void set_zoomSpeed_4(float value)
	{
		___zoomSpeed_4 = value;
	}

	inline static int32_t get_offset_of_keyZoomSpeed_5() { return static_cast<int32_t>(offsetof(CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B, ___keyZoomSpeed_5)); }
	inline float get_keyZoomSpeed_5() const { return ___keyZoomSpeed_5; }
	inline float* get_address_of_keyZoomSpeed_5() { return &___keyZoomSpeed_5; }
	inline void set_keyZoomSpeed_5(float value)
	{
		___keyZoomSpeed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAZOOM_TD8CB4E12EE033A7224A82B1849DFC88378E6479B_H
#ifndef CREATEHILLS_T47E99F12A2645EABF95BBF8CB4E411573E2030D5_H
#define CREATEHILLS_T47E99F12A2645EABF95BBF8CB4E411573E2030D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateHills
struct  CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture CreateHills::hillTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___hillTexture_4;
	// UnityEngine.PhysicsMaterial2D CreateHills::hillPhysicsMaterial
	PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * ___hillPhysicsMaterial_5;
	// System.Int32 CreateHills::numberOfPoints
	int32_t ___numberOfPoints_6;
	// System.Int32 CreateHills::numberOfHills
	int32_t ___numberOfHills_7;
	// UnityEngine.GameObject CreateHills::ball
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ball_8;
	// UnityEngine.Vector3 CreateHills::storedPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___storedPosition_9;
	// Vectrosity.VectorLine CreateHills::hills
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___hills_10;
	// UnityEngine.Vector2[] CreateHills::splinePoints
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___splinePoints_11;

public:
	inline static int32_t get_offset_of_hillTexture_4() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___hillTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_hillTexture_4() const { return ___hillTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_hillTexture_4() { return &___hillTexture_4; }
	inline void set_hillTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___hillTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___hillTexture_4), value);
	}

	inline static int32_t get_offset_of_hillPhysicsMaterial_5() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___hillPhysicsMaterial_5)); }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * get_hillPhysicsMaterial_5() const { return ___hillPhysicsMaterial_5; }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 ** get_address_of_hillPhysicsMaterial_5() { return &___hillPhysicsMaterial_5; }
	inline void set_hillPhysicsMaterial_5(PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * value)
	{
		___hillPhysicsMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___hillPhysicsMaterial_5), value);
	}

	inline static int32_t get_offset_of_numberOfPoints_6() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___numberOfPoints_6)); }
	inline int32_t get_numberOfPoints_6() const { return ___numberOfPoints_6; }
	inline int32_t* get_address_of_numberOfPoints_6() { return &___numberOfPoints_6; }
	inline void set_numberOfPoints_6(int32_t value)
	{
		___numberOfPoints_6 = value;
	}

	inline static int32_t get_offset_of_numberOfHills_7() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___numberOfHills_7)); }
	inline int32_t get_numberOfHills_7() const { return ___numberOfHills_7; }
	inline int32_t* get_address_of_numberOfHills_7() { return &___numberOfHills_7; }
	inline void set_numberOfHills_7(int32_t value)
	{
		___numberOfHills_7 = value;
	}

	inline static int32_t get_offset_of_ball_8() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___ball_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ball_8() const { return ___ball_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ball_8() { return &___ball_8; }
	inline void set_ball_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ball_8 = value;
		Il2CppCodeGenWriteBarrier((&___ball_8), value);
	}

	inline static int32_t get_offset_of_storedPosition_9() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___storedPosition_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_storedPosition_9() const { return ___storedPosition_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_storedPosition_9() { return &___storedPosition_9; }
	inline void set_storedPosition_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___storedPosition_9 = value;
	}

	inline static int32_t get_offset_of_hills_10() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___hills_10)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_hills_10() const { return ___hills_10; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_hills_10() { return &___hills_10; }
	inline void set_hills_10(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___hills_10 = value;
		Il2CppCodeGenWriteBarrier((&___hills_10), value);
	}

	inline static int32_t get_offset_of_splinePoints_11() { return static_cast<int32_t>(offsetof(CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5, ___splinePoints_11)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_splinePoints_11() const { return ___splinePoints_11; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_splinePoints_11() { return &___splinePoints_11; }
	inline void set_splinePoints_11(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___splinePoints_11 = value;
		Il2CppCodeGenWriteBarrier((&___splinePoints_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEHILLS_T47E99F12A2645EABF95BBF8CB4E411573E2030D5_H
#ifndef CREATESTARS_T07F8B8018F982C1C11EFF017130F2804CD785297_H
#define CREATESTARS_T07F8B8018F982C1C11EFF017130F2804CD785297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateStars
struct  CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 CreateStars::numberOfStars
	int32_t ___numberOfStars_4;
	// Vectrosity.VectorLine CreateStars::stars
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___stars_5;

public:
	inline static int32_t get_offset_of_numberOfStars_4() { return static_cast<int32_t>(offsetof(CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297, ___numberOfStars_4)); }
	inline int32_t get_numberOfStars_4() const { return ___numberOfStars_4; }
	inline int32_t* get_address_of_numberOfStars_4() { return &___numberOfStars_4; }
	inline void set_numberOfStars_4(int32_t value)
	{
		___numberOfStars_4 = value;
	}

	inline static int32_t get_offset_of_stars_5() { return static_cast<int32_t>(offsetof(CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297, ___stars_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_stars_5() const { return ___stars_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_stars_5() { return &___stars_5; }
	inline void set_stars_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___stars_5 = value;
		Il2CppCodeGenWriteBarrier((&___stars_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATESTARS_T07F8B8018F982C1C11EFF017130F2804CD785297_H
#ifndef CURVEPOINTCONTROL_TEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543_H
#define CURVEPOINTCONTROL_TEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurvePointControl
struct  CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 CurvePointControl::objectNumber
	int32_t ___objectNumber_4;
	// UnityEngine.GameObject CurvePointControl::controlObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controlObject_5;
	// UnityEngine.GameObject CurvePointControl::controlObject2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controlObject2_6;

public:
	inline static int32_t get_offset_of_objectNumber_4() { return static_cast<int32_t>(offsetof(CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543, ___objectNumber_4)); }
	inline int32_t get_objectNumber_4() const { return ___objectNumber_4; }
	inline int32_t* get_address_of_objectNumber_4() { return &___objectNumber_4; }
	inline void set_objectNumber_4(int32_t value)
	{
		___objectNumber_4 = value;
	}

	inline static int32_t get_offset_of_controlObject_5() { return static_cast<int32_t>(offsetof(CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543, ___controlObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controlObject_5() const { return ___controlObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controlObject_5() { return &___controlObject_5; }
	inline void set_controlObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controlObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___controlObject_5), value);
	}

	inline static int32_t get_offset_of_controlObject2_6() { return static_cast<int32_t>(offsetof(CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543, ___controlObject2_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controlObject2_6() const { return ___controlObject2_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controlObject2_6() { return &___controlObject2_6; }
	inline void set_controlObject2_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controlObject2_6 = value;
		Il2CppCodeGenWriteBarrier((&___controlObject2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEPOINTCONTROL_TEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543_H
#ifndef CUSTOMCOLLIDER_T350914B397EA4CB11F66C9E8F20B78008B33B69E_H
#define CUSTOMCOLLIDER_T350914B397EA4CB11F66C9E8F20B78008B33B69E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomCollider
struct  CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3[] CustomCollider::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_4;
	// System.Int32[] CustomCollider::triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___triangles_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CustomCollider::vertList
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___vertList_6;
	// UnityEngine.GameObject CustomCollider::greenLoaded
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenLoaded_7;
	// UnityEngine.GameObject CustomCollider::lowestPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lowestPoint_8;
	// UnityEngine.GameObject CustomCollider::highestPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___highestPoint_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CustomCollider::pointList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___pointList_10;

public:
	inline static int32_t get_offset_of_vertices_4() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___vertices_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_4() const { return ___vertices_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_4() { return &___vertices_4; }
	inline void set_vertices_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_4 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_4), value);
	}

	inline static int32_t get_offset_of_triangles_5() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___triangles_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_triangles_5() const { return ___triangles_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_triangles_5() { return &___triangles_5; }
	inline void set_triangles_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___triangles_5 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_5), value);
	}

	inline static int32_t get_offset_of_vertList_6() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___vertList_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_vertList_6() const { return ___vertList_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_vertList_6() { return &___vertList_6; }
	inline void set_vertList_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___vertList_6 = value;
		Il2CppCodeGenWriteBarrier((&___vertList_6), value);
	}

	inline static int32_t get_offset_of_greenLoaded_7() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___greenLoaded_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenLoaded_7() const { return ___greenLoaded_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenLoaded_7() { return &___greenLoaded_7; }
	inline void set_greenLoaded_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___greenLoaded_7), value);
	}

	inline static int32_t get_offset_of_lowestPoint_8() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___lowestPoint_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lowestPoint_8() const { return ___lowestPoint_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lowestPoint_8() { return &___lowestPoint_8; }
	inline void set_lowestPoint_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lowestPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___lowestPoint_8), value);
	}

	inline static int32_t get_offset_of_highestPoint_9() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___highestPoint_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_highestPoint_9() const { return ___highestPoint_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_highestPoint_9() { return &___highestPoint_9; }
	inline void set_highestPoint_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___highestPoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___highestPoint_9), value);
	}

	inline static int32_t get_offset_of_pointList_10() { return static_cast<int32_t>(offsetof(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E, ___pointList_10)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_pointList_10() const { return ___pointList_10; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_pointList_10() { return &___pointList_10; }
	inline void set_pointList_10(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___pointList_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointList_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCOLLIDER_T350914B397EA4CB11F66C9E8F20B78008B33B69E_H
#ifndef DAILYTEMPLATEHOLE_TF7629CEBE34ED72B83D645A5425D35E84CFB3F11_H
#define DAILYTEMPLATEHOLE_TF7629CEBE34ED72B83D645A5425D35E84CFB3F11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyTemplateHole
struct  DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.TemplateHoleLocation DailyTemplateHole::templateHoleLocation
	TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF * ___templateHoleLocation_5;

public:
	inline static int32_t get_offset_of_templateHoleLocation_5() { return static_cast<int32_t>(offsetof(DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11, ___templateHoleLocation_5)); }
	inline TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF * get_templateHoleLocation_5() const { return ___templateHoleLocation_5; }
	inline TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF ** get_address_of_templateHoleLocation_5() { return &___templateHoleLocation_5; }
	inline void set_templateHoleLocation_5(TemplateHoleLocation_tF14A1EAFA78ACD6A008E1E7D155A24E6C31FAEDF * value)
	{
		___templateHoleLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___templateHoleLocation_5), value);
	}
};

struct DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11_StaticFields
{
public:
	// DailyTemplateHole_DailyTemplateHolevent DailyTemplateHole::TemplateHoleSelected
	DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10 * ___TemplateHoleSelected_4;

public:
	inline static int32_t get_offset_of_TemplateHoleSelected_4() { return static_cast<int32_t>(offsetof(DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11_StaticFields, ___TemplateHoleSelected_4)); }
	inline DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10 * get_TemplateHoleSelected_4() const { return ___TemplateHoleSelected_4; }
	inline DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10 ** get_address_of_TemplateHoleSelected_4() { return &___TemplateHoleSelected_4; }
	inline void set_TemplateHoleSelected_4(DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10 * value)
	{
		___TemplateHoleSelected_4 = value;
		Il2CppCodeGenWriteBarrier((&___TemplateHoleSelected_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAILYTEMPLATEHOLE_TF7629CEBE34ED72B83D645A5425D35E84CFB3F11_H
#ifndef DATALOADEDMANAGER_T7C48A73C9DF0BC04E093E7D964BE7428B88687F6_H
#define DATALOADEDMANAGER_T7C48A73C9DF0BC04E093E7D964BE7428B88687F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataLoadedManager
struct  DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DataLoadedManager::arrowsLoaded
	bool ___arrowsLoaded_6;
	// System.Boolean DataLoadedManager::contoursLoaded
	bool ___contoursLoaded_7;
	// System.Boolean DataLoadedManager::puttsLoaded
	bool ___puttsLoaded_8;
	// System.Boolean DataLoadedManager::jacksCreated
	bool ___jacksCreated_9;
	// System.Boolean DataLoadedManager::jacksElevetionSet
	bool ___jacksElevetionSet_10;
	// System.Boolean DataLoadedManager::greenModelLoaded
	bool ___greenModelLoaded_11;
	// System.Boolean DataLoadedManager::appDataLoaded
	bool ___appDataLoaded_12;

public:
	inline static int32_t get_offset_of_arrowsLoaded_6() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___arrowsLoaded_6)); }
	inline bool get_arrowsLoaded_6() const { return ___arrowsLoaded_6; }
	inline bool* get_address_of_arrowsLoaded_6() { return &___arrowsLoaded_6; }
	inline void set_arrowsLoaded_6(bool value)
	{
		___arrowsLoaded_6 = value;
	}

	inline static int32_t get_offset_of_contoursLoaded_7() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___contoursLoaded_7)); }
	inline bool get_contoursLoaded_7() const { return ___contoursLoaded_7; }
	inline bool* get_address_of_contoursLoaded_7() { return &___contoursLoaded_7; }
	inline void set_contoursLoaded_7(bool value)
	{
		___contoursLoaded_7 = value;
	}

	inline static int32_t get_offset_of_puttsLoaded_8() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___puttsLoaded_8)); }
	inline bool get_puttsLoaded_8() const { return ___puttsLoaded_8; }
	inline bool* get_address_of_puttsLoaded_8() { return &___puttsLoaded_8; }
	inline void set_puttsLoaded_8(bool value)
	{
		___puttsLoaded_8 = value;
	}

	inline static int32_t get_offset_of_jacksCreated_9() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___jacksCreated_9)); }
	inline bool get_jacksCreated_9() const { return ___jacksCreated_9; }
	inline bool* get_address_of_jacksCreated_9() { return &___jacksCreated_9; }
	inline void set_jacksCreated_9(bool value)
	{
		___jacksCreated_9 = value;
	}

	inline static int32_t get_offset_of_jacksElevetionSet_10() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___jacksElevetionSet_10)); }
	inline bool get_jacksElevetionSet_10() const { return ___jacksElevetionSet_10; }
	inline bool* get_address_of_jacksElevetionSet_10() { return &___jacksElevetionSet_10; }
	inline void set_jacksElevetionSet_10(bool value)
	{
		___jacksElevetionSet_10 = value;
	}

	inline static int32_t get_offset_of_greenModelLoaded_11() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___greenModelLoaded_11)); }
	inline bool get_greenModelLoaded_11() const { return ___greenModelLoaded_11; }
	inline bool* get_address_of_greenModelLoaded_11() { return &___greenModelLoaded_11; }
	inline void set_greenModelLoaded_11(bool value)
	{
		___greenModelLoaded_11 = value;
	}

	inline static int32_t get_offset_of_appDataLoaded_12() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6, ___appDataLoaded_12)); }
	inline bool get_appDataLoaded_12() const { return ___appDataLoaded_12; }
	inline bool* get_address_of_appDataLoaded_12() { return &___appDataLoaded_12; }
	inline void set_appDataLoaded_12(bool value)
	{
		___appDataLoaded_12 = value;
	}
};

struct DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields
{
public:
	// System.Action DataLoadedManager::LoadJacksAfterAPIDataCopmlete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___LoadJacksAfterAPIDataCopmlete_4;
	// System.Action DataLoadedManager::DataLoadedComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DataLoadedComplete_5;

public:
	inline static int32_t get_offset_of_LoadJacksAfterAPIDataCopmlete_4() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields, ___LoadJacksAfterAPIDataCopmlete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_LoadJacksAfterAPIDataCopmlete_4() const { return ___LoadJacksAfterAPIDataCopmlete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_LoadJacksAfterAPIDataCopmlete_4() { return &___LoadJacksAfterAPIDataCopmlete_4; }
	inline void set_LoadJacksAfterAPIDataCopmlete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___LoadJacksAfterAPIDataCopmlete_4 = value;
		Il2CppCodeGenWriteBarrier((&___LoadJacksAfterAPIDataCopmlete_4), value);
	}

	inline static int32_t get_offset_of_DataLoadedComplete_5() { return static_cast<int32_t>(offsetof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields, ___DataLoadedComplete_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DataLoadedComplete_5() const { return ___DataLoadedComplete_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DataLoadedComplete_5() { return &___DataLoadedComplete_5; }
	inline void set_DataLoadedComplete_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DataLoadedComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___DataLoadedComplete_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATALOADEDMANAGER_T7C48A73C9DF0BC04E093E7D964BE7428B88687F6_H
#ifndef DRAWBOX_TDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C_H
#define DRAWBOX_TDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawBox
struct  DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DrawBox::moveSpeed
	float ___moveSpeed_4;
	// System.Single DrawBox::explodePower
	float ___explodePower_5;
	// UnityEngine.Camera DrawBox::vectorCam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___vectorCam_6;
	// System.Boolean DrawBox::mouseDown
	bool ___mouseDown_7;
	// UnityEngine.Rigidbody[] DrawBox::rigidbodies
	RigidbodyU5BU5D_t9F5D984DC2777DA3C6E2BEC6CE9D0F1C3D2E851B* ___rigidbodies_8;
	// System.Boolean DrawBox::canClick
	bool ___canClick_9;
	// System.Boolean DrawBox::boxDrawn
	bool ___boxDrawn_10;

public:
	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_explodePower_5() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___explodePower_5)); }
	inline float get_explodePower_5() const { return ___explodePower_5; }
	inline float* get_address_of_explodePower_5() { return &___explodePower_5; }
	inline void set_explodePower_5(float value)
	{
		___explodePower_5 = value;
	}

	inline static int32_t get_offset_of_vectorCam_6() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___vectorCam_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_vectorCam_6() const { return ___vectorCam_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_vectorCam_6() { return &___vectorCam_6; }
	inline void set_vectorCam_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___vectorCam_6 = value;
		Il2CppCodeGenWriteBarrier((&___vectorCam_6), value);
	}

	inline static int32_t get_offset_of_mouseDown_7() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___mouseDown_7)); }
	inline bool get_mouseDown_7() const { return ___mouseDown_7; }
	inline bool* get_address_of_mouseDown_7() { return &___mouseDown_7; }
	inline void set_mouseDown_7(bool value)
	{
		___mouseDown_7 = value;
	}

	inline static int32_t get_offset_of_rigidbodies_8() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___rigidbodies_8)); }
	inline RigidbodyU5BU5D_t9F5D984DC2777DA3C6E2BEC6CE9D0F1C3D2E851B* get_rigidbodies_8() const { return ___rigidbodies_8; }
	inline RigidbodyU5BU5D_t9F5D984DC2777DA3C6E2BEC6CE9D0F1C3D2E851B** get_address_of_rigidbodies_8() { return &___rigidbodies_8; }
	inline void set_rigidbodies_8(RigidbodyU5BU5D_t9F5D984DC2777DA3C6E2BEC6CE9D0F1C3D2E851B* value)
	{
		___rigidbodies_8 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbodies_8), value);
	}

	inline static int32_t get_offset_of_canClick_9() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___canClick_9)); }
	inline bool get_canClick_9() const { return ___canClick_9; }
	inline bool* get_address_of_canClick_9() { return &___canClick_9; }
	inline void set_canClick_9(bool value)
	{
		___canClick_9 = value;
	}

	inline static int32_t get_offset_of_boxDrawn_10() { return static_cast<int32_t>(offsetof(DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C, ___boxDrawn_10)); }
	inline bool get_boxDrawn_10() const { return ___boxDrawn_10; }
	inline bool* get_address_of_boxDrawn_10() { return &___boxDrawn_10; }
	inline void set_boxDrawn_10(bool value)
	{
		___boxDrawn_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWBOX_TDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C_H
#ifndef DRAWCURVE_TCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_H
#define DRAWCURVE_TCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawCurve
struct  DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture DrawCurve::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// UnityEngine.Color DrawCurve::lineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lineColor_5;
	// UnityEngine.Texture DrawCurve::dottedLineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___dottedLineTexture_6;
	// UnityEngine.Color DrawCurve::dottedLineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___dottedLineColor_7;
	// System.Int32 DrawCurve::segments
	int32_t ___segments_8;
	// UnityEngine.GameObject DrawCurve::anchorPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___anchorPoint_9;
	// UnityEngine.GameObject DrawCurve::controlPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controlPoint_10;
	// System.Int32 DrawCurve::numberOfCurves
	int32_t ___numberOfCurves_11;
	// Vectrosity.VectorLine DrawCurve::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_12;
	// Vectrosity.VectorLine DrawCurve::controlLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___controlLine_13;
	// System.Int32 DrawCurve::pointIndex
	int32_t ___pointIndex_14;
	// UnityEngine.GameObject DrawCurve::anchorObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___anchorObject_15;
	// System.Int32 DrawCurve::oldWidth
	int32_t ___oldWidth_16;
	// System.Boolean DrawCurve::useDottedLine
	bool ___useDottedLine_17;
	// System.Boolean DrawCurve::oldDottedLineSetting
	bool ___oldDottedLineSetting_18;
	// System.Int32 DrawCurve::oldSegments
	int32_t ___oldSegments_19;
	// System.Boolean DrawCurve::listPoints
	bool ___listPoints_20;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_lineColor_5() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___lineColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lineColor_5() const { return ___lineColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lineColor_5() { return &___lineColor_5; }
	inline void set_lineColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lineColor_5 = value;
	}

	inline static int32_t get_offset_of_dottedLineTexture_6() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___dottedLineTexture_6)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_dottedLineTexture_6() const { return ___dottedLineTexture_6; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_dottedLineTexture_6() { return &___dottedLineTexture_6; }
	inline void set_dottedLineTexture_6(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___dottedLineTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___dottedLineTexture_6), value);
	}

	inline static int32_t get_offset_of_dottedLineColor_7() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___dottedLineColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_dottedLineColor_7() const { return ___dottedLineColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_dottedLineColor_7() { return &___dottedLineColor_7; }
	inline void set_dottedLineColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___dottedLineColor_7 = value;
	}

	inline static int32_t get_offset_of_segments_8() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___segments_8)); }
	inline int32_t get_segments_8() const { return ___segments_8; }
	inline int32_t* get_address_of_segments_8() { return &___segments_8; }
	inline void set_segments_8(int32_t value)
	{
		___segments_8 = value;
	}

	inline static int32_t get_offset_of_anchorPoint_9() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___anchorPoint_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_anchorPoint_9() const { return ___anchorPoint_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_anchorPoint_9() { return &___anchorPoint_9; }
	inline void set_anchorPoint_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___anchorPoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___anchorPoint_9), value);
	}

	inline static int32_t get_offset_of_controlPoint_10() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___controlPoint_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controlPoint_10() const { return ___controlPoint_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controlPoint_10() { return &___controlPoint_10; }
	inline void set_controlPoint_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controlPoint_10 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoint_10), value);
	}

	inline static int32_t get_offset_of_numberOfCurves_11() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___numberOfCurves_11)); }
	inline int32_t get_numberOfCurves_11() const { return ___numberOfCurves_11; }
	inline int32_t* get_address_of_numberOfCurves_11() { return &___numberOfCurves_11; }
	inline void set_numberOfCurves_11(int32_t value)
	{
		___numberOfCurves_11 = value;
	}

	inline static int32_t get_offset_of_line_12() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___line_12)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_12() const { return ___line_12; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_12() { return &___line_12; }
	inline void set_line_12(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_12 = value;
		Il2CppCodeGenWriteBarrier((&___line_12), value);
	}

	inline static int32_t get_offset_of_controlLine_13() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___controlLine_13)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_controlLine_13() const { return ___controlLine_13; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_controlLine_13() { return &___controlLine_13; }
	inline void set_controlLine_13(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___controlLine_13 = value;
		Il2CppCodeGenWriteBarrier((&___controlLine_13), value);
	}

	inline static int32_t get_offset_of_pointIndex_14() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___pointIndex_14)); }
	inline int32_t get_pointIndex_14() const { return ___pointIndex_14; }
	inline int32_t* get_address_of_pointIndex_14() { return &___pointIndex_14; }
	inline void set_pointIndex_14(int32_t value)
	{
		___pointIndex_14 = value;
	}

	inline static int32_t get_offset_of_anchorObject_15() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___anchorObject_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_anchorObject_15() const { return ___anchorObject_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_anchorObject_15() { return &___anchorObject_15; }
	inline void set_anchorObject_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___anchorObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___anchorObject_15), value);
	}

	inline static int32_t get_offset_of_oldWidth_16() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___oldWidth_16)); }
	inline int32_t get_oldWidth_16() const { return ___oldWidth_16; }
	inline int32_t* get_address_of_oldWidth_16() { return &___oldWidth_16; }
	inline void set_oldWidth_16(int32_t value)
	{
		___oldWidth_16 = value;
	}

	inline static int32_t get_offset_of_useDottedLine_17() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___useDottedLine_17)); }
	inline bool get_useDottedLine_17() const { return ___useDottedLine_17; }
	inline bool* get_address_of_useDottedLine_17() { return &___useDottedLine_17; }
	inline void set_useDottedLine_17(bool value)
	{
		___useDottedLine_17 = value;
	}

	inline static int32_t get_offset_of_oldDottedLineSetting_18() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___oldDottedLineSetting_18)); }
	inline bool get_oldDottedLineSetting_18() const { return ___oldDottedLineSetting_18; }
	inline bool* get_address_of_oldDottedLineSetting_18() { return &___oldDottedLineSetting_18; }
	inline void set_oldDottedLineSetting_18(bool value)
	{
		___oldDottedLineSetting_18 = value;
	}

	inline static int32_t get_offset_of_oldSegments_19() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___oldSegments_19)); }
	inline int32_t get_oldSegments_19() const { return ___oldSegments_19; }
	inline int32_t* get_address_of_oldSegments_19() { return &___oldSegments_19; }
	inline void set_oldSegments_19(int32_t value)
	{
		___oldSegments_19 = value;
	}

	inline static int32_t get_offset_of_listPoints_20() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE, ___listPoints_20)); }
	inline bool get_listPoints_20() const { return ___listPoints_20; }
	inline bool* get_address_of_listPoints_20() { return &___listPoints_20; }
	inline void set_listPoints_20(bool value)
	{
		___listPoints_20 = value;
	}
};

struct DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields
{
public:
	// DrawCurve DrawCurve::use
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE * ___use_21;
	// UnityEngine.Camera DrawCurve::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_22;

public:
	inline static int32_t get_offset_of_use_21() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields, ___use_21)); }
	inline DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE * get_use_21() const { return ___use_21; }
	inline DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE ** get_address_of_use_21() { return &___use_21; }
	inline void set_use_21(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE * value)
	{
		___use_21 = value;
		Il2CppCodeGenWriteBarrier((&___use_21), value);
	}

	inline static int32_t get_offset_of_cam_22() { return static_cast<int32_t>(offsetof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields, ___cam_22)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_22() const { return ___cam_22; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_22() { return &___cam_22; }
	inline void set_cam_22(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_22 = value;
		Il2CppCodeGenWriteBarrier((&___cam_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWCURVE_TCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_H
#ifndef DRAWGRID_T44F7755547A0EC8DDF79656E4DA45FDE56721149_H
#define DRAWGRID_T44F7755547A0EC8DDF79656E4DA45FDE56721149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawGrid
struct  DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 DrawGrid::gridPixels
	int32_t ___gridPixels_4;
	// Vectrosity.VectorLine DrawGrid::gridLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___gridLine_5;

public:
	inline static int32_t get_offset_of_gridPixels_4() { return static_cast<int32_t>(offsetof(DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149, ___gridPixels_4)); }
	inline int32_t get_gridPixels_4() const { return ___gridPixels_4; }
	inline int32_t* get_address_of_gridPixels_4() { return &___gridPixels_4; }
	inline void set_gridPixels_4(int32_t value)
	{
		___gridPixels_4 = value;
	}

	inline static int32_t get_offset_of_gridLine_5() { return static_cast<int32_t>(offsetof(DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149, ___gridLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_gridLine_5() const { return ___gridLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_gridLine_5() { return &___gridLine_5; }
	inline void set_gridLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___gridLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___gridLine_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWGRID_T44F7755547A0EC8DDF79656E4DA45FDE56721149_H
#ifndef DRAWLINES_T5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1_H
#define DRAWLINES_T5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawLines
struct  DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DrawLines::rotateSpeed
	float ___rotateSpeed_4;
	// System.Single DrawLines::maxPoints
	float ___maxPoints_5;
	// Vectrosity.VectorLine DrawLines::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_6;
	// System.Boolean DrawLines::endReached
	bool ___endReached_7;
	// System.Boolean DrawLines::continuous
	bool ___continuous_8;
	// System.Boolean DrawLines::oldContinuous
	bool ___oldContinuous_9;
	// System.Boolean DrawLines::fillJoins
	bool ___fillJoins_10;
	// System.Boolean DrawLines::oldFillJoins
	bool ___oldFillJoins_11;
	// System.Boolean DrawLines::weldJoins
	bool ___weldJoins_12;
	// System.Boolean DrawLines::oldWeldJoins
	bool ___oldWeldJoins_13;
	// System.Boolean DrawLines::thickLine
	bool ___thickLine_14;
	// System.Boolean DrawLines::canClick
	bool ___canClick_15;

public:
	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_maxPoints_5() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___maxPoints_5)); }
	inline float get_maxPoints_5() const { return ___maxPoints_5; }
	inline float* get_address_of_maxPoints_5() { return &___maxPoints_5; }
	inline void set_maxPoints_5(float value)
	{
		___maxPoints_5 = value;
	}

	inline static int32_t get_offset_of_line_6() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___line_6)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_6() const { return ___line_6; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_6() { return &___line_6; }
	inline void set_line_6(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_6 = value;
		Il2CppCodeGenWriteBarrier((&___line_6), value);
	}

	inline static int32_t get_offset_of_endReached_7() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___endReached_7)); }
	inline bool get_endReached_7() const { return ___endReached_7; }
	inline bool* get_address_of_endReached_7() { return &___endReached_7; }
	inline void set_endReached_7(bool value)
	{
		___endReached_7 = value;
	}

	inline static int32_t get_offset_of_continuous_8() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___continuous_8)); }
	inline bool get_continuous_8() const { return ___continuous_8; }
	inline bool* get_address_of_continuous_8() { return &___continuous_8; }
	inline void set_continuous_8(bool value)
	{
		___continuous_8 = value;
	}

	inline static int32_t get_offset_of_oldContinuous_9() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___oldContinuous_9)); }
	inline bool get_oldContinuous_9() const { return ___oldContinuous_9; }
	inline bool* get_address_of_oldContinuous_9() { return &___oldContinuous_9; }
	inline void set_oldContinuous_9(bool value)
	{
		___oldContinuous_9 = value;
	}

	inline static int32_t get_offset_of_fillJoins_10() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___fillJoins_10)); }
	inline bool get_fillJoins_10() const { return ___fillJoins_10; }
	inline bool* get_address_of_fillJoins_10() { return &___fillJoins_10; }
	inline void set_fillJoins_10(bool value)
	{
		___fillJoins_10 = value;
	}

	inline static int32_t get_offset_of_oldFillJoins_11() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___oldFillJoins_11)); }
	inline bool get_oldFillJoins_11() const { return ___oldFillJoins_11; }
	inline bool* get_address_of_oldFillJoins_11() { return &___oldFillJoins_11; }
	inline void set_oldFillJoins_11(bool value)
	{
		___oldFillJoins_11 = value;
	}

	inline static int32_t get_offset_of_weldJoins_12() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___weldJoins_12)); }
	inline bool get_weldJoins_12() const { return ___weldJoins_12; }
	inline bool* get_address_of_weldJoins_12() { return &___weldJoins_12; }
	inline void set_weldJoins_12(bool value)
	{
		___weldJoins_12 = value;
	}

	inline static int32_t get_offset_of_oldWeldJoins_13() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___oldWeldJoins_13)); }
	inline bool get_oldWeldJoins_13() const { return ___oldWeldJoins_13; }
	inline bool* get_address_of_oldWeldJoins_13() { return &___oldWeldJoins_13; }
	inline void set_oldWeldJoins_13(bool value)
	{
		___oldWeldJoins_13 = value;
	}

	inline static int32_t get_offset_of_thickLine_14() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___thickLine_14)); }
	inline bool get_thickLine_14() const { return ___thickLine_14; }
	inline bool* get_address_of_thickLine_14() { return &___thickLine_14; }
	inline void set_thickLine_14(bool value)
	{
		___thickLine_14 = value;
	}

	inline static int32_t get_offset_of_canClick_15() { return static_cast<int32_t>(offsetof(DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1, ___canClick_15)); }
	inline bool get_canClick_15() const { return ___canClick_15; }
	inline bool* get_address_of_canClick_15() { return &___canClick_15; }
	inline void set_canClick_15(bool value)
	{
		___canClick_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWLINES_T5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1_H
#ifndef DRAWLINESMOUSE_T44BEFAF42C864E4492E03588D600B14A9E808794_H
#define DRAWLINESMOUSE_T44BEFAF42C864E4492E03588D600B14A9E808794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawLinesMouse
struct  DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D DrawLinesMouse::lineTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lineTex_4;
	// System.Int32 DrawLinesMouse::maxPoints
	int32_t ___maxPoints_5;
	// System.Single DrawLinesMouse::lineWidth
	float ___lineWidth_6;
	// System.Int32 DrawLinesMouse::minPixelMove
	int32_t ___minPixelMove_7;
	// System.Boolean DrawLinesMouse::useEndCap
	bool ___useEndCap_8;
	// UnityEngine.Texture2D DrawLinesMouse::capLineTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___capLineTex_9;
	// UnityEngine.Texture2D DrawLinesMouse::capTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___capTex_10;
	// System.Single DrawLinesMouse::capLineWidth
	float ___capLineWidth_11;
	// System.Boolean DrawLinesMouse::line3D
	bool ___line3D_12;
	// System.Single DrawLinesMouse::distanceFromCamera
	float ___distanceFromCamera_13;
	// Vectrosity.VectorLine DrawLinesMouse::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_14;
	// UnityEngine.Vector3 DrawLinesMouse::previousPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___previousPosition_15;
	// System.Int32 DrawLinesMouse::sqrMinPixelMove
	int32_t ___sqrMinPixelMove_16;
	// System.Boolean DrawLinesMouse::canDraw
	bool ___canDraw_17;

public:
	inline static int32_t get_offset_of_lineTex_4() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___lineTex_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lineTex_4() const { return ___lineTex_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lineTex_4() { return &___lineTex_4; }
	inline void set_lineTex_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lineTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex_4), value);
	}

	inline static int32_t get_offset_of_maxPoints_5() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___maxPoints_5)); }
	inline int32_t get_maxPoints_5() const { return ___maxPoints_5; }
	inline int32_t* get_address_of_maxPoints_5() { return &___maxPoints_5; }
	inline void set_maxPoints_5(int32_t value)
	{
		___maxPoints_5 = value;
	}

	inline static int32_t get_offset_of_lineWidth_6() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___lineWidth_6)); }
	inline float get_lineWidth_6() const { return ___lineWidth_6; }
	inline float* get_address_of_lineWidth_6() { return &___lineWidth_6; }
	inline void set_lineWidth_6(float value)
	{
		___lineWidth_6 = value;
	}

	inline static int32_t get_offset_of_minPixelMove_7() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___minPixelMove_7)); }
	inline int32_t get_minPixelMove_7() const { return ___minPixelMove_7; }
	inline int32_t* get_address_of_minPixelMove_7() { return &___minPixelMove_7; }
	inline void set_minPixelMove_7(int32_t value)
	{
		___minPixelMove_7 = value;
	}

	inline static int32_t get_offset_of_useEndCap_8() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___useEndCap_8)); }
	inline bool get_useEndCap_8() const { return ___useEndCap_8; }
	inline bool* get_address_of_useEndCap_8() { return &___useEndCap_8; }
	inline void set_useEndCap_8(bool value)
	{
		___useEndCap_8 = value;
	}

	inline static int32_t get_offset_of_capLineTex_9() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___capLineTex_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_capLineTex_9() const { return ___capLineTex_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_capLineTex_9() { return &___capLineTex_9; }
	inline void set_capLineTex_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___capLineTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___capLineTex_9), value);
	}

	inline static int32_t get_offset_of_capTex_10() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___capTex_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_capTex_10() const { return ___capTex_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_capTex_10() { return &___capTex_10; }
	inline void set_capTex_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___capTex_10 = value;
		Il2CppCodeGenWriteBarrier((&___capTex_10), value);
	}

	inline static int32_t get_offset_of_capLineWidth_11() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___capLineWidth_11)); }
	inline float get_capLineWidth_11() const { return ___capLineWidth_11; }
	inline float* get_address_of_capLineWidth_11() { return &___capLineWidth_11; }
	inline void set_capLineWidth_11(float value)
	{
		___capLineWidth_11 = value;
	}

	inline static int32_t get_offset_of_line3D_12() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___line3D_12)); }
	inline bool get_line3D_12() const { return ___line3D_12; }
	inline bool* get_address_of_line3D_12() { return &___line3D_12; }
	inline void set_line3D_12(bool value)
	{
		___line3D_12 = value;
	}

	inline static int32_t get_offset_of_distanceFromCamera_13() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___distanceFromCamera_13)); }
	inline float get_distanceFromCamera_13() const { return ___distanceFromCamera_13; }
	inline float* get_address_of_distanceFromCamera_13() { return &___distanceFromCamera_13; }
	inline void set_distanceFromCamera_13(float value)
	{
		___distanceFromCamera_13 = value;
	}

	inline static int32_t get_offset_of_line_14() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___line_14)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_14() const { return ___line_14; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_14() { return &___line_14; }
	inline void set_line_14(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_14 = value;
		Il2CppCodeGenWriteBarrier((&___line_14), value);
	}

	inline static int32_t get_offset_of_previousPosition_15() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___previousPosition_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_previousPosition_15() const { return ___previousPosition_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_previousPosition_15() { return &___previousPosition_15; }
	inline void set_previousPosition_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___previousPosition_15 = value;
	}

	inline static int32_t get_offset_of_sqrMinPixelMove_16() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___sqrMinPixelMove_16)); }
	inline int32_t get_sqrMinPixelMove_16() const { return ___sqrMinPixelMove_16; }
	inline int32_t* get_address_of_sqrMinPixelMove_16() { return &___sqrMinPixelMove_16; }
	inline void set_sqrMinPixelMove_16(int32_t value)
	{
		___sqrMinPixelMove_16 = value;
	}

	inline static int32_t get_offset_of_canDraw_17() { return static_cast<int32_t>(offsetof(DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794, ___canDraw_17)); }
	inline bool get_canDraw_17() const { return ___canDraw_17; }
	inline bool* get_address_of_canDraw_17() { return &___canDraw_17; }
	inline void set_canDraw_17(bool value)
	{
		___canDraw_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWLINESMOUSE_T44BEFAF42C864E4492E03588D600B14A9E808794_H
#ifndef DRAWLINESTOUCH_TCB035475C9054C2816A36335F3BAF562DB888233_H
#define DRAWLINESTOUCH_TCB035475C9054C2816A36335F3BAF562DB888233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawLinesTouch
struct  DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D DrawLinesTouch::lineTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lineTex_4;
	// System.Int32 DrawLinesTouch::maxPoints
	int32_t ___maxPoints_5;
	// System.Single DrawLinesTouch::lineWidth
	float ___lineWidth_6;
	// System.Int32 DrawLinesTouch::minPixelMove
	int32_t ___minPixelMove_7;
	// System.Boolean DrawLinesTouch::useEndCap
	bool ___useEndCap_8;
	// UnityEngine.Texture2D DrawLinesTouch::capLineTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___capLineTex_9;
	// UnityEngine.Texture2D DrawLinesTouch::capTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___capTex_10;
	// System.Single DrawLinesTouch::capLineWidth
	float ___capLineWidth_11;
	// Vectrosity.VectorLine DrawLinesTouch::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_12;
	// UnityEngine.Vector2 DrawLinesTouch::previousPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___previousPosition_13;
	// System.Int32 DrawLinesTouch::sqrMinPixelMove
	int32_t ___sqrMinPixelMove_14;
	// System.Boolean DrawLinesTouch::canDraw
	bool ___canDraw_15;
	// UnityEngine.Touch DrawLinesTouch::touch
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___touch_16;

public:
	inline static int32_t get_offset_of_lineTex_4() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___lineTex_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lineTex_4() const { return ___lineTex_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lineTex_4() { return &___lineTex_4; }
	inline void set_lineTex_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lineTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex_4), value);
	}

	inline static int32_t get_offset_of_maxPoints_5() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___maxPoints_5)); }
	inline int32_t get_maxPoints_5() const { return ___maxPoints_5; }
	inline int32_t* get_address_of_maxPoints_5() { return &___maxPoints_5; }
	inline void set_maxPoints_5(int32_t value)
	{
		___maxPoints_5 = value;
	}

	inline static int32_t get_offset_of_lineWidth_6() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___lineWidth_6)); }
	inline float get_lineWidth_6() const { return ___lineWidth_6; }
	inline float* get_address_of_lineWidth_6() { return &___lineWidth_6; }
	inline void set_lineWidth_6(float value)
	{
		___lineWidth_6 = value;
	}

	inline static int32_t get_offset_of_minPixelMove_7() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___minPixelMove_7)); }
	inline int32_t get_minPixelMove_7() const { return ___minPixelMove_7; }
	inline int32_t* get_address_of_minPixelMove_7() { return &___minPixelMove_7; }
	inline void set_minPixelMove_7(int32_t value)
	{
		___minPixelMove_7 = value;
	}

	inline static int32_t get_offset_of_useEndCap_8() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___useEndCap_8)); }
	inline bool get_useEndCap_8() const { return ___useEndCap_8; }
	inline bool* get_address_of_useEndCap_8() { return &___useEndCap_8; }
	inline void set_useEndCap_8(bool value)
	{
		___useEndCap_8 = value;
	}

	inline static int32_t get_offset_of_capLineTex_9() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___capLineTex_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_capLineTex_9() const { return ___capLineTex_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_capLineTex_9() { return &___capLineTex_9; }
	inline void set_capLineTex_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___capLineTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___capLineTex_9), value);
	}

	inline static int32_t get_offset_of_capTex_10() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___capTex_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_capTex_10() const { return ___capTex_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_capTex_10() { return &___capTex_10; }
	inline void set_capTex_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___capTex_10 = value;
		Il2CppCodeGenWriteBarrier((&___capTex_10), value);
	}

	inline static int32_t get_offset_of_capLineWidth_11() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___capLineWidth_11)); }
	inline float get_capLineWidth_11() const { return ___capLineWidth_11; }
	inline float* get_address_of_capLineWidth_11() { return &___capLineWidth_11; }
	inline void set_capLineWidth_11(float value)
	{
		___capLineWidth_11 = value;
	}

	inline static int32_t get_offset_of_line_12() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___line_12)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_12() const { return ___line_12; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_12() { return &___line_12; }
	inline void set_line_12(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_12 = value;
		Il2CppCodeGenWriteBarrier((&___line_12), value);
	}

	inline static int32_t get_offset_of_previousPosition_13() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___previousPosition_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_previousPosition_13() const { return ___previousPosition_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_previousPosition_13() { return &___previousPosition_13; }
	inline void set_previousPosition_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___previousPosition_13 = value;
	}

	inline static int32_t get_offset_of_sqrMinPixelMove_14() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___sqrMinPixelMove_14)); }
	inline int32_t get_sqrMinPixelMove_14() const { return ___sqrMinPixelMove_14; }
	inline int32_t* get_address_of_sqrMinPixelMove_14() { return &___sqrMinPixelMove_14; }
	inline void set_sqrMinPixelMove_14(int32_t value)
	{
		___sqrMinPixelMove_14 = value;
	}

	inline static int32_t get_offset_of_canDraw_15() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___canDraw_15)); }
	inline bool get_canDraw_15() const { return ___canDraw_15; }
	inline bool* get_address_of_canDraw_15() { return &___canDraw_15; }
	inline void set_canDraw_15(bool value)
	{
		___canDraw_15 = value;
	}

	inline static int32_t get_offset_of_touch_16() { return static_cast<int32_t>(offsetof(DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233, ___touch_16)); }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003  get_touch_16() const { return ___touch_16; }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003 * get_address_of_touch_16() { return &___touch_16; }
	inline void set_touch_16(Touch_t806752C775BA713A91B6588A07CA98417CABC003  value)
	{
		___touch_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWLINESTOUCH_TCB035475C9054C2816A36335F3BAF562DB888233_H
#ifndef DRAWPATH_T5378184F158682D121E87B61DFAAA57A2A178519_H
#define DRAWPATH_T5378184F158682D121E87B61DFAAA57A2A178519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawPath
struct  DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture DrawPath::lineTex
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTex_4;
	// UnityEngine.Color DrawPath::lineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lineColor_5;
	// System.Int32 DrawPath::maxPoints
	int32_t ___maxPoints_6;
	// System.Boolean DrawPath::continuousUpdate
	bool ___continuousUpdate_7;
	// UnityEngine.GameObject DrawPath::ballPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ballPrefab_8;
	// System.Single DrawPath::force
	float ___force_9;
	// Vectrosity.VectorLine DrawPath::pathLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___pathLine_10;
	// System.Int32 DrawPath::pathIndex
	int32_t ___pathIndex_11;
	// UnityEngine.GameObject DrawPath::ball
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ball_12;

public:
	inline static int32_t get_offset_of_lineTex_4() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___lineTex_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTex_4() const { return ___lineTex_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTex_4() { return &___lineTex_4; }
	inline void set_lineTex_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex_4), value);
	}

	inline static int32_t get_offset_of_lineColor_5() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___lineColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lineColor_5() const { return ___lineColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lineColor_5() { return &___lineColor_5; }
	inline void set_lineColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lineColor_5 = value;
	}

	inline static int32_t get_offset_of_maxPoints_6() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___maxPoints_6)); }
	inline int32_t get_maxPoints_6() const { return ___maxPoints_6; }
	inline int32_t* get_address_of_maxPoints_6() { return &___maxPoints_6; }
	inline void set_maxPoints_6(int32_t value)
	{
		___maxPoints_6 = value;
	}

	inline static int32_t get_offset_of_continuousUpdate_7() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___continuousUpdate_7)); }
	inline bool get_continuousUpdate_7() const { return ___continuousUpdate_7; }
	inline bool* get_address_of_continuousUpdate_7() { return &___continuousUpdate_7; }
	inline void set_continuousUpdate_7(bool value)
	{
		___continuousUpdate_7 = value;
	}

	inline static int32_t get_offset_of_ballPrefab_8() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___ballPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ballPrefab_8() const { return ___ballPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ballPrefab_8() { return &___ballPrefab_8; }
	inline void set_ballPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ballPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_8), value);
	}

	inline static int32_t get_offset_of_force_9() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___force_9)); }
	inline float get_force_9() const { return ___force_9; }
	inline float* get_address_of_force_9() { return &___force_9; }
	inline void set_force_9(float value)
	{
		___force_9 = value;
	}

	inline static int32_t get_offset_of_pathLine_10() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___pathLine_10)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_pathLine_10() const { return ___pathLine_10; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_pathLine_10() { return &___pathLine_10; }
	inline void set_pathLine_10(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___pathLine_10 = value;
		Il2CppCodeGenWriteBarrier((&___pathLine_10), value);
	}

	inline static int32_t get_offset_of_pathIndex_11() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___pathIndex_11)); }
	inline int32_t get_pathIndex_11() const { return ___pathIndex_11; }
	inline int32_t* get_address_of_pathIndex_11() { return &___pathIndex_11; }
	inline void set_pathIndex_11(int32_t value)
	{
		___pathIndex_11 = value;
	}

	inline static int32_t get_offset_of_ball_12() { return static_cast<int32_t>(offsetof(DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519, ___ball_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ball_12() const { return ___ball_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ball_12() { return &___ball_12; }
	inline void set_ball_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ball_12 = value;
		Il2CppCodeGenWriteBarrier((&___ball_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWPATH_T5378184F158682D121E87B61DFAAA57A2A178519_H
#ifndef DRAWPOINTS_TF5EE7B35E360642A7C4D1B56E0DBB59B1A945378_H
#define DRAWPOINTS_TF5EE7B35E360642A7C4D1B56E0DBB59B1A945378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawPoints
struct  DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DrawPoints::dotSize
	float ___dotSize_4;
	// System.Int32 DrawPoints::numberOfDots
	int32_t ___numberOfDots_5;
	// System.Int32 DrawPoints::numberOfRings
	int32_t ___numberOfRings_6;
	// UnityEngine.Color DrawPoints::dotColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___dotColor_7;

public:
	inline static int32_t get_offset_of_dotSize_4() { return static_cast<int32_t>(offsetof(DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378, ___dotSize_4)); }
	inline float get_dotSize_4() const { return ___dotSize_4; }
	inline float* get_address_of_dotSize_4() { return &___dotSize_4; }
	inline void set_dotSize_4(float value)
	{
		___dotSize_4 = value;
	}

	inline static int32_t get_offset_of_numberOfDots_5() { return static_cast<int32_t>(offsetof(DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378, ___numberOfDots_5)); }
	inline int32_t get_numberOfDots_5() const { return ___numberOfDots_5; }
	inline int32_t* get_address_of_numberOfDots_5() { return &___numberOfDots_5; }
	inline void set_numberOfDots_5(int32_t value)
	{
		___numberOfDots_5 = value;
	}

	inline static int32_t get_offset_of_numberOfRings_6() { return static_cast<int32_t>(offsetof(DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378, ___numberOfRings_6)); }
	inline int32_t get_numberOfRings_6() const { return ___numberOfRings_6; }
	inline int32_t* get_address_of_numberOfRings_6() { return &___numberOfRings_6; }
	inline void set_numberOfRings_6(int32_t value)
	{
		___numberOfRings_6 = value;
	}

	inline static int32_t get_offset_of_dotColor_7() { return static_cast<int32_t>(offsetof(DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378, ___dotColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_dotColor_7() const { return ___dotColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_dotColor_7() { return &___dotColor_7; }
	inline void set_dotColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___dotColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWPOINTS_TF5EE7B35E360642A7C4D1B56E0DBB59B1A945378_H
#ifndef DRAWRADIUS_T10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6_H
#define DRAWRADIUS_T10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawRadius
struct  DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DrawRadius::radius
	float ___radius_4;
	// System.Int32 DrawRadius::numSegments
	int32_t ___numSegments_5;
	// UnityEngine.LineRenderer DrawRadius::radiusLR
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___radiusLR_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_numSegments_5() { return static_cast<int32_t>(offsetof(DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6, ___numSegments_5)); }
	inline int32_t get_numSegments_5() const { return ___numSegments_5; }
	inline int32_t* get_address_of_numSegments_5() { return &___numSegments_5; }
	inline void set_numSegments_5(int32_t value)
	{
		___numSegments_5 = value;
	}

	inline static int32_t get_offset_of_radiusLR_6() { return static_cast<int32_t>(offsetof(DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6, ___radiusLR_6)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_radiusLR_6() const { return ___radiusLR_6; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_radiusLR_6() { return &___radiusLR_6; }
	inline void set_radiusLR_6(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___radiusLR_6 = value;
		Il2CppCodeGenWriteBarrier((&___radiusLR_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWRADIUS_T10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6_H
#ifndef ELLIPSE1_TAA67603AB076BAC54B1F84144CDD4BDB9860AB6B_H
#define ELLIPSE1_TAA67603AB076BAC54B1F84144CDD4BDB9860AB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ellipse1
struct  Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture Ellipse1::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Single Ellipse1::xRadius
	float ___xRadius_5;
	// System.Single Ellipse1::yRadius
	float ___yRadius_6;
	// System.Int32 Ellipse1::segments
	int32_t ___segments_7;
	// System.Single Ellipse1::pointRotation
	float ___pointRotation_8;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_xRadius_5() { return static_cast<int32_t>(offsetof(Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B, ___xRadius_5)); }
	inline float get_xRadius_5() const { return ___xRadius_5; }
	inline float* get_address_of_xRadius_5() { return &___xRadius_5; }
	inline void set_xRadius_5(float value)
	{
		___xRadius_5 = value;
	}

	inline static int32_t get_offset_of_yRadius_6() { return static_cast<int32_t>(offsetof(Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B, ___yRadius_6)); }
	inline float get_yRadius_6() const { return ___yRadius_6; }
	inline float* get_address_of_yRadius_6() { return &___yRadius_6; }
	inline void set_yRadius_6(float value)
	{
		___yRadius_6 = value;
	}

	inline static int32_t get_offset_of_segments_7() { return static_cast<int32_t>(offsetof(Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B, ___segments_7)); }
	inline int32_t get_segments_7() const { return ___segments_7; }
	inline int32_t* get_address_of_segments_7() { return &___segments_7; }
	inline void set_segments_7(int32_t value)
	{
		___segments_7 = value;
	}

	inline static int32_t get_offset_of_pointRotation_8() { return static_cast<int32_t>(offsetof(Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B, ___pointRotation_8)); }
	inline float get_pointRotation_8() const { return ___pointRotation_8; }
	inline float* get_address_of_pointRotation_8() { return &___pointRotation_8; }
	inline void set_pointRotation_8(float value)
	{
		___pointRotation_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSE1_TAA67603AB076BAC54B1F84144CDD4BDB9860AB6B_H
#ifndef ELLIPSE2_TD409B378EB9205F18850D2180327013ACFC97DCB_H
#define ELLIPSE2_TD409B378EB9205F18850D2180327013ACFC97DCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ellipse2
struct  Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture Ellipse2::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Int32 Ellipse2::segments
	int32_t ___segments_5;
	// System.Int32 Ellipse2::numberOfEllipses
	int32_t ___numberOfEllipses_6;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_segments_5() { return static_cast<int32_t>(offsetof(Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB, ___segments_5)); }
	inline int32_t get_segments_5() const { return ___segments_5; }
	inline int32_t* get_address_of_segments_5() { return &___segments_5; }
	inline void set_segments_5(int32_t value)
	{
		___segments_5 = value;
	}

	inline static int32_t get_offset_of_numberOfEllipses_6() { return static_cast<int32_t>(offsetof(Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB, ___numberOfEllipses_6)); }
	inline int32_t get_numberOfEllipses_6() const { return ___numberOfEllipses_6; }
	inline int32_t* get_address_of_numberOfEllipses_6() { return &___numberOfEllipses_6; }
	inline void set_numberOfEllipses_6(int32_t value)
	{
		___numberOfEllipses_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSE2_TD409B378EB9205F18850D2180327013ACFC97DCB_H
#ifndef ENDCAPDEMO_T24A2021892C1CE6F6D1E84A577B02DEBF9E7960A_H
#define ENDCAPDEMO_T24A2021892C1CE6F6D1E84A577B02DEBF9E7960A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EndCapDemo
struct  EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D EndCapDemo::lineTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lineTex_4;
	// UnityEngine.Texture2D EndCapDemo::lineTex2
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lineTex2_5;
	// UnityEngine.Texture2D EndCapDemo::lineTex3
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lineTex3_6;
	// UnityEngine.Texture2D EndCapDemo::frontTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___frontTex_7;
	// UnityEngine.Texture2D EndCapDemo::backTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___backTex_8;
	// UnityEngine.Texture2D EndCapDemo::capTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___capTex_9;

public:
	inline static int32_t get_offset_of_lineTex_4() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___lineTex_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lineTex_4() const { return ___lineTex_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lineTex_4() { return &___lineTex_4; }
	inline void set_lineTex_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lineTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex_4), value);
	}

	inline static int32_t get_offset_of_lineTex2_5() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___lineTex2_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lineTex2_5() const { return ___lineTex2_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lineTex2_5() { return &___lineTex2_5; }
	inline void set_lineTex2_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lineTex2_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex2_5), value);
	}

	inline static int32_t get_offset_of_lineTex3_6() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___lineTex3_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lineTex3_6() const { return ___lineTex3_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lineTex3_6() { return &___lineTex3_6; }
	inline void set_lineTex3_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lineTex3_6 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex3_6), value);
	}

	inline static int32_t get_offset_of_frontTex_7() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___frontTex_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_frontTex_7() const { return ___frontTex_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_frontTex_7() { return &___frontTex_7; }
	inline void set_frontTex_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___frontTex_7 = value;
		Il2CppCodeGenWriteBarrier((&___frontTex_7), value);
	}

	inline static int32_t get_offset_of_backTex_8() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___backTex_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_backTex_8() const { return ___backTex_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_backTex_8() { return &___backTex_8; }
	inline void set_backTex_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___backTex_8 = value;
		Il2CppCodeGenWriteBarrier((&___backTex_8), value);
	}

	inline static int32_t get_offset_of_capTex_9() { return static_cast<int32_t>(offsetof(EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A, ___capTex_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_capTex_9() const { return ___capTex_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_capTex_9() { return &___capTex_9; }
	inline void set_capTex_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___capTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___capTex_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDCAPDEMO_T24A2021892C1CE6F6D1E84A577B02DEBF9E7960A_H
#ifndef FINDLOWESTMESHPOINT_TAD11DF74019C431737B19B26512B89D9D4B5069F_H
#define FINDLOWESTMESHPOINT_TAD11DF74019C431737B19B26512B89D9D4B5069F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FindLowestMeshPoint
struct  FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CustomCollider FindLowestMeshPoint::customCollider
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * ___customCollider_4;
	// CourseManager FindLowestMeshPoint::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_5;
	// UnityEngine.GameObject FindLowestMeshPoint::colliderPointPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___colliderPointPrefab_6;

public:
	inline static int32_t get_offset_of_customCollider_4() { return static_cast<int32_t>(offsetof(FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F, ___customCollider_4)); }
	inline CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * get_customCollider_4() const { return ___customCollider_4; }
	inline CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E ** get_address_of_customCollider_4() { return &___customCollider_4; }
	inline void set_customCollider_4(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * value)
	{
		___customCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___customCollider_4), value);
	}

	inline static int32_t get_offset_of_courseManager_5() { return static_cast<int32_t>(offsetof(FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F, ___courseManager_5)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_5() const { return ___courseManager_5; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_5() { return &___courseManager_5; }
	inline void set_courseManager_5(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_5), value);
	}

	inline static int32_t get_offset_of_colliderPointPrefab_6() { return static_cast<int32_t>(offsetof(FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F, ___colliderPointPrefab_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_colliderPointPrefab_6() const { return ___colliderPointPrefab_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_colliderPointPrefab_6() { return &___colliderPointPrefab_6; }
	inline void set_colliderPointPrefab_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___colliderPointPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPointPrefab_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDLOWESTMESHPOINT_TAD11DF74019C431737B19B26512B89D9D4B5069F_H
#ifndef GAMEVIEWMANAGER_T1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_H
#define GAMEVIEWMANAGER_T1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameViewManager
struct  GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_StaticFields
{
public:
	// GameViewManager GameViewManager::_instance
	GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_StaticFields, ____instance_4)); }
	inline GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C * get__instance_4() const { return ____instance_4; }
	inline GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEVIEWMANAGER_T1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_H
#ifndef GREENDXFDELETE_T7BB0794D0F2BF22361A781BC47AEE07DEF6C60A7_H
#define GREENDXFDELETE_T7BB0794D0F2BF22361A781BC47AEE07DEF6C60A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreenDXFDelete
struct  GreenDXFDelete_t7BB0794D0F2BF22361A781BC47AEE07DEF6C60A7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENDXFDELETE_T7BB0794D0F2BF22361A781BC47AEE07DEF6C60A7_H
#ifndef GREENMOVEMENTNAVMANAGER_T8F23F81C48F7568669F230E8E06A872C5F4F6AF7_H
#define GREENMOVEMENTNAVMANAGER_T8F23F81C48F7568669F230E8E06A872C5F4F6AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreenMovementNavManager
struct  GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera GreenMovementNavManager::cameraGreen
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cameraGreen_4;
	// Doozy.Engine.UI.UIButton GreenMovementNavManager::greenLockBtn
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___greenLockBtn_5;
	// UnityEngine.Sprite GreenMovementNavManager::lockSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___lockSprite_6;
	// UnityEngine.Sprite GreenMovementNavManager::unlockSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___unlockSprite_7;
	// UnityEngine.GameObject GreenMovementNavManager::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_8;
	// CameraManager GreenMovementNavManager::cameraManager
	CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * ___cameraManager_9;
	// UnityEngine.GameObject GreenMovementNavManager::greenMovementNav
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenMovementNav_10;

public:
	inline static int32_t get_offset_of_cameraGreen_4() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___cameraGreen_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cameraGreen_4() const { return ___cameraGreen_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cameraGreen_4() { return &___cameraGreen_4; }
	inline void set_cameraGreen_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cameraGreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraGreen_4), value);
	}

	inline static int32_t get_offset_of_greenLockBtn_5() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___greenLockBtn_5)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_greenLockBtn_5() const { return ___greenLockBtn_5; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_greenLockBtn_5() { return &___greenLockBtn_5; }
	inline void set_greenLockBtn_5(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___greenLockBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___greenLockBtn_5), value);
	}

	inline static int32_t get_offset_of_lockSprite_6() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___lockSprite_6)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_lockSprite_6() const { return ___lockSprite_6; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_lockSprite_6() { return &___lockSprite_6; }
	inline void set_lockSprite_6(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___lockSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___lockSprite_6), value);
	}

	inline static int32_t get_offset_of_unlockSprite_7() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___unlockSprite_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_unlockSprite_7() const { return ___unlockSprite_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_unlockSprite_7() { return &___unlockSprite_7; }
	inline void set_unlockSprite_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___unlockSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___unlockSprite_7), value);
	}

	inline static int32_t get_offset_of_holeContainer_8() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___holeContainer_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_8() const { return ___holeContainer_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_8() { return &___holeContainer_8; }
	inline void set_holeContainer_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_8), value);
	}

	inline static int32_t get_offset_of_cameraManager_9() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___cameraManager_9)); }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * get_cameraManager_9() const { return ___cameraManager_9; }
	inline CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 ** get_address_of_cameraManager_9() { return &___cameraManager_9; }
	inline void set_cameraManager_9(CameraManager_tFE8FC1605CFE365D5018DFEDB04FBDA3B7E3F085 * value)
	{
		___cameraManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_9), value);
	}

	inline static int32_t get_offset_of_greenMovementNav_10() { return static_cast<int32_t>(offsetof(GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7, ___greenMovementNav_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenMovementNav_10() const { return ___greenMovementNav_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenMovementNav_10() { return &___greenMovementNav_10; }
	inline void set_greenMovementNav_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenMovementNav_10 = value;
		Il2CppCodeGenWriteBarrier((&___greenMovementNav_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENMOVEMENTNAVMANAGER_T8F23F81C48F7568669F230E8E06A872C5F4F6AF7_H
#ifndef GREENVIEWCONTROLLER_T7AE2514749D4E81DC71D9D5B8B0012D8C6DF5F5F_H
#define GREENVIEWCONTROLLER_T7AE2514749D4E81DC71D9D5B8B0012D8C6DF5F5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreenViewController
struct  GreenViewController_t7AE2514749D4E81DC71D9D5B8B0012D8C6DF5F5F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENVIEWCONTROLLER_T7AE2514749D4E81DC71D9D5B8B0012D8C6DF5F5F_H
#ifndef GRID3D_T05BB99665B664C9D29CB95227BEF6F439918EBE3_H
#define GRID3D_T05BB99665B664C9D29CB95227BEF6F439918EBE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grid3D
struct  Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Grid3D::numberOfLines
	int32_t ___numberOfLines_4;
	// System.Single Grid3D::distanceBetweenLines
	float ___distanceBetweenLines_5;
	// System.Single Grid3D::moveSpeed
	float ___moveSpeed_6;
	// System.Single Grid3D::rotateSpeed
	float ___rotateSpeed_7;
	// System.Single Grid3D::lineWidth
	float ___lineWidth_8;

public:
	inline static int32_t get_offset_of_numberOfLines_4() { return static_cast<int32_t>(offsetof(Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3, ___numberOfLines_4)); }
	inline int32_t get_numberOfLines_4() const { return ___numberOfLines_4; }
	inline int32_t* get_address_of_numberOfLines_4() { return &___numberOfLines_4; }
	inline void set_numberOfLines_4(int32_t value)
	{
		___numberOfLines_4 = value;
	}

	inline static int32_t get_offset_of_distanceBetweenLines_5() { return static_cast<int32_t>(offsetof(Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3, ___distanceBetweenLines_5)); }
	inline float get_distanceBetweenLines_5() const { return ___distanceBetweenLines_5; }
	inline float* get_address_of_distanceBetweenLines_5() { return &___distanceBetweenLines_5; }
	inline void set_distanceBetweenLines_5(float value)
	{
		___distanceBetweenLines_5 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_7() { return static_cast<int32_t>(offsetof(Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3, ___rotateSpeed_7)); }
	inline float get_rotateSpeed_7() const { return ___rotateSpeed_7; }
	inline float* get_address_of_rotateSpeed_7() { return &___rotateSpeed_7; }
	inline void set_rotateSpeed_7(float value)
	{
		___rotateSpeed_7 = value;
	}

	inline static int32_t get_offset_of_lineWidth_8() { return static_cast<int32_t>(offsetof(Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3, ___lineWidth_8)); }
	inline float get_lineWidth_8() const { return ___lineWidth_8; }
	inline float* get_address_of_lineWidth_8() { return &___lineWidth_8; }
	inline void set_lineWidth_8(float value)
	{
		___lineWidth_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRID3D_T05BB99665B664C9D29CB95227BEF6F439918EBE3_H
#ifndef HIGHLIGHT_T4C4D1CB930E1E218D59EE144C1E23955EBF63BDD_H
#define HIGHLIGHT_T4C4D1CB930E1E218D59EE144C1E23955EBF63BDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Highlight
struct  Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Highlight::lineWidth
	int32_t ___lineWidth_4;
	// System.Int32 Highlight::energyLineWidth
	int32_t ___energyLineWidth_5;
	// System.Single Highlight::selectionSize
	float ___selectionSize_6;
	// System.Single Highlight::force
	float ___force_7;
	// System.Int32 Highlight::pointsInEnergyLine
	int32_t ___pointsInEnergyLine_8;
	// Vectrosity.VectorLine Highlight::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_9;
	// Vectrosity.VectorLine Highlight::energyLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___energyLine_10;
	// UnityEngine.RaycastHit Highlight::hit
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___hit_11;
	// System.Int32 Highlight::selectIndex
	int32_t ___selectIndex_12;
	// System.Single Highlight::energyLevel
	float ___energyLevel_13;
	// System.Boolean Highlight::canClick
	bool ___canClick_14;
	// UnityEngine.GameObject[] Highlight::spheres
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___spheres_15;
	// System.Double Highlight::timer
	double ___timer_16;
	// System.Int32 Highlight::ignoreLayer
	int32_t ___ignoreLayer_17;
	// System.Int32 Highlight::defaultLayer
	int32_t ___defaultLayer_18;
	// System.Boolean Highlight::fading
	bool ___fading_19;

public:
	inline static int32_t get_offset_of_lineWidth_4() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___lineWidth_4)); }
	inline int32_t get_lineWidth_4() const { return ___lineWidth_4; }
	inline int32_t* get_address_of_lineWidth_4() { return &___lineWidth_4; }
	inline void set_lineWidth_4(int32_t value)
	{
		___lineWidth_4 = value;
	}

	inline static int32_t get_offset_of_energyLineWidth_5() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___energyLineWidth_5)); }
	inline int32_t get_energyLineWidth_5() const { return ___energyLineWidth_5; }
	inline int32_t* get_address_of_energyLineWidth_5() { return &___energyLineWidth_5; }
	inline void set_energyLineWidth_5(int32_t value)
	{
		___energyLineWidth_5 = value;
	}

	inline static int32_t get_offset_of_selectionSize_6() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___selectionSize_6)); }
	inline float get_selectionSize_6() const { return ___selectionSize_6; }
	inline float* get_address_of_selectionSize_6() { return &___selectionSize_6; }
	inline void set_selectionSize_6(float value)
	{
		___selectionSize_6 = value;
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___force_7)); }
	inline float get_force_7() const { return ___force_7; }
	inline float* get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(float value)
	{
		___force_7 = value;
	}

	inline static int32_t get_offset_of_pointsInEnergyLine_8() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___pointsInEnergyLine_8)); }
	inline int32_t get_pointsInEnergyLine_8() const { return ___pointsInEnergyLine_8; }
	inline int32_t* get_address_of_pointsInEnergyLine_8() { return &___pointsInEnergyLine_8; }
	inline void set_pointsInEnergyLine_8(int32_t value)
	{
		___pointsInEnergyLine_8 = value;
	}

	inline static int32_t get_offset_of_line_9() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___line_9)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_9() const { return ___line_9; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_9() { return &___line_9; }
	inline void set_line_9(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_9 = value;
		Il2CppCodeGenWriteBarrier((&___line_9), value);
	}

	inline static int32_t get_offset_of_energyLine_10() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___energyLine_10)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_energyLine_10() const { return ___energyLine_10; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_energyLine_10() { return &___energyLine_10; }
	inline void set_energyLine_10(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___energyLine_10 = value;
		Il2CppCodeGenWriteBarrier((&___energyLine_10), value);
	}

	inline static int32_t get_offset_of_hit_11() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___hit_11)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_hit_11() const { return ___hit_11; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_hit_11() { return &___hit_11; }
	inline void set_hit_11(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___hit_11 = value;
	}

	inline static int32_t get_offset_of_selectIndex_12() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___selectIndex_12)); }
	inline int32_t get_selectIndex_12() const { return ___selectIndex_12; }
	inline int32_t* get_address_of_selectIndex_12() { return &___selectIndex_12; }
	inline void set_selectIndex_12(int32_t value)
	{
		___selectIndex_12 = value;
	}

	inline static int32_t get_offset_of_energyLevel_13() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___energyLevel_13)); }
	inline float get_energyLevel_13() const { return ___energyLevel_13; }
	inline float* get_address_of_energyLevel_13() { return &___energyLevel_13; }
	inline void set_energyLevel_13(float value)
	{
		___energyLevel_13 = value;
	}

	inline static int32_t get_offset_of_canClick_14() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___canClick_14)); }
	inline bool get_canClick_14() const { return ___canClick_14; }
	inline bool* get_address_of_canClick_14() { return &___canClick_14; }
	inline void set_canClick_14(bool value)
	{
		___canClick_14 = value;
	}

	inline static int32_t get_offset_of_spheres_15() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___spheres_15)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_spheres_15() const { return ___spheres_15; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_spheres_15() { return &___spheres_15; }
	inline void set_spheres_15(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___spheres_15 = value;
		Il2CppCodeGenWriteBarrier((&___spheres_15), value);
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___timer_16)); }
	inline double get_timer_16() const { return ___timer_16; }
	inline double* get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(double value)
	{
		___timer_16 = value;
	}

	inline static int32_t get_offset_of_ignoreLayer_17() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___ignoreLayer_17)); }
	inline int32_t get_ignoreLayer_17() const { return ___ignoreLayer_17; }
	inline int32_t* get_address_of_ignoreLayer_17() { return &___ignoreLayer_17; }
	inline void set_ignoreLayer_17(int32_t value)
	{
		___ignoreLayer_17 = value;
	}

	inline static int32_t get_offset_of_defaultLayer_18() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___defaultLayer_18)); }
	inline int32_t get_defaultLayer_18() const { return ___defaultLayer_18; }
	inline int32_t* get_address_of_defaultLayer_18() { return &___defaultLayer_18; }
	inline void set_defaultLayer_18(int32_t value)
	{
		___defaultLayer_18 = value;
	}

	inline static int32_t get_offset_of_fading_19() { return static_cast<int32_t>(offsetof(Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD, ___fading_19)); }
	inline bool get_fading_19() const { return ___fading_19; }
	inline bool* get_address_of_fading_19() { return &___fading_19; }
	inline void set_fading_19(bool value)
	{
		___fading_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHT_T4C4D1CB930E1E218D59EE144C1E23955EBF63BDD_H
#ifndef HOLECONTROLLER_T91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0_H
#define HOLECONTROLLER_T91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoleController
struct  HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.BoxCollider HoleController::holeDragBoxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___holeDragBoxCollider_4;
	// UnityEngine.GameObject HoleController::rotateRectDragObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotateRectDragObject_5;

public:
	inline static int32_t get_offset_of_holeDragBoxCollider_4() { return static_cast<int32_t>(offsetof(HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0, ___holeDragBoxCollider_4)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_holeDragBoxCollider_4() const { return ___holeDragBoxCollider_4; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_holeDragBoxCollider_4() { return &___holeDragBoxCollider_4; }
	inline void set_holeDragBoxCollider_4(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___holeDragBoxCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___holeDragBoxCollider_4), value);
	}

	inline static int32_t get_offset_of_rotateRectDragObject_5() { return static_cast<int32_t>(offsetof(HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0, ___rotateRectDragObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotateRectDragObject_5() const { return ___rotateRectDragObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotateRectDragObject_5() { return &___rotateRectDragObject_5; }
	inline void set_rotateRectDragObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotateRectDragObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotateRectDragObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLECONTROLLER_T91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0_H
#ifndef HOLERECTCONTROLLER_T3D13E7A35AA9F3957DF511142E4289D151246EA6_H
#define HOLERECTCONTROLLER_T3D13E7A35AA9F3957DF511142E4289D151246EA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoleRectController
struct  HoleRectController_t3D13E7A35AA9F3957DF511142E4289D151246EA6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLERECTCONTROLLER_T3D13E7A35AA9F3957DF511142E4289D151246EA6_H
#ifndef JACKCONTROLLER_T5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_H
#define JACKCONTROLLER_T5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JackController
struct  JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single JackController::elevation
	float ___elevation_5;
	// System.Int32 JackController::jackIndex
	int32_t ___jackIndex_6;
	// UnityEngine.GameObject JackController::hitPointPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hitPointPrefab_7;
	// UnityEngine.Collider JackController::coll
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___coll_8;
	// UnityEngine.GameObject JackController::jackHitPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___jackHitPoint_9;
	// UnityEngine.GameObject JackController::greenLoaded
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenLoaded_10;
	// CourseManager JackController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_11;
	// System.Boolean JackController::initialized
	bool ___initialized_12;

public:
	inline static int32_t get_offset_of_elevation_5() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___elevation_5)); }
	inline float get_elevation_5() const { return ___elevation_5; }
	inline float* get_address_of_elevation_5() { return &___elevation_5; }
	inline void set_elevation_5(float value)
	{
		___elevation_5 = value;
	}

	inline static int32_t get_offset_of_jackIndex_6() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___jackIndex_6)); }
	inline int32_t get_jackIndex_6() const { return ___jackIndex_6; }
	inline int32_t* get_address_of_jackIndex_6() { return &___jackIndex_6; }
	inline void set_jackIndex_6(int32_t value)
	{
		___jackIndex_6 = value;
	}

	inline static int32_t get_offset_of_hitPointPrefab_7() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___hitPointPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hitPointPrefab_7() const { return ___hitPointPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hitPointPrefab_7() { return &___hitPointPrefab_7; }
	inline void set_hitPointPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hitPointPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___hitPointPrefab_7), value);
	}

	inline static int32_t get_offset_of_coll_8() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___coll_8)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_coll_8() const { return ___coll_8; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_coll_8() { return &___coll_8; }
	inline void set_coll_8(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___coll_8 = value;
		Il2CppCodeGenWriteBarrier((&___coll_8), value);
	}

	inline static int32_t get_offset_of_jackHitPoint_9() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___jackHitPoint_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_jackHitPoint_9() const { return ___jackHitPoint_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_jackHitPoint_9() { return &___jackHitPoint_9; }
	inline void set_jackHitPoint_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___jackHitPoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___jackHitPoint_9), value);
	}

	inline static int32_t get_offset_of_greenLoaded_10() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___greenLoaded_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenLoaded_10() const { return ___greenLoaded_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenLoaded_10() { return &___greenLoaded_10; }
	inline void set_greenLoaded_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenLoaded_10 = value;
		Il2CppCodeGenWriteBarrier((&___greenLoaded_10), value);
	}

	inline static int32_t get_offset_of_courseManager_11() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___courseManager_11)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_11() const { return ___courseManager_11; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_11() { return &___courseManager_11; }
	inline void set_courseManager_11(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_11), value);
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}
};

struct JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_StaticFields
{
public:
	// System.Action JackController::JackInit
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___JackInit_4;

public:
	inline static int32_t get_offset_of_JackInit_4() { return static_cast<int32_t>(offsetof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_StaticFields, ___JackInit_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_JackInit_4() const { return ___JackInit_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_JackInit_4() { return &___JackInit_4; }
	inline void set_JackInit_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___JackInit_4 = value;
		Il2CppCodeGenWriteBarrier((&___JackInit_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACKCONTROLLER_T5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_H
#ifndef JACKMANAGER_TDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_H
#define JACKMANAGER_TDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JackManager
struct  JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 JackManager::rows
	int32_t ___rows_7;
	// System.Int32 JackManager::columns
	int32_t ___columns_8;
	// UnityEngine.GameObject JackManager::jackPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___jackPrefab_9;
	// UnityEngine.GameObject JackManager::jackContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___jackContainer_10;
	// UnityEngine.GameObject JackManager::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_11;
	// System.Collections.Generic.List`1<JackController> JackManager::jackList
	List_1_tCB910459F316FBBEEBD292307AE8406532EB60BE * ___jackList_12;
	// CustomCollider JackManager::customCollider
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * ___customCollider_13;
	// System.Int32 JackManager::initializedJacks
	int32_t ___initializedJacks_14;

public:
	inline static int32_t get_offset_of_rows_7() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___rows_7)); }
	inline int32_t get_rows_7() const { return ___rows_7; }
	inline int32_t* get_address_of_rows_7() { return &___rows_7; }
	inline void set_rows_7(int32_t value)
	{
		___rows_7 = value;
	}

	inline static int32_t get_offset_of_columns_8() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___columns_8)); }
	inline int32_t get_columns_8() const { return ___columns_8; }
	inline int32_t* get_address_of_columns_8() { return &___columns_8; }
	inline void set_columns_8(int32_t value)
	{
		___columns_8 = value;
	}

	inline static int32_t get_offset_of_jackPrefab_9() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___jackPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_jackPrefab_9() const { return ___jackPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_jackPrefab_9() { return &___jackPrefab_9; }
	inline void set_jackPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___jackPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___jackPrefab_9), value);
	}

	inline static int32_t get_offset_of_jackContainer_10() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___jackContainer_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_jackContainer_10() const { return ___jackContainer_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_jackContainer_10() { return &___jackContainer_10; }
	inline void set_jackContainer_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___jackContainer_10 = value;
		Il2CppCodeGenWriteBarrier((&___jackContainer_10), value);
	}

	inline static int32_t get_offset_of_holeContainer_11() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___holeContainer_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_11() const { return ___holeContainer_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_11() { return &___holeContainer_11; }
	inline void set_holeContainer_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_11 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_11), value);
	}

	inline static int32_t get_offset_of_jackList_12() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___jackList_12)); }
	inline List_1_tCB910459F316FBBEEBD292307AE8406532EB60BE * get_jackList_12() const { return ___jackList_12; }
	inline List_1_tCB910459F316FBBEEBD292307AE8406532EB60BE ** get_address_of_jackList_12() { return &___jackList_12; }
	inline void set_jackList_12(List_1_tCB910459F316FBBEEBD292307AE8406532EB60BE * value)
	{
		___jackList_12 = value;
		Il2CppCodeGenWriteBarrier((&___jackList_12), value);
	}

	inline static int32_t get_offset_of_customCollider_13() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___customCollider_13)); }
	inline CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * get_customCollider_13() const { return ___customCollider_13; }
	inline CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E ** get_address_of_customCollider_13() { return &___customCollider_13; }
	inline void set_customCollider_13(CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E * value)
	{
		___customCollider_13 = value;
		Il2CppCodeGenWriteBarrier((&___customCollider_13), value);
	}

	inline static int32_t get_offset_of_initializedJacks_14() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F, ___initializedJacks_14)); }
	inline int32_t get_initializedJacks_14() const { return ___initializedJacks_14; }
	inline int32_t* get_address_of_initializedJacks_14() { return &___initializedJacks_14; }
	inline void set_initializedJacks_14(int32_t value)
	{
		___initializedJacks_14 = value;
	}
};

struct JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields
{
public:
	// System.Action JackManager::JacksCreated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___JacksCreated_4;
	// System.Action JackManager::JacksElevationSet
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___JacksElevationSet_5;
	// System.Action JackManager::JacksInitlizedComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___JacksInitlizedComplete_6;

public:
	inline static int32_t get_offset_of_JacksCreated_4() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields, ___JacksCreated_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_JacksCreated_4() const { return ___JacksCreated_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_JacksCreated_4() { return &___JacksCreated_4; }
	inline void set_JacksCreated_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___JacksCreated_4 = value;
		Il2CppCodeGenWriteBarrier((&___JacksCreated_4), value);
	}

	inline static int32_t get_offset_of_JacksElevationSet_5() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields, ___JacksElevationSet_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_JacksElevationSet_5() const { return ___JacksElevationSet_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_JacksElevationSet_5() { return &___JacksElevationSet_5; }
	inline void set_JacksElevationSet_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___JacksElevationSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___JacksElevationSet_5), value);
	}

	inline static int32_t get_offset_of_JacksInitlizedComplete_6() { return static_cast<int32_t>(offsetof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields, ___JacksInitlizedComplete_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_JacksInitlizedComplete_6() const { return ___JacksInitlizedComplete_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_JacksInitlizedComplete_6() { return &___JacksInitlizedComplete_6; }
	inline void set_JacksInitlizedComplete_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___JacksInitlizedComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___JacksInitlizedComplete_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACKMANAGER_TDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_H
#ifndef LINE_T840031C82BBD2F9288BA1D9CFF1C1F20C659D198_H
#define LINE_T840031C82BBD2F9288BA1D9CFF1C1F20C659D198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t840031C82BBD2F9288BA1D9CFF1C1F20C659D198  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINE_T840031C82BBD2F9288BA1D9CFF1C1F20C659D198_H
#ifndef MAKESPHERES_T90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C_H
#define MAKESPHERES_T90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MakeSpheres
struct  MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MakeSpheres::spherePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___spherePrefab_4;
	// System.Int32 MakeSpheres::numberOfSpheres
	int32_t ___numberOfSpheres_5;
	// System.Single MakeSpheres::area
	float ___area_6;

public:
	inline static int32_t get_offset_of_spherePrefab_4() { return static_cast<int32_t>(offsetof(MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C, ___spherePrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_spherePrefab_4() const { return ___spherePrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_spherePrefab_4() { return &___spherePrefab_4; }
	inline void set_spherePrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___spherePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___spherePrefab_4), value);
	}

	inline static int32_t get_offset_of_numberOfSpheres_5() { return static_cast<int32_t>(offsetof(MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C, ___numberOfSpheres_5)); }
	inline int32_t get_numberOfSpheres_5() const { return ___numberOfSpheres_5; }
	inline int32_t* get_address_of_numberOfSpheres_5() { return &___numberOfSpheres_5; }
	inline void set_numberOfSpheres_5(int32_t value)
	{
		___numberOfSpheres_5 = value;
	}

	inline static int32_t get_offset_of_area_6() { return static_cast<int32_t>(offsetof(MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C, ___area_6)); }
	inline float get_area_6() const { return ___area_6; }
	inline float* get_address_of_area_6() { return &___area_6; }
	inline void set_area_6(float value)
	{
		___area_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKESPHERES_T90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C_H
#ifndef MAKESPLINE_T59DB73B3047A74677B6E54E98FECD0D66B7BB89B_H
#define MAKESPLINE_T59DB73B3047A74677B6E54E98FECD0D66B7BB89B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MakeSpline
struct  MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 MakeSpline::segments
	int32_t ___segments_4;
	// System.Boolean MakeSpline::loop
	bool ___loop_5;
	// System.Boolean MakeSpline::usePoints
	bool ___usePoints_6;

public:
	inline static int32_t get_offset_of_segments_4() { return static_cast<int32_t>(offsetof(MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B, ___segments_4)); }
	inline int32_t get_segments_4() const { return ___segments_4; }
	inline int32_t* get_address_of_segments_4() { return &___segments_4; }
	inline void set_segments_4(int32_t value)
	{
		___segments_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_usePoints_6() { return static_cast<int32_t>(offsetof(MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B, ___usePoints_6)); }
	inline bool get_usePoints_6() const { return ___usePoints_6; }
	inline bool* get_address_of_usePoints_6() { return &___usePoints_6; }
	inline void set_usePoints_6(bool value)
	{
		___usePoints_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKESPLINE_T59DB73B3047A74677B6E54E98FECD0D66B7BB89B_H
#ifndef MASKLINE1_T5C9257FD341489BF9030867576A171B5BBF8F81C_H
#define MASKLINE1_T5C9257FD341489BF9030867576A171B5BBF8F81C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaskLine1
struct  MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 MaskLine1::numberOfRects
	int32_t ___numberOfRects_4;
	// UnityEngine.Color MaskLine1::lineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lineColor_5;
	// UnityEngine.GameObject MaskLine1::mask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mask_6;
	// System.Single MaskLine1::moveSpeed
	float ___moveSpeed_7;
	// Vectrosity.VectorLine MaskLine1::rectLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___rectLine_8;
	// System.Single MaskLine1::t
	float ___t_9;
	// UnityEngine.Vector3 MaskLine1::startPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startPos_10;

public:
	inline static int32_t get_offset_of_numberOfRects_4() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___numberOfRects_4)); }
	inline int32_t get_numberOfRects_4() const { return ___numberOfRects_4; }
	inline int32_t* get_address_of_numberOfRects_4() { return &___numberOfRects_4; }
	inline void set_numberOfRects_4(int32_t value)
	{
		___numberOfRects_4 = value;
	}

	inline static int32_t get_offset_of_lineColor_5() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___lineColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lineColor_5() const { return ___lineColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lineColor_5() { return &___lineColor_5; }
	inline void set_lineColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lineColor_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___mask_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mask_6() const { return ___mask_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}

	inline static int32_t get_offset_of_moveSpeed_7() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___moveSpeed_7)); }
	inline float get_moveSpeed_7() const { return ___moveSpeed_7; }
	inline float* get_address_of_moveSpeed_7() { return &___moveSpeed_7; }
	inline void set_moveSpeed_7(float value)
	{
		___moveSpeed_7 = value;
	}

	inline static int32_t get_offset_of_rectLine_8() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___rectLine_8)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_rectLine_8() const { return ___rectLine_8; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_rectLine_8() { return &___rectLine_8; }
	inline void set_rectLine_8(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___rectLine_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectLine_8), value);
	}

	inline static int32_t get_offset_of_t_9() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___t_9)); }
	inline float get_t_9() const { return ___t_9; }
	inline float* get_address_of_t_9() { return &___t_9; }
	inline void set_t_9(float value)
	{
		___t_9 = value;
	}

	inline static int32_t get_offset_of_startPos_10() { return static_cast<int32_t>(offsetof(MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C, ___startPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startPos_10() const { return ___startPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startPos_10() { return &___startPos_10; }
	inline void set_startPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startPos_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKLINE1_T5C9257FD341489BF9030867576A171B5BBF8F81C_H
#ifndef MASKLINE2_T53ADB385DB59E6419F38EDA524D43DD30BB31D4A_H
#define MASKLINE2_T53ADB385DB59E6419F38EDA524D43DD30BB31D4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaskLine2
struct  MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 MaskLine2::numberOfPoints
	int32_t ___numberOfPoints_4;
	// UnityEngine.Color MaskLine2::lineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lineColor_5;
	// UnityEngine.GameObject MaskLine2::mask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mask_6;
	// System.Single MaskLine2::lineWidth
	float ___lineWidth_7;
	// System.Single MaskLine2::lineHeight
	float ___lineHeight_8;
	// Vectrosity.VectorLine MaskLine2::spikeLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___spikeLine_9;
	// System.Single MaskLine2::t
	float ___t_10;
	// UnityEngine.Vector3 MaskLine2::startPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startPos_11;

public:
	inline static int32_t get_offset_of_numberOfPoints_4() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___numberOfPoints_4)); }
	inline int32_t get_numberOfPoints_4() const { return ___numberOfPoints_4; }
	inline int32_t* get_address_of_numberOfPoints_4() { return &___numberOfPoints_4; }
	inline void set_numberOfPoints_4(int32_t value)
	{
		___numberOfPoints_4 = value;
	}

	inline static int32_t get_offset_of_lineColor_5() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___lineColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lineColor_5() const { return ___lineColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lineColor_5() { return &___lineColor_5; }
	inline void set_lineColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lineColor_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___mask_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mask_6() const { return ___mask_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}

	inline static int32_t get_offset_of_lineWidth_7() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___lineWidth_7)); }
	inline float get_lineWidth_7() const { return ___lineWidth_7; }
	inline float* get_address_of_lineWidth_7() { return &___lineWidth_7; }
	inline void set_lineWidth_7(float value)
	{
		___lineWidth_7 = value;
	}

	inline static int32_t get_offset_of_lineHeight_8() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___lineHeight_8)); }
	inline float get_lineHeight_8() const { return ___lineHeight_8; }
	inline float* get_address_of_lineHeight_8() { return &___lineHeight_8; }
	inline void set_lineHeight_8(float value)
	{
		___lineHeight_8 = value;
	}

	inline static int32_t get_offset_of_spikeLine_9() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___spikeLine_9)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_spikeLine_9() const { return ___spikeLine_9; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_spikeLine_9() { return &___spikeLine_9; }
	inline void set_spikeLine_9(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___spikeLine_9 = value;
		Il2CppCodeGenWriteBarrier((&___spikeLine_9), value);
	}

	inline static int32_t get_offset_of_t_10() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___t_10)); }
	inline float get_t_10() const { return ___t_10; }
	inline float* get_address_of_t_10() { return &___t_10; }
	inline void set_t_10(float value)
	{
		___t_10 = value;
	}

	inline static int32_t get_offset_of_startPos_11() { return static_cast<int32_t>(offsetof(MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A, ___startPos_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startPos_11() const { return ___startPos_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startPos_11() { return &___startPos_11; }
	inline void set_startPos_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startPos_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKLINE2_T53ADB385DB59E6419F38EDA524D43DD30BB31D4A_H
#ifndef ORBIT_T6DB1A9E8DA03DED089353C732FDB647ED291C907_H
#define ORBIT_T6DB1A9E8DA03DED089353C732FDB647ED291C907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Orbit
struct  Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Orbit::orbitSpeed
	float ___orbitSpeed_4;
	// System.Single Orbit::rotateSpeed
	float ___rotateSpeed_5;
	// System.Int32 Orbit::orbitLineResolution
	int32_t ___orbitLineResolution_6;
	// UnityEngine.Material Orbit::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_7;

public:
	inline static int32_t get_offset_of_orbitSpeed_4() { return static_cast<int32_t>(offsetof(Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907, ___orbitSpeed_4)); }
	inline float get_orbitSpeed_4() const { return ___orbitSpeed_4; }
	inline float* get_address_of_orbitSpeed_4() { return &___orbitSpeed_4; }
	inline void set_orbitSpeed_4(float value)
	{
		___orbitSpeed_4 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907, ___rotateSpeed_5)); }
	inline float get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline float* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(float value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_orbitLineResolution_6() { return static_cast<int32_t>(offsetof(Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907, ___orbitLineResolution_6)); }
	inline int32_t get_orbitLineResolution_6() const { return ___orbitLineResolution_6; }
	inline int32_t* get_address_of_orbitLineResolution_6() { return &___orbitLineResolution_6; }
	inline void set_orbitLineResolution_6(int32_t value)
	{
		___orbitLineResolution_6 = value;
	}

	inline static int32_t get_offset_of_lineMaterial_7() { return static_cast<int32_t>(offsetof(Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907, ___lineMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_7() const { return ___lineMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_7() { return &___lineMaterial_7; }
	inline void set_lineMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBIT_T6DB1A9E8DA03DED089353C732FDB647ED291C907_H
#ifndef POWERBAR_TD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD_H
#define POWERBAR_TD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerBar
struct  PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PowerBar::speed
	float ___speed_4;
	// System.Int32 PowerBar::lineWidth
	int32_t ___lineWidth_5;
	// System.Single PowerBar::radius
	float ___radius_6;
	// System.Int32 PowerBar::segmentCount
	int32_t ___segmentCount_7;
	// Vectrosity.VectorLine PowerBar::bar
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___bar_8;
	// UnityEngine.Vector2 PowerBar::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_9;
	// System.Single PowerBar::currentPower
	float ___currentPower_10;
	// System.Single PowerBar::targetPower
	float ___targetPower_11;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_lineWidth_5() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___lineWidth_5)); }
	inline int32_t get_lineWidth_5() const { return ___lineWidth_5; }
	inline int32_t* get_address_of_lineWidth_5() { return &___lineWidth_5; }
	inline void set_lineWidth_5(int32_t value)
	{
		___lineWidth_5 = value;
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}

	inline static int32_t get_offset_of_segmentCount_7() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___segmentCount_7)); }
	inline int32_t get_segmentCount_7() const { return ___segmentCount_7; }
	inline int32_t* get_address_of_segmentCount_7() { return &___segmentCount_7; }
	inline void set_segmentCount_7(int32_t value)
	{
		___segmentCount_7 = value;
	}

	inline static int32_t get_offset_of_bar_8() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___bar_8)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_bar_8() const { return ___bar_8; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_bar_8() { return &___bar_8; }
	inline void set_bar_8(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___bar_8 = value;
		Il2CppCodeGenWriteBarrier((&___bar_8), value);
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___position_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_9() const { return ___position_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_currentPower_10() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___currentPower_10)); }
	inline float get_currentPower_10() const { return ___currentPower_10; }
	inline float* get_address_of_currentPower_10() { return &___currentPower_10; }
	inline void set_currentPower_10(float value)
	{
		___currentPower_10 = value;
	}

	inline static int32_t get_offset_of_targetPower_11() { return static_cast<int32_t>(offsetof(PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD, ___targetPower_11)); }
	inline float get_targetPower_11() const { return ___targetPower_11; }
	inline float* get_address_of_targetPower_11() { return &___targetPower_11; }
	inline void set_targetPower_11(float value)
	{
		___targetPower_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POWERBAR_TD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD_H
#ifndef REALLYBASICLINE_T1BE9F9BA472881B65D7E0AF5D45174BFB1D3A07B_H
#define REALLYBASICLINE_T1BE9F9BA472881B65D7E0AF5D45174BFB1D3A07B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReallyBasicLine
struct  ReallyBasicLine_t1BE9F9BA472881B65D7E0AF5D45174BFB1D3A07B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REALLYBASICLINE_T1BE9F9BA472881B65D7E0AF5D45174BFB1D3A07B_H
#ifndef ROTATEAROUNDY_T3D2585C95AC51C10EDFA7F3D09B7B517360F578F_H
#define ROTATEAROUNDY_T3D2585C95AC51C10EDFA7F3D09B7B517360F578F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateAroundY
struct  RotateAroundY_t3D2585C95AC51C10EDFA7F3D09B7B517360F578F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single RotateAroundY::rotateSpeed
	float ___rotateSpeed_4;

public:
	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(RotateAroundY_t3D2585C95AC51C10EDFA7F3D09B7B517360F578F, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEAROUNDY_T3D2585C95AC51C10EDFA7F3D09B7B517360F578F_H
#ifndef ROTATERECTCONTROLLER_T75FF516BB20A0F6F2C05865289810CE6D31307C5_H
#define ROTATERECTCONTROLLER_T75FF516BB20A0F6F2C05865289810CE6D31307C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateRectController
struct  RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform RotateRectController::hole
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___hole_5;
	// UnityEngine.Vector3 RotateRectController::ballStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ballStartPos_6;
	// UnityEngine.Vector3 RotateRectController::holeStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___holeStartPos_7;
	// System.Single RotateRectController::angle
	float ___angle_8;

public:
	inline static int32_t get_offset_of_hole_5() { return static_cast<int32_t>(offsetof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5, ___hole_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_hole_5() const { return ___hole_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_hole_5() { return &___hole_5; }
	inline void set_hole_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___hole_5 = value;
		Il2CppCodeGenWriteBarrier((&___hole_5), value);
	}

	inline static int32_t get_offset_of_ballStartPos_6() { return static_cast<int32_t>(offsetof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5, ___ballStartPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ballStartPos_6() const { return ___ballStartPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ballStartPos_6() { return &___ballStartPos_6; }
	inline void set_ballStartPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ballStartPos_6 = value;
	}

	inline static int32_t get_offset_of_holeStartPos_7() { return static_cast<int32_t>(offsetof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5, ___holeStartPos_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_holeStartPos_7() const { return ___holeStartPos_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_holeStartPos_7() { return &___holeStartPos_7; }
	inline void set_holeStartPos_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___holeStartPos_7 = value;
	}

	inline static int32_t get_offset_of_angle_8() { return static_cast<int32_t>(offsetof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5, ___angle_8)); }
	inline float get_angle_8() const { return ___angle_8; }
	inline float* get_address_of_angle_8() { return &___angle_8; }
	inline void set_angle_8(float value)
	{
		___angle_8 = value;
	}
};

struct RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5_StaticFields
{
public:
	// RotateRectController_RotateRectControllerEvent RotateRectController::RotateRectDragUpdate
	RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E * ___RotateRectDragUpdate_4;

public:
	inline static int32_t get_offset_of_RotateRectDragUpdate_4() { return static_cast<int32_t>(offsetof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5_StaticFields, ___RotateRectDragUpdate_4)); }
	inline RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E * get_RotateRectDragUpdate_4() const { return ___RotateRectDragUpdate_4; }
	inline RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E ** get_address_of_RotateRectDragUpdate_4() { return &___RotateRectDragUpdate_4; }
	inline void set_RotateRectDragUpdate_4(RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E * value)
	{
		___RotateRectDragUpdate_4 = value;
		Il2CppCodeGenWriteBarrier((&___RotateRectDragUpdate_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATERECTCONTROLLER_T75FF516BB20A0F6F2C05865289810CE6D31307C5_H
#ifndef ROTATERECTDRAGCONTROLLER_TBFBDDE98DFD49D3C41882B9786ABB3001DF93D27_H
#define ROTATERECTDRAGCONTROLLER_TBFBDDE98DFD49D3C41882B9786ABB3001DF93D27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateRectDragController
struct  RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform RotateRectDragController::hole
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___hole_4;
	// UnityEngine.Vector3 RotateRectDragController::rectRotationStart
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rectRotationStart_5;
	// UnityEngine.Vector3 RotateRectDragController::holeStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___holeStartPos_6;
	// System.Single RotateRectDragController::angle
	float ___angle_7;

public:
	inline static int32_t get_offset_of_hole_4() { return static_cast<int32_t>(offsetof(RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27, ___hole_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_hole_4() const { return ___hole_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_hole_4() { return &___hole_4; }
	inline void set_hole_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___hole_4 = value;
		Il2CppCodeGenWriteBarrier((&___hole_4), value);
	}

	inline static int32_t get_offset_of_rectRotationStart_5() { return static_cast<int32_t>(offsetof(RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27, ___rectRotationStart_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rectRotationStart_5() const { return ___rectRotationStart_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rectRotationStart_5() { return &___rectRotationStart_5; }
	inline void set_rectRotationStart_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rectRotationStart_5 = value;
	}

	inline static int32_t get_offset_of_holeStartPos_6() { return static_cast<int32_t>(offsetof(RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27, ___holeStartPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_holeStartPos_6() const { return ___holeStartPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_holeStartPos_6() { return &___holeStartPos_6; }
	inline void set_holeStartPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___holeStartPos_6 = value;
	}

	inline static int32_t get_offset_of_angle_7() { return static_cast<int32_t>(offsetof(RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27, ___angle_7)); }
	inline float get_angle_7() const { return ___angle_7; }
	inline float* get_address_of_angle_7() { return &___angle_7; }
	inline void set_angle_7(float value)
	{
		___angle_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATERECTDRAGCONTROLLER_TBFBDDE98DFD49D3C41882B9786ABB3001DF93D27_H
#ifndef ROTATEVIEWPOINT_TBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465_H
#define ROTATEVIEWPOINT_TBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateViewpoint
struct  RotateViewpoint_tBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single RotateViewpoint::rotateSpeed
	float ___rotateSpeed_4;

public:
	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(RotateViewpoint_tBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEVIEWPOINT_TBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465_H
#ifndef SCRIBBLECUBE_TD68C988842EE2BC756E33AC375AA4608F164F971_H
#define SCRIBBLECUBE_TD68C988842EE2BC756E33AC375AA4608F164F971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScribbleCube
struct  ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture ScribbleCube::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// UnityEngine.Material ScribbleCube::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_5;
	// System.Int32 ScribbleCube::lineWidth
	int32_t ___lineWidth_6;
	// UnityEngine.Color ScribbleCube::color1
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color1_7;
	// UnityEngine.Color ScribbleCube::color2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color2_8;
	// Vectrosity.VectorLine ScribbleCube::line
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___line_9;
	// System.Collections.Generic.List`1<UnityEngine.Color32> ScribbleCube::lineColors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___lineColors_10;
	// System.Int32 ScribbleCube::numberOfPoints
	int32_t ___numberOfPoints_11;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_lineMaterial_5() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___lineMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_5() const { return ___lineMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_5() { return &___lineMaterial_5; }
	inline void set_lineMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_5), value);
	}

	inline static int32_t get_offset_of_lineWidth_6() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___lineWidth_6)); }
	inline int32_t get_lineWidth_6() const { return ___lineWidth_6; }
	inline int32_t* get_address_of_lineWidth_6() { return &___lineWidth_6; }
	inline void set_lineWidth_6(int32_t value)
	{
		___lineWidth_6 = value;
	}

	inline static int32_t get_offset_of_color1_7() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___color1_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color1_7() const { return ___color1_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color1_7() { return &___color1_7; }
	inline void set_color1_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color1_7 = value;
	}

	inline static int32_t get_offset_of_color2_8() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___color2_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color2_8() const { return ___color2_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color2_8() { return &___color2_8; }
	inline void set_color2_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color2_8 = value;
	}

	inline static int32_t get_offset_of_line_9() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___line_9)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_line_9() const { return ___line_9; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_line_9() { return &___line_9; }
	inline void set_line_9(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___line_9 = value;
		Il2CppCodeGenWriteBarrier((&___line_9), value);
	}

	inline static int32_t get_offset_of_lineColors_10() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___lineColors_10)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_lineColors_10() const { return ___lineColors_10; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_lineColors_10() { return &___lineColors_10; }
	inline void set_lineColors_10(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___lineColors_10 = value;
		Il2CppCodeGenWriteBarrier((&___lineColors_10), value);
	}

	inline static int32_t get_offset_of_numberOfPoints_11() { return static_cast<int32_t>(offsetof(ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971, ___numberOfPoints_11)); }
	inline int32_t get_numberOfPoints_11() const { return ___numberOfPoints_11; }
	inline int32_t* get_address_of_numberOfPoints_11() { return &___numberOfPoints_11; }
	inline void set_numberOfPoints_11(int32_t value)
	{
		___numberOfPoints_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIBBLECUBE_TD68C988842EE2BC756E33AC375AA4608F164F971_H
#ifndef SELECTLINE_TDF546A9E791B5C8FB1FC94E756CA7224A457BA83_H
#define SELECTLINE_TDF546A9E791B5C8FB1FC94E756CA7224A457BA83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectLine
struct  SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SelectLine::lineThickness
	float ___lineThickness_4;
	// System.Int32 SelectLine::extraThickness
	int32_t ___extraThickness_5;
	// System.Int32 SelectLine::numberOfLines
	int32_t ___numberOfLines_6;
	// Vectrosity.VectorLine[] SelectLine::lines
	VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* ___lines_7;
	// System.Boolean[] SelectLine::wasSelected
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___wasSelected_8;

public:
	inline static int32_t get_offset_of_lineThickness_4() { return static_cast<int32_t>(offsetof(SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83, ___lineThickness_4)); }
	inline float get_lineThickness_4() const { return ___lineThickness_4; }
	inline float* get_address_of_lineThickness_4() { return &___lineThickness_4; }
	inline void set_lineThickness_4(float value)
	{
		___lineThickness_4 = value;
	}

	inline static int32_t get_offset_of_extraThickness_5() { return static_cast<int32_t>(offsetof(SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83, ___extraThickness_5)); }
	inline int32_t get_extraThickness_5() const { return ___extraThickness_5; }
	inline int32_t* get_address_of_extraThickness_5() { return &___extraThickness_5; }
	inline void set_extraThickness_5(int32_t value)
	{
		___extraThickness_5 = value;
	}

	inline static int32_t get_offset_of_numberOfLines_6() { return static_cast<int32_t>(offsetof(SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83, ___numberOfLines_6)); }
	inline int32_t get_numberOfLines_6() const { return ___numberOfLines_6; }
	inline int32_t* get_address_of_numberOfLines_6() { return &___numberOfLines_6; }
	inline void set_numberOfLines_6(int32_t value)
	{
		___numberOfLines_6 = value;
	}

	inline static int32_t get_offset_of_lines_7() { return static_cast<int32_t>(offsetof(SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83, ___lines_7)); }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* get_lines_7() const { return ___lines_7; }
	inline VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC** get_address_of_lines_7() { return &___lines_7; }
	inline void set_lines_7(VectorLineU5BU5D_t82A4E23740CA0B16345EB7FF1890BDB473F90DDC* value)
	{
		___lines_7 = value;
		Il2CppCodeGenWriteBarrier((&___lines_7), value);
	}

	inline static int32_t get_offset_of_wasSelected_8() { return static_cast<int32_t>(offsetof(SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83, ___wasSelected_8)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_wasSelected_8() const { return ___wasSelected_8; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_wasSelected_8() { return &___wasSelected_8; }
	inline void set_wasSelected_8(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___wasSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___wasSelected_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTLINE_TDF546A9E791B5C8FB1FC94E756CA7224A457BA83_H
#ifndef SELECTIONBOX_T1998C9F01A5387D335F7B91414D8BADF353F3176_H
#define SELECTIONBOX_T1998C9F01A5387D335F7B91414D8BADF353F3176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionBox
struct  SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.VectorLine SelectionBox::selectionLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___selectionLine_4;
	// UnityEngine.Vector2 SelectionBox::originalPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___originalPos_5;
	// System.Collections.Generic.List`1<UnityEngine.Color32> SelectionBox::lineColors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___lineColors_6;

public:
	inline static int32_t get_offset_of_selectionLine_4() { return static_cast<int32_t>(offsetof(SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176, ___selectionLine_4)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_selectionLine_4() const { return ___selectionLine_4; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_selectionLine_4() { return &___selectionLine_4; }
	inline void set_selectionLine_4(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___selectionLine_4 = value;
		Il2CppCodeGenWriteBarrier((&___selectionLine_4), value);
	}

	inline static int32_t get_offset_of_originalPos_5() { return static_cast<int32_t>(offsetof(SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176, ___originalPos_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_originalPos_5() const { return ___originalPos_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_originalPos_5() { return &___originalPos_5; }
	inline void set_originalPos_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___originalPos_5 = value;
	}

	inline static int32_t get_offset_of_lineColors_6() { return static_cast<int32_t>(offsetof(SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176, ___lineColors_6)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_lineColors_6() const { return ___lineColors_6; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_lineColors_6() { return &___lineColors_6; }
	inline void set_lineColors_6(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___lineColors_6 = value;
		Il2CppCodeGenWriteBarrier((&___lineColors_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBOX_T1998C9F01A5387D335F7B91414D8BADF353F3176_H
#ifndef SELECTIONBOX2_TDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F_H
#define SELECTIONBOX2_TDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionBox2
struct  SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture SelectionBox2::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Single SelectionBox2::textureScale
	float ___textureScale_5;
	// Vectrosity.VectorLine SelectionBox2::selectionLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___selectionLine_6;
	// UnityEngine.Vector2 SelectionBox2::originalPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___originalPos_7;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_textureScale_5() { return static_cast<int32_t>(offsetof(SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F, ___textureScale_5)); }
	inline float get_textureScale_5() const { return ___textureScale_5; }
	inline float* get_address_of_textureScale_5() { return &___textureScale_5; }
	inline void set_textureScale_5(float value)
	{
		___textureScale_5 = value;
	}

	inline static int32_t get_offset_of_selectionLine_6() { return static_cast<int32_t>(offsetof(SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F, ___selectionLine_6)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_selectionLine_6() const { return ___selectionLine_6; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_selectionLine_6() { return &___selectionLine_6; }
	inline void set_selectionLine_6(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___selectionLine_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectionLine_6), value);
	}

	inline static int32_t get_offset_of_originalPos_7() { return static_cast<int32_t>(offsetof(SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F, ___originalPos_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_originalPos_7() const { return ___originalPos_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_originalPos_7() { return &___originalPos_7; }
	inline void set_originalPos_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___originalPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBOX2_TDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F_H
#ifndef SIMPLE3D_TD353BA9778D54F14C0A0DD9C6B44CB0F96F28B33_H
#define SIMPLE3D_TD353BA9778D54F14C0A0DD9C6B44CB0F96F28B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Simple3D
struct  Simple3D_tD353BA9778D54F14C0A0DD9C6B44CB0F96F28B33  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLE3D_TD353BA9778D54F14C0A0DD9C6B44CB0F96F28B33_H
#ifndef SIMPLE3D2_T6135107C7BB271355D586CAAAE0CD3DC58FC752E_H
#define SIMPLE3D2_T6135107C7BB271355D586CAAAE0CD3DC58FC752E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Simple3D2
struct  Simple3D2_t6135107C7BB271355D586CAAAE0CD3DC58FC752E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextAsset Simple3D2::vectorCube
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___vectorCube_4;

public:
	inline static int32_t get_offset_of_vectorCube_4() { return static_cast<int32_t>(offsetof(Simple3D2_t6135107C7BB271355D586CAAAE0CD3DC58FC752E, ___vectorCube_4)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_vectorCube_4() const { return ___vectorCube_4; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_vectorCube_4() { return &___vectorCube_4; }
	inline void set_vectorCube_4(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___vectorCube_4 = value;
		Il2CppCodeGenWriteBarrier((&___vectorCube_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLE3D2_T6135107C7BB271355D586CAAAE0CD3DC58FC752E_H
#ifndef SIMPLE3D3_T3680FA91F96B5D53327E98CBB8A2FBBAEEEEDD1F_H
#define SIMPLE3D3_T3680FA91F96B5D53327E98CBB8A2FBBAEEEEDD1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Simple3D3
struct  Simple3D3_t3680FA91F96B5D53327E98CBB8A2FBBAEEEEDD1F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLE3D3_T3680FA91F96B5D53327E98CBB8A2FBBAEEEEDD1F_H
#ifndef SIMPLECURVE_T99F8A68D121ACC9B57654B9D6A606778029CEB70_H
#define SIMPLECURVE_T99F8A68D121ACC9B57654B9D6A606778029CEB70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleCurve
struct  SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2[] SimpleCurve::curvePoints
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___curvePoints_4;
	// System.Int32 SimpleCurve::segments
	int32_t ___segments_5;

public:
	inline static int32_t get_offset_of_curvePoints_4() { return static_cast<int32_t>(offsetof(SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70, ___curvePoints_4)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_curvePoints_4() const { return ___curvePoints_4; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_curvePoints_4() { return &___curvePoints_4; }
	inline void set_curvePoints_4(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___curvePoints_4 = value;
		Il2CppCodeGenWriteBarrier((&___curvePoints_4), value);
	}

	inline static int32_t get_offset_of_segments_5() { return static_cast<int32_t>(offsetof(SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70, ___segments_5)); }
	inline int32_t get_segments_5() const { return ___segments_5; }
	inline int32_t* get_address_of_segments_5() { return &___segments_5; }
	inline void set_segments_5(int32_t value)
	{
		___segments_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECURVE_T99F8A68D121ACC9B57654B9D6A606778029CEB70_H
#ifndef SPLINEFOLLOW2D_T1F921893E40163930DDD9FBC5850D30C85504ADB_H
#define SPLINEFOLLOW2D_T1F921893E40163930DDD9FBC5850D30C85504ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineFollow2D
struct  SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SplineFollow2D::segments
	int32_t ___segments_4;
	// System.Boolean SplineFollow2D::loop
	bool ___loop_5;
	// UnityEngine.Transform SplineFollow2D::cube
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cube_6;
	// System.Single SplineFollow2D::speed
	float ___speed_7;

public:
	inline static int32_t get_offset_of_segments_4() { return static_cast<int32_t>(offsetof(SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB, ___segments_4)); }
	inline int32_t get_segments_4() const { return ___segments_4; }
	inline int32_t* get_address_of_segments_4() { return &___segments_4; }
	inline void set_segments_4(int32_t value)
	{
		___segments_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_cube_6() { return static_cast<int32_t>(offsetof(SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB, ___cube_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cube_6() const { return ___cube_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cube_6() { return &___cube_6; }
	inline void set_cube_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cube_6 = value;
		Il2CppCodeGenWriteBarrier((&___cube_6), value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEFOLLOW2D_T1F921893E40163930DDD9FBC5850D30C85504ADB_H
#ifndef SPLINEFOLLOW3D_T5CCAA9886895B73129A1D91A87D0F85E2214AA55_H
#define SPLINEFOLLOW3D_T5CCAA9886895B73129A1D91A87D0F85E2214AA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineFollow3D
struct  SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SplineFollow3D::segments
	int32_t ___segments_4;
	// System.Boolean SplineFollow3D::doLoop
	bool ___doLoop_5;
	// UnityEngine.Transform SplineFollow3D::cube
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cube_6;
	// System.Single SplineFollow3D::speed
	float ___speed_7;

public:
	inline static int32_t get_offset_of_segments_4() { return static_cast<int32_t>(offsetof(SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55, ___segments_4)); }
	inline int32_t get_segments_4() const { return ___segments_4; }
	inline int32_t* get_address_of_segments_4() { return &___segments_4; }
	inline void set_segments_4(int32_t value)
	{
		___segments_4 = value;
	}

	inline static int32_t get_offset_of_doLoop_5() { return static_cast<int32_t>(offsetof(SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55, ___doLoop_5)); }
	inline bool get_doLoop_5() const { return ___doLoop_5; }
	inline bool* get_address_of_doLoop_5() { return &___doLoop_5; }
	inline void set_doLoop_5(bool value)
	{
		___doLoop_5 = value;
	}

	inline static int32_t get_offset_of_cube_6() { return static_cast<int32_t>(offsetof(SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55, ___cube_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cube_6() const { return ___cube_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cube_6() { return &___cube_6; }
	inline void set_cube_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cube_6 = value;
		Il2CppCodeGenWriteBarrier((&___cube_6), value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEFOLLOW3D_T5CCAA9886895B73129A1D91A87D0F85E2214AA55_H
#ifndef TEXTDEMO_TEFE68CCC74208CE78417CFFEE0E765672083667B_H
#define TEXTDEMO_TEFE68CCC74208CE78417CFFEE0E765672083667B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextDemo
struct  TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String TextDemo::text
	String_t* ___text_4;
	// System.Int32 TextDemo::textSize
	int32_t ___textSize_5;
	// Vectrosity.VectorLine TextDemo::textLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___textLine_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_textSize_5() { return static_cast<int32_t>(offsetof(TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B, ___textSize_5)); }
	inline int32_t get_textSize_5() const { return ___textSize_5; }
	inline int32_t* get_address_of_textSize_5() { return &___textSize_5; }
	inline void set_textSize_5(int32_t value)
	{
		___textSize_5 = value;
	}

	inline static int32_t get_offset_of_textLine_6() { return static_cast<int32_t>(offsetof(TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B, ___textLine_6)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_textLine_6() const { return ___textLine_6; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_textLine_6() { return &___textLine_6; }
	inline void set_textLine_6(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___textLine_6 = value;
		Il2CppCodeGenWriteBarrier((&___textLine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTDEMO_TEFE68CCC74208CE78417CFFEE0E765672083667B_H
#ifndef UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#define UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniformTexturedLine
struct  UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture UniformTexturedLine::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_4;
	// System.Single UniformTexturedLine::lineWidth
	float ___lineWidth_5;
	// System.Single UniformTexturedLine::textureScale
	float ___textureScale_6;

public:
	inline static int32_t get_offset_of_lineTexture_4() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___lineTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_4() const { return ___lineTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_4() { return &___lineTexture_4; }
	inline void set_lineTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_4), value);
	}

	inline static int32_t get_offset_of_lineWidth_5() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___lineWidth_5)); }
	inline float get_lineWidth_5() const { return ___lineWidth_5; }
	inline float* get_address_of_lineWidth_5() { return &___lineWidth_5; }
	inline void set_lineWidth_5(float value)
	{
		___lineWidth_5 = value;
	}

	inline static int32_t get_offset_of_textureScale_6() { return static_cast<int32_t>(offsetof(UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F, ___textureScale_6)); }
	inline float get_textureScale_6() const { return ___textureScale_6; }
	inline float* get_address_of_textureScale_6() { return &___textureScale_6; }
	inline void set_textureScale_6(float value)
	{
		___textureScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMTEXTUREDLINE_TBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F_H
#ifndef VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#define VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorObject
struct  VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// VectorObject_Shape VectorObject::shape
	int32_t ___shape_4;

public:
	inline static int32_t get_offset_of_shape_4() { return static_cast<int32_t>(offsetof(VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B, ___shape_4)); }
	inline int32_t get_shape_4() const { return ___shape_4; }
	inline int32_t* get_address_of_shape_4() { return &___shape_4; }
	inline void set_shape_4(int32_t value)
	{
		___shape_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT_T6A18426C986D9DF260E55DDD7CB58499730D312B_H
#ifndef XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H
#define XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XrayLineData
struct  XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture XrayLineData::lineTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___lineTexture_5;
	// System.Single XrayLineData::lineWidth
	float ___lineWidth_6;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>> XrayLineData::shapePoints
	List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * ___shapePoints_7;

public:
	inline static int32_t get_offset_of_lineTexture_5() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___lineTexture_5)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_lineTexture_5() const { return ___lineTexture_5; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_lineTexture_5() { return &___lineTexture_5; }
	inline void set_lineTexture_5(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___lineTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineTexture_5), value);
	}

	inline static int32_t get_offset_of_lineWidth_6() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___lineWidth_6)); }
	inline float get_lineWidth_6() const { return ___lineWidth_6; }
	inline float* get_address_of_lineWidth_6() { return &___lineWidth_6; }
	inline void set_lineWidth_6(float value)
	{
		___lineWidth_6 = value;
	}

	inline static int32_t get_offset_of_shapePoints_7() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3, ___shapePoints_7)); }
	inline List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * get_shapePoints_7() const { return ___shapePoints_7; }
	inline List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C ** get_address_of_shapePoints_7() { return &___shapePoints_7; }
	inline void set_shapePoints_7(List_1_t32916246A3DDDBFC219E335CCF07C7F68024800C * value)
	{
		___shapePoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___shapePoints_7), value);
	}
};

struct XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields
{
public:
	// XrayLineData XrayLineData::use
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * ___use_4;

public:
	inline static int32_t get_offset_of_use_4() { return static_cast<int32_t>(offsetof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields, ___use_4)); }
	inline XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * get_use_4() const { return ___use_4; }
	inline XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 ** get_address_of_use_4() { return &___use_4; }
	inline void set_use_4(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3 * value)
	{
		___use_4 = value;
		Il2CppCodeGenWriteBarrier((&___use_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRAYLINEDATA_T4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (Line_t840031C82BBD2F9288BA1D9CFF1C1F20C659D198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (ReallyBasicLine_t1BE9F9BA472881B65D7E0AF5D45174BFB1D3A07B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[3] = 
{
	UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F::get_offset_of_lineTexture_4(),
	UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F::get_offset_of_lineWidth_5(),
	UniformTexturedLine_tBA892DD4FF70A9E41C756A617FDF1EB3F3BB032F::get_offset_of_textureScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (Simple3D_tD353BA9778D54F14C0A0DD9C6B44CB0F96F28B33), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (Simple3D2_t6135107C7BB271355D586CAAAE0CD3DC58FC752E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3604[1] = 
{
	Simple3D2_t6135107C7BB271355D586CAAAE0CD3DC58FC752E::get_offset_of_vectorCube_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (Simple3D3_t3680FA91F96B5D53327E98CBB8A2FBBAEEEEDD1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3606[3] = 
{
	CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543::get_offset_of_objectNumber_4(),
	CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543::get_offset_of_controlObject_5(),
	CurvePointControl_tEA94A5EF9323ACC15BE6CD71E0DA69AE44C1D543::get_offset_of_controlObject2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE), -1, sizeof(DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3607[19] = 
{
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_lineTexture_4(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_lineColor_5(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_dottedLineTexture_6(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_dottedLineColor_7(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_segments_8(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_anchorPoint_9(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_controlPoint_10(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_numberOfCurves_11(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_line_12(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_controlLine_13(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_pointIndex_14(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_anchorObject_15(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_oldWidth_16(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_useDottedLine_17(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_oldDottedLineSetting_18(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_oldSegments_19(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE::get_offset_of_listPoints_20(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields::get_offset_of_use_21(),
	DrawCurve_tCCAEB90CD09FB46C4D9FED3D802B523FD284BEAE_StaticFields::get_offset_of_cam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[2] = 
{
	SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70::get_offset_of_curvePoints_4(),
	SimpleCurve_t99F8A68D121ACC9B57654B9D6A606778029CEB70::get_offset_of_segments_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3609[12] = 
{
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_rotateSpeed_4(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_maxPoints_5(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_line_6(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_endReached_7(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_continuous_8(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_oldContinuous_9(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_fillJoins_10(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_oldFillJoins_11(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_weldJoins_12(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_oldWeldJoins_13(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_thickLine_14(),
	DrawLines_t5C7A5BBFC30FAD52B80101ECD004839D7CDC0AB1::get_offset_of_canClick_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[14] = 
{
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_lineTex_4(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_maxPoints_5(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_lineWidth_6(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_minPixelMove_7(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_useEndCap_8(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_capLineTex_9(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_capTex_10(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_capLineWidth_11(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_line3D_12(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_distanceFromCamera_13(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_line_14(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_previousPosition_15(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_sqrMinPixelMove_16(),
	DrawLinesMouse_t44BEFAF42C864E4492E03588D600B14A9E808794::get_offset_of_canDraw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3611[13] = 
{
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_lineTex_4(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_maxPoints_5(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_lineWidth_6(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_minPixelMove_7(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_useEndCap_8(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_capLineTex_9(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_capTex_10(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_capLineWidth_11(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_line_12(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_previousPosition_13(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_sqrMinPixelMove_14(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_canDraw_15(),
	DrawLinesTouch_tCB035475C9054C2816A36335F3BAF562DB888233::get_offset_of_touch_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[5] = 
{
	Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B::get_offset_of_lineTexture_4(),
	Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B::get_offset_of_xRadius_5(),
	Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B::get_offset_of_yRadius_6(),
	Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B::get_offset_of_segments_7(),
	Ellipse1_tAA67603AB076BAC54B1F84144CDD4BDB9860AB6B::get_offset_of_pointRotation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[3] = 
{
	Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB::get_offset_of_lineTexture_4(),
	Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB::get_offset_of_segments_5(),
	Ellipse2_tD409B378EB9205F18850D2180327013ACFC97DCB::get_offset_of_numberOfEllipses_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[6] = 
{
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_lineTex_4(),
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_lineTex2_5(),
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_lineTex3_6(),
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_frontTex_7(),
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_backTex_8(),
	EndCapDemo_t24A2021892C1CE6F6D1E84A577B02DEBF9E7960A::get_offset_of_capTex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[2] = 
{
	DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149::get_offset_of_gridPixels_4(),
	DrawGrid_t44F7755547A0EC8DDF79656E4DA45FDE56721149::get_offset_of_gridLine_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[5] = 
{
	Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3::get_offset_of_numberOfLines_4(),
	Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3::get_offset_of_distanceBetweenLines_5(),
	Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3::get_offset_of_moveSpeed_6(),
	Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3::get_offset_of_rotateSpeed_7(),
	Grid3D_t05BB99665B664C9D29CB95227BEF6F439918EBE3::get_offset_of_lineWidth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[16] = 
{
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_lineWidth_4(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_energyLineWidth_5(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_selectionSize_6(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_force_7(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_pointsInEnergyLine_8(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_line_9(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_energyLine_10(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_hit_11(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_selectIndex_12(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_energyLevel_13(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_canClick_14(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_spheres_15(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_timer_16(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_ignoreLayer_17(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_defaultLayer_18(),
	Highlight_t4C4D1CB930E1E218D59EE144C1E23955EBF63BDD::get_offset_of_fading_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[7] = 
{
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CU3E1__state_0(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CU3E2__current_1(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_instantFade_2(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CU3E4__this_3(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CstartColorU3E5__2_4(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CthisIndexU3E5__3_5(),
	U3CFadeColorU3Ed__22_tAF497C7701CB339968BB75FECDE2E606992FF577::get_offset_of_U3CtU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[3] = 
{
	MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C::get_offset_of_spherePrefab_4(),
	MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C::get_offset_of_numberOfSpheres_5(),
	MakeSpheres_t90109DB9A959729CFAE3EE6DD82E4DA47DDD5A3C::get_offset_of_area_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (RotateAroundY_t3D2585C95AC51C10EDFA7F3D09B7B517360F578F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[1] = 
{
	RotateAroundY_t3D2585C95AC51C10EDFA7F3D09B7B517360F578F::get_offset_of_rotateSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[7] = 
{
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_numberOfRects_4(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_lineColor_5(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_mask_6(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_moveSpeed_7(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_rectLine_8(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_t_9(),
	MaskLine1_t5C9257FD341489BF9030867576A171B5BBF8F81C::get_offset_of_startPos_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[8] = 
{
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_numberOfPoints_4(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_lineColor_5(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_mask_6(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_lineWidth_7(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_lineHeight_8(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_spikeLine_9(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_t_10(),
	MaskLine2_t53ADB385DB59E6419F38EDA524D43DD30BB31D4A::get_offset_of_startPos_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[2] = 
{
	CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297::get_offset_of_numberOfStars_4(),
	CreateStars_t07F8B8018F982C1C11EFF017130F2804CD785297::get_offset_of_stars_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[4] = 
{
	Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907::get_offset_of_orbitSpeed_4(),
	Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907::get_offset_of_rotateSpeed_5(),
	Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907::get_offset_of_orbitLineResolution_6(),
	Orbit_t6DB1A9E8DA03DED089353C732FDB647ED291C907::get_offset_of_lineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (RotateViewpoint_tBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[1] = 
{
	RotateViewpoint_tBD926F8B60D1DFF6C04B94E125A21B6C1AC5F465::get_offset_of_rotateSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[7] = 
{
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_lineTexture_4(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_segments_5(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_visibleLineSegments_6(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_speed_7(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_startIndex_8(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_endIndex_9(),
	AnimatePartialLine_t8353382416A0859D7A0B39A20F0C062BD97610B6::get_offset_of_line_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[9] = 
{
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_lineTex_4(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_lineColor_5(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_maxPoints_6(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_continuousUpdate_7(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_ballPrefab_8(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_force_9(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_pathLine_10(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_pathIndex_11(),
	DrawPath_t5378184F158682D121E87B61DFAAA57A2A178519::get_offset_of_ball_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[5] = 
{
	U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451::get_offset_of_U3CU3E1__state_0(),
	U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451::get_offset_of_U3CU3E2__current_1(),
	U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451::get_offset_of_U3CU3E4__this_2(),
	U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451::get_offset_of_thisTransform_3(),
	U3CSamplePointsU3Ed__11_tA6E94A341AE69E60D93A4EFE238EC24991DD4451::get_offset_of_U3CrunningU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[4] = 
{
	DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378::get_offset_of_dotSize_4(),
	DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378::get_offset_of_numberOfDots_5(),
	DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378::get_offset_of_numberOfRings_6(),
	DrawPoints_tF5EE7B35E360642A7C4D1B56E0DBB59B1A945378::get_offset_of_dotColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[8] = 
{
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_speed_4(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_lineWidth_5(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_radius_6(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_segmentCount_7(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_bar_8(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_position_9(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_currentPower_10(),
	PowerBar_tD10DBFEFA7B27C77AFFB4141D3195F9D7B41A1BD::get_offset_of_targetPower_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[8] = 
{
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_hillTexture_4(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_hillPhysicsMaterial_5(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_numberOfPoints_6(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_numberOfHills_7(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_ball_8(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_storedPosition_9(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_hills_10(),
	CreateHills_t47E99F12A2645EABF95BBF8CB4E411573E2030D5::get_offset_of_splinePoints_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[2] = 
{
	CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B::get_offset_of_zoomSpeed_4(),
	CameraZoom_tD8CB4E12EE033A7224A82B1849DFC88378E6479B::get_offset_of_keyZoomSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[8] = 
{
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_lineTexture_4(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_lineMaterial_5(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_lineWidth_6(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_color1_7(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_color2_8(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_line_9(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_lineColors_10(),
	ScribbleCube_tD68C988842EE2BC756E33AC375AA4608F164F971::get_offset_of_numberOfPoints_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[3] = 
{
	SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176::get_offset_of_selectionLine_4(),
	SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176::get_offset_of_originalPos_5(),
	SelectionBox_t1998C9F01A5387D335F7B91414D8BADF353F3176::get_offset_of_lineColors_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[3] = 
{
	U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27::get_offset_of_U3CU3E1__state_0(),
	U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27::get_offset_of_U3CU3E2__current_1(),
	U3CCycleColorU3Ed__6_t8EB25DFD392E5F8365F1E48D49501ACC8B553E27::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3636[4] = 
{
	SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F::get_offset_of_lineTexture_4(),
	SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F::get_offset_of_textureScale_5(),
	SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F::get_offset_of_selectionLine_6(),
	SelectionBox2_tDBEEE3CC8903E57CDF662CD73C27FFA101CF3E9F::get_offset_of_originalPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[5] = 
{
	SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83::get_offset_of_lineThickness_4(),
	SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83::get_offset_of_extraThickness_5(),
	SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83::get_offset_of_numberOfLines_6(),
	SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83::get_offset_of_lines_7(),
	SelectLine_tDF546A9E791B5C8FB1FC94E756CA7224A457BA83::get_offset_of_wasSelected_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[3] = 
{
	MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B::get_offset_of_segments_4(),
	MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B::get_offset_of_loop_5(),
	MakeSpline_t59DB73B3047A74677B6E54E98FECD0D66B7BB89B::get_offset_of_usePoints_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3639[4] = 
{
	SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB::get_offset_of_segments_4(),
	SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB::get_offset_of_loop_5(),
	SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB::get_offset_of_cube_6(),
	SplineFollow2D_t1F921893E40163930DDD9FBC5850D30C85504ADB::get_offset_of_speed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3640[5] = 
{
	U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1::get_offset_of_U3ClineU3E5__2_3(),
	U3CStartU3Ed__4_tC42A5F1439C73FF835A1CDA996E878E8347EF1C1::get_offset_of_U3CdistU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[4] = 
{
	SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55::get_offset_of_segments_4(),
	SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55::get_offset_of_doLoop_5(),
	SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55::get_offset_of_cube_6(),
	SplineFollow3D_t5CCAA9886895B73129A1D91A87D0F85E2214AA55::get_offset_of_speed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[5] = 
{
	U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455::get_offset_of_U3ClineU3E5__2_3(),
	U3CStartU3Ed__4_t057C20B77A2DE2007EFAC0D6D8AE8854FD5EF455::get_offset_of_U3CdistU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[3] = 
{
	TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B::get_offset_of_text_4(),
	TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B::get_offset_of_textSize_5(),
	TextDemo_tEFE68CCC74208CE78417CFFEE0E765672083667B::get_offset_of_textLine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[7] = 
{
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_moveSpeed_4(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_explodePower_5(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_vectorCam_6(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_mouseDown_7(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_rigidbodies_8(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_canClick_9(),
	DrawBox_tDCD729571E4E007C00D0EBFD1ADD18E2DF50AD8C::get_offset_of_boxDrawn_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[3] = 
{
	U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__7_tE11A570AFAD9602C994C1F904115052D61726850::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[1] = 
{
	VectorObject_t6A18426C986D9DF260E55DDD7CB58499730D312B::get_offset_of_shape_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3647[3] = 
{
	Shape_t6F751C5B899E802E0F2B08C82EFB8C4E0DC706DB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3), -1, sizeof(XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3648[4] = 
{
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3_StaticFields::get_offset_of_use_4(),
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3::get_offset_of_lineTexture_5(),
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3::get_offset_of_lineWidth_6(),
	XrayLineData_t4BA3BBC2D3DC5AD5E7372BBC14FA9A9CEFB371F3::get_offset_of_shapePoints_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6), -1, sizeof(DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3649[9] = 
{
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields::get_offset_of_LoadJacksAfterAPIDataCopmlete_4(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6_StaticFields::get_offset_of_DataLoadedComplete_5(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_arrowsLoaded_6(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_contoursLoaded_7(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_puttsLoaded_8(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_jacksCreated_9(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_jacksElevetionSet_10(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_greenModelLoaded_11(),
	DataLoadedManager_t7C48A73C9DF0BC04E093E7D964BE7428B88687F6::get_offset_of_appDataLoaded_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[3] = 
{
	APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B::get_offset_of_apiBaseURL_4(),
	APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B::get_offset_of_permanentToken_5(),
	APIConfigurationController_tE8F2ED6E76DED60D8DCC80378972FB6B3850D83B::get_offset_of_apiRequest_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[4] = 
{
	U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForProjectoDataU3Ed__4_tBDAAE250C4FC2BCA59A4EA0025DF1A9A13A04D6E::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[2] = 
{
	APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6::get_offset_of_postHeaderDict_4(),
	APICourseCalendarController_tC06934C155FBA856B1495D7021C4B113FC09DBE6::get_offset_of_courseManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A), -1, sizeof(U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3653[2] = 
{
	U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB09D592FE943F06E23805710E2711C9A9B736B2A_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8), -1, sizeof(APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3654[2] = 
{
	APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8_StaticFields::get_offset_of_GreenElevationDataLoaded_4(),
	APIGreenElevationDataController_t36A30AA69AE3B94FF8C65E541D5D1031586B3AF8::get_offset_of_client_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993), -1, sizeof(U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3655[2] = 
{
	U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE049D2F79AFDC77CC44DD153BD80780466E61993_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[9] = 
{
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_apiBaseURL_4(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_permanentToken_5(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_apiRequest_6(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_courseManager_7(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_holeDragInput_8(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_holeRectRot_9(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_holeContainer_10(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_projectorStatusData_11(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_puttsUpdated_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[4] = 
{
	U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4::get_offset_of__webRequest_2(),
	U3CWaitForProjectoDataU3Ed__11_t0084AA54E6FBB2A61E0FBAF30BB8FD66144109F4::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[7] = 
{
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_apiBaseURL_4(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_permanentToken_5(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_preloader_6(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_apiRequest_7(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_postHeaderDict_8(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_client_9(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_courseManager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64), -1, sizeof(U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3659[2] = 
{
	U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3AB132847052CED2D0E9F15F12163B529E3E7D64_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[4] = 
{
	U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7::get_offset_of_data_2(),
	U3CWaitForGetPuttU3Ed__9_t945D70A7877F27EFCB7F908F6027C3AC94E5ABD7::get_offset_of__puttInteractive_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[2] = 
{
	APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630::get_offset_of_apiBaseURL_4(),
	APIElevationDataTransferController_tEE52CF62B640E345F993777E434B794B8E955630::get_offset_of_apiRequest_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[4] = 
{
	U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForElevationTransferDataU3Ed__4_t454674913F8BDFC04EF8A95F225843F2A5F843D3::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A), -1, sizeof(APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3663[6] = 
{
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A_StaticFields::get_offset_of_APIGetConfigurationDataComplete_4(),
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A::get_offset_of_apiBaseURL_5(),
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A::get_offset_of_apiRequest_6(),
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A::get_offset_of_projectorData_7(),
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A::get_offset_of_reloadPreviousGreen_8(),
	APIGetConfigurationController_tFE15AE0BE552D54DE1113DA0DC118284A20B214A::get_offset_of_courseManager_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[4] = 
{
	U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForConfigurationDataU3Ed__10_tEEFBC4CF6F071708602EC9FE2C35B3F1116B6438::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08), -1, sizeof(ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3665[6] = 
{
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08_StaticFields::get_offset_of_ArrowsCollectionComplete_4(),
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08::get_offset_of_arrowsGathersInHoleContainer_5(),
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08::get_offset_of_arrowsForRef_6(),
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08::get_offset_of_arrowCollectorCollider_7(),
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08::get_offset_of_arrowManager_8(),
	ArrowCollector_tB93D03CEEBC4AEB2FA63CC427E40A83E716C8D08::get_offset_of_arrowCollector_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (BallDrag_t98CB96C6069713A0429A7AC98A108AED42299166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[1] = 
{
	BallDrag_t98CB96C6069713A0429A7AC98A108AED42299166::get_offset_of_holeCamera_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[6] = 
{
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_scrollViewCont_4(),
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_listBtnPrefab_5(),
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_courseManager_6(),
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_dailyTemplateHoleBtnPrefab_7(),
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_jackManager_8(),
	CalendarController_tF48490BCBA0FD0DAAC6B0303E20302A057212338::get_offset_of_holeLocationPrefabList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (CourseCalendarDay_tC84B10CAB8C8E39932384D10C308A77D4DAB24F2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3668[8] = 
{
	CourseCalendarDay_tC84B10CAB8C8E39932384D10C308A77D4DAB24F2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[2] = 
{
	U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass8_0_t1D02706154F17F7E780B584C3E8A5381A149BB34::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[2] = 
{
	U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass9_0_t22C76A9F1226B1BCDFCAC0BB870837B79832F3A1::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (ConfigRoboGreen_tC89B896171249FAF5C285A85224E453E7E13930F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[1] = 
{
	ConfigRoboGreen_tC89B896171249FAF5C285A85224E453E7E13930F::get_offset_of_jackList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC), -1, sizeof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3672[3] = 
{
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields::get_offset_of_inst_4(),
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC::get_offset_of_popupName_5(),
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC::get_offset_of_alertPopup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7), -1, sizeof(APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3673[1] = 
{
	APIDXFController_t2916A9F6BB9FACD82B34EAA943CE1641A430E2F7_StaticFields::get_offset_of_DXFLoadedComplete_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[6] = 
{
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of_U3CU3E1__state_0(),
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of_U3CU3E2__current_1(),
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of__greenID_2(),
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of__typeId_3(),
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of_U3CU3E4__this_4(),
	U3CGetDXFModelFromAPIU3Ed__4_t512887611CD7AA6D61D5AC0C35C3670B07F63AF9::get_offset_of_U3CwwwU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C), -1, sizeof(APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3675[10] = 
{
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_GreenLoaded_4(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_RegionsLoaded_5(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_CoursesInRegionLoaded_6(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_GreenGroupsInCourseLoaded_7(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_apiBaseURL_8(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C_StaticFields::get_offset_of_permanentToken_9(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C::get_offset_of_postHeaderDict_10(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C::get_offset_of_courseManager_11(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C::get_offset_of_courseIDToLoad_12(),
	APIGreenController_t822F4A90519A3A01081E8CBA458F740113FED13C::get_offset_of_regionCodeToLoad_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (CourseDataEvent_t8E7637E2129D80FA7D871BC563CB4327F9E54901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8), -1, sizeof(U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3677[2] = 
{
	U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t69D8C8561E35A6BB1323BAC4C16D9303E18C12E8_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (APIJackController_t3C67DF98CDEEDEB36426B098DCAD1B0C9B44B70E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[6] = 
{
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_PostLoginURL_4(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_apiRequest_5(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_userNameForTesting_6(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_passwordForTesting_7(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_testingLOGIN_8(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_userController_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F), -1, sizeof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3680[2] = 
{
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[4] = 
{
	U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30::get_offset_of_U3CU3E1__state_0(),
	U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30::get_offset_of_U3CU3E2__current_1(),
	U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30::get_offset_of_U3CU3E4__this_2(),
	U3CLoginRequestU3Ed__8_t286EE8C57C961623E0E2098657F72DB3E401DB30::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[7] = 
{
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_speed_4(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_isDoneFading_5(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_isDoneAnimating_6(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_slope_7(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_slopeColor_8(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_hex_9(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_arrow_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[2] = 
{
	HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0::get_offset_of_holeDragBoxCollider_4(),
	HoleController_t91DD0DED9DDF9569FF870A52FC6EB7D5BDA9EDF0::get_offset_of_rotateRectDragObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC), -1, sizeof(JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3684[9] = 
{
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC_StaticFields::get_offset_of_JackInit_4(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_elevation_5(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_jackIndex_6(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_hitPointPrefab_7(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_coll_8(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_jackHitPoint_9(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_greenLoaded_10(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_courseManager_11(),
	JackController_t5FD444F8395BD5BE207724D6631BCAFD7ECB0AEC::get_offset_of_initialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[7] = 
{
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_vertices_4(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_triangles_5(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_vertList_6(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_greenLoaded_7(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_lowestPoint_8(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_highestPoint_9(),
	CustomCollider_t350914B397EA4CB11F66C9E8F20B78008B33B69E::get_offset_of_pointList_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11), -1, sizeof(DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3686[2] = 
{
	DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11_StaticFields::get_offset_of_TemplateHoleSelected_4(),
	DailyTemplateHole_tF7629CEBE34ED72B83D645A5425D35E84CFB3F11::get_offset_of_templateHoleLocation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (DailyTemplateHolevent_t297F3EF6EC1735B01FE448B6D3639F55933F3F10), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (ExtensionMethods_t9C246E3C94E8653839FE7FBBE6549E8030A5B14B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[3] = 
{
	FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F::get_offset_of_customCollider_4(),
	FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F::get_offset_of_courseManager_5(),
	FindLowestMeshPoint_tAD11DF74019C431737B19B26512B89D9D4B5069F::get_offset_of_colliderPointPrefab_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C), -1, sizeof(GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3690[1] = 
{
	GameViewManager_t1F1B600DC5E6B909BD9E4ADF9752B61C9BF8487C_StaticFields::get_offset_of__instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (GreenDXFDelete_t7BB0794D0F2BF22361A781BC47AEE07DEF6C60A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[7] = 
{
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_cameraGreen_4(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_greenLockBtn_5(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_lockSprite_6(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_unlockSprite_7(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_holeContainer_8(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_cameraManager_9(),
	GreenMovementNavManager_t8F23F81C48F7568669F230E8E06A872C5F4F6AF7::get_offset_of_greenMovementNav_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (GreenViewController_t7AE2514749D4E81DC71D9D5B8B0012D8C6DF5F5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[3] = 
{
	DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6::get_offset_of_radius_4(),
	DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6::get_offset_of_numSegments_5(),
	DrawRadius_t10C3BFE0D62C44E04E3A71EFDED10DFA8C8687C6::get_offset_of_radiusLR_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (HoleRectController_t3D13E7A35AA9F3957DF511142E4289D151246EA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F), -1, sizeof(JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3696[11] = 
{
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields::get_offset_of_JacksCreated_4(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields::get_offset_of_JacksElevationSet_5(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F_StaticFields::get_offset_of_JacksInitlizedComplete_6(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_rows_7(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_columns_8(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_jackPrefab_9(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_jackContainer_10(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_holeContainer_11(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_jackList_12(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_customCollider_13(),
	JackManager_tDA59B82C14AB5324C503FC1C5F1C2C1A72F3CB1F::get_offset_of_initializedJacks_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5), -1, sizeof(RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3697[5] = 
{
	RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5_StaticFields::get_offset_of_RotateRectDragUpdate_4(),
	RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5::get_offset_of_hole_5(),
	RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5::get_offset_of_ballStartPos_6(),
	RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5::get_offset_of_holeStartPos_7(),
	RotateRectController_t75FF516BB20A0F6F2C05865289810CE6D31307C5::get_offset_of_angle_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (RotateRectControllerEvent_t2699905974A56D173A88FB96BF0F0756A74A9D0E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[4] = 
{
	RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27::get_offset_of_hole_4(),
	RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27::get_offset_of_rectRotationStart_5(),
	RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27::get_offset_of_holeStartPos_6(),
	RotateRectDragController_tBFBDDE98DFD49D3C41882B9786ABB3001DF93D27::get_offset_of_angle_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
