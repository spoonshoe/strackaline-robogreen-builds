﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// System.Action`1<DG.Tweening.DOTweenAnimation>
struct Action_1_t1148FC14E6CA6B1B3A1A9D62351118D36E9A0A88;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.Outline
struct Outline_tB750E496976B072E79142D51C0A991AC20183095;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef DOTWEENANIMATIONEXTENSIONS_TED069AD902CB6735F3C41CD13A524CA7995C653F_H
#define DOTWEENANIMATIONEXTENSIONS_TED069AD902CB6735F3C41CD13A524CA7995C653F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimationExtensions
struct  DOTweenAnimationExtensions_tED069AD902CB6735F3C41CD13A524CA7995C653F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONEXTENSIONS_TED069AD902CB6735F3C41CD13A524CA7995C653F_H
#ifndef DOTWEENCYINSTRUCTION_T99180052E9B15DB8365A25440477F7BE9B4D6847_H
#define DOTWEENCYINSTRUCTION_T99180052E9B15DB8365A25440477F7BE9B4D6847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction
struct  DOTweenCYInstruction_t99180052E9B15DB8365A25440477F7BE9B4D6847  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCYINSTRUCTION_T99180052E9B15DB8365A25440477F7BE9B4D6847_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_TBF413496555AC2A272175AA8755DF59A98BCA0FA_H
#define U3CU3EC__DISPLAYCLASS10_0_TBF413496555AC2A272175AA8755DF59A98BCA0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_TBF413496555AC2A272175AA8755DF59A98BCA0FA_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647_H
#define U3CU3EC__DISPLAYCLASS11_0_T8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T790D7F2EB69070025BFD0B937F282076759B4A4B_H
#define U3CU3EC__DISPLAYCLASS12_0_T790D7F2EB69070025BFD0B937F282076759B4A4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T790D7F2EB69070025BFD0B937F282076759B4A4B_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_TDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480_H
#define U3CU3EC__DISPLAYCLASS13_0_TDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_TDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T1B353457EF31AB21155411F660244D4145FCF128_H
#define U3CU3EC__DISPLAYCLASS14_0_T1B353457EF31AB21155411F660244D4145FCF128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T1B353457EF31AB21155411F660244D4145FCF128_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_TCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4_H
#define U3CU3EC__DISPLAYCLASS15_0_TCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_TCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T706FEF5518F61907696FC3A310D73D37CD78B6A0_H
#define U3CU3EC__DISPLAYCLASS16_0_T706FEF5518F61907696FC3A310D73D37CD78B6A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T706FEF5518F61907696FC3A310D73D37CD78B6A0_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_TAE678224AB3EC50CFB472606215A68152D7F5AC1_H
#define U3CU3EC__DISPLAYCLASS17_0_TAE678224AB3EC50CFB472606215A68152D7F5AC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_TAE678224AB3EC50CFB472606215A68152D7F5AC1_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TA273C87FB6E798BAB978138FE380FF6FC9F4F138_H
#define U3CU3EC__DISPLAYCLASS18_0_TA273C87FB6E798BAB978138FE380FF6FC9F4F138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TA273C87FB6E798BAB978138FE380FF6FC9F4F138_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T1794A712D34E25EECD71BFB9CD23543FFC5F872E_H
#define U3CU3EC__DISPLAYCLASS19_0_T1794A712D34E25EECD71BFB9CD23543FFC5F872E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T1794A712D34E25EECD71BFB9CD23543FFC5F872E_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_T9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E_H
#define U3CU3EC__DISPLAYCLASS20_0_T9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_T9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TCDC578ECDF8FE00993AE0D17FF67093DD95E9D57_H
#define U3CU3EC__DISPLAYCLASS21_0_TCDC578ECDF8FE00993AE0D17FF67093DD95E9D57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TCDC578ECDF8FE00993AE0D17FF67093DD95E9D57_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T81A251AB34D6CE62518C247A08DFFE3807D9E0AF_H
#define U3CU3EC__DISPLAYCLASS22_0_T81A251AB34D6CE62518C247A08DFFE3807D9E0AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T81A251AB34D6CE62518C247A08DFFE3807D9E0AF_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T45ED683E687C7262CE823E5F9A9DAEA89874C085_H
#define U3CU3EC__DISPLAYCLASS23_0_T45ED683E687C7262CE823E5F9A9DAEA89874C085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T45ED683E687C7262CE823E5F9A9DAEA89874C085_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_T71F9474AA3C36F97F772BA59C25995156ECD66FC_H
#define U3CU3EC__DISPLAYCLASS24_0_T71F9474AA3C36F97F772BA59C25995156ECD66FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_T71F9474AA3C36F97F772BA59C25995156ECD66FC_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T975863A71A716224C6847B9A338C668DC6314E33_H
#define U3CU3EC__DISPLAYCLASS25_0_T975863A71A716224C6847B9A338C668DC6314E33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T975863A71A716224C6847B9A338C668DC6314E33_H
#ifndef U3CU3EC__DISPLAYCLASS26_0_T2A556C8F1165C48B48129CF97F4540C07A92C246_H
#define U3CU3EC__DISPLAYCLASS26_0_T2A556C8F1165C48B48129CF97F4540C07A92C246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS26_0_T2A556C8F1165C48B48129CF97F4540C07A92C246_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T532D99F0F8DA6E0614982A36F088D1A92EE1A202_H
#define U3CU3EC__DISPLAYCLASS27_0_T532D99F0F8DA6E0614982A36F088D1A92EE1A202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T532D99F0F8DA6E0614982A36F088D1A92EE1A202_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T240F883CE61D32824C0A8AB45434C4814C8191B9_H
#define U3CU3EC__DISPLAYCLASS28_0_T240F883CE61D32824C0A8AB45434C4814C8191B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T240F883CE61D32824C0A8AB45434C4814C8191B9_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#define U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF, ___target_0)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_0() const { return ___target_0; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642_H
#define U3CU3EC__DISPLAYCLASS30_0_T0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_TD33376E7AD1840C55C746AE041973A050EBC1E79_H
#define U3CU3EC__DISPLAYCLASS31_0_TD33376E7AD1840C55C746AE041973A050EBC1E79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_TD33376E7AD1840C55C746AE041973A050EBC1E79_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_TC0F866B0D751B9314266D288A9B0CAAF243044BF_H
#define U3CU3EC__DISPLAYCLASS32_0_TC0F866B0D751B9314266D288A9B0CAAF243044BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_TC0F866B0D751B9314266D288A9B0CAAF243044BF_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T842F7830A363757917266288415109DEB9756187_H
#define U3CU3EC__DISPLAYCLASS33_0_T842F7830A363757917266288415109DEB9756187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::target
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187, ___target_0)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_target_0() const { return ___target_0; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T842F7830A363757917266288415109DEB9756187_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D_H
#define U3CU3EC__DISPLAYCLASS34_0_T36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8_H
#define U3CU3EC__DISPLAYCLASS35_0_TD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T3AA43B8A77F27D4CD69D520B24592030974FC649_H
#define U3CU3EC__DISPLAYCLASS36_0_T3AA43B8A77F27D4CD69D520B24592030974FC649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T3AA43B8A77F27D4CD69D520B24592030974FC649_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#define U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#define U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#define U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#define U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#define U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_TDEEEB22115E6297DC859D85A3AE0A653EE94057A_H
#define U3CU3EC__DISPLAYCLASS9_0_TDEEEB22115E6297DC859D85A3AE0A653EE94057A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_TDEEEB22115E6297DC859D85A3AE0A653EE94057A_H
#ifndef DOTWEENMODULEUNITYVERSION_TD051D14A3CC840A947AF4E411FDA4A3E19E40CCE_H
#define DOTWEENMODULEUNITYVERSION_TD051D14A3CC840A947AF4E411FDA4A3E19E40CCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUnityVersion
struct  DOTweenModuleUnityVersion_tD051D14A3CC840A947AF4E411FDA4A3E19E40CCE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUNITYVERSION_TD051D14A3CC840A947AF4E411FDA4A3E19E40CCE_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943_H
#define U3CU3EC__DISPLAYCLASS8_0_TDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::target
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943, ___target_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_target_0() const { return ___target_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T6760BEDD777B5BEE7F93B1F79679119F3F062BB6_H
#define U3CU3EC__DISPLAYCLASS9_0_T6760BEDD777B5BEE7F93B1F79679119F3F062BB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::target
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6, ___target_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_target_0() const { return ___target_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T6760BEDD777B5BEE7F93B1F79679119F3F062BB6_H
#ifndef DOTWEENMODULEUTILS_TD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_H
#define DOTWEENMODULEUTILS_TD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUtils
struct  DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF  : public RuntimeObject
{
public:

public:
};

struct DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields
{
public:
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;

public:
	inline static int32_t get_offset_of__initialized_0() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields, ____initialized_0)); }
	inline bool get__initialized_0() const { return ____initialized_0; }
	inline bool* get_address_of__initialized_0() { return &____initialized_0; }
	inline void set__initialized_0(bool value)
	{
		____initialized_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUTILS_TD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_H
#ifndef PHYSICS_TFF3A360D9F8EF2286D1343AA0E89E67FC909D59D_H
#define PHYSICS_TFF3A360D9F8EF2286D1343AA0E89E67FC909D59D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUtils_Physics
struct  Physics_tFF3A360D9F8EF2286D1343AA0E89E67FC909D59D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS_TFF3A360D9F8EF2286D1343AA0E89E67FC909D59D_H
#ifndef DOTWEENPROSHORTCUTS_T80A7FD6E3B7F1ADA9725FDB20FCEB1F7354130AD_H
#define DOTWEENPROSHORTCUTS_T80A7FD6E3B7F1ADA9725FDB20FCEB1F7354130AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenProShortcuts
struct  DOTweenProShortcuts_t80A7FD6E3B7F1ADA9725FDB20FCEB1F7354130AD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENPROSHORTCUTS_T80A7FD6E3B7F1ADA9725FDB20FCEB1F7354130AD_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TB18864328E214B7B35F1EFA002FCF1896BA441B9_H
#define U3CU3EC__DISPLAYCLASS1_0_TB18864328E214B7B35F1EFA002FCF1896BA441B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenProShortcuts_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tB18864328E214B7B35F1EFA002FCF1896BA441B9  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenProShortcuts_<>c__DisplayClass1_0::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tB18864328E214B7B35F1EFA002FCF1896BA441B9, ___target_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_0() const { return ___target_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TB18864328E214B7B35F1EFA002FCF1896BA441B9_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15_H
#define U3CU3EC__DISPLAYCLASS2_0_T91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenProShortcuts_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenProShortcuts_<>c__DisplayClass2_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#define CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T819BB0973AFF22766749FF087B8AEFEAF3C2CB7D_H
#ifndef WAITFORCOMPLETION_T7E8309E6BD5D8BA1532973AC6730D960E0FD7A27_H
#define WAITFORCOMPLETION_T7E8309E6BD5D8BA1532973AC6730D960E0FD7A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForCompletion
struct  WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForCompletion::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORCOMPLETION_T7E8309E6BD5D8BA1532973AC6730D960E0FD7A27_H
#ifndef WAITFORELAPSEDLOOPS_T8E663E2313D0C1BBBFE108212DDD75BE81144C83_H
#define WAITFORELAPSEDLOOPS_T8E663E2313D0C1BBBFE108212DDD75BE81144C83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops
struct  WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::elapsedLoops
	int32_t ___elapsedLoops_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_elapsedLoops_1() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83, ___elapsedLoops_1)); }
	inline int32_t get_elapsedLoops_1() const { return ___elapsedLoops_1; }
	inline int32_t* get_address_of_elapsedLoops_1() { return &___elapsedLoops_1; }
	inline void set_elapsedLoops_1(int32_t value)
	{
		___elapsedLoops_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORELAPSEDLOOPS_T8E663E2313D0C1BBBFE108212DDD75BE81144C83_H
#ifndef WAITFORKILL_T12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB_H
#define WAITFORKILL_T12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForKill
struct  WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForKill::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORKILL_T12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB_H
#ifndef WAITFORPOSITION_TC5AD605D856D031196E313FD950FE37FC99E4BC3_H
#define WAITFORPOSITION_TC5AD605D856D031196E313FD950FE37FC99E4BC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForPosition
struct  WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForPosition::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;
	// System.Single DG.Tweening.DOTweenCYInstruction_WaitForPosition::position
	float ___position_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3, ___position_1)); }
	inline float get_position_1() const { return ___position_1; }
	inline float* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(float value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORPOSITION_TC5AD605D856D031196E313FD950FE37FC99E4BC3_H
#ifndef WAITFORREWIND_TA058A6E9E93DE243C93B1183D0ED12C6EAE43229_H
#define WAITFORREWIND_TA058A6E9E93DE243C93B1183D0ED12C6EAE43229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForRewind
struct  WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForRewind::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORREWIND_TA058A6E9E93DE243C93B1183D0ED12C6EAE43229_H
#ifndef WAITFORSTART_T4BCF21589FA389F96FCED9ABE22111121CB5813C_H
#define WAITFORSTART_T4BCF21589FA389F96FCED9ABE22111121CB5813C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction_WaitForStart
struct  WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForStart::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSTART_T4BCF21589FA389F96FCED9ABE22111121CB5813C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ANIMATIONTYPE_T41AAED88D509AA940EDA4C01819A6165731168C1_H
#define ANIMATIONTYPE_T41AAED88D509AA940EDA4C01819A6165731168C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimation_AnimationType
struct  AnimationType_t41AAED88D509AA940EDA4C01819A6165731168C1 
{
public:
	// System.Int32 DG.Tweening.DOTweenAnimation_AnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationType_t41AAED88D509AA940EDA4C01819A6165731168C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTYPE_T41AAED88D509AA940EDA4C01819A6165731168C1_H
#ifndef TARGETTYPE_T038393D21AEF634F48074EADA76ECCF398E25318_H
#define TARGETTYPE_T038393D21AEF634F48074EADA76ECCF398E25318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimation_TargetType
struct  TargetType_t038393D21AEF634F48074EADA76ECCF398E25318 
{
public:
	// System.Int32 DG.Tweening.DOTweenAnimation_TargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetType_t038393D21AEF634F48074EADA76ECCF398E25318, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T038393D21AEF634F48074EADA76ECCF398E25318_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_TC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417_H
#define U3CU3EC__DISPLAYCLASS29_0_TC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::endValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___endValue_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_4), value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___endValue_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___endValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_TC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T7A4101EB47B17C9335CA23259CDC3E76DE2576F7_H
#define U3CU3EC__DISPLAYCLASS37_0_T7A4101EB47B17C9335CA23259CDC3E76DE2576F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7, ___target_1)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_1() const { return ___target_1; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T7A4101EB47B17C9335CA23259CDC3E76DE2576F7_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T22563C87AB743724B1D6E93DCB17C37C84EAF2A6_H
#define U3CU3EC__DISPLAYCLASS38_0_T22563C87AB743724B1D6E93DCB17C37C84EAF2A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6, ___target_1)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_1() const { return ___target_1; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T22563C87AB743724B1D6E93DCB17C37C84EAF2A6_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T0C6FF7340BB553BCC49620701ED13F794930154B_H
#define U3CU3EC__DISPLAYCLASS39_0_T0C6FF7340BB553BCC49620701ED13F794930154B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B, ___target_1)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_1() const { return ___target_1; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T0C6FF7340BB553BCC49620701ED13F794930154B_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#define ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifndef SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#define SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#ifndef UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#define UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#define ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_10;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_11;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onStart_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onPlay_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onUpdate_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onStepComplete_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onComplete_17;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onTweenCreated_18;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onRewind_19;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___tween_20;

public:
	inline static int32_t get_offset_of_updateType_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___updateType_4)); }
	inline int32_t get_updateType_4() const { return ___updateType_4; }
	inline int32_t* get_address_of_updateType_4() { return &___updateType_4; }
	inline void set_updateType_4(int32_t value)
	{
		___updateType_4 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___isSpeedBased_5)); }
	inline bool get_isSpeedBased_5() const { return ___isSpeedBased_5; }
	inline bool* get_address_of_isSpeedBased_5() { return &___isSpeedBased_5; }
	inline void set_isSpeedBased_5(bool value)
	{
		___isSpeedBased_5 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnStart_6)); }
	inline bool get_hasOnStart_6() const { return ___hasOnStart_6; }
	inline bool* get_address_of_hasOnStart_6() { return &___hasOnStart_6; }
	inline void set_hasOnStart_6(bool value)
	{
		___hasOnStart_6 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnPlay_7)); }
	inline bool get_hasOnPlay_7() const { return ___hasOnPlay_7; }
	inline bool* get_address_of_hasOnPlay_7() { return &___hasOnPlay_7; }
	inline void set_hasOnPlay_7(bool value)
	{
		___hasOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnUpdate_8)); }
	inline bool get_hasOnUpdate_8() const { return ___hasOnUpdate_8; }
	inline bool* get_address_of_hasOnUpdate_8() { return &___hasOnUpdate_8; }
	inline void set_hasOnUpdate_8(bool value)
	{
		___hasOnUpdate_8 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnStepComplete_9)); }
	inline bool get_hasOnStepComplete_9() const { return ___hasOnStepComplete_9; }
	inline bool* get_address_of_hasOnStepComplete_9() { return &___hasOnStepComplete_9; }
	inline void set_hasOnStepComplete_9(bool value)
	{
		___hasOnStepComplete_9 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnComplete_10)); }
	inline bool get_hasOnComplete_10() const { return ___hasOnComplete_10; }
	inline bool* get_address_of_hasOnComplete_10() { return &___hasOnComplete_10; }
	inline void set_hasOnComplete_10(bool value)
	{
		___hasOnComplete_10 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnTweenCreated_11)); }
	inline bool get_hasOnTweenCreated_11() const { return ___hasOnTweenCreated_11; }
	inline bool* get_address_of_hasOnTweenCreated_11() { return &___hasOnTweenCreated_11; }
	inline void set_hasOnTweenCreated_11(bool value)
	{
		___hasOnTweenCreated_11 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnRewind_12)); }
	inline bool get_hasOnRewind_12() const { return ___hasOnRewind_12; }
	inline bool* get_address_of_hasOnRewind_12() { return &___hasOnRewind_12; }
	inline void set_hasOnRewind_12(bool value)
	{
		___hasOnRewind_12 = value;
	}

	inline static int32_t get_offset_of_onStart_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onStart_13)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onStart_13() const { return ___onStart_13; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onStart_13() { return &___onStart_13; }
	inline void set_onStart_13(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onStart_13 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_13), value);
	}

	inline static int32_t get_offset_of_onPlay_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onPlay_14)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onPlay_14() const { return ___onPlay_14; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onPlay_14() { return &___onPlay_14; }
	inline void set_onPlay_14(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onPlay_14 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_14), value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onUpdate_15)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onUpdate_15() const { return ___onUpdate_15; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_15), value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onStepComplete_16)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_16), value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onComplete_17)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onComplete_17() const { return ___onComplete_17; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_17), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onTweenCreated_18)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onTweenCreated_18() const { return ___onTweenCreated_18; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onTweenCreated_18() { return &___onTweenCreated_18; }
	inline void set_onTweenCreated_18(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onTweenCreated_18 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_18), value);
	}

	inline static int32_t get_offset_of_onRewind_19() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onRewind_19)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onRewind_19() const { return ___onRewind_19; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onRewind_19() { return &___onRewind_19; }
	inline void set_onRewind_19(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onRewind_19 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_19), value);
	}

	inline static int32_t get_offset_of_tween_20() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___tween_20)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_tween_20() const { return ___tween_20; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_tween_20() { return &___tween_20; }
	inline void set_tween_20(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___tween_20 = value;
		Il2CppCodeGenWriteBarrier((&___tween_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#ifndef DOTWEENANIMATION_TD3653FC0F0806D2747A349461B0F542EAE124B95_H
#define DOTWEENANIMATION_TD3653FC0F0806D2747A349461B0F542EAE124B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimation
struct  DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95  : public ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072
{
public:
	// System.Boolean DG.Tweening.DOTweenAnimation::targetIsSelf
	bool ___targetIsSelf_22;
	// UnityEngine.GameObject DG.Tweening.DOTweenAnimation::targetGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetGO_23;
	// System.Boolean DG.Tweening.DOTweenAnimation::tweenTargetIsTargetGO
	bool ___tweenTargetIsTargetGO_24;
	// System.Single DG.Tweening.DOTweenAnimation::delay
	float ___delay_25;
	// System.Single DG.Tweening.DOTweenAnimation::duration
	float ___duration_26;
	// DG.Tweening.Ease DG.Tweening.DOTweenAnimation::easeType
	int32_t ___easeType_27;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenAnimation::easeCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___easeCurve_28;
	// DG.Tweening.LoopType DG.Tweening.DOTweenAnimation::loopType
	int32_t ___loopType_29;
	// System.Int32 DG.Tweening.DOTweenAnimation::loops
	int32_t ___loops_30;
	// System.String DG.Tweening.DOTweenAnimation::id
	String_t* ___id_31;
	// System.Boolean DG.Tweening.DOTweenAnimation::isRelative
	bool ___isRelative_32;
	// System.Boolean DG.Tweening.DOTweenAnimation::isFrom
	bool ___isFrom_33;
	// System.Boolean DG.Tweening.DOTweenAnimation::isIndependentUpdate
	bool ___isIndependentUpdate_34;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoKill
	bool ___autoKill_35;
	// System.Boolean DG.Tweening.DOTweenAnimation::isActive
	bool ___isActive_36;
	// System.Boolean DG.Tweening.DOTweenAnimation::isValid
	bool ___isValid_37;
	// UnityEngine.Component DG.Tweening.DOTweenAnimation::target
	Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target_38;
	// DG.Tweening.DOTweenAnimation_AnimationType DG.Tweening.DOTweenAnimation::animationType
	int32_t ___animationType_39;
	// DG.Tweening.DOTweenAnimation_TargetType DG.Tweening.DOTweenAnimation::targetType
	int32_t ___targetType_40;
	// DG.Tweening.DOTweenAnimation_TargetType DG.Tweening.DOTweenAnimation::forcedTargetType
	int32_t ___forcedTargetType_41;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoPlay
	bool ___autoPlay_42;
	// System.Boolean DG.Tweening.DOTweenAnimation::useTargetAsV3
	bool ___useTargetAsV3_43;
	// System.Single DG.Tweening.DOTweenAnimation::endValueFloat
	float ___endValueFloat_44;
	// UnityEngine.Vector3 DG.Tweening.DOTweenAnimation::endValueV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endValueV3_45;
	// UnityEngine.Vector2 DG.Tweening.DOTweenAnimation::endValueV2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___endValueV2_46;
	// UnityEngine.Color DG.Tweening.DOTweenAnimation::endValueColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___endValueColor_47;
	// System.String DG.Tweening.DOTweenAnimation::endValueString
	String_t* ___endValueString_48;
	// UnityEngine.Rect DG.Tweening.DOTweenAnimation::endValueRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___endValueRect_49;
	// UnityEngine.Transform DG.Tweening.DOTweenAnimation::endValueTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___endValueTransform_50;
	// System.Boolean DG.Tweening.DOTweenAnimation::optionalBool0
	bool ___optionalBool0_51;
	// System.Single DG.Tweening.DOTweenAnimation::optionalFloat0
	float ___optionalFloat0_52;
	// System.Int32 DG.Tweening.DOTweenAnimation::optionalInt0
	int32_t ___optionalInt0_53;
	// DG.Tweening.RotateMode DG.Tweening.DOTweenAnimation::optionalRotationMode
	int32_t ___optionalRotationMode_54;
	// DG.Tweening.ScrambleMode DG.Tweening.DOTweenAnimation::optionalScrambleMode
	int32_t ___optionalScrambleMode_55;
	// System.String DG.Tweening.DOTweenAnimation::optionalString
	String_t* ___optionalString_56;
	// System.Boolean DG.Tweening.DOTweenAnimation::_tweenCreated
	bool ____tweenCreated_57;
	// System.Int32 DG.Tweening.DOTweenAnimation::_playCount
	int32_t ____playCount_58;

public:
	inline static int32_t get_offset_of_targetIsSelf_22() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___targetIsSelf_22)); }
	inline bool get_targetIsSelf_22() const { return ___targetIsSelf_22; }
	inline bool* get_address_of_targetIsSelf_22() { return &___targetIsSelf_22; }
	inline void set_targetIsSelf_22(bool value)
	{
		___targetIsSelf_22 = value;
	}

	inline static int32_t get_offset_of_targetGO_23() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___targetGO_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetGO_23() const { return ___targetGO_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetGO_23() { return &___targetGO_23; }
	inline void set_targetGO_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetGO_23 = value;
		Il2CppCodeGenWriteBarrier((&___targetGO_23), value);
	}

	inline static int32_t get_offset_of_tweenTargetIsTargetGO_24() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___tweenTargetIsTargetGO_24)); }
	inline bool get_tweenTargetIsTargetGO_24() const { return ___tweenTargetIsTargetGO_24; }
	inline bool* get_address_of_tweenTargetIsTargetGO_24() { return &___tweenTargetIsTargetGO_24; }
	inline void set_tweenTargetIsTargetGO_24(bool value)
	{
		___tweenTargetIsTargetGO_24 = value;
	}

	inline static int32_t get_offset_of_delay_25() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___delay_25)); }
	inline float get_delay_25() const { return ___delay_25; }
	inline float* get_address_of_delay_25() { return &___delay_25; }
	inline void set_delay_25(float value)
	{
		___delay_25 = value;
	}

	inline static int32_t get_offset_of_duration_26() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___duration_26)); }
	inline float get_duration_26() const { return ___duration_26; }
	inline float* get_address_of_duration_26() { return &___duration_26; }
	inline void set_duration_26(float value)
	{
		___duration_26 = value;
	}

	inline static int32_t get_offset_of_easeType_27() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___easeType_27)); }
	inline int32_t get_easeType_27() const { return ___easeType_27; }
	inline int32_t* get_address_of_easeType_27() { return &___easeType_27; }
	inline void set_easeType_27(int32_t value)
	{
		___easeType_27 = value;
	}

	inline static int32_t get_offset_of_easeCurve_28() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___easeCurve_28)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_easeCurve_28() const { return ___easeCurve_28; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_easeCurve_28() { return &___easeCurve_28; }
	inline void set_easeCurve_28(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___easeCurve_28 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_28), value);
	}

	inline static int32_t get_offset_of_loopType_29() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___loopType_29)); }
	inline int32_t get_loopType_29() const { return ___loopType_29; }
	inline int32_t* get_address_of_loopType_29() { return &___loopType_29; }
	inline void set_loopType_29(int32_t value)
	{
		___loopType_29 = value;
	}

	inline static int32_t get_offset_of_loops_30() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___loops_30)); }
	inline int32_t get_loops_30() const { return ___loops_30; }
	inline int32_t* get_address_of_loops_30() { return &___loops_30; }
	inline void set_loops_30(int32_t value)
	{
		___loops_30 = value;
	}

	inline static int32_t get_offset_of_id_31() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___id_31)); }
	inline String_t* get_id_31() const { return ___id_31; }
	inline String_t** get_address_of_id_31() { return &___id_31; }
	inline void set_id_31(String_t* value)
	{
		___id_31 = value;
		Il2CppCodeGenWriteBarrier((&___id_31), value);
	}

	inline static int32_t get_offset_of_isRelative_32() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___isRelative_32)); }
	inline bool get_isRelative_32() const { return ___isRelative_32; }
	inline bool* get_address_of_isRelative_32() { return &___isRelative_32; }
	inline void set_isRelative_32(bool value)
	{
		___isRelative_32 = value;
	}

	inline static int32_t get_offset_of_isFrom_33() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___isFrom_33)); }
	inline bool get_isFrom_33() const { return ___isFrom_33; }
	inline bool* get_address_of_isFrom_33() { return &___isFrom_33; }
	inline void set_isFrom_33(bool value)
	{
		___isFrom_33 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_34() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___isIndependentUpdate_34)); }
	inline bool get_isIndependentUpdate_34() const { return ___isIndependentUpdate_34; }
	inline bool* get_address_of_isIndependentUpdate_34() { return &___isIndependentUpdate_34; }
	inline void set_isIndependentUpdate_34(bool value)
	{
		___isIndependentUpdate_34 = value;
	}

	inline static int32_t get_offset_of_autoKill_35() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___autoKill_35)); }
	inline bool get_autoKill_35() const { return ___autoKill_35; }
	inline bool* get_address_of_autoKill_35() { return &___autoKill_35; }
	inline void set_autoKill_35(bool value)
	{
		___autoKill_35 = value;
	}

	inline static int32_t get_offset_of_isActive_36() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___isActive_36)); }
	inline bool get_isActive_36() const { return ___isActive_36; }
	inline bool* get_address_of_isActive_36() { return &___isActive_36; }
	inline void set_isActive_36(bool value)
	{
		___isActive_36 = value;
	}

	inline static int32_t get_offset_of_isValid_37() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___isValid_37)); }
	inline bool get_isValid_37() const { return ___isValid_37; }
	inline bool* get_address_of_isValid_37() { return &___isValid_37; }
	inline void set_isValid_37(bool value)
	{
		___isValid_37 = value;
	}

	inline static int32_t get_offset_of_target_38() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___target_38)); }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * get_target_38() const { return ___target_38; }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 ** get_address_of_target_38() { return &___target_38; }
	inline void set_target_38(Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * value)
	{
		___target_38 = value;
		Il2CppCodeGenWriteBarrier((&___target_38), value);
	}

	inline static int32_t get_offset_of_animationType_39() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___animationType_39)); }
	inline int32_t get_animationType_39() const { return ___animationType_39; }
	inline int32_t* get_address_of_animationType_39() { return &___animationType_39; }
	inline void set_animationType_39(int32_t value)
	{
		___animationType_39 = value;
	}

	inline static int32_t get_offset_of_targetType_40() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___targetType_40)); }
	inline int32_t get_targetType_40() const { return ___targetType_40; }
	inline int32_t* get_address_of_targetType_40() { return &___targetType_40; }
	inline void set_targetType_40(int32_t value)
	{
		___targetType_40 = value;
	}

	inline static int32_t get_offset_of_forcedTargetType_41() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___forcedTargetType_41)); }
	inline int32_t get_forcedTargetType_41() const { return ___forcedTargetType_41; }
	inline int32_t* get_address_of_forcedTargetType_41() { return &___forcedTargetType_41; }
	inline void set_forcedTargetType_41(int32_t value)
	{
		___forcedTargetType_41 = value;
	}

	inline static int32_t get_offset_of_autoPlay_42() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___autoPlay_42)); }
	inline bool get_autoPlay_42() const { return ___autoPlay_42; }
	inline bool* get_address_of_autoPlay_42() { return &___autoPlay_42; }
	inline void set_autoPlay_42(bool value)
	{
		___autoPlay_42 = value;
	}

	inline static int32_t get_offset_of_useTargetAsV3_43() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___useTargetAsV3_43)); }
	inline bool get_useTargetAsV3_43() const { return ___useTargetAsV3_43; }
	inline bool* get_address_of_useTargetAsV3_43() { return &___useTargetAsV3_43; }
	inline void set_useTargetAsV3_43(bool value)
	{
		___useTargetAsV3_43 = value;
	}

	inline static int32_t get_offset_of_endValueFloat_44() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueFloat_44)); }
	inline float get_endValueFloat_44() const { return ___endValueFloat_44; }
	inline float* get_address_of_endValueFloat_44() { return &___endValueFloat_44; }
	inline void set_endValueFloat_44(float value)
	{
		___endValueFloat_44 = value;
	}

	inline static int32_t get_offset_of_endValueV3_45() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueV3_45)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endValueV3_45() const { return ___endValueV3_45; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endValueV3_45() { return &___endValueV3_45; }
	inline void set_endValueV3_45(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endValueV3_45 = value;
	}

	inline static int32_t get_offset_of_endValueV2_46() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueV2_46)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_endValueV2_46() const { return ___endValueV2_46; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_endValueV2_46() { return &___endValueV2_46; }
	inline void set_endValueV2_46(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___endValueV2_46 = value;
	}

	inline static int32_t get_offset_of_endValueColor_47() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueColor_47)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_endValueColor_47() const { return ___endValueColor_47; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_endValueColor_47() { return &___endValueColor_47; }
	inline void set_endValueColor_47(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___endValueColor_47 = value;
	}

	inline static int32_t get_offset_of_endValueString_48() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueString_48)); }
	inline String_t* get_endValueString_48() const { return ___endValueString_48; }
	inline String_t** get_address_of_endValueString_48() { return &___endValueString_48; }
	inline void set_endValueString_48(String_t* value)
	{
		___endValueString_48 = value;
		Il2CppCodeGenWriteBarrier((&___endValueString_48), value);
	}

	inline static int32_t get_offset_of_endValueRect_49() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueRect_49)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_endValueRect_49() const { return ___endValueRect_49; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_endValueRect_49() { return &___endValueRect_49; }
	inline void set_endValueRect_49(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___endValueRect_49 = value;
	}

	inline static int32_t get_offset_of_endValueTransform_50() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___endValueTransform_50)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_endValueTransform_50() const { return ___endValueTransform_50; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_endValueTransform_50() { return &___endValueTransform_50; }
	inline void set_endValueTransform_50(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___endValueTransform_50 = value;
		Il2CppCodeGenWriteBarrier((&___endValueTransform_50), value);
	}

	inline static int32_t get_offset_of_optionalBool0_51() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalBool0_51)); }
	inline bool get_optionalBool0_51() const { return ___optionalBool0_51; }
	inline bool* get_address_of_optionalBool0_51() { return &___optionalBool0_51; }
	inline void set_optionalBool0_51(bool value)
	{
		___optionalBool0_51 = value;
	}

	inline static int32_t get_offset_of_optionalFloat0_52() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalFloat0_52)); }
	inline float get_optionalFloat0_52() const { return ___optionalFloat0_52; }
	inline float* get_address_of_optionalFloat0_52() { return &___optionalFloat0_52; }
	inline void set_optionalFloat0_52(float value)
	{
		___optionalFloat0_52 = value;
	}

	inline static int32_t get_offset_of_optionalInt0_53() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalInt0_53)); }
	inline int32_t get_optionalInt0_53() const { return ___optionalInt0_53; }
	inline int32_t* get_address_of_optionalInt0_53() { return &___optionalInt0_53; }
	inline void set_optionalInt0_53(int32_t value)
	{
		___optionalInt0_53 = value;
	}

	inline static int32_t get_offset_of_optionalRotationMode_54() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalRotationMode_54)); }
	inline int32_t get_optionalRotationMode_54() const { return ___optionalRotationMode_54; }
	inline int32_t* get_address_of_optionalRotationMode_54() { return &___optionalRotationMode_54; }
	inline void set_optionalRotationMode_54(int32_t value)
	{
		___optionalRotationMode_54 = value;
	}

	inline static int32_t get_offset_of_optionalScrambleMode_55() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalScrambleMode_55)); }
	inline int32_t get_optionalScrambleMode_55() const { return ___optionalScrambleMode_55; }
	inline int32_t* get_address_of_optionalScrambleMode_55() { return &___optionalScrambleMode_55; }
	inline void set_optionalScrambleMode_55(int32_t value)
	{
		___optionalScrambleMode_55 = value;
	}

	inline static int32_t get_offset_of_optionalString_56() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ___optionalString_56)); }
	inline String_t* get_optionalString_56() const { return ___optionalString_56; }
	inline String_t** get_address_of_optionalString_56() { return &___optionalString_56; }
	inline void set_optionalString_56(String_t* value)
	{
		___optionalString_56 = value;
		Il2CppCodeGenWriteBarrier((&___optionalString_56), value);
	}

	inline static int32_t get_offset_of__tweenCreated_57() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ____tweenCreated_57)); }
	inline bool get__tweenCreated_57() const { return ____tweenCreated_57; }
	inline bool* get_address_of__tweenCreated_57() { return &____tweenCreated_57; }
	inline void set__tweenCreated_57(bool value)
	{
		____tweenCreated_57 = value;
	}

	inline static int32_t get_offset_of__playCount_58() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95, ____playCount_58)); }
	inline int32_t get__playCount_58() const { return ____playCount_58; }
	inline int32_t* get_address_of__playCount_58() { return &____playCount_58; }
	inline void set__playCount_58(int32_t value)
	{
		____playCount_58 = value;
	}
};

struct DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95_StaticFields
{
public:
	// System.Action`1<DG.Tweening.DOTweenAnimation> DG.Tweening.DOTweenAnimation::OnReset
	Action_1_t1148FC14E6CA6B1B3A1A9D62351118D36E9A0A88 * ___OnReset_21;

public:
	inline static int32_t get_offset_of_OnReset_21() { return static_cast<int32_t>(offsetof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95_StaticFields, ___OnReset_21)); }
	inline Action_1_t1148FC14E6CA6B1B3A1A9D62351118D36E9A0A88 * get_OnReset_21() const { return ___OnReset_21; }
	inline Action_1_t1148FC14E6CA6B1B3A1A9D62351118D36E9A0A88 ** get_address_of_OnReset_21() { return &___OnReset_21; }
	inline void set_OnReset_21(Action_1_t1148FC14E6CA6B1B3A1A9D62351118D36E9A0A88 * value)
	{
		___OnReset_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATION_TD3653FC0F0806D2747A349461B0F542EAE124B95_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4400[1] = 
{
	U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4401[1] = 
{
	U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4403[1] = 
{
	U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4404[1] = 
{
	U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[1] = 
{
	U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[1] = 
{
	U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4407[1] = 
{
	U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4408[1] = 
{
	U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[1] = 
{
	U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4410[1] = 
{
	U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[1] = 
{
	U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4412[1] = 
{
	U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4413[1] = 
{
	U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4414[1] = 
{
	U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[1] = 
{
	U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4416[1] = 
{
	U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4417[1] = 
{
	U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4418[1] = 
{
	U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4419[1] = 
{
	U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4420[1] = 
{
	U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[1] = 
{
	U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4422[1] = 
{
	U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4423[1] = 
{
	U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4424[1] = 
{
	U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4425[1] = 
{
	U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4426[6] = 
{
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_endValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4427[1] = 
{
	U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4428[1] = 
{
	U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4429[1] = 
{
	U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4430[1] = 
{
	U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[1] = 
{
	U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4432[1] = 
{
	U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4433[1] = 
{
	U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4434[2] = 
{
	U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4435[2] = 
{
	U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4436[2] = 
{
	U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (DOTweenModuleUnityVersion_tD051D14A3CC840A947AF4E411FDA4A3E19E40CCE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4438[2] = 
{
	U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943::get_offset_of_propertyID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4439[2] = 
{
	U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6::get_offset_of_propertyID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (DOTweenCYInstruction_t99180052E9B15DB8365A25440477F7BE9B4D6847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4441[1] = 
{
	WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4442[1] = 
{
	WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4443[1] = 
{
	WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4444[2] = 
{
	WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83::get_offset_of_t_0(),
	WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83::get_offset_of_elapsedLoops_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4445[2] = 
{
	WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3::get_offset_of_t_0(),
	WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4446[1] = 
{
	WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF), -1, sizeof(DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4447[1] = 
{
	DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields::get_offset_of__initialized_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (Physics_tFF3A360D9F8EF2286D1343AA0E89E67FC909D59D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95), -1, sizeof(DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4449[38] = 
{
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95_StaticFields::get_offset_of_OnReset_21(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_targetIsSelf_22(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_targetGO_23(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_tweenTargetIsTargetGO_24(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_delay_25(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_duration_26(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_easeType_27(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_easeCurve_28(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_loopType_29(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_loops_30(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_id_31(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_isRelative_32(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_isFrom_33(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_isIndependentUpdate_34(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_autoKill_35(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_isActive_36(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_isValid_37(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_target_38(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_animationType_39(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_targetType_40(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_forcedTargetType_41(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_autoPlay_42(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_useTargetAsV3_43(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueFloat_44(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueV3_45(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueV2_46(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueColor_47(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueString_48(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueRect_49(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_endValueTransform_50(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalBool0_51(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalFloat0_52(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalInt0_53(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalRotationMode_54(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalScrambleMode_55(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of_optionalString_56(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of__tweenCreated_57(),
	DOTweenAnimation_tD3653FC0F0806D2747A349461B0F542EAE124B95::get_offset_of__playCount_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (AnimationType_t41AAED88D509AA940EDA4C01819A6165731168C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4450[23] = 
{
	AnimationType_t41AAED88D509AA940EDA4C01819A6165731168C1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (TargetType_t038393D21AEF634F48074EADA76ECCF398E25318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4451[17] = 
{
	TargetType_t038393D21AEF634F48074EADA76ECCF398E25318::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (DOTweenAnimationExtensions_tED069AD902CB6735F3C41CD13A524CA7995C653F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (DOTweenProShortcuts_t80A7FD6E3B7F1ADA9725FDB20FCEB1F7354130AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (U3CU3Ec__DisplayClass1_0_tB18864328E214B7B35F1EFA002FCF1896BA441B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4454[1] = 
{
	U3CU3Ec__DisplayClass1_0_tB18864328E214B7B35F1EFA002FCF1896BA441B9::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (U3CU3Ec__DisplayClass2_0_t91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4455[1] = 
{
	U3CU3Ec__DisplayClass2_0_t91C9965ED74E98BFB50C8AF4BE3EA3DC4FC63E15::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
