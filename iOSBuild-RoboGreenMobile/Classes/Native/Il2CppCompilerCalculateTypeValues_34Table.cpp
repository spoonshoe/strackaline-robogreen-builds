﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>
struct Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0;
// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo>
struct Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<Vectrosity.RefInt>
struct List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE;
// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.FastAction
struct FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_tEB3C45F52B2EC36790A5BEB17A3563EF497F1D08;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_tD6EBFF8BA607B0FB200C2D1B553C6996E6B000D2;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t6D80255BE2AA33BD665B5DD299ED2F2CE5C83EC3;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t3A154598FF553EEC995DE302C61F04B77806BF06;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_t27D00FF28C58999C4836111F8FA4A90945F7B08E;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_t155798842065133E8DD5F993A46F77466E927C37;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_tC5DF0CADB1D1176578E8F64E3E6B36835DCC1499;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652;
// TMPro.TMP_Character
struct TMP_Character_t1875AACA978396521498D6A699052C187903553D;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.TextCore.Glyph
struct Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector2[][]
struct Vector2U5BU5DU5BU5D_t41709A351740BF974538A8E22E3C6E80FD767554;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// Vectrosity.IVectorObject
struct IVectorObject_t6A320644FF8ACBE266FD9BA1E8D31DED52FAE201;
// Vectrosity.LineManager
struct LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776;
// Vectrosity.RefInt
struct RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2;
// Vectrosity.VectorLine
struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F;
// Vectrosity.VisibilityControl
struct VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA;
// Vectrosity.VisibilityControlStatic
struct VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef U3CMODULEU3E_TDBDF5538A0E6A38DC8C394D40F51A58BB8ADEC1F_H
#define U3CMODULEU3E_TDBDF5538A0E6A38DC8C394D40F51A58BB8ADEC1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tDBDF5538A0E6A38DC8C394D40F51A58BB8ADEC1F 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TDBDF5538A0E6A38DC8C394D40F51A58BB8ADEC1F_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#define TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t2549189919276EB833F83EA937AB992420E1B199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifndef SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#define SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject
struct  SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker_SpriteData> TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject::frames
	List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E, ___frames_0)); }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * get_frames_0() const { return ___frames_0; }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifndef TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#define TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement_Legacy
struct  TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement_Legacy::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement_Legacy::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement_Legacy::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement_Legacy::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement_Legacy::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement_Legacy::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement_Legacy::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement_Legacy::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement_Legacy::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifndef TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#define TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextParsingUtilities
struct  TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35  : public RuntimeObject
{
public:

public:
};

struct TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields
{
public:
	// TMPro.TMP_TextParsingUtilities TMPro.TMP_TextParsingUtilities::s_Instance
	TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields, ___s_Instance_0)); }
	inline TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#ifndef TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#define TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#ifndef TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#define TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_GraphicQueueLookup_4;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_InternalUpdateQueue
	List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * ___m_InternalUpdateQueue_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_InternalUpdateLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_InternalUpdateLookup_6;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}

	inline static int32_t get_offset_of_m_InternalUpdateQueue_5() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_InternalUpdateQueue_5)); }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * get_m_InternalUpdateQueue_5() const { return ___m_InternalUpdateQueue_5; }
	inline List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E ** get_address_of_m_InternalUpdateQueue_5() { return &___m_InternalUpdateQueue_5; }
	inline void set_m_InternalUpdateQueue_5(List_1_t55CBC57B3D32750DF17AC1ECCF2AD4A569704E0E * value)
	{
		___m_InternalUpdateQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalUpdateQueue_5), value);
	}

	inline static int32_t get_offset_of_m_InternalUpdateLookup_6() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_InternalUpdateLookup_6)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_InternalUpdateLookup_6() const { return ___m_InternalUpdateLookup_6; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_InternalUpdateLookup_6() { return &___m_InternalUpdateLookup_6; }
	inline void set_m_InternalUpdateLookup_6(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_InternalUpdateLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalUpdateLookup_6), value);
	}
};

struct TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#ifndef TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#define TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_LayoutRebuildQueue_1)); }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_GraphicRebuildQueue_3)); }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#ifndef TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#define TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_tC5DF0CADB1D1176578E8F64E3E6B36835DCC1499 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_t27D00FF28C58999C4836111F8FA4A90945F7B08E * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_tD6EBFF8BA607B0FB200C2D1B553C6996E6B000D2 * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_t155798842065133E8DD5F993A46F77466E927C37 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t6D80255BE2AA33BD665B5DD299ED2F2CE5C83EC3 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_tEB3C45F52B2EC36790A5BEB17A3563EF497F1D08 * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction TMPro.TMPro_EventManager::RESOURCE_LOAD_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___RESOURCE_LOAD_EVENT_9;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t3A154598FF553EEC995DE302C61F04B77806BF06 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___OnPreRenderObject_Event_11;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * ___TEXT_CHANGED_EVENT_12;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_tC5DF0CADB1D1176578E8F64E3E6B36835DCC1499 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_tC5DF0CADB1D1176578E8F64E3E6B36835DCC1499 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_tC5DF0CADB1D1176578E8F64E3E6B36835DCC1499 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_t27D00FF28C58999C4836111F8FA4A90945F7B08E * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_t27D00FF28C58999C4836111F8FA4A90945F7B08E ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_t27D00FF28C58999C4836111F8FA4A90945F7B08E * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_tD6EBFF8BA607B0FB200C2D1B553C6996E6B000D2 * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_tD6EBFF8BA607B0FB200C2D1B553C6996E6B000D2 ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_tD6EBFF8BA607B0FB200C2D1B553C6996E6B000D2 * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_t155798842065133E8DD5F993A46F77466E927C37 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_t155798842065133E8DD5F993A46F77466E927C37 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_t155798842065133E8DD5F993A46F77466E927C37 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t6D80255BE2AA33BD665B5DD299ED2F2CE5C83EC3 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t6D80255BE2AA33BD665B5DD299ED2F2CE5C83EC3 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t6D80255BE2AA33BD665B5DD299ED2F2CE5C83EC3 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_tEB3C45F52B2EC36790A5BEB17A3563EF497F1D08 * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_tEB3C45F52B2EC36790A5BEB17A3563EF497F1D08 ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_tEB3C45F52B2EC36790A5BEB17A3563EF497F1D08 * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_RESOURCE_LOAD_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___RESOURCE_LOAD_EVENT_9)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_RESOURCE_LOAD_EVENT_9() const { return ___RESOURCE_LOAD_EVENT_9; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_RESOURCE_LOAD_EVENT_9() { return &___RESOURCE_LOAD_EVENT_9; }
	inline void set_RESOURCE_LOAD_EVENT_9(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___RESOURCE_LOAD_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___RESOURCE_LOAD_EVENT_9), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10)); }
	inline FastAction_2_t3A154598FF553EEC995DE302C61F04B77806BF06 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline FastAction_2_t3A154598FF553EEC995DE302C61F04B77806BF06 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(FastAction_2_t3A154598FF553EEC995DE302C61F04B77806BF06 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___OnPreRenderObject_Event_11)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_OnPreRenderObject_Event_11() const { return ___OnPreRenderObject_Event_11; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_OnPreRenderObject_Event_11() { return &___OnPreRenderObject_Event_11; }
	inline void set_OnPreRenderObject_Event_11(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___OnPreRenderObject_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_11), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_12() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_CHANGED_EVENT_12)); }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * get_TEXT_CHANGED_EVENT_12() const { return ___TEXT_CHANGED_EVENT_12; }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 ** get_address_of_TEXT_CHANGED_EVENT_12() { return &___TEXT_CHANGED_EVENT_12; }
	inline void set_TEXT_CHANGED_EVENT_12(FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * value)
	{
		___TEXT_CHANGED_EVENT_12 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifndef TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#define TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifndef U3CDISABLELINEU3EC__ITERATOR0_T06E7DE41DE23F7600A9345366B2490E01E011B39_H
#define U3CDISABLELINEU3EC__ITERATOR0_T06E7DE41DE23F7600A9345366B2490E01E011B39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.LineManager_<DisableLine>c__Iterator0
struct  U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39  : public RuntimeObject
{
public:
	// System.Single Vectrosity.LineManager_<DisableLine>c__Iterator0::time
	float ___time_0;
	// System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::remove
	bool ___remove_1;
	// Vectrosity.VectorLine Vectrosity.LineManager_<DisableLine>c__Iterator0::vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___vectorLine_2;
	// System.Int32 Vectrosity.LineManager_<DisableLine>c__Iterator0::U24PC
	int32_t ___U24PC_3;
	// System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::U24current
	RuntimeObject * ___U24current_4;
	// System.Single Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>time
	float ___U3CU24U3Etime_5;
	// System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>remove
	bool ___U3CU24U3Eremove_6;
	// Vectrosity.VectorLine Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___U3CU24U3EvectorLine_7;
	// Vectrosity.LineManager Vectrosity.LineManager_<DisableLine>c__Iterator0::<>f__this
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_remove_1() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___remove_1)); }
	inline bool get_remove_1() const { return ___remove_1; }
	inline bool* get_address_of_remove_1() { return &___remove_1; }
	inline void set_remove_1(bool value)
	{
		___remove_1 = value;
	}

	inline static int32_t get_offset_of_vectorLine_2() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___vectorLine_2)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_vectorLine_2() const { return ___vectorLine_2; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_vectorLine_2() { return &___vectorLine_2; }
	inline void set_vectorLine_2(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___vectorLine_2 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLine_2), value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etime_5() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U3CU24U3Etime_5)); }
	inline float get_U3CU24U3Etime_5() const { return ___U3CU24U3Etime_5; }
	inline float* get_address_of_U3CU24U3Etime_5() { return &___U3CU24U3Etime_5; }
	inline void set_U3CU24U3Etime_5(float value)
	{
		___U3CU24U3Etime_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eremove_6() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U3CU24U3Eremove_6)); }
	inline bool get_U3CU24U3Eremove_6() const { return ___U3CU24U3Eremove_6; }
	inline bool* get_address_of_U3CU24U3Eremove_6() { return &___U3CU24U3Eremove_6; }
	inline void set_U3CU24U3Eremove_6(bool value)
	{
		___U3CU24U3Eremove_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EvectorLine_7() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U3CU24U3EvectorLine_7)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_U3CU24U3EvectorLine_7() const { return ___U3CU24U3EvectorLine_7; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_U3CU24U3EvectorLine_7() { return &___U3CU24U3EvectorLine_7; }
	inline void set_U3CU24U3EvectorLine_7(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___U3CU24U3EvectorLine_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3EvectorLine_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39, ___U3CU3Ef__this_8)); }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISABLELINEU3EC__ITERATOR0_T06E7DE41DE23F7600A9345366B2490E01E011B39_H
#ifndef REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#define REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.RefInt
struct  RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.RefInt::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFINT_T48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2_H
#ifndef VECTORCHAR_T795E92F6B0AAB4627A4E348B6022EE07124C2F3C_H
#define VECTORCHAR_T795E92F6B0AAB4627A4E348B6022EE07124C2F3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorChar
struct  VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C  : public RuntimeObject
{
public:

public:
};

struct VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C_StaticFields
{
public:
	// UnityEngine.Vector2[][] Vectrosity.VectorChar::points
	Vector2U5BU5DU5BU5D_t41709A351740BF974538A8E22E3C6E80FD767554* ___points_0;

public:
	inline static int32_t get_offset_of_points_0() { return static_cast<int32_t>(offsetof(VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C_StaticFields, ___points_0)); }
	inline Vector2U5BU5DU5BU5D_t41709A351740BF974538A8E22E3C6E80FD767554* get_points_0() const { return ___points_0; }
	inline Vector2U5BU5DU5BU5D_t41709A351740BF974538A8E22E3C6E80FD767554** get_address_of_points_0() { return &___points_0; }
	inline void set_points_0(Vector2U5BU5DU5BU5D_t41709A351740BF974538A8E22E3C6E80FD767554* value)
	{
		___points_0 = value;
		Il2CppCodeGenWriteBarrier((&___points_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORCHAR_T795E92F6B0AAB4627A4E348B6022EE07124C2F3C_H
#ifndef U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#define U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3
struct  U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBECAMEINVISIBLEU3EC__ITERATOR3_T5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73_H
#ifndef U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#define U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2
struct  U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBECAMEVISIBLEU3EC__ITERATOR2_T6B38B859F4467015CB65FC0839CB8136E48C1403_H
#ifndef U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#define U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1
struct  U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::<>f__this
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVISIBILITYTESTU3EC__ITERATOR1_T23C7897B9619831180B98866CDF6C0E00B31EE48_H
#ifndef U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#define U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4
struct  U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControlStatic Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::<>f__this
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7, ___U3CU3Ef__this_2)); }
	inline VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITCHECKU3EC__ITERATOR4_T1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#define __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#define __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#define SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame
struct  SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifndef SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#define SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize
struct  SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifndef TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#define TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontStyleStack
struct  TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifndef TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#define TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkID_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifndef TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#define TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#define TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Int32>
struct  TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_ItemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#define TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Single>
struct  TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	float ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_ItemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_DefaultItem_3)); }
	inline float get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline float* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(float value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#define TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_ItemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_DefaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_DefaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifndef TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#define TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifndef UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#define UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_UnicodeChar
struct  UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A 
{
public:
	// System.Int32 TMPro.TMP_Text_UnicodeChar::unicode
	int32_t ___unicode_0;
	// System.Int32 TMPro.TMP_Text_UnicodeChar::stringIndex
	int32_t ___stringIndex_1;
	// System.Int32 TMPro.TMP_Text_UnicodeChar::length
	int32_t ___length_2;

public:
	inline static int32_t get_offset_of_unicode_0() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___unicode_0)); }
	inline int32_t get_unicode_0() const { return ___unicode_0; }
	inline int32_t* get_address_of_unicode_0() { return &___unicode_0; }
	inline void set_unicode_0(int32_t value)
	{
		___unicode_0 = value;
	}

	inline static int32_t get_offset_of_stringIndex_1() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___stringIndex_1)); }
	inline int32_t get_stringIndex_1() const { return ___stringIndex_1; }
	inline int32_t* get_address_of_stringIndex_1() { return &___stringIndex_1; }
	inline void set_stringIndex_1(int32_t value)
	{
		___stringIndex_1 = value;
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#ifndef TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#define TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifndef TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#define TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#define CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#ifndef COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#define COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorMode
struct  ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#define FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeight
struct  FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifndef MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#define MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#ifndef MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#define MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifndef SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#define SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifndef SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#define SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteData
struct  SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::frame
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::spriteSourceSize
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::sourceSize
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___frame_1)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___spriteSourceSize_4)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___sourceSize_5)); }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___pivot_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
#endif // SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifndef TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#define TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#define TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference>
struct  TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_ItemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_DefaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#define TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32>
struct  TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_ItemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_DefaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifndef TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#define TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_TextInputSources
struct  TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4 
{
public:
	// System.Int32 TMPro.TMP_Text_TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#define TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___textComponent_2)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#ifndef LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#define LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities_LineSegment
struct  LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities_LineSegment::Point1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities_LineSegment::Point2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D, ___Point1_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D, ___Point2_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#define TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifndef TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#define TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnitType
struct  TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF 
{
public:
	// System.Int32 TMPro.TagUnitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifndef TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#define TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagValueType
struct  TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC 
{
public:
	// System.Int32 TMPro.TagValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#define TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextElementType
struct  TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857 
{
public:
	// System.Byte TMPro.TextElementType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifndef TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#define TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifndef TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#define TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifndef TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#define TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#define VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#define BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Brightness
struct  Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4 
{
public:
	// System.Int32 Vectrosity.Brightness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESS_TDF7412E025A9E23469DD70EDE4EBF92CBACD56D4_H
#ifndef CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#define CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.CanvasState
struct  CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6 
{
public:
	// System.Int32 Vectrosity.CanvasState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSTATE_T9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6_H
#ifndef ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#define ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.EndCap
struct  EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F 
{
public:
	// System.Int32 Vectrosity.EndCap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDCAP_TDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F_H
#ifndef JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#define JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Joins
struct  Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F 
{
public:
	// System.Int32 Vectrosity.Joins::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINS_T10F2955C021100EC84153BE7EDEDBA60219FB60F_H
#ifndef LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#define LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.LineType
struct  LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1 
{
public:
	// System.Int32 Vectrosity.LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T73EEB07AD123A0E80FC96AB022A611709FCA7FA1_H
#ifndef FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#define FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine_FunctionName
struct  FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9 
{
public:
	// System.Int32 Vectrosity.VectorLine_FunctionName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONNAME_T3289981332E983EA64C4A1631FBF9146BC2570D9_H
#ifndef VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#define VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorManager
struct  VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993  : public RuntimeObject
{
public:

public:
};

struct VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields
{
public:
	// System.Single Vectrosity.VectorManager::minBrightnessDistance
	float ___minBrightnessDistance_0;
	// System.Single Vectrosity.VectorManager::maxBrightnessDistance
	float ___maxBrightnessDistance_1;
	// System.Int32 Vectrosity.VectorManager::brightnessLevels
	int32_t ___brightnessLevels_2;
	// System.Single Vectrosity.VectorManager::distanceCheckFrequency
	float ___distanceCheckFrequency_3;
	// UnityEngine.Color Vectrosity.VectorManager::fogColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___fogColor_4;
	// System.Boolean Vectrosity.VectorManager::useDraw3D
	bool ___useDraw3D_5;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines_6;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers_7;
	// System.Int32 Vectrosity.VectorManager::_arrayCount
	int32_t ____arrayCount_8;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines2
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines2_9;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers2
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers2_10;
	// System.Int32 Vectrosity.VectorManager::_arrayCount2
	int32_t ____arrayCount2_11;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Vectrosity.VectorManager::transforms3
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___transforms3_12;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines3
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___vectorLines3_13;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorManager::oldDistances
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___oldDistances_14;
	// System.Collections.Generic.List`1<UnityEngine.Color> Vectrosity.VectorManager::colors
	List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * ___colors_15;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers3
	List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * ___objectNumbers3_16;
	// System.Int32 Vectrosity.VectorManager::_arrayCount3
	int32_t ____arrayCount3_17;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh> Vectrosity.VectorManager::meshTable
	Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * ___meshTable_18;

public:
	inline static int32_t get_offset_of_minBrightnessDistance_0() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___minBrightnessDistance_0)); }
	inline float get_minBrightnessDistance_0() const { return ___minBrightnessDistance_0; }
	inline float* get_address_of_minBrightnessDistance_0() { return &___minBrightnessDistance_0; }
	inline void set_minBrightnessDistance_0(float value)
	{
		___minBrightnessDistance_0 = value;
	}

	inline static int32_t get_offset_of_maxBrightnessDistance_1() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___maxBrightnessDistance_1)); }
	inline float get_maxBrightnessDistance_1() const { return ___maxBrightnessDistance_1; }
	inline float* get_address_of_maxBrightnessDistance_1() { return &___maxBrightnessDistance_1; }
	inline void set_maxBrightnessDistance_1(float value)
	{
		___maxBrightnessDistance_1 = value;
	}

	inline static int32_t get_offset_of_brightnessLevels_2() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___brightnessLevels_2)); }
	inline int32_t get_brightnessLevels_2() const { return ___brightnessLevels_2; }
	inline int32_t* get_address_of_brightnessLevels_2() { return &___brightnessLevels_2; }
	inline void set_brightnessLevels_2(int32_t value)
	{
		___brightnessLevels_2 = value;
	}

	inline static int32_t get_offset_of_distanceCheckFrequency_3() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___distanceCheckFrequency_3)); }
	inline float get_distanceCheckFrequency_3() const { return ___distanceCheckFrequency_3; }
	inline float* get_address_of_distanceCheckFrequency_3() { return &___distanceCheckFrequency_3; }
	inline void set_distanceCheckFrequency_3(float value)
	{
		___distanceCheckFrequency_3 = value;
	}

	inline static int32_t get_offset_of_fogColor_4() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___fogColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_fogColor_4() const { return ___fogColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_fogColor_4() { return &___fogColor_4; }
	inline void set_fogColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___fogColor_4 = value;
	}

	inline static int32_t get_offset_of_useDraw3D_5() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___useDraw3D_5)); }
	inline bool get_useDraw3D_5() const { return ___useDraw3D_5; }
	inline bool* get_address_of_useDraw3D_5() { return &___useDraw3D_5; }
	inline void set_useDraw3D_5(bool value)
	{
		___useDraw3D_5 = value;
	}

	inline static int32_t get_offset_of_vectorLines_6() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines_6)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines_6() const { return ___vectorLines_6; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines_6() { return &___vectorLines_6; }
	inline void set_vectorLines_6(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines_6 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines_6), value);
	}

	inline static int32_t get_offset_of_objectNumbers_7() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers_7)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers_7() const { return ___objectNumbers_7; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers_7() { return &___objectNumbers_7; }
	inline void set_objectNumbers_7(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers_7 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers_7), value);
	}

	inline static int32_t get_offset_of__arrayCount_8() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount_8)); }
	inline int32_t get__arrayCount_8() const { return ____arrayCount_8; }
	inline int32_t* get_address_of__arrayCount_8() { return &____arrayCount_8; }
	inline void set__arrayCount_8(int32_t value)
	{
		____arrayCount_8 = value;
	}

	inline static int32_t get_offset_of_vectorLines2_9() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines2_9)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines2_9() const { return ___vectorLines2_9; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines2_9() { return &___vectorLines2_9; }
	inline void set_vectorLines2_9(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines2_9 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines2_9), value);
	}

	inline static int32_t get_offset_of_objectNumbers2_10() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers2_10)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers2_10() const { return ___objectNumbers2_10; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers2_10() { return &___objectNumbers2_10; }
	inline void set_objectNumbers2_10(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers2_10 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers2_10), value);
	}

	inline static int32_t get_offset_of__arrayCount2_11() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount2_11)); }
	inline int32_t get__arrayCount2_11() const { return ____arrayCount2_11; }
	inline int32_t* get_address_of__arrayCount2_11() { return &____arrayCount2_11; }
	inline void set__arrayCount2_11(int32_t value)
	{
		____arrayCount2_11 = value;
	}

	inline static int32_t get_offset_of_transforms3_12() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___transforms3_12)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_transforms3_12() const { return ___transforms3_12; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_transforms3_12() { return &___transforms3_12; }
	inline void set_transforms3_12(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___transforms3_12 = value;
		Il2CppCodeGenWriteBarrier((&___transforms3_12), value);
	}

	inline static int32_t get_offset_of_vectorLines3_13() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___vectorLines3_13)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_vectorLines3_13() const { return ___vectorLines3_13; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_vectorLines3_13() { return &___vectorLines3_13; }
	inline void set_vectorLines3_13(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___vectorLines3_13 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLines3_13), value);
	}

	inline static int32_t get_offset_of_oldDistances_14() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___oldDistances_14)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_oldDistances_14() const { return ___oldDistances_14; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_oldDistances_14() { return &___oldDistances_14; }
	inline void set_oldDistances_14(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___oldDistances_14 = value;
		Il2CppCodeGenWriteBarrier((&___oldDistances_14), value);
	}

	inline static int32_t get_offset_of_colors_15() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___colors_15)); }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * get_colors_15() const { return ___colors_15; }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E ** get_address_of_colors_15() { return &___colors_15; }
	inline void set_colors_15(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * value)
	{
		___colors_15 = value;
		Il2CppCodeGenWriteBarrier((&___colors_15), value);
	}

	inline static int32_t get_offset_of_objectNumbers3_16() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___objectNumbers3_16)); }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * get_objectNumbers3_16() const { return ___objectNumbers3_16; }
	inline List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE ** get_address_of_objectNumbers3_16() { return &___objectNumbers3_16; }
	inline void set_objectNumbers3_16(List_1_t5FE96BDE147F766FE8ACB5B85EB2C6A3F9A5ECDE * value)
	{
		___objectNumbers3_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectNumbers3_16), value);
	}

	inline static int32_t get_offset_of__arrayCount3_17() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ____arrayCount3_17)); }
	inline int32_t get__arrayCount3_17() const { return ____arrayCount3_17; }
	inline int32_t* get_address_of__arrayCount3_17() { return &____arrayCount3_17; }
	inline void set__arrayCount3_17(int32_t value)
	{
		____arrayCount3_17 = value;
	}

	inline static int32_t get_offset_of_meshTable_18() { return static_cast<int32_t>(offsetof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields, ___meshTable_18)); }
	inline Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * get_meshTable_18() const { return ___meshTable_18; }
	inline Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 ** get_address_of_meshTable_18() { return &___meshTable_18; }
	inline void set_meshTable_18(Dictionary_2_tE1F10B46B17B98B8216962ABCCB2327B0D5278B0 * value)
	{
		___meshTable_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshTable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORMANAGER_TE2B75B0CD71CA1D1344A4C606D667F55E0706993_H
#ifndef VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#define VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Visibility
struct  Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174 
{
public:
	// System.Int32 Vectrosity.Visibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITY_TFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174_H
#ifndef CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#define CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#ifndef COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#define COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___Colors_2)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifndef RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#define RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.RichTextTagAttribute
struct  RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 
{
public:
	// System.Int32 TMPro.RichTextTagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 TMPro.RichTextTagAttribute::valueHashCode
	int32_t ___valueHashCode_1;
	// TMPro.TagValueType TMPro.RichTextTagAttribute::valueType
	int32_t ___valueType_2;
	// System.Int32 TMPro.RichTextTagAttribute::valueStartIndex
	int32_t ___valueStartIndex_3;
	// System.Int32 TMPro.RichTextTagAttribute::valueLength
	int32_t ___valueLength_4;
	// TMPro.TagUnitType TMPro.RichTextTagAttribute::unitType
	int32_t ___unitType_5;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_1() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueHashCode_1)); }
	inline int32_t get_valueHashCode_1() const { return ___valueHashCode_1; }
	inline int32_t* get_address_of_valueHashCode_1() { return &___valueHashCode_1; }
	inline void set_valueHashCode_1(int32_t value)
	{
		___valueHashCode_1 = value;
	}

	inline static int32_t get_offset_of_valueType_2() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueType_2)); }
	inline int32_t get_valueType_2() const { return ___valueType_2; }
	inline int32_t* get_address_of_valueType_2() { return &___valueType_2; }
	inline void set_valueType_2(int32_t value)
	{
		___valueType_2 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_3() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueStartIndex_3)); }
	inline int32_t get_valueStartIndex_3() const { return ___valueStartIndex_3; }
	inline int32_t* get_address_of_valueStartIndex_3() { return &___valueStartIndex_3; }
	inline void set_valueStartIndex_3(int32_t value)
	{
		___valueStartIndex_3 = value;
	}

	inline static int32_t get_offset_of_valueLength_4() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueLength_4)); }
	inline int32_t get_valueLength_4() const { return ___valueLength_4; }
	inline int32_t* get_address_of_valueLength_4() { return &___valueLength_4; }
	inline void set_valueLength_4(int32_t value)
	{
		___valueLength_4 = value;
	}

	inline static int32_t get_offset_of_unitType_5() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___unitType_5)); }
	inline int32_t get_unitType_5() const { return ___unitType_5; }
	inline int32_t* get_address_of_unitType_5() { return &___unitType_5; }
	inline void set_unitType_5(int32_t value)
	{
		___unitType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#define TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight>
struct  TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_ItemStack_0)); }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#define TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_ItemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifndef TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#define TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344  : public RuntimeObject
{
public:
	// TMPro.TextElementType TMPro.TMP_TextElement::m_ElementType
	uint8_t ___m_ElementType_0;
	// System.UInt32 TMPro.TMP_TextElement::m_Unicode
	uint32_t ___m_Unicode_1;
	// UnityEngine.TextCore.Glyph TMPro.TMP_TextElement::m_Glyph
	Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * ___m_Glyph_2;
	// System.UInt32 TMPro.TMP_TextElement::m_GlyphIndex
	uint32_t ___m_GlyphIndex_3;
	// System.Single TMPro.TMP_TextElement::m_Scale
	float ___m_Scale_4;

public:
	inline static int32_t get_offset_of_m_ElementType_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_ElementType_0)); }
	inline uint8_t get_m_ElementType_0() const { return ___m_ElementType_0; }
	inline uint8_t* get_address_of_m_ElementType_0() { return &___m_ElementType_0; }
	inline void set_m_ElementType_0(uint8_t value)
	{
		___m_ElementType_0 = value;
	}

	inline static int32_t get_offset_of_m_Unicode_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Unicode_1)); }
	inline uint32_t get_m_Unicode_1() const { return ___m_Unicode_1; }
	inline uint32_t* get_address_of_m_Unicode_1() { return &___m_Unicode_1; }
	inline void set_m_Unicode_1(uint32_t value)
	{
		___m_Unicode_1 = value;
	}

	inline static int32_t get_offset_of_m_Glyph_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Glyph_2)); }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * get_m_Glyph_2() const { return ___m_Glyph_2; }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 ** get_address_of_m_Glyph_2() { return &___m_Glyph_2; }
	inline void set_m_Glyph_2(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * value)
	{
		___m_Glyph_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Glyph_2), value);
	}

	inline static int32_t get_offset_of_m_GlyphIndex_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_GlyphIndex_3)); }
	inline uint32_t get_m_GlyphIndex_3() const { return ___m_GlyphIndex_3; }
	inline uint32_t* get_address_of_m_GlyphIndex_3() { return &___m_GlyphIndex_3; }
	inline void set_m_GlyphIndex_3(uint32_t value)
	{
		___m_GlyphIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_Scale_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Scale_4)); }
	inline float get_m_Scale_4() const { return ___m_Scale_4; }
	inline float* get_address_of_m_Scale_4() { return &___m_Scale_4; }
	inline void set_m_Scale_4(float value)
	{
		___m_Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef CAPINFO_T359B32B68C74DE323554A69C22AC50969886AD01_H
#define CAPINFO_T359B32B68C74DE323554A69C22AC50969886AD01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.CapInfo
struct  CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01  : public RuntimeObject
{
public:
	// Vectrosity.EndCap Vectrosity.CapInfo::capType
	int32_t ___capType_0;
	// UnityEngine.Texture Vectrosity.CapInfo::texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___texture_1;
	// System.Single Vectrosity.CapInfo::ratio1
	float ___ratio1_2;
	// System.Single Vectrosity.CapInfo::ratio2
	float ___ratio2_3;
	// System.Single Vectrosity.CapInfo::offset1
	float ___offset1_4;
	// System.Single Vectrosity.CapInfo::offset2
	float ___offset2_5;
	// System.Single Vectrosity.CapInfo::scale1
	float ___scale1_6;
	// System.Single Vectrosity.CapInfo::scale2
	float ___scale2_7;
	// System.Single[] Vectrosity.CapInfo::uvHeights
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___uvHeights_8;

public:
	inline static int32_t get_offset_of_capType_0() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___capType_0)); }
	inline int32_t get_capType_0() const { return ___capType_0; }
	inline int32_t* get_address_of_capType_0() { return &___capType_0; }
	inline void set_capType_0(int32_t value)
	{
		___capType_0 = value;
	}

	inline static int32_t get_offset_of_texture_1() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___texture_1)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_texture_1() const { return ___texture_1; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_texture_1() { return &___texture_1; }
	inline void set_texture_1(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___texture_1 = value;
		Il2CppCodeGenWriteBarrier((&___texture_1), value);
	}

	inline static int32_t get_offset_of_ratio1_2() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___ratio1_2)); }
	inline float get_ratio1_2() const { return ___ratio1_2; }
	inline float* get_address_of_ratio1_2() { return &___ratio1_2; }
	inline void set_ratio1_2(float value)
	{
		___ratio1_2 = value;
	}

	inline static int32_t get_offset_of_ratio2_3() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___ratio2_3)); }
	inline float get_ratio2_3() const { return ___ratio2_3; }
	inline float* get_address_of_ratio2_3() { return &___ratio2_3; }
	inline void set_ratio2_3(float value)
	{
		___ratio2_3 = value;
	}

	inline static int32_t get_offset_of_offset1_4() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___offset1_4)); }
	inline float get_offset1_4() const { return ___offset1_4; }
	inline float* get_address_of_offset1_4() { return &___offset1_4; }
	inline void set_offset1_4(float value)
	{
		___offset1_4 = value;
	}

	inline static int32_t get_offset_of_offset2_5() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___offset2_5)); }
	inline float get_offset2_5() const { return ___offset2_5; }
	inline float* get_address_of_offset2_5() { return &___offset2_5; }
	inline void set_offset2_5(float value)
	{
		___offset2_5 = value;
	}

	inline static int32_t get_offset_of_scale1_6() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___scale1_6)); }
	inline float get_scale1_6() const { return ___scale1_6; }
	inline float* get_address_of_scale1_6() { return &___scale1_6; }
	inline void set_scale1_6(float value)
	{
		___scale1_6 = value;
	}

	inline static int32_t get_offset_of_scale2_7() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___scale2_7)); }
	inline float get_scale2_7() const { return ___scale2_7; }
	inline float* get_address_of_scale2_7() { return &___scale2_7; }
	inline void set_scale2_7(float value)
	{
		___scale2_7 = value;
	}

	inline static int32_t get_offset_of_uvHeights_8() { return static_cast<int32_t>(offsetof(CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01, ___uvHeights_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_uvHeights_8() const { return ___uvHeights_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_uvHeights_8() { return &___uvHeights_8; }
	inline void set_uvHeights_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___uvHeights_8 = value;
		Il2CppCodeGenWriteBarrier((&___uvHeights_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPINFO_T359B32B68C74DE323554A69C22AC50969886AD01_H
#ifndef VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#define VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine
struct  VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_lineVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_lineVertices_0;
	// UnityEngine.Vector2[] Vectrosity.VectorLine::m_lineUVs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_lineUVs_1;
	// UnityEngine.Color32[] Vectrosity.VectorLine::m_lineColors
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_lineColors_2;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::m_lineTriangles
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lineTriangles_3;
	// System.Int32 Vectrosity.VectorLine::m_vertexCount
	int32_t ___m_vertexCount_4;
	// UnityEngine.GameObject Vectrosity.VectorLine::m_go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_go_5;
	// UnityEngine.RectTransform Vectrosity.VectorLine::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_6;
	// Vectrosity.IVectorObject Vectrosity.VectorLine::m_vectorObject
	RuntimeObject* ___m_vectorObject_7;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_color_8;
	// Vectrosity.CanvasState Vectrosity.VectorLine::m_canvasState
	int32_t ___m_canvasState_9;
	// System.Boolean Vectrosity.VectorLine::m_is2D
	bool ___m_is2D_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::m_points2
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_points2_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::m_points3
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_points3_12;
	// System.Int32 Vectrosity.VectorLine::m_pointsCount
	int32_t ___m_pointsCount_13;
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_screenPoints
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_screenPoints_14;
	// System.Single[] Vectrosity.VectorLine::m_lineWidths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_lineWidths_15;
	// System.Single Vectrosity.VectorLine::m_lineWidth
	float ___m_lineWidth_16;
	// System.Single Vectrosity.VectorLine::m_maxWeldDistance
	float ___m_maxWeldDistance_17;
	// System.Single[] Vectrosity.VectorLine::m_distances
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_distances_18;
	// System.String Vectrosity.VectorLine::m_name
	String_t* ___m_name_19;
	// UnityEngine.Material Vectrosity.VectorLine::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_20;
	// UnityEngine.Texture Vectrosity.VectorLine::m_originalTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_originalTexture_21;
	// UnityEngine.Texture Vectrosity.VectorLine::m_texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_texture_22;
	// System.Boolean Vectrosity.VectorLine::m_active
	bool ___m_active_23;
	// Vectrosity.LineType Vectrosity.VectorLine::m_lineType
	int32_t ___m_lineType_24;
	// System.Single Vectrosity.VectorLine::m_capLength
	float ___m_capLength_25;
	// System.Boolean Vectrosity.VectorLine::m_smoothWidth
	bool ___m_smoothWidth_26;
	// System.Boolean Vectrosity.VectorLine::m_smoothColor
	bool ___m_smoothColor_27;
	// Vectrosity.Joins Vectrosity.VectorLine::m_joins
	int32_t ___m_joins_28;
	// System.Boolean Vectrosity.VectorLine::m_isAutoDrawing
	bool ___m_isAutoDrawing_29;
	// System.Int32 Vectrosity.VectorLine::m_drawStart
	int32_t ___m_drawStart_30;
	// System.Int32 Vectrosity.VectorLine::m_drawEnd
	int32_t ___m_drawEnd_31;
	// System.Int32 Vectrosity.VectorLine::m_endPointsUpdate
	int32_t ___m_endPointsUpdate_32;
	// System.Boolean Vectrosity.VectorLine::m_useNormals
	bool ___m_useNormals_33;
	// System.Boolean Vectrosity.VectorLine::m_useTangents
	bool ___m_useTangents_34;
	// System.Boolean Vectrosity.VectorLine::m_normalsCalculated
	bool ___m_normalsCalculated_35;
	// System.Boolean Vectrosity.VectorLine::m_tangentsCalculated
	bool ___m_tangentsCalculated_36;
	// Vectrosity.EndCap Vectrosity.VectorLine::m_capType
	int32_t ___m_capType_37;
	// System.String Vectrosity.VectorLine::m_endCap
	String_t* ___m_endCap_38;
	// System.Boolean Vectrosity.VectorLine::m_useCapColors
	bool ___m_useCapColors_39;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_frontColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_frontColor_40;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_backColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_backColor_41;
	// System.Int32 Vectrosity.VectorLine::m_frontEndCapIndex
	int32_t ___m_frontEndCapIndex_42;
	// System.Int32 Vectrosity.VectorLine::m_backEndCapIndex
	int32_t ___m_backEndCapIndex_43;
	// System.Single Vectrosity.VectorLine::m_lineUVBottom
	float ___m_lineUVBottom_44;
	// System.Single Vectrosity.VectorLine::m_lineUVTop
	float ___m_lineUVTop_45;
	// System.Single Vectrosity.VectorLine::m_frontCapUVBottom
	float ___m_frontCapUVBottom_46;
	// System.Single Vectrosity.VectorLine::m_frontCapUVTop
	float ___m_frontCapUVTop_47;
	// System.Single Vectrosity.VectorLine::m_backCapUVBottom
	float ___m_backCapUVBottom_48;
	// System.Single Vectrosity.VectorLine::m_backCapUVTop
	float ___m_backCapUVTop_49;
	// System.Boolean Vectrosity.VectorLine::m_continuousTexture
	bool ___m_continuousTexture_50;
	// UnityEngine.Transform Vectrosity.VectorLine::m_drawTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_drawTransform_51;
	// System.Boolean Vectrosity.VectorLine::m_viewportDraw
	bool ___m_viewportDraw_52;
	// System.Single Vectrosity.VectorLine::m_textureScale
	float ___m_textureScale_53;
	// System.Boolean Vectrosity.VectorLine::m_useTextureScale
	bool ___m_useTextureScale_54;
	// System.Single Vectrosity.VectorLine::m_textureOffset
	float ___m_textureOffset_55;
	// System.Boolean Vectrosity.VectorLine::m_useMatrix
	bool ___m_useMatrix_56;
	// UnityEngine.Matrix4x4 Vectrosity.VectorLine::m_matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_matrix_57;
	// System.Boolean Vectrosity.VectorLine::m_collider
	bool ___m_collider_58;
	// System.Boolean Vectrosity.VectorLine::m_trigger
	bool ___m_trigger_59;
	// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::m_physicsMaterial
	PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * ___m_physicsMaterial_60;
	// System.Boolean Vectrosity.VectorLine::m_alignOddWidthToPixels
	bool ___m_alignOddWidthToPixels_61;

public:
	inline static int32_t get_offset_of_m_lineVertices_0() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineVertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_lineVertices_0() const { return ___m_lineVertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_lineVertices_0() { return &___m_lineVertices_0; }
	inline void set_m_lineVertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_lineVertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineVertices_0), value);
	}

	inline static int32_t get_offset_of_m_lineUVs_1() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVs_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_lineUVs_1() const { return ___m_lineUVs_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_lineUVs_1() { return &___m_lineUVs_1; }
	inline void set_m_lineUVs_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_lineUVs_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineUVs_1), value);
	}

	inline static int32_t get_offset_of_m_lineColors_2() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineColors_2)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_lineColors_2() const { return ___m_lineColors_2; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_lineColors_2() { return &___m_lineColors_2; }
	inline void set_m_lineColors_2(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_lineColors_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineColors_2), value);
	}

	inline static int32_t get_offset_of_m_lineTriangles_3() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineTriangles_3)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lineTriangles_3() const { return ___m_lineTriangles_3; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lineTriangles_3() { return &___m_lineTriangles_3; }
	inline void set_m_lineTriangles_3(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lineTriangles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineTriangles_3), value);
	}

	inline static int32_t get_offset_of_m_vertexCount_4() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vertexCount_4)); }
	inline int32_t get_m_vertexCount_4() const { return ___m_vertexCount_4; }
	inline int32_t* get_address_of_m_vertexCount_4() { return &___m_vertexCount_4; }
	inline void set_m_vertexCount_4(int32_t value)
	{
		___m_vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_m_go_5() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_go_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_go_5() const { return ___m_go_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_go_5() { return &___m_go_5; }
	inline void set_m_go_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_go_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_go_5), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_6() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_rectTransform_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_6() const { return ___m_rectTransform_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_6() { return &___m_rectTransform_6; }
	inline void set_m_rectTransform_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_6), value);
	}

	inline static int32_t get_offset_of_m_vectorObject_7() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_vectorObject_7)); }
	inline RuntimeObject* get_m_vectorObject_7() const { return ___m_vectorObject_7; }
	inline RuntimeObject** get_address_of_m_vectorObject_7() { return &___m_vectorObject_7; }
	inline void set_m_vectorObject_7(RuntimeObject* value)
	{
		___m_vectorObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorObject_7), value);
	}

	inline static int32_t get_offset_of_m_color_8() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_color_8)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_color_8() const { return ___m_color_8; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_color_8() { return &___m_color_8; }
	inline void set_m_color_8(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_color_8 = value;
	}

	inline static int32_t get_offset_of_m_canvasState_9() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_canvasState_9)); }
	inline int32_t get_m_canvasState_9() const { return ___m_canvasState_9; }
	inline int32_t* get_address_of_m_canvasState_9() { return &___m_canvasState_9; }
	inline void set_m_canvasState_9(int32_t value)
	{
		___m_canvasState_9 = value;
	}

	inline static int32_t get_offset_of_m_is2D_10() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_is2D_10)); }
	inline bool get_m_is2D_10() const { return ___m_is2D_10; }
	inline bool* get_address_of_m_is2D_10() { return &___m_is2D_10; }
	inline void set_m_is2D_10(bool value)
	{
		___m_is2D_10 = value;
	}

	inline static int32_t get_offset_of_m_points2_11() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points2_11)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_points2_11() const { return ___m_points2_11; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_points2_11() { return &___m_points2_11; }
	inline void set_m_points2_11(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_points2_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_points2_11), value);
	}

	inline static int32_t get_offset_of_m_points3_12() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_points3_12)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_points3_12() const { return ___m_points3_12; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_points3_12() { return &___m_points3_12; }
	inline void set_m_points3_12(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_points3_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_points3_12), value);
	}

	inline static int32_t get_offset_of_m_pointsCount_13() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_pointsCount_13)); }
	inline int32_t get_m_pointsCount_13() const { return ___m_pointsCount_13; }
	inline int32_t* get_address_of_m_pointsCount_13() { return &___m_pointsCount_13; }
	inline void set_m_pointsCount_13(int32_t value)
	{
		___m_pointsCount_13 = value;
	}

	inline static int32_t get_offset_of_m_screenPoints_14() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_screenPoints_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_screenPoints_14() const { return ___m_screenPoints_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_screenPoints_14() { return &___m_screenPoints_14; }
	inline void set_m_screenPoints_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_screenPoints_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_screenPoints_14), value);
	}

	inline static int32_t get_offset_of_m_lineWidths_15() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidths_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_lineWidths_15() const { return ___m_lineWidths_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_lineWidths_15() { return &___m_lineWidths_15; }
	inline void set_m_lineWidths_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_lineWidths_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWidths_15), value);
	}

	inline static int32_t get_offset_of_m_lineWidth_16() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineWidth_16)); }
	inline float get_m_lineWidth_16() const { return ___m_lineWidth_16; }
	inline float* get_address_of_m_lineWidth_16() { return &___m_lineWidth_16; }
	inline void set_m_lineWidth_16(float value)
	{
		___m_lineWidth_16 = value;
	}

	inline static int32_t get_offset_of_m_maxWeldDistance_17() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_maxWeldDistance_17)); }
	inline float get_m_maxWeldDistance_17() const { return ___m_maxWeldDistance_17; }
	inline float* get_address_of_m_maxWeldDistance_17() { return &___m_maxWeldDistance_17; }
	inline void set_m_maxWeldDistance_17(float value)
	{
		___m_maxWeldDistance_17 = value;
	}

	inline static int32_t get_offset_of_m_distances_18() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_distances_18)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_distances_18() const { return ___m_distances_18; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_distances_18() { return &___m_distances_18; }
	inline void set_m_distances_18(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_distances_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_distances_18), value);
	}

	inline static int32_t get_offset_of_m_name_19() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_name_19)); }
	inline String_t* get_m_name_19() const { return ___m_name_19; }
	inline String_t** get_address_of_m_name_19() { return &___m_name_19; }
	inline void set_m_name_19(String_t* value)
	{
		___m_name_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_19), value);
	}

	inline static int32_t get_offset_of_m_material_20() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_material_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_20() const { return ___m_material_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_20() { return &___m_material_20; }
	inline void set_m_material_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_20), value);
	}

	inline static int32_t get_offset_of_m_originalTexture_21() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_originalTexture_21)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_originalTexture_21() const { return ___m_originalTexture_21; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_originalTexture_21() { return &___m_originalTexture_21; }
	inline void set_m_originalTexture_21(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_originalTexture_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalTexture_21), value);
	}

	inline static int32_t get_offset_of_m_texture_22() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_texture_22)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_texture_22() const { return ___m_texture_22; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_texture_22() { return &___m_texture_22; }
	inline void set_m_texture_22(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_texture_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_texture_22), value);
	}

	inline static int32_t get_offset_of_m_active_23() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_active_23)); }
	inline bool get_m_active_23() const { return ___m_active_23; }
	inline bool* get_address_of_m_active_23() { return &___m_active_23; }
	inline void set_m_active_23(bool value)
	{
		___m_active_23 = value;
	}

	inline static int32_t get_offset_of_m_lineType_24() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineType_24)); }
	inline int32_t get_m_lineType_24() const { return ___m_lineType_24; }
	inline int32_t* get_address_of_m_lineType_24() { return &___m_lineType_24; }
	inline void set_m_lineType_24(int32_t value)
	{
		___m_lineType_24 = value;
	}

	inline static int32_t get_offset_of_m_capLength_25() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capLength_25)); }
	inline float get_m_capLength_25() const { return ___m_capLength_25; }
	inline float* get_address_of_m_capLength_25() { return &___m_capLength_25; }
	inline void set_m_capLength_25(float value)
	{
		___m_capLength_25 = value;
	}

	inline static int32_t get_offset_of_m_smoothWidth_26() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothWidth_26)); }
	inline bool get_m_smoothWidth_26() const { return ___m_smoothWidth_26; }
	inline bool* get_address_of_m_smoothWidth_26() { return &___m_smoothWidth_26; }
	inline void set_m_smoothWidth_26(bool value)
	{
		___m_smoothWidth_26 = value;
	}

	inline static int32_t get_offset_of_m_smoothColor_27() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_smoothColor_27)); }
	inline bool get_m_smoothColor_27() const { return ___m_smoothColor_27; }
	inline bool* get_address_of_m_smoothColor_27() { return &___m_smoothColor_27; }
	inline void set_m_smoothColor_27(bool value)
	{
		___m_smoothColor_27 = value;
	}

	inline static int32_t get_offset_of_m_joins_28() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_joins_28)); }
	inline int32_t get_m_joins_28() const { return ___m_joins_28; }
	inline int32_t* get_address_of_m_joins_28() { return &___m_joins_28; }
	inline void set_m_joins_28(int32_t value)
	{
		___m_joins_28 = value;
	}

	inline static int32_t get_offset_of_m_isAutoDrawing_29() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_isAutoDrawing_29)); }
	inline bool get_m_isAutoDrawing_29() const { return ___m_isAutoDrawing_29; }
	inline bool* get_address_of_m_isAutoDrawing_29() { return &___m_isAutoDrawing_29; }
	inline void set_m_isAutoDrawing_29(bool value)
	{
		___m_isAutoDrawing_29 = value;
	}

	inline static int32_t get_offset_of_m_drawStart_30() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawStart_30)); }
	inline int32_t get_m_drawStart_30() const { return ___m_drawStart_30; }
	inline int32_t* get_address_of_m_drawStart_30() { return &___m_drawStart_30; }
	inline void set_m_drawStart_30(int32_t value)
	{
		___m_drawStart_30 = value;
	}

	inline static int32_t get_offset_of_m_drawEnd_31() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawEnd_31)); }
	inline int32_t get_m_drawEnd_31() const { return ___m_drawEnd_31; }
	inline int32_t* get_address_of_m_drawEnd_31() { return &___m_drawEnd_31; }
	inline void set_m_drawEnd_31(int32_t value)
	{
		___m_drawEnd_31 = value;
	}

	inline static int32_t get_offset_of_m_endPointsUpdate_32() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endPointsUpdate_32)); }
	inline int32_t get_m_endPointsUpdate_32() const { return ___m_endPointsUpdate_32; }
	inline int32_t* get_address_of_m_endPointsUpdate_32() { return &___m_endPointsUpdate_32; }
	inline void set_m_endPointsUpdate_32(int32_t value)
	{
		___m_endPointsUpdate_32 = value;
	}

	inline static int32_t get_offset_of_m_useNormals_33() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useNormals_33)); }
	inline bool get_m_useNormals_33() const { return ___m_useNormals_33; }
	inline bool* get_address_of_m_useNormals_33() { return &___m_useNormals_33; }
	inline void set_m_useNormals_33(bool value)
	{
		___m_useNormals_33 = value;
	}

	inline static int32_t get_offset_of_m_useTangents_34() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTangents_34)); }
	inline bool get_m_useTangents_34() const { return ___m_useTangents_34; }
	inline bool* get_address_of_m_useTangents_34() { return &___m_useTangents_34; }
	inline void set_m_useTangents_34(bool value)
	{
		___m_useTangents_34 = value;
	}

	inline static int32_t get_offset_of_m_normalsCalculated_35() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_normalsCalculated_35)); }
	inline bool get_m_normalsCalculated_35() const { return ___m_normalsCalculated_35; }
	inline bool* get_address_of_m_normalsCalculated_35() { return &___m_normalsCalculated_35; }
	inline void set_m_normalsCalculated_35(bool value)
	{
		___m_normalsCalculated_35 = value;
	}

	inline static int32_t get_offset_of_m_tangentsCalculated_36() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_tangentsCalculated_36)); }
	inline bool get_m_tangentsCalculated_36() const { return ___m_tangentsCalculated_36; }
	inline bool* get_address_of_m_tangentsCalculated_36() { return &___m_tangentsCalculated_36; }
	inline void set_m_tangentsCalculated_36(bool value)
	{
		___m_tangentsCalculated_36 = value;
	}

	inline static int32_t get_offset_of_m_capType_37() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_capType_37)); }
	inline int32_t get_m_capType_37() const { return ___m_capType_37; }
	inline int32_t* get_address_of_m_capType_37() { return &___m_capType_37; }
	inline void set_m_capType_37(int32_t value)
	{
		___m_capType_37 = value;
	}

	inline static int32_t get_offset_of_m_endCap_38() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_endCap_38)); }
	inline String_t* get_m_endCap_38() const { return ___m_endCap_38; }
	inline String_t** get_address_of_m_endCap_38() { return &___m_endCap_38; }
	inline void set_m_endCap_38(String_t* value)
	{
		___m_endCap_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_endCap_38), value);
	}

	inline static int32_t get_offset_of_m_useCapColors_39() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useCapColors_39)); }
	inline bool get_m_useCapColors_39() const { return ___m_useCapColors_39; }
	inline bool* get_address_of_m_useCapColors_39() { return &___m_useCapColors_39; }
	inline void set_m_useCapColors_39(bool value)
	{
		___m_useCapColors_39 = value;
	}

	inline static int32_t get_offset_of_m_frontColor_40() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontColor_40)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_frontColor_40() const { return ___m_frontColor_40; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_frontColor_40() { return &___m_frontColor_40; }
	inline void set_m_frontColor_40(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_frontColor_40 = value;
	}

	inline static int32_t get_offset_of_m_backColor_41() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backColor_41)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_backColor_41() const { return ___m_backColor_41; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_backColor_41() { return &___m_backColor_41; }
	inline void set_m_backColor_41(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_backColor_41 = value;
	}

	inline static int32_t get_offset_of_m_frontEndCapIndex_42() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontEndCapIndex_42)); }
	inline int32_t get_m_frontEndCapIndex_42() const { return ___m_frontEndCapIndex_42; }
	inline int32_t* get_address_of_m_frontEndCapIndex_42() { return &___m_frontEndCapIndex_42; }
	inline void set_m_frontEndCapIndex_42(int32_t value)
	{
		___m_frontEndCapIndex_42 = value;
	}

	inline static int32_t get_offset_of_m_backEndCapIndex_43() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backEndCapIndex_43)); }
	inline int32_t get_m_backEndCapIndex_43() const { return ___m_backEndCapIndex_43; }
	inline int32_t* get_address_of_m_backEndCapIndex_43() { return &___m_backEndCapIndex_43; }
	inline void set_m_backEndCapIndex_43(int32_t value)
	{
		___m_backEndCapIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_lineUVBottom_44() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVBottom_44)); }
	inline float get_m_lineUVBottom_44() const { return ___m_lineUVBottom_44; }
	inline float* get_address_of_m_lineUVBottom_44() { return &___m_lineUVBottom_44; }
	inline void set_m_lineUVBottom_44(float value)
	{
		___m_lineUVBottom_44 = value;
	}

	inline static int32_t get_offset_of_m_lineUVTop_45() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_lineUVTop_45)); }
	inline float get_m_lineUVTop_45() const { return ___m_lineUVTop_45; }
	inline float* get_address_of_m_lineUVTop_45() { return &___m_lineUVTop_45; }
	inline void set_m_lineUVTop_45(float value)
	{
		___m_lineUVTop_45 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVBottom_46() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVBottom_46)); }
	inline float get_m_frontCapUVBottom_46() const { return ___m_frontCapUVBottom_46; }
	inline float* get_address_of_m_frontCapUVBottom_46() { return &___m_frontCapUVBottom_46; }
	inline void set_m_frontCapUVBottom_46(float value)
	{
		___m_frontCapUVBottom_46 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVTop_47() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_frontCapUVTop_47)); }
	inline float get_m_frontCapUVTop_47() const { return ___m_frontCapUVTop_47; }
	inline float* get_address_of_m_frontCapUVTop_47() { return &___m_frontCapUVTop_47; }
	inline void set_m_frontCapUVTop_47(float value)
	{
		___m_frontCapUVTop_47 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVBottom_48() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVBottom_48)); }
	inline float get_m_backCapUVBottom_48() const { return ___m_backCapUVBottom_48; }
	inline float* get_address_of_m_backCapUVBottom_48() { return &___m_backCapUVBottom_48; }
	inline void set_m_backCapUVBottom_48(float value)
	{
		___m_backCapUVBottom_48 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVTop_49() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_backCapUVTop_49)); }
	inline float get_m_backCapUVTop_49() const { return ___m_backCapUVTop_49; }
	inline float* get_address_of_m_backCapUVTop_49() { return &___m_backCapUVTop_49; }
	inline void set_m_backCapUVTop_49(float value)
	{
		___m_backCapUVTop_49 = value;
	}

	inline static int32_t get_offset_of_m_continuousTexture_50() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_continuousTexture_50)); }
	inline bool get_m_continuousTexture_50() const { return ___m_continuousTexture_50; }
	inline bool* get_address_of_m_continuousTexture_50() { return &___m_continuousTexture_50; }
	inline void set_m_continuousTexture_50(bool value)
	{
		___m_continuousTexture_50 = value;
	}

	inline static int32_t get_offset_of_m_drawTransform_51() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_drawTransform_51)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_drawTransform_51() const { return ___m_drawTransform_51; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_drawTransform_51() { return &___m_drawTransform_51; }
	inline void set_m_drawTransform_51(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_drawTransform_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_drawTransform_51), value);
	}

	inline static int32_t get_offset_of_m_viewportDraw_52() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_viewportDraw_52)); }
	inline bool get_m_viewportDraw_52() const { return ___m_viewportDraw_52; }
	inline bool* get_address_of_m_viewportDraw_52() { return &___m_viewportDraw_52; }
	inline void set_m_viewportDraw_52(bool value)
	{
		___m_viewportDraw_52 = value;
	}

	inline static int32_t get_offset_of_m_textureScale_53() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureScale_53)); }
	inline float get_m_textureScale_53() const { return ___m_textureScale_53; }
	inline float* get_address_of_m_textureScale_53() { return &___m_textureScale_53; }
	inline void set_m_textureScale_53(float value)
	{
		___m_textureScale_53 = value;
	}

	inline static int32_t get_offset_of_m_useTextureScale_54() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useTextureScale_54)); }
	inline bool get_m_useTextureScale_54() const { return ___m_useTextureScale_54; }
	inline bool* get_address_of_m_useTextureScale_54() { return &___m_useTextureScale_54; }
	inline void set_m_useTextureScale_54(bool value)
	{
		___m_useTextureScale_54 = value;
	}

	inline static int32_t get_offset_of_m_textureOffset_55() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_textureOffset_55)); }
	inline float get_m_textureOffset_55() const { return ___m_textureOffset_55; }
	inline float* get_address_of_m_textureOffset_55() { return &___m_textureOffset_55; }
	inline void set_m_textureOffset_55(float value)
	{
		___m_textureOffset_55 = value;
	}

	inline static int32_t get_offset_of_m_useMatrix_56() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_useMatrix_56)); }
	inline bool get_m_useMatrix_56() const { return ___m_useMatrix_56; }
	inline bool* get_address_of_m_useMatrix_56() { return &___m_useMatrix_56; }
	inline void set_m_useMatrix_56(bool value)
	{
		___m_useMatrix_56 = value;
	}

	inline static int32_t get_offset_of_m_matrix_57() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_matrix_57)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_matrix_57() const { return ___m_matrix_57; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_matrix_57() { return &___m_matrix_57; }
	inline void set_m_matrix_57(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_matrix_57 = value;
	}

	inline static int32_t get_offset_of_m_collider_58() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_collider_58)); }
	inline bool get_m_collider_58() const { return ___m_collider_58; }
	inline bool* get_address_of_m_collider_58() { return &___m_collider_58; }
	inline void set_m_collider_58(bool value)
	{
		___m_collider_58 = value;
	}

	inline static int32_t get_offset_of_m_trigger_59() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_trigger_59)); }
	inline bool get_m_trigger_59() const { return ___m_trigger_59; }
	inline bool* get_address_of_m_trigger_59() { return &___m_trigger_59; }
	inline void set_m_trigger_59(bool value)
	{
		___m_trigger_59 = value;
	}

	inline static int32_t get_offset_of_m_physicsMaterial_60() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_physicsMaterial_60)); }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * get_m_physicsMaterial_60() const { return ___m_physicsMaterial_60; }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 ** get_address_of_m_physicsMaterial_60() { return &___m_physicsMaterial_60; }
	inline void set_m_physicsMaterial_60(PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * value)
	{
		___m_physicsMaterial_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_physicsMaterial_60), value);
	}

	inline static int32_t get_offset_of_m_alignOddWidthToPixels_61() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F, ___m_alignOddWidthToPixels_61)); }
	inline bool get_m_alignOddWidthToPixels_61() const { return ___m_alignOddWidthToPixels_61; }
	inline bool* get_address_of_m_alignOddWidthToPixels_61() { return &___m_alignOddWidthToPixels_61; }
	inline void set_m_alignOddWidthToPixels_61(bool value)
	{
		___m_alignOddWidthToPixels_61 = value;
	}
};

struct VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields
{
public:
	// UnityEngine.Vector3 Vectrosity.VectorLine::v3zero
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v3zero_62;
	// UnityEngine.Canvas Vectrosity.VectorLine::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_63;
	// UnityEngine.Transform Vectrosity.VectorLine::camTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___camTransform_64;
	// UnityEngine.Camera Vectrosity.VectorLine::cam3D
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam3D_65;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldPosition_66;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldRotation_67;
	// System.Boolean Vectrosity.VectorLine::lineManagerCreated
	bool ___lineManagerCreated_68;
	// Vectrosity.LineManager Vectrosity.VectorLine::m_lineManager
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * ___m_lineManager_69;
	// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo> Vectrosity.VectorLine::capDictionary
	Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * ___capDictionary_70;
	// System.Int32 Vectrosity.VectorLine::endianDiff1
	int32_t ___endianDiff1_71;
	// System.Int32 Vectrosity.VectorLine::endianDiff2
	int32_t ___endianDiff2_72;
	// System.Byte[] Vectrosity.VectorLine::byteBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___byteBlock_73;
	// System.String[] Vectrosity.VectorLine::functionNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___functionNames_74;

public:
	inline static int32_t get_offset_of_v3zero_62() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___v3zero_62)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v3zero_62() const { return ___v3zero_62; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v3zero_62() { return &___v3zero_62; }
	inline void set_v3zero_62(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v3zero_62 = value;
	}

	inline static int32_t get_offset_of_m_canvas_63() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_canvas_63)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_63() const { return ___m_canvas_63; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_63() { return &___m_canvas_63; }
	inline void set_m_canvas_63(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_63), value);
	}

	inline static int32_t get_offset_of_camTransform_64() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___camTransform_64)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_camTransform_64() const { return ___camTransform_64; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_camTransform_64() { return &___camTransform_64; }
	inline void set_camTransform_64(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___camTransform_64 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_64), value);
	}

	inline static int32_t get_offset_of_cam3D_65() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___cam3D_65)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam3D_65() const { return ___cam3D_65; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam3D_65() { return &___cam3D_65; }
	inline void set_cam3D_65(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam3D_65 = value;
		Il2CppCodeGenWriteBarrier((&___cam3D_65), value);
	}

	inline static int32_t get_offset_of_oldPosition_66() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldPosition_66)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldPosition_66() const { return ___oldPosition_66; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldPosition_66() { return &___oldPosition_66; }
	inline void set_oldPosition_66(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldPosition_66 = value;
	}

	inline static int32_t get_offset_of_oldRotation_67() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___oldRotation_67)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldRotation_67() const { return ___oldRotation_67; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldRotation_67() { return &___oldRotation_67; }
	inline void set_oldRotation_67(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldRotation_67 = value;
	}

	inline static int32_t get_offset_of_lineManagerCreated_68() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___lineManagerCreated_68)); }
	inline bool get_lineManagerCreated_68() const { return ___lineManagerCreated_68; }
	inline bool* get_address_of_lineManagerCreated_68() { return &___lineManagerCreated_68; }
	inline void set_lineManagerCreated_68(bool value)
	{
		___lineManagerCreated_68 = value;
	}

	inline static int32_t get_offset_of_m_lineManager_69() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___m_lineManager_69)); }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * get_m_lineManager_69() const { return ___m_lineManager_69; }
	inline LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 ** get_address_of_m_lineManager_69() { return &___m_lineManager_69; }
	inline void set_m_lineManager_69(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776 * value)
	{
		___m_lineManager_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineManager_69), value);
	}

	inline static int32_t get_offset_of_capDictionary_70() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___capDictionary_70)); }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * get_capDictionary_70() const { return ___capDictionary_70; }
	inline Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D ** get_address_of_capDictionary_70() { return &___capDictionary_70; }
	inline void set_capDictionary_70(Dictionary_2_t35508D58E43FFE3609D09418752BBADE00BD889D * value)
	{
		___capDictionary_70 = value;
		Il2CppCodeGenWriteBarrier((&___capDictionary_70), value);
	}

	inline static int32_t get_offset_of_endianDiff1_71() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff1_71)); }
	inline int32_t get_endianDiff1_71() const { return ___endianDiff1_71; }
	inline int32_t* get_address_of_endianDiff1_71() { return &___endianDiff1_71; }
	inline void set_endianDiff1_71(int32_t value)
	{
		___endianDiff1_71 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_72() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___endianDiff2_72)); }
	inline int32_t get_endianDiff2_72() const { return ___endianDiff2_72; }
	inline int32_t* get_address_of_endianDiff2_72() { return &___endianDiff2_72; }
	inline void set_endianDiff2_72(int32_t value)
	{
		___endianDiff2_72 = value;
	}

	inline static int32_t get_offset_of_byteBlock_73() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___byteBlock_73)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_byteBlock_73() const { return ___byteBlock_73; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_byteBlock_73() { return &___byteBlock_73; }
	inline void set_byteBlock_73(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___byteBlock_73 = value;
		Il2CppCodeGenWriteBarrier((&___byteBlock_73), value);
	}

	inline static int32_t get_offset_of_functionNames_74() { return static_cast<int32_t>(offsetof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields, ___functionNames_74)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_functionNames_74() const { return ___functionNames_74; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_functionNames_74() { return &___functionNames_74; }
	inline void set_functionNames_74(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___functionNames_74 = value;
		Il2CppCodeGenWriteBarrier((&___functionNames_74), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORLINE_TE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#define BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball
struct  Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#ifndef BALLRUNPLAYER_T9338BEAE346F60931A8E00795FD7170D91DEF21C_H
#define BALLRUNPLAYER_T9338BEAE346F60931A8E00795FD7170D91DEF21C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallRunPlayer
struct  BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform BallRunPlayer::ballModel
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ballModel_4;
	// System.Boolean BallRunPlayer::start
	bool ___start_5;
	// UnityEngine.Vector3 BallRunPlayer::moveDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveDirection_6;
	// UnityEngine.CharacterController BallRunPlayer::characterController
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___characterController_7;
	// UnityEngine.Vector3 BallRunPlayer::startPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startPosition_8;
	// System.Boolean BallRunPlayer::isJump
	bool ___isJump_9;

public:
	inline static int32_t get_offset_of_ballModel_4() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___ballModel_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ballModel_4() const { return ___ballModel_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ballModel_4() { return &___ballModel_4; }
	inline void set_ballModel_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ballModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___ballModel_4), value);
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___start_5)); }
	inline bool get_start_5() const { return ___start_5; }
	inline bool* get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(bool value)
	{
		___start_5 = value;
	}

	inline static int32_t get_offset_of_moveDirection_6() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___moveDirection_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveDirection_6() const { return ___moveDirection_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveDirection_6() { return &___moveDirection_6; }
	inline void set_moveDirection_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveDirection_6 = value;
	}

	inline static int32_t get_offset_of_characterController_7() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___characterController_7)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_characterController_7() const { return ___characterController_7; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_characterController_7() { return &___characterController_7; }
	inline void set_characterController_7(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___characterController_7 = value;
		Il2CppCodeGenWriteBarrier((&___characterController_7), value);
	}

	inline static int32_t get_offset_of_startPosition_8() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___startPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startPosition_8() const { return ___startPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startPosition_8() { return &___startPosition_8; }
	inline void set_startPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startPosition_8 = value;
	}

	inline static int32_t get_offset_of_isJump_9() { return static_cast<int32_t>(offsetof(BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C, ___isJump_9)); }
	inline bool get_isJump_9() const { return ___isJump_9; }
	inline bool* get_address_of_isJump_9() { return &___isJump_9; }
	inline void set_isJump_9(bool value)
	{
		___isJump_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLRUNPLAYER_T9338BEAE346F60931A8E00795FD7170D91DEF21C_H
#ifndef CUBESELECT_TB77B073FCD9256C5109FE277E7B7F3B484BD98E3_H
#define CUBESELECT_TB77B073FCD9256C5109FE277E7B7F3B484BD98E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeSelect
struct  CubeSelect_tB77B073FCD9256C5109FE277E7B7F3B484BD98E3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CubeSelect::cube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube_4;

public:
	inline static int32_t get_offset_of_cube_4() { return static_cast<int32_t>(offsetof(CubeSelect_tB77B073FCD9256C5109FE277E7B7F3B484BD98E3, ___cube_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube_4() const { return ___cube_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube_4() { return &___cube_4; }
	inline void set_cube_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube_4 = value;
		Il2CppCodeGenWriteBarrier((&___cube_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBESELECT_TB77B073FCD9256C5109FE277E7B7F3B484BD98E3_H
#ifndef DOUBLETAPME_T926E96BAB88B4D49653B65853CE0353174E70E4A_H
#define DOUBLETAPME_T926E96BAB88B4D49653B65853CE0353174E70E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleTapMe
struct  DoubleTapMe_t926E96BAB88B4D49653B65853CE0353174E70E4A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAPME_T926E96BAB88B4D49653B65853CE0353174E70E4A_H
#ifndef DRAGME_T26C3C20FB43EC07563B7ACECA717D87AEE41155A_H
#define DRAGME_T26C3C20FB43EC07563B7ACECA717D87AEE41155A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragMe
struct  DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh DragMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color DragMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;
	// UnityEngine.Vector3 DragMe::deltaPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deltaPosition_6;
	// System.Int32 DragMe::fingerIndex
	int32_t ___fingerIndex_7;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_6() { return static_cast<int32_t>(offsetof(DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A, ___deltaPosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deltaPosition_6() const { return ___deltaPosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deltaPosition_6() { return &___deltaPosition_6; }
	inline void set_deltaPosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deltaPosition_6 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_7() { return static_cast<int32_t>(offsetof(DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A, ___fingerIndex_7)); }
	inline int32_t get_fingerIndex_7() const { return ___fingerIndex_7; }
	inline int32_t* get_address_of_fingerIndex_7() { return &___fingerIndex_7; }
	inline void set_fingerIndex_7(int32_t value)
	{
		___fingerIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGME_T26C3C20FB43EC07563B7ACECA717D87AEE41155A_H
#ifndef ETWINDOW_T90349E97A6AE294226C71D6398D2DA2135F91956_H
#define ETWINDOW_T90349E97A6AE294226C71D6398D2DA2135F91956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETWindow
struct  ETWindow_t90349E97A6AE294226C71D6398D2DA2135F91956  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean ETWindow::drag
	bool ___drag_4;

public:
	inline static int32_t get_offset_of_drag_4() { return static_cast<int32_t>(offsetof(ETWindow_t90349E97A6AE294226C71D6398D2DA2135F91956, ___drag_4)); }
	inline bool get_drag_4() const { return ___drag_4; }
	inline bool* get_address_of_drag_4() { return &___drag_4; }
	inline void set_drag_4(bool value)
	{
		___drag_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETWINDOW_T90349E97A6AE294226C71D6398D2DA2135F91956_H
#ifndef FINGERTOUCH_TA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D_H
#define FINGERTOUCH_TA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerTouch
struct  FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh FingerTouch::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Vector3 FingerTouch::deltaPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deltaPosition_5;
	// System.Int32 FingerTouch::fingerId
	int32_t ___fingerId_6;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_deltaPosition_5() { return static_cast<int32_t>(offsetof(FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D, ___deltaPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deltaPosition_5() const { return ___deltaPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deltaPosition_5() { return &___deltaPosition_5; }
	inline void set_deltaPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deltaPosition_5 = value;
	}

	inline static int32_t get_offset_of_fingerId_6() { return static_cast<int32_t>(offsetof(FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D, ___fingerId_6)); }
	inline int32_t get_fingerId_6() const { return ___fingerId_6; }
	inline int32_t* get_address_of_fingerId_6() { return &___fingerId_6; }
	inline void set_fingerId_6(int32_t value)
	{
		___fingerId_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERTOUCH_TA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D_H
#ifndef GLOBALEASYTOUCHEVENT_T0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C_H
#define GLOBALEASYTOUCHEVENT_T0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalEasyTouchEvent
struct  GlobalEasyTouchEvent_t0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text GlobalEasyTouchEvent::statText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statText_4;

public:
	inline static int32_t get_offset_of_statText_4() { return static_cast<int32_t>(offsetof(GlobalEasyTouchEvent_t0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C, ___statText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statText_4() const { return ___statText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statText_4() { return &___statText_4; }
	inline void set_statText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statText_4 = value;
		Il2CppCodeGenWriteBarrier((&___statText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALEASYTOUCHEVENT_T0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C_H
#ifndef LOADEXAMPLES_T0CD5186C64195143339A78768F33CEDDA33DACCD_H
#define LOADEXAMPLES_T0CD5186C64195143339A78768F33CEDDA33DACCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadExamples
struct  LoadExamples_t0CD5186C64195143339A78768F33CEDDA33DACCD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADEXAMPLES_T0CD5186C64195143339A78768F33CEDDA33DACCD_H
#ifndef LONGTAPME_TFCCE53939858AD9373BC3DDCAAA39654E4F46779_H
#define LONGTAPME_TFCCE53939858AD9373BC3DDCAAA39654E4F46779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LongTapMe
struct  LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh LongTapMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color LongTapMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPME_TFCCE53939858AD9373BC3DDCAAA39654E4F46779_H
#ifndef MULTICAMERATOUCH_T27561691899B8D077DE0A95D4BA779E299C34246_H
#define MULTICAMERATOUCH_T27561691899B8D077DE0A95D4BA779E299C34246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiCameraTouch
struct  MultiCameraTouch_t27561691899B8D077DE0A95D4BA779E299C34246  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text MultiCameraTouch::label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___label_4;

public:
	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(MultiCameraTouch_t27561691899B8D077DE0A95D4BA779E299C34246, ___label_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_label_4() const { return ___label_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier((&___label_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICAMERATOUCH_T27561691899B8D077DE0A95D4BA779E299C34246_H
#ifndef MULTICAMERAUI_T12735E383D8761C12C540B8ADFFFA95B70DE8563_H
#define MULTICAMERAUI_T12735E383D8761C12C540B8ADFFFA95B70DE8563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiCameraUI
struct  MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera MultiCameraUI::cam2
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam2_4;
	// UnityEngine.Camera MultiCameraUI::cam3
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam3_5;

public:
	inline static int32_t get_offset_of_cam2_4() { return static_cast<int32_t>(offsetof(MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563, ___cam2_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam2_4() const { return ___cam2_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam2_4() { return &___cam2_4; }
	inline void set_cam2_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam2_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_4), value);
	}

	inline static int32_t get_offset_of_cam3_5() { return static_cast<int32_t>(offsetof(MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563, ___cam3_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam3_5() const { return ___cam3_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam3_5() { return &___cam3_5; }
	inline void set_cam3_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam3_5 = value;
		Il2CppCodeGenWriteBarrier((&___cam3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICAMERAUI_T12735E383D8761C12C540B8ADFFFA95B70DE8563_H
#ifndef MULTILAYERTOUCH_T7086C9897928EB82C35E7ACC7F57670C8E1DA6C7_H
#define MULTILAYERTOUCH_T7086C9897928EB82C35E7ACC7F57670C8E1DA6C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiLayerTouch
struct  MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text MultiLayerTouch::label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___label_4;
	// UnityEngine.UI.Text MultiLayerTouch::label2
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___label2_5;

public:
	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7, ___label_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_label_4() const { return ___label_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier((&___label_4), value);
	}

	inline static int32_t get_offset_of_label2_5() { return static_cast<int32_t>(offsetof(MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7, ___label2_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_label2_5() const { return ___label2_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_label2_5() { return &___label2_5; }
	inline void set_label2_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___label2_5 = value;
		Il2CppCodeGenWriteBarrier((&___label2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILAYERTOUCH_T7086C9897928EB82C35E7ACC7F57670C8E1DA6C7_H
#ifndef MULTILAYERUI_T097B3D533CC4D4897272ED062F4C8D14F38E723E_H
#define MULTILAYERUI_T097B3D533CC4D4897272ED062F4C8D14F38E723E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiLayerUI
struct  MultiLayerUI_t097B3D533CC4D4897272ED062F4C8D14F38E723E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILAYERUI_T097B3D533CC4D4897272ED062F4C8D14F38E723E_H
#ifndef MUTLIFINGERSSCREENTOUCH_T56F49D8EBD54112E7A9F67AA318AE672911627E7_H
#define MUTLIFINGERSSCREENTOUCH_T56F49D8EBD54112E7A9F67AA318AE672911627E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MutliFingersScreenTouch
struct  MutliFingersScreenTouch_t56F49D8EBD54112E7A9F67AA318AE672911627E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MutliFingersScreenTouch::touchGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___touchGameObject_4;

public:
	inline static int32_t get_offset_of_touchGameObject_4() { return static_cast<int32_t>(offsetof(MutliFingersScreenTouch_t56F49D8EBD54112E7A9F67AA318AE672911627E7, ___touchGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_touchGameObject_4() const { return ___touchGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_touchGameObject_4() { return &___touchGameObject_4; }
	inline void set_touchGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___touchGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___touchGameObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUTLIFINGERSSCREENTOUCH_T56F49D8EBD54112E7A9F67AA318AE672911627E7_H
#ifndef PINCHME_T6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6_H
#define PINCHME_T6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PinchMe
struct  PinchMe_t6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh PinchMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(PinchMe_t6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHME_T6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6_H
#ifndef RTSCAMERA_T514C2FDB125D689261EB16B7EF1D8B23915ABD47_H
#define RTSCAMERA_T514C2FDB125D689261EB16B7EF1D8B23915ABD47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RTSCamera
struct  RTSCamera_t514C2FDB125D689261EB16B7EF1D8B23915ABD47  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 RTSCamera::delta
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___delta_4;

public:
	inline static int32_t get_offset_of_delta_4() { return static_cast<int32_t>(offsetof(RTSCamera_t514C2FDB125D689261EB16B7EF1D8B23915ABD47, ___delta_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_delta_4() const { return ___delta_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_delta_4() { return &___delta_4; }
	inline void set_delta_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___delta_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTSCAMERA_T514C2FDB125D689261EB16B7EF1D8B23915ABD47_H
#ifndef SWIPE_T37F8398E806C3E56E6AC1897B603B75E09460899_H
#define SWIPE_T37F8398E806C3E56E6AC1897B603B75E09460899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Swipe
struct  Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Swipe::trail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trail_4;
	// UnityEngine.UI.Text Swipe::swipeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___swipeText_5;

public:
	inline static int32_t get_offset_of_trail_4() { return static_cast<int32_t>(offsetof(Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899, ___trail_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trail_4() const { return ___trail_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trail_4() { return &___trail_4; }
	inline void set_trail_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trail_4 = value;
		Il2CppCodeGenWriteBarrier((&___trail_4), value);
	}

	inline static int32_t get_offset_of_swipeText_5() { return static_cast<int32_t>(offsetof(Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899, ___swipeText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_swipeText_5() const { return ___swipeText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_swipeText_5() { return &___swipeText_5; }
	inline void set_swipeText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___swipeText_5 = value;
		Il2CppCodeGenWriteBarrier((&___swipeText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPE_T37F8398E806C3E56E6AC1897B603B75E09460899_H
#ifndef TAPME_T91364143D18FB3D29539FBD90A0C81D807082D9C_H
#define TAPME_T91364143D18FB3D29539FBD90A0C81D807082D9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapMe
struct  TapMe_t91364143D18FB3D29539FBD90A0C81D807082D9C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPME_T91364143D18FB3D29539FBD90A0C81D807082D9C_H
#ifndef THIRDPERSONCAMERA_T30B1D5421B432A30B8A56A0DBC22E5EC882603A2_H
#define THIRDPERSONCAMERA_T30B1D5421B432A30B8A56A0DBC22E5EC882603A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdPersonCamera
struct  ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ThirdPersonCamera::distanceAway
	float ___distanceAway_4;
	// System.Single ThirdPersonCamera::distanceUp
	float ___distanceUp_5;
	// System.Single ThirdPersonCamera::smooth
	float ___smooth_6;
	// UnityEngine.GameObject ThirdPersonCamera::hovercraft
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hovercraft_7;
	// UnityEngine.Vector3 ThirdPersonCamera::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_8;
	// UnityEngine.Transform ThirdPersonCamera::follow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___follow_9;

public:
	inline static int32_t get_offset_of_distanceAway_4() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___distanceAway_4)); }
	inline float get_distanceAway_4() const { return ___distanceAway_4; }
	inline float* get_address_of_distanceAway_4() { return &___distanceAway_4; }
	inline void set_distanceAway_4(float value)
	{
		___distanceAway_4 = value;
	}

	inline static int32_t get_offset_of_distanceUp_5() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___distanceUp_5)); }
	inline float get_distanceUp_5() const { return ___distanceUp_5; }
	inline float* get_address_of_distanceUp_5() { return &___distanceUp_5; }
	inline void set_distanceUp_5(float value)
	{
		___distanceUp_5 = value;
	}

	inline static int32_t get_offset_of_smooth_6() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___smooth_6)); }
	inline float get_smooth_6() const { return ___smooth_6; }
	inline float* get_address_of_smooth_6() { return &___smooth_6; }
	inline void set_smooth_6(float value)
	{
		___smooth_6 = value;
	}

	inline static int32_t get_offset_of_hovercraft_7() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___hovercraft_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hovercraft_7() const { return ___hovercraft_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hovercraft_7() { return &___hovercraft_7; }
	inline void set_hovercraft_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hovercraft_7 = value;
		Il2CppCodeGenWriteBarrier((&___hovercraft_7), value);
	}

	inline static int32_t get_offset_of_targetPosition_8() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___targetPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_8() const { return ___targetPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_8() { return &___targetPosition_8; }
	inline void set_targetPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_8 = value;
	}

	inline static int32_t get_offset_of_follow_9() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2, ___follow_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_follow_9() const { return ___follow_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_follow_9() { return &___follow_9; }
	inline void set_follow_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___follow_9 = value;
		Il2CppCodeGenWriteBarrier((&___follow_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCAMERA_T30B1D5421B432A30B8A56A0DBC22E5EC882603A2_H
#ifndef TOOGLEPICKMETHODUI_TD6A5E8764A6DE4FDE6DFD65BC9862F98266771D3_H
#define TOOGLEPICKMETHODUI_TD6A5E8764A6DE4FDE6DFD65BC9862F98266771D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TooglePickMethodUI
struct  TooglePickMethodUI_tD6A5E8764A6DE4FDE6DFD65BC9862F98266771D3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOGLEPICKMETHODUI_TD6A5E8764A6DE4FDE6DFD65BC9862F98266771D3_H
#ifndef TOUCHME_T00D1AB05380A4C1B20F60D37AE4754B261276174_H
#define TOUCHME_T00D1AB05380A4C1B20F60D37AE4754B261276174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchMe
struct  TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TouchMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color TouchMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHME_T00D1AB05380A4C1B20F60D37AE4754B261276174_H
#ifndef TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#define TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwistMe
struct  TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwistMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTME_TFCD23EA1FAED83FA2795551DC2CDACEC48A43431_H
#ifndef TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#define TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoDoubleTapMe
struct  TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWODOUBLETAPME_T211C12A2A4072CC22EEAD156B4A13F613EEC7FA6_H
#ifndef TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#define TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoDragMe
struct  TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoDragMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Vector3 TwoDragMe::deltaPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deltaPosition_5;
	// UnityEngine.Color TwoDragMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_6;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_deltaPosition_5() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___deltaPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deltaPosition_5() const { return ___deltaPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deltaPosition_5() { return &___deltaPosition_5; }
	inline void set_deltaPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deltaPosition_5 = value;
	}

	inline static int32_t get_offset_of_startColor_6() { return static_cast<int32_t>(offsetof(TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744, ___startColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_6() const { return ___startColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_6() { return &___startColor_6; }
	inline void set_startColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWODRAGME_T5E6E6B3660C0861DB7912C03C48875494A7B3744_H
#ifndef TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#define TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoLongTapMe
struct  TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoLongTapMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color TwoLongTapMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOLONGTAPME_T64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90_H
#ifndef TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#define TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoSwipe
struct  TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject TwoSwipe::trail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trail_4;
	// UnityEngine.UI.Text TwoSwipe::swipeData
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___swipeData_5;

public:
	inline static int32_t get_offset_of_trail_4() { return static_cast<int32_t>(offsetof(TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1, ___trail_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trail_4() const { return ___trail_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trail_4() { return &___trail_4; }
	inline void set_trail_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trail_4 = value;
		Il2CppCodeGenWriteBarrier((&___trail_4), value);
	}

	inline static int32_t get_offset_of_swipeData_5() { return static_cast<int32_t>(offsetof(TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1, ___swipeData_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_swipeData_5() const { return ___swipeData_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_swipeData_5() { return &___swipeData_5; }
	inline void set_swipeData_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___swipeData_5 = value;
		Il2CppCodeGenWriteBarrier((&___swipeData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOSWIPE_T7302475B6D99C134833786A691276DDC5873FEE1_H
#ifndef TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#define TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoTapMe
struct  TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOTAPME_T2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39_H
#ifndef TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#define TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoTouchMe
struct  TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh TwoTouchMe::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.Color TwoTouchMe::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_startColor_5() { return static_cast<int32_t>(offsetof(TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4, ___startColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_5() const { return ___startColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_5() { return &___startColor_5; }
	inline void set_startColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOTOUCHME_TB6158994C50B03E2BB08085F32767614E965B8F4_H
#ifndef UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#define UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICompatibility
struct  UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPATIBILITY_T544AB7DA19A39A90E4C4388AE1DA7183D4297BC2_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#define BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.BrightnessControl
struct  BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.BrightnessControl::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.BrightnessControl::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.BrightnessControl::m_useLine
	bool ___m_useLine_6;
	// System.Boolean Vectrosity.BrightnessControl::m_destroyed
	bool ___m_destroyed_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_useLine_6() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_useLine_6)); }
	inline bool get_m_useLine_6() const { return ___m_useLine_6; }
	inline bool* get_address_of_m_useLine_6() { return &___m_useLine_6; }
	inline void set_m_useLine_6(bool value)
	{
		___m_useLine_6 = value;
	}

	inline static int32_t get_offset_of_m_destroyed_7() { return static_cast<int32_t>(offsetof(BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97, ___m_destroyed_7)); }
	inline bool get_m_destroyed_7() const { return ___m_destroyed_7; }
	inline bool* get_address_of_m_destroyed_7() { return &___m_destroyed_7; }
	inline void set_m_destroyed_7(bool value)
	{
		___m_destroyed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESSCONTROL_TB70BEB70438B69CD3087C838A1F6498836912A97_H
#ifndef LINEMANAGER_T4991D2C017698413E3BA236D61D0FD00B877D776_H
#define LINEMANAGER_T4991D2C017698413E3BA236D61D0FD00B877D776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.LineManager
struct  LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Vectrosity.LineManager::destroyed
	bool ___destroyed_7;

public:
	inline static int32_t get_offset_of_destroyed_7() { return static_cast<int32_t>(offsetof(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776, ___destroyed_7)); }
	inline bool get_destroyed_7() const { return ___destroyed_7; }
	inline bool* get_address_of_destroyed_7() { return &___destroyed_7; }
	inline void set_destroyed_7(bool value)
	{
		___destroyed_7 = value;
	}
};

struct LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields
{
public:
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.LineManager::lines
	List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * ___lines_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Vectrosity.LineManager::transforms
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___transforms_5;
	// System.Int32 Vectrosity.LineManager::lineCount
	int32_t ___lineCount_6;

public:
	inline static int32_t get_offset_of_lines_4() { return static_cast<int32_t>(offsetof(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields, ___lines_4)); }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * get_lines_4() const { return ___lines_4; }
	inline List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 ** get_address_of_lines_4() { return &___lines_4; }
	inline void set_lines_4(List_1_t8FF1651FFBBE28B6D2D6F3D21BD2D16C88CD7980 * value)
	{
		___lines_4 = value;
		Il2CppCodeGenWriteBarrier((&___lines_4), value);
	}

	inline static int32_t get_offset_of_transforms_5() { return static_cast<int32_t>(offsetof(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields, ___transforms_5)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_transforms_5() const { return ___transforms_5; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_transforms_5() { return &___transforms_5; }
	inline void set_transforms_5(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___transforms_5 = value;
		Il2CppCodeGenWriteBarrier((&___transforms_5), value);
	}

	inline static int32_t get_offset_of_lineCount_6() { return static_cast<int32_t>(offsetof(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields, ___lineCount_6)); }
	inline int32_t get_lineCount_6() const { return ___lineCount_6; }
	inline int32_t* get_address_of_lineCount_6() { return &___lineCount_6; }
	inline void set_lineCount_6(int32_t value)
	{
		___lineCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEMANAGER_T4991D2C017698413E3BA236D61D0FD00B877D776_H
#ifndef VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#define VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorObject3D
struct  VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Vectrosity.VectorObject3D::m_updateVerts
	bool ___m_updateVerts_4;
	// System.Boolean Vectrosity.VectorObject3D::m_updateUVs
	bool ___m_updateUVs_5;
	// System.Boolean Vectrosity.VectorObject3D::m_updateColors
	bool ___m_updateColors_6;
	// System.Boolean Vectrosity.VectorObject3D::m_updateNormals
	bool ___m_updateNormals_7;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTangents
	bool ___m_updateTangents_8;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTris
	bool ___m_updateTris_9;
	// UnityEngine.Mesh Vectrosity.VectorObject3D::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_10;
	// Vectrosity.VectorLine Vectrosity.VectorObject3D::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_11;
	// UnityEngine.Material Vectrosity.VectorObject3D::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_12;
	// System.Boolean Vectrosity.VectorObject3D::m_useCustomMaterial
	bool ___m_useCustomMaterial_13;

public:
	inline static int32_t get_offset_of_m_updateVerts_4() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateVerts_4)); }
	inline bool get_m_updateVerts_4() const { return ___m_updateVerts_4; }
	inline bool* get_address_of_m_updateVerts_4() { return &___m_updateVerts_4; }
	inline void set_m_updateVerts_4(bool value)
	{
		___m_updateVerts_4 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_5() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateUVs_5)); }
	inline bool get_m_updateUVs_5() const { return ___m_updateUVs_5; }
	inline bool* get_address_of_m_updateUVs_5() { return &___m_updateUVs_5; }
	inline void set_m_updateUVs_5(bool value)
	{
		___m_updateUVs_5 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_6() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateColors_6)); }
	inline bool get_m_updateColors_6() const { return ___m_updateColors_6; }
	inline bool* get_address_of_m_updateColors_6() { return &___m_updateColors_6; }
	inline void set_m_updateColors_6(bool value)
	{
		___m_updateColors_6 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_7() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateNormals_7)); }
	inline bool get_m_updateNormals_7() const { return ___m_updateNormals_7; }
	inline bool* get_address_of_m_updateNormals_7() { return &___m_updateNormals_7; }
	inline void set_m_updateNormals_7(bool value)
	{
		___m_updateNormals_7 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_8() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateTangents_8)); }
	inline bool get_m_updateTangents_8() const { return ___m_updateTangents_8; }
	inline bool* get_address_of_m_updateTangents_8() { return &___m_updateTangents_8; }
	inline void set_m_updateTangents_8(bool value)
	{
		___m_updateTangents_8 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_9() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_updateTris_9)); }
	inline bool get_m_updateTris_9() const { return ___m_updateTris_9; }
	inline bool* get_address_of_m_updateTris_9() { return &___m_updateTris_9; }
	inline void set_m_updateTris_9(bool value)
	{
		___m_updateTris_9 = value;
	}

	inline static int32_t get_offset_of_m_mesh_10() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_mesh_10)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_10() const { return ___m_mesh_10; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_10() { return &___m_mesh_10; }
	inline void set_m_mesh_10(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_10), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_11() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_vectorLine_11)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_11() const { return ___m_vectorLine_11; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_11() { return &___m_vectorLine_11; }
	inline void set_m_vectorLine_11(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_11), value);
	}

	inline static int32_t get_offset_of_m_material_12() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_material_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_12() const { return ___m_material_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_12() { return &___m_material_12; }
	inline void set_m_material_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_12), value);
	}

	inline static int32_t get_offset_of_m_useCustomMaterial_13() { return static_cast<int32_t>(offsetof(VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE, ___m_useCustomMaterial_13)); }
	inline bool get_m_useCustomMaterial_13() const { return ___m_useCustomMaterial_13; }
	inline bool* get_address_of_m_useCustomMaterial_13() { return &___m_useCustomMaterial_13; }
	inline void set_m_useCustomMaterial_13(bool value)
	{
		___m_useCustomMaterial_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT3D_T0452EB1566879B916AA1C3566CA8E8431102CEAE_H
#ifndef VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#define VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControl
struct  VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControl::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControl::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControl::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControl::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROL_T045FEA605549ACF751298973DF50D16BE9A0F6DA_H
#ifndef VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#define VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlAlways
struct  VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlAlways::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlAlways::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROLALWAYS_TD8273ED584609B8826098FAF70DFAD95C1AFBA9E_H
#ifndef VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#define VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VisibilityControlStatic
struct  VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlStatic::m_objectNumber
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlStatic::m_vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;
	// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::m_originalMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_originalMatrix_8;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_objectNumber_4)); }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2 * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectNumber_4), value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_vectorLine_5)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_vectorLine_5), value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}

	inline static int32_t get_offset_of_m_originalMatrix_8() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B, ___m_originalMatrix_8)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_originalMatrix_8() const { return ___m_originalMatrix_8; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_originalMatrix_8() { return &___m_originalMatrix_8; }
	inline void set_m_originalMatrix_8(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_originalMatrix_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCONTROLSTATIC_T3834E5EE6724D1FEF309C33A74B62E2638D4777B_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#define TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_highlightColor_50;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_highlightPadding
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_highlightPadding_51;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_52;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_53;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  ___m_fontColorGradient_54;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_fontColorGradientPreset_55;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_56;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_57;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_spriteColor_59;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_60;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_faceColor_61;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_outlineColor_62;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_63;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_64;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_65;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_66;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___m_sizeStack_67;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_68;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_69;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___m_FontWeightStack_70;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_71;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_72;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_73;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_74;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_75;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_76;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_77;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___m_fontStyleStack_78;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_79;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_80;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_81;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___m_lineJustificationStack_82;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_textContainerLocalCorners_83;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_84;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_85;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_86;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_88;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_89;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_90;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_91;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_92;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_93;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_94;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_95;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_96;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_97;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_98;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_99;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_100;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_101;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_linkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_103;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_104;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_105;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_106;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_107;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_108;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_109;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_110;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_111;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_113;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_115;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_116;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_117;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_118;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_119;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_120;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_122;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_123;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_124;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_125;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_126;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_127;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margin_128;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_129;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_130;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_131;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_132;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_133;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___m_textInfo_134;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_135;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_136;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_137;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_138;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_139;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_140;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_141;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_142;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___m_spriteAnimator_143;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_144;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_145;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_146;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_147;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_148;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_149;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_LayoutElement_150;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_151;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_152;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_153;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_154;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_155;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_156;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_157;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_158;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_159;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_160;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_161;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_162;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_163;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_164;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_165;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_166;
	// TMPro.TMP_Text_TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_167;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_168;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_169;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_170;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_htmlTag_171;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* ___m_xmlAttribute_172;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_attributeParameterValues_173;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_174;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_175;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___m_indentStack_176;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_177;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_178;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_FXMatrix_179;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_180;
	// TMPro.TMP_Text_UnicodeChar[] TMPro.TMP_Text::m_TextParsingBuffer
	UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* ___m_TextParsingBuffer_181;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___m_internalCharacterInfo_182;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_input_CharArray_183;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_184;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_185;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedWordWrapState_186;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedLineState_187;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_188;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_191;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_192;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_193;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_194;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_195;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_196;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_197;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_198;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_199;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_200;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_201;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_202;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___m_meshExtents_203;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_htmlColor_204;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_colorStack_205;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_underlineColorStack_206;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_strikethroughColorStack_207;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_highlightColorStack_208;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_colorGradientPreset_209;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___m_colorGradientStack_210;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_211;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_212;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___m_styleStack_213;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___m_actionStack_214;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_215;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_216;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___m_baselineOffsetStack_217;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_218;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_219;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___m_cached_TextElement_220;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Underline_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Underline_Character_221;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Ellipsis_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Ellipsis_Character_222;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_223;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_currentSpriteAsset_224;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_225;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_226;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_227;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_228;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___k_Power_229;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sharedMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterial_36)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceStack_39)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor32_45)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor_46)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColor_48)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColor_49)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColor_50)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_highlightPadding_51() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightPadding_51)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_highlightPadding_51() const { return ___m_highlightPadding_51; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_highlightPadding_51() { return &___m_highlightPadding_51; }
	inline void set_m_highlightPadding_51(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_highlightPadding_51 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableVertexGradient_52)); }
	inline bool get_m_enableVertexGradient_52() const { return ___m_enableVertexGradient_52; }
	inline bool* get_address_of_m_enableVertexGradient_52() { return &___m_enableVertexGradient_52; }
	inline void set_m_enableVertexGradient_52(bool value)
	{
		___m_enableVertexGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_53() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorMode_53)); }
	inline int32_t get_m_colorMode_53() const { return ___m_colorMode_53; }
	inline int32_t* get_address_of_m_colorMode_53() { return &___m_colorMode_53; }
	inline void set_m_colorMode_53(int32_t value)
	{
		___m_colorMode_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_54() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradient_54)); }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  get_m_fontColorGradient_54() const { return ___m_fontColorGradient_54; }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A * get_address_of_m_fontColorGradient_54() { return &___m_fontColorGradient_54; }
	inline void set_m_fontColorGradient_54(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  value)
	{
		___m_fontColorGradient_54 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_55() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradientPreset_55)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_fontColorGradientPreset_55() const { return ___m_fontColorGradientPreset_55; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_fontColorGradientPreset_55() { return &___m_fontColorGradientPreset_55; }
	inline void set_m_fontColorGradientPreset_55(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_fontColorGradientPreset_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_55), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_56() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAsset_56)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_56() const { return ___m_spriteAsset_56; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_56() { return &___m_spriteAsset_56; }
	inline void set_m_spriteAsset_56(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_56), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_57() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintAllSprites_57)); }
	inline bool get_m_tintAllSprites_57() const { return ___m_tintAllSprites_57; }
	inline bool* get_address_of_m_tintAllSprites_57() { return &___m_tintAllSprites_57; }
	inline void set_m_tintAllSprites_57(bool value)
	{
		___m_tintAllSprites_57 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_58() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintSprite_58)); }
	inline bool get_m_tintSprite_58() const { return ___m_tintSprite_58; }
	inline bool* get_address_of_m_tintSprite_58() { return &___m_tintSprite_58; }
	inline void set_m_tintSprite_58(bool value)
	{
		___m_tintSprite_58 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteColor_59)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_spriteColor_59() const { return ___m_spriteColor_59; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_spriteColor_59() { return &___m_spriteColor_59; }
	inline void set_m_spriteColor_59(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_spriteColor_59 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_60() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overrideHtmlColors_60)); }
	inline bool get_m_overrideHtmlColors_60() const { return ___m_overrideHtmlColors_60; }
	inline bool* get_address_of_m_overrideHtmlColors_60() { return &___m_overrideHtmlColors_60; }
	inline void set_m_overrideHtmlColors_60(bool value)
	{
		___m_overrideHtmlColors_60 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_61() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_faceColor_61)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_faceColor_61() const { return ___m_faceColor_61; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_faceColor_61() { return &___m_faceColor_61; }
	inline void set_m_faceColor_61(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_faceColor_61 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_62() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineColor_62)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_outlineColor_62() const { return ___m_outlineColor_62; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_outlineColor_62() { return &___m_outlineColor_62; }
	inline void set_m_outlineColor_62(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_outlineColor_62 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_63() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineWidth_63)); }
	inline float get_m_outlineWidth_63() const { return ___m_outlineWidth_63; }
	inline float* get_address_of_m_outlineWidth_63() { return &___m_outlineWidth_63; }
	inline void set_m_outlineWidth_63(float value)
	{
		___m_outlineWidth_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_64() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSize_64)); }
	inline float get_m_fontSize_64() const { return ___m_fontSize_64; }
	inline float* get_address_of_m_fontSize_64() { return &___m_fontSize_64; }
	inline void set_m_fontSize_64(float value)
	{
		___m_fontSize_64 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_65() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontSize_65)); }
	inline float get_m_currentFontSize_65() const { return ___m_currentFontSize_65; }
	inline float* get_address_of_m_currentFontSize_65() { return &___m_currentFontSize_65; }
	inline void set_m_currentFontSize_65(float value)
	{
		___m_currentFontSize_65 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_66() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeBase_66)); }
	inline float get_m_fontSizeBase_66() const { return ___m_fontSizeBase_66; }
	inline float* get_address_of_m_fontSizeBase_66() { return &___m_fontSizeBase_66; }
	inline void set_m_fontSizeBase_66(float value)
	{
		___m_fontSizeBase_66 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_67() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sizeStack_67)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_m_sizeStack_67() const { return ___m_sizeStack_67; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_m_sizeStack_67() { return &___m_sizeStack_67; }
	inline void set_m_sizeStack_67(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___m_sizeStack_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_68() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeight_68)); }
	inline int32_t get_m_fontWeight_68() const { return ___m_fontWeight_68; }
	inline int32_t* get_address_of_m_fontWeight_68() { return &___m_fontWeight_68; }
	inline void set_m_fontWeight_68(int32_t value)
	{
		___m_fontWeight_68 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightInternal_69() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightInternal_69)); }
	inline int32_t get_m_FontWeightInternal_69() const { return ___m_FontWeightInternal_69; }
	inline int32_t* get_address_of_m_FontWeightInternal_69() { return &___m_FontWeightInternal_69; }
	inline void set_m_FontWeightInternal_69(int32_t value)
	{
		___m_FontWeightInternal_69 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightStack_70() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightStack_70)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_m_FontWeightStack_70() const { return ___m_FontWeightStack_70; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_m_FontWeightStack_70() { return &___m_FontWeightStack_70; }
	inline void set_m_FontWeightStack_70(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___m_FontWeightStack_70 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_71() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableAutoSizing_71)); }
	inline bool get_m_enableAutoSizing_71() const { return ___m_enableAutoSizing_71; }
	inline bool* get_address_of_m_enableAutoSizing_71() { return &___m_enableAutoSizing_71; }
	inline void set_m_enableAutoSizing_71(bool value)
	{
		___m_enableAutoSizing_71 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_72() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxFontSize_72)); }
	inline float get_m_maxFontSize_72() const { return ___m_maxFontSize_72; }
	inline float* get_address_of_m_maxFontSize_72() { return &___m_maxFontSize_72; }
	inline void set_m_maxFontSize_72(float value)
	{
		___m_maxFontSize_72 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_73() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minFontSize_73)); }
	inline float get_m_minFontSize_73() const { return ___m_minFontSize_73; }
	inline float* get_address_of_m_minFontSize_73() { return &___m_minFontSize_73; }
	inline void set_m_minFontSize_73(float value)
	{
		___m_minFontSize_73 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_74() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMin_74)); }
	inline float get_m_fontSizeMin_74() const { return ___m_fontSizeMin_74; }
	inline float* get_address_of_m_fontSizeMin_74() { return &___m_fontSizeMin_74; }
	inline void set_m_fontSizeMin_74(float value)
	{
		___m_fontSizeMin_74 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_75() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMax_75)); }
	inline float get_m_fontSizeMax_75() const { return ___m_fontSizeMax_75; }
	inline float* get_address_of_m_fontSizeMax_75() { return &___m_fontSizeMax_75; }
	inline void set_m_fontSizeMax_75(float value)
	{
		___m_fontSizeMax_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_76() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyle_76)); }
	inline int32_t get_m_fontStyle_76() const { return ___m_fontStyle_76; }
	inline int32_t* get_address_of_m_fontStyle_76() { return &___m_fontStyle_76; }
	inline void set_m_fontStyle_76(int32_t value)
	{
		___m_fontStyle_76 = value;
	}

	inline static int32_t get_offset_of_m_FontStyleInternal_77() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontStyleInternal_77)); }
	inline int32_t get_m_FontStyleInternal_77() const { return ___m_FontStyleInternal_77; }
	inline int32_t* get_address_of_m_FontStyleInternal_77() { return &___m_FontStyleInternal_77; }
	inline void set_m_FontStyleInternal_77(int32_t value)
	{
		___m_FontStyleInternal_77 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyleStack_78)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_m_fontStyleStack_78() const { return ___m_fontStyleStack_78; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_m_fontStyleStack_78() { return &___m_fontStyleStack_78; }
	inline void set_m_fontStyleStack_78(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___m_fontStyleStack_78 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_79() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingBold_79)); }
	inline bool get_m_isUsingBold_79() const { return ___m_isUsingBold_79; }
	inline bool* get_address_of_m_isUsingBold_79() { return &___m_isUsingBold_79; }
	inline void set_m_isUsingBold_79(bool value)
	{
		___m_isUsingBold_79 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_80() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textAlignment_80)); }
	inline int32_t get_m_textAlignment_80() const { return ___m_textAlignment_80; }
	inline int32_t* get_address_of_m_textAlignment_80() { return &___m_textAlignment_80; }
	inline void set_m_textAlignment_80(int32_t value)
	{
		___m_textAlignment_80 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_81() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustification_81)); }
	inline int32_t get_m_lineJustification_81() const { return ___m_lineJustification_81; }
	inline int32_t* get_address_of_m_lineJustification_81() { return &___m_lineJustification_81; }
	inline void set_m_lineJustification_81(int32_t value)
	{
		___m_lineJustification_81 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_82() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustificationStack_82)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_m_lineJustificationStack_82() const { return ___m_lineJustificationStack_82; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_m_lineJustificationStack_82() { return &___m_lineJustificationStack_82; }
	inline void set_m_lineJustificationStack_82(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___m_lineJustificationStack_82 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_83() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textContainerLocalCorners_83)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_textContainerLocalCorners_83() const { return ___m_textContainerLocalCorners_83; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_textContainerLocalCorners_83() { return &___m_textContainerLocalCorners_83; }
	inline void set_m_textContainerLocalCorners_83(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_textContainerLocalCorners_83 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_83), value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterSpacing_84)); }
	inline float get_m_characterSpacing_84() const { return ___m_characterSpacing_84; }
	inline float* get_address_of_m_characterSpacing_84() { return &___m_characterSpacing_84; }
	inline void set_m_characterSpacing_84(float value)
	{
		___m_characterSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cSpacing_85)); }
	inline float get_m_cSpacing_85() const { return ___m_cSpacing_85; }
	inline float* get_address_of_m_cSpacing_85() { return &___m_cSpacing_85; }
	inline void set_m_cSpacing_85(float value)
	{
		___m_cSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_monoSpacing_86)); }
	inline float get_m_monoSpacing_86() const { return ___m_monoSpacing_86; }
	inline float* get_address_of_m_monoSpacing_86() { return &___m_monoSpacing_86; }
	inline void set_m_monoSpacing_86(float value)
	{
		___m_monoSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordSpacing_87)); }
	inline float get_m_wordSpacing_87() const { return ___m_wordSpacing_87; }
	inline float* get_address_of_m_wordSpacing_87() { return &___m_wordSpacing_87; }
	inline void set_m_wordSpacing_87(float value)
	{
		___m_wordSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_88() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacing_88)); }
	inline float get_m_lineSpacing_88() const { return ___m_lineSpacing_88; }
	inline float* get_address_of_m_lineSpacing_88() { return &___m_lineSpacing_88; }
	inline void set_m_lineSpacing_88(float value)
	{
		___m_lineSpacing_88 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_89() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingDelta_89)); }
	inline float get_m_lineSpacingDelta_89() const { return ___m_lineSpacingDelta_89; }
	inline float* get_address_of_m_lineSpacingDelta_89() { return &___m_lineSpacingDelta_89; }
	inline void set_m_lineSpacingDelta_89(float value)
	{
		___m_lineSpacingDelta_89 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_90() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineHeight_90)); }
	inline float get_m_lineHeight_90() const { return ___m_lineHeight_90; }
	inline float* get_address_of_m_lineHeight_90() { return &___m_lineHeight_90; }
	inline void set_m_lineHeight_90(float value)
	{
		___m_lineHeight_90 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_91() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingMax_91)); }
	inline float get_m_lineSpacingMax_91() const { return ___m_lineSpacingMax_91; }
	inline float* get_address_of_m_lineSpacingMax_91() { return &___m_lineSpacingMax_91; }
	inline void set_m_lineSpacingMax_91(float value)
	{
		___m_lineSpacingMax_91 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_92() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_paragraphSpacing_92)); }
	inline float get_m_paragraphSpacing_92() const { return ___m_paragraphSpacing_92; }
	inline float* get_address_of_m_paragraphSpacing_92() { return &___m_paragraphSpacing_92; }
	inline void set_m_paragraphSpacing_92(float value)
	{
		___m_paragraphSpacing_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_93() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthMaxAdj_93)); }
	inline float get_m_charWidthMaxAdj_93() const { return ___m_charWidthMaxAdj_93; }
	inline float* get_address_of_m_charWidthMaxAdj_93() { return &___m_charWidthMaxAdj_93; }
	inline void set_m_charWidthMaxAdj_93(float value)
	{
		___m_charWidthMaxAdj_93 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_94() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthAdjDelta_94)); }
	inline float get_m_charWidthAdjDelta_94() const { return ___m_charWidthAdjDelta_94; }
	inline float* get_address_of_m_charWidthAdjDelta_94() { return &___m_charWidthAdjDelta_94; }
	inline void set_m_charWidthAdjDelta_94(float value)
	{
		___m_charWidthAdjDelta_94 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableWordWrapping_95)); }
	inline bool get_m_enableWordWrapping_95() const { return ___m_enableWordWrapping_95; }
	inline bool* get_address_of_m_enableWordWrapping_95() { return &___m_enableWordWrapping_95; }
	inline void set_m_enableWordWrapping_95(bool value)
	{
		___m_enableWordWrapping_95 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_96() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCharacterWrappingEnabled_96)); }
	inline bool get_m_isCharacterWrappingEnabled_96() const { return ___m_isCharacterWrappingEnabled_96; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_96() { return &___m_isCharacterWrappingEnabled_96; }
	inline void set_m_isCharacterWrappingEnabled_96(bool value)
	{
		___m_isCharacterWrappingEnabled_96 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_97() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNonBreakingSpace_97)); }
	inline bool get_m_isNonBreakingSpace_97() const { return ___m_isNonBreakingSpace_97; }
	inline bool* get_address_of_m_isNonBreakingSpace_97() { return &___m_isNonBreakingSpace_97; }
	inline void set_m_isNonBreakingSpace_97(bool value)
	{
		___m_isNonBreakingSpace_97 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_98() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isIgnoringAlignment_98)); }
	inline bool get_m_isIgnoringAlignment_98() const { return ___m_isIgnoringAlignment_98; }
	inline bool* get_address_of_m_isIgnoringAlignment_98() { return &___m_isIgnoringAlignment_98; }
	inline void set_m_isIgnoringAlignment_98(bool value)
	{
		___m_isIgnoringAlignment_98 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_99() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordWrappingRatios_99)); }
	inline float get_m_wordWrappingRatios_99() const { return ___m_wordWrappingRatios_99; }
	inline float* get_address_of_m_wordWrappingRatios_99() { return &___m_wordWrappingRatios_99; }
	inline void set_m_wordWrappingRatios_99(float value)
	{
		___m_wordWrappingRatios_99 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_100() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overflowMode_100)); }
	inline int32_t get_m_overflowMode_100() const { return ___m_overflowMode_100; }
	inline int32_t* get_address_of_m_overflowMode_100() { return &___m_overflowMode_100; }
	inline void set_m_overflowMode_100(int32_t value)
	{
		___m_overflowMode_100 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_101() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstOverflowCharacterIndex_101)); }
	inline int32_t get_m_firstOverflowCharacterIndex_101() const { return ___m_firstOverflowCharacterIndex_101; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_101() { return &___m_firstOverflowCharacterIndex_101; }
	inline void set_m_firstOverflowCharacterIndex_101(int32_t value)
	{
		___m_firstOverflowCharacterIndex_101 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_linkedTextComponent_102)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_linkedTextComponent_102() const { return ___m_linkedTextComponent_102; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_linkedTextComponent_102() { return &___m_linkedTextComponent_102; }
	inline void set_m_linkedTextComponent_102(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_linkedTextComponent_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_102), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_103() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLinkedTextComponent_103)); }
	inline bool get_m_isLinkedTextComponent_103() const { return ___m_isLinkedTextComponent_103; }
	inline bool* get_address_of_m_isLinkedTextComponent_103() { return &___m_isLinkedTextComponent_103; }
	inline void set_m_isLinkedTextComponent_103(bool value)
	{
		___m_isLinkedTextComponent_103 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_104() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isTextTruncated_104)); }
	inline bool get_m_isTextTruncated_104() const { return ___m_isTextTruncated_104; }
	inline bool* get_address_of_m_isTextTruncated_104() { return &___m_isTextTruncated_104; }
	inline void set_m_isTextTruncated_104(bool value)
	{
		___m_isTextTruncated_104 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_105() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableKerning_105)); }
	inline bool get_m_enableKerning_105() const { return ___m_enableKerning_105; }
	inline bool* get_address_of_m_enableKerning_105() { return &___m_enableKerning_105; }
	inline void set_m_enableKerning_105(bool value)
	{
		___m_enableKerning_105 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_106() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableExtraPadding_106)); }
	inline bool get_m_enableExtraPadding_106() const { return ___m_enableExtraPadding_106; }
	inline bool* get_address_of_m_enableExtraPadding_106() { return &___m_enableExtraPadding_106; }
	inline void set_m_enableExtraPadding_106(bool value)
	{
		___m_enableExtraPadding_106 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_107() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___checkPaddingRequired_107)); }
	inline bool get_checkPaddingRequired_107() const { return ___checkPaddingRequired_107; }
	inline bool* get_address_of_checkPaddingRequired_107() { return &___checkPaddingRequired_107; }
	inline void set_checkPaddingRequired_107(bool value)
	{
		___checkPaddingRequired_107 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_108() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRichText_108)); }
	inline bool get_m_isRichText_108() const { return ___m_isRichText_108; }
	inline bool* get_address_of_m_isRichText_108() { return &___m_isRichText_108; }
	inline void set_m_isRichText_108(bool value)
	{
		___m_isRichText_108 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_109() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_parseCtrlCharacters_109)); }
	inline bool get_m_parseCtrlCharacters_109() const { return ___m_parseCtrlCharacters_109; }
	inline bool* get_address_of_m_parseCtrlCharacters_109() { return &___m_parseCtrlCharacters_109; }
	inline void set_m_parseCtrlCharacters_109(bool value)
	{
		___m_parseCtrlCharacters_109 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_110() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOverlay_110)); }
	inline bool get_m_isOverlay_110() const { return ___m_isOverlay_110; }
	inline bool* get_address_of_m_isOverlay_110() { return &___m_isOverlay_110; }
	inline void set_m_isOverlay_110(bool value)
	{
		___m_isOverlay_110 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_111() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOrthographic_111)); }
	inline bool get_m_isOrthographic_111() const { return ___m_isOrthographic_111; }
	inline bool* get_address_of_m_isOrthographic_111() { return &___m_isOrthographic_111; }
	inline void set_m_isOrthographic_111(bool value)
	{
		___m_isOrthographic_111 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_112() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCullingEnabled_112)); }
	inline bool get_m_isCullingEnabled_112() const { return ___m_isCullingEnabled_112; }
	inline bool* get_address_of_m_isCullingEnabled_112() { return &___m_isCullingEnabled_112; }
	inline void set_m_isCullingEnabled_112(bool value)
	{
		___m_isCullingEnabled_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreRectMaskCulling_113)); }
	inline bool get_m_ignoreRectMaskCulling_113() const { return ___m_ignoreRectMaskCulling_113; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_113() { return &___m_ignoreRectMaskCulling_113; }
	inline void set_m_ignoreRectMaskCulling_113(bool value)
	{
		___m_ignoreRectMaskCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_114() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreCulling_114)); }
	inline bool get_m_ignoreCulling_114() const { return ___m_ignoreCulling_114; }
	inline bool* get_address_of_m_ignoreCulling_114() { return &___m_ignoreCulling_114; }
	inline void set_m_ignoreCulling_114(bool value)
	{
		___m_ignoreCulling_114 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_horizontalMapping_115)); }
	inline int32_t get_m_horizontalMapping_115() const { return ___m_horizontalMapping_115; }
	inline int32_t* get_address_of_m_horizontalMapping_115() { return &___m_horizontalMapping_115; }
	inline void set_m_horizontalMapping_115(int32_t value)
	{
		___m_horizontalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_116() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticalMapping_116)); }
	inline int32_t get_m_verticalMapping_116() const { return ___m_verticalMapping_116; }
	inline int32_t* get_address_of_m_verticalMapping_116() { return &___m_verticalMapping_116; }
	inline void set_m_verticalMapping_116(int32_t value)
	{
		___m_verticalMapping_116 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_117() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_uvLineOffset_117)); }
	inline float get_m_uvLineOffset_117() const { return ___m_uvLineOffset_117; }
	inline float* get_address_of_m_uvLineOffset_117() { return &___m_uvLineOffset_117; }
	inline void set_m_uvLineOffset_117(float value)
	{
		___m_uvLineOffset_117 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_118() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderMode_118)); }
	inline int32_t get_m_renderMode_118() const { return ___m_renderMode_118; }
	inline int32_t* get_address_of_m_renderMode_118() { return &___m_renderMode_118; }
	inline void set_m_renderMode_118(int32_t value)
	{
		___m_renderMode_118 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_119() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_geometrySortingOrder_119)); }
	inline int32_t get_m_geometrySortingOrder_119() const { return ___m_geometrySortingOrder_119; }
	inline int32_t* get_address_of_m_geometrySortingOrder_119() { return &___m_geometrySortingOrder_119; }
	inline void set_m_geometrySortingOrder_119(int32_t value)
	{
		___m_geometrySortingOrder_119 = value;
	}

	inline static int32_t get_offset_of_m_VertexBufferAutoSizeReduction_120() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_VertexBufferAutoSizeReduction_120)); }
	inline bool get_m_VertexBufferAutoSizeReduction_120() const { return ___m_VertexBufferAutoSizeReduction_120; }
	inline bool* get_address_of_m_VertexBufferAutoSizeReduction_120() { return &___m_VertexBufferAutoSizeReduction_120; }
	inline void set_m_VertexBufferAutoSizeReduction_120(bool value)
	{
		___m_VertexBufferAutoSizeReduction_120 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_121() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacter_121)); }
	inline int32_t get_m_firstVisibleCharacter_121() const { return ___m_firstVisibleCharacter_121; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_121() { return &___m_firstVisibleCharacter_121; }
	inline void set_m_firstVisibleCharacter_121(int32_t value)
	{
		___m_firstVisibleCharacter_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_122() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleCharacters_122)); }
	inline int32_t get_m_maxVisibleCharacters_122() const { return ___m_maxVisibleCharacters_122; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_122() { return &___m_maxVisibleCharacters_122; }
	inline void set_m_maxVisibleCharacters_122(int32_t value)
	{
		___m_maxVisibleCharacters_122 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_123() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleWords_123)); }
	inline int32_t get_m_maxVisibleWords_123() const { return ___m_maxVisibleWords_123; }
	inline int32_t* get_address_of_m_maxVisibleWords_123() { return &___m_maxVisibleWords_123; }
	inline void set_m_maxVisibleWords_123(int32_t value)
	{
		___m_maxVisibleWords_123 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_124() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleLines_124)); }
	inline int32_t get_m_maxVisibleLines_124() const { return ___m_maxVisibleLines_124; }
	inline int32_t* get_address_of_m_maxVisibleLines_124() { return &___m_maxVisibleLines_124; }
	inline void set_m_maxVisibleLines_124(int32_t value)
	{
		___m_maxVisibleLines_124 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_125() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_useMaxVisibleDescender_125)); }
	inline bool get_m_useMaxVisibleDescender_125() const { return ___m_useMaxVisibleDescender_125; }
	inline bool* get_address_of_m_useMaxVisibleDescender_125() { return &___m_useMaxVisibleDescender_125; }
	inline void set_m_useMaxVisibleDescender_125(bool value)
	{
		___m_useMaxVisibleDescender_125 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_126() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageToDisplay_126)); }
	inline int32_t get_m_pageToDisplay_126() const { return ___m_pageToDisplay_126; }
	inline int32_t* get_address_of_m_pageToDisplay_126() { return &___m_pageToDisplay_126; }
	inline void set_m_pageToDisplay_126(int32_t value)
	{
		___m_pageToDisplay_126 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_127() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNewPage_127)); }
	inline bool get_m_isNewPage_127() const { return ___m_isNewPage_127; }
	inline bool* get_address_of_m_isNewPage_127() { return &___m_isNewPage_127; }
	inline void set_m_isNewPage_127(bool value)
	{
		___m_isNewPage_127 = value;
	}

	inline static int32_t get_offset_of_m_margin_128() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_margin_128)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margin_128() const { return ___m_margin_128; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margin_128() { return &___m_margin_128; }
	inline void set_m_margin_128(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margin_128 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_129() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginLeft_129)); }
	inline float get_m_marginLeft_129() const { return ___m_marginLeft_129; }
	inline float* get_address_of_m_marginLeft_129() { return &___m_marginLeft_129; }
	inline void set_m_marginLeft_129(float value)
	{
		___m_marginLeft_129 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginRight_130)); }
	inline float get_m_marginRight_130() const { return ___m_marginRight_130; }
	inline float* get_address_of_m_marginRight_130() { return &___m_marginRight_130; }
	inline void set_m_marginRight_130(float value)
	{
		___m_marginRight_130 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_131() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginWidth_131)); }
	inline float get_m_marginWidth_131() const { return ___m_marginWidth_131; }
	inline float* get_address_of_m_marginWidth_131() { return &___m_marginWidth_131; }
	inline void set_m_marginWidth_131(float value)
	{
		___m_marginWidth_131 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_132() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginHeight_132)); }
	inline float get_m_marginHeight_132() const { return ___m_marginHeight_132; }
	inline float* get_address_of_m_marginHeight_132() { return &___m_marginHeight_132; }
	inline void set_m_marginHeight_132(float value)
	{
		___m_marginHeight_132 = value;
	}

	inline static int32_t get_offset_of_m_width_133() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_width_133)); }
	inline float get_m_width_133() const { return ___m_width_133; }
	inline float* get_address_of_m_width_133() { return &___m_width_133; }
	inline void set_m_width_133(float value)
	{
		___m_width_133 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_134() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textInfo_134)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_m_textInfo_134() const { return ___m_textInfo_134; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_m_textInfo_134() { return &___m_textInfo_134; }
	inline void set_m_textInfo_134(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___m_textInfo_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_134), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_135() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_havePropertiesChanged_135)); }
	inline bool get_m_havePropertiesChanged_135() const { return ___m_havePropertiesChanged_135; }
	inline bool* get_address_of_m_havePropertiesChanged_135() { return &___m_havePropertiesChanged_135; }
	inline void set_m_havePropertiesChanged_135(bool value)
	{
		___m_havePropertiesChanged_135 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_136() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingLegacyAnimationComponent_136)); }
	inline bool get_m_isUsingLegacyAnimationComponent_136() const { return ___m_isUsingLegacyAnimationComponent_136; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_136() { return &___m_isUsingLegacyAnimationComponent_136; }
	inline void set_m_isUsingLegacyAnimationComponent_136(bool value)
	{
		___m_isUsingLegacyAnimationComponent_136 = value;
	}

	inline static int32_t get_offset_of_m_transform_137() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_transform_137)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_137() const { return ___m_transform_137; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_137() { return &___m_transform_137; }
	inline void set_m_transform_137(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_137), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_138() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_rectTransform_138)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_138() const { return ___m_rectTransform_138; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_138() { return &___m_rectTransform_138; }
	inline void set_m_rectTransform_138(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_138 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_138), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___U3CautoSizeTextContainerU3Ek__BackingField_139)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_139() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return &___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_139(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_139 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_140() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_autoSizeTextContainer_140)); }
	inline bool get_m_autoSizeTextContainer_140() const { return ___m_autoSizeTextContainer_140; }
	inline bool* get_address_of_m_autoSizeTextContainer_140() { return &___m_autoSizeTextContainer_140; }
	inline void set_m_autoSizeTextContainer_140(bool value)
	{
		___m_autoSizeTextContainer_140 = value;
	}

	inline static int32_t get_offset_of_m_mesh_141() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_mesh_141)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_141() const { return ___m_mesh_141; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_141() { return &___m_mesh_141; }
	inline void set_m_mesh_141(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_141), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_142() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isVolumetricText_142)); }
	inline bool get_m_isVolumetricText_142() const { return ___m_isVolumetricText_142; }
	inline bool* get_address_of_m_isVolumetricText_142() { return &___m_isVolumetricText_142; }
	inline void set_m_isVolumetricText_142(bool value)
	{
		___m_isVolumetricText_142 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_143() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimator_143)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_m_spriteAnimator_143() const { return ___m_spriteAnimator_143; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_m_spriteAnimator_143() { return &___m_spriteAnimator_143; }
	inline void set_m_spriteAnimator_143(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___m_spriteAnimator_143 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_143), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_144() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleHeight_144)); }
	inline float get_m_flexibleHeight_144() const { return ___m_flexibleHeight_144; }
	inline float* get_address_of_m_flexibleHeight_144() { return &___m_flexibleHeight_144; }
	inline void set_m_flexibleHeight_144(float value)
	{
		___m_flexibleHeight_144 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_145() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleWidth_145)); }
	inline float get_m_flexibleWidth_145() const { return ___m_flexibleWidth_145; }
	inline float* get_address_of_m_flexibleWidth_145() { return &___m_flexibleWidth_145; }
	inline void set_m_flexibleWidth_145(float value)
	{
		___m_flexibleWidth_145 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minWidth_146)); }
	inline float get_m_minWidth_146() const { return ___m_minWidth_146; }
	inline float* get_address_of_m_minWidth_146() { return &___m_minWidth_146; }
	inline void set_m_minWidth_146(float value)
	{
		___m_minWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minHeight_147)); }
	inline float get_m_minHeight_147() const { return ___m_minHeight_147; }
	inline float* get_address_of_m_minHeight_147() { return &___m_minHeight_147; }
	inline void set_m_minHeight_147(float value)
	{
		___m_minHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxWidth_148)); }
	inline float get_m_maxWidth_148() const { return ___m_maxWidth_148; }
	inline float* get_address_of_m_maxWidth_148() { return &___m_maxWidth_148; }
	inline void set_m_maxWidth_148(float value)
	{
		___m_maxWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_149() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxHeight_149)); }
	inline float get_m_maxHeight_149() const { return ___m_maxHeight_149; }
	inline float* get_address_of_m_maxHeight_149() { return &___m_maxHeight_149; }
	inline void set_m_maxHeight_149(float value)
	{
		___m_maxHeight_149 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_150() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_LayoutElement_150)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_LayoutElement_150() const { return ___m_LayoutElement_150; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_LayoutElement_150() { return &___m_LayoutElement_150; }
	inline void set_m_LayoutElement_150(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_LayoutElement_150 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_150), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_151() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredWidth_151)); }
	inline float get_m_preferredWidth_151() const { return ___m_preferredWidth_151; }
	inline float* get_address_of_m_preferredWidth_151() { return &___m_preferredWidth_151; }
	inline void set_m_preferredWidth_151(float value)
	{
		___m_preferredWidth_151 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_152() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedWidth_152)); }
	inline float get_m_renderedWidth_152() const { return ___m_renderedWidth_152; }
	inline float* get_address_of_m_renderedWidth_152() { return &___m_renderedWidth_152; }
	inline void set_m_renderedWidth_152(float value)
	{
		___m_renderedWidth_152 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_153() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredWidthDirty_153)); }
	inline bool get_m_isPreferredWidthDirty_153() const { return ___m_isPreferredWidthDirty_153; }
	inline bool* get_address_of_m_isPreferredWidthDirty_153() { return &___m_isPreferredWidthDirty_153; }
	inline void set_m_isPreferredWidthDirty_153(bool value)
	{
		___m_isPreferredWidthDirty_153 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_154() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredHeight_154)); }
	inline float get_m_preferredHeight_154() const { return ___m_preferredHeight_154; }
	inline float* get_address_of_m_preferredHeight_154() { return &___m_preferredHeight_154; }
	inline void set_m_preferredHeight_154(float value)
	{
		___m_preferredHeight_154 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_155() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedHeight_155)); }
	inline float get_m_renderedHeight_155() const { return ___m_renderedHeight_155; }
	inline float* get_address_of_m_renderedHeight_155() { return &___m_renderedHeight_155; }
	inline void set_m_renderedHeight_155(float value)
	{
		___m_renderedHeight_155 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_156() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredHeightDirty_156)); }
	inline bool get_m_isPreferredHeightDirty_156() const { return ___m_isPreferredHeightDirty_156; }
	inline bool* get_address_of_m_isPreferredHeightDirty_156() { return &___m_isPreferredHeightDirty_156; }
	inline void set_m_isPreferredHeightDirty_156(bool value)
	{
		___m_isPreferredHeightDirty_156 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_157() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculatingPreferredValues_157)); }
	inline bool get_m_isCalculatingPreferredValues_157() const { return ___m_isCalculatingPreferredValues_157; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_157() { return &___m_isCalculatingPreferredValues_157; }
	inline void set_m_isCalculatingPreferredValues_157(bool value)
	{
		___m_isCalculatingPreferredValues_157 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_158() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_recursiveCount_158)); }
	inline int32_t get_m_recursiveCount_158() const { return ___m_recursiveCount_158; }
	inline int32_t* get_address_of_m_recursiveCount_158() { return &___m_recursiveCount_158; }
	inline void set_m_recursiveCount_158(int32_t value)
	{
		___m_recursiveCount_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_159() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutPriority_159)); }
	inline int32_t get_m_layoutPriority_159() const { return ___m_layoutPriority_159; }
	inline int32_t* get_address_of_m_layoutPriority_159() { return &___m_layoutPriority_159; }
	inline void set_m_layoutPriority_159(int32_t value)
	{
		___m_layoutPriority_159 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_160() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculateSizeRequired_160)); }
	inline bool get_m_isCalculateSizeRequired_160() const { return ___m_isCalculateSizeRequired_160; }
	inline bool* get_address_of_m_isCalculateSizeRequired_160() { return &___m_isCalculateSizeRequired_160; }
	inline void set_m_isCalculateSizeRequired_160(bool value)
	{
		___m_isCalculateSizeRequired_160 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLayoutDirty_161)); }
	inline bool get_m_isLayoutDirty_161() const { return ___m_isLayoutDirty_161; }
	inline bool* get_address_of_m_isLayoutDirty_161() { return &___m_isLayoutDirty_161; }
	inline void set_m_isLayoutDirty_161(bool value)
	{
		___m_isLayoutDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_162() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticesAlreadyDirty_162)); }
	inline bool get_m_verticesAlreadyDirty_162() const { return ___m_verticesAlreadyDirty_162; }
	inline bool* get_address_of_m_verticesAlreadyDirty_162() { return &___m_verticesAlreadyDirty_162; }
	inline void set_m_verticesAlreadyDirty_162(bool value)
	{
		___m_verticesAlreadyDirty_162 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_163() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutAlreadyDirty_163)); }
	inline bool get_m_layoutAlreadyDirty_163() const { return ___m_layoutAlreadyDirty_163; }
	inline bool* get_address_of_m_layoutAlreadyDirty_163() { return &___m_layoutAlreadyDirty_163; }
	inline void set_m_layoutAlreadyDirty_163(bool value)
	{
		___m_layoutAlreadyDirty_163 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_164() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAwake_164)); }
	inline bool get_m_isAwake_164() const { return ___m_isAwake_164; }
	inline bool* get_address_of_m_isAwake_164() { return &___m_isAwake_164; }
	inline void set_m_isAwake_164(bool value)
	{
		___m_isAwake_164 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_165() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isWaitingOnResourceLoad_165)); }
	inline bool get_m_isWaitingOnResourceLoad_165() const { return ___m_isWaitingOnResourceLoad_165; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_165() { return &___m_isWaitingOnResourceLoad_165; }
	inline void set_m_isWaitingOnResourceLoad_165(bool value)
	{
		___m_isWaitingOnResourceLoad_165 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_166() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isInputParsingRequired_166)); }
	inline bool get_m_isInputParsingRequired_166() const { return ___m_isInputParsingRequired_166; }
	inline bool* get_address_of_m_isInputParsingRequired_166() { return &___m_isInputParsingRequired_166; }
	inline void set_m_isInputParsingRequired_166(bool value)
	{
		___m_isInputParsingRequired_166 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_167() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_inputSource_167)); }
	inline int32_t get_m_inputSource_167() const { return ___m_inputSource_167; }
	inline int32_t* get_address_of_m_inputSource_167() { return &___m_inputSource_167; }
	inline void set_m_inputSource_167(int32_t value)
	{
		___m_inputSource_167 = value;
	}

	inline static int32_t get_offset_of_old_text_168() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___old_text_168)); }
	inline String_t* get_old_text_168() const { return ___old_text_168; }
	inline String_t** get_address_of_old_text_168() { return &___old_text_168; }
	inline void set_old_text_168(String_t* value)
	{
		___old_text_168 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_168), value);
	}

	inline static int32_t get_offset_of_m_fontScale_169() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScale_169)); }
	inline float get_m_fontScale_169() const { return ___m_fontScale_169; }
	inline float* get_address_of_m_fontScale_169() { return &___m_fontScale_169; }
	inline void set_m_fontScale_169(float value)
	{
		___m_fontScale_169 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_170() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScaleMultiplier_170)); }
	inline float get_m_fontScaleMultiplier_170() const { return ___m_fontScaleMultiplier_170; }
	inline float* get_address_of_m_fontScaleMultiplier_170() { return &___m_fontScaleMultiplier_170; }
	inline void set_m_fontScaleMultiplier_170(float value)
	{
		___m_fontScaleMultiplier_170 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_171() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlTag_171)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_htmlTag_171() const { return ___m_htmlTag_171; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_htmlTag_171() { return &___m_htmlTag_171; }
	inline void set_m_htmlTag_171(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_htmlTag_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_171), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_172() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xmlAttribute_172)); }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* get_m_xmlAttribute_172() const { return ___m_xmlAttribute_172; }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652** get_address_of_m_xmlAttribute_172() { return &___m_xmlAttribute_172; }
	inline void set_m_xmlAttribute_172(RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* value)
	{
		___m_xmlAttribute_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_172), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_173() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_attributeParameterValues_173)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_attributeParameterValues_173() const { return ___m_attributeParameterValues_173; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_attributeParameterValues_173() { return &___m_attributeParameterValues_173; }
	inline void set_m_attributeParameterValues_173(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_attributeParameterValues_173 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_173), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_174() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_LineIndent_174)); }
	inline float get_tag_LineIndent_174() const { return ___tag_LineIndent_174; }
	inline float* get_address_of_tag_LineIndent_174() { return &___tag_LineIndent_174; }
	inline void set_tag_LineIndent_174(float value)
	{
		___tag_LineIndent_174 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_175() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_Indent_175)); }
	inline float get_tag_Indent_175() const { return ___tag_Indent_175; }
	inline float* get_address_of_tag_Indent_175() { return &___tag_Indent_175; }
	inline void set_tag_Indent_175(float value)
	{
		___tag_Indent_175 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_176() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_indentStack_176)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_m_indentStack_176() const { return ___m_indentStack_176; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_m_indentStack_176() { return &___m_indentStack_176; }
	inline void set_m_indentStack_176(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___m_indentStack_176 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_177() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_NoParsing_177)); }
	inline bool get_tag_NoParsing_177() const { return ___tag_NoParsing_177; }
	inline bool* get_address_of_tag_NoParsing_177() { return &___tag_NoParsing_177; }
	inline void set_tag_NoParsing_177(bool value)
	{
		___tag_NoParsing_177 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_178() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isParsingText_178)); }
	inline bool get_m_isParsingText_178() const { return ___m_isParsingText_178; }
	inline bool* get_address_of_m_isParsingText_178() { return &___m_isParsingText_178; }
	inline void set_m_isParsingText_178(bool value)
	{
		___m_isParsingText_178 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_179() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FXMatrix_179)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_FXMatrix_179() const { return ___m_FXMatrix_179; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_FXMatrix_179() { return &___m_FXMatrix_179; }
	inline void set_m_FXMatrix_179(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_FXMatrix_179 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_180() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isFXMatrixSet_180)); }
	inline bool get_m_isFXMatrixSet_180() const { return ___m_isFXMatrixSet_180; }
	inline bool* get_address_of_m_isFXMatrixSet_180() { return &___m_isFXMatrixSet_180; }
	inline void set_m_isFXMatrixSet_180(bool value)
	{
		___m_isFXMatrixSet_180 = value;
	}

	inline static int32_t get_offset_of_m_TextParsingBuffer_181() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_TextParsingBuffer_181)); }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* get_m_TextParsingBuffer_181() const { return ___m_TextParsingBuffer_181; }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505** get_address_of_m_TextParsingBuffer_181() { return &___m_TextParsingBuffer_181; }
	inline void set_m_TextParsingBuffer_181(UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* value)
	{
		___m_TextParsingBuffer_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextParsingBuffer_181), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_182() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_internalCharacterInfo_182)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_m_internalCharacterInfo_182() const { return ___m_internalCharacterInfo_182; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_m_internalCharacterInfo_182() { return &___m_internalCharacterInfo_182; }
	inline void set_m_internalCharacterInfo_182(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___m_internalCharacterInfo_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_182), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_183() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_input_CharArray_183)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_input_CharArray_183() const { return ___m_input_CharArray_183; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_input_CharArray_183() { return &___m_input_CharArray_183; }
	inline void set_m_input_CharArray_183(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_input_CharArray_183 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_183), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_184() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charArray_Length_184)); }
	inline int32_t get_m_charArray_Length_184() const { return ___m_charArray_Length_184; }
	inline int32_t* get_address_of_m_charArray_Length_184() { return &___m_charArray_Length_184; }
	inline void set_m_charArray_Length_184(int32_t value)
	{
		___m_charArray_Length_184 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_185() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_totalCharacterCount_185)); }
	inline int32_t get_m_totalCharacterCount_185() const { return ___m_totalCharacterCount_185; }
	inline int32_t* get_address_of_m_totalCharacterCount_185() { return &___m_totalCharacterCount_185; }
	inline void set_m_totalCharacterCount_185(int32_t value)
	{
		___m_totalCharacterCount_185 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_186() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedWordWrapState_186)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedWordWrapState_186() const { return ___m_SavedWordWrapState_186; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedWordWrapState_186() { return &___m_SavedWordWrapState_186; }
	inline void set_m_SavedWordWrapState_186(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedWordWrapState_186 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_187() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedLineState_187)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedLineState_187() const { return ___m_SavedLineState_187; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedLineState_187() { return &___m_SavedLineState_187; }
	inline void set_m_SavedLineState_187(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedLineState_187 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_188() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterCount_188)); }
	inline int32_t get_m_characterCount_188() const { return ___m_characterCount_188; }
	inline int32_t* get_address_of_m_characterCount_188() { return &___m_characterCount_188; }
	inline void set_m_characterCount_188(int32_t value)
	{
		___m_characterCount_188 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstCharacterOfLine_189)); }
	inline int32_t get_m_firstCharacterOfLine_189() const { return ___m_firstCharacterOfLine_189; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_189() { return &___m_firstCharacterOfLine_189; }
	inline void set_m_firstCharacterOfLine_189(int32_t value)
	{
		___m_firstCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacterOfLine_190)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_190() const { return ___m_firstVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_190() { return &___m_firstVisibleCharacterOfLine_190; }
	inline void set_m_firstVisibleCharacterOfLine_190(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_191() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastCharacterOfLine_191)); }
	inline int32_t get_m_lastCharacterOfLine_191() const { return ___m_lastCharacterOfLine_191; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_191() { return &___m_lastCharacterOfLine_191; }
	inline void set_m_lastCharacterOfLine_191(int32_t value)
	{
		___m_lastCharacterOfLine_191 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_192() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastVisibleCharacterOfLine_192)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_192() const { return ___m_lastVisibleCharacterOfLine_192; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_192() { return &___m_lastVisibleCharacterOfLine_192; }
	inline void set_m_lastVisibleCharacterOfLine_192(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_192 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineNumber_193)); }
	inline int32_t get_m_lineNumber_193() const { return ___m_lineNumber_193; }
	inline int32_t* get_address_of_m_lineNumber_193() { return &___m_lineNumber_193; }
	inline void set_m_lineNumber_193(int32_t value)
	{
		___m_lineNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_194() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineVisibleCharacterCount_194)); }
	inline int32_t get_m_lineVisibleCharacterCount_194() const { return ___m_lineVisibleCharacterCount_194; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_194() { return &___m_lineVisibleCharacterCount_194; }
	inline void set_m_lineVisibleCharacterCount_194(int32_t value)
	{
		___m_lineVisibleCharacterCount_194 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_195() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageNumber_195)); }
	inline int32_t get_m_pageNumber_195() const { return ___m_pageNumber_195; }
	inline int32_t* get_address_of_m_pageNumber_195() { return &___m_pageNumber_195; }
	inline void set_m_pageNumber_195(int32_t value)
	{
		___m_pageNumber_195 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxAscender_196)); }
	inline float get_m_maxAscender_196() const { return ___m_maxAscender_196; }
	inline float* get_address_of_m_maxAscender_196() { return &___m_maxAscender_196; }
	inline void set_m_maxAscender_196(float value)
	{
		___m_maxAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_197() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxCapHeight_197)); }
	inline float get_m_maxCapHeight_197() const { return ___m_maxCapHeight_197; }
	inline float* get_address_of_m_maxCapHeight_197() { return &___m_maxCapHeight_197; }
	inline void set_m_maxCapHeight_197(float value)
	{
		___m_maxCapHeight_197 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxDescender_198)); }
	inline float get_m_maxDescender_198() const { return ___m_maxDescender_198; }
	inline float* get_address_of_m_maxDescender_198() { return &___m_maxDescender_198; }
	inline void set_m_maxDescender_198(float value)
	{
		___m_maxDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineAscender_199)); }
	inline float get_m_maxLineAscender_199() const { return ___m_maxLineAscender_199; }
	inline float* get_address_of_m_maxLineAscender_199() { return &___m_maxLineAscender_199; }
	inline void set_m_maxLineAscender_199(float value)
	{
		___m_maxLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_200() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineDescender_200)); }
	inline float get_m_maxLineDescender_200() const { return ___m_maxLineDescender_200; }
	inline float* get_address_of_m_maxLineDescender_200() { return &___m_maxLineDescender_200; }
	inline void set_m_maxLineDescender_200(float value)
	{
		___m_maxLineDescender_200 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_201() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_startOfLineAscender_201)); }
	inline float get_m_startOfLineAscender_201() const { return ___m_startOfLineAscender_201; }
	inline float* get_address_of_m_startOfLineAscender_201() { return &___m_startOfLineAscender_201; }
	inline void set_m_startOfLineAscender_201(float value)
	{
		___m_startOfLineAscender_201 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_202() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineOffset_202)); }
	inline float get_m_lineOffset_202() const { return ___m_lineOffset_202; }
	inline float* get_address_of_m_lineOffset_202() { return &___m_lineOffset_202; }
	inline void set_m_lineOffset_202(float value)
	{
		___m_lineOffset_202 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_203() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_meshExtents_203)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_m_meshExtents_203() const { return ___m_meshExtents_203; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_m_meshExtents_203() { return &___m_meshExtents_203; }
	inline void set_m_meshExtents_203(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___m_meshExtents_203 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_204() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlColor_204)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_htmlColor_204() const { return ___m_htmlColor_204; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_htmlColor_204() { return &___m_htmlColor_204; }
	inline void set_m_htmlColor_204(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_htmlColor_204 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorStack_205)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_colorStack_205() const { return ___m_colorStack_205; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_colorStack_205() { return &___m_colorStack_205; }
	inline void set_m_colorStack_205(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_colorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColorStack_206)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_underlineColorStack_206() const { return ___m_underlineColorStack_206; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_underlineColorStack_206() { return &___m_underlineColorStack_206; }
	inline void set_m_underlineColorStack_206(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_underlineColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColorStack_207)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_strikethroughColorStack_207() const { return ___m_strikethroughColorStack_207; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_strikethroughColorStack_207() { return &___m_strikethroughColorStack_207; }
	inline void set_m_strikethroughColorStack_207(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_strikethroughColorStack_207 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColorStack_208)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_highlightColorStack_208() const { return ___m_highlightColorStack_208; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_highlightColorStack_208() { return &___m_highlightColorStack_208; }
	inline void set_m_highlightColorStack_208(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_highlightColorStack_208 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_209() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientPreset_209)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_colorGradientPreset_209() const { return ___m_colorGradientPreset_209; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_colorGradientPreset_209() { return &___m_colorGradientPreset_209; }
	inline void set_m_colorGradientPreset_209(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_colorGradientPreset_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_209), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_210() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientStack_210)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_m_colorGradientStack_210() const { return ___m_colorGradientStack_210; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_m_colorGradientStack_210() { return &___m_colorGradientStack_210; }
	inline void set_m_colorGradientStack_210(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___m_colorGradientStack_210 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_211() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tabSpacing_211)); }
	inline float get_m_tabSpacing_211() const { return ___m_tabSpacing_211; }
	inline float* get_address_of_m_tabSpacing_211() { return &___m_tabSpacing_211; }
	inline void set_m_tabSpacing_211(float value)
	{
		___m_tabSpacing_211 = value;
	}

	inline static int32_t get_offset_of_m_spacing_212() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spacing_212)); }
	inline float get_m_spacing_212() const { return ___m_spacing_212; }
	inline float* get_address_of_m_spacing_212() { return &___m_spacing_212; }
	inline void set_m_spacing_212(float value)
	{
		___m_spacing_212 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_213() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_styleStack_213)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_m_styleStack_213() const { return ___m_styleStack_213; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_m_styleStack_213() { return &___m_styleStack_213; }
	inline void set_m_styleStack_213(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___m_styleStack_213 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_214() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_actionStack_214)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_m_actionStack_214() const { return ___m_actionStack_214; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_m_actionStack_214() { return &___m_actionStack_214; }
	inline void set_m_actionStack_214(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___m_actionStack_214 = value;
	}

	inline static int32_t get_offset_of_m_padding_215() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_padding_215)); }
	inline float get_m_padding_215() const { return ___m_padding_215; }
	inline float* get_address_of_m_padding_215() { return &___m_padding_215; }
	inline void set_m_padding_215(float value)
	{
		___m_padding_215 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_216() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffset_216)); }
	inline float get_m_baselineOffset_216() const { return ___m_baselineOffset_216; }
	inline float* get_address_of_m_baselineOffset_216() { return &___m_baselineOffset_216; }
	inline void set_m_baselineOffset_216(float value)
	{
		___m_baselineOffset_216 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_217() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffsetStack_217)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_m_baselineOffsetStack_217() const { return ___m_baselineOffsetStack_217; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_m_baselineOffsetStack_217() { return &___m_baselineOffsetStack_217; }
	inline void set_m_baselineOffsetStack_217(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___m_baselineOffsetStack_217 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_218() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xAdvance_218)); }
	inline float get_m_xAdvance_218() const { return ___m_xAdvance_218; }
	inline float* get_address_of_m_xAdvance_218() { return &___m_xAdvance_218; }
	inline void set_m_xAdvance_218(float value)
	{
		___m_xAdvance_218 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_219() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textElementType_219)); }
	inline int32_t get_m_textElementType_219() const { return ___m_textElementType_219; }
	inline int32_t* get_address_of_m_textElementType_219() { return &___m_textElementType_219; }
	inline void set_m_textElementType_219(int32_t value)
	{
		___m_textElementType_219 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_220() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_TextElement_220)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_m_cached_TextElement_220() const { return ___m_cached_TextElement_220; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_m_cached_TextElement_220() { return &___m_cached_TextElement_220; }
	inline void set_m_cached_TextElement_220(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___m_cached_TextElement_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_220), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_Character_221() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Underline_Character_221)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Underline_Character_221() const { return ___m_cached_Underline_Character_221; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Underline_Character_221() { return &___m_cached_Underline_Character_221; }
	inline void set_m_cached_Underline_Character_221(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Underline_Character_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_Character_221), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_Character_222() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Ellipsis_Character_222)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Ellipsis_Character_222() const { return ___m_cached_Ellipsis_Character_222; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Ellipsis_Character_222() { return &___m_cached_Ellipsis_Character_222; }
	inline void set_m_cached_Ellipsis_Character_222(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Ellipsis_Character_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_Character_222), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_223() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_defaultSpriteAsset_223)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_223() const { return ___m_defaultSpriteAsset_223; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_223() { return &___m_defaultSpriteAsset_223; }
	inline void set_m_defaultSpriteAsset_223(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_223), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_224() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentSpriteAsset_224)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_currentSpriteAsset_224() const { return ___m_currentSpriteAsset_224; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_currentSpriteAsset_224() { return &___m_currentSpriteAsset_224; }
	inline void set_m_currentSpriteAsset_224(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_currentSpriteAsset_224 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_224), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_225() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteCount_225)); }
	inline int32_t get_m_spriteCount_225() const { return ___m_spriteCount_225; }
	inline int32_t* get_address_of_m_spriteCount_225() { return &___m_spriteCount_225; }
	inline void set_m_spriteCount_225(int32_t value)
	{
		___m_spriteCount_225 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_226() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteIndex_226)); }
	inline int32_t get_m_spriteIndex_226() const { return ___m_spriteIndex_226; }
	inline int32_t* get_address_of_m_spriteIndex_226() { return &___m_spriteIndex_226; }
	inline void set_m_spriteIndex_226(int32_t value)
	{
		___m_spriteIndex_226 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_227() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimationID_227)); }
	inline int32_t get_m_spriteAnimationID_227() const { return ___m_spriteAnimationID_227; }
	inline int32_t* get_address_of_m_spriteAnimationID_227() { return &___m_spriteAnimationID_227; }
	inline void set_m_spriteAnimationID_227(int32_t value)
	{
		___m_spriteAnimationID_227 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_228() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreActiveState_228)); }
	inline bool get_m_ignoreActiveState_228() const { return ___m_ignoreActiveState_228; }
	inline bool* get_address_of_m_ignoreActiveState_228() { return &___m_ignoreActiveState_228; }
	inline void set_m_ignoreActiveState_228(bool value)
	{
		___m_ignoreActiveState_228 = value;
	}

	inline static int32_t get_offset_of_k_Power_229() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___k_Power_229)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_k_Power_229() const { return ___k_Power_229; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_k_Power_229() { return &___k_Power_229; }
	inline void set_k_Power_229(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___k_Power_229 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_229), value);
	}
};

struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargePositiveVector2_230;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargeNegativeVector2_231;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_232;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_233;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_234;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_235;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_230() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveVector2_230)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargePositiveVector2_230() const { return ___k_LargePositiveVector2_230; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargePositiveVector2_230() { return &___k_LargePositiveVector2_230; }
	inline void set_k_LargePositiveVector2_230(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargePositiveVector2_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_231() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeVector2_231)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargeNegativeVector2_231() const { return ___k_LargeNegativeVector2_231; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargeNegativeVector2_231() { return &___k_LargeNegativeVector2_231; }
	inline void set_k_LargeNegativeVector2_231(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargeNegativeVector2_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_232() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveFloat_232)); }
	inline float get_k_LargePositiveFloat_232() const { return ___k_LargePositiveFloat_232; }
	inline float* get_address_of_k_LargePositiveFloat_232() { return &___k_LargePositiveFloat_232; }
	inline void set_k_LargePositiveFloat_232(float value)
	{
		___k_LargePositiveFloat_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_233() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeFloat_233)); }
	inline float get_k_LargeNegativeFloat_233() const { return ___k_LargeNegativeFloat_233; }
	inline float* get_address_of_k_LargeNegativeFloat_233() { return &___k_LargeNegativeFloat_233; }
	inline void set_k_LargeNegativeFloat_233(float value)
	{
		___k_LargeNegativeFloat_233 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_234() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveInt_234)); }
	inline int32_t get_k_LargePositiveInt_234() const { return ___k_LargePositiveInt_234; }
	inline int32_t* get_address_of_k_LargePositiveInt_234() { return &___k_LargePositiveInt_234; }
	inline void set_k_LargePositiveInt_234(int32_t value)
	{
		___k_LargePositiveInt_234 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_235() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeInt_235)); }
	inline int32_t get_k_LargeNegativeInt_235() const { return ___k_LargeNegativeInt_235; }
	inline int32_t* get_address_of_k_LargeNegativeInt_235() { return &___k_LargeNegativeInt_235; }
	inline void set_k_LargeNegativeInt_235(int32_t value)
	{
		___k_LargeNegativeInt_235 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifndef RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#define RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Texture_30;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_UVRect_31;

public:
	inline static int32_t get_offset_of_m_Texture_30() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_Texture_30)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Texture_30() const { return ___m_Texture_30; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Texture_30() { return &___m_Texture_30; }
	inline void set_m_Texture_30(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Texture_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_30), value);
	}

	inline static int32_t get_offset_of_m_UVRect_31() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_UVRect_31)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_UVRect_31() const { return ___m_UVRect_31; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_UVRect_31() { return &___m_UVRect_31; }
	inline void set_m_UVRect_31(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_UVRect_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifndef VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H
#define VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorObject2D
struct  VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650  : public RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8
{
public:
	// System.Boolean Vectrosity.VectorObject2D::m_updateVerts
	bool ___m_updateVerts_32;
	// System.Boolean Vectrosity.VectorObject2D::m_updateUVs
	bool ___m_updateUVs_33;
	// System.Boolean Vectrosity.VectorObject2D::m_updateColors
	bool ___m_updateColors_34;
	// System.Boolean Vectrosity.VectorObject2D::m_updateNormals
	bool ___m_updateNormals_35;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTangents
	bool ___m_updateTangents_36;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTris
	bool ___m_updateTris_37;
	// UnityEngine.Mesh Vectrosity.VectorObject2D::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_38;
	// Vectrosity.VectorLine Vectrosity.VectorObject2D::vectorLine
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * ___vectorLine_39;

public:
	inline static int32_t get_offset_of_m_updateVerts_32() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateVerts_32)); }
	inline bool get_m_updateVerts_32() const { return ___m_updateVerts_32; }
	inline bool* get_address_of_m_updateVerts_32() { return &___m_updateVerts_32; }
	inline void set_m_updateVerts_32(bool value)
	{
		___m_updateVerts_32 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_33() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateUVs_33)); }
	inline bool get_m_updateUVs_33() const { return ___m_updateUVs_33; }
	inline bool* get_address_of_m_updateUVs_33() { return &___m_updateUVs_33; }
	inline void set_m_updateUVs_33(bool value)
	{
		___m_updateUVs_33 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_34() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateColors_34)); }
	inline bool get_m_updateColors_34() const { return ___m_updateColors_34; }
	inline bool* get_address_of_m_updateColors_34() { return &___m_updateColors_34; }
	inline void set_m_updateColors_34(bool value)
	{
		___m_updateColors_34 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_35() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateNormals_35)); }
	inline bool get_m_updateNormals_35() const { return ___m_updateNormals_35; }
	inline bool* get_address_of_m_updateNormals_35() { return &___m_updateNormals_35; }
	inline void set_m_updateNormals_35(bool value)
	{
		___m_updateNormals_35 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_36() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateTangents_36)); }
	inline bool get_m_updateTangents_36() const { return ___m_updateTangents_36; }
	inline bool* get_address_of_m_updateTangents_36() { return &___m_updateTangents_36; }
	inline void set_m_updateTangents_36(bool value)
	{
		___m_updateTangents_36 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_37() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_updateTris_37)); }
	inline bool get_m_updateTris_37() const { return ___m_updateTris_37; }
	inline bool* get_address_of_m_updateTris_37() { return &___m_updateTris_37; }
	inline void set_m_updateTris_37(bool value)
	{
		___m_updateTris_37 = value;
	}

	inline static int32_t get_offset_of_m_mesh_38() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___m_mesh_38)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_38() const { return ___m_mesh_38; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_38() { return &___m_mesh_38; }
	inline void set_m_mesh_38(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_38), value);
	}

	inline static int32_t get_offset_of_vectorLine_39() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650, ___vectorLine_39)); }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * get_vectorLine_39() const { return ___vectorLine_39; }
	inline VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F ** get_address_of_vectorLine_39() { return &___vectorLine_39; }
	inline void set_vectorLine_39(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F * value)
	{
		___vectorLine_39 = value;
		Il2CppCodeGenWriteBarrier((&___vectorLine_39), value);
	}
};

struct VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields
{
public:
	// UnityEngine.UI.VertexHelper Vectrosity.VectorObject2D::vertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___vertexHelper_40;

public:
	inline static int32_t get_offset_of_vertexHelper_40() { return static_cast<int32_t>(offsetof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields, ___vertexHelper_40)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_vertexHelper_40() const { return ___vertexHelper_40; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_vertexHelper_40() { return &___vertexHelper_40; }
	inline void set_vertexHelper_40(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___vertexHelper_40 = value;
		Il2CppCodeGenWriteBarrier((&___vertexHelper_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOROBJECT2D_TBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3400[3] = 
{
	MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3401[5] = 
{
	TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3402[12] = 
{
	FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3403[10] = 
{
	FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7), -1, sizeof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3404[206] = 
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_text_30(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isRightToLeft_31(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontAsset_32(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentFontAsset_33(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isSDFShader_34(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_sharedMaterial_35(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentMaterial_36(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferences_37(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferenceIndexLookup_38(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferenceStack_39(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentMaterialIndex_40(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSharedMaterials_41(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontMaterial_42(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontMaterials_43(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isMaterialDirty_44(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColor32_45(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColor_46(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_s_colorWhite_47(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_underlineColor_48(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_strikethroughColor_49(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightColor_50(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightPadding_51(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableVertexGradient_52(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorMode_53(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColorGradient_54(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColorGradientPreset_55(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAsset_56(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tintAllSprites_57(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tintSprite_58(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteColor_59(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_overrideHtmlColors_60(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_faceColor_61(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_outlineColor_62(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_outlineWidth_63(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSize_64(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentFontSize_65(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeBase_66(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_sizeStack_67(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontWeight_68(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontWeightInternal_69(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontWeightStack_70(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableAutoSizing_71(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxFontSize_72(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minFontSize_73(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeMin_74(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeMax_75(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontStyle_76(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontStyleInternal_77(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontStyleStack_78(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isUsingBold_79(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textAlignment_80(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineJustification_81(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineJustificationStack_82(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textContainerLocalCorners_83(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_characterSpacing_84(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cSpacing_85(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_monoSpacing_86(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_wordSpacing_87(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacing_88(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacingDelta_89(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineHeight_90(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacingMax_91(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_paragraphSpacing_92(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charWidthMaxAdj_93(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charWidthAdjDelta_94(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableWordWrapping_95(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCharacterWrappingEnabled_96(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isNonBreakingSpace_97(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isIgnoringAlignment_98(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_wordWrappingRatios_99(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_overflowMode_100(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstOverflowCharacterIndex_101(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_linkedTextComponent_102(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isLinkedTextComponent_103(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isTextTruncated_104(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableKerning_105(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableExtraPadding_106(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_checkPaddingRequired_107(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isRichText_108(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_parseCtrlCharacters_109(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isOverlay_110(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isOrthographic_111(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCullingEnabled_112(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreRectMaskCulling_113(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreCulling_114(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_horizontalMapping_115(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_verticalMapping_116(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_uvLineOffset_117(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderMode_118(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_geometrySortingOrder_119(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_VertexBufferAutoSizeReduction_120(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstVisibleCharacter_121(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleCharacters_122(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleWords_123(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleLines_124(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_useMaxVisibleDescender_125(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_pageToDisplay_126(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isNewPage_127(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_margin_128(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginLeft_129(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginRight_130(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginWidth_131(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginHeight_132(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_width_133(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textInfo_134(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_havePropertiesChanged_135(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isUsingLegacyAnimationComponent_136(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_transform_137(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_rectTransform_138(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_autoSizeTextContainer_140(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_mesh_141(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isVolumetricText_142(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAnimator_143(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_flexibleHeight_144(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_flexibleWidth_145(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minWidth_146(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minHeight_147(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxWidth_148(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxHeight_149(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_LayoutElement_150(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_preferredWidth_151(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderedWidth_152(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isPreferredWidthDirty_153(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_preferredHeight_154(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderedHeight_155(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isPreferredHeightDirty_156(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCalculatingPreferredValues_157(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_recursiveCount_158(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_layoutPriority_159(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCalculateSizeRequired_160(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isLayoutDirty_161(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_verticesAlreadyDirty_162(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_layoutAlreadyDirty_163(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isAwake_164(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isWaitingOnResourceLoad_165(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isInputParsingRequired_166(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_inputSource_167(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_old_text_168(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontScale_169(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontScaleMultiplier_170(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_htmlTag_171(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_xmlAttribute_172(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_attributeParameterValues_173(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_LineIndent_174(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_Indent_175(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_indentStack_176(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_NoParsing_177(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isParsingText_178(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FXMatrix_179(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isFXMatrixSet_180(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_TextParsingBuffer_181(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_internalCharacterInfo_182(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_input_CharArray_183(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charArray_Length_184(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_totalCharacterCount_185(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_SavedWordWrapState_186(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_SavedLineState_187(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_characterCount_188(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstCharacterOfLine_189(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstVisibleCharacterOfLine_190(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lastCharacterOfLine_191(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lastVisibleCharacterOfLine_192(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineNumber_193(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineVisibleCharacterCount_194(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_pageNumber_195(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxAscender_196(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxCapHeight_197(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxDescender_198(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxLineAscender_199(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxLineDescender_200(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_startOfLineAscender_201(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineOffset_202(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_meshExtents_203(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_htmlColor_204(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorStack_205(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_underlineColorStack_206(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_strikethroughColorStack_207(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightColorStack_208(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorGradientPreset_209(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorGradientStack_210(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tabSpacing_211(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spacing_212(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_styleStack_213(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_actionStack_214(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_padding_215(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_baselineOffset_216(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_baselineOffsetStack_217(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_xAdvance_218(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textElementType_219(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_TextElement_220(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_Underline_Character_221(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_Ellipsis_Character_222(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_defaultSpriteAsset_223(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentSpriteAsset_224(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteCount_225(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteIndex_226(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAnimationID_227(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreActiveState_228(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_k_Power_229(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveVector2_230(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeVector2_231(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveFloat_232(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeFloat_233(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveInt_234(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeInt_235(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3405[5] = 
{
	TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A)+ sizeof (RuntimeObject), sizeof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A ), 0, 0 };
extern const int32_t g_FieldOffsetTable3406[3] = 
{
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_unicode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_stringIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3407[3] = 
{
	TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[5] = 
{
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_ElementType_0(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Unicode_1(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Glyph_2(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_GlyphIndex_3(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[9] = 
{
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_id_0(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_x_1(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_y_2(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_width_3(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_height_4(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_xOffset_5(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_yOffset_6(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_xAdvance_7(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181), -1, sizeof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3410[18] = 
{
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_textComponent_2(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_characterCount_3(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_spriteCount_4(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_spaceCount_5(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_wordCount_6(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_linkCount_7(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_lineCount_8(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_pageCount_9(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_materialCount_10(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_characterInfo_11(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_wordInfo_12(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_linkInfo_13(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_lineInfo_14(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_pageInfo_15(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_meshInfo_16(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35), -1, sizeof(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3411[3] = 
{
	TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields::get_offset_of_s_Instance_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3412[4] = 
{
	CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708)+ sizeof (RuntimeObject), sizeof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3413[2] = 
{
	CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45), -1, sizeof(TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3414[3] = 
{
	TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D)+ sizeof (RuntimeObject), sizeof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3415[2] = 
{
	LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B), -1, sizeof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3416[7] = 
{
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_GraphicQueueLookup_4(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_InternalUpdateQueue_5(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_InternalUpdateLookup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC), -1, sizeof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3417[5] = 
{
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3418[3] = 
{
	Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D), -1, sizeof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3419[13] = 
{
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_RESOURCE_LOAD_EVENT_9(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_OnPreRenderObject_Event_11(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3420[3] = 
{
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44), -1, sizeof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3422[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3423[8] = 
{
	TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A)+ sizeof (RuntimeObject), sizeof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable3424[4] = 
{
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3425[5] = 
{
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[7] = 
{
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[4] = 
{
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB ), 0, 0 };
extern const int32_t g_FieldOffsetTable3428[3] = 
{
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3)+ sizeof (RuntimeObject), sizeof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3429[2] = 
{
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3430[2] = 
{
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[55] = 
{
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5)+ sizeof (RuntimeObject), sizeof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3432[3] = 
{
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98)+ sizeof (RuntimeObject), sizeof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3433[6] = 
{
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueHashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueStartIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_unitType_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3434[3] = 
{
	SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (TexturePacker_t2549189919276EB833F83EA937AB992420E1B199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04)+ sizeof (RuntimeObject), sizeof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3436[4] = 
{
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E)+ sizeof (RuntimeObject), sizeof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E ), 0, 0 };
extern const int32_t g_FieldOffsetTable3437[2] = 
{
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728)+ sizeof (RuntimeObject), sizeof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3438[7] = 
{
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[1] = 
{
	SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3440[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (U3CModuleU3E_tDBDF5538A0E6A38DC8C394D40F51A58BB8ADEC1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[4] = 
{
	BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97::get_offset_of_m_objectNumber_4(),
	BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97::get_offset_of_m_vectorLine_5(),
	BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97::get_offset_of_m_useLine_6(),
	BrightnessControl_tB70BEB70438B69CD3087C838A1F6498836912A97::get_offset_of_m_destroyed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[9] = 
{
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_capType_0(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_texture_1(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_ratio1_2(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_ratio2_3(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_offset1_4(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_offset2_5(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_scale1_6(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_scale2_7(),
	CapInfo_t359B32B68C74DE323554A69C22AC50969886AD01::get_offset_of_uvHeights_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776), -1, sizeof(LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3447[4] = 
{
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields::get_offset_of_lines_4(),
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields::get_offset_of_transforms_5(),
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776_StaticFields::get_offset_of_lineCount_6(),
	LineManager_t4991D2C017698413E3BA236D61D0FD00B877D776::get_offset_of_destroyed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[9] = 
{
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_time_0(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_remove_1(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_vectorLine_2(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U24PC_3(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U24current_4(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U3CU24U3Etime_5(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U3CU24U3Eremove_6(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U3CU24U3EvectorLine_7(),
	U3CDisableLineU3Ec__Iterator0_t06E7DE41DE23F7600A9345366B2490E01E011B39::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[1] = 
{
	RefInt_t48EEC09BFAF129479FF0EE5B4DD84AD8D3C6FBB2::get_offset_of_i_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C), -1, sizeof(VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3450[1] = 
{
	VectorChar_t795E92F6B0AAB4627A4E348B6022EE07124C2F3C_StaticFields::get_offset_of_points_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3451[4] = 
{
	LineType_t73EEB07AD123A0E80FC96AB022A611709FCA7FA1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3452[4] = 
{
	Joins_t10F2955C021100EC84153BE7EDEDBA60219FB60F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3453[6] = 
{
	EndCap_tDEAA2C9FB9A84A5253A0EC8D77306D53B4F28C9F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3454[5] = 
{
	Visibility_tFBB4653FDAD5FF8AFDCD4E3FBE65EFBD0318C174::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3455[3] = 
{
	Brightness_tDF7412E025A9E23469DD70EDE4EBF92CBACD56D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3456[4] = 
{
	CanvasState_t9DCF2296DCEC556038FBF7588D3EEC1EC3D0D5E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F), -1, sizeof(VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3457[75] = 
{
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineVertices_0(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineUVs_1(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineColors_2(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineTriangles_3(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_vertexCount_4(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_go_5(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_rectTransform_6(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_vectorObject_7(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_color_8(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_canvasState_9(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_is2D_10(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_points2_11(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_points3_12(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_pointsCount_13(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_screenPoints_14(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineWidths_15(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineWidth_16(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_maxWeldDistance_17(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_distances_18(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_name_19(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_material_20(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_originalTexture_21(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_texture_22(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_active_23(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineType_24(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_capLength_25(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_smoothWidth_26(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_smoothColor_27(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_joins_28(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_isAutoDrawing_29(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_drawStart_30(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_drawEnd_31(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_endPointsUpdate_32(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_useNormals_33(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_useTangents_34(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_normalsCalculated_35(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_tangentsCalculated_36(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_capType_37(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_endCap_38(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_useCapColors_39(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_frontColor_40(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_backColor_41(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_frontEndCapIndex_42(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_backEndCapIndex_43(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineUVBottom_44(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_lineUVTop_45(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_frontCapUVBottom_46(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_frontCapUVTop_47(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_backCapUVBottom_48(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_backCapUVTop_49(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_continuousTexture_50(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_drawTransform_51(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_viewportDraw_52(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_textureScale_53(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_useTextureScale_54(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_textureOffset_55(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_useMatrix_56(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_matrix_57(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_collider_58(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_trigger_59(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_physicsMaterial_60(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F::get_offset_of_m_alignOddWidthToPixels_61(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_v3zero_62(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_m_canvas_63(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_camTransform_64(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_cam3D_65(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_oldPosition_66(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_oldRotation_67(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_lineManagerCreated_68(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_m_lineManager_69(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_capDictionary_70(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_endianDiff1_71(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_endianDiff2_72(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_byteBlock_73(),
	VectorLine_tE47B2160D4512DFE7FBF8B8FCA31CA0DB8FC788F_StaticFields::get_offset_of_functionNames_74(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3458[6] = 
{
	FunctionName_t3289981332E983EA64C4A1631FBF9146BC2570D9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993), -1, sizeof(VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3459[19] = 
{
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_minBrightnessDistance_0(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_maxBrightnessDistance_1(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_brightnessLevels_2(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_distanceCheckFrequency_3(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_fogColor_4(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_useDraw3D_5(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_vectorLines_6(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_objectNumbers_7(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of__arrayCount_8(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_vectorLines2_9(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_objectNumbers2_10(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of__arrayCount2_11(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_transforms3_12(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_vectorLines3_13(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_oldDistances_14(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_colors_15(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_objectNumbers3_16(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of__arrayCount3_17(),
	VectorManager_tE2B75B0CD71CA1D1344A4C606D667F55E0706993_StaticFields::get_offset_of_meshTable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650), -1, sizeof(VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3460[9] = 
{
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateVerts_32(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateUVs_33(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateColors_34(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateNormals_35(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateTangents_36(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_updateTris_37(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_m_mesh_38(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650::get_offset_of_vectorLine_39(),
	VectorObject2D_tBB62ACE59FC25AE4B95BDC36E5924D37E6B24650_StaticFields::get_offset_of_vertexHelper_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[10] = 
{
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateVerts_4(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateUVs_5(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateColors_6(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateNormals_7(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateTangents_8(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_updateTris_9(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_mesh_10(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_vectorLine_11(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_material_12(),
	VectorObject3D_t0452EB1566879B916AA1C3566CA8E8431102CEAE::get_offset_of_m_useCustomMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[4] = 
{
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA::get_offset_of_m_objectNumber_4(),
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA::get_offset_of_m_vectorLine_5(),
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA::get_offset_of_m_destroyed_6(),
	VisibilityControl_t045FEA605549ACF751298973DF50D16BE9A0F6DA::get_offset_of_m_dontDestroyLine_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[3] = 
{
	U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48::get_offset_of_U24PC_0(),
	U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48::get_offset_of_U24current_1(),
	U3CVisibilityTestU3Ec__Iterator1_t23C7897B9619831180B98866CDF6C0E00B31EE48::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3464[3] = 
{
	U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403::get_offset_of_U24PC_0(),
	U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403::get_offset_of_U24current_1(),
	U3COnBecameVisibleU3Ec__Iterator2_t6B38B859F4467015CB65FC0839CB8136E48C1403::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3465[3] = 
{
	U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73::get_offset_of_U24PC_0(),
	U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73::get_offset_of_U24current_1(),
	U3COnBecameInvisibleU3Ec__Iterator3_t5B571ADF3C4F02015B82402CCB27D3F1F3D1FD73::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[4] = 
{
	VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E::get_offset_of_m_objectNumber_4(),
	VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E::get_offset_of_m_vectorLine_5(),
	VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E::get_offset_of_m_destroyed_6(),
	VisibilityControlAlways_tD8273ED584609B8826098FAF70DFAD95C1AFBA9E::get_offset_of_m_dontDestroyLine_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[5] = 
{
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B::get_offset_of_m_objectNumber_4(),
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B::get_offset_of_m_vectorLine_5(),
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B::get_offset_of_m_destroyed_6(),
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B::get_offset_of_m_dontDestroyLine_7(),
	VisibilityControlStatic_t3834E5EE6724D1FEF309C33A74B62E2638D4777B::get_offset_of_m_originalMatrix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[3] = 
{
	U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7::get_offset_of_U24PC_0(),
	U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7::get_offset_of_U24current_1(),
	U3CWaitCheckU3Ec__Iterator4_t1FED5A77590066EDDB55AFE3775BC5B5B0AED4A7::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[2] = 
{
	MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7::get_offset_of_label_4(),
	MultiLayerTouch_t7086C9897928EB82C35E7ACC7F57670C8E1DA6C7::get_offset_of_label2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (MultiLayerUI_t097B3D533CC4D4897272ED062F4C8D14F38E723E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (MultiCameraTouch_t27561691899B8D077DE0A95D4BA779E299C34246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3472[1] = 
{
	MultiCameraTouch_t27561691899B8D077DE0A95D4BA779E299C34246::get_offset_of_label_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[2] = 
{
	MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563::get_offset_of_cam2_4(),
	MultiCameraUI_t12735E383D8761C12C540B8ADFFFA95B70DE8563::get_offset_of_cam3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (CubeSelect_tB77B073FCD9256C5109FE277E7B7F3B484BD98E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[1] = 
{
	CubeSelect_tB77B073FCD9256C5109FE277E7B7F3B484BD98E3::get_offset_of_cube_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (RTSCamera_t514C2FDB125D689261EB16B7EF1D8B23915ABD47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3475[1] = 
{
	RTSCamera_t514C2FDB125D689261EB16B7EF1D8B23915ABD47::get_offset_of_delta_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[6] = 
{
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_ballModel_4(),
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_start_5(),
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_moveDirection_6(),
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_characterController_7(),
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_startPosition_8(),
	BallRunPlayer_t9338BEAE346F60931A8E00795FD7170D91DEF21C::get_offset_of_isJump_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[6] = 
{
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_distanceAway_4(),
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_distanceUp_5(),
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_smooth_6(),
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_hovercraft_7(),
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_targetPosition_8(),
	ThirdPersonCamera_t30B1D5421B432A30B8A56A0DBC22E5EC882603A2::get_offset_of_follow_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (LoadExamples_t0CD5186C64195143339A78768F33CEDDA33DACCD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[3] = 
{
	FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D::get_offset_of_textMesh_4(),
	FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D::get_offset_of_deltaPosition_5(),
	FingerTouch_tA8E336DAA46DA11DAD4AB8D7B2ED3B0B715C387D::get_offset_of_fingerId_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (MutliFingersScreenTouch_t56F49D8EBD54112E7A9F67AA318AE672911627E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[1] = 
{
	MutliFingersScreenTouch_t56F49D8EBD54112E7A9F67AA318AE672911627E7::get_offset_of_touchGameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (DoubleTapMe_t926E96BAB88B4D49653B65853CE0353174E70E4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[4] = 
{
	DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A::get_offset_of_textMesh_4(),
	DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A::get_offset_of_startColor_5(),
	DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A::get_offset_of_deltaPosition_6(),
	DragMe_t26C3C20FB43EC07563B7ACECA717D87AEE41155A::get_offset_of_fingerIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[2] = 
{
	LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779::get_offset_of_textMesh_4(),
	LongTapMe_tFCCE53939858AD9373BC3DDCAAA39654E4F46779::get_offset_of_startColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[2] = 
{
	Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899::get_offset_of_trail_4(),
	Swipe_t37F8398E806C3E56E6AC1897B603B75E09460899::get_offset_of_swipeText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (TapMe_t91364143D18FB3D29539FBD90A0C81D807082D9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3487[2] = 
{
	TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174::get_offset_of_textMesh_4(),
	TouchMe_t00D1AB05380A4C1B20F60D37AE4754B261276174::get_offset_of_startColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (PinchMe_t6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[1] = 
{
	PinchMe_t6CE794737BA51D9CE8C8C1EEB50CFC33079D52F6::get_offset_of_textMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (TooglePickMethodUI_tD6A5E8764A6DE4FDE6DFD65BC9862F98266771D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[1] = 
{
	TwistMe_tFCD23EA1FAED83FA2795551DC2CDACEC48A43431::get_offset_of_textMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (TwoDoubleTapMe_t211C12A2A4072CC22EEAD156B4A13F613EEC7FA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3492[3] = 
{
	TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744::get_offset_of_textMesh_4(),
	TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744::get_offset_of_deltaPosition_5(),
	TwoDragMe_t5E6E6B3660C0861DB7912C03C48875494A7B3744::get_offset_of_startColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[2] = 
{
	TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90::get_offset_of_textMesh_4(),
	TwoLongTapMe_t64A8275D71E0AD4D85B8DFF10BE64E2E9052CB90::get_offset_of_startColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3494[2] = 
{
	TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1::get_offset_of_trail_4(),
	TwoSwipe_t7302475B6D99C134833786A691276DDC5873FEE1::get_offset_of_swipeData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (TwoTapMe_t2A4D01DA9C4ACA4A3064A3A5FC76D99C9F3AFF39), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[2] = 
{
	TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4::get_offset_of_textMesh_4(),
	TwoTouchMe_tB6158994C50B03E2BB08085F32767614E965B8F4::get_offset_of_startColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (ETWindow_t90349E97A6AE294226C71D6398D2DA2135F91956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[1] = 
{
	ETWindow_t90349E97A6AE294226C71D6398D2DA2135F91956::get_offset_of_drag_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (GlobalEasyTouchEvent_t0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3498[1] = 
{
	GlobalEasyTouchEvent_t0D90E301901D5F3C4E3D6E990A2C96CDEAE7BF1C::get_offset_of_statText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (UICompatibility_t544AB7DA19A39A90E4C4388AE1DA7183D4297BC2), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
