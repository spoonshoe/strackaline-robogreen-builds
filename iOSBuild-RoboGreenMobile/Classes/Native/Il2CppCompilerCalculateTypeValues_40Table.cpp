﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Doozy.Engine.Events.StringEvent
struct StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6;
// Doozy.Engine.Message/OnMessageHandleDelegate
struct OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D;
// HedgehogTeam.EasyTouch.EasyTouch
struct EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080;
// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler
struct DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap[]
struct DoubleTapU5BU5D_t35C6527C7D639577BA6AD5110AB8199AEA4C3E98;
// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA;
// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A;
// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler
struct DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF;
// HedgehogTeam.EasyTouch.EasyTouch/DragHandler
struct DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801;
// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B;
// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler
struct DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940;
// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler
struct EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621;
// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler
struct LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler
struct LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler
struct LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1;
// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler
struct OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F;
// HedgehogTeam.EasyTouch.EasyTouch/PickedObject
struct PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05;
// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler
struct PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57;
// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4;
// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler
struct PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29;
// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler
struct PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler
struct SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3;
// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler
struct SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler
struct SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler
struct SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB;
// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler
struct TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E;
// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929;
// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D;
// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler
struct UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3;
// HedgehogTeam.EasyTouch.EasyTouchInput
struct EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708;
// HedgehogTeam.EasyTouch.Finger
struct Finger_t1C55110621038C162D210785A1F1490A0273B205;
// HedgehogTeam.EasyTouch.Finger[]
struct FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95;
// HedgehogTeam.EasyTouch.QuickTouch/OnTouch
struct OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064;
// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe
struct OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B;
// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction
struct OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203;
// HedgehogTeam.EasyTouch.TwoFingerGesture
struct TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>>
struct Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera>
struct List_1_tFBA3FF3651F5CED1D17338B3B16949F652FC67EF;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture>
struct List_1_t4247767F3D684EE6D1C2815C234363AA5767B283;
// System.Collections.Generic.List`1<UnityEngine.Camera>
struct List_1_tBCCD79D8856B6659FEAA93388A85C5AF306364A6;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC;
// System.Func`2<System.String,System.String>
struct Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera>
struct Predicate_1_t389E2DA5B8944E708A1DA9C952D13DA405715983;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#define MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Message
struct  Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E  : public RuntimeObject
{
public:

public:
};

struct Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>> Doozy.Engine.Message::Handlers
	Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * ___Handlers_1;
	// Doozy.Engine.Message_OnMessageHandleDelegate Doozy.Engine.Message::OnMessageHandle
	OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * ___OnMessageHandle_2;

public:
	inline static int32_t get_offset_of_Handlers_1() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___Handlers_1)); }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * get_Handlers_1() const { return ___Handlers_1; }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA ** get_address_of_Handlers_1() { return &___Handlers_1; }
	inline void set_Handlers_1(Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * value)
	{
		___Handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___Handlers_1), value);
	}

	inline static int32_t get_offset_of_OnMessageHandle_2() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___OnMessageHandle_2)); }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * get_OnMessageHandle_2() const { return ___OnMessageHandle_2; }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D ** get_address_of_OnMessageHandle_2() { return &___OnMessageHandle_2; }
	inline void set_OnMessageHandle_2(OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * value)
	{
		___OnMessageHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessageHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T16C75EC0F7B6B6764DC1D62AFAF622266A77CE88_H
#define U3CU3EC__DISPLAYCLASS15_0_T16C75EC0F7B6B6764DC1D62AFAF622266A77CE88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Message_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t16C75EC0F7B6B6764DC1D62AFAF622266A77CE88  : public RuntimeObject
{
public:
	// System.Delegate Doozy.Engine.Message_<>c__DisplayClass15_0::callback
	Delegate_t * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t16C75EC0F7B6B6764DC1D62AFAF622266A77CE88, ___callback_0)); }
	inline Delegate_t * get_callback_0() const { return ___callback_0; }
	inline Delegate_t ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Delegate_t * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T16C75EC0F7B6B6764DC1D62AFAF622266A77CE88_H
#ifndef MESSAGEEXTENSIONS_T51C1F2A0256E90FDE441416BE35590DCF6D44375_H
#define MESSAGEEXTENSIONS_T51C1F2A0256E90FDE441416BE35590DCF6D44375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.MessageExtensions
struct  MessageExtensions_t51C1F2A0256E90FDE441416BE35590DCF6D44375  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEXTENSIONS_T51C1F2A0256E90FDE441416BE35590DCF6D44375_H
#ifndef ASSETUTILS_T61FDC3B29E3D755E2B26673EF6859DB0E79668B6_H
#define ASSETUTILS_T61FDC3B29E3D755E2B26673EF6859DB0E79668B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.AssetUtils
struct  AssetUtils_t61FDC3B29E3D755E2B26673EF6859DB0E79668B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETUTILS_T61FDC3B29E3D755E2B26673EF6859DB0E79668B6_H
#ifndef COLORUTILS_T98FDBBDA3C9612D6E9C2E5AA710BD6353F87204A_H
#define COLORUTILS_T98FDBBDA3C9612D6E9C2E5AA710BD6353F87204A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorUtils
struct  ColorUtils_t98FDBBDA3C9612D6E9C2E5AA710BD6353F87204A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILS_T98FDBBDA3C9612D6E9C2E5AA710BD6353F87204A_H
#ifndef DOOZYEXECUTIONORDER_T365361C4936386D0C5DE804E9ED8AC4620C4BD35_H
#define DOOZYEXECUTIONORDER_T365361C4936386D0C5DE804E9ED8AC4620C4BD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.DoozyExecutionOrder
struct  DoozyExecutionOrder_t365361C4936386D0C5DE804E9ED8AC4620C4BD35  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOOZYEXECUTIONORDER_T365361C4936386D0C5DE804E9ED8AC4620C4BD35_H
#ifndef FILEUTILS_T96473A9EF48218F647190AD1F034A7DA49E8F2F6_H
#define FILEUTILS_T96473A9EF48218F647190AD1F034A7DA49E8F2F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.FileUtils
struct  FileUtils_t96473A9EF48218F647190AD1F034A7DA49E8F2F6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_T96473A9EF48218F647190AD1F034A7DA49E8F2F6_H
#ifndef U3CU3EC_T62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_H
#define U3CU3EC_T62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.FileUtils_<>c
struct  U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields
{
public:
	// Doozy.Engine.Utils.FileUtils_<>c Doozy.Engine.Utils.FileUtils_<>c::<>9
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Doozy.Engine.Utils.FileUtils_<>c::<>9__9_0
	Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * ___U3CU3E9__9_0_1;
	// System.Func`2<System.String,System.Boolean> Doozy.Engine.Utils.FileUtils_<>c::<>9__9_1
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__9_1_2;
	// System.Func`2<System.String,System.Boolean> Doozy.Engine.Utils.FileUtils_<>c::<>9__9_2
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__9_2_3;
	// System.Func`2<System.String,System.String> Doozy.Engine.Utils.FileUtils_<>c::<>9__9_3
	Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * ___U3CU3E9__9_3_4;
	// System.Func`2<System.String,System.Boolean> Doozy.Engine.Utils.FileUtils_<>c::<>9__17_0
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__17_0_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9__9_0_1)); }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * get_U3CU3E9__9_0_1() const { return ___U3CU3E9__9_0_1; }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 ** get_address_of_U3CU3E9__9_0_1() { return &___U3CU3E9__9_0_1; }
	inline void set_U3CU3E9__9_0_1(Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * value)
	{
		___U3CU3E9__9_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9__9_1_2)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__9_1_2() const { return ___U3CU3E9__9_1_2; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__9_1_2() { return &___U3CU3E9__9_1_2; }
	inline void set_U3CU3E9__9_1_2(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__9_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9__9_2_3)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__9_2_3() const { return ___U3CU3E9__9_2_3; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__9_2_3() { return &___U3CU3E9__9_2_3; }
	inline void set_U3CU3E9__9_2_3(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__9_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9__9_3_4)); }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * get_U3CU3E9__9_3_4() const { return ___U3CU3E9__9_3_4; }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 ** get_address_of_U3CU3E9__9_3_4() { return &___U3CU3E9__9_3_4; }
	inline void set_U3CU3E9__9_3_4(Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * value)
	{
		___U3CU3E9__9_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields, ___U3CU3E9__17_0_5)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__17_0_5() const { return ___U3CU3E9__17_0_5; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__17_0_5() { return &___U3CU3E9__17_0_5; }
	inline void set_U3CU3E9__17_0_5(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__17_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_H
#ifndef MENUUTILS_TBDD43269757F837F55C408B4C40313E5CD77F66C_H
#define MENUUTILS_TBDD43269757F837F55C408B4C40313E5CD77F66C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.MenuUtils
struct  MenuUtils_tBDD43269757F837F55C408B4C40313E5CD77F66C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUUTILS_TBDD43269757F837F55C408B4C40313E5CD77F66C_H
#ifndef SCRIPTUTILS_TA3F985D6ECC70B592FDA88C95951828CED2D2931_H
#define SCRIPTUTILS_TA3F985D6ECC70B592FDA88C95951828CED2D2931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ScriptUtils
struct  ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931  : public RuntimeObject
{
public:

public:
};

struct ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931_StaticFields
{
public:
	// System.Boolean Doozy.Engine.Utils.ScriptUtils::debug
	bool ___debug_2;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931_StaticFields, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTUTILS_TA3F985D6ECC70B592FDA88C95951828CED2D2931_H
#ifndef UNITYRESOURCES_T30DB39338ECD04577D96032EEF6FF8552A21C9AF_H
#define UNITYRESOURCES_T30DB39338ECD04577D96032EEF6FF8552A21C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.UnityResources
struct  UnityResources_t30DB39338ECD04577D96032EEF6FF8552A21C9AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRESOURCES_T30DB39338ECD04577D96032EEF6FF8552A21C9AF_H
#ifndef ECAMERA_T51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9_H
#define ECAMERA_T51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.ECamera
struct  ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9  : public RuntimeObject
{
public:
	// UnityEngine.Camera HedgehogTeam.EasyTouch.ECamera::camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___camera_0;
	// System.Boolean HedgehogTeam.EasyTouch.ECamera::guiCamera
	bool ___guiCamera_1;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9, ___camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_camera_0() const { return ___camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}

	inline static int32_t get_offset_of_guiCamera_1() { return static_cast<int32_t>(offsetof(ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9, ___guiCamera_1)); }
	inline bool get_guiCamera_1() const { return ___guiCamera_1; }
	inline bool* get_address_of_guiCamera_1() { return &___guiCamera_1; }
	inline void set_guiCamera_1(bool value)
	{
		___guiCamera_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECAMERA_T51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9_H
#ifndef U3CU3EC_T7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_H
#define U3CU3EC_T7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_<>c
struct  U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_<>c HedgehogTeam.EasyTouch.EasyTouch_<>c::<>9
	U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D * ___U3CU3E9_0;
	// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch_<>c::<>9__221_0
	Predicate_1_t389E2DA5B8944E708A1DA9C952D13DA405715983 * ___U3CU3E9__221_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__221_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields, ___U3CU3E9__221_0_1)); }
	inline Predicate_1_t389E2DA5B8944E708A1DA9C952D13DA405715983 * get_U3CU3E9__221_0_1() const { return ___U3CU3E9__221_0_1; }
	inline Predicate_1_t389E2DA5B8944E708A1DA9C952D13DA405715983 ** get_address_of_U3CU3E9__221_0_1() { return &___U3CU3E9__221_0_1; }
	inline void set_U3CU3E9__221_0_1(Predicate_1_t389E2DA5B8944E708A1DA9C952D13DA405715983 * value)
	{
		___U3CU3E9__221_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__221_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_H
#ifndef U3CU3EC__DISPLAYCLASS240_0_T046EBBE2BE2D9E387606842666C648D1537299D4_H
#define U3CU3EC__DISPLAYCLASS240_0_T046EBBE2BE2D9E387606842666C648D1537299D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass240_0
struct  U3CU3Ec__DisplayClass240_0_t046EBBE2BE2D9E387606842666C648D1537299D4  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass240_0::gesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___gesture_0;

public:
	inline static int32_t get_offset_of_gesture_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass240_0_t046EBBE2BE2D9E387606842666C648D1537299D4, ___gesture_0)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get_gesture_0() const { return ___gesture_0; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of_gesture_0() { return &___gesture_0; }
	inline void set_gesture_0(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		___gesture_0 = value;
		Il2CppCodeGenWriteBarrier((&___gesture_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS240_0_T046EBBE2BE2D9E387606842666C648D1537299D4_H
#ifndef U3CU3EC__DISPLAYCLASS273_0_T1D044BA6A7BACD05300F7506B518CD042C180845_H
#define U3CU3EC__DISPLAYCLASS273_0_T1D044BA6A7BACD05300F7506B518CD042C180845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass273_0
struct  U3CU3Ec__DisplayClass273_0_t1D044BA6A7BACD05300F7506B518CD042C180845  : public RuntimeObject
{
public:
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass273_0::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_0;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass273_0_t1D044BA6A7BACD05300F7506B518CD042C180845, ___cam_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_0() const { return ___cam_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier((&___cam_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS273_0_T1D044BA6A7BACD05300F7506B518CD042C180845_H
#ifndef U3CSINGLEORDOUBLE2FINGERSU3ED__235_TAA54E2FBB78113A1929421716CD9EF7735873355_H
#define U3CSINGLEORDOUBLE2FINGERSU3ED__235_TAA54E2FBB78113A1929421716CD9EF7735873355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235
struct  U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::<>4__this
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355, ___U3CU3E4__this_2)); }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSINGLEORDOUBLE2FINGERSU3ED__235_TAA54E2FBB78113A1929421716CD9EF7735873355_H
#ifndef U3CSINGLEORDOUBLEU3ED__229_TA04A31FF7C737CB4E23EBEA145EB660FAE758625_H
#define U3CSINGLEORDOUBLEU3ED__229_TA04A31FF7C737CB4E23EBEA145EB660FAE758625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229
struct  U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::<>4__this
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * ___U3CU3E4__this_2;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::fingerIndex
	int32_t ___fingerIndex_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625, ___U3CU3E4__this_2)); }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_fingerIndex_3() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625, ___fingerIndex_3)); }
	inline int32_t get_fingerIndex_3() const { return ___fingerIndex_3; }
	inline int32_t* get_address_of_fingerIndex_3() { return &___fingerIndex_3; }
	inline void set_fingerIndex_3(int32_t value)
	{
		___fingerIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSINGLEORDOUBLEU3ED__229_TA04A31FF7C737CB4E23EBEA145EB660FAE758625_H
#ifndef DOUBLETAP_T770BAA916711CCD2CFCCBA7305212234BD0CA3CD_H
#define DOUBLETAP_T770BAA916711CCD2CFCCBA7305212234BD0CA3CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DoubleTap
struct  DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD  : public RuntimeObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::inDoubleTap
	bool ___inDoubleTap_0;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::inWait
	bool ___inWait_1;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::time
	float ___time_2;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::count
	int32_t ___count_3;
	// HedgehogTeam.EasyTouch.Finger HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::finger
	Finger_t1C55110621038C162D210785A1F1490A0273B205 * ___finger_4;

public:
	inline static int32_t get_offset_of_inDoubleTap_0() { return static_cast<int32_t>(offsetof(DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD, ___inDoubleTap_0)); }
	inline bool get_inDoubleTap_0() const { return ___inDoubleTap_0; }
	inline bool* get_address_of_inDoubleTap_0() { return &___inDoubleTap_0; }
	inline void set_inDoubleTap_0(bool value)
	{
		___inDoubleTap_0 = value;
	}

	inline static int32_t get_offset_of_inWait_1() { return static_cast<int32_t>(offsetof(DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD, ___inWait_1)); }
	inline bool get_inWait_1() const { return ___inWait_1; }
	inline bool* get_address_of_inWait_1() { return &___inWait_1; }
	inline void set_inWait_1(bool value)
	{
		___inWait_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_finger_4() { return static_cast<int32_t>(offsetof(DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD, ___finger_4)); }
	inline Finger_t1C55110621038C162D210785A1F1490A0273B205 * get_finger_4() const { return ___finger_4; }
	inline Finger_t1C55110621038C162D210785A1F1490A0273B205 ** get_address_of_finger_4() { return &___finger_4; }
	inline void set_finger_4(Finger_t1C55110621038C162D210785A1F1490A0273B205 * value)
	{
		___finger_4 = value;
		Il2CppCodeGenWriteBarrier((&___finger_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAP_T770BAA916711CCD2CFCCBA7305212234BD0CA3CD_H
#ifndef PICKEDOBJECT_T1252EC4DBC7B31CB1265013835D4CBBEE7700B05_H
#define PICKEDOBJECT_T1252EC4DBC7B31CB1265013835D4CBBEE7700B05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PickedObject
struct  PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05  : public RuntimeObject
{
public:
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch_PickedObject::pickedObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedObj_0;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch_PickedObject::pickedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___pickedCamera_1;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch_PickedObject::isGUI
	bool ___isGUI_2;

public:
	inline static int32_t get_offset_of_pickedObj_0() { return static_cast<int32_t>(offsetof(PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05, ___pickedObj_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedObj_0() const { return ___pickedObj_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedObj_0() { return &___pickedObj_0; }
	inline void set_pickedObj_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedObj_0 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObj_0), value);
	}

	inline static int32_t get_offset_of_pickedCamera_1() { return static_cast<int32_t>(offsetof(PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05, ___pickedCamera_1)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_pickedCamera_1() const { return ___pickedCamera_1; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_pickedCamera_1() { return &___pickedCamera_1; }
	inline void set_pickedCamera_1(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___pickedCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_1), value);
	}

	inline static int32_t get_offset_of_isGUI_2() { return static_cast<int32_t>(offsetof(PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05, ___isGUI_2)); }
	inline bool get_isGUI_2() const { return ___isGUI_2; }
	inline bool* get_address_of_isGUI_2() { return &___isGUI_2; }
	inline void set_isGUI_2(bool value)
	{
		___isGUI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKEDOBJECT_T1252EC4DBC7B31CB1265013835D4CBBEE7700B05_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef GAMEEVENTMESSAGE_TB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE_H
#define GAMEEVENTMESSAGE_TB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.GameEventMessage
struct  GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// System.Boolean Doozy.Engine.GameEventMessage::<IsSystemEvent>k__BackingField
	bool ___U3CIsSystemEventU3Ek__BackingField_4;
	// System.String Doozy.Engine.GameEventMessage::EventName
	String_t* ___EventName_5;
	// UnityEngine.GameObject Doozy.Engine.GameEventMessage::Source
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Source_6;

public:
	inline static int32_t get_offset_of_U3CIsSystemEventU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE, ___U3CIsSystemEventU3Ek__BackingField_4)); }
	inline bool get_U3CIsSystemEventU3Ek__BackingField_4() const { return ___U3CIsSystemEventU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsSystemEventU3Ek__BackingField_4() { return &___U3CIsSystemEventU3Ek__BackingField_4; }
	inline void set_U3CIsSystemEventU3Ek__BackingField_4(bool value)
	{
		___U3CIsSystemEventU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_EventName_5() { return static_cast<int32_t>(offsetof(GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE, ___EventName_5)); }
	inline String_t* get_EventName_5() const { return ___EventName_5; }
	inline String_t** get_address_of_EventName_5() { return &___EventName_5; }
	inline void set_EventName_5(String_t* value)
	{
		___EventName_5 = value;
		Il2CppCodeGenWriteBarrier((&___EventName_5), value);
	}

	inline static int32_t get_offset_of_Source_6() { return static_cast<int32_t>(offsetof(GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE, ___Source_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Source_6() const { return ___Source_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Source_6() { return &___Source_6; }
	inline void set_Source_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Source_6 = value;
		Il2CppCodeGenWriteBarrier((&___Source_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTMESSAGE_TB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE_H
#ifndef RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#define RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.RangedFloat
struct  RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109 
{
public:
	// System.Single Doozy.Engine.RangedFloat::MinValue
	float ___MinValue_0;
	// System.Single Doozy.Engine.RangedFloat::MaxValue
	float ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#define UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<HedgehogTeam.EasyTouch.Gesture>
struct  UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T5E400396ACE62817E46E296A845A6E2D50E81CD4_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#define LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Language
struct  Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874 
{
public:
	// System.Int32 Doozy.Engine.Language::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#ifndef MCOLOR_T478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0_H
#define MCOLOR_T478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.MColor
struct  MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.MColor::Name
	String_t* ___Name_0;
	// UnityEngine.Color Doozy.Engine.MColor::M50
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M50_1;
	// UnityEngine.Color Doozy.Engine.MColor::M100
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M100_2;
	// UnityEngine.Color Doozy.Engine.MColor::M200
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M200_3;
	// UnityEngine.Color Doozy.Engine.MColor::M300
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M300_4;
	// UnityEngine.Color Doozy.Engine.MColor::M400
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M400_5;
	// UnityEngine.Color Doozy.Engine.MColor::M500
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M500_6;
	// UnityEngine.Color Doozy.Engine.MColor::M600
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M600_7;
	// UnityEngine.Color Doozy.Engine.MColor::M700
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M700_8;
	// UnityEngine.Color Doozy.Engine.MColor::M800
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M800_9;
	// UnityEngine.Color Doozy.Engine.MColor::M900
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___M900_10;
	// UnityEngine.Color Doozy.Engine.MColor::A100
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___A100_11;
	// UnityEngine.Color Doozy.Engine.MColor::A200
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___A200_12;
	// UnityEngine.Color Doozy.Engine.MColor::A400
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___A400_13;
	// UnityEngine.Color Doozy.Engine.MColor::A700
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___A700_14;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_M50_1() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M50_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M50_1() const { return ___M50_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M50_1() { return &___M50_1; }
	inline void set_M50_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M50_1 = value;
	}

	inline static int32_t get_offset_of_M100_2() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M100_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M100_2() const { return ___M100_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M100_2() { return &___M100_2; }
	inline void set_M100_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M100_2 = value;
	}

	inline static int32_t get_offset_of_M200_3() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M200_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M200_3() const { return ___M200_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M200_3() { return &___M200_3; }
	inline void set_M200_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M200_3 = value;
	}

	inline static int32_t get_offset_of_M300_4() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M300_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M300_4() const { return ___M300_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M300_4() { return &___M300_4; }
	inline void set_M300_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M300_4 = value;
	}

	inline static int32_t get_offset_of_M400_5() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M400_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M400_5() const { return ___M400_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M400_5() { return &___M400_5; }
	inline void set_M400_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M400_5 = value;
	}

	inline static int32_t get_offset_of_M500_6() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M500_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M500_6() const { return ___M500_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M500_6() { return &___M500_6; }
	inline void set_M500_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M500_6 = value;
	}

	inline static int32_t get_offset_of_M600_7() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M600_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M600_7() const { return ___M600_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M600_7() { return &___M600_7; }
	inline void set_M600_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M600_7 = value;
	}

	inline static int32_t get_offset_of_M700_8() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M700_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M700_8() const { return ___M700_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M700_8() { return &___M700_8; }
	inline void set_M700_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M700_8 = value;
	}

	inline static int32_t get_offset_of_M800_9() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M800_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M800_9() const { return ___M800_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M800_9() { return &___M800_9; }
	inline void set_M800_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M800_9 = value;
	}

	inline static int32_t get_offset_of_M900_10() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___M900_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_M900_10() const { return ___M900_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_M900_10() { return &___M900_10; }
	inline void set_M900_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___M900_10 = value;
	}

	inline static int32_t get_offset_of_A100_11() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___A100_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_A100_11() const { return ___A100_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_A100_11() { return &___A100_11; }
	inline void set_A100_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___A100_11 = value;
	}

	inline static int32_t get_offset_of_A200_12() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___A200_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_A200_12() const { return ___A200_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_A200_12() { return &___A200_12; }
	inline void set_A200_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___A200_12 = value;
	}

	inline static int32_t get_offset_of_A400_13() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___A400_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_A400_13() const { return ___A400_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_A400_13() { return &___A400_13; }
	inline void set_A400_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___A400_13 = value;
	}

	inline static int32_t get_offset_of_A700_14() { return static_cast<int32_t>(offsetof(MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0, ___A700_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_A700_14() const { return ___A700_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_A700_14() { return &___A700_14; }
	inline void set_A700_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___A700_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MCOLOR_T478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0_H
#ifndef MODULE_T9ABC50C1B8826DC14B89EDC57A836097536536CE_H
#define MODULE_T9ABC50C1B8826DC14B89EDC57A836097536536CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Module
struct  Module_t9ABC50C1B8826DC14B89EDC57A836097536536CE 
{
public:
	// System.Int32 Doozy.Engine.Module::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Module_t9ABC50C1B8826DC14B89EDC57A836097536536CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULE_T9ABC50C1B8826DC14B89EDC57A836097536536CE_H
#ifndef TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#define TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.TargetVariable
struct  TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF 
{
public:
	// System.Int32 Doozy.Engine.Progress.TargetVariable::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#ifndef SETUPMODE_TADC952D5BB14901A477FDE98692C068C3B931560_H
#define SETUPMODE_TADC952D5BB14901A477FDE98692C068C3B931560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SetupMode
struct  SetupMode_tADC952D5BB14901A477FDE98692C068C3B931560 
{
public:
	// System.Int32 Doozy.Engine.SetupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SetupMode_tADC952D5BB14901A477FDE98692C068C3B931560, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPMODE_TADC952D5BB14901A477FDE98692C068C3B931560_H
#ifndef SYSTEMGAMEEVENT_TF2E56884D1E47C416C36DC3B16CDA2CCC2B90053_H
#define SYSTEMGAMEEVENT_TF2E56884D1E47C416C36DC3B16CDA2CCC2B90053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SystemGameEvent
struct  SystemGameEvent_tF2E56884D1E47C416C36DC3B16CDA2CCC2B90053 
{
public:
	// System.Int32 Doozy.Engine.SystemGameEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemGameEvent_tF2E56884D1E47C416C36DC3B16CDA2CCC2B90053, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMGAMEEVENT_TF2E56884D1E47C416C36DC3B16CDA2CCC2B90053_H
#ifndef UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#define UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonBehaviorType
struct  UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756 
{
public:
	// System.Int32 Doozy.Engine.UI.UIButtonBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#ifndef CONVERSIONS_T7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA_H
#define CONVERSIONS_T7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorUtils_Conversions
struct  Conversions_t7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA 
{
public:
	// System.Int32 Doozy.Engine.Utils.ColorUtils_Conversions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Conversions_t7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONS_T7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA_H
#ifndef DCOLOR_T873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37_H
#define DCOLOR_T873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.DColor
struct  DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.Utils.DColor::ColorName
	String_t* ___ColorName_1;
	// UnityEngine.Color Doozy.Engine.Utils.DColor::Light
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___Light_2;
	// UnityEngine.Color Doozy.Engine.Utils.DColor::Normal
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___Normal_3;
	// UnityEngine.Color Doozy.Engine.Utils.DColor::Dark
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___Dark_4;

public:
	inline static int32_t get_offset_of_ColorName_1() { return static_cast<int32_t>(offsetof(DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37, ___ColorName_1)); }
	inline String_t* get_ColorName_1() const { return ___ColorName_1; }
	inline String_t** get_address_of_ColorName_1() { return &___ColorName_1; }
	inline void set_ColorName_1(String_t* value)
	{
		___ColorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ColorName_1), value);
	}

	inline static int32_t get_offset_of_Light_2() { return static_cast<int32_t>(offsetof(DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37, ___Light_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_Light_2() const { return ___Light_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_Light_2() { return &___Light_2; }
	inline void set_Light_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___Light_2 = value;
	}

	inline static int32_t get_offset_of_Normal_3() { return static_cast<int32_t>(offsetof(DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37, ___Normal_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_Normal_3() const { return ___Normal_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_Normal_3() { return &___Normal_3; }
	inline void set_Normal_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___Normal_3 = value;
	}

	inline static int32_t get_offset_of_Dark_4() { return static_cast<int32_t>(offsetof(DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37, ___Dark_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_Dark_4() const { return ___Dark_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_Dark_4() { return &___Dark_4; }
	inline void set_Dark_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___Dark_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DCOLOR_T873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37_H
#ifndef COMPONENTNAME_TE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D_H
#define COMPONENTNAME_TE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.DoozyPath_ComponentName
struct  ComponentName_tE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D 
{
public:
	// System.Int32 Doozy.Engine.Utils.DoozyPath_ComponentName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ComponentName_tE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTNAME_TE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D_H
#ifndef DOOZYUTILS_T72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_H
#define DOOZYUTILS_T72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.DoozyUtils
struct  DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11  : public RuntimeObject
{
public:

public:
};

struct DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields
{
public:
	// UnityEngine.Color Doozy.Engine.Utils.DoozyUtils::BackgroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BackgroundColor_2;
	// UnityEngine.Color Doozy.Engine.Utils.DoozyUtils::CheckmarkColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___CheckmarkColor_3;
	// UnityEngine.Color Doozy.Engine.Utils.DoozyUtils::OverlayColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___OverlayColor_4;
	// UnityEngine.Color Doozy.Engine.Utils.DoozyUtils::TextColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___TextColor_5;

public:
	inline static int32_t get_offset_of_BackgroundColor_2() { return static_cast<int32_t>(offsetof(DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields, ___BackgroundColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BackgroundColor_2() const { return ___BackgroundColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BackgroundColor_2() { return &___BackgroundColor_2; }
	inline void set_BackgroundColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BackgroundColor_2 = value;
	}

	inline static int32_t get_offset_of_CheckmarkColor_3() { return static_cast<int32_t>(offsetof(DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields, ___CheckmarkColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_CheckmarkColor_3() const { return ___CheckmarkColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_CheckmarkColor_3() { return &___CheckmarkColor_3; }
	inline void set_CheckmarkColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___CheckmarkColor_3 = value;
	}

	inline static int32_t get_offset_of_OverlayColor_4() { return static_cast<int32_t>(offsetof(DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields, ___OverlayColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_OverlayColor_4() const { return ___OverlayColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_OverlayColor_4() { return &___OverlayColor_4; }
	inline void set_OverlayColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___OverlayColor_4 = value;
	}

	inline static int32_t get_offset_of_TextColor_5() { return static_cast<int32_t>(offsetof(DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields, ___TextColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_TextColor_5() const { return ___TextColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_TextColor_5() { return &___TextColor_5; }
	inline void set_TextColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___TextColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOOZYUTILS_T72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_H
#ifndef DOUBLETAPDETECTION_TCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1_H
#define DOUBLETAPDETECTION_TCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DoubleTapDetection
struct  DoubleTapDetection_tCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_DoubleTapDetection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DoubleTapDetection_tCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAPDETECTION_TCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1_H
#ifndef EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#define EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_EvtType
struct  EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_EvtType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVTTYPE_TF2599608DD71DDF1D3511085CE11F92749BBEDEB_H
#ifndef GESTUREPRIORITY_T510B565B1C472F1433ECC01CB698AF28E26E7595_H
#define GESTUREPRIORITY_T510B565B1C472F1433ECC01CB698AF28E26E7595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_GesturePriority
struct  GesturePriority_t510B565B1C472F1433ECC01CB698AF28E26E7595 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_GesturePriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GesturePriority_t510B565B1C472F1433ECC01CB698AF28E26E7595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREPRIORITY_T510B565B1C472F1433ECC01CB698AF28E26E7595_H
#ifndef GESTURETYPE_T363EDA7F208202EE99EF740869A7DC7223E1D811_H
#define GESTURETYPE_T363EDA7F208202EE99EF740869A7DC7223E1D811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_GestureType
struct  GestureType_t363EDA7F208202EE99EF740869A7DC7223E1D811 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_GestureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureType_t363EDA7F208202EE99EF740869A7DC7223E1D811, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURETYPE_T363EDA7F208202EE99EF740869A7DC7223E1D811_H
#ifndef SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#define SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection
struct  SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_TEC87368746528A6E300E8C86A39C67F74E4004FE_H
#ifndef TWOFINGERPICKMETHOD_T2C80556D7952522FA119B278E0C0D330039C772B_H
#define TWOFINGERPICKMETHOD_T2C80556D7952522FA119B278E0C0D330039C772B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TwoFingerPickMethod
struct  TwoFingerPickMethod_t2C80556D7952522FA119B278E0C0D330039C772B 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch_TwoFingerPickMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TwoFingerPickMethod_t2C80556D7952522FA119B278E0C0D330039C772B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOFINGERPICKMETHOD_T2C80556D7952522FA119B278E0C0D330039C772B_H
#ifndef EASYTOUCHINPUT_T128D6FA07BC618F0BA809DA3D8C0C733E8400708_H
#define EASYTOUCHINPUT_T128D6FA07BC618F0BA809DA3D8C0C733E8400708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchInput
struct  EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708  : public RuntimeObject
{
public:
	// UnityEngine.Vector2[] HedgehogTeam.EasyTouch.EasyTouchInput::oldMousePosition
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___oldMousePosition_0;
	// System.Int32[] HedgehogTeam.EasyTouch.EasyTouchInput::tapCount
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tapCount_1;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::startActionTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___startActionTime_2;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::deltaTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___deltaTime_3;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::tapeTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___tapeTime_4;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchInput::bComplex
	bool ___bComplex_5;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::deltaFingerPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___deltaFingerPosition_6;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::oldFinger2Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldFinger2Position_7;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::complexCenter
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___complexCenter_8;

public:
	inline static int32_t get_offset_of_oldMousePosition_0() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___oldMousePosition_0)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_oldMousePosition_0() const { return ___oldMousePosition_0; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_oldMousePosition_0() { return &___oldMousePosition_0; }
	inline void set_oldMousePosition_0(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___oldMousePosition_0 = value;
		Il2CppCodeGenWriteBarrier((&___oldMousePosition_0), value);
	}

	inline static int32_t get_offset_of_tapCount_1() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___tapCount_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tapCount_1() const { return ___tapCount_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tapCount_1() { return &___tapCount_1; }
	inline void set_tapCount_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tapCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___tapCount_1), value);
	}

	inline static int32_t get_offset_of_startActionTime_2() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___startActionTime_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_startActionTime_2() const { return ___startActionTime_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_startActionTime_2() { return &___startActionTime_2; }
	inline void set_startActionTime_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___startActionTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___startActionTime_2), value);
	}

	inline static int32_t get_offset_of_deltaTime_3() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___deltaTime_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_deltaTime_3() const { return ___deltaTime_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_deltaTime_3() { return &___deltaTime_3; }
	inline void set_deltaTime_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___deltaTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___deltaTime_3), value);
	}

	inline static int32_t get_offset_of_tapeTime_4() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___tapeTime_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_tapeTime_4() const { return ___tapeTime_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_tapeTime_4() { return &___tapeTime_4; }
	inline void set_tapeTime_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___tapeTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___tapeTime_4), value);
	}

	inline static int32_t get_offset_of_bComplex_5() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___bComplex_5)); }
	inline bool get_bComplex_5() const { return ___bComplex_5; }
	inline bool* get_address_of_bComplex_5() { return &___bComplex_5; }
	inline void set_bComplex_5(bool value)
	{
		___bComplex_5 = value;
	}

	inline static int32_t get_offset_of_deltaFingerPosition_6() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___deltaFingerPosition_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_deltaFingerPosition_6() const { return ___deltaFingerPosition_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_deltaFingerPosition_6() { return &___deltaFingerPosition_6; }
	inline void set_deltaFingerPosition_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___deltaFingerPosition_6 = value;
	}

	inline static int32_t get_offset_of_oldFinger2Position_7() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___oldFinger2Position_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldFinger2Position_7() const { return ___oldFinger2Position_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldFinger2Position_7() { return &___oldFinger2Position_7; }
	inline void set_oldFinger2Position_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldFinger2Position_7 = value;
	}

	inline static int32_t get_offset_of_complexCenter_8() { return static_cast<int32_t>(offsetof(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708, ___complexCenter_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_complexCenter_8() const { return ___complexCenter_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_complexCenter_8() { return &___complexCenter_8; }
	inline void set_complexCenter_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___complexCenter_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHINPUT_T128D6FA07BC618F0BA809DA3D8C0C733E8400708_H
#ifndef AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#define AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction
struct  AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AffectedAxesAction_t17525013560DA17DF342324DD05709442FA0E35F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFFECTEDAXESACTION_T17525013560DA17DF342324DD05709442FA0E35F_H
#ifndef DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#define DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_DirectAction
struct  DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_DirectAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectAction_t2DA4B931DD5F12B094BF8F34956F693ECF9A3394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T2DA4B931DD5F12B094BF8F34956F693ECF9A3394_H
#ifndef GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#define GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase_GameObjectType
struct  GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase_GameObjectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameObjectType_t492E23ADC8FD3BDFC9BCC107B25F269EF496843D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTTYPE_T492E23ADC8FD3BDFC9BCC107B25F269EF496843D_H
#ifndef ACTIONTRIGGERING_T4EBEF0C41F664AD4F2486D1D94FB08B68756BECC_H
#define ACTIONTRIGGERING_T4EBEF0C41F664AD4F2486D1D94FB08B68756BECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch_ActionTriggering
struct  ActionTriggering_t4EBEF0C41F664AD4F2486D1D94FB08B68756BECC 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTouch_ActionTriggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTriggering_t4EBEF0C41F664AD4F2486D1D94FB08B68756BECC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T4EBEF0C41F664AD4F2486D1D94FB08B68756BECC_H
#ifndef ONTOUCH_T82B81EB29D25795F77E81BAD75857C97A1712064_H
#define ONTOUCH_T82B81EB29D25795F77E81BAD75857C97A1712064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch_OnTouch
struct  OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCH_T82B81EB29D25795F77E81BAD75857C97A1712064_H
#ifndef ONTOUCHNOTOVERME_T4ECA3D0BCC536612665000E70BDEB32F4D978C5B_H
#define ONTOUCHNOTOVERME_T4ECA3D0BCC536612665000E70BDEB32F4D978C5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch_OnTouchNotOverMe
struct  OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHNOTOVERME_T4ECA3D0BCC536612665000E70BDEB32F4D978C5B_H
#ifndef ACTIONROTATIONDIRECTION_TA352B79DFBD10C88D34C35B51D576CB8FABC76D1_H
#define ACTIONROTATIONDIRECTION_TA352B79DFBD10C88D34C35B51D576CB8FABC76D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist_ActionRotationDirection
struct  ActionRotationDirection_tA352B79DFBD10C88D34C35B51D576CB8FABC76D1 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTwist_ActionRotationDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionRotationDirection_tA352B79DFBD10C88D34C35B51D576CB8FABC76D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONROTATIONDIRECTION_TA352B79DFBD10C88D34C35B51D576CB8FABC76D1_H
#ifndef ACTIONTIGGERING_T25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D_H
#define ACTIONTIGGERING_T25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist_ActionTiggering
struct  ActionTiggering_t25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTwist_ActionTiggering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionTiggering_t25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTIGGERING_T25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D_H
#ifndef ONTWISTACTION_TBA2645893393633838C9D725F00FE87DB85F2203_H
#define ONTWISTACTION_TBA2645893393633838C9D725F00FE87DB85F2203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist_OnTwistAction
struct  OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203  : public UnityEvent_1_t5E400396ACE62817E46E296A845A6E2D50E81CD4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTWISTACTION_TBA2645893393633838C9D725F00FE87DB85F2203_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#define BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.BaseFinger
struct  BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::touchCount
	int32_t ___touchCount_1;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_2;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_3;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::deltaPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___deltaPosition_4;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::actionTime
	float ___actionTime_5;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::deltaTime
	float ___deltaTime_6;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.BaseFinger::pickedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___pickedCamera_7;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedObject_8;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isGuiCamera
	bool ___isGuiCamera_9;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isOverGui
	bool ___isOverGui_10;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedUIElement
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedUIElement_11;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::altitudeAngle
	float ___altitudeAngle_12;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::azimuthAngle
	float ___azimuthAngle_13;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::maximumPossiblePressure
	float ___maximumPossiblePressure_14;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::pressure
	float ___pressure_15;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radius
	float ___radius_16;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radiusVariance
	float ___radiusVariance_17;
	// UnityEngine.TouchType HedgehogTeam.EasyTouch.BaseFinger::touchType
	int32_t ___touchType_18;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_touchCount_1() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___touchCount_1)); }
	inline int32_t get_touchCount_1() const { return ___touchCount_1; }
	inline int32_t* get_address_of_touchCount_1() { return &___touchCount_1; }
	inline void set_touchCount_1(int32_t value)
	{
		___touchCount_1 = value;
	}

	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___startPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___position_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_3() const { return ___position_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_4() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___deltaPosition_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_deltaPosition_4() const { return ___deltaPosition_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_deltaPosition_4() { return &___deltaPosition_4; }
	inline void set_deltaPosition_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___deltaPosition_4 = value;
	}

	inline static int32_t get_offset_of_actionTime_5() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___actionTime_5)); }
	inline float get_actionTime_5() const { return ___actionTime_5; }
	inline float* get_address_of_actionTime_5() { return &___actionTime_5; }
	inline void set_actionTime_5(float value)
	{
		___actionTime_5 = value;
	}

	inline static int32_t get_offset_of_deltaTime_6() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___deltaTime_6)); }
	inline float get_deltaTime_6() const { return ___deltaTime_6; }
	inline float* get_address_of_deltaTime_6() { return &___deltaTime_6; }
	inline void set_deltaTime_6(float value)
	{
		___deltaTime_6 = value;
	}

	inline static int32_t get_offset_of_pickedCamera_7() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_pickedCamera_7() const { return ___pickedCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_pickedCamera_7() { return &___pickedCamera_7; }
	inline void set_pickedCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___pickedCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_7), value);
	}

	inline static int32_t get_offset_of_pickedObject_8() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedObject_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedObject_8() const { return ___pickedObject_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedObject_8() { return &___pickedObject_8; }
	inline void set_pickedObject_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_8), value);
	}

	inline static int32_t get_offset_of_isGuiCamera_9() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___isGuiCamera_9)); }
	inline bool get_isGuiCamera_9() const { return ___isGuiCamera_9; }
	inline bool* get_address_of_isGuiCamera_9() { return &___isGuiCamera_9; }
	inline void set_isGuiCamera_9(bool value)
	{
		___isGuiCamera_9 = value;
	}

	inline static int32_t get_offset_of_isOverGui_10() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___isOverGui_10)); }
	inline bool get_isOverGui_10() const { return ___isOverGui_10; }
	inline bool* get_address_of_isOverGui_10() { return &___isOverGui_10; }
	inline void set_isOverGui_10(bool value)
	{
		___isOverGui_10 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_11() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pickedUIElement_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedUIElement_11() const { return ___pickedUIElement_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedUIElement_11() { return &___pickedUIElement_11; }
	inline void set_pickedUIElement_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedUIElement_11 = value;
		Il2CppCodeGenWriteBarrier((&___pickedUIElement_11), value);
	}

	inline static int32_t get_offset_of_altitudeAngle_12() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___altitudeAngle_12)); }
	inline float get_altitudeAngle_12() const { return ___altitudeAngle_12; }
	inline float* get_address_of_altitudeAngle_12() { return &___altitudeAngle_12; }
	inline void set_altitudeAngle_12(float value)
	{
		___altitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_azimuthAngle_13() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___azimuthAngle_13)); }
	inline float get_azimuthAngle_13() const { return ___azimuthAngle_13; }
	inline float* get_address_of_azimuthAngle_13() { return &___azimuthAngle_13; }
	inline void set_azimuthAngle_13(float value)
	{
		___azimuthAngle_13 = value;
	}

	inline static int32_t get_offset_of_maximumPossiblePressure_14() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___maximumPossiblePressure_14)); }
	inline float get_maximumPossiblePressure_14() const { return ___maximumPossiblePressure_14; }
	inline float* get_address_of_maximumPossiblePressure_14() { return &___maximumPossiblePressure_14; }
	inline void set_maximumPossiblePressure_14(float value)
	{
		___maximumPossiblePressure_14 = value;
	}

	inline static int32_t get_offset_of_pressure_15() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___pressure_15)); }
	inline float get_pressure_15() const { return ___pressure_15; }
	inline float* get_address_of_pressure_15() { return &___pressure_15; }
	inline void set_pressure_15(float value)
	{
		___pressure_15 = value;
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___radius_16)); }
	inline float get_radius_16() const { return ___radius_16; }
	inline float* get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(float value)
	{
		___radius_16 = value;
	}

	inline static int32_t get_offset_of_radiusVariance_17() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___radiusVariance_17)); }
	inline float get_radiusVariance_17() const { return ___radiusVariance_17; }
	inline float* get_address_of_radiusVariance_17() { return &___radiusVariance_17; }
	inline void set_radiusVariance_17(float value)
	{
		___radiusVariance_17 = value;
	}

	inline static int32_t get_offset_of_touchType_18() { return static_cast<int32_t>(offsetof(BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10, ___touchType_18)); }
	inline int32_t get_touchType_18() const { return ___touchType_18; }
	inline int32_t* get_address_of_touchType_18() { return &___touchType_18; }
	inline void set_touchType_18(int32_t value)
	{
		___touchType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFINGER_T9768A6FE4C44544089206888AB7529092AB2FD10_H
#ifndef TWOFINGERGESTURE_TE01A70108992AD19FFB0D7F47A418873C9269A0B_H
#define TWOFINGERGESTURE_TE01A70108992AD19FFB0D7F47A418873C9269A0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.TwoFingerGesture
struct  TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::currentGesture
	int32_t ___currentGesture_0;
	// HedgehogTeam.EasyTouch.EasyTouch_GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::oldGesture
	int32_t ___oldGesture_1;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger0
	int32_t ___finger0_2;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger1
	int32_t ___finger1_3;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startTimeAction
	float ___startTimeAction_4;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::timeSinceStartAction
	float ___timeSinceStartAction_5;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_6;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_7;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::deltaPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___deltaPosition_8;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::oldStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldStartPosition_9;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startDistance
	float ___startDistance_10;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::fingerDistance
	float ___fingerDistance_11;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::oldFingerDistance
	float ___oldFingerDistance_12;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockPinch
	bool ___lockPinch_13;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockTwist
	bool ___lockTwist_14;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastPinch
	float ___lastPinch_15;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastTwistAngle
	float ___lastTwistAngle_16;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedObject_17;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::oldPickedObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___oldPickedObject_18;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.TwoFingerGesture::pickedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___pickedCamera_19;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isGuiCamera
	bool ___isGuiCamera_20;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isOverGui
	bool ___isOverGui_21;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedUIElement
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickedUIElement_22;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::dragStart
	bool ___dragStart_23;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::swipeStart
	bool ___swipeStart_24;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::inSingleDoubleTaps
	bool ___inSingleDoubleTaps_25;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::tapCurentTime
	float ___tapCurentTime_26;

public:
	inline static int32_t get_offset_of_currentGesture_0() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___currentGesture_0)); }
	inline int32_t get_currentGesture_0() const { return ___currentGesture_0; }
	inline int32_t* get_address_of_currentGesture_0() { return &___currentGesture_0; }
	inline void set_currentGesture_0(int32_t value)
	{
		___currentGesture_0 = value;
	}

	inline static int32_t get_offset_of_oldGesture_1() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___oldGesture_1)); }
	inline int32_t get_oldGesture_1() const { return ___oldGesture_1; }
	inline int32_t* get_address_of_oldGesture_1() { return &___oldGesture_1; }
	inline void set_oldGesture_1(int32_t value)
	{
		___oldGesture_1 = value;
	}

	inline static int32_t get_offset_of_finger0_2() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___finger0_2)); }
	inline int32_t get_finger0_2() const { return ___finger0_2; }
	inline int32_t* get_address_of_finger0_2() { return &___finger0_2; }
	inline void set_finger0_2(int32_t value)
	{
		___finger0_2 = value;
	}

	inline static int32_t get_offset_of_finger1_3() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___finger1_3)); }
	inline int32_t get_finger1_3() const { return ___finger1_3; }
	inline int32_t* get_address_of_finger1_3() { return &___finger1_3; }
	inline void set_finger1_3(int32_t value)
	{
		___finger1_3 = value;
	}

	inline static int32_t get_offset_of_startTimeAction_4() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___startTimeAction_4)); }
	inline float get_startTimeAction_4() const { return ___startTimeAction_4; }
	inline float* get_address_of_startTimeAction_4() { return &___startTimeAction_4; }
	inline void set_startTimeAction_4(float value)
	{
		___startTimeAction_4 = value;
	}

	inline static int32_t get_offset_of_timeSinceStartAction_5() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___timeSinceStartAction_5)); }
	inline float get_timeSinceStartAction_5() const { return ___timeSinceStartAction_5; }
	inline float* get_address_of_timeSinceStartAction_5() { return &___timeSinceStartAction_5; }
	inline void set_timeSinceStartAction_5(float value)
	{
		___timeSinceStartAction_5 = value;
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___startPosition_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___position_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_7() const { return ___position_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_8() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___deltaPosition_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_deltaPosition_8() const { return ___deltaPosition_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_deltaPosition_8() { return &___deltaPosition_8; }
	inline void set_deltaPosition_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___deltaPosition_8 = value;
	}

	inline static int32_t get_offset_of_oldStartPosition_9() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___oldStartPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldStartPosition_9() const { return ___oldStartPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldStartPosition_9() { return &___oldStartPosition_9; }
	inline void set_oldStartPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldStartPosition_9 = value;
	}

	inline static int32_t get_offset_of_startDistance_10() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___startDistance_10)); }
	inline float get_startDistance_10() const { return ___startDistance_10; }
	inline float* get_address_of_startDistance_10() { return &___startDistance_10; }
	inline void set_startDistance_10(float value)
	{
		___startDistance_10 = value;
	}

	inline static int32_t get_offset_of_fingerDistance_11() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___fingerDistance_11)); }
	inline float get_fingerDistance_11() const { return ___fingerDistance_11; }
	inline float* get_address_of_fingerDistance_11() { return &___fingerDistance_11; }
	inline void set_fingerDistance_11(float value)
	{
		___fingerDistance_11 = value;
	}

	inline static int32_t get_offset_of_oldFingerDistance_12() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___oldFingerDistance_12)); }
	inline float get_oldFingerDistance_12() const { return ___oldFingerDistance_12; }
	inline float* get_address_of_oldFingerDistance_12() { return &___oldFingerDistance_12; }
	inline void set_oldFingerDistance_12(float value)
	{
		___oldFingerDistance_12 = value;
	}

	inline static int32_t get_offset_of_lockPinch_13() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___lockPinch_13)); }
	inline bool get_lockPinch_13() const { return ___lockPinch_13; }
	inline bool* get_address_of_lockPinch_13() { return &___lockPinch_13; }
	inline void set_lockPinch_13(bool value)
	{
		___lockPinch_13 = value;
	}

	inline static int32_t get_offset_of_lockTwist_14() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___lockTwist_14)); }
	inline bool get_lockTwist_14() const { return ___lockTwist_14; }
	inline bool* get_address_of_lockTwist_14() { return &___lockTwist_14; }
	inline void set_lockTwist_14(bool value)
	{
		___lockTwist_14 = value;
	}

	inline static int32_t get_offset_of_lastPinch_15() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___lastPinch_15)); }
	inline float get_lastPinch_15() const { return ___lastPinch_15; }
	inline float* get_address_of_lastPinch_15() { return &___lastPinch_15; }
	inline void set_lastPinch_15(float value)
	{
		___lastPinch_15 = value;
	}

	inline static int32_t get_offset_of_lastTwistAngle_16() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___lastTwistAngle_16)); }
	inline float get_lastTwistAngle_16() const { return ___lastTwistAngle_16; }
	inline float* get_address_of_lastTwistAngle_16() { return &___lastTwistAngle_16; }
	inline void set_lastTwistAngle_16(float value)
	{
		___lastTwistAngle_16 = value;
	}

	inline static int32_t get_offset_of_pickedObject_17() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___pickedObject_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedObject_17() const { return ___pickedObject_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedObject_17() { return &___pickedObject_17; }
	inline void set_pickedObject_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_17), value);
	}

	inline static int32_t get_offset_of_oldPickedObject_18() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___oldPickedObject_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_oldPickedObject_18() const { return ___oldPickedObject_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_oldPickedObject_18() { return &___oldPickedObject_18; }
	inline void set_oldPickedObject_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___oldPickedObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___oldPickedObject_18), value);
	}

	inline static int32_t get_offset_of_pickedCamera_19() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___pickedCamera_19)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_pickedCamera_19() const { return ___pickedCamera_19; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_pickedCamera_19() { return &___pickedCamera_19; }
	inline void set_pickedCamera_19(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___pickedCamera_19 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_19), value);
	}

	inline static int32_t get_offset_of_isGuiCamera_20() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___isGuiCamera_20)); }
	inline bool get_isGuiCamera_20() const { return ___isGuiCamera_20; }
	inline bool* get_address_of_isGuiCamera_20() { return &___isGuiCamera_20; }
	inline void set_isGuiCamera_20(bool value)
	{
		___isGuiCamera_20 = value;
	}

	inline static int32_t get_offset_of_isOverGui_21() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___isOverGui_21)); }
	inline bool get_isOverGui_21() const { return ___isOverGui_21; }
	inline bool* get_address_of_isOverGui_21() { return &___isOverGui_21; }
	inline void set_isOverGui_21(bool value)
	{
		___isOverGui_21 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_22() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___pickedUIElement_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickedUIElement_22() const { return ___pickedUIElement_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickedUIElement_22() { return &___pickedUIElement_22; }
	inline void set_pickedUIElement_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickedUIElement_22 = value;
		Il2CppCodeGenWriteBarrier((&___pickedUIElement_22), value);
	}

	inline static int32_t get_offset_of_dragStart_23() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___dragStart_23)); }
	inline bool get_dragStart_23() const { return ___dragStart_23; }
	inline bool* get_address_of_dragStart_23() { return &___dragStart_23; }
	inline void set_dragStart_23(bool value)
	{
		___dragStart_23 = value;
	}

	inline static int32_t get_offset_of_swipeStart_24() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___swipeStart_24)); }
	inline bool get_swipeStart_24() const { return ___swipeStart_24; }
	inline bool* get_address_of_swipeStart_24() { return &___swipeStart_24; }
	inline void set_swipeStart_24(bool value)
	{
		___swipeStart_24 = value;
	}

	inline static int32_t get_offset_of_inSingleDoubleTaps_25() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___inSingleDoubleTaps_25)); }
	inline bool get_inSingleDoubleTaps_25() const { return ___inSingleDoubleTaps_25; }
	inline bool* get_address_of_inSingleDoubleTaps_25() { return &___inSingleDoubleTaps_25; }
	inline void set_inSingleDoubleTaps_25(bool value)
	{
		___inSingleDoubleTaps_25 = value;
	}

	inline static int32_t get_offset_of_tapCurentTime_26() { return static_cast<int32_t>(offsetof(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B, ___tapCurentTime_26)); }
	inline float get_tapCurentTime_26() const { return ___tapCurentTime_26; }
	inline float* get_address_of_tapCurentTime_26() { return &___tapCurentTime_26; }
	inline void set_tapCurentTime_26(float value)
	{
		___tapCurentTime_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOFINGERGESTURE_TE01A70108992AD19FFB0D7F47A418873C9269A0B_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ONMESSAGEHANDLEDELEGATE_TE979D029A260114F32638C86CA46E6A7D355AA4D_H
#define ONMESSAGEHANDLEDELEGATE_TE979D029A260114F32638C86CA46E6A7D355AA4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Message_OnMessageHandleDelegate
struct  OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMESSAGEHANDLEDELEGATE_TE979D029A260114F32638C86CA46E6A7D355AA4D_H
#ifndef DOOZYPATH_T20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_H
#define DOOZYPATH_T20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.DoozyPath
struct  DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

struct DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields
{
public:
	// System.String Doozy.Engine.Utils.DoozyPath::DOOZY_PATH
	String_t* ___DOOZY_PATH_41;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_PATH
	String_t* ___EDITOR_PATH_42;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_PATH
	String_t* ___ENGINE_PATH_43;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_FONTS_PATH
	String_t* ___EDITOR_FONTS_PATH_44;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_GUI_PATH
	String_t* ___EDITOR_GUI_PATH_45;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_IMAGES_PATH
	String_t* ___EDITOR_IMAGES_PATH_46;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_INTERNAL_PATH
	String_t* ___EDITOR_INTERNAL_PATH_47;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_SETTINGS_PATH
	String_t* ___EDITOR_SETTINGS_PATH_48;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_SKINS_PATH
	String_t* ___EDITOR_SKINS_PATH_49;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_NODY_PATH
	String_t* ___EDITOR_NODY_PATH_50;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_NODY_IMAGES_PATH
	String_t* ___EDITOR_NODY_IMAGES_PATH_51;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_NODY_SKINS_PATH
	String_t* ___EDITOR_NODY_SKINS_PATH_52;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_NODY_SETTINGS_PATH
	String_t* ___EDITOR_NODY_SETTINGS_PATH_53;
	// System.String Doozy.Engine.Utils.DoozyPath::EDITOR_NODY_UTILS_PATH
	String_t* ___EDITOR_NODY_UTILS_PATH_54;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_NODY_PATH
	String_t* ___ENGINE_NODY_PATH_55;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_NODY_RESOURCES_PATH
	String_t* ___ENGINE_NODY_RESOURCES_PATH_56;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_SOUNDY_PATH
	String_t* ___ENGINE_SOUNDY_PATH_57;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_SOUNDY_RESOURCES_PATH
	String_t* ___ENGINE_SOUNDY_RESOURCES_PATH_58;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_TOUCHY_PATH
	String_t* ___ENGINE_TOUCHY_PATH_59;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_TOUCHY_RESOURCES_PATH
	String_t* ___ENGINE_TOUCHY_RESOURCES_PATH_60;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_PATH
	String_t* ___ENGINE_RESOURCES_PATH_61;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_PATH
	String_t* ___ENGINE_RESOURCES_DATA_PATH_62;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_SOUNDY_PATH
	String_t* ___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_UIBUTTON_PATH
	String_t* ___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_UICANVAS_PATH
	String_t* ___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_UIDRAWER_PATH
	String_t* ___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_UIPOPUP_PATH
	String_t* ___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_RESOURCES_DATA_UIVIEW_PATH
	String_t* ___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_UI_PATH
	String_t* ___ENGINE_UI_PATH_69;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_UI_RESOURCES_PATH
	String_t* ___ENGINE_UI_RESOURCES_PATH_70;
	// System.String Doozy.Engine.Utils.DoozyPath::UIANIMATIONS_RESOURCES_PATH
	String_t* ___UIANIMATIONS_RESOURCES_PATH_71;
	// System.String Doozy.Engine.Utils.DoozyPath::HIDE_UIANIMATIONS_RESOURCES_PATH
	String_t* ___HIDE_UIANIMATIONS_RESOURCES_PATH_72;
	// System.String Doozy.Engine.Utils.DoozyPath::LOOP_UIANIMATIONS_RESOURCES_PATH
	String_t* ___LOOP_UIANIMATIONS_RESOURCES_PATH_73;
	// System.String Doozy.Engine.Utils.DoozyPath::PUNCH_UIANIMATIONS_RESOURCES_PATH
	String_t* ___PUNCH_UIANIMATIONS_RESOURCES_PATH_74;
	// System.String Doozy.Engine.Utils.DoozyPath::SHOW_UIANIMATIONS_RESOURCES_PATH
	String_t* ___SHOW_UIANIMATIONS_RESOURCES_PATH_75;
	// System.String Doozy.Engine.Utils.DoozyPath::STATE_UIANIMATIONS_RESOURCES_PATH
	String_t* ___STATE_UIANIMATIONS_RESOURCES_PATH_76;
	// System.String Doozy.Engine.Utils.DoozyPath::UIBUTTON_PATH
	String_t* ___UIBUTTON_PATH_77;
	// System.String Doozy.Engine.Utils.DoozyPath::UIBUTTON_RESOURCES_PATH
	String_t* ___UIBUTTON_RESOURCES_PATH_78;
	// System.String Doozy.Engine.Utils.DoozyPath::UICANVAS_PATH
	String_t* ___UICANVAS_PATH_79;
	// System.String Doozy.Engine.Utils.DoozyPath::UICANVAS_RESOURCES_PATH
	String_t* ___UICANVAS_RESOURCES_PATH_80;
	// System.String Doozy.Engine.Utils.DoozyPath::UIDRAWER_PATH
	String_t* ___UIDRAWER_PATH_81;
	// System.String Doozy.Engine.Utils.DoozyPath::UIDRAWER_RESOURCES_PATH
	String_t* ___UIDRAWER_RESOURCES_PATH_82;
	// System.String Doozy.Engine.Utils.DoozyPath::UIPOPUP_PATH
	String_t* ___UIPOPUP_PATH_83;
	// System.String Doozy.Engine.Utils.DoozyPath::UIPOPUP_RESOURCES_PATH
	String_t* ___UIPOPUP_RESOURCES_PATH_84;
	// System.String Doozy.Engine.Utils.DoozyPath::UIVIEW_PATH
	String_t* ___UIVIEW_PATH_85;
	// System.String Doozy.Engine.Utils.DoozyPath::UIVIEW_RESOURCES_PATH
	String_t* ___UIVIEW_RESOURCES_PATH_86;
	// System.String Doozy.Engine.Utils.DoozyPath::UITOGGLE_PATH
	String_t* ___UITOGGLE_PATH_87;
	// System.String Doozy.Engine.Utils.DoozyPath::UITOGGLE_RESOURCES_PATH
	String_t* ___UITOGGLE_RESOURCES_PATH_88;
	// System.String Doozy.Engine.Utils.DoozyPath::ENGINE_UTILS_PATH
	String_t* ___ENGINE_UTILS_PATH_89;
	// System.String Doozy.Engine.Utils.DoozyPath::s_basePath
	String_t* ___s_basePath_90;

public:
	inline static int32_t get_offset_of_DOOZY_PATH_41() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___DOOZY_PATH_41)); }
	inline String_t* get_DOOZY_PATH_41() const { return ___DOOZY_PATH_41; }
	inline String_t** get_address_of_DOOZY_PATH_41() { return &___DOOZY_PATH_41; }
	inline void set_DOOZY_PATH_41(String_t* value)
	{
		___DOOZY_PATH_41 = value;
		Il2CppCodeGenWriteBarrier((&___DOOZY_PATH_41), value);
	}

	inline static int32_t get_offset_of_EDITOR_PATH_42() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_PATH_42)); }
	inline String_t* get_EDITOR_PATH_42() const { return ___EDITOR_PATH_42; }
	inline String_t** get_address_of_EDITOR_PATH_42() { return &___EDITOR_PATH_42; }
	inline void set_EDITOR_PATH_42(String_t* value)
	{
		___EDITOR_PATH_42 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_PATH_42), value);
	}

	inline static int32_t get_offset_of_ENGINE_PATH_43() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_PATH_43)); }
	inline String_t* get_ENGINE_PATH_43() const { return ___ENGINE_PATH_43; }
	inline String_t** get_address_of_ENGINE_PATH_43() { return &___ENGINE_PATH_43; }
	inline void set_ENGINE_PATH_43(String_t* value)
	{
		___ENGINE_PATH_43 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_PATH_43), value);
	}

	inline static int32_t get_offset_of_EDITOR_FONTS_PATH_44() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_FONTS_PATH_44)); }
	inline String_t* get_EDITOR_FONTS_PATH_44() const { return ___EDITOR_FONTS_PATH_44; }
	inline String_t** get_address_of_EDITOR_FONTS_PATH_44() { return &___EDITOR_FONTS_PATH_44; }
	inline void set_EDITOR_FONTS_PATH_44(String_t* value)
	{
		___EDITOR_FONTS_PATH_44 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_FONTS_PATH_44), value);
	}

	inline static int32_t get_offset_of_EDITOR_GUI_PATH_45() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_GUI_PATH_45)); }
	inline String_t* get_EDITOR_GUI_PATH_45() const { return ___EDITOR_GUI_PATH_45; }
	inline String_t** get_address_of_EDITOR_GUI_PATH_45() { return &___EDITOR_GUI_PATH_45; }
	inline void set_EDITOR_GUI_PATH_45(String_t* value)
	{
		___EDITOR_GUI_PATH_45 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_GUI_PATH_45), value);
	}

	inline static int32_t get_offset_of_EDITOR_IMAGES_PATH_46() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_IMAGES_PATH_46)); }
	inline String_t* get_EDITOR_IMAGES_PATH_46() const { return ___EDITOR_IMAGES_PATH_46; }
	inline String_t** get_address_of_EDITOR_IMAGES_PATH_46() { return &___EDITOR_IMAGES_PATH_46; }
	inline void set_EDITOR_IMAGES_PATH_46(String_t* value)
	{
		___EDITOR_IMAGES_PATH_46 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_IMAGES_PATH_46), value);
	}

	inline static int32_t get_offset_of_EDITOR_INTERNAL_PATH_47() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_INTERNAL_PATH_47)); }
	inline String_t* get_EDITOR_INTERNAL_PATH_47() const { return ___EDITOR_INTERNAL_PATH_47; }
	inline String_t** get_address_of_EDITOR_INTERNAL_PATH_47() { return &___EDITOR_INTERNAL_PATH_47; }
	inline void set_EDITOR_INTERNAL_PATH_47(String_t* value)
	{
		___EDITOR_INTERNAL_PATH_47 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_INTERNAL_PATH_47), value);
	}

	inline static int32_t get_offset_of_EDITOR_SETTINGS_PATH_48() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_SETTINGS_PATH_48)); }
	inline String_t* get_EDITOR_SETTINGS_PATH_48() const { return ___EDITOR_SETTINGS_PATH_48; }
	inline String_t** get_address_of_EDITOR_SETTINGS_PATH_48() { return &___EDITOR_SETTINGS_PATH_48; }
	inline void set_EDITOR_SETTINGS_PATH_48(String_t* value)
	{
		___EDITOR_SETTINGS_PATH_48 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_SETTINGS_PATH_48), value);
	}

	inline static int32_t get_offset_of_EDITOR_SKINS_PATH_49() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_SKINS_PATH_49)); }
	inline String_t* get_EDITOR_SKINS_PATH_49() const { return ___EDITOR_SKINS_PATH_49; }
	inline String_t** get_address_of_EDITOR_SKINS_PATH_49() { return &___EDITOR_SKINS_PATH_49; }
	inline void set_EDITOR_SKINS_PATH_49(String_t* value)
	{
		___EDITOR_SKINS_PATH_49 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_SKINS_PATH_49), value);
	}

	inline static int32_t get_offset_of_EDITOR_NODY_PATH_50() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_NODY_PATH_50)); }
	inline String_t* get_EDITOR_NODY_PATH_50() const { return ___EDITOR_NODY_PATH_50; }
	inline String_t** get_address_of_EDITOR_NODY_PATH_50() { return &___EDITOR_NODY_PATH_50; }
	inline void set_EDITOR_NODY_PATH_50(String_t* value)
	{
		___EDITOR_NODY_PATH_50 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_NODY_PATH_50), value);
	}

	inline static int32_t get_offset_of_EDITOR_NODY_IMAGES_PATH_51() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_NODY_IMAGES_PATH_51)); }
	inline String_t* get_EDITOR_NODY_IMAGES_PATH_51() const { return ___EDITOR_NODY_IMAGES_PATH_51; }
	inline String_t** get_address_of_EDITOR_NODY_IMAGES_PATH_51() { return &___EDITOR_NODY_IMAGES_PATH_51; }
	inline void set_EDITOR_NODY_IMAGES_PATH_51(String_t* value)
	{
		___EDITOR_NODY_IMAGES_PATH_51 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_NODY_IMAGES_PATH_51), value);
	}

	inline static int32_t get_offset_of_EDITOR_NODY_SKINS_PATH_52() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_NODY_SKINS_PATH_52)); }
	inline String_t* get_EDITOR_NODY_SKINS_PATH_52() const { return ___EDITOR_NODY_SKINS_PATH_52; }
	inline String_t** get_address_of_EDITOR_NODY_SKINS_PATH_52() { return &___EDITOR_NODY_SKINS_PATH_52; }
	inline void set_EDITOR_NODY_SKINS_PATH_52(String_t* value)
	{
		___EDITOR_NODY_SKINS_PATH_52 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_NODY_SKINS_PATH_52), value);
	}

	inline static int32_t get_offset_of_EDITOR_NODY_SETTINGS_PATH_53() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_NODY_SETTINGS_PATH_53)); }
	inline String_t* get_EDITOR_NODY_SETTINGS_PATH_53() const { return ___EDITOR_NODY_SETTINGS_PATH_53; }
	inline String_t** get_address_of_EDITOR_NODY_SETTINGS_PATH_53() { return &___EDITOR_NODY_SETTINGS_PATH_53; }
	inline void set_EDITOR_NODY_SETTINGS_PATH_53(String_t* value)
	{
		___EDITOR_NODY_SETTINGS_PATH_53 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_NODY_SETTINGS_PATH_53), value);
	}

	inline static int32_t get_offset_of_EDITOR_NODY_UTILS_PATH_54() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___EDITOR_NODY_UTILS_PATH_54)); }
	inline String_t* get_EDITOR_NODY_UTILS_PATH_54() const { return ___EDITOR_NODY_UTILS_PATH_54; }
	inline String_t** get_address_of_EDITOR_NODY_UTILS_PATH_54() { return &___EDITOR_NODY_UTILS_PATH_54; }
	inline void set_EDITOR_NODY_UTILS_PATH_54(String_t* value)
	{
		___EDITOR_NODY_UTILS_PATH_54 = value;
		Il2CppCodeGenWriteBarrier((&___EDITOR_NODY_UTILS_PATH_54), value);
	}

	inline static int32_t get_offset_of_ENGINE_NODY_PATH_55() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_NODY_PATH_55)); }
	inline String_t* get_ENGINE_NODY_PATH_55() const { return ___ENGINE_NODY_PATH_55; }
	inline String_t** get_address_of_ENGINE_NODY_PATH_55() { return &___ENGINE_NODY_PATH_55; }
	inline void set_ENGINE_NODY_PATH_55(String_t* value)
	{
		___ENGINE_NODY_PATH_55 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_NODY_PATH_55), value);
	}

	inline static int32_t get_offset_of_ENGINE_NODY_RESOURCES_PATH_56() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_NODY_RESOURCES_PATH_56)); }
	inline String_t* get_ENGINE_NODY_RESOURCES_PATH_56() const { return ___ENGINE_NODY_RESOURCES_PATH_56; }
	inline String_t** get_address_of_ENGINE_NODY_RESOURCES_PATH_56() { return &___ENGINE_NODY_RESOURCES_PATH_56; }
	inline void set_ENGINE_NODY_RESOURCES_PATH_56(String_t* value)
	{
		___ENGINE_NODY_RESOURCES_PATH_56 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_NODY_RESOURCES_PATH_56), value);
	}

	inline static int32_t get_offset_of_ENGINE_SOUNDY_PATH_57() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_SOUNDY_PATH_57)); }
	inline String_t* get_ENGINE_SOUNDY_PATH_57() const { return ___ENGINE_SOUNDY_PATH_57; }
	inline String_t** get_address_of_ENGINE_SOUNDY_PATH_57() { return &___ENGINE_SOUNDY_PATH_57; }
	inline void set_ENGINE_SOUNDY_PATH_57(String_t* value)
	{
		___ENGINE_SOUNDY_PATH_57 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_SOUNDY_PATH_57), value);
	}

	inline static int32_t get_offset_of_ENGINE_SOUNDY_RESOURCES_PATH_58() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_SOUNDY_RESOURCES_PATH_58)); }
	inline String_t* get_ENGINE_SOUNDY_RESOURCES_PATH_58() const { return ___ENGINE_SOUNDY_RESOURCES_PATH_58; }
	inline String_t** get_address_of_ENGINE_SOUNDY_RESOURCES_PATH_58() { return &___ENGINE_SOUNDY_RESOURCES_PATH_58; }
	inline void set_ENGINE_SOUNDY_RESOURCES_PATH_58(String_t* value)
	{
		___ENGINE_SOUNDY_RESOURCES_PATH_58 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_SOUNDY_RESOURCES_PATH_58), value);
	}

	inline static int32_t get_offset_of_ENGINE_TOUCHY_PATH_59() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_TOUCHY_PATH_59)); }
	inline String_t* get_ENGINE_TOUCHY_PATH_59() const { return ___ENGINE_TOUCHY_PATH_59; }
	inline String_t** get_address_of_ENGINE_TOUCHY_PATH_59() { return &___ENGINE_TOUCHY_PATH_59; }
	inline void set_ENGINE_TOUCHY_PATH_59(String_t* value)
	{
		___ENGINE_TOUCHY_PATH_59 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_TOUCHY_PATH_59), value);
	}

	inline static int32_t get_offset_of_ENGINE_TOUCHY_RESOURCES_PATH_60() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_TOUCHY_RESOURCES_PATH_60)); }
	inline String_t* get_ENGINE_TOUCHY_RESOURCES_PATH_60() const { return ___ENGINE_TOUCHY_RESOURCES_PATH_60; }
	inline String_t** get_address_of_ENGINE_TOUCHY_RESOURCES_PATH_60() { return &___ENGINE_TOUCHY_RESOURCES_PATH_60; }
	inline void set_ENGINE_TOUCHY_RESOURCES_PATH_60(String_t* value)
	{
		___ENGINE_TOUCHY_RESOURCES_PATH_60 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_TOUCHY_RESOURCES_PATH_60), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_PATH_61() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_PATH_61)); }
	inline String_t* get_ENGINE_RESOURCES_PATH_61() const { return ___ENGINE_RESOURCES_PATH_61; }
	inline String_t** get_address_of_ENGINE_RESOURCES_PATH_61() { return &___ENGINE_RESOURCES_PATH_61; }
	inline void set_ENGINE_RESOURCES_PATH_61(String_t* value)
	{
		___ENGINE_RESOURCES_PATH_61 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_PATH_61), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_PATH_62() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_PATH_62)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_PATH_62() const { return ___ENGINE_RESOURCES_DATA_PATH_62; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_PATH_62() { return &___ENGINE_RESOURCES_DATA_PATH_62; }
	inline void set_ENGINE_RESOURCES_DATA_PATH_62(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_PATH_62 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_PATH_62), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_SOUNDY_PATH_63() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_SOUNDY_PATH_63() const { return ___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_SOUNDY_PATH_63() { return &___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63; }
	inline void set_ENGINE_RESOURCES_DATA_SOUNDY_PATH_63(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_SOUNDY_PATH_63), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64() const { return ___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64() { return &___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64; }
	inline void set_ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_UICANVAS_PATH_65() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_UICANVAS_PATH_65() const { return ___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_UICANVAS_PATH_65() { return &___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65; }
	inline void set_ENGINE_RESOURCES_DATA_UICANVAS_PATH_65(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_UICANVAS_PATH_65), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66() const { return ___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66() { return &___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66; }
	inline void set_ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67() const { return ___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67() { return &___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67; }
	inline void set_ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67), value);
	}

	inline static int32_t get_offset_of_ENGINE_RESOURCES_DATA_UIVIEW_PATH_68() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68)); }
	inline String_t* get_ENGINE_RESOURCES_DATA_UIVIEW_PATH_68() const { return ___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68; }
	inline String_t** get_address_of_ENGINE_RESOURCES_DATA_UIVIEW_PATH_68() { return &___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68; }
	inline void set_ENGINE_RESOURCES_DATA_UIVIEW_PATH_68(String_t* value)
	{
		___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_RESOURCES_DATA_UIVIEW_PATH_68), value);
	}

	inline static int32_t get_offset_of_ENGINE_UI_PATH_69() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_UI_PATH_69)); }
	inline String_t* get_ENGINE_UI_PATH_69() const { return ___ENGINE_UI_PATH_69; }
	inline String_t** get_address_of_ENGINE_UI_PATH_69() { return &___ENGINE_UI_PATH_69; }
	inline void set_ENGINE_UI_PATH_69(String_t* value)
	{
		___ENGINE_UI_PATH_69 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_UI_PATH_69), value);
	}

	inline static int32_t get_offset_of_ENGINE_UI_RESOURCES_PATH_70() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_UI_RESOURCES_PATH_70)); }
	inline String_t* get_ENGINE_UI_RESOURCES_PATH_70() const { return ___ENGINE_UI_RESOURCES_PATH_70; }
	inline String_t** get_address_of_ENGINE_UI_RESOURCES_PATH_70() { return &___ENGINE_UI_RESOURCES_PATH_70; }
	inline void set_ENGINE_UI_RESOURCES_PATH_70(String_t* value)
	{
		___ENGINE_UI_RESOURCES_PATH_70 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_UI_RESOURCES_PATH_70), value);
	}

	inline static int32_t get_offset_of_UIANIMATIONS_RESOURCES_PATH_71() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIANIMATIONS_RESOURCES_PATH_71)); }
	inline String_t* get_UIANIMATIONS_RESOURCES_PATH_71() const { return ___UIANIMATIONS_RESOURCES_PATH_71; }
	inline String_t** get_address_of_UIANIMATIONS_RESOURCES_PATH_71() { return &___UIANIMATIONS_RESOURCES_PATH_71; }
	inline void set_UIANIMATIONS_RESOURCES_PATH_71(String_t* value)
	{
		___UIANIMATIONS_RESOURCES_PATH_71 = value;
		Il2CppCodeGenWriteBarrier((&___UIANIMATIONS_RESOURCES_PATH_71), value);
	}

	inline static int32_t get_offset_of_HIDE_UIANIMATIONS_RESOURCES_PATH_72() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___HIDE_UIANIMATIONS_RESOURCES_PATH_72)); }
	inline String_t* get_HIDE_UIANIMATIONS_RESOURCES_PATH_72() const { return ___HIDE_UIANIMATIONS_RESOURCES_PATH_72; }
	inline String_t** get_address_of_HIDE_UIANIMATIONS_RESOURCES_PATH_72() { return &___HIDE_UIANIMATIONS_RESOURCES_PATH_72; }
	inline void set_HIDE_UIANIMATIONS_RESOURCES_PATH_72(String_t* value)
	{
		___HIDE_UIANIMATIONS_RESOURCES_PATH_72 = value;
		Il2CppCodeGenWriteBarrier((&___HIDE_UIANIMATIONS_RESOURCES_PATH_72), value);
	}

	inline static int32_t get_offset_of_LOOP_UIANIMATIONS_RESOURCES_PATH_73() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___LOOP_UIANIMATIONS_RESOURCES_PATH_73)); }
	inline String_t* get_LOOP_UIANIMATIONS_RESOURCES_PATH_73() const { return ___LOOP_UIANIMATIONS_RESOURCES_PATH_73; }
	inline String_t** get_address_of_LOOP_UIANIMATIONS_RESOURCES_PATH_73() { return &___LOOP_UIANIMATIONS_RESOURCES_PATH_73; }
	inline void set_LOOP_UIANIMATIONS_RESOURCES_PATH_73(String_t* value)
	{
		___LOOP_UIANIMATIONS_RESOURCES_PATH_73 = value;
		Il2CppCodeGenWriteBarrier((&___LOOP_UIANIMATIONS_RESOURCES_PATH_73), value);
	}

	inline static int32_t get_offset_of_PUNCH_UIANIMATIONS_RESOURCES_PATH_74() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___PUNCH_UIANIMATIONS_RESOURCES_PATH_74)); }
	inline String_t* get_PUNCH_UIANIMATIONS_RESOURCES_PATH_74() const { return ___PUNCH_UIANIMATIONS_RESOURCES_PATH_74; }
	inline String_t** get_address_of_PUNCH_UIANIMATIONS_RESOURCES_PATH_74() { return &___PUNCH_UIANIMATIONS_RESOURCES_PATH_74; }
	inline void set_PUNCH_UIANIMATIONS_RESOURCES_PATH_74(String_t* value)
	{
		___PUNCH_UIANIMATIONS_RESOURCES_PATH_74 = value;
		Il2CppCodeGenWriteBarrier((&___PUNCH_UIANIMATIONS_RESOURCES_PATH_74), value);
	}

	inline static int32_t get_offset_of_SHOW_UIANIMATIONS_RESOURCES_PATH_75() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___SHOW_UIANIMATIONS_RESOURCES_PATH_75)); }
	inline String_t* get_SHOW_UIANIMATIONS_RESOURCES_PATH_75() const { return ___SHOW_UIANIMATIONS_RESOURCES_PATH_75; }
	inline String_t** get_address_of_SHOW_UIANIMATIONS_RESOURCES_PATH_75() { return &___SHOW_UIANIMATIONS_RESOURCES_PATH_75; }
	inline void set_SHOW_UIANIMATIONS_RESOURCES_PATH_75(String_t* value)
	{
		___SHOW_UIANIMATIONS_RESOURCES_PATH_75 = value;
		Il2CppCodeGenWriteBarrier((&___SHOW_UIANIMATIONS_RESOURCES_PATH_75), value);
	}

	inline static int32_t get_offset_of_STATE_UIANIMATIONS_RESOURCES_PATH_76() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___STATE_UIANIMATIONS_RESOURCES_PATH_76)); }
	inline String_t* get_STATE_UIANIMATIONS_RESOURCES_PATH_76() const { return ___STATE_UIANIMATIONS_RESOURCES_PATH_76; }
	inline String_t** get_address_of_STATE_UIANIMATIONS_RESOURCES_PATH_76() { return &___STATE_UIANIMATIONS_RESOURCES_PATH_76; }
	inline void set_STATE_UIANIMATIONS_RESOURCES_PATH_76(String_t* value)
	{
		___STATE_UIANIMATIONS_RESOURCES_PATH_76 = value;
		Il2CppCodeGenWriteBarrier((&___STATE_UIANIMATIONS_RESOURCES_PATH_76), value);
	}

	inline static int32_t get_offset_of_UIBUTTON_PATH_77() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIBUTTON_PATH_77)); }
	inline String_t* get_UIBUTTON_PATH_77() const { return ___UIBUTTON_PATH_77; }
	inline String_t** get_address_of_UIBUTTON_PATH_77() { return &___UIBUTTON_PATH_77; }
	inline void set_UIBUTTON_PATH_77(String_t* value)
	{
		___UIBUTTON_PATH_77 = value;
		Il2CppCodeGenWriteBarrier((&___UIBUTTON_PATH_77), value);
	}

	inline static int32_t get_offset_of_UIBUTTON_RESOURCES_PATH_78() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIBUTTON_RESOURCES_PATH_78)); }
	inline String_t* get_UIBUTTON_RESOURCES_PATH_78() const { return ___UIBUTTON_RESOURCES_PATH_78; }
	inline String_t** get_address_of_UIBUTTON_RESOURCES_PATH_78() { return &___UIBUTTON_RESOURCES_PATH_78; }
	inline void set_UIBUTTON_RESOURCES_PATH_78(String_t* value)
	{
		___UIBUTTON_RESOURCES_PATH_78 = value;
		Il2CppCodeGenWriteBarrier((&___UIBUTTON_RESOURCES_PATH_78), value);
	}

	inline static int32_t get_offset_of_UICANVAS_PATH_79() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UICANVAS_PATH_79)); }
	inline String_t* get_UICANVAS_PATH_79() const { return ___UICANVAS_PATH_79; }
	inline String_t** get_address_of_UICANVAS_PATH_79() { return &___UICANVAS_PATH_79; }
	inline void set_UICANVAS_PATH_79(String_t* value)
	{
		___UICANVAS_PATH_79 = value;
		Il2CppCodeGenWriteBarrier((&___UICANVAS_PATH_79), value);
	}

	inline static int32_t get_offset_of_UICANVAS_RESOURCES_PATH_80() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UICANVAS_RESOURCES_PATH_80)); }
	inline String_t* get_UICANVAS_RESOURCES_PATH_80() const { return ___UICANVAS_RESOURCES_PATH_80; }
	inline String_t** get_address_of_UICANVAS_RESOURCES_PATH_80() { return &___UICANVAS_RESOURCES_PATH_80; }
	inline void set_UICANVAS_RESOURCES_PATH_80(String_t* value)
	{
		___UICANVAS_RESOURCES_PATH_80 = value;
		Il2CppCodeGenWriteBarrier((&___UICANVAS_RESOURCES_PATH_80), value);
	}

	inline static int32_t get_offset_of_UIDRAWER_PATH_81() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIDRAWER_PATH_81)); }
	inline String_t* get_UIDRAWER_PATH_81() const { return ___UIDRAWER_PATH_81; }
	inline String_t** get_address_of_UIDRAWER_PATH_81() { return &___UIDRAWER_PATH_81; }
	inline void set_UIDRAWER_PATH_81(String_t* value)
	{
		___UIDRAWER_PATH_81 = value;
		Il2CppCodeGenWriteBarrier((&___UIDRAWER_PATH_81), value);
	}

	inline static int32_t get_offset_of_UIDRAWER_RESOURCES_PATH_82() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIDRAWER_RESOURCES_PATH_82)); }
	inline String_t* get_UIDRAWER_RESOURCES_PATH_82() const { return ___UIDRAWER_RESOURCES_PATH_82; }
	inline String_t** get_address_of_UIDRAWER_RESOURCES_PATH_82() { return &___UIDRAWER_RESOURCES_PATH_82; }
	inline void set_UIDRAWER_RESOURCES_PATH_82(String_t* value)
	{
		___UIDRAWER_RESOURCES_PATH_82 = value;
		Il2CppCodeGenWriteBarrier((&___UIDRAWER_RESOURCES_PATH_82), value);
	}

	inline static int32_t get_offset_of_UIPOPUP_PATH_83() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIPOPUP_PATH_83)); }
	inline String_t* get_UIPOPUP_PATH_83() const { return ___UIPOPUP_PATH_83; }
	inline String_t** get_address_of_UIPOPUP_PATH_83() { return &___UIPOPUP_PATH_83; }
	inline void set_UIPOPUP_PATH_83(String_t* value)
	{
		___UIPOPUP_PATH_83 = value;
		Il2CppCodeGenWriteBarrier((&___UIPOPUP_PATH_83), value);
	}

	inline static int32_t get_offset_of_UIPOPUP_RESOURCES_PATH_84() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIPOPUP_RESOURCES_PATH_84)); }
	inline String_t* get_UIPOPUP_RESOURCES_PATH_84() const { return ___UIPOPUP_RESOURCES_PATH_84; }
	inline String_t** get_address_of_UIPOPUP_RESOURCES_PATH_84() { return &___UIPOPUP_RESOURCES_PATH_84; }
	inline void set_UIPOPUP_RESOURCES_PATH_84(String_t* value)
	{
		___UIPOPUP_RESOURCES_PATH_84 = value;
		Il2CppCodeGenWriteBarrier((&___UIPOPUP_RESOURCES_PATH_84), value);
	}

	inline static int32_t get_offset_of_UIVIEW_PATH_85() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIVIEW_PATH_85)); }
	inline String_t* get_UIVIEW_PATH_85() const { return ___UIVIEW_PATH_85; }
	inline String_t** get_address_of_UIVIEW_PATH_85() { return &___UIVIEW_PATH_85; }
	inline void set_UIVIEW_PATH_85(String_t* value)
	{
		___UIVIEW_PATH_85 = value;
		Il2CppCodeGenWriteBarrier((&___UIVIEW_PATH_85), value);
	}

	inline static int32_t get_offset_of_UIVIEW_RESOURCES_PATH_86() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UIVIEW_RESOURCES_PATH_86)); }
	inline String_t* get_UIVIEW_RESOURCES_PATH_86() const { return ___UIVIEW_RESOURCES_PATH_86; }
	inline String_t** get_address_of_UIVIEW_RESOURCES_PATH_86() { return &___UIVIEW_RESOURCES_PATH_86; }
	inline void set_UIVIEW_RESOURCES_PATH_86(String_t* value)
	{
		___UIVIEW_RESOURCES_PATH_86 = value;
		Il2CppCodeGenWriteBarrier((&___UIVIEW_RESOURCES_PATH_86), value);
	}

	inline static int32_t get_offset_of_UITOGGLE_PATH_87() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UITOGGLE_PATH_87)); }
	inline String_t* get_UITOGGLE_PATH_87() const { return ___UITOGGLE_PATH_87; }
	inline String_t** get_address_of_UITOGGLE_PATH_87() { return &___UITOGGLE_PATH_87; }
	inline void set_UITOGGLE_PATH_87(String_t* value)
	{
		___UITOGGLE_PATH_87 = value;
		Il2CppCodeGenWriteBarrier((&___UITOGGLE_PATH_87), value);
	}

	inline static int32_t get_offset_of_UITOGGLE_RESOURCES_PATH_88() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___UITOGGLE_RESOURCES_PATH_88)); }
	inline String_t* get_UITOGGLE_RESOURCES_PATH_88() const { return ___UITOGGLE_RESOURCES_PATH_88; }
	inline String_t** get_address_of_UITOGGLE_RESOURCES_PATH_88() { return &___UITOGGLE_RESOURCES_PATH_88; }
	inline void set_UITOGGLE_RESOURCES_PATH_88(String_t* value)
	{
		___UITOGGLE_RESOURCES_PATH_88 = value;
		Il2CppCodeGenWriteBarrier((&___UITOGGLE_RESOURCES_PATH_88), value);
	}

	inline static int32_t get_offset_of_ENGINE_UTILS_PATH_89() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___ENGINE_UTILS_PATH_89)); }
	inline String_t* get_ENGINE_UTILS_PATH_89() const { return ___ENGINE_UTILS_PATH_89; }
	inline String_t** get_address_of_ENGINE_UTILS_PATH_89() { return &___ENGINE_UTILS_PATH_89; }
	inline void set_ENGINE_UTILS_PATH_89(String_t* value)
	{
		___ENGINE_UTILS_PATH_89 = value;
		Il2CppCodeGenWriteBarrier((&___ENGINE_UTILS_PATH_89), value);
	}

	inline static int32_t get_offset_of_s_basePath_90() { return static_cast<int32_t>(offsetof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields, ___s_basePath_90)); }
	inline String_t* get_s_basePath_90() const { return ___s_basePath_90; }
	inline String_t** get_address_of_s_basePath_90() { return &___s_basePath_90; }
	inline void set_s_basePath_90(String_t* value)
	{
		___s_basePath_90 = value;
		Il2CppCodeGenWriteBarrier((&___s_basePath_90), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOOZYPATH_T20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_H
#ifndef LANGUAGEPACK_T279C78E79D8922AB9E588FDD820BA0E125242C30_H
#define LANGUAGEPACK_T279C78E79D8922AB9E588FDD820BA0E125242C30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.LanguagePack
struct  LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

struct LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30_StaticFields
{
public:
	// Doozy.Engine.Language Doozy.Engine.Utils.LanguagePack::s_currentLanguage
	int32_t ___s_currentLanguage_6;

public:
	inline static int32_t get_offset_of_s_currentLanguage_6() { return static_cast<int32_t>(offsetof(LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30_StaticFields, ___s_currentLanguage_6)); }
	inline int32_t get_s_currentLanguage_6() const { return ___s_currentLanguage_6; }
	inline int32_t* get_address_of_s_currentLanguage_6() { return &___s_currentLanguage_6; }
	inline void set_s_currentLanguage_6(int32_t value)
	{
		___s_currentLanguage_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEPACK_T279C78E79D8922AB9E588FDD820BA0E125242C30_H
#ifndef CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#define CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler
struct  Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCEL2FINGERSHANDLER_T7CB2A8C009515F4B09D7B8F7F16489F486E28FBD_H
#ifndef DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#define DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler
struct  DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAP2FINGERSHANDLER_T17FE5590609D32A98F6E325E57959AD2630989A8_H
#ifndef DOUBLETAPHANDLER_T3FDE1E5C632D85BB111A67CFF698B3519DC7AD11_H
#define DOUBLETAPHANDLER_T3FDE1E5C632D85BB111A67CFF698B3519DC7AD11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler
struct  DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAPHANDLER_T3FDE1E5C632D85BB111A67CFF698B3519DC7AD11_H
#ifndef DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#define DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler
struct  Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAG2FINGERSHANDLER_TD0EA90C5A55FB1447BDDC17A218E381914486AAA_H
#ifndef DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#define DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler
struct  DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEND2FINGERSHANDLER_TAECC166E35C4D391CE8638C9F7A097C7095DF83A_H
#ifndef DRAGENDHANDLER_T2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF_H
#define DRAGENDHANDLER_T2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler
struct  DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGENDHANDLER_T2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF_H
#ifndef DRAGHANDLER_TFE30182A454D9F6BF14FB28EE9D72902AEE7A801_H
#define DRAGHANDLER_TFE30182A454D9F6BF14FB28EE9D72902AEE7A801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragHandler
struct  DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGHANDLER_TFE30182A454D9F6BF14FB28EE9D72902AEE7A801_H
#ifndef DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#define DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler
struct  DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTART2FINGERSHANDLER_T2619E363975FAC51655EE9C799F91228F759992B_H
#ifndef DRAGSTARTHANDLER_T59144B31D5B9DECED9817E48885D6F47ABC1C940_H
#define DRAGSTARTHANDLER_T59144B31D5B9DECED9817E48885D6F47ABC1C940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler
struct  DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTARTHANDLER_T59144B31D5B9DECED9817E48885D6F47ABC1C940_H
#ifndef EASYTOUCHISREADYHANDLER_T9BA04EFD67A48EDD1484C54C36D063A9AAF47621_H
#define EASYTOUCHISREADYHANDLER_T9BA04EFD67A48EDD1484C54C36D063A9AAF47621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler
struct  EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHISREADYHANDLER_T9BA04EFD67A48EDD1484C54C36D063A9AAF47621_H
#ifndef LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#define LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler
struct  LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAP2FINGERSHANDLER_T7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6_H
#ifndef LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#define LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler
struct  LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPEND2FINGERSHANDLER_TB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8_H
#ifndef LONGTAPENDHANDLER_T437BD68FF1BA1CEF31FCA34E4F57D062840503EB_H
#define LONGTAPENDHANDLER_T437BD68FF1BA1CEF31FCA34E4F57D062840503EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler
struct  LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPENDHANDLER_T437BD68FF1BA1CEF31FCA34E4F57D062840503EB_H
#ifndef LONGTAPHANDLER_TB8340A0546B488510FF8A64074446971AD2B8F30_H
#define LONGTAPHANDLER_TB8340A0546B488510FF8A64074446971AD2B8F30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler
struct  LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPHANDLER_TB8340A0546B488510FF8A64074446971AD2B8F30_H
#ifndef LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#define LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler
struct  LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPSTART2FINGERSHANDLER_T87451224DEA4225531CE32BB25025362C9FF8779_H
#ifndef LONGTAPSTARTHANDLER_T1BF61F7E0A4FD8FA13892A857539164EB9D459E1_H
#define LONGTAPSTARTHANDLER_T1BF61F7E0A4FD8FA13892A857539164EB9D459E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler
struct  LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPSTARTHANDLER_T1BF61F7E0A4FD8FA13892A857539164EB9D459E1_H
#ifndef OVERUIELEMENTHANDLER_T90BBA9590BC6FC4763CB4904C0FDC7A1789E841F_H
#define OVERUIELEMENTHANDLER_T90BBA9590BC6FC4763CB4904C0FDC7A1789E841F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler
struct  OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERUIELEMENTHANDLER_T90BBA9590BC6FC4763CB4904C0FDC7A1789E841F_H
#ifndef PINCHENDHANDLER_T328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57_H
#define PINCHENDHANDLER_T328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler
struct  PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHENDHANDLER_T328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57_H
#ifndef PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#define PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PinchHandler
struct  PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHHANDLER_TF6C154F2E6B285725DCBCA31093A671FB63D82C4_H
#ifndef PINCHINHANDLER_TEEC531E31D0693EF3B8B5C003F29EAA49643FB29_H
#define PINCHINHANDLER_TEEC531E31D0693EF3B8B5C003F29EAA49643FB29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler
struct  PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHINHANDLER_TEEC531E31D0693EF3B8B5C003F29EAA49643FB29_H
#ifndef PINCHOUTHANDLER_TE83ACE23BADAC7A2674EDCAF2110B6E7F214321B_H
#define PINCHOUTHANDLER_TE83ACE23BADAC7A2674EDCAF2110B6E7F214321B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler
struct  PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHOUTHANDLER_TE83ACE23BADAC7A2674EDCAF2110B6E7F214321B_H
#ifndef SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#define SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler
struct  SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAP2FINGERSHANDLER_T69B9A77A3C573460C22EB6AC66BE58CFED414B30_H
#ifndef SIMPLETAPHANDLER_T19011340636873AF4B306D0E18C3B4D27AD168D3_H
#define SIMPLETAPHANDLER_T19011340636873AF4B306D0E18C3B4D27AD168D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler
struct  SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAPHANDLER_T19011340636873AF4B306D0E18C3B4D27AD168D3_H
#ifndef SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#define SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler
struct  Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPE2FINGERSHANDLER_TD078F362AFFD04689777BD0ED938378B5043C862_H
#ifndef SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#define SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler
struct  SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEEND2FINGERSHANDLER_T28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC_H
#ifndef SWIPEENDHANDLER_T51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F_H
#define SWIPEENDHANDLER_T51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler
struct  SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEENDHANDLER_T51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F_H
#ifndef SWIPEHANDLER_T27BB40235B941FBCA58EFD2A53012354ECC00CB0_H
#define SWIPEHANDLER_T27BB40235B941FBCA58EFD2A53012354ECC00CB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler
struct  SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEHANDLER_T27BB40235B941FBCA58EFD2A53012354ECC00CB0_H
#ifndef SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#define SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler
struct  SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPESTART2FINGERSHANDLER_TC1C95E21681B8F61FE6D26963E144E82E3C9F0A5_H
#ifndef SWIPESTARTHANDLER_TE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB_H
#define SWIPESTARTHANDLER_TE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler
struct  SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPESTARTHANDLER_TE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB_H
#ifndef TOUCHCANCELHANDLER_T6F03769BCF5479D8D674AF13F319913EFF995398_H
#define TOUCHCANCELHANDLER_T6F03769BCF5479D8D674AF13F319913EFF995398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler
struct  TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCANCELHANDLER_T6F03769BCF5479D8D674AF13F319913EFF995398_H
#ifndef TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#define TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler
struct  TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWN2FINGERSHANDLER_T251A5FA76A3F9423662FE35E40F058F327AF6454_H
#ifndef TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#define TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler
struct  TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWNHANDLER_T25633332BED9DC567480AE796788433ADA5A4955_H
#ifndef TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#define TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler
struct  TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTART2FINGERSHANDLER_TA11253D3A78D2C563463B595B5705A6014E786BE_H
#ifndef TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#define TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler
struct  TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTARTHANDLER_T7C4A2E33DF016E9802FE7059781725CAD91E6853_H
#ifndef TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#define TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler
struct  TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUP2FINGERSHANDLER_TCED587BF5F82C26E21B89F248E968C122C0AE1C0_H
#ifndef TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#define TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler
struct  TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUPHANDLER_TFC3606D15F5A86B81730262C45C99738033DD46E_H
#ifndef TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#define TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler
struct  TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTENDHANDLER_T96B95E104506BDB3EE3E0762FE554E3BF302B929_H
#ifndef TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#define TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_TwistHandler
struct  TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTHANDLER_T8669F951A600E6F2AD113BD25B754C4C10EC5C1D_H
#ifndef UIELEMENTTOUCHUPHANDLER_TF7CB7C9434A913316961E5665EB14357CC007FD3_H
#define UIELEMENTTOUCHUPHANDLER_TF7CB7C9434A913316961E5665EB14357CC007FD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler
struct  UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIELEMENTTOUCHUPHANDLER_TF7CB7C9434A913316961E5665EB14357CC007FD3_H
#ifndef FINGER_T1C55110621038C162D210785A1F1490A0273B205_H
#define FINGER_T1C55110621038C162D210785A1F1490A0273B205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Finger
struct  Finger_t1C55110621038C162D210785A1F1490A0273B205  : public BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10
{
public:
	// System.Single HedgehogTeam.EasyTouch.Finger::startTimeAction
	float ___startTimeAction_19;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Finger::oldPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldPosition_20;
	// System.Int32 HedgehogTeam.EasyTouch.Finger::tapCount
	int32_t ___tapCount_21;
	// UnityEngine.TouchPhase HedgehogTeam.EasyTouch.Finger::phase
	int32_t ___phase_22;
	// HedgehogTeam.EasyTouch.EasyTouch_GestureType HedgehogTeam.EasyTouch.Finger::gesture
	int32_t ___gesture_23;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection HedgehogTeam.EasyTouch.Finger::oldSwipeType
	int32_t ___oldSwipeType_24;

public:
	inline static int32_t get_offset_of_startTimeAction_19() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___startTimeAction_19)); }
	inline float get_startTimeAction_19() const { return ___startTimeAction_19; }
	inline float* get_address_of_startTimeAction_19() { return &___startTimeAction_19; }
	inline void set_startTimeAction_19(float value)
	{
		___startTimeAction_19 = value;
	}

	inline static int32_t get_offset_of_oldPosition_20() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___oldPosition_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldPosition_20() const { return ___oldPosition_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldPosition_20() { return &___oldPosition_20; }
	inline void set_oldPosition_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldPosition_20 = value;
	}

	inline static int32_t get_offset_of_tapCount_21() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___tapCount_21)); }
	inline int32_t get_tapCount_21() const { return ___tapCount_21; }
	inline int32_t* get_address_of_tapCount_21() { return &___tapCount_21; }
	inline void set_tapCount_21(int32_t value)
	{
		___tapCount_21 = value;
	}

	inline static int32_t get_offset_of_phase_22() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___phase_22)); }
	inline int32_t get_phase_22() const { return ___phase_22; }
	inline int32_t* get_address_of_phase_22() { return &___phase_22; }
	inline void set_phase_22(int32_t value)
	{
		___phase_22 = value;
	}

	inline static int32_t get_offset_of_gesture_23() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___gesture_23)); }
	inline int32_t get_gesture_23() const { return ___gesture_23; }
	inline int32_t* get_address_of_gesture_23() { return &___gesture_23; }
	inline void set_gesture_23(int32_t value)
	{
		___gesture_23 = value;
	}

	inline static int32_t get_offset_of_oldSwipeType_24() { return static_cast<int32_t>(offsetof(Finger_t1C55110621038C162D210785A1F1490A0273B205, ___oldSwipeType_24)); }
	inline int32_t get_oldSwipeType_24() const { return ___oldSwipeType_24; }
	inline int32_t* get_address_of_oldSwipeType_24() { return &___oldSwipeType_24; }
	inline void set_oldSwipeType_24(int32_t value)
	{
		___oldSwipeType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGER_T1C55110621038C162D210785A1F1490A0273B205_H
#ifndef GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#define GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Gesture
struct  Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95  : public BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection HedgehogTeam.EasyTouch.Gesture::swipe
	int32_t ___swipe_19;
	// System.Single HedgehogTeam.EasyTouch.Gesture::swipeLength
	float ___swipeLength_20;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Gesture::swipeVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___swipeVector_21;
	// System.Single HedgehogTeam.EasyTouch.Gesture::deltaPinch
	float ___deltaPinch_22;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twistAngle
	float ___twistAngle_23;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twoFingerDistance
	float ___twoFingerDistance_24;
	// HedgehogTeam.EasyTouch.EasyTouch_EvtType HedgehogTeam.EasyTouch.Gesture::type
	int32_t ___type_25;

public:
	inline static int32_t get_offset_of_swipe_19() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipe_19)); }
	inline int32_t get_swipe_19() const { return ___swipe_19; }
	inline int32_t* get_address_of_swipe_19() { return &___swipe_19; }
	inline void set_swipe_19(int32_t value)
	{
		___swipe_19 = value;
	}

	inline static int32_t get_offset_of_swipeLength_20() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipeLength_20)); }
	inline float get_swipeLength_20() const { return ___swipeLength_20; }
	inline float* get_address_of_swipeLength_20() { return &___swipeLength_20; }
	inline void set_swipeLength_20(float value)
	{
		___swipeLength_20 = value;
	}

	inline static int32_t get_offset_of_swipeVector_21() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___swipeVector_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_swipeVector_21() const { return ___swipeVector_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_swipeVector_21() { return &___swipeVector_21; }
	inline void set_swipeVector_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___swipeVector_21 = value;
	}

	inline static int32_t get_offset_of_deltaPinch_22() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___deltaPinch_22)); }
	inline float get_deltaPinch_22() const { return ___deltaPinch_22; }
	inline float* get_address_of_deltaPinch_22() { return &___deltaPinch_22; }
	inline void set_deltaPinch_22(float value)
	{
		___deltaPinch_22 = value;
	}

	inline static int32_t get_offset_of_twistAngle_23() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___twistAngle_23)); }
	inline float get_twistAngle_23() const { return ___twistAngle_23; }
	inline float* get_address_of_twistAngle_23() { return &___twistAngle_23; }
	inline void set_twistAngle_23(float value)
	{
		___twistAngle_23 = value;
	}

	inline static int32_t get_offset_of_twoFingerDistance_24() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___twoFingerDistance_24)); }
	inline float get_twoFingerDistance_24() const { return ___twoFingerDistance_24; }
	inline float* get_address_of_twoFingerDistance_24() { return &___twoFingerDistance_24; }
	inline void set_twoFingerDistance_24(float value)
	{
		___twoFingerDistance_24 = value;
	}

	inline static int32_t get_offset_of_type_25() { return static_cast<int32_t>(offsetof(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95, ___type_25)); }
	inline int32_t get_type_25() const { return ___type_25; }
	inline int32_t* get_address_of_type_25() { return &___type_25; }
	inline void set_type_25(int32_t value)
	{
		___type_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_TBB08976F00DA24D38D7CE640CCD907A521BA3E95_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef UILANGUAGEPACK_TE7D8D00757F959D5424D6E03F460B617D47A0D29_H
#define UILANGUAGEPACK_TE7D8D00757F959D5424D6E03F460B617D47A0D29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.UILanguagePack
struct  UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29  : public LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30
{
public:
	// Doozy.Engine.Language Doozy.Engine.Utils.UILanguagePack::TargetLanguage
	int32_t ___TargetLanguage_9;
	// System.String Doozy.Engine.Utils.UILanguagePack::Action
	String_t* ___Action_10;
	// System.String Doozy.Engine.Utils.UILanguagePack::Actions
	String_t* ___Actions_11;
	// System.String Doozy.Engine.Utils.UILanguagePack::ActivateLoadedScenesNodeName
	String_t* ___ActivateLoadedScenesNodeName_12;
	// System.String Doozy.Engine.Utils.UILanguagePack::ActiveNode
	String_t* ___ActiveNode_13;
	// System.String Doozy.Engine.Utils.UILanguagePack::ActiveSceneChange
	String_t* ___ActiveSceneChange_14;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddCategory
	String_t* ___AddCategory_15;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddItem
	String_t* ___AddItem_16;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddSounds
	String_t* ___AddSounds_17;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddSource
	String_t* ___AddSource_18;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddSymbolToAllBuildTargetGroups
	String_t* ___AddSymbolToAllBuildTargetGroups_19;
	// System.String Doozy.Engine.Utils.UILanguagePack::AddToPopupQueue
	String_t* ___AddToPopupQueue_20;
	// System.String Doozy.Engine.Utils.UILanguagePack::After
	String_t* ___After_21;
	// System.String Doozy.Engine.Utils.UILanguagePack::AllSoundsBeforePlaying
	String_t* ___AllSoundsBeforePlaying_22;
	// System.String Doozy.Engine.Utils.UILanguagePack::AllowMultipleClicks
	String_t* ___AllowMultipleClicks_23;
	// System.String Doozy.Engine.Utils.UILanguagePack::AllowSceneActivation
	String_t* ___AllowSceneActivation_24;
	// System.String Doozy.Engine.Utils.UILanguagePack::AlternateInput
	String_t* ___AlternateInput_25;
	// System.String Doozy.Engine.Utils.UILanguagePack::AlternateKeyCode
	String_t* ___AlternateKeyCode_26;
	// System.String Doozy.Engine.Utils.UILanguagePack::AlternateVirtualButton
	String_t* ___AlternateVirtualButton_27;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnimateAllUIViewsWithSameCategoryAndName
	String_t* ___AnimateAllUIViewsWithSameCategoryAndName_28;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnimateValue
	String_t* ___AnimateValue_29;
	// System.String Doozy.Engine.Utils.UILanguagePack::Animation
	String_t* ___Animation_30;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnimationCurve
	String_t* ___AnimationCurve_31;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnimationType
	String_t* ___AnimationType_32;
	// System.String Doozy.Engine.Utils.UILanguagePack::Animator
	String_t* ___Animator_33;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnimatorEvents
	String_t* ___AnimatorEvents_34;
	// System.String Doozy.Engine.Utils.UILanguagePack::Animators
	String_t* ___Animators_35;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnotherEntryExists
	String_t* ___AnotherEntryExists_36;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyGameEvent
	String_t* ___AnyGameEvent_37;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyGameEventWillTriggerThisListener
	String_t* ___AnyGameEventWillTriggerThisListener_38;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyScene
	String_t* ___AnyScene_39;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyUIButtonWillTriggerThisListener
	String_t* ___AnyUIButtonWillTriggerThisListener_40;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyUIDrawerWillTriggerThisListener
	String_t* ___AnyUIDrawerWillTriggerThisListener_41;
	// System.String Doozy.Engine.Utils.UILanguagePack::AnyUIViewWillTriggerThisListener
	String_t* ___AnyUIViewWillTriggerThisListener_42;
	// System.String Doozy.Engine.Utils.UILanguagePack::ApplicationQuit
	String_t* ___ApplicationQuit_43;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureConvertToGraph
	String_t* ___AreYouSureConvertToGraph_44;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureConvertToSubGraph
	String_t* ___AreYouSureConvertToSubGraph_45;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureYouWantToDeleteDatabase
	String_t* ___AreYouSureYouWantToDeleteDatabase_46;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureYouWantToDeletePopupReference
	String_t* ___AreYouSureYouWantToDeletePopupReference_47;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureYouWantToRemoveCategory
	String_t* ___AreYouSureYouWantToRemoveCategory_48;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureYouWantToRemoveTheEntry
	String_t* ___AreYouSureYouWantToRemoveTheEntry_49;
	// System.String Doozy.Engine.Utils.UILanguagePack::AreYouSureYouWantToResetDatabase
	String_t* ___AreYouSureYouWantToResetDatabase_50;
	// System.String Doozy.Engine.Utils.UILanguagePack::Arrow
	String_t* ___Arrow_51;
	// System.String Doozy.Engine.Utils.UILanguagePack::ArrowComponents
	String_t* ___ArrowComponents_52;
	// System.String Doozy.Engine.Utils.UILanguagePack::AtFinish
	String_t* ___AtFinish_53;
	// System.String Doozy.Engine.Utils.UILanguagePack::AtStart
	String_t* ___AtStart_54;
	// System.String Doozy.Engine.Utils.UILanguagePack::AudioClip
	String_t* ___AudioClip_55;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoDisableUIInteractionsDescription
	String_t* ___AutoDisableUIInteractionsDescription_56;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoHideAfterShow
	String_t* ___AutoHideAfterShow_57;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoKillIdleControllers
	String_t* ___AutoKillIdleControllers_58;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoKillIdleControllersDescription
	String_t* ___AutoKillIdleControllersDescription_59;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoResetSequence
	String_t* ___AutoResetSequence_60;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoSelectButtonAfterShow
	String_t* ___AutoSelectButtonAfterShow_61;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoSort
	String_t* ___AutoSort_62;
	// System.String Doozy.Engine.Utils.UILanguagePack::AutoStartLoopAnimation
	String_t* ___AutoStartLoopAnimation_63;
	// System.String Doozy.Engine.Utils.UILanguagePack::BackButton
	String_t* ___BackButton_64;
	// System.String Doozy.Engine.Utils.UILanguagePack::BackButtonNodeName
	String_t* ___BackButtonNodeName_65;
	// System.String Doozy.Engine.Utils.UILanguagePack::Behavior
	String_t* ___Behavior_66;
	// System.String Doozy.Engine.Utils.UILanguagePack::BehaviorAtStart
	String_t* ___BehaviorAtStart_67;
	// System.String Doozy.Engine.Utils.UILanguagePack::Behaviors
	String_t* ___Behaviors_68;
	// System.String Doozy.Engine.Utils.UILanguagePack::BuildIndex
	String_t* ___BuildIndex_69;
	// System.String Doozy.Engine.Utils.UILanguagePack::ButtonCategory
	String_t* ___ButtonCategory_70;
	// System.String Doozy.Engine.Utils.UILanguagePack::ButtonLabel
	String_t* ___ButtonLabel_71;
	// System.String Doozy.Engine.Utils.UILanguagePack::ButtonName
	String_t* ___ButtonName_72;
	// System.String Doozy.Engine.Utils.UILanguagePack::Buttons
	String_t* ___Buttons_73;
	// System.String Doozy.Engine.Utils.UILanguagePack::Cancel
	String_t* ___Cancel_74;
	// System.String Doozy.Engine.Utils.UILanguagePack::CannotAddEmptyEntry
	String_t* ___CannotAddEmptyEntry_75;
	// System.String Doozy.Engine.Utils.UILanguagePack::Canvas
	String_t* ___Canvas_76;
	// System.String Doozy.Engine.Utils.UILanguagePack::CanvasName
	String_t* ___CanvasName_77;
	// System.String Doozy.Engine.Utils.UILanguagePack::Category
	String_t* ___Category_78;
	// System.String Doozy.Engine.Utils.UILanguagePack::CategoryIsEmpty
	String_t* ___CategoryIsEmpty_79;
	// System.String Doozy.Engine.Utils.UILanguagePack::Center
	String_t* ___Center_80;
	// System.String Doozy.Engine.Utils.UILanguagePack::CenterSelectedNodes
	String_t* ___CenterSelectedNodes_81;
	// System.String Doozy.Engine.Utils.UILanguagePack::Chance
	String_t* ___Chance_82;
	// System.String Doozy.Engine.Utils.UILanguagePack::CheckingForIssues
	String_t* ___CheckingForIssues_83;
	// System.String Doozy.Engine.Utils.UILanguagePack::Clear
	String_t* ___Clear_84;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClearRecentOpenedGraphsList
	String_t* ___ClearRecentOpenedGraphsList_85;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClearRecentOpenedGraphsListDescription
	String_t* ___ClearRecentOpenedGraphsListDescription_86;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClearSearch
	String_t* ___ClearSearch_87;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClickAnywhere
	String_t* ___ClickAnywhere_88;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClickContainer
	String_t* ___ClickContainer_89;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClickMode
	String_t* ___ClickMode_90;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClickOverlay
	String_t* ___ClickOverlay_91;
	// System.String Doozy.Engine.Utils.UILanguagePack::Close
	String_t* ___Close_92;
	// System.String Doozy.Engine.Utils.UILanguagePack::CloseDirection
	String_t* ___CloseDirection_93;
	// System.String Doozy.Engine.Utils.UILanguagePack::CloseDrawer
	String_t* ___CloseDrawer_94;
	// System.String Doozy.Engine.Utils.UILanguagePack::CloseSpeed
	String_t* ___CloseSpeed_95;
	// System.String Doozy.Engine.Utils.UILanguagePack::Closed
	String_t* ___Closed_96;
	// System.String Doozy.Engine.Utils.UILanguagePack::ClosedPosition
	String_t* ___ClosedPosition_97;
	// System.String Doozy.Engine.Utils.UILanguagePack::Compiling
	String_t* ___Compiling_98;
	// System.String Doozy.Engine.Utils.UILanguagePack::ComponentDisabled
	String_t* ___ComponentDisabled_99;
	// System.String Doozy.Engine.Utils.UILanguagePack::Connected
	String_t* ___Connected_100;
	// System.String Doozy.Engine.Utils.UILanguagePack::ConnectionPoint
	String_t* ___ConnectionPoint_101;
	// System.String Doozy.Engine.Utils.UILanguagePack::Connections
	String_t* ___Connections_102;
	// System.String Doozy.Engine.Utils.UILanguagePack::Container
	String_t* ___Container_103;
	// System.String Doozy.Engine.Utils.UILanguagePack::ContainerSize
	String_t* ___ContainerSize_104;
	// System.String Doozy.Engine.Utils.UILanguagePack::Continue
	String_t* ___Continue_105;
	// System.String Doozy.Engine.Utils.UILanguagePack::ControllerIdleKillDuration
	String_t* ___ControllerIdleKillDuration_106;
	// System.String Doozy.Engine.Utils.UILanguagePack::ControllerIdleKillDurationDescription
	String_t* ___ControllerIdleKillDurationDescription_107;
	// System.String Doozy.Engine.Utils.UILanguagePack::ControllerName
	String_t* ___ControllerName_108;
	// System.String Doozy.Engine.Utils.UILanguagePack::ConvertToGraph
	String_t* ___ConvertToGraph_109;
	// System.String Doozy.Engine.Utils.UILanguagePack::ConvertToSubGraph
	String_t* ___ConvertToSubGraph_110;
	// System.String Doozy.Engine.Utils.UILanguagePack::Copy
	String_t* ___Copy_111;
	// System.String Doozy.Engine.Utils.UILanguagePack::Create
	String_t* ___Create_112;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateAnimation
	String_t* ___CreateAnimation_113;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateGraph
	String_t* ___CreateGraph_114;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateNewCategory
	String_t* ___CreateNewCategory_115;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateNewGraph
	String_t* ___CreateNewGraph_116;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateNewGraphAsSubGraph
	String_t* ___CreateNewGraphAsSubGraph_117;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateNode
	String_t* ___CreateNode_118;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateParentAndCenterPivot
	String_t* ___CreateParentAndCenterPivot_119;
	// System.String Doozy.Engine.Utils.UILanguagePack::CreateSubGraph
	String_t* ___CreateSubGraph_120;
	// System.String Doozy.Engine.Utils.UILanguagePack::CurrentTimeScale
	String_t* ___CurrentTimeScale_121;
	// System.String Doozy.Engine.Utils.UILanguagePack::Custom
	String_t* ___Custom_122;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomName
	String_t* ___CustomName_123;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomPosition
	String_t* ___CustomPosition_124;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomResetValue
	String_t* ___CustomResetValue_125;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomSortingLayer
	String_t* ___CustomSortingLayer_126;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomSortingOrder
	String_t* ___CustomSortingOrder_127;
	// System.String Doozy.Engine.Utils.UILanguagePack::CustomStartPosition
	String_t* ___CustomStartPosition_128;
	// System.String Doozy.Engine.Utils.UILanguagePack::Cut
	String_t* ___Cut_129;
	// System.String Doozy.Engine.Utils.UILanguagePack::Database
	String_t* ___Database_130;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseAlreadyExists
	String_t* ___DatabaseAlreadyExists_131;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseHasBeenReset
	String_t* ___DatabaseHasBeenReset_132;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseIsEmpty
	String_t* ___DatabaseIsEmpty_133;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseName
	String_t* ___DatabaseName_134;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseRefreshed
	String_t* ___DatabaseRefreshed_135;
	// System.String Doozy.Engine.Utils.UILanguagePack::DatabaseSorted
	String_t* ___DatabaseSorted_136;
	// System.String Doozy.Engine.Utils.UILanguagePack::Debug
	String_t* ___Debug_137;
	// System.String Doozy.Engine.Utils.UILanguagePack::DebugMode
	String_t* ___DebugMode_138;
	// System.String Doozy.Engine.Utils.UILanguagePack::DefaultDotAnimationSpeedDescription
	String_t* ___DefaultDotAnimationSpeedDescription_139;
	// System.String Doozy.Engine.Utils.UILanguagePack::DefaultValues
	String_t* ___DefaultValues_140;
	// System.String Doozy.Engine.Utils.UILanguagePack::DefaultValuesDescription
	String_t* ___DefaultValuesDescription_141;
	// System.String Doozy.Engine.Utils.UILanguagePack::DefaultZoom
	String_t* ___DefaultZoom_142;
	// System.String Doozy.Engine.Utils.UILanguagePack::DefaultZoomDescription
	String_t* ___DefaultZoomDescription_143;
	// System.String Doozy.Engine.Utils.UILanguagePack::Delete
	String_t* ___Delete_144;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeleteDatabase
	String_t* ___DeleteDatabase_145;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeletePopupReference
	String_t* ___DeletePopupReference_146;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeletePreset
	String_t* ___DeletePreset_147;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeletedPopupReference
	String_t* ___DeletedPopupReference_148;
	// System.String Doozy.Engine.Utils.UILanguagePack::Description
	String_t* ___Description_149;
	// System.String Doozy.Engine.Utils.UILanguagePack::Deselect
	String_t* ___Deselect_150;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeselectAnyButton
	String_t* ___DeselectAnyButton_151;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeselectButton
	String_t* ___DeselectButton_152;
	// System.String Doozy.Engine.Utils.UILanguagePack::DeselectButtonAfterClick
	String_t* ___DeselectButtonAfterClick_153;
	// System.String Doozy.Engine.Utils.UILanguagePack::DestroyAfterHide
	String_t* ___DestroyAfterHide_154;
	// System.String Doozy.Engine.Utils.UILanguagePack::DetectGestures
	String_t* ___DetectGestures_155;
	// System.String Doozy.Engine.Utils.UILanguagePack::Direction
	String_t* ___Direction_156;
	// System.String Doozy.Engine.Utils.UILanguagePack::Disable
	String_t* ___Disable_157;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisableButtonInterval
	String_t* ___DisableButtonInterval_158;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisableCanvas
	String_t* ___DisableCanvas_159;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisableFunctionality
	String_t* ___DisableFunctionality_160;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisableGameObject
	String_t* ___DisableGameObject_161;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisableInterval
	String_t* ___DisableInterval_162;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisablePlugin
	String_t* ___DisablePlugin_163;
	// System.String Doozy.Engine.Utils.UILanguagePack::Disabled
	String_t* ___Disabled_164;
	// System.String Doozy.Engine.Utils.UILanguagePack::Disconnect
	String_t* ___Disconnect_165;
	// System.String Doozy.Engine.Utils.UILanguagePack::DispatchButtonClicks
	String_t* ___DispatchButtonClicks_166;
	// System.String Doozy.Engine.Utils.UILanguagePack::DispatchGameEvents
	String_t* ___DispatchGameEvents_167;
	// System.String Doozy.Engine.Utils.UILanguagePack::DisplayTarget
	String_t* ___DisplayTarget_168;
	// System.String Doozy.Engine.Utils.UILanguagePack::DontDestroyGameObjectOnLoad
	String_t* ___DontDestroyGameObjectOnLoad_169;
	// System.String Doozy.Engine.Utils.UILanguagePack::DotAnimationSpeed
	String_t* ___DotAnimationSpeed_170;
	// System.String Doozy.Engine.Utils.UILanguagePack::Down
	String_t* ___Down_171;
	// System.String Doozy.Engine.Utils.UILanguagePack::DragDrawer
	String_t* ___DragDrawer_172;
	// System.String Doozy.Engine.Utils.UILanguagePack::DrawerName
	String_t* ___DrawerName_173;
	// System.String Doozy.Engine.Utils.UILanguagePack::DropAudioClipsHere
	String_t* ___DropAudioClipsHere_174;
	// System.String Doozy.Engine.Utils.UILanguagePack::Duration
	String_t* ___Duration_175;
	// System.String Doozy.Engine.Utils.UILanguagePack::Ease
	String_t* ___Ease_176;
	// System.String Doozy.Engine.Utils.UILanguagePack::EaseType
	String_t* ___EaseType_177;
	// System.String Doozy.Engine.Utils.UILanguagePack::Effect
	String_t* ___Effect_178;
	// System.String Doozy.Engine.Utils.UILanguagePack::Elasticity
	String_t* ___Elasticity_179;
	// System.String Doozy.Engine.Utils.UILanguagePack::Email
	String_t* ___Email_180;
	// System.String Doozy.Engine.Utils.UILanguagePack::Empty
	String_t* ___Empty_181;
	// System.String Doozy.Engine.Utils.UILanguagePack::Enable
	String_t* ___Enable_182;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnableFunctionality
	String_t* ___EnableFunctionality_183;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnablePlugin
	String_t* ___EnablePlugin_184;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnableSupportForMasterAudio
	String_t* ___EnableSupportForMasterAudio_185;
	// System.String Doozy.Engine.Utils.UILanguagePack::Enabled
	String_t* ___Enabled_186;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnterCategoryName
	String_t* ___EnterCategoryName_187;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnterDatabaseName
	String_t* ___EnterDatabaseName_188;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnterGameEventToListenFor
	String_t* ___EnterGameEventToListenFor_189;
	// System.String Doozy.Engine.Utils.UILanguagePack::EnterNodeName
	String_t* ___EnterNodeName_190;
	// System.String Doozy.Engine.Utils.UILanguagePack::Error
	String_t* ___Error_191;
	// System.String Doozy.Engine.Utils.UILanguagePack::Event
	String_t* ___Event_192;
	// System.String Doozy.Engine.Utils.UILanguagePack::EveryLongTapWillTriggerThisListener
	String_t* ___EveryLongTapWillTriggerThisListener_193;
	// System.String Doozy.Engine.Utils.UILanguagePack::EverySwipeWillTriggerThisListener
	String_t* ___EverySwipeWillTriggerThisListener_194;
	// System.String Doozy.Engine.Utils.UILanguagePack::EveryTapWillTriggerThisListener
	String_t* ___EveryTapWillTriggerThisListener_195;
	// System.String Doozy.Engine.Utils.UILanguagePack::ExitNodeName
	String_t* ___ExitNodeName_196;
	// System.String Doozy.Engine.Utils.UILanguagePack::Fade
	String_t* ___Fade_197;
	// System.String Doozy.Engine.Utils.UILanguagePack::FadeBy
	String_t* ___FadeBy_198;
	// System.String Doozy.Engine.Utils.UILanguagePack::FadeFrom
	String_t* ___FadeFrom_199;
	// System.String Doozy.Engine.Utils.UILanguagePack::FadeOutContainer
	String_t* ___FadeOutContainer_200;
	// System.String Doozy.Engine.Utils.UILanguagePack::FadeTo
	String_t* ___FadeTo_201;
	// System.String Doozy.Engine.Utils.UILanguagePack::FixedSize
	String_t* ___FixedSize_202;
	// System.String Doozy.Engine.Utils.UILanguagePack::FsmName
	String_t* ___FsmName_203;
	// System.String Doozy.Engine.Utils.UILanguagePack::Functionality
	String_t* ___Functionality_204;
	// System.String Doozy.Engine.Utils.UILanguagePack::FunctionalityDescription
	String_t* ___FunctionalityDescription_205;
	// System.String Doozy.Engine.Utils.UILanguagePack::GameEvent
	String_t* ___GameEvent_206;
	// System.String Doozy.Engine.Utils.UILanguagePack::GameEvents
	String_t* ___GameEvents_207;
	// System.String Doozy.Engine.Utils.UILanguagePack::GameObject
	String_t* ___GameObject_208;
	// System.String Doozy.Engine.Utils.UILanguagePack::GeneralSettings
	String_t* ___GeneralSettings_209;
	// System.String Doozy.Engine.Utils.UILanguagePack::GestureType
	String_t* ___GestureType_210;
	// System.String Doozy.Engine.Utils.UILanguagePack::GetInTouch
	String_t* ___GetInTouch_211;
	// System.String Doozy.Engine.Utils.UILanguagePack::GetPosition
	String_t* ___GetPosition_212;
	// System.String Doozy.Engine.Utils.UILanguagePack::GetSceneBy
	String_t* ___GetSceneBy_213;
	// System.String Doozy.Engine.Utils.UILanguagePack::GlobalListener
	String_t* ___GlobalListener_214;
	// System.String Doozy.Engine.Utils.UILanguagePack::GoToEnterNode
	String_t* ___GoToEnterNode_215;
	// System.String Doozy.Engine.Utils.UILanguagePack::GoToExitNode
	String_t* ___GoToExitNode_216;
	// System.String Doozy.Engine.Utils.UILanguagePack::GoToStartNode
	String_t* ___GoToStartNode_217;
	// System.String Doozy.Engine.Utils.UILanguagePack::Graph
	String_t* ___Graph_218;
	// System.String Doozy.Engine.Utils.UILanguagePack::GraphHasNoNodes
	String_t* ___GraphHasNoNodes_219;
	// System.String Doozy.Engine.Utils.UILanguagePack::GraphId
	String_t* ___GraphId_220;
	// System.String Doozy.Engine.Utils.UILanguagePack::GraphModel
	String_t* ___GraphModel_221;
	// System.String Doozy.Engine.Utils.UILanguagePack::GraphicRaycaster
	String_t* ___GraphicRaycaster_222;
	// System.String Doozy.Engine.Utils.UILanguagePack::HasBeenAddedToClipboard
	String_t* ___HasBeenAddedToClipboard_223;
	// System.String Doozy.Engine.Utils.UILanguagePack::Height
	String_t* ___Height_224;
	// System.String Doozy.Engine.Utils.UILanguagePack::Help
	String_t* ___Help_225;
	// System.String Doozy.Engine.Utils.UILanguagePack::HelpResources
	String_t* ___HelpResources_226;
	// System.String Doozy.Engine.Utils.UILanguagePack::Hide
	String_t* ___Hide_227;
	// System.String Doozy.Engine.Utils.UILanguagePack::HideAnimationWillNotWork
	String_t* ___HideAnimationWillNotWork_228;
	// System.String Doozy.Engine.Utils.UILanguagePack::HidePopup
	String_t* ___HidePopup_229;
	// System.String Doozy.Engine.Utils.UILanguagePack::HideProgressor
	String_t* ___HideProgressor_230;
	// System.String Doozy.Engine.Utils.UILanguagePack::HideUIPopupBy
	String_t* ___HideUIPopupBy_231;
	// System.String Doozy.Engine.Utils.UILanguagePack::HideView
	String_t* ___HideView_232;
	// System.String Doozy.Engine.Utils.UILanguagePack::HideViews
	String_t* ___HideViews_233;
	// System.String Doozy.Engine.Utils.UILanguagePack::HowToUse
	String_t* ___HowToUse_234;
	// System.String Doozy.Engine.Utils.UILanguagePack::IdleCheckInterval
	String_t* ___IdleCheckInterval_235;
	// System.String Doozy.Engine.Utils.UILanguagePack::IdleCheckIntervalDescription
	String_t* ___IdleCheckIntervalDescription_236;
	// System.String Doozy.Engine.Utils.UILanguagePack::IgnoreListenerPause
	String_t* ___IgnoreListenerPause_237;
	// System.String Doozy.Engine.Utils.UILanguagePack::IgnoreTimescale
	String_t* ___IgnoreTimescale_238;
	// System.String Doozy.Engine.Utils.UILanguagePack::Image
	String_t* ___Image_239;
	// System.String Doozy.Engine.Utils.UILanguagePack::Images
	String_t* ___Images_240;
	// System.String Doozy.Engine.Utils.UILanguagePack::IncludeAudioClipNamesInSearch
	String_t* ___IncludeAudioClipNamesInSearch_241;
	// System.String Doozy.Engine.Utils.UILanguagePack::Info
	String_t* ___Info_242;
	// System.String Doozy.Engine.Utils.UILanguagePack::InputConnected
	String_t* ___InputConnected_243;
	// System.String Doozy.Engine.Utils.UILanguagePack::InputConnections
	String_t* ___InputConnections_244;
	// System.String Doozy.Engine.Utils.UILanguagePack::InputMode
	String_t* ___InputMode_245;
	// System.String Doozy.Engine.Utils.UILanguagePack::InputNotConnected
	String_t* ___InputNotConnected_246;
	// System.String Doozy.Engine.Utils.UILanguagePack::Installed
	String_t* ___Installed_247;
	// System.String Doozy.Engine.Utils.UILanguagePack::InstantAction
	String_t* ___InstantAction_248;
	// System.String Doozy.Engine.Utils.UILanguagePack::InstantAnimation
	String_t* ___InstantAnimation_249;
	// System.String Doozy.Engine.Utils.UILanguagePack::Integrations
	String_t* ___Integrations_250;
	// System.String Doozy.Engine.Utils.UILanguagePack::IsNotPrefab
	String_t* ___IsNotPrefab_251;
	// System.String Doozy.Engine.Utils.UILanguagePack::Key
	String_t* ___Key_252;
	// System.String Doozy.Engine.Utils.UILanguagePack::KeyCode
	String_t* ___KeyCode_253;
	// System.String Doozy.Engine.Utils.UILanguagePack::Label
	String_t* ___Label_254;
	// System.String Doozy.Engine.Utils.UILanguagePack::Labels
	String_t* ___Labels_255;
	// System.String Doozy.Engine.Utils.UILanguagePack::Language
	String_t* ___Language_256;
	// System.String Doozy.Engine.Utils.UILanguagePack::LastModified
	String_t* ___LastModified_257;
	// System.String Doozy.Engine.Utils.UILanguagePack::Left
	String_t* ___Left_258;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListIsEmpty
	String_t* ___ListIsEmpty_259;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListenForAllGameEvents
	String_t* ___ListenForAllGameEvents_260;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListenForAllUIButtons
	String_t* ___ListenForAllUIButtons_261;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListenForAllUIDrawers
	String_t* ___ListenForAllUIDrawers_262;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListenForAllUIViews
	String_t* ___ListenForAllUIViews_263;
	// System.String Doozy.Engine.Utils.UILanguagePack::ListeningForGameEvent
	String_t* ___ListeningForGameEvent_264;
	// System.String Doozy.Engine.Utils.UILanguagePack::Load
	String_t* ___Load_265;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadBehavior
	String_t* ___LoadBehavior_266;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadPreset
	String_t* ___LoadPreset_267;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadSceneBy
	String_t* ___LoadSceneBy_268;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadSceneMode
	String_t* ___LoadSceneMode_269;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadSceneNodeName
	String_t* ___LoadSceneNodeName_270;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadSelectedPresetAtRuntime
	String_t* ___LoadSelectedPresetAtRuntime_271;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoadedGraph
	String_t* ___LoadedGraph_272;
	// System.String Doozy.Engine.Utils.UILanguagePack::LongTapDuration
	String_t* ___LongTapDuration_273;
	// System.String Doozy.Engine.Utils.UILanguagePack::LongTapDurationDescription
	String_t* ___LongTapDurationDescription_274;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoopSound
	String_t* ___LoopSound_275;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoopType
	String_t* ___LoopType_276;
	// System.String Doozy.Engine.Utils.UILanguagePack::LoopView
	String_t* ___LoopView_277;
	// System.String Doozy.Engine.Utils.UILanguagePack::Manual
	String_t* ___Manual_278;
	// System.String Doozy.Engine.Utils.UILanguagePack::Max
	String_t* ___Max_279;
	// System.String Doozy.Engine.Utils.UILanguagePack::MaxValue
	String_t* ___MaxValue_280;
	// System.String Doozy.Engine.Utils.UILanguagePack::Min
	String_t* ___Min_281;
	// System.String Doozy.Engine.Utils.UILanguagePack::MinValue
	String_t* ___MinValue_282;
	// System.String Doozy.Engine.Utils.UILanguagePack::MinimumNumberOfControllers
	String_t* ___MinimumNumberOfControllers_283;
	// System.String Doozy.Engine.Utils.UILanguagePack::MinimumNumberOfControllersDescription
	String_t* ___MinimumNumberOfControllersDescription_284;
	// System.String Doozy.Engine.Utils.UILanguagePack::MinimumSize
	String_t* ___MinimumSize_285;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingPrefabReference
	String_t* ___MissingPrefabReference_286;
	// System.String Doozy.Engine.Utils.UILanguagePack::Move
	String_t* ___Move_287;
	// System.String Doozy.Engine.Utils.UILanguagePack::MoveBy
	String_t* ___MoveBy_288;
	// System.String Doozy.Engine.Utils.UILanguagePack::MoveDown
	String_t* ___MoveDown_289;
	// System.String Doozy.Engine.Utils.UILanguagePack::MoveFrom
	String_t* ___MoveFrom_290;
	// System.String Doozy.Engine.Utils.UILanguagePack::MoveTo
	String_t* ___MoveTo_291;
	// System.String Doozy.Engine.Utils.UILanguagePack::MoveUp
	String_t* ___MoveUp_292;
	// System.String Doozy.Engine.Utils.UILanguagePack::Multiplier
	String_t* ___Multiplier_293;
	// System.String Doozy.Engine.Utils.UILanguagePack::MuteAllSounds
	String_t* ___MuteAllSounds_294;
	// System.String Doozy.Engine.Utils.UILanguagePack::Name
	String_t* ___Name_295;
	// System.String Doozy.Engine.Utils.UILanguagePack::New
	String_t* ___New_296;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewCategory
	String_t* ___NewCategory_297;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewCategoryNameCannotBeEmpty
	String_t* ___NewCategoryNameCannotBeEmpty_298;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewDatabase
	String_t* ___NewDatabase_299;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewPopup
	String_t* ___NewPopup_300;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewPreset
	String_t* ___NewPreset_301;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewPresetNameCannotBeEmpty
	String_t* ___NewPresetNameCannotBeEmpty_302;
	// System.String Doozy.Engine.Utils.UILanguagePack::NewSoundDatabase
	String_t* ___NewSoundDatabase_303;
	// System.String Doozy.Engine.Utils.UILanguagePack::News
	String_t* ___News_304;
	// System.String Doozy.Engine.Utils.UILanguagePack::No
	String_t* ___No_305;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoAnimationEnabled
	String_t* ___NoAnimationEnabled_306;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoAnimatorFound
	String_t* ___NoAnimatorFound_307;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoGraphReferenced
	String_t* ___NoGraphReferenced_308;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSound
	String_t* ___NoSound_309;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSoundsHaveBeenAdded
	String_t* ___NoSoundsHaveBeenAdded_310;
	// System.String Doozy.Engine.Utils.UILanguagePack::Node
	String_t* ___Node_311;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodeId
	String_t* ___NodeId_312;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodeName
	String_t* ___NodeName_313;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodeNameTooltip
	String_t* ___NodeNameTooltip_314;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodeState
	String_t* ___NodeState_315;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodeWidth
	String_t* ___NodeWidth_316;
	// System.String Doozy.Engine.Utils.UILanguagePack::Nodes
	String_t* ___Nodes_317;
	// System.String Doozy.Engine.Utils.UILanguagePack::NodySettings
	String_t* ___NodySettings_318;
	// System.String Doozy.Engine.Utils.UILanguagePack::NormalLoopAnimation
	String_t* ___NormalLoopAnimation_319;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotConnected
	String_t* ___NotConnected_320;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotInstalled
	String_t* ___NotInstalled_321;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoteworthyInformation
	String_t* ___NoteworthyInformation_322;
	// System.String Doozy.Engine.Utils.UILanguagePack::NumberOfLoops
	String_t* ___NumberOfLoops_323;
	// System.String Doozy.Engine.Utils.UILanguagePack::Ok
	String_t* ___Ok_324;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnAnimationFinished
	String_t* ___OnAnimationFinished_325;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnAnimationStart
	String_t* ___OnAnimationStart_326;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnClick
	String_t* ___OnClick_327;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnDeselected
	String_t* ___OnDeselected_328;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnDoubleClick
	String_t* ___OnDoubleClick_329;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnEnter
	String_t* ___OnEnter_330;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnEnterNode
	String_t* ___OnEnterNode_331;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnExit
	String_t* ___OnExit_332;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnExitNode
	String_t* ___OnExitNode_333;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnFixedUpdate
	String_t* ___OnFixedUpdate_334;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnLateUpdate
	String_t* ___OnLateUpdate_335;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnLoadScene
	String_t* ___OnLoadScene_336;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnLongClick
	String_t* ___OnLongClick_337;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnNodeFixedUpdate
	String_t* ___OnNodeFixedUpdate_338;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnNodeLateUpdate
	String_t* ___OnNodeLateUpdate_339;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnNodeUpdate
	String_t* ___OnNodeUpdate_340;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnPointerDown
	String_t* ___OnPointerDown_341;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnPointerEnter
	String_t* ___OnPointerEnter_342;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnPointerExit
	String_t* ___OnPointerExit_343;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnPointerUp
	String_t* ___OnPointerUp_344;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnSceneLoaded
	String_t* ___OnSceneLoaded_345;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnSelected
	String_t* ___OnSelected_346;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnTrigger
	String_t* ___OnTrigger_347;
	// System.String Doozy.Engine.Utils.UILanguagePack::OnUpdate
	String_t* ___OnUpdate_348;
	// System.String Doozy.Engine.Utils.UILanguagePack::Open
	String_t* ___Open_349;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenControlPanel
	String_t* ___OpenControlPanel_350;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenDatabase
	String_t* ___OpenDatabase_351;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenDrawer
	String_t* ___OpenDrawer_352;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenGraph
	String_t* ___OpenGraph_353;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenNody
	String_t* ___OpenNody_354;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenSpeed
	String_t* ___OpenSpeed_355;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenSubGraph
	String_t* ___OpenSubGraph_356;
	// System.String Doozy.Engine.Utils.UILanguagePack::Opened
	String_t* ___Opened_357;
	// System.String Doozy.Engine.Utils.UILanguagePack::OpenedPosition
	String_t* ___OpenedPosition_358;
	// System.String Doozy.Engine.Utils.UILanguagePack::OperationCannotBeUndone
	String_t* ___OperationCannotBeUndone_359;
	// System.String Doozy.Engine.Utils.UILanguagePack::OrientationDetectorDescription
	String_t* ___OrientationDetectorDescription_360;
	// System.String Doozy.Engine.Utils.UILanguagePack::OtherReferences
	String_t* ___OtherReferences_361;
	// System.String Doozy.Engine.Utils.UILanguagePack::OutputAudioMixerGroup
	String_t* ___OutputAudioMixerGroup_362;
	// System.String Doozy.Engine.Utils.UILanguagePack::OutputConnected
	String_t* ___OutputConnected_363;
	// System.String Doozy.Engine.Utils.UILanguagePack::OutputConnections
	String_t* ___OutputConnections_364;
	// System.String Doozy.Engine.Utils.UILanguagePack::OutputMixerGroup
	String_t* ___OutputMixerGroup_365;
	// System.String Doozy.Engine.Utils.UILanguagePack::OutputNotConnected
	String_t* ___OutputNotConnected_366;
	// System.String Doozy.Engine.Utils.UILanguagePack::Overlay
	String_t* ___Overlay_367;
	// System.String Doozy.Engine.Utils.UILanguagePack::Override
	String_t* ___Override_368;
	// System.String Doozy.Engine.Utils.UILanguagePack::OverrideColor
	String_t* ___OverrideColor_369;
	// System.String Doozy.Engine.Utils.UILanguagePack::Overview
	String_t* ___Overview_370;
	// System.String Doozy.Engine.Utils.UILanguagePack::OverviewZoom
	String_t* ___OverviewZoom_371;
	// System.String Doozy.Engine.Utils.UILanguagePack::ParameterName
	String_t* ___ParameterName_372;
	// System.String Doozy.Engine.Utils.UILanguagePack::ParameterType
	String_t* ___ParameterType_373;
	// System.String Doozy.Engine.Utils.UILanguagePack::ParticleSystem
	String_t* ___ParticleSystem_374;
	// System.String Doozy.Engine.Utils.UILanguagePack::Paste
	String_t* ___Paste_375;
	// System.String Doozy.Engine.Utils.UILanguagePack::PauseAllSounds
	String_t* ___PauseAllSounds_376;
	// System.String Doozy.Engine.Utils.UILanguagePack::PercentageOfScreenZeroToOne
	String_t* ___PercentageOfScreenZeroToOne_377;
	// System.String Doozy.Engine.Utils.UILanguagePack::PitchSemitones
	String_t* ___PitchSemitones_378;
	// System.String Doozy.Engine.Utils.UILanguagePack::Play
	String_t* ___Play_379;
	// System.String Doozy.Engine.Utils.UILanguagePack::PlayAnimationInZeroSeconds
	String_t* ___PlayAnimationInZeroSeconds_380;
	// System.String Doozy.Engine.Utils.UILanguagePack::PlayMode
	String_t* ___PlayMode_381;
	// System.String Doozy.Engine.Utils.UILanguagePack::PlaySound
	String_t* ___PlaySound_382;
	// System.String Doozy.Engine.Utils.UILanguagePack::PleaseEnterNewName
	String_t* ___PleaseEnterNewName_383;
	// System.String Doozy.Engine.Utils.UILanguagePack::PopupName
	String_t* ___PopupName_384;
	// System.String Doozy.Engine.Utils.UILanguagePack::PopupPrefab
	String_t* ___PopupPrefab_385;
	// System.String Doozy.Engine.Utils.UILanguagePack::Portal
	String_t* ___Portal_386;
	// System.String Doozy.Engine.Utils.UILanguagePack::PortalNodeName
	String_t* ___PortalNodeName_387;
	// System.String Doozy.Engine.Utils.UILanguagePack::Prefix
	String_t* ___Prefix_388;
	// System.String Doozy.Engine.Utils.UILanguagePack::PresetCategory
	String_t* ___PresetCategory_389;
	// System.String Doozy.Engine.Utils.UILanguagePack::PresetName
	String_t* ___PresetName_390;
	// System.String Doozy.Engine.Utils.UILanguagePack::PreviewAnimation
	String_t* ___PreviewAnimation_391;
	// System.String Doozy.Engine.Utils.UILanguagePack::Progress
	String_t* ___Progress_392;
	// System.String Doozy.Engine.Utils.UILanguagePack::ProgressTargets
	String_t* ___ProgressTargets_393;
	// System.String Doozy.Engine.Utils.UILanguagePack::Progressor
	String_t* ___Progressor_394;
	// System.String Doozy.Engine.Utils.UILanguagePack::Progressors
	String_t* ___Progressors_395;
	// System.String Doozy.Engine.Utils.UILanguagePack::PunchBy
	String_t* ___PunchBy_396;
	// System.String Doozy.Engine.Utils.UILanguagePack::RandomDuration
	String_t* ___RandomDuration_397;
	// System.String Doozy.Engine.Utils.UILanguagePack::RandomNodeName
	String_t* ___RandomNodeName_398;
	// System.String Doozy.Engine.Utils.UILanguagePack::Recent
	String_t* ___Recent_399;
	// System.String Doozy.Engine.Utils.UILanguagePack::Refresh
	String_t* ___Refresh_400;
	// System.String Doozy.Engine.Utils.UILanguagePack::RefreshDatabase
	String_t* ___RefreshDatabase_401;
	// System.String Doozy.Engine.Utils.UILanguagePack::RegisterInterval
	String_t* ___RegisterInterval_402;
	// System.String Doozy.Engine.Utils.UILanguagePack::Remove
	String_t* ___Remove_403;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveCategory
	String_t* ___RemoveCategory_404;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveDuplicates
	String_t* ___RemoveDuplicates_405;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveEmptyCategories
	String_t* ___RemoveEmptyCategories_406;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveEmptyEntries
	String_t* ___RemoveEmptyEntries_407;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveItem
	String_t* ___RemoveItem_408;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveNullEntries
	String_t* ___RemoveNullEntries_409;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemoveSymbolFromAllBuildTargetGroups
	String_t* ___RemoveSymbolFromAllBuildTargetGroups_410;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemovedDuplicateEntries
	String_t* ___RemovedDuplicateEntries_411;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemovedEntriesWithNoName
	String_t* ___RemovedEntriesWithNoName_412;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemovedEntriesWithNullPrefabs
	String_t* ___RemovedEntriesWithNullPrefabs_413;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemovedEntry
	String_t* ___RemovedEntry_414;
	// System.String Doozy.Engine.Utils.UILanguagePack::RemovedNullEntries
	String_t* ___RemovedNullEntries_415;
	// System.String Doozy.Engine.Utils.UILanguagePack::Rename
	String_t* ___Rename_416;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameCategory
	String_t* ___RenameCategory_417;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameCategoryDialogMessage
	String_t* ___RenameCategoryDialogMessage_418;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameGameObjectTo
	String_t* ___RenameGameObjectTo_419;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameNodeTo
	String_t* ___RenameNodeTo_420;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenamePrefix
	String_t* ___RenamePrefix_421;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameSoundDatabase
	String_t* ___RenameSoundDatabase_422;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameSoundDatabaseDialogMessage
	String_t* ___RenameSoundDatabaseDialogMessage_423;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameSuffix
	String_t* ___RenameSuffix_424;
	// System.String Doozy.Engine.Utils.UILanguagePack::RenameTo
	String_t* ___RenameTo_425;
	// System.String Doozy.Engine.Utils.UILanguagePack::Reset
	String_t* ___Reset_426;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetAnimationSettings
	String_t* ___ResetAnimationSettings_427;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetClosedPosition
	String_t* ___ResetClosedPosition_428;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetDatabase
	String_t* ___ResetDatabase_429;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetOpenedPosition
	String_t* ___ResetOpenedPosition_430;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetPosition
	String_t* ___ResetPosition_431;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetRoot
	String_t* ___ResetRoot_432;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetTrigger
	String_t* ___ResetTrigger_433;
	// System.String Doozy.Engine.Utils.UILanguagePack::ResetValue
	String_t* ___ResetValue_434;
	// System.String Doozy.Engine.Utils.UILanguagePack::ReversedProgress
	String_t* ___ReversedProgress_435;
	// System.String Doozy.Engine.Utils.UILanguagePack::Right
	String_t* ___Right_436;
	// System.String Doozy.Engine.Utils.UILanguagePack::Rotate
	String_t* ___Rotate_437;
	// System.String Doozy.Engine.Utils.UILanguagePack::RotateBy
	String_t* ___RotateBy_438;
	// System.String Doozy.Engine.Utils.UILanguagePack::RotateFrom
	String_t* ___RotateFrom_439;
	// System.String Doozy.Engine.Utils.UILanguagePack::RotateMode
	String_t* ___RotateMode_440;
	// System.String Doozy.Engine.Utils.UILanguagePack::RotateTo
	String_t* ___RotateTo_441;
	// System.String Doozy.Engine.Utils.UILanguagePack::RuntimeOptions
	String_t* ___RuntimeOptions_442;
	// System.String Doozy.Engine.Utils.UILanguagePack::RuntimePreset
	String_t* ___RuntimePreset_443;
	// System.String Doozy.Engine.Utils.UILanguagePack::Save
	String_t* ___Save_444;
	// System.String Doozy.Engine.Utils.UILanguagePack::SaveAs
	String_t* ___SaveAs_445;
	// System.String Doozy.Engine.Utils.UILanguagePack::SavePreset
	String_t* ___SavePreset_446;
	// System.String Doozy.Engine.Utils.UILanguagePack::Scale
	String_t* ___Scale_447;
	// System.String Doozy.Engine.Utils.UILanguagePack::ScaleBy
	String_t* ___ScaleBy_448;
	// System.String Doozy.Engine.Utils.UILanguagePack::ScaleFrom
	String_t* ___ScaleFrom_449;
	// System.String Doozy.Engine.Utils.UILanguagePack::ScaleTo
	String_t* ___ScaleTo_450;
	// System.String Doozy.Engine.Utils.UILanguagePack::Scene
	String_t* ___Scene_451;
	// System.String Doozy.Engine.Utils.UILanguagePack::SceneActivationDelay
	String_t* ___SceneActivationDelay_452;
	// System.String Doozy.Engine.Utils.UILanguagePack::SceneBuildIndex
	String_t* ___SceneBuildIndex_453;
	// System.String Doozy.Engine.Utils.UILanguagePack::SceneLoad
	String_t* ___SceneLoad_454;
	// System.String Doozy.Engine.Utils.UILanguagePack::SceneName
	String_t* ___SceneName_455;
	// System.String Doozy.Engine.Utils.UILanguagePack::SceneUnload
	String_t* ___SceneUnload_456;
	// System.String Doozy.Engine.Utils.UILanguagePack::ScriptingDefineSymbol
	String_t* ___ScriptingDefineSymbol_457;
	// System.String Doozy.Engine.Utils.UILanguagePack::Search
	String_t* ___Search_458;
	// System.String Doozy.Engine.Utils.UILanguagePack::SearchForCategories
	String_t* ___SearchForCategories_459;
	// System.String Doozy.Engine.Utils.UILanguagePack::SearchForDatabases
	String_t* ___SearchForDatabases_460;
	// System.String Doozy.Engine.Utils.UILanguagePack::SearchForUIPopupLinks
	String_t* ___SearchForUIPopupLinks_461;
	// System.String Doozy.Engine.Utils.UILanguagePack::Seconds
	String_t* ___Seconds_462;
	// System.String Doozy.Engine.Utils.UILanguagePack::SecondsDelay
	String_t* ___SecondsDelay_463;
	// System.String Doozy.Engine.Utils.UILanguagePack::Select
	String_t* ___Select_464;
	// System.String Doozy.Engine.Utils.UILanguagePack::SelectButton
	String_t* ___SelectButton_465;
	// System.String Doozy.Engine.Utils.UILanguagePack::SelectSwipeDirection
	String_t* ___SelectSwipeDirection_466;
	// System.String Doozy.Engine.Utils.UILanguagePack::SelectedLoopAnimation
	String_t* ___SelectedLoopAnimation_467;
	// System.String Doozy.Engine.Utils.UILanguagePack::Send
	String_t* ___Send_468;
	// System.String Doozy.Engine.Utils.UILanguagePack::SendGameEvent
	String_t* ___SendGameEvent_469;
	// System.String Doozy.Engine.Utils.UILanguagePack::SendGameEvents
	String_t* ___SendGameEvents_470;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetActiveNode
	String_t* ___SetActiveNode_471;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetAsSoundName
	String_t* ___SetAsSoundName_472;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetBoolValueTo
	String_t* ___SetBoolValueTo_473;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetFloatValueTo
	String_t* ___SetFloatValueTo_474;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetIntValueTo
	String_t* ___SetIntValueTo_475;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetPosition
	String_t* ___SetPosition_476;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetTargetGameObject
	String_t* ___SetTargetGameObject_477;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetUIButtonToListenFor
	String_t* ___SetUIButtonToListenFor_478;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetUIDrawerToListenFor
	String_t* ___SetUIDrawerToListenFor_479;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetUIViewToListenFor
	String_t* ___SetUIViewToListenFor_480;
	// System.String Doozy.Engine.Utils.UILanguagePack::SetValue
	String_t* ___SetValue_481;
	// System.String Doozy.Engine.Utils.UILanguagePack::Settings
	String_t* ___Settings_482;
	// System.String Doozy.Engine.Utils.UILanguagePack::Show
	String_t* ___Show_483;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowAnimationWillNotWork
	String_t* ___ShowAnimationWillNotWork_484;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowCurveModifier
	String_t* ___ShowCurveModifier_485;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowPopup
	String_t* ___ShowPopup_486;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowProgressor
	String_t* ___ShowProgressor_487;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowView
	String_t* ___ShowView_488;
	// System.String Doozy.Engine.Utils.UILanguagePack::ShowViews
	String_t* ___ShowViews_489;
	// System.String Doozy.Engine.Utils.UILanguagePack::Simulate
	String_t* ___Simulate_490;
	// System.String Doozy.Engine.Utils.UILanguagePack::SocialLinks
	String_t* ___SocialLinks_491;
	// System.String Doozy.Engine.Utils.UILanguagePack::Socket
	String_t* ___Socket_492;
	// System.String Doozy.Engine.Utils.UILanguagePack::Sort
	String_t* ___Sort_493;
	// System.String Doozy.Engine.Utils.UILanguagePack::SortDatabase
	String_t* ___SortDatabase_494;
	// System.String Doozy.Engine.Utils.UILanguagePack::SortingSteps
	String_t* ___SortingSteps_495;
	// System.String Doozy.Engine.Utils.UILanguagePack::Sound
	String_t* ___Sound_496;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundAction
	String_t* ___SoundAction_497;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundDatabases
	String_t* ___SoundDatabases_498;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundName
	String_t* ___SoundName_499;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundNodeName
	String_t* ___SoundNodeName_500;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundSource
	String_t* ___SoundSource_501;
	// System.String Doozy.Engine.Utils.UILanguagePack::Sounds
	String_t* ___Sounds_502;
	// System.String Doozy.Engine.Utils.UILanguagePack::Soundy
	String_t* ___Soundy_503;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundyDatabase
	String_t* ___SoundyDatabase_504;
	// System.String Doozy.Engine.Utils.UILanguagePack::SoundySettings
	String_t* ___SoundySettings_505;
	// System.String Doozy.Engine.Utils.UILanguagePack::SourceName
	String_t* ___SourceName_506;
	// System.String Doozy.Engine.Utils.UILanguagePack::Sources
	String_t* ___Sources_507;
	// System.String Doozy.Engine.Utils.UILanguagePack::SpatialBlend
	String_t* ___SpatialBlend_508;
	// System.String Doozy.Engine.Utils.UILanguagePack::Speed
	String_t* ___Speed_509;
	// System.String Doozy.Engine.Utils.UILanguagePack::StartDelay
	String_t* ___StartDelay_510;
	// System.String Doozy.Engine.Utils.UILanguagePack::StartNodeName
	String_t* ___StartNodeName_511;
	// System.String Doozy.Engine.Utils.UILanguagePack::StopAllSounds
	String_t* ___StopAllSounds_512;
	// System.String Doozy.Engine.Utils.UILanguagePack::StopAnimation
	String_t* ___StopAnimation_513;
	// System.String Doozy.Engine.Utils.UILanguagePack::StopBehavior
	String_t* ___StopBehavior_514;
	// System.String Doozy.Engine.Utils.UILanguagePack::StopSound
	String_t* ___StopSound_515;
	// System.String Doozy.Engine.Utils.UILanguagePack::SubGraph
	String_t* ___SubGraph_516;
	// System.String Doozy.Engine.Utils.UILanguagePack::SubGraphNodeName
	String_t* ___SubGraphNodeName_517;
	// System.String Doozy.Engine.Utils.UILanguagePack::Suffix
	String_t* ___Suffix_518;
	// System.String Doozy.Engine.Utils.UILanguagePack::SupportEmail
	String_t* ___SupportEmail_519;
	// System.String Doozy.Engine.Utils.UILanguagePack::SwipeDirection
	String_t* ___SwipeDirection_520;
	// System.String Doozy.Engine.Utils.UILanguagePack::SwipeLength
	String_t* ___SwipeLength_521;
	// System.String Doozy.Engine.Utils.UILanguagePack::SwipeLengthDescription
	String_t* ___SwipeLengthDescription_522;
	// System.String Doozy.Engine.Utils.UILanguagePack::SwitchBackNodeName
	String_t* ___SwitchBackNodeName_523;
	// System.String Doozy.Engine.Utils.UILanguagePack::Target
	String_t* ___Target_524;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetAnimator
	String_t* ___TargetAnimator_525;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetAnimatorDoesNotHaveAnAnimatorController
	String_t* ___TargetAnimatorDoesNotHaveAnAnimatorController_526;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetAnimatorDoesNotHaveAnyParameters
	String_t* ___TargetAnimatorDoesNotHaveAnyParameters_527;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetCanvas
	String_t* ___TargetCanvas_528;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetFsm
	String_t* ___TargetFsm_529;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetGameObject
	String_t* ___TargetGameObject_530;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetLabel
	String_t* ___TargetLabel_531;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetOrientation
	String_t* ___TargetOrientation_532;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetProgress
	String_t* ___TargetProgress_533;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetTimeScale
	String_t* ___TargetTimeScale_534;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetValue
	String_t* ___TargetValue_535;
	// System.String Doozy.Engine.Utils.UILanguagePack::TargetVariable
	String_t* ___TargetVariable_536;
	// System.String Doozy.Engine.Utils.UILanguagePack::Text
	String_t* ___Text_537;
	// System.String Doozy.Engine.Utils.UILanguagePack::TextLabel
	String_t* ___TextLabel_538;
	// System.String Doozy.Engine.Utils.UILanguagePack::TextMeshPro
	String_t* ___TextMeshPro_539;
	// System.String Doozy.Engine.Utils.UILanguagePack::TextMeshProLabel
	String_t* ___TextMeshProLabel_540;
	// System.String Doozy.Engine.Utils.UILanguagePack::Time
	String_t* ___Time_541;
	// System.String Doozy.Engine.Utils.UILanguagePack::TimeScaleNodeName
	String_t* ___TimeScaleNodeName_542;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleComponentBehaviors
	String_t* ___ToggleComponentBehaviors_543;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleLabel
	String_t* ___ToggleLabel_544;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleOFF
	String_t* ___ToggleOFF_545;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleON
	String_t* ___ToggleON_546;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleProgressor
	String_t* ___ToggleProgressor_547;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleSettings
	String_t* ___ToggleSettings_548;
	// System.String Doozy.Engine.Utils.UILanguagePack::ToggleSupportForThirdPartyPlugins
	String_t* ___ToggleSupportForThirdPartyPlugins_549;
	// System.String Doozy.Engine.Utils.UILanguagePack::TouchySettings
	String_t* ___TouchySettings_550;
	// System.String Doozy.Engine.Utils.UILanguagePack::TriggerAction
	String_t* ___TriggerAction_551;
	// System.String Doozy.Engine.Utils.UILanguagePack::TriggerEventsAfterAnimation
	String_t* ___TriggerEventsAfterAnimation_552;
	// System.String Doozy.Engine.Utils.UILanguagePack::TriggerName
	String_t* ___TriggerName_553;
	// System.String Doozy.Engine.Utils.UILanguagePack::UIDrawerNodeName
	String_t* ___UIDrawerNodeName_554;
	// System.String Doozy.Engine.Utils.UILanguagePack::UINodeNodeName
	String_t* ___UINodeNodeName_555;
	// System.String Doozy.Engine.Utils.UILanguagePack::UIPopupDatabase
	String_t* ___UIPopupDatabase_556;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnityEvent
	String_t* ___UnityEvent_557;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnityEvents
	String_t* ___UnityEvents_558;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnloadSceneNodeName
	String_t* ___UnloadSceneNodeName_559;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnmuteAllSounds
	String_t* ___UnmuteAllSounds_560;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnpauseAllSounds
	String_t* ___UnpauseAllSounds_561;
	// System.String Doozy.Engine.Utils.UILanguagePack::Up
	String_t* ___Up_562;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdateContainer
	String_t* ___UpdateContainer_563;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdateEffect
	String_t* ___UpdateEffect_564;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdateOnHide
	String_t* ___UpdateOnHide_565;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdateOnShow
	String_t* ___UpdateOnShow_566;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdatePopupName
	String_t* ___UpdatePopupName_567;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdatePopupPrefab
	String_t* ___UpdatePopupPrefab_568;
	// System.String Doozy.Engine.Utils.UILanguagePack::UpdateValue
	String_t* ___UpdateValue_569;
	// System.String Doozy.Engine.Utils.UILanguagePack::UseBackButtonDescription
	String_t* ___UseBackButtonDescription_570;
	// System.String Doozy.Engine.Utils.UILanguagePack::UseCustomFromAndTo
	String_t* ___UseCustomFromAndTo_571;
	// System.String Doozy.Engine.Utils.UILanguagePack::UseMultiplier
	String_t* ___UseMultiplier_572;
	// System.String Doozy.Engine.Utils.UILanguagePack::UsefulLinks
	String_t* ___UsefulLinks_573;
	// System.String Doozy.Engine.Utils.UILanguagePack::Value
	String_t* ___Value_574;
	// System.String Doozy.Engine.Utils.UILanguagePack::Version
	String_t* ___Version_575;
	// System.String Doozy.Engine.Utils.UILanguagePack::Vibrato
	String_t* ___Vibrato_576;
	// System.String Doozy.Engine.Utils.UILanguagePack::ViewCategory
	String_t* ___ViewCategory_577;
	// System.String Doozy.Engine.Utils.UILanguagePack::ViewName
	String_t* ___ViewName_578;
	// System.String Doozy.Engine.Utils.UILanguagePack::VirtualButton
	String_t* ___VirtualButton_579;
	// System.String Doozy.Engine.Utils.UILanguagePack::VolumeDb
	String_t* ___VolumeDb_580;
	// System.String Doozy.Engine.Utils.UILanguagePack::WaitFor
	String_t* ___WaitFor_581;
	// System.String Doozy.Engine.Utils.UILanguagePack::WaitForAnimationToFinish
	String_t* ___WaitForAnimationToFinish_582;
	// System.String Doozy.Engine.Utils.UILanguagePack::WaitForSceneToUnload
	String_t* ___WaitForSceneToUnload_583;
	// System.String Doozy.Engine.Utils.UILanguagePack::WaitNodeName
	String_t* ___WaitNodeName_584;
	// System.String Doozy.Engine.Utils.UILanguagePack::Warning
	String_t* ___Warning_585;
	// System.String Doozy.Engine.Utils.UILanguagePack::Weight
	String_t* ___Weight_586;
	// System.String Doozy.Engine.Utils.UILanguagePack::WhenClosed
	String_t* ___WhenClosed_587;
	// System.String Doozy.Engine.Utils.UILanguagePack::WhenUIDrawerIsClosed
	String_t* ___WhenUIDrawerIsClosed_588;
	// System.String Doozy.Engine.Utils.UILanguagePack::WhenUIPopupIsHiddenDisable
	String_t* ___WhenUIPopupIsHiddenDisable_589;
	// System.String Doozy.Engine.Utils.UILanguagePack::WhenUIViewIsHiddenDisable
	String_t* ___WhenUIViewIsHiddenDisable_590;
	// System.String Doozy.Engine.Utils.UILanguagePack::WholeNumbers
	String_t* ___WholeNumbers_591;
	// System.String Doozy.Engine.Utils.UILanguagePack::Width
	String_t* ___Width_592;
	// System.String Doozy.Engine.Utils.UILanguagePack::X
	String_t* ___X_593;
	// System.String Doozy.Engine.Utils.UILanguagePack::Y
	String_t* ___Y_594;
	// System.String Doozy.Engine.Utils.UILanguagePack::Yes
	String_t* ___Yes_595;
	// System.String Doozy.Engine.Utils.UILanguagePack::YouAreResponsibleToUpdateYourCode
	String_t* ___YouAreResponsibleToUpdateYourCode_596;
	// System.String Doozy.Engine.Utils.UILanguagePack::YouCanOnlyAddPrefabsToDatabase
	String_t* ___YouCanOnlyAddPrefabsToDatabase_597;
	// System.String Doozy.Engine.Utils.UILanguagePack::YouTube
	String_t* ___YouTube_598;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingTargetFsmMessage
	String_t* ___MissingTargetFsmMessage_599;
	// System.String Doozy.Engine.Utils.UILanguagePack::SelectListenerToActivateMessage
	String_t* ___SelectListenerToActivateMessage_600;
	// System.String Doozy.Engine.Utils.UILanguagePack::HowToUsePlaymakerEventDispatcherMessage
	String_t* ___HowToUsePlaymakerEventDispatcherMessage_601;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingDrawerNameTitle
	String_t* ___MissingDrawerNameTitle_602;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingDrawerNameMessage
	String_t* ___MissingDrawerNameMessage_603;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingGameEventTitle
	String_t* ___MissingGameEventTitle_604;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingGameEventMessage
	String_t* ___MissingGameEventMessage_605;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingSceneNameTitle
	String_t* ___MissingSceneNameTitle_606;
	// System.String Doozy.Engine.Utils.UILanguagePack::MissingSceneNameMessage
	String_t* ___MissingSceneNameMessage_607;
	// System.String Doozy.Engine.Utils.UILanguagePack::WrongSceneBuildIndexTitle
	String_t* ___WrongSceneBuildIndexTitle_608;
	// System.String Doozy.Engine.Utils.UILanguagePack::WrongSceneBuildIndexMessage
	String_t* ___WrongSceneBuildIndexMessage_609;
	// System.String Doozy.Engine.Utils.UILanguagePack::DoubleClickNodeToOpenSubGraphMessage
	String_t* ___DoubleClickNodeToOpenSubGraphMessage_610;
	// System.String Doozy.Engine.Utils.UILanguagePack::DuplicateNodeMessage
	String_t* ___DuplicateNodeMessage_611;
	// System.String Doozy.Engine.Utils.UILanguagePack::DuplicateNodeTitle
	String_t* ___DuplicateNodeTitle_612;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoGraphReferencedMessage
	String_t* ___NoGraphReferencedMessage_613;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoGraphReferencedTitle
	String_t* ___NoGraphReferencedTitle_614;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSourceConnectedMessage
	String_t* ___NoSourceConnectedMessage_615;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSourceConnectedTitle
	String_t* ___NoSourceConnectedTitle_616;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSubGraphReferencedMessage
	String_t* ___NoSubGraphReferencedMessage_617;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoSubGraphReferencedTitle
	String_t* ___NoSubGraphReferencedTitle_618;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoTargetConnectedMessage
	String_t* ___NoTargetConnectedMessage_619;
	// System.String Doozy.Engine.Utils.UILanguagePack::NoTargetConnectedTitle
	String_t* ___NoTargetConnectedTitle_620;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotConnectedMessage
	String_t* ___NotConnectedMessage_621;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotConnectedTitle
	String_t* ___NotConnectedTitle_622;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotListeningForAnyGameEventMessage
	String_t* ___NotListeningForAnyGameEventMessage_623;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotListeningForAnyGameEventTitle
	String_t* ___NotListeningForAnyGameEventTitle_624;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotSendingAnyGameEventMessage
	String_t* ___NotSendingAnyGameEventMessage_625;
	// System.String Doozy.Engine.Utils.UILanguagePack::NotSendingAnyGameEventTitle
	String_t* ___NotSendingAnyGameEventTitle_626;
	// System.String Doozy.Engine.Utils.UILanguagePack::ProgressTargetAnimatorParameterInfo
	String_t* ___ProgressTargetAnimatorParameterInfo_627;
	// System.String Doozy.Engine.Utils.UILanguagePack::ReferencedGraphIsNotSubGraphMessage
	String_t* ___ReferencedGraphIsNotSubGraphMessage_628;
	// System.String Doozy.Engine.Utils.UILanguagePack::ReferencedGraphIsNotSubGraphTitle
	String_t* ___ReferencedGraphIsNotSubGraphTitle_629;
	// System.String Doozy.Engine.Utils.UILanguagePack::ReferencedGraphIsSubGraphMessage
	String_t* ___ReferencedGraphIsSubGraphMessage_630;
	// System.String Doozy.Engine.Utils.UILanguagePack::ReferencedGraphIsSubGraphTitle
	String_t* ___ReferencedGraphIsSubGraphTitle_631;
	// System.String Doozy.Engine.Utils.UILanguagePack::SomeProgressTargetsGetUpdatedOnlyInPlayMode
	String_t* ___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632;
	// System.String Doozy.Engine.Utils.UILanguagePack::SupportForMasterAudioNotEnabled
	String_t* ___SupportForMasterAudioNotEnabled_633;
	// System.String Doozy.Engine.Utils.UILanguagePack::SupportForPlaymakerNotEnabled
	String_t* ___SupportForPlaymakerNotEnabled_634;
	// System.String Doozy.Engine.Utils.UILanguagePack::SupportForTextMeshProNotEnabled
	String_t* ___SupportForTextMeshProNotEnabled_635;
	// System.String Doozy.Engine.Utils.UILanguagePack::ThisClassShouldBeExtended
	String_t* ___ThisClassShouldBeExtended_636;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnnamedNodeMessage
	String_t* ___UnnamedNodeMessage_637;
	// System.String Doozy.Engine.Utils.UILanguagePack::UnnamedNodeTitle
	String_t* ___UnnamedNodeTitle_638;

public:
	inline static int32_t get_offset_of_TargetLanguage_9() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetLanguage_9)); }
	inline int32_t get_TargetLanguage_9() const { return ___TargetLanguage_9; }
	inline int32_t* get_address_of_TargetLanguage_9() { return &___TargetLanguage_9; }
	inline void set_TargetLanguage_9(int32_t value)
	{
		___TargetLanguage_9 = value;
	}

	inline static int32_t get_offset_of_Action_10() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Action_10)); }
	inline String_t* get_Action_10() const { return ___Action_10; }
	inline String_t** get_address_of_Action_10() { return &___Action_10; }
	inline void set_Action_10(String_t* value)
	{
		___Action_10 = value;
		Il2CppCodeGenWriteBarrier((&___Action_10), value);
	}

	inline static int32_t get_offset_of_Actions_11() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Actions_11)); }
	inline String_t* get_Actions_11() const { return ___Actions_11; }
	inline String_t** get_address_of_Actions_11() { return &___Actions_11; }
	inline void set_Actions_11(String_t* value)
	{
		___Actions_11 = value;
		Il2CppCodeGenWriteBarrier((&___Actions_11), value);
	}

	inline static int32_t get_offset_of_ActivateLoadedScenesNodeName_12() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ActivateLoadedScenesNodeName_12)); }
	inline String_t* get_ActivateLoadedScenesNodeName_12() const { return ___ActivateLoadedScenesNodeName_12; }
	inline String_t** get_address_of_ActivateLoadedScenesNodeName_12() { return &___ActivateLoadedScenesNodeName_12; }
	inline void set_ActivateLoadedScenesNodeName_12(String_t* value)
	{
		___ActivateLoadedScenesNodeName_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActivateLoadedScenesNodeName_12), value);
	}

	inline static int32_t get_offset_of_ActiveNode_13() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ActiveNode_13)); }
	inline String_t* get_ActiveNode_13() const { return ___ActiveNode_13; }
	inline String_t** get_address_of_ActiveNode_13() { return &___ActiveNode_13; }
	inline void set_ActiveNode_13(String_t* value)
	{
		___ActiveNode_13 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveNode_13), value);
	}

	inline static int32_t get_offset_of_ActiveSceneChange_14() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ActiveSceneChange_14)); }
	inline String_t* get_ActiveSceneChange_14() const { return ___ActiveSceneChange_14; }
	inline String_t** get_address_of_ActiveSceneChange_14() { return &___ActiveSceneChange_14; }
	inline void set_ActiveSceneChange_14(String_t* value)
	{
		___ActiveSceneChange_14 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveSceneChange_14), value);
	}

	inline static int32_t get_offset_of_AddCategory_15() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddCategory_15)); }
	inline String_t* get_AddCategory_15() const { return ___AddCategory_15; }
	inline String_t** get_address_of_AddCategory_15() { return &___AddCategory_15; }
	inline void set_AddCategory_15(String_t* value)
	{
		___AddCategory_15 = value;
		Il2CppCodeGenWriteBarrier((&___AddCategory_15), value);
	}

	inline static int32_t get_offset_of_AddItem_16() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddItem_16)); }
	inline String_t* get_AddItem_16() const { return ___AddItem_16; }
	inline String_t** get_address_of_AddItem_16() { return &___AddItem_16; }
	inline void set_AddItem_16(String_t* value)
	{
		___AddItem_16 = value;
		Il2CppCodeGenWriteBarrier((&___AddItem_16), value);
	}

	inline static int32_t get_offset_of_AddSounds_17() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddSounds_17)); }
	inline String_t* get_AddSounds_17() const { return ___AddSounds_17; }
	inline String_t** get_address_of_AddSounds_17() { return &___AddSounds_17; }
	inline void set_AddSounds_17(String_t* value)
	{
		___AddSounds_17 = value;
		Il2CppCodeGenWriteBarrier((&___AddSounds_17), value);
	}

	inline static int32_t get_offset_of_AddSource_18() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddSource_18)); }
	inline String_t* get_AddSource_18() const { return ___AddSource_18; }
	inline String_t** get_address_of_AddSource_18() { return &___AddSource_18; }
	inline void set_AddSource_18(String_t* value)
	{
		___AddSource_18 = value;
		Il2CppCodeGenWriteBarrier((&___AddSource_18), value);
	}

	inline static int32_t get_offset_of_AddSymbolToAllBuildTargetGroups_19() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddSymbolToAllBuildTargetGroups_19)); }
	inline String_t* get_AddSymbolToAllBuildTargetGroups_19() const { return ___AddSymbolToAllBuildTargetGroups_19; }
	inline String_t** get_address_of_AddSymbolToAllBuildTargetGroups_19() { return &___AddSymbolToAllBuildTargetGroups_19; }
	inline void set_AddSymbolToAllBuildTargetGroups_19(String_t* value)
	{
		___AddSymbolToAllBuildTargetGroups_19 = value;
		Il2CppCodeGenWriteBarrier((&___AddSymbolToAllBuildTargetGroups_19), value);
	}

	inline static int32_t get_offset_of_AddToPopupQueue_20() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AddToPopupQueue_20)); }
	inline String_t* get_AddToPopupQueue_20() const { return ___AddToPopupQueue_20; }
	inline String_t** get_address_of_AddToPopupQueue_20() { return &___AddToPopupQueue_20; }
	inline void set_AddToPopupQueue_20(String_t* value)
	{
		___AddToPopupQueue_20 = value;
		Il2CppCodeGenWriteBarrier((&___AddToPopupQueue_20), value);
	}

	inline static int32_t get_offset_of_After_21() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___After_21)); }
	inline String_t* get_After_21() const { return ___After_21; }
	inline String_t** get_address_of_After_21() { return &___After_21; }
	inline void set_After_21(String_t* value)
	{
		___After_21 = value;
		Il2CppCodeGenWriteBarrier((&___After_21), value);
	}

	inline static int32_t get_offset_of_AllSoundsBeforePlaying_22() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AllSoundsBeforePlaying_22)); }
	inline String_t* get_AllSoundsBeforePlaying_22() const { return ___AllSoundsBeforePlaying_22; }
	inline String_t** get_address_of_AllSoundsBeforePlaying_22() { return &___AllSoundsBeforePlaying_22; }
	inline void set_AllSoundsBeforePlaying_22(String_t* value)
	{
		___AllSoundsBeforePlaying_22 = value;
		Il2CppCodeGenWriteBarrier((&___AllSoundsBeforePlaying_22), value);
	}

	inline static int32_t get_offset_of_AllowMultipleClicks_23() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AllowMultipleClicks_23)); }
	inline String_t* get_AllowMultipleClicks_23() const { return ___AllowMultipleClicks_23; }
	inline String_t** get_address_of_AllowMultipleClicks_23() { return &___AllowMultipleClicks_23; }
	inline void set_AllowMultipleClicks_23(String_t* value)
	{
		___AllowMultipleClicks_23 = value;
		Il2CppCodeGenWriteBarrier((&___AllowMultipleClicks_23), value);
	}

	inline static int32_t get_offset_of_AllowSceneActivation_24() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AllowSceneActivation_24)); }
	inline String_t* get_AllowSceneActivation_24() const { return ___AllowSceneActivation_24; }
	inline String_t** get_address_of_AllowSceneActivation_24() { return &___AllowSceneActivation_24; }
	inline void set_AllowSceneActivation_24(String_t* value)
	{
		___AllowSceneActivation_24 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSceneActivation_24), value);
	}

	inline static int32_t get_offset_of_AlternateInput_25() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AlternateInput_25)); }
	inline String_t* get_AlternateInput_25() const { return ___AlternateInput_25; }
	inline String_t** get_address_of_AlternateInput_25() { return &___AlternateInput_25; }
	inline void set_AlternateInput_25(String_t* value)
	{
		___AlternateInput_25 = value;
		Il2CppCodeGenWriteBarrier((&___AlternateInput_25), value);
	}

	inline static int32_t get_offset_of_AlternateKeyCode_26() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AlternateKeyCode_26)); }
	inline String_t* get_AlternateKeyCode_26() const { return ___AlternateKeyCode_26; }
	inline String_t** get_address_of_AlternateKeyCode_26() { return &___AlternateKeyCode_26; }
	inline void set_AlternateKeyCode_26(String_t* value)
	{
		___AlternateKeyCode_26 = value;
		Il2CppCodeGenWriteBarrier((&___AlternateKeyCode_26), value);
	}

	inline static int32_t get_offset_of_AlternateVirtualButton_27() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AlternateVirtualButton_27)); }
	inline String_t* get_AlternateVirtualButton_27() const { return ___AlternateVirtualButton_27; }
	inline String_t** get_address_of_AlternateVirtualButton_27() { return &___AlternateVirtualButton_27; }
	inline void set_AlternateVirtualButton_27(String_t* value)
	{
		___AlternateVirtualButton_27 = value;
		Il2CppCodeGenWriteBarrier((&___AlternateVirtualButton_27), value);
	}

	inline static int32_t get_offset_of_AnimateAllUIViewsWithSameCategoryAndName_28() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnimateAllUIViewsWithSameCategoryAndName_28)); }
	inline String_t* get_AnimateAllUIViewsWithSameCategoryAndName_28() const { return ___AnimateAllUIViewsWithSameCategoryAndName_28; }
	inline String_t** get_address_of_AnimateAllUIViewsWithSameCategoryAndName_28() { return &___AnimateAllUIViewsWithSameCategoryAndName_28; }
	inline void set_AnimateAllUIViewsWithSameCategoryAndName_28(String_t* value)
	{
		___AnimateAllUIViewsWithSameCategoryAndName_28 = value;
		Il2CppCodeGenWriteBarrier((&___AnimateAllUIViewsWithSameCategoryAndName_28), value);
	}

	inline static int32_t get_offset_of_AnimateValue_29() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnimateValue_29)); }
	inline String_t* get_AnimateValue_29() const { return ___AnimateValue_29; }
	inline String_t** get_address_of_AnimateValue_29() { return &___AnimateValue_29; }
	inline void set_AnimateValue_29(String_t* value)
	{
		___AnimateValue_29 = value;
		Il2CppCodeGenWriteBarrier((&___AnimateValue_29), value);
	}

	inline static int32_t get_offset_of_Animation_30() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Animation_30)); }
	inline String_t* get_Animation_30() const { return ___Animation_30; }
	inline String_t** get_address_of_Animation_30() { return &___Animation_30; }
	inline void set_Animation_30(String_t* value)
	{
		___Animation_30 = value;
		Il2CppCodeGenWriteBarrier((&___Animation_30), value);
	}

	inline static int32_t get_offset_of_AnimationCurve_31() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnimationCurve_31)); }
	inline String_t* get_AnimationCurve_31() const { return ___AnimationCurve_31; }
	inline String_t** get_address_of_AnimationCurve_31() { return &___AnimationCurve_31; }
	inline void set_AnimationCurve_31(String_t* value)
	{
		___AnimationCurve_31 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_31), value);
	}

	inline static int32_t get_offset_of_AnimationType_32() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnimationType_32)); }
	inline String_t* get_AnimationType_32() const { return ___AnimationType_32; }
	inline String_t** get_address_of_AnimationType_32() { return &___AnimationType_32; }
	inline void set_AnimationType_32(String_t* value)
	{
		___AnimationType_32 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationType_32), value);
	}

	inline static int32_t get_offset_of_Animator_33() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Animator_33)); }
	inline String_t* get_Animator_33() const { return ___Animator_33; }
	inline String_t** get_address_of_Animator_33() { return &___Animator_33; }
	inline void set_Animator_33(String_t* value)
	{
		___Animator_33 = value;
		Il2CppCodeGenWriteBarrier((&___Animator_33), value);
	}

	inline static int32_t get_offset_of_AnimatorEvents_34() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnimatorEvents_34)); }
	inline String_t* get_AnimatorEvents_34() const { return ___AnimatorEvents_34; }
	inline String_t** get_address_of_AnimatorEvents_34() { return &___AnimatorEvents_34; }
	inline void set_AnimatorEvents_34(String_t* value)
	{
		___AnimatorEvents_34 = value;
		Il2CppCodeGenWriteBarrier((&___AnimatorEvents_34), value);
	}

	inline static int32_t get_offset_of_Animators_35() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Animators_35)); }
	inline String_t* get_Animators_35() const { return ___Animators_35; }
	inline String_t** get_address_of_Animators_35() { return &___Animators_35; }
	inline void set_Animators_35(String_t* value)
	{
		___Animators_35 = value;
		Il2CppCodeGenWriteBarrier((&___Animators_35), value);
	}

	inline static int32_t get_offset_of_AnotherEntryExists_36() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnotherEntryExists_36)); }
	inline String_t* get_AnotherEntryExists_36() const { return ___AnotherEntryExists_36; }
	inline String_t** get_address_of_AnotherEntryExists_36() { return &___AnotherEntryExists_36; }
	inline void set_AnotherEntryExists_36(String_t* value)
	{
		___AnotherEntryExists_36 = value;
		Il2CppCodeGenWriteBarrier((&___AnotherEntryExists_36), value);
	}

	inline static int32_t get_offset_of_AnyGameEvent_37() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyGameEvent_37)); }
	inline String_t* get_AnyGameEvent_37() const { return ___AnyGameEvent_37; }
	inline String_t** get_address_of_AnyGameEvent_37() { return &___AnyGameEvent_37; }
	inline void set_AnyGameEvent_37(String_t* value)
	{
		___AnyGameEvent_37 = value;
		Il2CppCodeGenWriteBarrier((&___AnyGameEvent_37), value);
	}

	inline static int32_t get_offset_of_AnyGameEventWillTriggerThisListener_38() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyGameEventWillTriggerThisListener_38)); }
	inline String_t* get_AnyGameEventWillTriggerThisListener_38() const { return ___AnyGameEventWillTriggerThisListener_38; }
	inline String_t** get_address_of_AnyGameEventWillTriggerThisListener_38() { return &___AnyGameEventWillTriggerThisListener_38; }
	inline void set_AnyGameEventWillTriggerThisListener_38(String_t* value)
	{
		___AnyGameEventWillTriggerThisListener_38 = value;
		Il2CppCodeGenWriteBarrier((&___AnyGameEventWillTriggerThisListener_38), value);
	}

	inline static int32_t get_offset_of_AnyScene_39() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyScene_39)); }
	inline String_t* get_AnyScene_39() const { return ___AnyScene_39; }
	inline String_t** get_address_of_AnyScene_39() { return &___AnyScene_39; }
	inline void set_AnyScene_39(String_t* value)
	{
		___AnyScene_39 = value;
		Il2CppCodeGenWriteBarrier((&___AnyScene_39), value);
	}

	inline static int32_t get_offset_of_AnyUIButtonWillTriggerThisListener_40() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyUIButtonWillTriggerThisListener_40)); }
	inline String_t* get_AnyUIButtonWillTriggerThisListener_40() const { return ___AnyUIButtonWillTriggerThisListener_40; }
	inline String_t** get_address_of_AnyUIButtonWillTriggerThisListener_40() { return &___AnyUIButtonWillTriggerThisListener_40; }
	inline void set_AnyUIButtonWillTriggerThisListener_40(String_t* value)
	{
		___AnyUIButtonWillTriggerThisListener_40 = value;
		Il2CppCodeGenWriteBarrier((&___AnyUIButtonWillTriggerThisListener_40), value);
	}

	inline static int32_t get_offset_of_AnyUIDrawerWillTriggerThisListener_41() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyUIDrawerWillTriggerThisListener_41)); }
	inline String_t* get_AnyUIDrawerWillTriggerThisListener_41() const { return ___AnyUIDrawerWillTriggerThisListener_41; }
	inline String_t** get_address_of_AnyUIDrawerWillTriggerThisListener_41() { return &___AnyUIDrawerWillTriggerThisListener_41; }
	inline void set_AnyUIDrawerWillTriggerThisListener_41(String_t* value)
	{
		___AnyUIDrawerWillTriggerThisListener_41 = value;
		Il2CppCodeGenWriteBarrier((&___AnyUIDrawerWillTriggerThisListener_41), value);
	}

	inline static int32_t get_offset_of_AnyUIViewWillTriggerThisListener_42() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AnyUIViewWillTriggerThisListener_42)); }
	inline String_t* get_AnyUIViewWillTriggerThisListener_42() const { return ___AnyUIViewWillTriggerThisListener_42; }
	inline String_t** get_address_of_AnyUIViewWillTriggerThisListener_42() { return &___AnyUIViewWillTriggerThisListener_42; }
	inline void set_AnyUIViewWillTriggerThisListener_42(String_t* value)
	{
		___AnyUIViewWillTriggerThisListener_42 = value;
		Il2CppCodeGenWriteBarrier((&___AnyUIViewWillTriggerThisListener_42), value);
	}

	inline static int32_t get_offset_of_ApplicationQuit_43() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ApplicationQuit_43)); }
	inline String_t* get_ApplicationQuit_43() const { return ___ApplicationQuit_43; }
	inline String_t** get_address_of_ApplicationQuit_43() { return &___ApplicationQuit_43; }
	inline void set_ApplicationQuit_43(String_t* value)
	{
		___ApplicationQuit_43 = value;
		Il2CppCodeGenWriteBarrier((&___ApplicationQuit_43), value);
	}

	inline static int32_t get_offset_of_AreYouSureConvertToGraph_44() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureConvertToGraph_44)); }
	inline String_t* get_AreYouSureConvertToGraph_44() const { return ___AreYouSureConvertToGraph_44; }
	inline String_t** get_address_of_AreYouSureConvertToGraph_44() { return &___AreYouSureConvertToGraph_44; }
	inline void set_AreYouSureConvertToGraph_44(String_t* value)
	{
		___AreYouSureConvertToGraph_44 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureConvertToGraph_44), value);
	}

	inline static int32_t get_offset_of_AreYouSureConvertToSubGraph_45() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureConvertToSubGraph_45)); }
	inline String_t* get_AreYouSureConvertToSubGraph_45() const { return ___AreYouSureConvertToSubGraph_45; }
	inline String_t** get_address_of_AreYouSureConvertToSubGraph_45() { return &___AreYouSureConvertToSubGraph_45; }
	inline void set_AreYouSureConvertToSubGraph_45(String_t* value)
	{
		___AreYouSureConvertToSubGraph_45 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureConvertToSubGraph_45), value);
	}

	inline static int32_t get_offset_of_AreYouSureYouWantToDeleteDatabase_46() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureYouWantToDeleteDatabase_46)); }
	inline String_t* get_AreYouSureYouWantToDeleteDatabase_46() const { return ___AreYouSureYouWantToDeleteDatabase_46; }
	inline String_t** get_address_of_AreYouSureYouWantToDeleteDatabase_46() { return &___AreYouSureYouWantToDeleteDatabase_46; }
	inline void set_AreYouSureYouWantToDeleteDatabase_46(String_t* value)
	{
		___AreYouSureYouWantToDeleteDatabase_46 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureYouWantToDeleteDatabase_46), value);
	}

	inline static int32_t get_offset_of_AreYouSureYouWantToDeletePopupReference_47() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureYouWantToDeletePopupReference_47)); }
	inline String_t* get_AreYouSureYouWantToDeletePopupReference_47() const { return ___AreYouSureYouWantToDeletePopupReference_47; }
	inline String_t** get_address_of_AreYouSureYouWantToDeletePopupReference_47() { return &___AreYouSureYouWantToDeletePopupReference_47; }
	inline void set_AreYouSureYouWantToDeletePopupReference_47(String_t* value)
	{
		___AreYouSureYouWantToDeletePopupReference_47 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureYouWantToDeletePopupReference_47), value);
	}

	inline static int32_t get_offset_of_AreYouSureYouWantToRemoveCategory_48() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureYouWantToRemoveCategory_48)); }
	inline String_t* get_AreYouSureYouWantToRemoveCategory_48() const { return ___AreYouSureYouWantToRemoveCategory_48; }
	inline String_t** get_address_of_AreYouSureYouWantToRemoveCategory_48() { return &___AreYouSureYouWantToRemoveCategory_48; }
	inline void set_AreYouSureYouWantToRemoveCategory_48(String_t* value)
	{
		___AreYouSureYouWantToRemoveCategory_48 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureYouWantToRemoveCategory_48), value);
	}

	inline static int32_t get_offset_of_AreYouSureYouWantToRemoveTheEntry_49() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureYouWantToRemoveTheEntry_49)); }
	inline String_t* get_AreYouSureYouWantToRemoveTheEntry_49() const { return ___AreYouSureYouWantToRemoveTheEntry_49; }
	inline String_t** get_address_of_AreYouSureYouWantToRemoveTheEntry_49() { return &___AreYouSureYouWantToRemoveTheEntry_49; }
	inline void set_AreYouSureYouWantToRemoveTheEntry_49(String_t* value)
	{
		___AreYouSureYouWantToRemoveTheEntry_49 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureYouWantToRemoveTheEntry_49), value);
	}

	inline static int32_t get_offset_of_AreYouSureYouWantToResetDatabase_50() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AreYouSureYouWantToResetDatabase_50)); }
	inline String_t* get_AreYouSureYouWantToResetDatabase_50() const { return ___AreYouSureYouWantToResetDatabase_50; }
	inline String_t** get_address_of_AreYouSureYouWantToResetDatabase_50() { return &___AreYouSureYouWantToResetDatabase_50; }
	inline void set_AreYouSureYouWantToResetDatabase_50(String_t* value)
	{
		___AreYouSureYouWantToResetDatabase_50 = value;
		Il2CppCodeGenWriteBarrier((&___AreYouSureYouWantToResetDatabase_50), value);
	}

	inline static int32_t get_offset_of_Arrow_51() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Arrow_51)); }
	inline String_t* get_Arrow_51() const { return ___Arrow_51; }
	inline String_t** get_address_of_Arrow_51() { return &___Arrow_51; }
	inline void set_Arrow_51(String_t* value)
	{
		___Arrow_51 = value;
		Il2CppCodeGenWriteBarrier((&___Arrow_51), value);
	}

	inline static int32_t get_offset_of_ArrowComponents_52() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ArrowComponents_52)); }
	inline String_t* get_ArrowComponents_52() const { return ___ArrowComponents_52; }
	inline String_t** get_address_of_ArrowComponents_52() { return &___ArrowComponents_52; }
	inline void set_ArrowComponents_52(String_t* value)
	{
		___ArrowComponents_52 = value;
		Il2CppCodeGenWriteBarrier((&___ArrowComponents_52), value);
	}

	inline static int32_t get_offset_of_AtFinish_53() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AtFinish_53)); }
	inline String_t* get_AtFinish_53() const { return ___AtFinish_53; }
	inline String_t** get_address_of_AtFinish_53() { return &___AtFinish_53; }
	inline void set_AtFinish_53(String_t* value)
	{
		___AtFinish_53 = value;
		Il2CppCodeGenWriteBarrier((&___AtFinish_53), value);
	}

	inline static int32_t get_offset_of_AtStart_54() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AtStart_54)); }
	inline String_t* get_AtStart_54() const { return ___AtStart_54; }
	inline String_t** get_address_of_AtStart_54() { return &___AtStart_54; }
	inline void set_AtStart_54(String_t* value)
	{
		___AtStart_54 = value;
		Il2CppCodeGenWriteBarrier((&___AtStart_54), value);
	}

	inline static int32_t get_offset_of_AudioClip_55() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AudioClip_55)); }
	inline String_t* get_AudioClip_55() const { return ___AudioClip_55; }
	inline String_t** get_address_of_AudioClip_55() { return &___AudioClip_55; }
	inline void set_AudioClip_55(String_t* value)
	{
		___AudioClip_55 = value;
		Il2CppCodeGenWriteBarrier((&___AudioClip_55), value);
	}

	inline static int32_t get_offset_of_AutoDisableUIInteractionsDescription_56() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoDisableUIInteractionsDescription_56)); }
	inline String_t* get_AutoDisableUIInteractionsDescription_56() const { return ___AutoDisableUIInteractionsDescription_56; }
	inline String_t** get_address_of_AutoDisableUIInteractionsDescription_56() { return &___AutoDisableUIInteractionsDescription_56; }
	inline void set_AutoDisableUIInteractionsDescription_56(String_t* value)
	{
		___AutoDisableUIInteractionsDescription_56 = value;
		Il2CppCodeGenWriteBarrier((&___AutoDisableUIInteractionsDescription_56), value);
	}

	inline static int32_t get_offset_of_AutoHideAfterShow_57() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoHideAfterShow_57)); }
	inline String_t* get_AutoHideAfterShow_57() const { return ___AutoHideAfterShow_57; }
	inline String_t** get_address_of_AutoHideAfterShow_57() { return &___AutoHideAfterShow_57; }
	inline void set_AutoHideAfterShow_57(String_t* value)
	{
		___AutoHideAfterShow_57 = value;
		Il2CppCodeGenWriteBarrier((&___AutoHideAfterShow_57), value);
	}

	inline static int32_t get_offset_of_AutoKillIdleControllers_58() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoKillIdleControllers_58)); }
	inline String_t* get_AutoKillIdleControllers_58() const { return ___AutoKillIdleControllers_58; }
	inline String_t** get_address_of_AutoKillIdleControllers_58() { return &___AutoKillIdleControllers_58; }
	inline void set_AutoKillIdleControllers_58(String_t* value)
	{
		___AutoKillIdleControllers_58 = value;
		Il2CppCodeGenWriteBarrier((&___AutoKillIdleControllers_58), value);
	}

	inline static int32_t get_offset_of_AutoKillIdleControllersDescription_59() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoKillIdleControllersDescription_59)); }
	inline String_t* get_AutoKillIdleControllersDescription_59() const { return ___AutoKillIdleControllersDescription_59; }
	inline String_t** get_address_of_AutoKillIdleControllersDescription_59() { return &___AutoKillIdleControllersDescription_59; }
	inline void set_AutoKillIdleControllersDescription_59(String_t* value)
	{
		___AutoKillIdleControllersDescription_59 = value;
		Il2CppCodeGenWriteBarrier((&___AutoKillIdleControllersDescription_59), value);
	}

	inline static int32_t get_offset_of_AutoResetSequence_60() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoResetSequence_60)); }
	inline String_t* get_AutoResetSequence_60() const { return ___AutoResetSequence_60; }
	inline String_t** get_address_of_AutoResetSequence_60() { return &___AutoResetSequence_60; }
	inline void set_AutoResetSequence_60(String_t* value)
	{
		___AutoResetSequence_60 = value;
		Il2CppCodeGenWriteBarrier((&___AutoResetSequence_60), value);
	}

	inline static int32_t get_offset_of_AutoSelectButtonAfterShow_61() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoSelectButtonAfterShow_61)); }
	inline String_t* get_AutoSelectButtonAfterShow_61() const { return ___AutoSelectButtonAfterShow_61; }
	inline String_t** get_address_of_AutoSelectButtonAfterShow_61() { return &___AutoSelectButtonAfterShow_61; }
	inline void set_AutoSelectButtonAfterShow_61(String_t* value)
	{
		___AutoSelectButtonAfterShow_61 = value;
		Il2CppCodeGenWriteBarrier((&___AutoSelectButtonAfterShow_61), value);
	}

	inline static int32_t get_offset_of_AutoSort_62() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoSort_62)); }
	inline String_t* get_AutoSort_62() const { return ___AutoSort_62; }
	inline String_t** get_address_of_AutoSort_62() { return &___AutoSort_62; }
	inline void set_AutoSort_62(String_t* value)
	{
		___AutoSort_62 = value;
		Il2CppCodeGenWriteBarrier((&___AutoSort_62), value);
	}

	inline static int32_t get_offset_of_AutoStartLoopAnimation_63() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___AutoStartLoopAnimation_63)); }
	inline String_t* get_AutoStartLoopAnimation_63() const { return ___AutoStartLoopAnimation_63; }
	inline String_t** get_address_of_AutoStartLoopAnimation_63() { return &___AutoStartLoopAnimation_63; }
	inline void set_AutoStartLoopAnimation_63(String_t* value)
	{
		___AutoStartLoopAnimation_63 = value;
		Il2CppCodeGenWriteBarrier((&___AutoStartLoopAnimation_63), value);
	}

	inline static int32_t get_offset_of_BackButton_64() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___BackButton_64)); }
	inline String_t* get_BackButton_64() const { return ___BackButton_64; }
	inline String_t** get_address_of_BackButton_64() { return &___BackButton_64; }
	inline void set_BackButton_64(String_t* value)
	{
		___BackButton_64 = value;
		Il2CppCodeGenWriteBarrier((&___BackButton_64), value);
	}

	inline static int32_t get_offset_of_BackButtonNodeName_65() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___BackButtonNodeName_65)); }
	inline String_t* get_BackButtonNodeName_65() const { return ___BackButtonNodeName_65; }
	inline String_t** get_address_of_BackButtonNodeName_65() { return &___BackButtonNodeName_65; }
	inline void set_BackButtonNodeName_65(String_t* value)
	{
		___BackButtonNodeName_65 = value;
		Il2CppCodeGenWriteBarrier((&___BackButtonNodeName_65), value);
	}

	inline static int32_t get_offset_of_Behavior_66() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Behavior_66)); }
	inline String_t* get_Behavior_66() const { return ___Behavior_66; }
	inline String_t** get_address_of_Behavior_66() { return &___Behavior_66; }
	inline void set_Behavior_66(String_t* value)
	{
		___Behavior_66 = value;
		Il2CppCodeGenWriteBarrier((&___Behavior_66), value);
	}

	inline static int32_t get_offset_of_BehaviorAtStart_67() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___BehaviorAtStart_67)); }
	inline String_t* get_BehaviorAtStart_67() const { return ___BehaviorAtStart_67; }
	inline String_t** get_address_of_BehaviorAtStart_67() { return &___BehaviorAtStart_67; }
	inline void set_BehaviorAtStart_67(String_t* value)
	{
		___BehaviorAtStart_67 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviorAtStart_67), value);
	}

	inline static int32_t get_offset_of_Behaviors_68() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Behaviors_68)); }
	inline String_t* get_Behaviors_68() const { return ___Behaviors_68; }
	inline String_t** get_address_of_Behaviors_68() { return &___Behaviors_68; }
	inline void set_Behaviors_68(String_t* value)
	{
		___Behaviors_68 = value;
		Il2CppCodeGenWriteBarrier((&___Behaviors_68), value);
	}

	inline static int32_t get_offset_of_BuildIndex_69() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___BuildIndex_69)); }
	inline String_t* get_BuildIndex_69() const { return ___BuildIndex_69; }
	inline String_t** get_address_of_BuildIndex_69() { return &___BuildIndex_69; }
	inline void set_BuildIndex_69(String_t* value)
	{
		___BuildIndex_69 = value;
		Il2CppCodeGenWriteBarrier((&___BuildIndex_69), value);
	}

	inline static int32_t get_offset_of_ButtonCategory_70() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ButtonCategory_70)); }
	inline String_t* get_ButtonCategory_70() const { return ___ButtonCategory_70; }
	inline String_t** get_address_of_ButtonCategory_70() { return &___ButtonCategory_70; }
	inline void set_ButtonCategory_70(String_t* value)
	{
		___ButtonCategory_70 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCategory_70), value);
	}

	inline static int32_t get_offset_of_ButtonLabel_71() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ButtonLabel_71)); }
	inline String_t* get_ButtonLabel_71() const { return ___ButtonLabel_71; }
	inline String_t** get_address_of_ButtonLabel_71() { return &___ButtonLabel_71; }
	inline void set_ButtonLabel_71(String_t* value)
	{
		___ButtonLabel_71 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonLabel_71), value);
	}

	inline static int32_t get_offset_of_ButtonName_72() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ButtonName_72)); }
	inline String_t* get_ButtonName_72() const { return ___ButtonName_72; }
	inline String_t** get_address_of_ButtonName_72() { return &___ButtonName_72; }
	inline void set_ButtonName_72(String_t* value)
	{
		___ButtonName_72 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonName_72), value);
	}

	inline static int32_t get_offset_of_Buttons_73() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Buttons_73)); }
	inline String_t* get_Buttons_73() const { return ___Buttons_73; }
	inline String_t** get_address_of_Buttons_73() { return &___Buttons_73; }
	inline void set_Buttons_73(String_t* value)
	{
		___Buttons_73 = value;
		Il2CppCodeGenWriteBarrier((&___Buttons_73), value);
	}

	inline static int32_t get_offset_of_Cancel_74() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Cancel_74)); }
	inline String_t* get_Cancel_74() const { return ___Cancel_74; }
	inline String_t** get_address_of_Cancel_74() { return &___Cancel_74; }
	inline void set_Cancel_74(String_t* value)
	{
		___Cancel_74 = value;
		Il2CppCodeGenWriteBarrier((&___Cancel_74), value);
	}

	inline static int32_t get_offset_of_CannotAddEmptyEntry_75() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CannotAddEmptyEntry_75)); }
	inline String_t* get_CannotAddEmptyEntry_75() const { return ___CannotAddEmptyEntry_75; }
	inline String_t** get_address_of_CannotAddEmptyEntry_75() { return &___CannotAddEmptyEntry_75; }
	inline void set_CannotAddEmptyEntry_75(String_t* value)
	{
		___CannotAddEmptyEntry_75 = value;
		Il2CppCodeGenWriteBarrier((&___CannotAddEmptyEntry_75), value);
	}

	inline static int32_t get_offset_of_Canvas_76() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Canvas_76)); }
	inline String_t* get_Canvas_76() const { return ___Canvas_76; }
	inline String_t** get_address_of_Canvas_76() { return &___Canvas_76; }
	inline void set_Canvas_76(String_t* value)
	{
		___Canvas_76 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_76), value);
	}

	inline static int32_t get_offset_of_CanvasName_77() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CanvasName_77)); }
	inline String_t* get_CanvasName_77() const { return ___CanvasName_77; }
	inline String_t** get_address_of_CanvasName_77() { return &___CanvasName_77; }
	inline void set_CanvasName_77(String_t* value)
	{
		___CanvasName_77 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasName_77), value);
	}

	inline static int32_t get_offset_of_Category_78() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Category_78)); }
	inline String_t* get_Category_78() const { return ___Category_78; }
	inline String_t** get_address_of_Category_78() { return &___Category_78; }
	inline void set_Category_78(String_t* value)
	{
		___Category_78 = value;
		Il2CppCodeGenWriteBarrier((&___Category_78), value);
	}

	inline static int32_t get_offset_of_CategoryIsEmpty_79() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CategoryIsEmpty_79)); }
	inline String_t* get_CategoryIsEmpty_79() const { return ___CategoryIsEmpty_79; }
	inline String_t** get_address_of_CategoryIsEmpty_79() { return &___CategoryIsEmpty_79; }
	inline void set_CategoryIsEmpty_79(String_t* value)
	{
		___CategoryIsEmpty_79 = value;
		Il2CppCodeGenWriteBarrier((&___CategoryIsEmpty_79), value);
	}

	inline static int32_t get_offset_of_Center_80() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Center_80)); }
	inline String_t* get_Center_80() const { return ___Center_80; }
	inline String_t** get_address_of_Center_80() { return &___Center_80; }
	inline void set_Center_80(String_t* value)
	{
		___Center_80 = value;
		Il2CppCodeGenWriteBarrier((&___Center_80), value);
	}

	inline static int32_t get_offset_of_CenterSelectedNodes_81() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CenterSelectedNodes_81)); }
	inline String_t* get_CenterSelectedNodes_81() const { return ___CenterSelectedNodes_81; }
	inline String_t** get_address_of_CenterSelectedNodes_81() { return &___CenterSelectedNodes_81; }
	inline void set_CenterSelectedNodes_81(String_t* value)
	{
		___CenterSelectedNodes_81 = value;
		Il2CppCodeGenWriteBarrier((&___CenterSelectedNodes_81), value);
	}

	inline static int32_t get_offset_of_Chance_82() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Chance_82)); }
	inline String_t* get_Chance_82() const { return ___Chance_82; }
	inline String_t** get_address_of_Chance_82() { return &___Chance_82; }
	inline void set_Chance_82(String_t* value)
	{
		___Chance_82 = value;
		Il2CppCodeGenWriteBarrier((&___Chance_82), value);
	}

	inline static int32_t get_offset_of_CheckingForIssues_83() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CheckingForIssues_83)); }
	inline String_t* get_CheckingForIssues_83() const { return ___CheckingForIssues_83; }
	inline String_t** get_address_of_CheckingForIssues_83() { return &___CheckingForIssues_83; }
	inline void set_CheckingForIssues_83(String_t* value)
	{
		___CheckingForIssues_83 = value;
		Il2CppCodeGenWriteBarrier((&___CheckingForIssues_83), value);
	}

	inline static int32_t get_offset_of_Clear_84() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Clear_84)); }
	inline String_t* get_Clear_84() const { return ___Clear_84; }
	inline String_t** get_address_of_Clear_84() { return &___Clear_84; }
	inline void set_Clear_84(String_t* value)
	{
		___Clear_84 = value;
		Il2CppCodeGenWriteBarrier((&___Clear_84), value);
	}

	inline static int32_t get_offset_of_ClearRecentOpenedGraphsList_85() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClearRecentOpenedGraphsList_85)); }
	inline String_t* get_ClearRecentOpenedGraphsList_85() const { return ___ClearRecentOpenedGraphsList_85; }
	inline String_t** get_address_of_ClearRecentOpenedGraphsList_85() { return &___ClearRecentOpenedGraphsList_85; }
	inline void set_ClearRecentOpenedGraphsList_85(String_t* value)
	{
		___ClearRecentOpenedGraphsList_85 = value;
		Il2CppCodeGenWriteBarrier((&___ClearRecentOpenedGraphsList_85), value);
	}

	inline static int32_t get_offset_of_ClearRecentOpenedGraphsListDescription_86() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClearRecentOpenedGraphsListDescription_86)); }
	inline String_t* get_ClearRecentOpenedGraphsListDescription_86() const { return ___ClearRecentOpenedGraphsListDescription_86; }
	inline String_t** get_address_of_ClearRecentOpenedGraphsListDescription_86() { return &___ClearRecentOpenedGraphsListDescription_86; }
	inline void set_ClearRecentOpenedGraphsListDescription_86(String_t* value)
	{
		___ClearRecentOpenedGraphsListDescription_86 = value;
		Il2CppCodeGenWriteBarrier((&___ClearRecentOpenedGraphsListDescription_86), value);
	}

	inline static int32_t get_offset_of_ClearSearch_87() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClearSearch_87)); }
	inline String_t* get_ClearSearch_87() const { return ___ClearSearch_87; }
	inline String_t** get_address_of_ClearSearch_87() { return &___ClearSearch_87; }
	inline void set_ClearSearch_87(String_t* value)
	{
		___ClearSearch_87 = value;
		Il2CppCodeGenWriteBarrier((&___ClearSearch_87), value);
	}

	inline static int32_t get_offset_of_ClickAnywhere_88() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClickAnywhere_88)); }
	inline String_t* get_ClickAnywhere_88() const { return ___ClickAnywhere_88; }
	inline String_t** get_address_of_ClickAnywhere_88() { return &___ClickAnywhere_88; }
	inline void set_ClickAnywhere_88(String_t* value)
	{
		___ClickAnywhere_88 = value;
		Il2CppCodeGenWriteBarrier((&___ClickAnywhere_88), value);
	}

	inline static int32_t get_offset_of_ClickContainer_89() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClickContainer_89)); }
	inline String_t* get_ClickContainer_89() const { return ___ClickContainer_89; }
	inline String_t** get_address_of_ClickContainer_89() { return &___ClickContainer_89; }
	inline void set_ClickContainer_89(String_t* value)
	{
		___ClickContainer_89 = value;
		Il2CppCodeGenWriteBarrier((&___ClickContainer_89), value);
	}

	inline static int32_t get_offset_of_ClickMode_90() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClickMode_90)); }
	inline String_t* get_ClickMode_90() const { return ___ClickMode_90; }
	inline String_t** get_address_of_ClickMode_90() { return &___ClickMode_90; }
	inline void set_ClickMode_90(String_t* value)
	{
		___ClickMode_90 = value;
		Il2CppCodeGenWriteBarrier((&___ClickMode_90), value);
	}

	inline static int32_t get_offset_of_ClickOverlay_91() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClickOverlay_91)); }
	inline String_t* get_ClickOverlay_91() const { return ___ClickOverlay_91; }
	inline String_t** get_address_of_ClickOverlay_91() { return &___ClickOverlay_91; }
	inline void set_ClickOverlay_91(String_t* value)
	{
		___ClickOverlay_91 = value;
		Il2CppCodeGenWriteBarrier((&___ClickOverlay_91), value);
	}

	inline static int32_t get_offset_of_Close_92() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Close_92)); }
	inline String_t* get_Close_92() const { return ___Close_92; }
	inline String_t** get_address_of_Close_92() { return &___Close_92; }
	inline void set_Close_92(String_t* value)
	{
		___Close_92 = value;
		Il2CppCodeGenWriteBarrier((&___Close_92), value);
	}

	inline static int32_t get_offset_of_CloseDirection_93() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CloseDirection_93)); }
	inline String_t* get_CloseDirection_93() const { return ___CloseDirection_93; }
	inline String_t** get_address_of_CloseDirection_93() { return &___CloseDirection_93; }
	inline void set_CloseDirection_93(String_t* value)
	{
		___CloseDirection_93 = value;
		Il2CppCodeGenWriteBarrier((&___CloseDirection_93), value);
	}

	inline static int32_t get_offset_of_CloseDrawer_94() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CloseDrawer_94)); }
	inline String_t* get_CloseDrawer_94() const { return ___CloseDrawer_94; }
	inline String_t** get_address_of_CloseDrawer_94() { return &___CloseDrawer_94; }
	inline void set_CloseDrawer_94(String_t* value)
	{
		___CloseDrawer_94 = value;
		Il2CppCodeGenWriteBarrier((&___CloseDrawer_94), value);
	}

	inline static int32_t get_offset_of_CloseSpeed_95() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CloseSpeed_95)); }
	inline String_t* get_CloseSpeed_95() const { return ___CloseSpeed_95; }
	inline String_t** get_address_of_CloseSpeed_95() { return &___CloseSpeed_95; }
	inline void set_CloseSpeed_95(String_t* value)
	{
		___CloseSpeed_95 = value;
		Il2CppCodeGenWriteBarrier((&___CloseSpeed_95), value);
	}

	inline static int32_t get_offset_of_Closed_96() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Closed_96)); }
	inline String_t* get_Closed_96() const { return ___Closed_96; }
	inline String_t** get_address_of_Closed_96() { return &___Closed_96; }
	inline void set_Closed_96(String_t* value)
	{
		___Closed_96 = value;
		Il2CppCodeGenWriteBarrier((&___Closed_96), value);
	}

	inline static int32_t get_offset_of_ClosedPosition_97() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ClosedPosition_97)); }
	inline String_t* get_ClosedPosition_97() const { return ___ClosedPosition_97; }
	inline String_t** get_address_of_ClosedPosition_97() { return &___ClosedPosition_97; }
	inline void set_ClosedPosition_97(String_t* value)
	{
		___ClosedPosition_97 = value;
		Il2CppCodeGenWriteBarrier((&___ClosedPosition_97), value);
	}

	inline static int32_t get_offset_of_Compiling_98() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Compiling_98)); }
	inline String_t* get_Compiling_98() const { return ___Compiling_98; }
	inline String_t** get_address_of_Compiling_98() { return &___Compiling_98; }
	inline void set_Compiling_98(String_t* value)
	{
		___Compiling_98 = value;
		Il2CppCodeGenWriteBarrier((&___Compiling_98), value);
	}

	inline static int32_t get_offset_of_ComponentDisabled_99() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ComponentDisabled_99)); }
	inline String_t* get_ComponentDisabled_99() const { return ___ComponentDisabled_99; }
	inline String_t** get_address_of_ComponentDisabled_99() { return &___ComponentDisabled_99; }
	inline void set_ComponentDisabled_99(String_t* value)
	{
		___ComponentDisabled_99 = value;
		Il2CppCodeGenWriteBarrier((&___ComponentDisabled_99), value);
	}

	inline static int32_t get_offset_of_Connected_100() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Connected_100)); }
	inline String_t* get_Connected_100() const { return ___Connected_100; }
	inline String_t** get_address_of_Connected_100() { return &___Connected_100; }
	inline void set_Connected_100(String_t* value)
	{
		___Connected_100 = value;
		Il2CppCodeGenWriteBarrier((&___Connected_100), value);
	}

	inline static int32_t get_offset_of_ConnectionPoint_101() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ConnectionPoint_101)); }
	inline String_t* get_ConnectionPoint_101() const { return ___ConnectionPoint_101; }
	inline String_t** get_address_of_ConnectionPoint_101() { return &___ConnectionPoint_101; }
	inline void set_ConnectionPoint_101(String_t* value)
	{
		___ConnectionPoint_101 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectionPoint_101), value);
	}

	inline static int32_t get_offset_of_Connections_102() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Connections_102)); }
	inline String_t* get_Connections_102() const { return ___Connections_102; }
	inline String_t** get_address_of_Connections_102() { return &___Connections_102; }
	inline void set_Connections_102(String_t* value)
	{
		___Connections_102 = value;
		Il2CppCodeGenWriteBarrier((&___Connections_102), value);
	}

	inline static int32_t get_offset_of_Container_103() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Container_103)); }
	inline String_t* get_Container_103() const { return ___Container_103; }
	inline String_t** get_address_of_Container_103() { return &___Container_103; }
	inline void set_Container_103(String_t* value)
	{
		___Container_103 = value;
		Il2CppCodeGenWriteBarrier((&___Container_103), value);
	}

	inline static int32_t get_offset_of_ContainerSize_104() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ContainerSize_104)); }
	inline String_t* get_ContainerSize_104() const { return ___ContainerSize_104; }
	inline String_t** get_address_of_ContainerSize_104() { return &___ContainerSize_104; }
	inline void set_ContainerSize_104(String_t* value)
	{
		___ContainerSize_104 = value;
		Il2CppCodeGenWriteBarrier((&___ContainerSize_104), value);
	}

	inline static int32_t get_offset_of_Continue_105() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Continue_105)); }
	inline String_t* get_Continue_105() const { return ___Continue_105; }
	inline String_t** get_address_of_Continue_105() { return &___Continue_105; }
	inline void set_Continue_105(String_t* value)
	{
		___Continue_105 = value;
		Il2CppCodeGenWriteBarrier((&___Continue_105), value);
	}

	inline static int32_t get_offset_of_ControllerIdleKillDuration_106() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ControllerIdleKillDuration_106)); }
	inline String_t* get_ControllerIdleKillDuration_106() const { return ___ControllerIdleKillDuration_106; }
	inline String_t** get_address_of_ControllerIdleKillDuration_106() { return &___ControllerIdleKillDuration_106; }
	inline void set_ControllerIdleKillDuration_106(String_t* value)
	{
		___ControllerIdleKillDuration_106 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerIdleKillDuration_106), value);
	}

	inline static int32_t get_offset_of_ControllerIdleKillDurationDescription_107() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ControllerIdleKillDurationDescription_107)); }
	inline String_t* get_ControllerIdleKillDurationDescription_107() const { return ___ControllerIdleKillDurationDescription_107; }
	inline String_t** get_address_of_ControllerIdleKillDurationDescription_107() { return &___ControllerIdleKillDurationDescription_107; }
	inline void set_ControllerIdleKillDurationDescription_107(String_t* value)
	{
		___ControllerIdleKillDurationDescription_107 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerIdleKillDurationDescription_107), value);
	}

	inline static int32_t get_offset_of_ControllerName_108() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ControllerName_108)); }
	inline String_t* get_ControllerName_108() const { return ___ControllerName_108; }
	inline String_t** get_address_of_ControllerName_108() { return &___ControllerName_108; }
	inline void set_ControllerName_108(String_t* value)
	{
		___ControllerName_108 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerName_108), value);
	}

	inline static int32_t get_offset_of_ConvertToGraph_109() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ConvertToGraph_109)); }
	inline String_t* get_ConvertToGraph_109() const { return ___ConvertToGraph_109; }
	inline String_t** get_address_of_ConvertToGraph_109() { return &___ConvertToGraph_109; }
	inline void set_ConvertToGraph_109(String_t* value)
	{
		___ConvertToGraph_109 = value;
		Il2CppCodeGenWriteBarrier((&___ConvertToGraph_109), value);
	}

	inline static int32_t get_offset_of_ConvertToSubGraph_110() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ConvertToSubGraph_110)); }
	inline String_t* get_ConvertToSubGraph_110() const { return ___ConvertToSubGraph_110; }
	inline String_t** get_address_of_ConvertToSubGraph_110() { return &___ConvertToSubGraph_110; }
	inline void set_ConvertToSubGraph_110(String_t* value)
	{
		___ConvertToSubGraph_110 = value;
		Il2CppCodeGenWriteBarrier((&___ConvertToSubGraph_110), value);
	}

	inline static int32_t get_offset_of_Copy_111() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Copy_111)); }
	inline String_t* get_Copy_111() const { return ___Copy_111; }
	inline String_t** get_address_of_Copy_111() { return &___Copy_111; }
	inline void set_Copy_111(String_t* value)
	{
		___Copy_111 = value;
		Il2CppCodeGenWriteBarrier((&___Copy_111), value);
	}

	inline static int32_t get_offset_of_Create_112() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Create_112)); }
	inline String_t* get_Create_112() const { return ___Create_112; }
	inline String_t** get_address_of_Create_112() { return &___Create_112; }
	inline void set_Create_112(String_t* value)
	{
		___Create_112 = value;
		Il2CppCodeGenWriteBarrier((&___Create_112), value);
	}

	inline static int32_t get_offset_of_CreateAnimation_113() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateAnimation_113)); }
	inline String_t* get_CreateAnimation_113() const { return ___CreateAnimation_113; }
	inline String_t** get_address_of_CreateAnimation_113() { return &___CreateAnimation_113; }
	inline void set_CreateAnimation_113(String_t* value)
	{
		___CreateAnimation_113 = value;
		Il2CppCodeGenWriteBarrier((&___CreateAnimation_113), value);
	}

	inline static int32_t get_offset_of_CreateGraph_114() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateGraph_114)); }
	inline String_t* get_CreateGraph_114() const { return ___CreateGraph_114; }
	inline String_t** get_address_of_CreateGraph_114() { return &___CreateGraph_114; }
	inline void set_CreateGraph_114(String_t* value)
	{
		___CreateGraph_114 = value;
		Il2CppCodeGenWriteBarrier((&___CreateGraph_114), value);
	}

	inline static int32_t get_offset_of_CreateNewCategory_115() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateNewCategory_115)); }
	inline String_t* get_CreateNewCategory_115() const { return ___CreateNewCategory_115; }
	inline String_t** get_address_of_CreateNewCategory_115() { return &___CreateNewCategory_115; }
	inline void set_CreateNewCategory_115(String_t* value)
	{
		___CreateNewCategory_115 = value;
		Il2CppCodeGenWriteBarrier((&___CreateNewCategory_115), value);
	}

	inline static int32_t get_offset_of_CreateNewGraph_116() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateNewGraph_116)); }
	inline String_t* get_CreateNewGraph_116() const { return ___CreateNewGraph_116; }
	inline String_t** get_address_of_CreateNewGraph_116() { return &___CreateNewGraph_116; }
	inline void set_CreateNewGraph_116(String_t* value)
	{
		___CreateNewGraph_116 = value;
		Il2CppCodeGenWriteBarrier((&___CreateNewGraph_116), value);
	}

	inline static int32_t get_offset_of_CreateNewGraphAsSubGraph_117() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateNewGraphAsSubGraph_117)); }
	inline String_t* get_CreateNewGraphAsSubGraph_117() const { return ___CreateNewGraphAsSubGraph_117; }
	inline String_t** get_address_of_CreateNewGraphAsSubGraph_117() { return &___CreateNewGraphAsSubGraph_117; }
	inline void set_CreateNewGraphAsSubGraph_117(String_t* value)
	{
		___CreateNewGraphAsSubGraph_117 = value;
		Il2CppCodeGenWriteBarrier((&___CreateNewGraphAsSubGraph_117), value);
	}

	inline static int32_t get_offset_of_CreateNode_118() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateNode_118)); }
	inline String_t* get_CreateNode_118() const { return ___CreateNode_118; }
	inline String_t** get_address_of_CreateNode_118() { return &___CreateNode_118; }
	inline void set_CreateNode_118(String_t* value)
	{
		___CreateNode_118 = value;
		Il2CppCodeGenWriteBarrier((&___CreateNode_118), value);
	}

	inline static int32_t get_offset_of_CreateParentAndCenterPivot_119() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateParentAndCenterPivot_119)); }
	inline String_t* get_CreateParentAndCenterPivot_119() const { return ___CreateParentAndCenterPivot_119; }
	inline String_t** get_address_of_CreateParentAndCenterPivot_119() { return &___CreateParentAndCenterPivot_119; }
	inline void set_CreateParentAndCenterPivot_119(String_t* value)
	{
		___CreateParentAndCenterPivot_119 = value;
		Il2CppCodeGenWriteBarrier((&___CreateParentAndCenterPivot_119), value);
	}

	inline static int32_t get_offset_of_CreateSubGraph_120() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CreateSubGraph_120)); }
	inline String_t* get_CreateSubGraph_120() const { return ___CreateSubGraph_120; }
	inline String_t** get_address_of_CreateSubGraph_120() { return &___CreateSubGraph_120; }
	inline void set_CreateSubGraph_120(String_t* value)
	{
		___CreateSubGraph_120 = value;
		Il2CppCodeGenWriteBarrier((&___CreateSubGraph_120), value);
	}

	inline static int32_t get_offset_of_CurrentTimeScale_121() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CurrentTimeScale_121)); }
	inline String_t* get_CurrentTimeScale_121() const { return ___CurrentTimeScale_121; }
	inline String_t** get_address_of_CurrentTimeScale_121() { return &___CurrentTimeScale_121; }
	inline void set_CurrentTimeScale_121(String_t* value)
	{
		___CurrentTimeScale_121 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentTimeScale_121), value);
	}

	inline static int32_t get_offset_of_Custom_122() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Custom_122)); }
	inline String_t* get_Custom_122() const { return ___Custom_122; }
	inline String_t** get_address_of_Custom_122() { return &___Custom_122; }
	inline void set_Custom_122(String_t* value)
	{
		___Custom_122 = value;
		Il2CppCodeGenWriteBarrier((&___Custom_122), value);
	}

	inline static int32_t get_offset_of_CustomName_123() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomName_123)); }
	inline String_t* get_CustomName_123() const { return ___CustomName_123; }
	inline String_t** get_address_of_CustomName_123() { return &___CustomName_123; }
	inline void set_CustomName_123(String_t* value)
	{
		___CustomName_123 = value;
		Il2CppCodeGenWriteBarrier((&___CustomName_123), value);
	}

	inline static int32_t get_offset_of_CustomPosition_124() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomPosition_124)); }
	inline String_t* get_CustomPosition_124() const { return ___CustomPosition_124; }
	inline String_t** get_address_of_CustomPosition_124() { return &___CustomPosition_124; }
	inline void set_CustomPosition_124(String_t* value)
	{
		___CustomPosition_124 = value;
		Il2CppCodeGenWriteBarrier((&___CustomPosition_124), value);
	}

	inline static int32_t get_offset_of_CustomResetValue_125() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomResetValue_125)); }
	inline String_t* get_CustomResetValue_125() const { return ___CustomResetValue_125; }
	inline String_t** get_address_of_CustomResetValue_125() { return &___CustomResetValue_125; }
	inline void set_CustomResetValue_125(String_t* value)
	{
		___CustomResetValue_125 = value;
		Il2CppCodeGenWriteBarrier((&___CustomResetValue_125), value);
	}

	inline static int32_t get_offset_of_CustomSortingLayer_126() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomSortingLayer_126)); }
	inline String_t* get_CustomSortingLayer_126() const { return ___CustomSortingLayer_126; }
	inline String_t** get_address_of_CustomSortingLayer_126() { return &___CustomSortingLayer_126; }
	inline void set_CustomSortingLayer_126(String_t* value)
	{
		___CustomSortingLayer_126 = value;
		Il2CppCodeGenWriteBarrier((&___CustomSortingLayer_126), value);
	}

	inline static int32_t get_offset_of_CustomSortingOrder_127() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomSortingOrder_127)); }
	inline String_t* get_CustomSortingOrder_127() const { return ___CustomSortingOrder_127; }
	inline String_t** get_address_of_CustomSortingOrder_127() { return &___CustomSortingOrder_127; }
	inline void set_CustomSortingOrder_127(String_t* value)
	{
		___CustomSortingOrder_127 = value;
		Il2CppCodeGenWriteBarrier((&___CustomSortingOrder_127), value);
	}

	inline static int32_t get_offset_of_CustomStartPosition_128() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___CustomStartPosition_128)); }
	inline String_t* get_CustomStartPosition_128() const { return ___CustomStartPosition_128; }
	inline String_t** get_address_of_CustomStartPosition_128() { return &___CustomStartPosition_128; }
	inline void set_CustomStartPosition_128(String_t* value)
	{
		___CustomStartPosition_128 = value;
		Il2CppCodeGenWriteBarrier((&___CustomStartPosition_128), value);
	}

	inline static int32_t get_offset_of_Cut_129() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Cut_129)); }
	inline String_t* get_Cut_129() const { return ___Cut_129; }
	inline String_t** get_address_of_Cut_129() { return &___Cut_129; }
	inline void set_Cut_129(String_t* value)
	{
		___Cut_129 = value;
		Il2CppCodeGenWriteBarrier((&___Cut_129), value);
	}

	inline static int32_t get_offset_of_Database_130() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Database_130)); }
	inline String_t* get_Database_130() const { return ___Database_130; }
	inline String_t** get_address_of_Database_130() { return &___Database_130; }
	inline void set_Database_130(String_t* value)
	{
		___Database_130 = value;
		Il2CppCodeGenWriteBarrier((&___Database_130), value);
	}

	inline static int32_t get_offset_of_DatabaseAlreadyExists_131() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseAlreadyExists_131)); }
	inline String_t* get_DatabaseAlreadyExists_131() const { return ___DatabaseAlreadyExists_131; }
	inline String_t** get_address_of_DatabaseAlreadyExists_131() { return &___DatabaseAlreadyExists_131; }
	inline void set_DatabaseAlreadyExists_131(String_t* value)
	{
		___DatabaseAlreadyExists_131 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseAlreadyExists_131), value);
	}

	inline static int32_t get_offset_of_DatabaseHasBeenReset_132() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseHasBeenReset_132)); }
	inline String_t* get_DatabaseHasBeenReset_132() const { return ___DatabaseHasBeenReset_132; }
	inline String_t** get_address_of_DatabaseHasBeenReset_132() { return &___DatabaseHasBeenReset_132; }
	inline void set_DatabaseHasBeenReset_132(String_t* value)
	{
		___DatabaseHasBeenReset_132 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseHasBeenReset_132), value);
	}

	inline static int32_t get_offset_of_DatabaseIsEmpty_133() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseIsEmpty_133)); }
	inline String_t* get_DatabaseIsEmpty_133() const { return ___DatabaseIsEmpty_133; }
	inline String_t** get_address_of_DatabaseIsEmpty_133() { return &___DatabaseIsEmpty_133; }
	inline void set_DatabaseIsEmpty_133(String_t* value)
	{
		___DatabaseIsEmpty_133 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseIsEmpty_133), value);
	}

	inline static int32_t get_offset_of_DatabaseName_134() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseName_134)); }
	inline String_t* get_DatabaseName_134() const { return ___DatabaseName_134; }
	inline String_t** get_address_of_DatabaseName_134() { return &___DatabaseName_134; }
	inline void set_DatabaseName_134(String_t* value)
	{
		___DatabaseName_134 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseName_134), value);
	}

	inline static int32_t get_offset_of_DatabaseRefreshed_135() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseRefreshed_135)); }
	inline String_t* get_DatabaseRefreshed_135() const { return ___DatabaseRefreshed_135; }
	inline String_t** get_address_of_DatabaseRefreshed_135() { return &___DatabaseRefreshed_135; }
	inline void set_DatabaseRefreshed_135(String_t* value)
	{
		___DatabaseRefreshed_135 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseRefreshed_135), value);
	}

	inline static int32_t get_offset_of_DatabaseSorted_136() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DatabaseSorted_136)); }
	inline String_t* get_DatabaseSorted_136() const { return ___DatabaseSorted_136; }
	inline String_t** get_address_of_DatabaseSorted_136() { return &___DatabaseSorted_136; }
	inline void set_DatabaseSorted_136(String_t* value)
	{
		___DatabaseSorted_136 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseSorted_136), value);
	}

	inline static int32_t get_offset_of_Debug_137() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Debug_137)); }
	inline String_t* get_Debug_137() const { return ___Debug_137; }
	inline String_t** get_address_of_Debug_137() { return &___Debug_137; }
	inline void set_Debug_137(String_t* value)
	{
		___Debug_137 = value;
		Il2CppCodeGenWriteBarrier((&___Debug_137), value);
	}

	inline static int32_t get_offset_of_DebugMode_138() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DebugMode_138)); }
	inline String_t* get_DebugMode_138() const { return ___DebugMode_138; }
	inline String_t** get_address_of_DebugMode_138() { return &___DebugMode_138; }
	inline void set_DebugMode_138(String_t* value)
	{
		___DebugMode_138 = value;
		Il2CppCodeGenWriteBarrier((&___DebugMode_138), value);
	}

	inline static int32_t get_offset_of_DefaultDotAnimationSpeedDescription_139() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DefaultDotAnimationSpeedDescription_139)); }
	inline String_t* get_DefaultDotAnimationSpeedDescription_139() const { return ___DefaultDotAnimationSpeedDescription_139; }
	inline String_t** get_address_of_DefaultDotAnimationSpeedDescription_139() { return &___DefaultDotAnimationSpeedDescription_139; }
	inline void set_DefaultDotAnimationSpeedDescription_139(String_t* value)
	{
		___DefaultDotAnimationSpeedDescription_139 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultDotAnimationSpeedDescription_139), value);
	}

	inline static int32_t get_offset_of_DefaultValues_140() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DefaultValues_140)); }
	inline String_t* get_DefaultValues_140() const { return ___DefaultValues_140; }
	inline String_t** get_address_of_DefaultValues_140() { return &___DefaultValues_140; }
	inline void set_DefaultValues_140(String_t* value)
	{
		___DefaultValues_140 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValues_140), value);
	}

	inline static int32_t get_offset_of_DefaultValuesDescription_141() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DefaultValuesDescription_141)); }
	inline String_t* get_DefaultValuesDescription_141() const { return ___DefaultValuesDescription_141; }
	inline String_t** get_address_of_DefaultValuesDescription_141() { return &___DefaultValuesDescription_141; }
	inline void set_DefaultValuesDescription_141(String_t* value)
	{
		___DefaultValuesDescription_141 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValuesDescription_141), value);
	}

	inline static int32_t get_offset_of_DefaultZoom_142() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DefaultZoom_142)); }
	inline String_t* get_DefaultZoom_142() const { return ___DefaultZoom_142; }
	inline String_t** get_address_of_DefaultZoom_142() { return &___DefaultZoom_142; }
	inline void set_DefaultZoom_142(String_t* value)
	{
		___DefaultZoom_142 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultZoom_142), value);
	}

	inline static int32_t get_offset_of_DefaultZoomDescription_143() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DefaultZoomDescription_143)); }
	inline String_t* get_DefaultZoomDescription_143() const { return ___DefaultZoomDescription_143; }
	inline String_t** get_address_of_DefaultZoomDescription_143() { return &___DefaultZoomDescription_143; }
	inline void set_DefaultZoomDescription_143(String_t* value)
	{
		___DefaultZoomDescription_143 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultZoomDescription_143), value);
	}

	inline static int32_t get_offset_of_Delete_144() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Delete_144)); }
	inline String_t* get_Delete_144() const { return ___Delete_144; }
	inline String_t** get_address_of_Delete_144() { return &___Delete_144; }
	inline void set_Delete_144(String_t* value)
	{
		___Delete_144 = value;
		Il2CppCodeGenWriteBarrier((&___Delete_144), value);
	}

	inline static int32_t get_offset_of_DeleteDatabase_145() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeleteDatabase_145)); }
	inline String_t* get_DeleteDatabase_145() const { return ___DeleteDatabase_145; }
	inline String_t** get_address_of_DeleteDatabase_145() { return &___DeleteDatabase_145; }
	inline void set_DeleteDatabase_145(String_t* value)
	{
		___DeleteDatabase_145 = value;
		Il2CppCodeGenWriteBarrier((&___DeleteDatabase_145), value);
	}

	inline static int32_t get_offset_of_DeletePopupReference_146() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeletePopupReference_146)); }
	inline String_t* get_DeletePopupReference_146() const { return ___DeletePopupReference_146; }
	inline String_t** get_address_of_DeletePopupReference_146() { return &___DeletePopupReference_146; }
	inline void set_DeletePopupReference_146(String_t* value)
	{
		___DeletePopupReference_146 = value;
		Il2CppCodeGenWriteBarrier((&___DeletePopupReference_146), value);
	}

	inline static int32_t get_offset_of_DeletePreset_147() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeletePreset_147)); }
	inline String_t* get_DeletePreset_147() const { return ___DeletePreset_147; }
	inline String_t** get_address_of_DeletePreset_147() { return &___DeletePreset_147; }
	inline void set_DeletePreset_147(String_t* value)
	{
		___DeletePreset_147 = value;
		Il2CppCodeGenWriteBarrier((&___DeletePreset_147), value);
	}

	inline static int32_t get_offset_of_DeletedPopupReference_148() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeletedPopupReference_148)); }
	inline String_t* get_DeletedPopupReference_148() const { return ___DeletedPopupReference_148; }
	inline String_t** get_address_of_DeletedPopupReference_148() { return &___DeletedPopupReference_148; }
	inline void set_DeletedPopupReference_148(String_t* value)
	{
		___DeletedPopupReference_148 = value;
		Il2CppCodeGenWriteBarrier((&___DeletedPopupReference_148), value);
	}

	inline static int32_t get_offset_of_Description_149() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Description_149)); }
	inline String_t* get_Description_149() const { return ___Description_149; }
	inline String_t** get_address_of_Description_149() { return &___Description_149; }
	inline void set_Description_149(String_t* value)
	{
		___Description_149 = value;
		Il2CppCodeGenWriteBarrier((&___Description_149), value);
	}

	inline static int32_t get_offset_of_Deselect_150() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Deselect_150)); }
	inline String_t* get_Deselect_150() const { return ___Deselect_150; }
	inline String_t** get_address_of_Deselect_150() { return &___Deselect_150; }
	inline void set_Deselect_150(String_t* value)
	{
		___Deselect_150 = value;
		Il2CppCodeGenWriteBarrier((&___Deselect_150), value);
	}

	inline static int32_t get_offset_of_DeselectAnyButton_151() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeselectAnyButton_151)); }
	inline String_t* get_DeselectAnyButton_151() const { return ___DeselectAnyButton_151; }
	inline String_t** get_address_of_DeselectAnyButton_151() { return &___DeselectAnyButton_151; }
	inline void set_DeselectAnyButton_151(String_t* value)
	{
		___DeselectAnyButton_151 = value;
		Il2CppCodeGenWriteBarrier((&___DeselectAnyButton_151), value);
	}

	inline static int32_t get_offset_of_DeselectButton_152() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeselectButton_152)); }
	inline String_t* get_DeselectButton_152() const { return ___DeselectButton_152; }
	inline String_t** get_address_of_DeselectButton_152() { return &___DeselectButton_152; }
	inline void set_DeselectButton_152(String_t* value)
	{
		___DeselectButton_152 = value;
		Il2CppCodeGenWriteBarrier((&___DeselectButton_152), value);
	}

	inline static int32_t get_offset_of_DeselectButtonAfterClick_153() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DeselectButtonAfterClick_153)); }
	inline String_t* get_DeselectButtonAfterClick_153() const { return ___DeselectButtonAfterClick_153; }
	inline String_t** get_address_of_DeselectButtonAfterClick_153() { return &___DeselectButtonAfterClick_153; }
	inline void set_DeselectButtonAfterClick_153(String_t* value)
	{
		___DeselectButtonAfterClick_153 = value;
		Il2CppCodeGenWriteBarrier((&___DeselectButtonAfterClick_153), value);
	}

	inline static int32_t get_offset_of_DestroyAfterHide_154() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DestroyAfterHide_154)); }
	inline String_t* get_DestroyAfterHide_154() const { return ___DestroyAfterHide_154; }
	inline String_t** get_address_of_DestroyAfterHide_154() { return &___DestroyAfterHide_154; }
	inline void set_DestroyAfterHide_154(String_t* value)
	{
		___DestroyAfterHide_154 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyAfterHide_154), value);
	}

	inline static int32_t get_offset_of_DetectGestures_155() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DetectGestures_155)); }
	inline String_t* get_DetectGestures_155() const { return ___DetectGestures_155; }
	inline String_t** get_address_of_DetectGestures_155() { return &___DetectGestures_155; }
	inline void set_DetectGestures_155(String_t* value)
	{
		___DetectGestures_155 = value;
		Il2CppCodeGenWriteBarrier((&___DetectGestures_155), value);
	}

	inline static int32_t get_offset_of_Direction_156() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Direction_156)); }
	inline String_t* get_Direction_156() const { return ___Direction_156; }
	inline String_t** get_address_of_Direction_156() { return &___Direction_156; }
	inline void set_Direction_156(String_t* value)
	{
		___Direction_156 = value;
		Il2CppCodeGenWriteBarrier((&___Direction_156), value);
	}

	inline static int32_t get_offset_of_Disable_157() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Disable_157)); }
	inline String_t* get_Disable_157() const { return ___Disable_157; }
	inline String_t** get_address_of_Disable_157() { return &___Disable_157; }
	inline void set_Disable_157(String_t* value)
	{
		___Disable_157 = value;
		Il2CppCodeGenWriteBarrier((&___Disable_157), value);
	}

	inline static int32_t get_offset_of_DisableButtonInterval_158() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisableButtonInterval_158)); }
	inline String_t* get_DisableButtonInterval_158() const { return ___DisableButtonInterval_158; }
	inline String_t** get_address_of_DisableButtonInterval_158() { return &___DisableButtonInterval_158; }
	inline void set_DisableButtonInterval_158(String_t* value)
	{
		___DisableButtonInterval_158 = value;
		Il2CppCodeGenWriteBarrier((&___DisableButtonInterval_158), value);
	}

	inline static int32_t get_offset_of_DisableCanvas_159() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisableCanvas_159)); }
	inline String_t* get_DisableCanvas_159() const { return ___DisableCanvas_159; }
	inline String_t** get_address_of_DisableCanvas_159() { return &___DisableCanvas_159; }
	inline void set_DisableCanvas_159(String_t* value)
	{
		___DisableCanvas_159 = value;
		Il2CppCodeGenWriteBarrier((&___DisableCanvas_159), value);
	}

	inline static int32_t get_offset_of_DisableFunctionality_160() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisableFunctionality_160)); }
	inline String_t* get_DisableFunctionality_160() const { return ___DisableFunctionality_160; }
	inline String_t** get_address_of_DisableFunctionality_160() { return &___DisableFunctionality_160; }
	inline void set_DisableFunctionality_160(String_t* value)
	{
		___DisableFunctionality_160 = value;
		Il2CppCodeGenWriteBarrier((&___DisableFunctionality_160), value);
	}

	inline static int32_t get_offset_of_DisableGameObject_161() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisableGameObject_161)); }
	inline String_t* get_DisableGameObject_161() const { return ___DisableGameObject_161; }
	inline String_t** get_address_of_DisableGameObject_161() { return &___DisableGameObject_161; }
	inline void set_DisableGameObject_161(String_t* value)
	{
		___DisableGameObject_161 = value;
		Il2CppCodeGenWriteBarrier((&___DisableGameObject_161), value);
	}

	inline static int32_t get_offset_of_DisableInterval_162() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisableInterval_162)); }
	inline String_t* get_DisableInterval_162() const { return ___DisableInterval_162; }
	inline String_t** get_address_of_DisableInterval_162() { return &___DisableInterval_162; }
	inline void set_DisableInterval_162(String_t* value)
	{
		___DisableInterval_162 = value;
		Il2CppCodeGenWriteBarrier((&___DisableInterval_162), value);
	}

	inline static int32_t get_offset_of_DisablePlugin_163() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisablePlugin_163)); }
	inline String_t* get_DisablePlugin_163() const { return ___DisablePlugin_163; }
	inline String_t** get_address_of_DisablePlugin_163() { return &___DisablePlugin_163; }
	inline void set_DisablePlugin_163(String_t* value)
	{
		___DisablePlugin_163 = value;
		Il2CppCodeGenWriteBarrier((&___DisablePlugin_163), value);
	}

	inline static int32_t get_offset_of_Disabled_164() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Disabled_164)); }
	inline String_t* get_Disabled_164() const { return ___Disabled_164; }
	inline String_t** get_address_of_Disabled_164() { return &___Disabled_164; }
	inline void set_Disabled_164(String_t* value)
	{
		___Disabled_164 = value;
		Il2CppCodeGenWriteBarrier((&___Disabled_164), value);
	}

	inline static int32_t get_offset_of_Disconnect_165() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Disconnect_165)); }
	inline String_t* get_Disconnect_165() const { return ___Disconnect_165; }
	inline String_t** get_address_of_Disconnect_165() { return &___Disconnect_165; }
	inline void set_Disconnect_165(String_t* value)
	{
		___Disconnect_165 = value;
		Il2CppCodeGenWriteBarrier((&___Disconnect_165), value);
	}

	inline static int32_t get_offset_of_DispatchButtonClicks_166() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DispatchButtonClicks_166)); }
	inline String_t* get_DispatchButtonClicks_166() const { return ___DispatchButtonClicks_166; }
	inline String_t** get_address_of_DispatchButtonClicks_166() { return &___DispatchButtonClicks_166; }
	inline void set_DispatchButtonClicks_166(String_t* value)
	{
		___DispatchButtonClicks_166 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchButtonClicks_166), value);
	}

	inline static int32_t get_offset_of_DispatchGameEvents_167() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DispatchGameEvents_167)); }
	inline String_t* get_DispatchGameEvents_167() const { return ___DispatchGameEvents_167; }
	inline String_t** get_address_of_DispatchGameEvents_167() { return &___DispatchGameEvents_167; }
	inline void set_DispatchGameEvents_167(String_t* value)
	{
		___DispatchGameEvents_167 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchGameEvents_167), value);
	}

	inline static int32_t get_offset_of_DisplayTarget_168() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DisplayTarget_168)); }
	inline String_t* get_DisplayTarget_168() const { return ___DisplayTarget_168; }
	inline String_t** get_address_of_DisplayTarget_168() { return &___DisplayTarget_168; }
	inline void set_DisplayTarget_168(String_t* value)
	{
		___DisplayTarget_168 = value;
		Il2CppCodeGenWriteBarrier((&___DisplayTarget_168), value);
	}

	inline static int32_t get_offset_of_DontDestroyGameObjectOnLoad_169() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DontDestroyGameObjectOnLoad_169)); }
	inline String_t* get_DontDestroyGameObjectOnLoad_169() const { return ___DontDestroyGameObjectOnLoad_169; }
	inline String_t** get_address_of_DontDestroyGameObjectOnLoad_169() { return &___DontDestroyGameObjectOnLoad_169; }
	inline void set_DontDestroyGameObjectOnLoad_169(String_t* value)
	{
		___DontDestroyGameObjectOnLoad_169 = value;
		Il2CppCodeGenWriteBarrier((&___DontDestroyGameObjectOnLoad_169), value);
	}

	inline static int32_t get_offset_of_DotAnimationSpeed_170() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DotAnimationSpeed_170)); }
	inline String_t* get_DotAnimationSpeed_170() const { return ___DotAnimationSpeed_170; }
	inline String_t** get_address_of_DotAnimationSpeed_170() { return &___DotAnimationSpeed_170; }
	inline void set_DotAnimationSpeed_170(String_t* value)
	{
		___DotAnimationSpeed_170 = value;
		Il2CppCodeGenWriteBarrier((&___DotAnimationSpeed_170), value);
	}

	inline static int32_t get_offset_of_Down_171() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Down_171)); }
	inline String_t* get_Down_171() const { return ___Down_171; }
	inline String_t** get_address_of_Down_171() { return &___Down_171; }
	inline void set_Down_171(String_t* value)
	{
		___Down_171 = value;
		Il2CppCodeGenWriteBarrier((&___Down_171), value);
	}

	inline static int32_t get_offset_of_DragDrawer_172() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DragDrawer_172)); }
	inline String_t* get_DragDrawer_172() const { return ___DragDrawer_172; }
	inline String_t** get_address_of_DragDrawer_172() { return &___DragDrawer_172; }
	inline void set_DragDrawer_172(String_t* value)
	{
		___DragDrawer_172 = value;
		Il2CppCodeGenWriteBarrier((&___DragDrawer_172), value);
	}

	inline static int32_t get_offset_of_DrawerName_173() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DrawerName_173)); }
	inline String_t* get_DrawerName_173() const { return ___DrawerName_173; }
	inline String_t** get_address_of_DrawerName_173() { return &___DrawerName_173; }
	inline void set_DrawerName_173(String_t* value)
	{
		___DrawerName_173 = value;
		Il2CppCodeGenWriteBarrier((&___DrawerName_173), value);
	}

	inline static int32_t get_offset_of_DropAudioClipsHere_174() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DropAudioClipsHere_174)); }
	inline String_t* get_DropAudioClipsHere_174() const { return ___DropAudioClipsHere_174; }
	inline String_t** get_address_of_DropAudioClipsHere_174() { return &___DropAudioClipsHere_174; }
	inline void set_DropAudioClipsHere_174(String_t* value)
	{
		___DropAudioClipsHere_174 = value;
		Il2CppCodeGenWriteBarrier((&___DropAudioClipsHere_174), value);
	}

	inline static int32_t get_offset_of_Duration_175() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Duration_175)); }
	inline String_t* get_Duration_175() const { return ___Duration_175; }
	inline String_t** get_address_of_Duration_175() { return &___Duration_175; }
	inline void set_Duration_175(String_t* value)
	{
		___Duration_175 = value;
		Il2CppCodeGenWriteBarrier((&___Duration_175), value);
	}

	inline static int32_t get_offset_of_Ease_176() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Ease_176)); }
	inline String_t* get_Ease_176() const { return ___Ease_176; }
	inline String_t** get_address_of_Ease_176() { return &___Ease_176; }
	inline void set_Ease_176(String_t* value)
	{
		___Ease_176 = value;
		Il2CppCodeGenWriteBarrier((&___Ease_176), value);
	}

	inline static int32_t get_offset_of_EaseType_177() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EaseType_177)); }
	inline String_t* get_EaseType_177() const { return ___EaseType_177; }
	inline String_t** get_address_of_EaseType_177() { return &___EaseType_177; }
	inline void set_EaseType_177(String_t* value)
	{
		___EaseType_177 = value;
		Il2CppCodeGenWriteBarrier((&___EaseType_177), value);
	}

	inline static int32_t get_offset_of_Effect_178() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Effect_178)); }
	inline String_t* get_Effect_178() const { return ___Effect_178; }
	inline String_t** get_address_of_Effect_178() { return &___Effect_178; }
	inline void set_Effect_178(String_t* value)
	{
		___Effect_178 = value;
		Il2CppCodeGenWriteBarrier((&___Effect_178), value);
	}

	inline static int32_t get_offset_of_Elasticity_179() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Elasticity_179)); }
	inline String_t* get_Elasticity_179() const { return ___Elasticity_179; }
	inline String_t** get_address_of_Elasticity_179() { return &___Elasticity_179; }
	inline void set_Elasticity_179(String_t* value)
	{
		___Elasticity_179 = value;
		Il2CppCodeGenWriteBarrier((&___Elasticity_179), value);
	}

	inline static int32_t get_offset_of_Email_180() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Email_180)); }
	inline String_t* get_Email_180() const { return ___Email_180; }
	inline String_t** get_address_of_Email_180() { return &___Email_180; }
	inline void set_Email_180(String_t* value)
	{
		___Email_180 = value;
		Il2CppCodeGenWriteBarrier((&___Email_180), value);
	}

	inline static int32_t get_offset_of_Empty_181() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Empty_181)); }
	inline String_t* get_Empty_181() const { return ___Empty_181; }
	inline String_t** get_address_of_Empty_181() { return &___Empty_181; }
	inline void set_Empty_181(String_t* value)
	{
		___Empty_181 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_181), value);
	}

	inline static int32_t get_offset_of_Enable_182() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Enable_182)); }
	inline String_t* get_Enable_182() const { return ___Enable_182; }
	inline String_t** get_address_of_Enable_182() { return &___Enable_182; }
	inline void set_Enable_182(String_t* value)
	{
		___Enable_182 = value;
		Il2CppCodeGenWriteBarrier((&___Enable_182), value);
	}

	inline static int32_t get_offset_of_EnableFunctionality_183() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnableFunctionality_183)); }
	inline String_t* get_EnableFunctionality_183() const { return ___EnableFunctionality_183; }
	inline String_t** get_address_of_EnableFunctionality_183() { return &___EnableFunctionality_183; }
	inline void set_EnableFunctionality_183(String_t* value)
	{
		___EnableFunctionality_183 = value;
		Il2CppCodeGenWriteBarrier((&___EnableFunctionality_183), value);
	}

	inline static int32_t get_offset_of_EnablePlugin_184() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnablePlugin_184)); }
	inline String_t* get_EnablePlugin_184() const { return ___EnablePlugin_184; }
	inline String_t** get_address_of_EnablePlugin_184() { return &___EnablePlugin_184; }
	inline void set_EnablePlugin_184(String_t* value)
	{
		___EnablePlugin_184 = value;
		Il2CppCodeGenWriteBarrier((&___EnablePlugin_184), value);
	}

	inline static int32_t get_offset_of_EnableSupportForMasterAudio_185() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnableSupportForMasterAudio_185)); }
	inline String_t* get_EnableSupportForMasterAudio_185() const { return ___EnableSupportForMasterAudio_185; }
	inline String_t** get_address_of_EnableSupportForMasterAudio_185() { return &___EnableSupportForMasterAudio_185; }
	inline void set_EnableSupportForMasterAudio_185(String_t* value)
	{
		___EnableSupportForMasterAudio_185 = value;
		Il2CppCodeGenWriteBarrier((&___EnableSupportForMasterAudio_185), value);
	}

	inline static int32_t get_offset_of_Enabled_186() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Enabled_186)); }
	inline String_t* get_Enabled_186() const { return ___Enabled_186; }
	inline String_t** get_address_of_Enabled_186() { return &___Enabled_186; }
	inline void set_Enabled_186(String_t* value)
	{
		___Enabled_186 = value;
		Il2CppCodeGenWriteBarrier((&___Enabled_186), value);
	}

	inline static int32_t get_offset_of_EnterCategoryName_187() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnterCategoryName_187)); }
	inline String_t* get_EnterCategoryName_187() const { return ___EnterCategoryName_187; }
	inline String_t** get_address_of_EnterCategoryName_187() { return &___EnterCategoryName_187; }
	inline void set_EnterCategoryName_187(String_t* value)
	{
		___EnterCategoryName_187 = value;
		Il2CppCodeGenWriteBarrier((&___EnterCategoryName_187), value);
	}

	inline static int32_t get_offset_of_EnterDatabaseName_188() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnterDatabaseName_188)); }
	inline String_t* get_EnterDatabaseName_188() const { return ___EnterDatabaseName_188; }
	inline String_t** get_address_of_EnterDatabaseName_188() { return &___EnterDatabaseName_188; }
	inline void set_EnterDatabaseName_188(String_t* value)
	{
		___EnterDatabaseName_188 = value;
		Il2CppCodeGenWriteBarrier((&___EnterDatabaseName_188), value);
	}

	inline static int32_t get_offset_of_EnterGameEventToListenFor_189() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnterGameEventToListenFor_189)); }
	inline String_t* get_EnterGameEventToListenFor_189() const { return ___EnterGameEventToListenFor_189; }
	inline String_t** get_address_of_EnterGameEventToListenFor_189() { return &___EnterGameEventToListenFor_189; }
	inline void set_EnterGameEventToListenFor_189(String_t* value)
	{
		___EnterGameEventToListenFor_189 = value;
		Il2CppCodeGenWriteBarrier((&___EnterGameEventToListenFor_189), value);
	}

	inline static int32_t get_offset_of_EnterNodeName_190() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EnterNodeName_190)); }
	inline String_t* get_EnterNodeName_190() const { return ___EnterNodeName_190; }
	inline String_t** get_address_of_EnterNodeName_190() { return &___EnterNodeName_190; }
	inline void set_EnterNodeName_190(String_t* value)
	{
		___EnterNodeName_190 = value;
		Il2CppCodeGenWriteBarrier((&___EnterNodeName_190), value);
	}

	inline static int32_t get_offset_of_Error_191() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Error_191)); }
	inline String_t* get_Error_191() const { return ___Error_191; }
	inline String_t** get_address_of_Error_191() { return &___Error_191; }
	inline void set_Error_191(String_t* value)
	{
		___Error_191 = value;
		Il2CppCodeGenWriteBarrier((&___Error_191), value);
	}

	inline static int32_t get_offset_of_Event_192() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Event_192)); }
	inline String_t* get_Event_192() const { return ___Event_192; }
	inline String_t** get_address_of_Event_192() { return &___Event_192; }
	inline void set_Event_192(String_t* value)
	{
		___Event_192 = value;
		Il2CppCodeGenWriteBarrier((&___Event_192), value);
	}

	inline static int32_t get_offset_of_EveryLongTapWillTriggerThisListener_193() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EveryLongTapWillTriggerThisListener_193)); }
	inline String_t* get_EveryLongTapWillTriggerThisListener_193() const { return ___EveryLongTapWillTriggerThisListener_193; }
	inline String_t** get_address_of_EveryLongTapWillTriggerThisListener_193() { return &___EveryLongTapWillTriggerThisListener_193; }
	inline void set_EveryLongTapWillTriggerThisListener_193(String_t* value)
	{
		___EveryLongTapWillTriggerThisListener_193 = value;
		Il2CppCodeGenWriteBarrier((&___EveryLongTapWillTriggerThisListener_193), value);
	}

	inline static int32_t get_offset_of_EverySwipeWillTriggerThisListener_194() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EverySwipeWillTriggerThisListener_194)); }
	inline String_t* get_EverySwipeWillTriggerThisListener_194() const { return ___EverySwipeWillTriggerThisListener_194; }
	inline String_t** get_address_of_EverySwipeWillTriggerThisListener_194() { return &___EverySwipeWillTriggerThisListener_194; }
	inline void set_EverySwipeWillTriggerThisListener_194(String_t* value)
	{
		___EverySwipeWillTriggerThisListener_194 = value;
		Il2CppCodeGenWriteBarrier((&___EverySwipeWillTriggerThisListener_194), value);
	}

	inline static int32_t get_offset_of_EveryTapWillTriggerThisListener_195() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___EveryTapWillTriggerThisListener_195)); }
	inline String_t* get_EveryTapWillTriggerThisListener_195() const { return ___EveryTapWillTriggerThisListener_195; }
	inline String_t** get_address_of_EveryTapWillTriggerThisListener_195() { return &___EveryTapWillTriggerThisListener_195; }
	inline void set_EveryTapWillTriggerThisListener_195(String_t* value)
	{
		___EveryTapWillTriggerThisListener_195 = value;
		Il2CppCodeGenWriteBarrier((&___EveryTapWillTriggerThisListener_195), value);
	}

	inline static int32_t get_offset_of_ExitNodeName_196() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ExitNodeName_196)); }
	inline String_t* get_ExitNodeName_196() const { return ___ExitNodeName_196; }
	inline String_t** get_address_of_ExitNodeName_196() { return &___ExitNodeName_196; }
	inline void set_ExitNodeName_196(String_t* value)
	{
		___ExitNodeName_196 = value;
		Il2CppCodeGenWriteBarrier((&___ExitNodeName_196), value);
	}

	inline static int32_t get_offset_of_Fade_197() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Fade_197)); }
	inline String_t* get_Fade_197() const { return ___Fade_197; }
	inline String_t** get_address_of_Fade_197() { return &___Fade_197; }
	inline void set_Fade_197(String_t* value)
	{
		___Fade_197 = value;
		Il2CppCodeGenWriteBarrier((&___Fade_197), value);
	}

	inline static int32_t get_offset_of_FadeBy_198() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FadeBy_198)); }
	inline String_t* get_FadeBy_198() const { return ___FadeBy_198; }
	inline String_t** get_address_of_FadeBy_198() { return &___FadeBy_198; }
	inline void set_FadeBy_198(String_t* value)
	{
		___FadeBy_198 = value;
		Il2CppCodeGenWriteBarrier((&___FadeBy_198), value);
	}

	inline static int32_t get_offset_of_FadeFrom_199() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FadeFrom_199)); }
	inline String_t* get_FadeFrom_199() const { return ___FadeFrom_199; }
	inline String_t** get_address_of_FadeFrom_199() { return &___FadeFrom_199; }
	inline void set_FadeFrom_199(String_t* value)
	{
		___FadeFrom_199 = value;
		Il2CppCodeGenWriteBarrier((&___FadeFrom_199), value);
	}

	inline static int32_t get_offset_of_FadeOutContainer_200() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FadeOutContainer_200)); }
	inline String_t* get_FadeOutContainer_200() const { return ___FadeOutContainer_200; }
	inline String_t** get_address_of_FadeOutContainer_200() { return &___FadeOutContainer_200; }
	inline void set_FadeOutContainer_200(String_t* value)
	{
		___FadeOutContainer_200 = value;
		Il2CppCodeGenWriteBarrier((&___FadeOutContainer_200), value);
	}

	inline static int32_t get_offset_of_FadeTo_201() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FadeTo_201)); }
	inline String_t* get_FadeTo_201() const { return ___FadeTo_201; }
	inline String_t** get_address_of_FadeTo_201() { return &___FadeTo_201; }
	inline void set_FadeTo_201(String_t* value)
	{
		___FadeTo_201 = value;
		Il2CppCodeGenWriteBarrier((&___FadeTo_201), value);
	}

	inline static int32_t get_offset_of_FixedSize_202() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FixedSize_202)); }
	inline String_t* get_FixedSize_202() const { return ___FixedSize_202; }
	inline String_t** get_address_of_FixedSize_202() { return &___FixedSize_202; }
	inline void set_FixedSize_202(String_t* value)
	{
		___FixedSize_202 = value;
		Il2CppCodeGenWriteBarrier((&___FixedSize_202), value);
	}

	inline static int32_t get_offset_of_FsmName_203() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FsmName_203)); }
	inline String_t* get_FsmName_203() const { return ___FsmName_203; }
	inline String_t** get_address_of_FsmName_203() { return &___FsmName_203; }
	inline void set_FsmName_203(String_t* value)
	{
		___FsmName_203 = value;
		Il2CppCodeGenWriteBarrier((&___FsmName_203), value);
	}

	inline static int32_t get_offset_of_Functionality_204() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Functionality_204)); }
	inline String_t* get_Functionality_204() const { return ___Functionality_204; }
	inline String_t** get_address_of_Functionality_204() { return &___Functionality_204; }
	inline void set_Functionality_204(String_t* value)
	{
		___Functionality_204 = value;
		Il2CppCodeGenWriteBarrier((&___Functionality_204), value);
	}

	inline static int32_t get_offset_of_FunctionalityDescription_205() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___FunctionalityDescription_205)); }
	inline String_t* get_FunctionalityDescription_205() const { return ___FunctionalityDescription_205; }
	inline String_t** get_address_of_FunctionalityDescription_205() { return &___FunctionalityDescription_205; }
	inline void set_FunctionalityDescription_205(String_t* value)
	{
		___FunctionalityDescription_205 = value;
		Il2CppCodeGenWriteBarrier((&___FunctionalityDescription_205), value);
	}

	inline static int32_t get_offset_of_GameEvent_206() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GameEvent_206)); }
	inline String_t* get_GameEvent_206() const { return ___GameEvent_206; }
	inline String_t** get_address_of_GameEvent_206() { return &___GameEvent_206; }
	inline void set_GameEvent_206(String_t* value)
	{
		___GameEvent_206 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_206), value);
	}

	inline static int32_t get_offset_of_GameEvents_207() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GameEvents_207)); }
	inline String_t* get_GameEvents_207() const { return ___GameEvents_207; }
	inline String_t** get_address_of_GameEvents_207() { return &___GameEvents_207; }
	inline void set_GameEvents_207(String_t* value)
	{
		___GameEvents_207 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvents_207), value);
	}

	inline static int32_t get_offset_of_GameObject_208() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GameObject_208)); }
	inline String_t* get_GameObject_208() const { return ___GameObject_208; }
	inline String_t** get_address_of_GameObject_208() { return &___GameObject_208; }
	inline void set_GameObject_208(String_t* value)
	{
		___GameObject_208 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_208), value);
	}

	inline static int32_t get_offset_of_GeneralSettings_209() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GeneralSettings_209)); }
	inline String_t* get_GeneralSettings_209() const { return ___GeneralSettings_209; }
	inline String_t** get_address_of_GeneralSettings_209() { return &___GeneralSettings_209; }
	inline void set_GeneralSettings_209(String_t* value)
	{
		___GeneralSettings_209 = value;
		Il2CppCodeGenWriteBarrier((&___GeneralSettings_209), value);
	}

	inline static int32_t get_offset_of_GestureType_210() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GestureType_210)); }
	inline String_t* get_GestureType_210() const { return ___GestureType_210; }
	inline String_t** get_address_of_GestureType_210() { return &___GestureType_210; }
	inline void set_GestureType_210(String_t* value)
	{
		___GestureType_210 = value;
		Il2CppCodeGenWriteBarrier((&___GestureType_210), value);
	}

	inline static int32_t get_offset_of_GetInTouch_211() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GetInTouch_211)); }
	inline String_t* get_GetInTouch_211() const { return ___GetInTouch_211; }
	inline String_t** get_address_of_GetInTouch_211() { return &___GetInTouch_211; }
	inline void set_GetInTouch_211(String_t* value)
	{
		___GetInTouch_211 = value;
		Il2CppCodeGenWriteBarrier((&___GetInTouch_211), value);
	}

	inline static int32_t get_offset_of_GetPosition_212() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GetPosition_212)); }
	inline String_t* get_GetPosition_212() const { return ___GetPosition_212; }
	inline String_t** get_address_of_GetPosition_212() { return &___GetPosition_212; }
	inline void set_GetPosition_212(String_t* value)
	{
		___GetPosition_212 = value;
		Il2CppCodeGenWriteBarrier((&___GetPosition_212), value);
	}

	inline static int32_t get_offset_of_GetSceneBy_213() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GetSceneBy_213)); }
	inline String_t* get_GetSceneBy_213() const { return ___GetSceneBy_213; }
	inline String_t** get_address_of_GetSceneBy_213() { return &___GetSceneBy_213; }
	inline void set_GetSceneBy_213(String_t* value)
	{
		___GetSceneBy_213 = value;
		Il2CppCodeGenWriteBarrier((&___GetSceneBy_213), value);
	}

	inline static int32_t get_offset_of_GlobalListener_214() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GlobalListener_214)); }
	inline String_t* get_GlobalListener_214() const { return ___GlobalListener_214; }
	inline String_t** get_address_of_GlobalListener_214() { return &___GlobalListener_214; }
	inline void set_GlobalListener_214(String_t* value)
	{
		___GlobalListener_214 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalListener_214), value);
	}

	inline static int32_t get_offset_of_GoToEnterNode_215() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GoToEnterNode_215)); }
	inline String_t* get_GoToEnterNode_215() const { return ___GoToEnterNode_215; }
	inline String_t** get_address_of_GoToEnterNode_215() { return &___GoToEnterNode_215; }
	inline void set_GoToEnterNode_215(String_t* value)
	{
		___GoToEnterNode_215 = value;
		Il2CppCodeGenWriteBarrier((&___GoToEnterNode_215), value);
	}

	inline static int32_t get_offset_of_GoToExitNode_216() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GoToExitNode_216)); }
	inline String_t* get_GoToExitNode_216() const { return ___GoToExitNode_216; }
	inline String_t** get_address_of_GoToExitNode_216() { return &___GoToExitNode_216; }
	inline void set_GoToExitNode_216(String_t* value)
	{
		___GoToExitNode_216 = value;
		Il2CppCodeGenWriteBarrier((&___GoToExitNode_216), value);
	}

	inline static int32_t get_offset_of_GoToStartNode_217() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GoToStartNode_217)); }
	inline String_t* get_GoToStartNode_217() const { return ___GoToStartNode_217; }
	inline String_t** get_address_of_GoToStartNode_217() { return &___GoToStartNode_217; }
	inline void set_GoToStartNode_217(String_t* value)
	{
		___GoToStartNode_217 = value;
		Il2CppCodeGenWriteBarrier((&___GoToStartNode_217), value);
	}

	inline static int32_t get_offset_of_Graph_218() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Graph_218)); }
	inline String_t* get_Graph_218() const { return ___Graph_218; }
	inline String_t** get_address_of_Graph_218() { return &___Graph_218; }
	inline void set_Graph_218(String_t* value)
	{
		___Graph_218 = value;
		Il2CppCodeGenWriteBarrier((&___Graph_218), value);
	}

	inline static int32_t get_offset_of_GraphHasNoNodes_219() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GraphHasNoNodes_219)); }
	inline String_t* get_GraphHasNoNodes_219() const { return ___GraphHasNoNodes_219; }
	inline String_t** get_address_of_GraphHasNoNodes_219() { return &___GraphHasNoNodes_219; }
	inline void set_GraphHasNoNodes_219(String_t* value)
	{
		___GraphHasNoNodes_219 = value;
		Il2CppCodeGenWriteBarrier((&___GraphHasNoNodes_219), value);
	}

	inline static int32_t get_offset_of_GraphId_220() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GraphId_220)); }
	inline String_t* get_GraphId_220() const { return ___GraphId_220; }
	inline String_t** get_address_of_GraphId_220() { return &___GraphId_220; }
	inline void set_GraphId_220(String_t* value)
	{
		___GraphId_220 = value;
		Il2CppCodeGenWriteBarrier((&___GraphId_220), value);
	}

	inline static int32_t get_offset_of_GraphModel_221() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GraphModel_221)); }
	inline String_t* get_GraphModel_221() const { return ___GraphModel_221; }
	inline String_t** get_address_of_GraphModel_221() { return &___GraphModel_221; }
	inline void set_GraphModel_221(String_t* value)
	{
		___GraphModel_221 = value;
		Il2CppCodeGenWriteBarrier((&___GraphModel_221), value);
	}

	inline static int32_t get_offset_of_GraphicRaycaster_222() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___GraphicRaycaster_222)); }
	inline String_t* get_GraphicRaycaster_222() const { return ___GraphicRaycaster_222; }
	inline String_t** get_address_of_GraphicRaycaster_222() { return &___GraphicRaycaster_222; }
	inline void set_GraphicRaycaster_222(String_t* value)
	{
		___GraphicRaycaster_222 = value;
		Il2CppCodeGenWriteBarrier((&___GraphicRaycaster_222), value);
	}

	inline static int32_t get_offset_of_HasBeenAddedToClipboard_223() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HasBeenAddedToClipboard_223)); }
	inline String_t* get_HasBeenAddedToClipboard_223() const { return ___HasBeenAddedToClipboard_223; }
	inline String_t** get_address_of_HasBeenAddedToClipboard_223() { return &___HasBeenAddedToClipboard_223; }
	inline void set_HasBeenAddedToClipboard_223(String_t* value)
	{
		___HasBeenAddedToClipboard_223 = value;
		Il2CppCodeGenWriteBarrier((&___HasBeenAddedToClipboard_223), value);
	}

	inline static int32_t get_offset_of_Height_224() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Height_224)); }
	inline String_t* get_Height_224() const { return ___Height_224; }
	inline String_t** get_address_of_Height_224() { return &___Height_224; }
	inline void set_Height_224(String_t* value)
	{
		___Height_224 = value;
		Il2CppCodeGenWriteBarrier((&___Height_224), value);
	}

	inline static int32_t get_offset_of_Help_225() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Help_225)); }
	inline String_t* get_Help_225() const { return ___Help_225; }
	inline String_t** get_address_of_Help_225() { return &___Help_225; }
	inline void set_Help_225(String_t* value)
	{
		___Help_225 = value;
		Il2CppCodeGenWriteBarrier((&___Help_225), value);
	}

	inline static int32_t get_offset_of_HelpResources_226() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HelpResources_226)); }
	inline String_t* get_HelpResources_226() const { return ___HelpResources_226; }
	inline String_t** get_address_of_HelpResources_226() { return &___HelpResources_226; }
	inline void set_HelpResources_226(String_t* value)
	{
		___HelpResources_226 = value;
		Il2CppCodeGenWriteBarrier((&___HelpResources_226), value);
	}

	inline static int32_t get_offset_of_Hide_227() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Hide_227)); }
	inline String_t* get_Hide_227() const { return ___Hide_227; }
	inline String_t** get_address_of_Hide_227() { return &___Hide_227; }
	inline void set_Hide_227(String_t* value)
	{
		___Hide_227 = value;
		Il2CppCodeGenWriteBarrier((&___Hide_227), value);
	}

	inline static int32_t get_offset_of_HideAnimationWillNotWork_228() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HideAnimationWillNotWork_228)); }
	inline String_t* get_HideAnimationWillNotWork_228() const { return ___HideAnimationWillNotWork_228; }
	inline String_t** get_address_of_HideAnimationWillNotWork_228() { return &___HideAnimationWillNotWork_228; }
	inline void set_HideAnimationWillNotWork_228(String_t* value)
	{
		___HideAnimationWillNotWork_228 = value;
		Il2CppCodeGenWriteBarrier((&___HideAnimationWillNotWork_228), value);
	}

	inline static int32_t get_offset_of_HidePopup_229() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HidePopup_229)); }
	inline String_t* get_HidePopup_229() const { return ___HidePopup_229; }
	inline String_t** get_address_of_HidePopup_229() { return &___HidePopup_229; }
	inline void set_HidePopup_229(String_t* value)
	{
		___HidePopup_229 = value;
		Il2CppCodeGenWriteBarrier((&___HidePopup_229), value);
	}

	inline static int32_t get_offset_of_HideProgressor_230() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HideProgressor_230)); }
	inline String_t* get_HideProgressor_230() const { return ___HideProgressor_230; }
	inline String_t** get_address_of_HideProgressor_230() { return &___HideProgressor_230; }
	inline void set_HideProgressor_230(String_t* value)
	{
		___HideProgressor_230 = value;
		Il2CppCodeGenWriteBarrier((&___HideProgressor_230), value);
	}

	inline static int32_t get_offset_of_HideUIPopupBy_231() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HideUIPopupBy_231)); }
	inline String_t* get_HideUIPopupBy_231() const { return ___HideUIPopupBy_231; }
	inline String_t** get_address_of_HideUIPopupBy_231() { return &___HideUIPopupBy_231; }
	inline void set_HideUIPopupBy_231(String_t* value)
	{
		___HideUIPopupBy_231 = value;
		Il2CppCodeGenWriteBarrier((&___HideUIPopupBy_231), value);
	}

	inline static int32_t get_offset_of_HideView_232() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HideView_232)); }
	inline String_t* get_HideView_232() const { return ___HideView_232; }
	inline String_t** get_address_of_HideView_232() { return &___HideView_232; }
	inline void set_HideView_232(String_t* value)
	{
		___HideView_232 = value;
		Il2CppCodeGenWriteBarrier((&___HideView_232), value);
	}

	inline static int32_t get_offset_of_HideViews_233() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HideViews_233)); }
	inline String_t* get_HideViews_233() const { return ___HideViews_233; }
	inline String_t** get_address_of_HideViews_233() { return &___HideViews_233; }
	inline void set_HideViews_233(String_t* value)
	{
		___HideViews_233 = value;
		Il2CppCodeGenWriteBarrier((&___HideViews_233), value);
	}

	inline static int32_t get_offset_of_HowToUse_234() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HowToUse_234)); }
	inline String_t* get_HowToUse_234() const { return ___HowToUse_234; }
	inline String_t** get_address_of_HowToUse_234() { return &___HowToUse_234; }
	inline void set_HowToUse_234(String_t* value)
	{
		___HowToUse_234 = value;
		Il2CppCodeGenWriteBarrier((&___HowToUse_234), value);
	}

	inline static int32_t get_offset_of_IdleCheckInterval_235() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IdleCheckInterval_235)); }
	inline String_t* get_IdleCheckInterval_235() const { return ___IdleCheckInterval_235; }
	inline String_t** get_address_of_IdleCheckInterval_235() { return &___IdleCheckInterval_235; }
	inline void set_IdleCheckInterval_235(String_t* value)
	{
		___IdleCheckInterval_235 = value;
		Il2CppCodeGenWriteBarrier((&___IdleCheckInterval_235), value);
	}

	inline static int32_t get_offset_of_IdleCheckIntervalDescription_236() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IdleCheckIntervalDescription_236)); }
	inline String_t* get_IdleCheckIntervalDescription_236() const { return ___IdleCheckIntervalDescription_236; }
	inline String_t** get_address_of_IdleCheckIntervalDescription_236() { return &___IdleCheckIntervalDescription_236; }
	inline void set_IdleCheckIntervalDescription_236(String_t* value)
	{
		___IdleCheckIntervalDescription_236 = value;
		Il2CppCodeGenWriteBarrier((&___IdleCheckIntervalDescription_236), value);
	}

	inline static int32_t get_offset_of_IgnoreListenerPause_237() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IgnoreListenerPause_237)); }
	inline String_t* get_IgnoreListenerPause_237() const { return ___IgnoreListenerPause_237; }
	inline String_t** get_address_of_IgnoreListenerPause_237() { return &___IgnoreListenerPause_237; }
	inline void set_IgnoreListenerPause_237(String_t* value)
	{
		___IgnoreListenerPause_237 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoreListenerPause_237), value);
	}

	inline static int32_t get_offset_of_IgnoreTimescale_238() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IgnoreTimescale_238)); }
	inline String_t* get_IgnoreTimescale_238() const { return ___IgnoreTimescale_238; }
	inline String_t** get_address_of_IgnoreTimescale_238() { return &___IgnoreTimescale_238; }
	inline void set_IgnoreTimescale_238(String_t* value)
	{
		___IgnoreTimescale_238 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoreTimescale_238), value);
	}

	inline static int32_t get_offset_of_Image_239() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Image_239)); }
	inline String_t* get_Image_239() const { return ___Image_239; }
	inline String_t** get_address_of_Image_239() { return &___Image_239; }
	inline void set_Image_239(String_t* value)
	{
		___Image_239 = value;
		Il2CppCodeGenWriteBarrier((&___Image_239), value);
	}

	inline static int32_t get_offset_of_Images_240() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Images_240)); }
	inline String_t* get_Images_240() const { return ___Images_240; }
	inline String_t** get_address_of_Images_240() { return &___Images_240; }
	inline void set_Images_240(String_t* value)
	{
		___Images_240 = value;
		Il2CppCodeGenWriteBarrier((&___Images_240), value);
	}

	inline static int32_t get_offset_of_IncludeAudioClipNamesInSearch_241() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IncludeAudioClipNamesInSearch_241)); }
	inline String_t* get_IncludeAudioClipNamesInSearch_241() const { return ___IncludeAudioClipNamesInSearch_241; }
	inline String_t** get_address_of_IncludeAudioClipNamesInSearch_241() { return &___IncludeAudioClipNamesInSearch_241; }
	inline void set_IncludeAudioClipNamesInSearch_241(String_t* value)
	{
		___IncludeAudioClipNamesInSearch_241 = value;
		Il2CppCodeGenWriteBarrier((&___IncludeAudioClipNamesInSearch_241), value);
	}

	inline static int32_t get_offset_of_Info_242() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Info_242)); }
	inline String_t* get_Info_242() const { return ___Info_242; }
	inline String_t** get_address_of_Info_242() { return &___Info_242; }
	inline void set_Info_242(String_t* value)
	{
		___Info_242 = value;
		Il2CppCodeGenWriteBarrier((&___Info_242), value);
	}

	inline static int32_t get_offset_of_InputConnected_243() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InputConnected_243)); }
	inline String_t* get_InputConnected_243() const { return ___InputConnected_243; }
	inline String_t** get_address_of_InputConnected_243() { return &___InputConnected_243; }
	inline void set_InputConnected_243(String_t* value)
	{
		___InputConnected_243 = value;
		Il2CppCodeGenWriteBarrier((&___InputConnected_243), value);
	}

	inline static int32_t get_offset_of_InputConnections_244() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InputConnections_244)); }
	inline String_t* get_InputConnections_244() const { return ___InputConnections_244; }
	inline String_t** get_address_of_InputConnections_244() { return &___InputConnections_244; }
	inline void set_InputConnections_244(String_t* value)
	{
		___InputConnections_244 = value;
		Il2CppCodeGenWriteBarrier((&___InputConnections_244), value);
	}

	inline static int32_t get_offset_of_InputMode_245() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InputMode_245)); }
	inline String_t* get_InputMode_245() const { return ___InputMode_245; }
	inline String_t** get_address_of_InputMode_245() { return &___InputMode_245; }
	inline void set_InputMode_245(String_t* value)
	{
		___InputMode_245 = value;
		Il2CppCodeGenWriteBarrier((&___InputMode_245), value);
	}

	inline static int32_t get_offset_of_InputNotConnected_246() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InputNotConnected_246)); }
	inline String_t* get_InputNotConnected_246() const { return ___InputNotConnected_246; }
	inline String_t** get_address_of_InputNotConnected_246() { return &___InputNotConnected_246; }
	inline void set_InputNotConnected_246(String_t* value)
	{
		___InputNotConnected_246 = value;
		Il2CppCodeGenWriteBarrier((&___InputNotConnected_246), value);
	}

	inline static int32_t get_offset_of_Installed_247() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Installed_247)); }
	inline String_t* get_Installed_247() const { return ___Installed_247; }
	inline String_t** get_address_of_Installed_247() { return &___Installed_247; }
	inline void set_Installed_247(String_t* value)
	{
		___Installed_247 = value;
		Il2CppCodeGenWriteBarrier((&___Installed_247), value);
	}

	inline static int32_t get_offset_of_InstantAction_248() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InstantAction_248)); }
	inline String_t* get_InstantAction_248() const { return ___InstantAction_248; }
	inline String_t** get_address_of_InstantAction_248() { return &___InstantAction_248; }
	inline void set_InstantAction_248(String_t* value)
	{
		___InstantAction_248 = value;
		Il2CppCodeGenWriteBarrier((&___InstantAction_248), value);
	}

	inline static int32_t get_offset_of_InstantAnimation_249() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___InstantAnimation_249)); }
	inline String_t* get_InstantAnimation_249() const { return ___InstantAnimation_249; }
	inline String_t** get_address_of_InstantAnimation_249() { return &___InstantAnimation_249; }
	inline void set_InstantAnimation_249(String_t* value)
	{
		___InstantAnimation_249 = value;
		Il2CppCodeGenWriteBarrier((&___InstantAnimation_249), value);
	}

	inline static int32_t get_offset_of_Integrations_250() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Integrations_250)); }
	inline String_t* get_Integrations_250() const { return ___Integrations_250; }
	inline String_t** get_address_of_Integrations_250() { return &___Integrations_250; }
	inline void set_Integrations_250(String_t* value)
	{
		___Integrations_250 = value;
		Il2CppCodeGenWriteBarrier((&___Integrations_250), value);
	}

	inline static int32_t get_offset_of_IsNotPrefab_251() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___IsNotPrefab_251)); }
	inline String_t* get_IsNotPrefab_251() const { return ___IsNotPrefab_251; }
	inline String_t** get_address_of_IsNotPrefab_251() { return &___IsNotPrefab_251; }
	inline void set_IsNotPrefab_251(String_t* value)
	{
		___IsNotPrefab_251 = value;
		Il2CppCodeGenWriteBarrier((&___IsNotPrefab_251), value);
	}

	inline static int32_t get_offset_of_Key_252() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Key_252)); }
	inline String_t* get_Key_252() const { return ___Key_252; }
	inline String_t** get_address_of_Key_252() { return &___Key_252; }
	inline void set_Key_252(String_t* value)
	{
		___Key_252 = value;
		Il2CppCodeGenWriteBarrier((&___Key_252), value);
	}

	inline static int32_t get_offset_of_KeyCode_253() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___KeyCode_253)); }
	inline String_t* get_KeyCode_253() const { return ___KeyCode_253; }
	inline String_t** get_address_of_KeyCode_253() { return &___KeyCode_253; }
	inline void set_KeyCode_253(String_t* value)
	{
		___KeyCode_253 = value;
		Il2CppCodeGenWriteBarrier((&___KeyCode_253), value);
	}

	inline static int32_t get_offset_of_Label_254() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Label_254)); }
	inline String_t* get_Label_254() const { return ___Label_254; }
	inline String_t** get_address_of_Label_254() { return &___Label_254; }
	inline void set_Label_254(String_t* value)
	{
		___Label_254 = value;
		Il2CppCodeGenWriteBarrier((&___Label_254), value);
	}

	inline static int32_t get_offset_of_Labels_255() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Labels_255)); }
	inline String_t* get_Labels_255() const { return ___Labels_255; }
	inline String_t** get_address_of_Labels_255() { return &___Labels_255; }
	inline void set_Labels_255(String_t* value)
	{
		___Labels_255 = value;
		Il2CppCodeGenWriteBarrier((&___Labels_255), value);
	}

	inline static int32_t get_offset_of_Language_256() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Language_256)); }
	inline String_t* get_Language_256() const { return ___Language_256; }
	inline String_t** get_address_of_Language_256() { return &___Language_256; }
	inline void set_Language_256(String_t* value)
	{
		___Language_256 = value;
		Il2CppCodeGenWriteBarrier((&___Language_256), value);
	}

	inline static int32_t get_offset_of_LastModified_257() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LastModified_257)); }
	inline String_t* get_LastModified_257() const { return ___LastModified_257; }
	inline String_t** get_address_of_LastModified_257() { return &___LastModified_257; }
	inline void set_LastModified_257(String_t* value)
	{
		___LastModified_257 = value;
		Il2CppCodeGenWriteBarrier((&___LastModified_257), value);
	}

	inline static int32_t get_offset_of_Left_258() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Left_258)); }
	inline String_t* get_Left_258() const { return ___Left_258; }
	inline String_t** get_address_of_Left_258() { return &___Left_258; }
	inline void set_Left_258(String_t* value)
	{
		___Left_258 = value;
		Il2CppCodeGenWriteBarrier((&___Left_258), value);
	}

	inline static int32_t get_offset_of_ListIsEmpty_259() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListIsEmpty_259)); }
	inline String_t* get_ListIsEmpty_259() const { return ___ListIsEmpty_259; }
	inline String_t** get_address_of_ListIsEmpty_259() { return &___ListIsEmpty_259; }
	inline void set_ListIsEmpty_259(String_t* value)
	{
		___ListIsEmpty_259 = value;
		Il2CppCodeGenWriteBarrier((&___ListIsEmpty_259), value);
	}

	inline static int32_t get_offset_of_ListenForAllGameEvents_260() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListenForAllGameEvents_260)); }
	inline String_t* get_ListenForAllGameEvents_260() const { return ___ListenForAllGameEvents_260; }
	inline String_t** get_address_of_ListenForAllGameEvents_260() { return &___ListenForAllGameEvents_260; }
	inline void set_ListenForAllGameEvents_260(String_t* value)
	{
		___ListenForAllGameEvents_260 = value;
		Il2CppCodeGenWriteBarrier((&___ListenForAllGameEvents_260), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIButtons_261() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListenForAllUIButtons_261)); }
	inline String_t* get_ListenForAllUIButtons_261() const { return ___ListenForAllUIButtons_261; }
	inline String_t** get_address_of_ListenForAllUIButtons_261() { return &___ListenForAllUIButtons_261; }
	inline void set_ListenForAllUIButtons_261(String_t* value)
	{
		___ListenForAllUIButtons_261 = value;
		Il2CppCodeGenWriteBarrier((&___ListenForAllUIButtons_261), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIDrawers_262() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListenForAllUIDrawers_262)); }
	inline String_t* get_ListenForAllUIDrawers_262() const { return ___ListenForAllUIDrawers_262; }
	inline String_t** get_address_of_ListenForAllUIDrawers_262() { return &___ListenForAllUIDrawers_262; }
	inline void set_ListenForAllUIDrawers_262(String_t* value)
	{
		___ListenForAllUIDrawers_262 = value;
		Il2CppCodeGenWriteBarrier((&___ListenForAllUIDrawers_262), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIViews_263() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListenForAllUIViews_263)); }
	inline String_t* get_ListenForAllUIViews_263() const { return ___ListenForAllUIViews_263; }
	inline String_t** get_address_of_ListenForAllUIViews_263() { return &___ListenForAllUIViews_263; }
	inline void set_ListenForAllUIViews_263(String_t* value)
	{
		___ListenForAllUIViews_263 = value;
		Il2CppCodeGenWriteBarrier((&___ListenForAllUIViews_263), value);
	}

	inline static int32_t get_offset_of_ListeningForGameEvent_264() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ListeningForGameEvent_264)); }
	inline String_t* get_ListeningForGameEvent_264() const { return ___ListeningForGameEvent_264; }
	inline String_t** get_address_of_ListeningForGameEvent_264() { return &___ListeningForGameEvent_264; }
	inline void set_ListeningForGameEvent_264(String_t* value)
	{
		___ListeningForGameEvent_264 = value;
		Il2CppCodeGenWriteBarrier((&___ListeningForGameEvent_264), value);
	}

	inline static int32_t get_offset_of_Load_265() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Load_265)); }
	inline String_t* get_Load_265() const { return ___Load_265; }
	inline String_t** get_address_of_Load_265() { return &___Load_265; }
	inline void set_Load_265(String_t* value)
	{
		___Load_265 = value;
		Il2CppCodeGenWriteBarrier((&___Load_265), value);
	}

	inline static int32_t get_offset_of_LoadBehavior_266() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadBehavior_266)); }
	inline String_t* get_LoadBehavior_266() const { return ___LoadBehavior_266; }
	inline String_t** get_address_of_LoadBehavior_266() { return &___LoadBehavior_266; }
	inline void set_LoadBehavior_266(String_t* value)
	{
		___LoadBehavior_266 = value;
		Il2CppCodeGenWriteBarrier((&___LoadBehavior_266), value);
	}

	inline static int32_t get_offset_of_LoadPreset_267() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadPreset_267)); }
	inline String_t* get_LoadPreset_267() const { return ___LoadPreset_267; }
	inline String_t** get_address_of_LoadPreset_267() { return &___LoadPreset_267; }
	inline void set_LoadPreset_267(String_t* value)
	{
		___LoadPreset_267 = value;
		Il2CppCodeGenWriteBarrier((&___LoadPreset_267), value);
	}

	inline static int32_t get_offset_of_LoadSceneBy_268() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadSceneBy_268)); }
	inline String_t* get_LoadSceneBy_268() const { return ___LoadSceneBy_268; }
	inline String_t** get_address_of_LoadSceneBy_268() { return &___LoadSceneBy_268; }
	inline void set_LoadSceneBy_268(String_t* value)
	{
		___LoadSceneBy_268 = value;
		Il2CppCodeGenWriteBarrier((&___LoadSceneBy_268), value);
	}

	inline static int32_t get_offset_of_LoadSceneMode_269() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadSceneMode_269)); }
	inline String_t* get_LoadSceneMode_269() const { return ___LoadSceneMode_269; }
	inline String_t** get_address_of_LoadSceneMode_269() { return &___LoadSceneMode_269; }
	inline void set_LoadSceneMode_269(String_t* value)
	{
		___LoadSceneMode_269 = value;
		Il2CppCodeGenWriteBarrier((&___LoadSceneMode_269), value);
	}

	inline static int32_t get_offset_of_LoadSceneNodeName_270() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadSceneNodeName_270)); }
	inline String_t* get_LoadSceneNodeName_270() const { return ___LoadSceneNodeName_270; }
	inline String_t** get_address_of_LoadSceneNodeName_270() { return &___LoadSceneNodeName_270; }
	inline void set_LoadSceneNodeName_270(String_t* value)
	{
		___LoadSceneNodeName_270 = value;
		Il2CppCodeGenWriteBarrier((&___LoadSceneNodeName_270), value);
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_271() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadSelectedPresetAtRuntime_271)); }
	inline String_t* get_LoadSelectedPresetAtRuntime_271() const { return ___LoadSelectedPresetAtRuntime_271; }
	inline String_t** get_address_of_LoadSelectedPresetAtRuntime_271() { return &___LoadSelectedPresetAtRuntime_271; }
	inline void set_LoadSelectedPresetAtRuntime_271(String_t* value)
	{
		___LoadSelectedPresetAtRuntime_271 = value;
		Il2CppCodeGenWriteBarrier((&___LoadSelectedPresetAtRuntime_271), value);
	}

	inline static int32_t get_offset_of_LoadedGraph_272() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoadedGraph_272)); }
	inline String_t* get_LoadedGraph_272() const { return ___LoadedGraph_272; }
	inline String_t** get_address_of_LoadedGraph_272() { return &___LoadedGraph_272; }
	inline void set_LoadedGraph_272(String_t* value)
	{
		___LoadedGraph_272 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedGraph_272), value);
	}

	inline static int32_t get_offset_of_LongTapDuration_273() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LongTapDuration_273)); }
	inline String_t* get_LongTapDuration_273() const { return ___LongTapDuration_273; }
	inline String_t** get_address_of_LongTapDuration_273() { return &___LongTapDuration_273; }
	inline void set_LongTapDuration_273(String_t* value)
	{
		___LongTapDuration_273 = value;
		Il2CppCodeGenWriteBarrier((&___LongTapDuration_273), value);
	}

	inline static int32_t get_offset_of_LongTapDurationDescription_274() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LongTapDurationDescription_274)); }
	inline String_t* get_LongTapDurationDescription_274() const { return ___LongTapDurationDescription_274; }
	inline String_t** get_address_of_LongTapDurationDescription_274() { return &___LongTapDurationDescription_274; }
	inline void set_LongTapDurationDescription_274(String_t* value)
	{
		___LongTapDurationDescription_274 = value;
		Il2CppCodeGenWriteBarrier((&___LongTapDurationDescription_274), value);
	}

	inline static int32_t get_offset_of_LoopSound_275() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoopSound_275)); }
	inline String_t* get_LoopSound_275() const { return ___LoopSound_275; }
	inline String_t** get_address_of_LoopSound_275() { return &___LoopSound_275; }
	inline void set_LoopSound_275(String_t* value)
	{
		___LoopSound_275 = value;
		Il2CppCodeGenWriteBarrier((&___LoopSound_275), value);
	}

	inline static int32_t get_offset_of_LoopType_276() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoopType_276)); }
	inline String_t* get_LoopType_276() const { return ___LoopType_276; }
	inline String_t** get_address_of_LoopType_276() { return &___LoopType_276; }
	inline void set_LoopType_276(String_t* value)
	{
		___LoopType_276 = value;
		Il2CppCodeGenWriteBarrier((&___LoopType_276), value);
	}

	inline static int32_t get_offset_of_LoopView_277() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___LoopView_277)); }
	inline String_t* get_LoopView_277() const { return ___LoopView_277; }
	inline String_t** get_address_of_LoopView_277() { return &___LoopView_277; }
	inline void set_LoopView_277(String_t* value)
	{
		___LoopView_277 = value;
		Il2CppCodeGenWriteBarrier((&___LoopView_277), value);
	}

	inline static int32_t get_offset_of_Manual_278() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Manual_278)); }
	inline String_t* get_Manual_278() const { return ___Manual_278; }
	inline String_t** get_address_of_Manual_278() { return &___Manual_278; }
	inline void set_Manual_278(String_t* value)
	{
		___Manual_278 = value;
		Il2CppCodeGenWriteBarrier((&___Manual_278), value);
	}

	inline static int32_t get_offset_of_Max_279() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Max_279)); }
	inline String_t* get_Max_279() const { return ___Max_279; }
	inline String_t** get_address_of_Max_279() { return &___Max_279; }
	inline void set_Max_279(String_t* value)
	{
		___Max_279 = value;
		Il2CppCodeGenWriteBarrier((&___Max_279), value);
	}

	inline static int32_t get_offset_of_MaxValue_280() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MaxValue_280)); }
	inline String_t* get_MaxValue_280() const { return ___MaxValue_280; }
	inline String_t** get_address_of_MaxValue_280() { return &___MaxValue_280; }
	inline void set_MaxValue_280(String_t* value)
	{
		___MaxValue_280 = value;
		Il2CppCodeGenWriteBarrier((&___MaxValue_280), value);
	}

	inline static int32_t get_offset_of_Min_281() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Min_281)); }
	inline String_t* get_Min_281() const { return ___Min_281; }
	inline String_t** get_address_of_Min_281() { return &___Min_281; }
	inline void set_Min_281(String_t* value)
	{
		___Min_281 = value;
		Il2CppCodeGenWriteBarrier((&___Min_281), value);
	}

	inline static int32_t get_offset_of_MinValue_282() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MinValue_282)); }
	inline String_t* get_MinValue_282() const { return ___MinValue_282; }
	inline String_t** get_address_of_MinValue_282() { return &___MinValue_282; }
	inline void set_MinValue_282(String_t* value)
	{
		___MinValue_282 = value;
		Il2CppCodeGenWriteBarrier((&___MinValue_282), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfControllers_283() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MinimumNumberOfControllers_283)); }
	inline String_t* get_MinimumNumberOfControllers_283() const { return ___MinimumNumberOfControllers_283; }
	inline String_t** get_address_of_MinimumNumberOfControllers_283() { return &___MinimumNumberOfControllers_283; }
	inline void set_MinimumNumberOfControllers_283(String_t* value)
	{
		___MinimumNumberOfControllers_283 = value;
		Il2CppCodeGenWriteBarrier((&___MinimumNumberOfControllers_283), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfControllersDescription_284() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MinimumNumberOfControllersDescription_284)); }
	inline String_t* get_MinimumNumberOfControllersDescription_284() const { return ___MinimumNumberOfControllersDescription_284; }
	inline String_t** get_address_of_MinimumNumberOfControllersDescription_284() { return &___MinimumNumberOfControllersDescription_284; }
	inline void set_MinimumNumberOfControllersDescription_284(String_t* value)
	{
		___MinimumNumberOfControllersDescription_284 = value;
		Il2CppCodeGenWriteBarrier((&___MinimumNumberOfControllersDescription_284), value);
	}

	inline static int32_t get_offset_of_MinimumSize_285() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MinimumSize_285)); }
	inline String_t* get_MinimumSize_285() const { return ___MinimumSize_285; }
	inline String_t** get_address_of_MinimumSize_285() { return &___MinimumSize_285; }
	inline void set_MinimumSize_285(String_t* value)
	{
		___MinimumSize_285 = value;
		Il2CppCodeGenWriteBarrier((&___MinimumSize_285), value);
	}

	inline static int32_t get_offset_of_MissingPrefabReference_286() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingPrefabReference_286)); }
	inline String_t* get_MissingPrefabReference_286() const { return ___MissingPrefabReference_286; }
	inline String_t** get_address_of_MissingPrefabReference_286() { return &___MissingPrefabReference_286; }
	inline void set_MissingPrefabReference_286(String_t* value)
	{
		___MissingPrefabReference_286 = value;
		Il2CppCodeGenWriteBarrier((&___MissingPrefabReference_286), value);
	}

	inline static int32_t get_offset_of_Move_287() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Move_287)); }
	inline String_t* get_Move_287() const { return ___Move_287; }
	inline String_t** get_address_of_Move_287() { return &___Move_287; }
	inline void set_Move_287(String_t* value)
	{
		___Move_287 = value;
		Il2CppCodeGenWriteBarrier((&___Move_287), value);
	}

	inline static int32_t get_offset_of_MoveBy_288() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MoveBy_288)); }
	inline String_t* get_MoveBy_288() const { return ___MoveBy_288; }
	inline String_t** get_address_of_MoveBy_288() { return &___MoveBy_288; }
	inline void set_MoveBy_288(String_t* value)
	{
		___MoveBy_288 = value;
		Il2CppCodeGenWriteBarrier((&___MoveBy_288), value);
	}

	inline static int32_t get_offset_of_MoveDown_289() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MoveDown_289)); }
	inline String_t* get_MoveDown_289() const { return ___MoveDown_289; }
	inline String_t** get_address_of_MoveDown_289() { return &___MoveDown_289; }
	inline void set_MoveDown_289(String_t* value)
	{
		___MoveDown_289 = value;
		Il2CppCodeGenWriteBarrier((&___MoveDown_289), value);
	}

	inline static int32_t get_offset_of_MoveFrom_290() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MoveFrom_290)); }
	inline String_t* get_MoveFrom_290() const { return ___MoveFrom_290; }
	inline String_t** get_address_of_MoveFrom_290() { return &___MoveFrom_290; }
	inline void set_MoveFrom_290(String_t* value)
	{
		___MoveFrom_290 = value;
		Il2CppCodeGenWriteBarrier((&___MoveFrom_290), value);
	}

	inline static int32_t get_offset_of_MoveTo_291() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MoveTo_291)); }
	inline String_t* get_MoveTo_291() const { return ___MoveTo_291; }
	inline String_t** get_address_of_MoveTo_291() { return &___MoveTo_291; }
	inline void set_MoveTo_291(String_t* value)
	{
		___MoveTo_291 = value;
		Il2CppCodeGenWriteBarrier((&___MoveTo_291), value);
	}

	inline static int32_t get_offset_of_MoveUp_292() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MoveUp_292)); }
	inline String_t* get_MoveUp_292() const { return ___MoveUp_292; }
	inline String_t** get_address_of_MoveUp_292() { return &___MoveUp_292; }
	inline void set_MoveUp_292(String_t* value)
	{
		___MoveUp_292 = value;
		Il2CppCodeGenWriteBarrier((&___MoveUp_292), value);
	}

	inline static int32_t get_offset_of_Multiplier_293() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Multiplier_293)); }
	inline String_t* get_Multiplier_293() const { return ___Multiplier_293; }
	inline String_t** get_address_of_Multiplier_293() { return &___Multiplier_293; }
	inline void set_Multiplier_293(String_t* value)
	{
		___Multiplier_293 = value;
		Il2CppCodeGenWriteBarrier((&___Multiplier_293), value);
	}

	inline static int32_t get_offset_of_MuteAllSounds_294() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MuteAllSounds_294)); }
	inline String_t* get_MuteAllSounds_294() const { return ___MuteAllSounds_294; }
	inline String_t** get_address_of_MuteAllSounds_294() { return &___MuteAllSounds_294; }
	inline void set_MuteAllSounds_294(String_t* value)
	{
		___MuteAllSounds_294 = value;
		Il2CppCodeGenWriteBarrier((&___MuteAllSounds_294), value);
	}

	inline static int32_t get_offset_of_Name_295() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Name_295)); }
	inline String_t* get_Name_295() const { return ___Name_295; }
	inline String_t** get_address_of_Name_295() { return &___Name_295; }
	inline void set_Name_295(String_t* value)
	{
		___Name_295 = value;
		Il2CppCodeGenWriteBarrier((&___Name_295), value);
	}

	inline static int32_t get_offset_of_New_296() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___New_296)); }
	inline String_t* get_New_296() const { return ___New_296; }
	inline String_t** get_address_of_New_296() { return &___New_296; }
	inline void set_New_296(String_t* value)
	{
		___New_296 = value;
		Il2CppCodeGenWriteBarrier((&___New_296), value);
	}

	inline static int32_t get_offset_of_NewCategory_297() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewCategory_297)); }
	inline String_t* get_NewCategory_297() const { return ___NewCategory_297; }
	inline String_t** get_address_of_NewCategory_297() { return &___NewCategory_297; }
	inline void set_NewCategory_297(String_t* value)
	{
		___NewCategory_297 = value;
		Il2CppCodeGenWriteBarrier((&___NewCategory_297), value);
	}

	inline static int32_t get_offset_of_NewCategoryNameCannotBeEmpty_298() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewCategoryNameCannotBeEmpty_298)); }
	inline String_t* get_NewCategoryNameCannotBeEmpty_298() const { return ___NewCategoryNameCannotBeEmpty_298; }
	inline String_t** get_address_of_NewCategoryNameCannotBeEmpty_298() { return &___NewCategoryNameCannotBeEmpty_298; }
	inline void set_NewCategoryNameCannotBeEmpty_298(String_t* value)
	{
		___NewCategoryNameCannotBeEmpty_298 = value;
		Il2CppCodeGenWriteBarrier((&___NewCategoryNameCannotBeEmpty_298), value);
	}

	inline static int32_t get_offset_of_NewDatabase_299() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewDatabase_299)); }
	inline String_t* get_NewDatabase_299() const { return ___NewDatabase_299; }
	inline String_t** get_address_of_NewDatabase_299() { return &___NewDatabase_299; }
	inline void set_NewDatabase_299(String_t* value)
	{
		___NewDatabase_299 = value;
		Il2CppCodeGenWriteBarrier((&___NewDatabase_299), value);
	}

	inline static int32_t get_offset_of_NewPopup_300() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewPopup_300)); }
	inline String_t* get_NewPopup_300() const { return ___NewPopup_300; }
	inline String_t** get_address_of_NewPopup_300() { return &___NewPopup_300; }
	inline void set_NewPopup_300(String_t* value)
	{
		___NewPopup_300 = value;
		Il2CppCodeGenWriteBarrier((&___NewPopup_300), value);
	}

	inline static int32_t get_offset_of_NewPreset_301() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewPreset_301)); }
	inline String_t* get_NewPreset_301() const { return ___NewPreset_301; }
	inline String_t** get_address_of_NewPreset_301() { return &___NewPreset_301; }
	inline void set_NewPreset_301(String_t* value)
	{
		___NewPreset_301 = value;
		Il2CppCodeGenWriteBarrier((&___NewPreset_301), value);
	}

	inline static int32_t get_offset_of_NewPresetNameCannotBeEmpty_302() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewPresetNameCannotBeEmpty_302)); }
	inline String_t* get_NewPresetNameCannotBeEmpty_302() const { return ___NewPresetNameCannotBeEmpty_302; }
	inline String_t** get_address_of_NewPresetNameCannotBeEmpty_302() { return &___NewPresetNameCannotBeEmpty_302; }
	inline void set_NewPresetNameCannotBeEmpty_302(String_t* value)
	{
		___NewPresetNameCannotBeEmpty_302 = value;
		Il2CppCodeGenWriteBarrier((&___NewPresetNameCannotBeEmpty_302), value);
	}

	inline static int32_t get_offset_of_NewSoundDatabase_303() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NewSoundDatabase_303)); }
	inline String_t* get_NewSoundDatabase_303() const { return ___NewSoundDatabase_303; }
	inline String_t** get_address_of_NewSoundDatabase_303() { return &___NewSoundDatabase_303; }
	inline void set_NewSoundDatabase_303(String_t* value)
	{
		___NewSoundDatabase_303 = value;
		Il2CppCodeGenWriteBarrier((&___NewSoundDatabase_303), value);
	}

	inline static int32_t get_offset_of_News_304() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___News_304)); }
	inline String_t* get_News_304() const { return ___News_304; }
	inline String_t** get_address_of_News_304() { return &___News_304; }
	inline void set_News_304(String_t* value)
	{
		___News_304 = value;
		Il2CppCodeGenWriteBarrier((&___News_304), value);
	}

	inline static int32_t get_offset_of_No_305() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___No_305)); }
	inline String_t* get_No_305() const { return ___No_305; }
	inline String_t** get_address_of_No_305() { return &___No_305; }
	inline void set_No_305(String_t* value)
	{
		___No_305 = value;
		Il2CppCodeGenWriteBarrier((&___No_305), value);
	}

	inline static int32_t get_offset_of_NoAnimationEnabled_306() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoAnimationEnabled_306)); }
	inline String_t* get_NoAnimationEnabled_306() const { return ___NoAnimationEnabled_306; }
	inline String_t** get_address_of_NoAnimationEnabled_306() { return &___NoAnimationEnabled_306; }
	inline void set_NoAnimationEnabled_306(String_t* value)
	{
		___NoAnimationEnabled_306 = value;
		Il2CppCodeGenWriteBarrier((&___NoAnimationEnabled_306), value);
	}

	inline static int32_t get_offset_of_NoAnimatorFound_307() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoAnimatorFound_307)); }
	inline String_t* get_NoAnimatorFound_307() const { return ___NoAnimatorFound_307; }
	inline String_t** get_address_of_NoAnimatorFound_307() { return &___NoAnimatorFound_307; }
	inline void set_NoAnimatorFound_307(String_t* value)
	{
		___NoAnimatorFound_307 = value;
		Il2CppCodeGenWriteBarrier((&___NoAnimatorFound_307), value);
	}

	inline static int32_t get_offset_of_NoGraphReferenced_308() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoGraphReferenced_308)); }
	inline String_t* get_NoGraphReferenced_308() const { return ___NoGraphReferenced_308; }
	inline String_t** get_address_of_NoGraphReferenced_308() { return &___NoGraphReferenced_308; }
	inline void set_NoGraphReferenced_308(String_t* value)
	{
		___NoGraphReferenced_308 = value;
		Il2CppCodeGenWriteBarrier((&___NoGraphReferenced_308), value);
	}

	inline static int32_t get_offset_of_NoSound_309() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSound_309)); }
	inline String_t* get_NoSound_309() const { return ___NoSound_309; }
	inline String_t** get_address_of_NoSound_309() { return &___NoSound_309; }
	inline void set_NoSound_309(String_t* value)
	{
		___NoSound_309 = value;
		Il2CppCodeGenWriteBarrier((&___NoSound_309), value);
	}

	inline static int32_t get_offset_of_NoSoundsHaveBeenAdded_310() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSoundsHaveBeenAdded_310)); }
	inline String_t* get_NoSoundsHaveBeenAdded_310() const { return ___NoSoundsHaveBeenAdded_310; }
	inline String_t** get_address_of_NoSoundsHaveBeenAdded_310() { return &___NoSoundsHaveBeenAdded_310; }
	inline void set_NoSoundsHaveBeenAdded_310(String_t* value)
	{
		___NoSoundsHaveBeenAdded_310 = value;
		Il2CppCodeGenWriteBarrier((&___NoSoundsHaveBeenAdded_310), value);
	}

	inline static int32_t get_offset_of_Node_311() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Node_311)); }
	inline String_t* get_Node_311() const { return ___Node_311; }
	inline String_t** get_address_of_Node_311() { return &___Node_311; }
	inline void set_Node_311(String_t* value)
	{
		___Node_311 = value;
		Il2CppCodeGenWriteBarrier((&___Node_311), value);
	}

	inline static int32_t get_offset_of_NodeId_312() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodeId_312)); }
	inline String_t* get_NodeId_312() const { return ___NodeId_312; }
	inline String_t** get_address_of_NodeId_312() { return &___NodeId_312; }
	inline void set_NodeId_312(String_t* value)
	{
		___NodeId_312 = value;
		Il2CppCodeGenWriteBarrier((&___NodeId_312), value);
	}

	inline static int32_t get_offset_of_NodeName_313() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodeName_313)); }
	inline String_t* get_NodeName_313() const { return ___NodeName_313; }
	inline String_t** get_address_of_NodeName_313() { return &___NodeName_313; }
	inline void set_NodeName_313(String_t* value)
	{
		___NodeName_313 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_313), value);
	}

	inline static int32_t get_offset_of_NodeNameTooltip_314() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodeNameTooltip_314)); }
	inline String_t* get_NodeNameTooltip_314() const { return ___NodeNameTooltip_314; }
	inline String_t** get_address_of_NodeNameTooltip_314() { return &___NodeNameTooltip_314; }
	inline void set_NodeNameTooltip_314(String_t* value)
	{
		___NodeNameTooltip_314 = value;
		Il2CppCodeGenWriteBarrier((&___NodeNameTooltip_314), value);
	}

	inline static int32_t get_offset_of_NodeState_315() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodeState_315)); }
	inline String_t* get_NodeState_315() const { return ___NodeState_315; }
	inline String_t** get_address_of_NodeState_315() { return &___NodeState_315; }
	inline void set_NodeState_315(String_t* value)
	{
		___NodeState_315 = value;
		Il2CppCodeGenWriteBarrier((&___NodeState_315), value);
	}

	inline static int32_t get_offset_of_NodeWidth_316() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodeWidth_316)); }
	inline String_t* get_NodeWidth_316() const { return ___NodeWidth_316; }
	inline String_t** get_address_of_NodeWidth_316() { return &___NodeWidth_316; }
	inline void set_NodeWidth_316(String_t* value)
	{
		___NodeWidth_316 = value;
		Il2CppCodeGenWriteBarrier((&___NodeWidth_316), value);
	}

	inline static int32_t get_offset_of_Nodes_317() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Nodes_317)); }
	inline String_t* get_Nodes_317() const { return ___Nodes_317; }
	inline String_t** get_address_of_Nodes_317() { return &___Nodes_317; }
	inline void set_Nodes_317(String_t* value)
	{
		___Nodes_317 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_317), value);
	}

	inline static int32_t get_offset_of_NodySettings_318() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NodySettings_318)); }
	inline String_t* get_NodySettings_318() const { return ___NodySettings_318; }
	inline String_t** get_address_of_NodySettings_318() { return &___NodySettings_318; }
	inline void set_NodySettings_318(String_t* value)
	{
		___NodySettings_318 = value;
		Il2CppCodeGenWriteBarrier((&___NodySettings_318), value);
	}

	inline static int32_t get_offset_of_NormalLoopAnimation_319() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NormalLoopAnimation_319)); }
	inline String_t* get_NormalLoopAnimation_319() const { return ___NormalLoopAnimation_319; }
	inline String_t** get_address_of_NormalLoopAnimation_319() { return &___NormalLoopAnimation_319; }
	inline void set_NormalLoopAnimation_319(String_t* value)
	{
		___NormalLoopAnimation_319 = value;
		Il2CppCodeGenWriteBarrier((&___NormalLoopAnimation_319), value);
	}

	inline static int32_t get_offset_of_NotConnected_320() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotConnected_320)); }
	inline String_t* get_NotConnected_320() const { return ___NotConnected_320; }
	inline String_t** get_address_of_NotConnected_320() { return &___NotConnected_320; }
	inline void set_NotConnected_320(String_t* value)
	{
		___NotConnected_320 = value;
		Il2CppCodeGenWriteBarrier((&___NotConnected_320), value);
	}

	inline static int32_t get_offset_of_NotInstalled_321() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotInstalled_321)); }
	inline String_t* get_NotInstalled_321() const { return ___NotInstalled_321; }
	inline String_t** get_address_of_NotInstalled_321() { return &___NotInstalled_321; }
	inline void set_NotInstalled_321(String_t* value)
	{
		___NotInstalled_321 = value;
		Il2CppCodeGenWriteBarrier((&___NotInstalled_321), value);
	}

	inline static int32_t get_offset_of_NoteworthyInformation_322() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoteworthyInformation_322)); }
	inline String_t* get_NoteworthyInformation_322() const { return ___NoteworthyInformation_322; }
	inline String_t** get_address_of_NoteworthyInformation_322() { return &___NoteworthyInformation_322; }
	inline void set_NoteworthyInformation_322(String_t* value)
	{
		___NoteworthyInformation_322 = value;
		Il2CppCodeGenWriteBarrier((&___NoteworthyInformation_322), value);
	}

	inline static int32_t get_offset_of_NumberOfLoops_323() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NumberOfLoops_323)); }
	inline String_t* get_NumberOfLoops_323() const { return ___NumberOfLoops_323; }
	inline String_t** get_address_of_NumberOfLoops_323() { return &___NumberOfLoops_323; }
	inline void set_NumberOfLoops_323(String_t* value)
	{
		___NumberOfLoops_323 = value;
		Il2CppCodeGenWriteBarrier((&___NumberOfLoops_323), value);
	}

	inline static int32_t get_offset_of_Ok_324() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Ok_324)); }
	inline String_t* get_Ok_324() const { return ___Ok_324; }
	inline String_t** get_address_of_Ok_324() { return &___Ok_324; }
	inline void set_Ok_324(String_t* value)
	{
		___Ok_324 = value;
		Il2CppCodeGenWriteBarrier((&___Ok_324), value);
	}

	inline static int32_t get_offset_of_OnAnimationFinished_325() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnAnimationFinished_325)); }
	inline String_t* get_OnAnimationFinished_325() const { return ___OnAnimationFinished_325; }
	inline String_t** get_address_of_OnAnimationFinished_325() { return &___OnAnimationFinished_325; }
	inline void set_OnAnimationFinished_325(String_t* value)
	{
		___OnAnimationFinished_325 = value;
		Il2CppCodeGenWriteBarrier((&___OnAnimationFinished_325), value);
	}

	inline static int32_t get_offset_of_OnAnimationStart_326() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnAnimationStart_326)); }
	inline String_t* get_OnAnimationStart_326() const { return ___OnAnimationStart_326; }
	inline String_t** get_address_of_OnAnimationStart_326() { return &___OnAnimationStart_326; }
	inline void set_OnAnimationStart_326(String_t* value)
	{
		___OnAnimationStart_326 = value;
		Il2CppCodeGenWriteBarrier((&___OnAnimationStart_326), value);
	}

	inline static int32_t get_offset_of_OnClick_327() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnClick_327)); }
	inline String_t* get_OnClick_327() const { return ___OnClick_327; }
	inline String_t** get_address_of_OnClick_327() { return &___OnClick_327; }
	inline void set_OnClick_327(String_t* value)
	{
		___OnClick_327 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_327), value);
	}

	inline static int32_t get_offset_of_OnDeselected_328() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnDeselected_328)); }
	inline String_t* get_OnDeselected_328() const { return ___OnDeselected_328; }
	inline String_t** get_address_of_OnDeselected_328() { return &___OnDeselected_328; }
	inline void set_OnDeselected_328(String_t* value)
	{
		___OnDeselected_328 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselected_328), value);
	}

	inline static int32_t get_offset_of_OnDoubleClick_329() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnDoubleClick_329)); }
	inline String_t* get_OnDoubleClick_329() const { return ___OnDoubleClick_329; }
	inline String_t** get_address_of_OnDoubleClick_329() { return &___OnDoubleClick_329; }
	inline void set_OnDoubleClick_329(String_t* value)
	{
		___OnDoubleClick_329 = value;
		Il2CppCodeGenWriteBarrier((&___OnDoubleClick_329), value);
	}

	inline static int32_t get_offset_of_OnEnter_330() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnEnter_330)); }
	inline String_t* get_OnEnter_330() const { return ___OnEnter_330; }
	inline String_t** get_address_of_OnEnter_330() { return &___OnEnter_330; }
	inline void set_OnEnter_330(String_t* value)
	{
		___OnEnter_330 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnter_330), value);
	}

	inline static int32_t get_offset_of_OnEnterNode_331() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnEnterNode_331)); }
	inline String_t* get_OnEnterNode_331() const { return ___OnEnterNode_331; }
	inline String_t** get_address_of_OnEnterNode_331() { return &___OnEnterNode_331; }
	inline void set_OnEnterNode_331(String_t* value)
	{
		___OnEnterNode_331 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnterNode_331), value);
	}

	inline static int32_t get_offset_of_OnExit_332() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnExit_332)); }
	inline String_t* get_OnExit_332() const { return ___OnExit_332; }
	inline String_t** get_address_of_OnExit_332() { return &___OnExit_332; }
	inline void set_OnExit_332(String_t* value)
	{
		___OnExit_332 = value;
		Il2CppCodeGenWriteBarrier((&___OnExit_332), value);
	}

	inline static int32_t get_offset_of_OnExitNode_333() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnExitNode_333)); }
	inline String_t* get_OnExitNode_333() const { return ___OnExitNode_333; }
	inline String_t** get_address_of_OnExitNode_333() { return &___OnExitNode_333; }
	inline void set_OnExitNode_333(String_t* value)
	{
		___OnExitNode_333 = value;
		Il2CppCodeGenWriteBarrier((&___OnExitNode_333), value);
	}

	inline static int32_t get_offset_of_OnFixedUpdate_334() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnFixedUpdate_334)); }
	inline String_t* get_OnFixedUpdate_334() const { return ___OnFixedUpdate_334; }
	inline String_t** get_address_of_OnFixedUpdate_334() { return &___OnFixedUpdate_334; }
	inline void set_OnFixedUpdate_334(String_t* value)
	{
		___OnFixedUpdate_334 = value;
		Il2CppCodeGenWriteBarrier((&___OnFixedUpdate_334), value);
	}

	inline static int32_t get_offset_of_OnLateUpdate_335() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnLateUpdate_335)); }
	inline String_t* get_OnLateUpdate_335() const { return ___OnLateUpdate_335; }
	inline String_t** get_address_of_OnLateUpdate_335() { return &___OnLateUpdate_335; }
	inline void set_OnLateUpdate_335(String_t* value)
	{
		___OnLateUpdate_335 = value;
		Il2CppCodeGenWriteBarrier((&___OnLateUpdate_335), value);
	}

	inline static int32_t get_offset_of_OnLoadScene_336() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnLoadScene_336)); }
	inline String_t* get_OnLoadScene_336() const { return ___OnLoadScene_336; }
	inline String_t** get_address_of_OnLoadScene_336() { return &___OnLoadScene_336; }
	inline void set_OnLoadScene_336(String_t* value)
	{
		___OnLoadScene_336 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadScene_336), value);
	}

	inline static int32_t get_offset_of_OnLongClick_337() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnLongClick_337)); }
	inline String_t* get_OnLongClick_337() const { return ___OnLongClick_337; }
	inline String_t** get_address_of_OnLongClick_337() { return &___OnLongClick_337; }
	inline void set_OnLongClick_337(String_t* value)
	{
		___OnLongClick_337 = value;
		Il2CppCodeGenWriteBarrier((&___OnLongClick_337), value);
	}

	inline static int32_t get_offset_of_OnNodeFixedUpdate_338() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnNodeFixedUpdate_338)); }
	inline String_t* get_OnNodeFixedUpdate_338() const { return ___OnNodeFixedUpdate_338; }
	inline String_t** get_address_of_OnNodeFixedUpdate_338() { return &___OnNodeFixedUpdate_338; }
	inline void set_OnNodeFixedUpdate_338(String_t* value)
	{
		___OnNodeFixedUpdate_338 = value;
		Il2CppCodeGenWriteBarrier((&___OnNodeFixedUpdate_338), value);
	}

	inline static int32_t get_offset_of_OnNodeLateUpdate_339() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnNodeLateUpdate_339)); }
	inline String_t* get_OnNodeLateUpdate_339() const { return ___OnNodeLateUpdate_339; }
	inline String_t** get_address_of_OnNodeLateUpdate_339() { return &___OnNodeLateUpdate_339; }
	inline void set_OnNodeLateUpdate_339(String_t* value)
	{
		___OnNodeLateUpdate_339 = value;
		Il2CppCodeGenWriteBarrier((&___OnNodeLateUpdate_339), value);
	}

	inline static int32_t get_offset_of_OnNodeUpdate_340() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnNodeUpdate_340)); }
	inline String_t* get_OnNodeUpdate_340() const { return ___OnNodeUpdate_340; }
	inline String_t** get_address_of_OnNodeUpdate_340() { return &___OnNodeUpdate_340; }
	inline void set_OnNodeUpdate_340(String_t* value)
	{
		___OnNodeUpdate_340 = value;
		Il2CppCodeGenWriteBarrier((&___OnNodeUpdate_340), value);
	}

	inline static int32_t get_offset_of_OnPointerDown_341() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnPointerDown_341)); }
	inline String_t* get_OnPointerDown_341() const { return ___OnPointerDown_341; }
	inline String_t** get_address_of_OnPointerDown_341() { return &___OnPointerDown_341; }
	inline void set_OnPointerDown_341(String_t* value)
	{
		___OnPointerDown_341 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerDown_341), value);
	}

	inline static int32_t get_offset_of_OnPointerEnter_342() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnPointerEnter_342)); }
	inline String_t* get_OnPointerEnter_342() const { return ___OnPointerEnter_342; }
	inline String_t** get_address_of_OnPointerEnter_342() { return &___OnPointerEnter_342; }
	inline void set_OnPointerEnter_342(String_t* value)
	{
		___OnPointerEnter_342 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_342), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_343() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnPointerExit_343)); }
	inline String_t* get_OnPointerExit_343() const { return ___OnPointerExit_343; }
	inline String_t** get_address_of_OnPointerExit_343() { return &___OnPointerExit_343; }
	inline void set_OnPointerExit_343(String_t* value)
	{
		___OnPointerExit_343 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_343), value);
	}

	inline static int32_t get_offset_of_OnPointerUp_344() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnPointerUp_344)); }
	inline String_t* get_OnPointerUp_344() const { return ___OnPointerUp_344; }
	inline String_t** get_address_of_OnPointerUp_344() { return &___OnPointerUp_344; }
	inline void set_OnPointerUp_344(String_t* value)
	{
		___OnPointerUp_344 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerUp_344), value);
	}

	inline static int32_t get_offset_of_OnSceneLoaded_345() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnSceneLoaded_345)); }
	inline String_t* get_OnSceneLoaded_345() const { return ___OnSceneLoaded_345; }
	inline String_t** get_address_of_OnSceneLoaded_345() { return &___OnSceneLoaded_345; }
	inline void set_OnSceneLoaded_345(String_t* value)
	{
		___OnSceneLoaded_345 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneLoaded_345), value);
	}

	inline static int32_t get_offset_of_OnSelected_346() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnSelected_346)); }
	inline String_t* get_OnSelected_346() const { return ___OnSelected_346; }
	inline String_t** get_address_of_OnSelected_346() { return &___OnSelected_346; }
	inline void set_OnSelected_346(String_t* value)
	{
		___OnSelected_346 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelected_346), value);
	}

	inline static int32_t get_offset_of_OnTrigger_347() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnTrigger_347)); }
	inline String_t* get_OnTrigger_347() const { return ___OnTrigger_347; }
	inline String_t** get_address_of_OnTrigger_347() { return &___OnTrigger_347; }
	inline void set_OnTrigger_347(String_t* value)
	{
		___OnTrigger_347 = value;
		Il2CppCodeGenWriteBarrier((&___OnTrigger_347), value);
	}

	inline static int32_t get_offset_of_OnUpdate_348() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OnUpdate_348)); }
	inline String_t* get_OnUpdate_348() const { return ___OnUpdate_348; }
	inline String_t** get_address_of_OnUpdate_348() { return &___OnUpdate_348; }
	inline void set_OnUpdate_348(String_t* value)
	{
		___OnUpdate_348 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdate_348), value);
	}

	inline static int32_t get_offset_of_Open_349() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Open_349)); }
	inline String_t* get_Open_349() const { return ___Open_349; }
	inline String_t** get_address_of_Open_349() { return &___Open_349; }
	inline void set_Open_349(String_t* value)
	{
		___Open_349 = value;
		Il2CppCodeGenWriteBarrier((&___Open_349), value);
	}

	inline static int32_t get_offset_of_OpenControlPanel_350() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenControlPanel_350)); }
	inline String_t* get_OpenControlPanel_350() const { return ___OpenControlPanel_350; }
	inline String_t** get_address_of_OpenControlPanel_350() { return &___OpenControlPanel_350; }
	inline void set_OpenControlPanel_350(String_t* value)
	{
		___OpenControlPanel_350 = value;
		Il2CppCodeGenWriteBarrier((&___OpenControlPanel_350), value);
	}

	inline static int32_t get_offset_of_OpenDatabase_351() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenDatabase_351)); }
	inline String_t* get_OpenDatabase_351() const { return ___OpenDatabase_351; }
	inline String_t** get_address_of_OpenDatabase_351() { return &___OpenDatabase_351; }
	inline void set_OpenDatabase_351(String_t* value)
	{
		___OpenDatabase_351 = value;
		Il2CppCodeGenWriteBarrier((&___OpenDatabase_351), value);
	}

	inline static int32_t get_offset_of_OpenDrawer_352() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenDrawer_352)); }
	inline String_t* get_OpenDrawer_352() const { return ___OpenDrawer_352; }
	inline String_t** get_address_of_OpenDrawer_352() { return &___OpenDrawer_352; }
	inline void set_OpenDrawer_352(String_t* value)
	{
		___OpenDrawer_352 = value;
		Il2CppCodeGenWriteBarrier((&___OpenDrawer_352), value);
	}

	inline static int32_t get_offset_of_OpenGraph_353() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenGraph_353)); }
	inline String_t* get_OpenGraph_353() const { return ___OpenGraph_353; }
	inline String_t** get_address_of_OpenGraph_353() { return &___OpenGraph_353; }
	inline void set_OpenGraph_353(String_t* value)
	{
		___OpenGraph_353 = value;
		Il2CppCodeGenWriteBarrier((&___OpenGraph_353), value);
	}

	inline static int32_t get_offset_of_OpenNody_354() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenNody_354)); }
	inline String_t* get_OpenNody_354() const { return ___OpenNody_354; }
	inline String_t** get_address_of_OpenNody_354() { return &___OpenNody_354; }
	inline void set_OpenNody_354(String_t* value)
	{
		___OpenNody_354 = value;
		Il2CppCodeGenWriteBarrier((&___OpenNody_354), value);
	}

	inline static int32_t get_offset_of_OpenSpeed_355() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenSpeed_355)); }
	inline String_t* get_OpenSpeed_355() const { return ___OpenSpeed_355; }
	inline String_t** get_address_of_OpenSpeed_355() { return &___OpenSpeed_355; }
	inline void set_OpenSpeed_355(String_t* value)
	{
		___OpenSpeed_355 = value;
		Il2CppCodeGenWriteBarrier((&___OpenSpeed_355), value);
	}

	inline static int32_t get_offset_of_OpenSubGraph_356() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenSubGraph_356)); }
	inline String_t* get_OpenSubGraph_356() const { return ___OpenSubGraph_356; }
	inline String_t** get_address_of_OpenSubGraph_356() { return &___OpenSubGraph_356; }
	inline void set_OpenSubGraph_356(String_t* value)
	{
		___OpenSubGraph_356 = value;
		Il2CppCodeGenWriteBarrier((&___OpenSubGraph_356), value);
	}

	inline static int32_t get_offset_of_Opened_357() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Opened_357)); }
	inline String_t* get_Opened_357() const { return ___Opened_357; }
	inline String_t** get_address_of_Opened_357() { return &___Opened_357; }
	inline void set_Opened_357(String_t* value)
	{
		___Opened_357 = value;
		Il2CppCodeGenWriteBarrier((&___Opened_357), value);
	}

	inline static int32_t get_offset_of_OpenedPosition_358() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OpenedPosition_358)); }
	inline String_t* get_OpenedPosition_358() const { return ___OpenedPosition_358; }
	inline String_t** get_address_of_OpenedPosition_358() { return &___OpenedPosition_358; }
	inline void set_OpenedPosition_358(String_t* value)
	{
		___OpenedPosition_358 = value;
		Il2CppCodeGenWriteBarrier((&___OpenedPosition_358), value);
	}

	inline static int32_t get_offset_of_OperationCannotBeUndone_359() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OperationCannotBeUndone_359)); }
	inline String_t* get_OperationCannotBeUndone_359() const { return ___OperationCannotBeUndone_359; }
	inline String_t** get_address_of_OperationCannotBeUndone_359() { return &___OperationCannotBeUndone_359; }
	inline void set_OperationCannotBeUndone_359(String_t* value)
	{
		___OperationCannotBeUndone_359 = value;
		Il2CppCodeGenWriteBarrier((&___OperationCannotBeUndone_359), value);
	}

	inline static int32_t get_offset_of_OrientationDetectorDescription_360() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OrientationDetectorDescription_360)); }
	inline String_t* get_OrientationDetectorDescription_360() const { return ___OrientationDetectorDescription_360; }
	inline String_t** get_address_of_OrientationDetectorDescription_360() { return &___OrientationDetectorDescription_360; }
	inline void set_OrientationDetectorDescription_360(String_t* value)
	{
		___OrientationDetectorDescription_360 = value;
		Il2CppCodeGenWriteBarrier((&___OrientationDetectorDescription_360), value);
	}

	inline static int32_t get_offset_of_OtherReferences_361() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OtherReferences_361)); }
	inline String_t* get_OtherReferences_361() const { return ___OtherReferences_361; }
	inline String_t** get_address_of_OtherReferences_361() { return &___OtherReferences_361; }
	inline void set_OtherReferences_361(String_t* value)
	{
		___OtherReferences_361 = value;
		Il2CppCodeGenWriteBarrier((&___OtherReferences_361), value);
	}

	inline static int32_t get_offset_of_OutputAudioMixerGroup_362() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OutputAudioMixerGroup_362)); }
	inline String_t* get_OutputAudioMixerGroup_362() const { return ___OutputAudioMixerGroup_362; }
	inline String_t** get_address_of_OutputAudioMixerGroup_362() { return &___OutputAudioMixerGroup_362; }
	inline void set_OutputAudioMixerGroup_362(String_t* value)
	{
		___OutputAudioMixerGroup_362 = value;
		Il2CppCodeGenWriteBarrier((&___OutputAudioMixerGroup_362), value);
	}

	inline static int32_t get_offset_of_OutputConnected_363() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OutputConnected_363)); }
	inline String_t* get_OutputConnected_363() const { return ___OutputConnected_363; }
	inline String_t** get_address_of_OutputConnected_363() { return &___OutputConnected_363; }
	inline void set_OutputConnected_363(String_t* value)
	{
		___OutputConnected_363 = value;
		Il2CppCodeGenWriteBarrier((&___OutputConnected_363), value);
	}

	inline static int32_t get_offset_of_OutputConnections_364() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OutputConnections_364)); }
	inline String_t* get_OutputConnections_364() const { return ___OutputConnections_364; }
	inline String_t** get_address_of_OutputConnections_364() { return &___OutputConnections_364; }
	inline void set_OutputConnections_364(String_t* value)
	{
		___OutputConnections_364 = value;
		Il2CppCodeGenWriteBarrier((&___OutputConnections_364), value);
	}

	inline static int32_t get_offset_of_OutputMixerGroup_365() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OutputMixerGroup_365)); }
	inline String_t* get_OutputMixerGroup_365() const { return ___OutputMixerGroup_365; }
	inline String_t** get_address_of_OutputMixerGroup_365() { return &___OutputMixerGroup_365; }
	inline void set_OutputMixerGroup_365(String_t* value)
	{
		___OutputMixerGroup_365 = value;
		Il2CppCodeGenWriteBarrier((&___OutputMixerGroup_365), value);
	}

	inline static int32_t get_offset_of_OutputNotConnected_366() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OutputNotConnected_366)); }
	inline String_t* get_OutputNotConnected_366() const { return ___OutputNotConnected_366; }
	inline String_t** get_address_of_OutputNotConnected_366() { return &___OutputNotConnected_366; }
	inline void set_OutputNotConnected_366(String_t* value)
	{
		___OutputNotConnected_366 = value;
		Il2CppCodeGenWriteBarrier((&___OutputNotConnected_366), value);
	}

	inline static int32_t get_offset_of_Overlay_367() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Overlay_367)); }
	inline String_t* get_Overlay_367() const { return ___Overlay_367; }
	inline String_t** get_address_of_Overlay_367() { return &___Overlay_367; }
	inline void set_Overlay_367(String_t* value)
	{
		___Overlay_367 = value;
		Il2CppCodeGenWriteBarrier((&___Overlay_367), value);
	}

	inline static int32_t get_offset_of_Override_368() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Override_368)); }
	inline String_t* get_Override_368() const { return ___Override_368; }
	inline String_t** get_address_of_Override_368() { return &___Override_368; }
	inline void set_Override_368(String_t* value)
	{
		___Override_368 = value;
		Il2CppCodeGenWriteBarrier((&___Override_368), value);
	}

	inline static int32_t get_offset_of_OverrideColor_369() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OverrideColor_369)); }
	inline String_t* get_OverrideColor_369() const { return ___OverrideColor_369; }
	inline String_t** get_address_of_OverrideColor_369() { return &___OverrideColor_369; }
	inline void set_OverrideColor_369(String_t* value)
	{
		___OverrideColor_369 = value;
		Il2CppCodeGenWriteBarrier((&___OverrideColor_369), value);
	}

	inline static int32_t get_offset_of_Overview_370() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Overview_370)); }
	inline String_t* get_Overview_370() const { return ___Overview_370; }
	inline String_t** get_address_of_Overview_370() { return &___Overview_370; }
	inline void set_Overview_370(String_t* value)
	{
		___Overview_370 = value;
		Il2CppCodeGenWriteBarrier((&___Overview_370), value);
	}

	inline static int32_t get_offset_of_OverviewZoom_371() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___OverviewZoom_371)); }
	inline String_t* get_OverviewZoom_371() const { return ___OverviewZoom_371; }
	inline String_t** get_address_of_OverviewZoom_371() { return &___OverviewZoom_371; }
	inline void set_OverviewZoom_371(String_t* value)
	{
		___OverviewZoom_371 = value;
		Il2CppCodeGenWriteBarrier((&___OverviewZoom_371), value);
	}

	inline static int32_t get_offset_of_ParameterName_372() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ParameterName_372)); }
	inline String_t* get_ParameterName_372() const { return ___ParameterName_372; }
	inline String_t** get_address_of_ParameterName_372() { return &___ParameterName_372; }
	inline void set_ParameterName_372(String_t* value)
	{
		___ParameterName_372 = value;
		Il2CppCodeGenWriteBarrier((&___ParameterName_372), value);
	}

	inline static int32_t get_offset_of_ParameterType_373() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ParameterType_373)); }
	inline String_t* get_ParameterType_373() const { return ___ParameterType_373; }
	inline String_t** get_address_of_ParameterType_373() { return &___ParameterType_373; }
	inline void set_ParameterType_373(String_t* value)
	{
		___ParameterType_373 = value;
		Il2CppCodeGenWriteBarrier((&___ParameterType_373), value);
	}

	inline static int32_t get_offset_of_ParticleSystem_374() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ParticleSystem_374)); }
	inline String_t* get_ParticleSystem_374() const { return ___ParticleSystem_374; }
	inline String_t** get_address_of_ParticleSystem_374() { return &___ParticleSystem_374; }
	inline void set_ParticleSystem_374(String_t* value)
	{
		___ParticleSystem_374 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleSystem_374), value);
	}

	inline static int32_t get_offset_of_Paste_375() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Paste_375)); }
	inline String_t* get_Paste_375() const { return ___Paste_375; }
	inline String_t** get_address_of_Paste_375() { return &___Paste_375; }
	inline void set_Paste_375(String_t* value)
	{
		___Paste_375 = value;
		Il2CppCodeGenWriteBarrier((&___Paste_375), value);
	}

	inline static int32_t get_offset_of_PauseAllSounds_376() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PauseAllSounds_376)); }
	inline String_t* get_PauseAllSounds_376() const { return ___PauseAllSounds_376; }
	inline String_t** get_address_of_PauseAllSounds_376() { return &___PauseAllSounds_376; }
	inline void set_PauseAllSounds_376(String_t* value)
	{
		___PauseAllSounds_376 = value;
		Il2CppCodeGenWriteBarrier((&___PauseAllSounds_376), value);
	}

	inline static int32_t get_offset_of_PercentageOfScreenZeroToOne_377() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PercentageOfScreenZeroToOne_377)); }
	inline String_t* get_PercentageOfScreenZeroToOne_377() const { return ___PercentageOfScreenZeroToOne_377; }
	inline String_t** get_address_of_PercentageOfScreenZeroToOne_377() { return &___PercentageOfScreenZeroToOne_377; }
	inline void set_PercentageOfScreenZeroToOne_377(String_t* value)
	{
		___PercentageOfScreenZeroToOne_377 = value;
		Il2CppCodeGenWriteBarrier((&___PercentageOfScreenZeroToOne_377), value);
	}

	inline static int32_t get_offset_of_PitchSemitones_378() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PitchSemitones_378)); }
	inline String_t* get_PitchSemitones_378() const { return ___PitchSemitones_378; }
	inline String_t** get_address_of_PitchSemitones_378() { return &___PitchSemitones_378; }
	inline void set_PitchSemitones_378(String_t* value)
	{
		___PitchSemitones_378 = value;
		Il2CppCodeGenWriteBarrier((&___PitchSemitones_378), value);
	}

	inline static int32_t get_offset_of_Play_379() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Play_379)); }
	inline String_t* get_Play_379() const { return ___Play_379; }
	inline String_t** get_address_of_Play_379() { return &___Play_379; }
	inline void set_Play_379(String_t* value)
	{
		___Play_379 = value;
		Il2CppCodeGenWriteBarrier((&___Play_379), value);
	}

	inline static int32_t get_offset_of_PlayAnimationInZeroSeconds_380() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PlayAnimationInZeroSeconds_380)); }
	inline String_t* get_PlayAnimationInZeroSeconds_380() const { return ___PlayAnimationInZeroSeconds_380; }
	inline String_t** get_address_of_PlayAnimationInZeroSeconds_380() { return &___PlayAnimationInZeroSeconds_380; }
	inline void set_PlayAnimationInZeroSeconds_380(String_t* value)
	{
		___PlayAnimationInZeroSeconds_380 = value;
		Il2CppCodeGenWriteBarrier((&___PlayAnimationInZeroSeconds_380), value);
	}

	inline static int32_t get_offset_of_PlayMode_381() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PlayMode_381)); }
	inline String_t* get_PlayMode_381() const { return ___PlayMode_381; }
	inline String_t** get_address_of_PlayMode_381() { return &___PlayMode_381; }
	inline void set_PlayMode_381(String_t* value)
	{
		___PlayMode_381 = value;
		Il2CppCodeGenWriteBarrier((&___PlayMode_381), value);
	}

	inline static int32_t get_offset_of_PlaySound_382() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PlaySound_382)); }
	inline String_t* get_PlaySound_382() const { return ___PlaySound_382; }
	inline String_t** get_address_of_PlaySound_382() { return &___PlaySound_382; }
	inline void set_PlaySound_382(String_t* value)
	{
		___PlaySound_382 = value;
		Il2CppCodeGenWriteBarrier((&___PlaySound_382), value);
	}

	inline static int32_t get_offset_of_PleaseEnterNewName_383() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PleaseEnterNewName_383)); }
	inline String_t* get_PleaseEnterNewName_383() const { return ___PleaseEnterNewName_383; }
	inline String_t** get_address_of_PleaseEnterNewName_383() { return &___PleaseEnterNewName_383; }
	inline void set_PleaseEnterNewName_383(String_t* value)
	{
		___PleaseEnterNewName_383 = value;
		Il2CppCodeGenWriteBarrier((&___PleaseEnterNewName_383), value);
	}

	inline static int32_t get_offset_of_PopupName_384() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PopupName_384)); }
	inline String_t* get_PopupName_384() const { return ___PopupName_384; }
	inline String_t** get_address_of_PopupName_384() { return &___PopupName_384; }
	inline void set_PopupName_384(String_t* value)
	{
		___PopupName_384 = value;
		Il2CppCodeGenWriteBarrier((&___PopupName_384), value);
	}

	inline static int32_t get_offset_of_PopupPrefab_385() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PopupPrefab_385)); }
	inline String_t* get_PopupPrefab_385() const { return ___PopupPrefab_385; }
	inline String_t** get_address_of_PopupPrefab_385() { return &___PopupPrefab_385; }
	inline void set_PopupPrefab_385(String_t* value)
	{
		___PopupPrefab_385 = value;
		Il2CppCodeGenWriteBarrier((&___PopupPrefab_385), value);
	}

	inline static int32_t get_offset_of_Portal_386() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Portal_386)); }
	inline String_t* get_Portal_386() const { return ___Portal_386; }
	inline String_t** get_address_of_Portal_386() { return &___Portal_386; }
	inline void set_Portal_386(String_t* value)
	{
		___Portal_386 = value;
		Il2CppCodeGenWriteBarrier((&___Portal_386), value);
	}

	inline static int32_t get_offset_of_PortalNodeName_387() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PortalNodeName_387)); }
	inline String_t* get_PortalNodeName_387() const { return ___PortalNodeName_387; }
	inline String_t** get_address_of_PortalNodeName_387() { return &___PortalNodeName_387; }
	inline void set_PortalNodeName_387(String_t* value)
	{
		___PortalNodeName_387 = value;
		Il2CppCodeGenWriteBarrier((&___PortalNodeName_387), value);
	}

	inline static int32_t get_offset_of_Prefix_388() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Prefix_388)); }
	inline String_t* get_Prefix_388() const { return ___Prefix_388; }
	inline String_t** get_address_of_Prefix_388() { return &___Prefix_388; }
	inline void set_Prefix_388(String_t* value)
	{
		___Prefix_388 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_388), value);
	}

	inline static int32_t get_offset_of_PresetCategory_389() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PresetCategory_389)); }
	inline String_t* get_PresetCategory_389() const { return ___PresetCategory_389; }
	inline String_t** get_address_of_PresetCategory_389() { return &___PresetCategory_389; }
	inline void set_PresetCategory_389(String_t* value)
	{
		___PresetCategory_389 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_389), value);
	}

	inline static int32_t get_offset_of_PresetName_390() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PresetName_390)); }
	inline String_t* get_PresetName_390() const { return ___PresetName_390; }
	inline String_t** get_address_of_PresetName_390() { return &___PresetName_390; }
	inline void set_PresetName_390(String_t* value)
	{
		___PresetName_390 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_390), value);
	}

	inline static int32_t get_offset_of_PreviewAnimation_391() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PreviewAnimation_391)); }
	inline String_t* get_PreviewAnimation_391() const { return ___PreviewAnimation_391; }
	inline String_t** get_address_of_PreviewAnimation_391() { return &___PreviewAnimation_391; }
	inline void set_PreviewAnimation_391(String_t* value)
	{
		___PreviewAnimation_391 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewAnimation_391), value);
	}

	inline static int32_t get_offset_of_Progress_392() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Progress_392)); }
	inline String_t* get_Progress_392() const { return ___Progress_392; }
	inline String_t** get_address_of_Progress_392() { return &___Progress_392; }
	inline void set_Progress_392(String_t* value)
	{
		___Progress_392 = value;
		Il2CppCodeGenWriteBarrier((&___Progress_392), value);
	}

	inline static int32_t get_offset_of_ProgressTargets_393() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ProgressTargets_393)); }
	inline String_t* get_ProgressTargets_393() const { return ___ProgressTargets_393; }
	inline String_t** get_address_of_ProgressTargets_393() { return &___ProgressTargets_393; }
	inline void set_ProgressTargets_393(String_t* value)
	{
		___ProgressTargets_393 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressTargets_393), value);
	}

	inline static int32_t get_offset_of_Progressor_394() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Progressor_394)); }
	inline String_t* get_Progressor_394() const { return ___Progressor_394; }
	inline String_t** get_address_of_Progressor_394() { return &___Progressor_394; }
	inline void set_Progressor_394(String_t* value)
	{
		___Progressor_394 = value;
		Il2CppCodeGenWriteBarrier((&___Progressor_394), value);
	}

	inline static int32_t get_offset_of_Progressors_395() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Progressors_395)); }
	inline String_t* get_Progressors_395() const { return ___Progressors_395; }
	inline String_t** get_address_of_Progressors_395() { return &___Progressors_395; }
	inline void set_Progressors_395(String_t* value)
	{
		___Progressors_395 = value;
		Il2CppCodeGenWriteBarrier((&___Progressors_395), value);
	}

	inline static int32_t get_offset_of_PunchBy_396() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___PunchBy_396)); }
	inline String_t* get_PunchBy_396() const { return ___PunchBy_396; }
	inline String_t** get_address_of_PunchBy_396() { return &___PunchBy_396; }
	inline void set_PunchBy_396(String_t* value)
	{
		___PunchBy_396 = value;
		Il2CppCodeGenWriteBarrier((&___PunchBy_396), value);
	}

	inline static int32_t get_offset_of_RandomDuration_397() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RandomDuration_397)); }
	inline String_t* get_RandomDuration_397() const { return ___RandomDuration_397; }
	inline String_t** get_address_of_RandomDuration_397() { return &___RandomDuration_397; }
	inline void set_RandomDuration_397(String_t* value)
	{
		___RandomDuration_397 = value;
		Il2CppCodeGenWriteBarrier((&___RandomDuration_397), value);
	}

	inline static int32_t get_offset_of_RandomNodeName_398() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RandomNodeName_398)); }
	inline String_t* get_RandomNodeName_398() const { return ___RandomNodeName_398; }
	inline String_t** get_address_of_RandomNodeName_398() { return &___RandomNodeName_398; }
	inline void set_RandomNodeName_398(String_t* value)
	{
		___RandomNodeName_398 = value;
		Il2CppCodeGenWriteBarrier((&___RandomNodeName_398), value);
	}

	inline static int32_t get_offset_of_Recent_399() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Recent_399)); }
	inline String_t* get_Recent_399() const { return ___Recent_399; }
	inline String_t** get_address_of_Recent_399() { return &___Recent_399; }
	inline void set_Recent_399(String_t* value)
	{
		___Recent_399 = value;
		Il2CppCodeGenWriteBarrier((&___Recent_399), value);
	}

	inline static int32_t get_offset_of_Refresh_400() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Refresh_400)); }
	inline String_t* get_Refresh_400() const { return ___Refresh_400; }
	inline String_t** get_address_of_Refresh_400() { return &___Refresh_400; }
	inline void set_Refresh_400(String_t* value)
	{
		___Refresh_400 = value;
		Il2CppCodeGenWriteBarrier((&___Refresh_400), value);
	}

	inline static int32_t get_offset_of_RefreshDatabase_401() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RefreshDatabase_401)); }
	inline String_t* get_RefreshDatabase_401() const { return ___RefreshDatabase_401; }
	inline String_t** get_address_of_RefreshDatabase_401() { return &___RefreshDatabase_401; }
	inline void set_RefreshDatabase_401(String_t* value)
	{
		___RefreshDatabase_401 = value;
		Il2CppCodeGenWriteBarrier((&___RefreshDatabase_401), value);
	}

	inline static int32_t get_offset_of_RegisterInterval_402() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RegisterInterval_402)); }
	inline String_t* get_RegisterInterval_402() const { return ___RegisterInterval_402; }
	inline String_t** get_address_of_RegisterInterval_402() { return &___RegisterInterval_402; }
	inline void set_RegisterInterval_402(String_t* value)
	{
		___RegisterInterval_402 = value;
		Il2CppCodeGenWriteBarrier((&___RegisterInterval_402), value);
	}

	inline static int32_t get_offset_of_Remove_403() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Remove_403)); }
	inline String_t* get_Remove_403() const { return ___Remove_403; }
	inline String_t** get_address_of_Remove_403() { return &___Remove_403; }
	inline void set_Remove_403(String_t* value)
	{
		___Remove_403 = value;
		Il2CppCodeGenWriteBarrier((&___Remove_403), value);
	}

	inline static int32_t get_offset_of_RemoveCategory_404() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveCategory_404)); }
	inline String_t* get_RemoveCategory_404() const { return ___RemoveCategory_404; }
	inline String_t** get_address_of_RemoveCategory_404() { return &___RemoveCategory_404; }
	inline void set_RemoveCategory_404(String_t* value)
	{
		___RemoveCategory_404 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveCategory_404), value);
	}

	inline static int32_t get_offset_of_RemoveDuplicates_405() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveDuplicates_405)); }
	inline String_t* get_RemoveDuplicates_405() const { return ___RemoveDuplicates_405; }
	inline String_t** get_address_of_RemoveDuplicates_405() { return &___RemoveDuplicates_405; }
	inline void set_RemoveDuplicates_405(String_t* value)
	{
		___RemoveDuplicates_405 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveDuplicates_405), value);
	}

	inline static int32_t get_offset_of_RemoveEmptyCategories_406() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveEmptyCategories_406)); }
	inline String_t* get_RemoveEmptyCategories_406() const { return ___RemoveEmptyCategories_406; }
	inline String_t** get_address_of_RemoveEmptyCategories_406() { return &___RemoveEmptyCategories_406; }
	inline void set_RemoveEmptyCategories_406(String_t* value)
	{
		___RemoveEmptyCategories_406 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveEmptyCategories_406), value);
	}

	inline static int32_t get_offset_of_RemoveEmptyEntries_407() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveEmptyEntries_407)); }
	inline String_t* get_RemoveEmptyEntries_407() const { return ___RemoveEmptyEntries_407; }
	inline String_t** get_address_of_RemoveEmptyEntries_407() { return &___RemoveEmptyEntries_407; }
	inline void set_RemoveEmptyEntries_407(String_t* value)
	{
		___RemoveEmptyEntries_407 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveEmptyEntries_407), value);
	}

	inline static int32_t get_offset_of_RemoveItem_408() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveItem_408)); }
	inline String_t* get_RemoveItem_408() const { return ___RemoveItem_408; }
	inline String_t** get_address_of_RemoveItem_408() { return &___RemoveItem_408; }
	inline void set_RemoveItem_408(String_t* value)
	{
		___RemoveItem_408 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveItem_408), value);
	}

	inline static int32_t get_offset_of_RemoveNullEntries_409() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveNullEntries_409)); }
	inline String_t* get_RemoveNullEntries_409() const { return ___RemoveNullEntries_409; }
	inline String_t** get_address_of_RemoveNullEntries_409() { return &___RemoveNullEntries_409; }
	inline void set_RemoveNullEntries_409(String_t* value)
	{
		___RemoveNullEntries_409 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveNullEntries_409), value);
	}

	inline static int32_t get_offset_of_RemoveSymbolFromAllBuildTargetGroups_410() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemoveSymbolFromAllBuildTargetGroups_410)); }
	inline String_t* get_RemoveSymbolFromAllBuildTargetGroups_410() const { return ___RemoveSymbolFromAllBuildTargetGroups_410; }
	inline String_t** get_address_of_RemoveSymbolFromAllBuildTargetGroups_410() { return &___RemoveSymbolFromAllBuildTargetGroups_410; }
	inline void set_RemoveSymbolFromAllBuildTargetGroups_410(String_t* value)
	{
		___RemoveSymbolFromAllBuildTargetGroups_410 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveSymbolFromAllBuildTargetGroups_410), value);
	}

	inline static int32_t get_offset_of_RemovedDuplicateEntries_411() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemovedDuplicateEntries_411)); }
	inline String_t* get_RemovedDuplicateEntries_411() const { return ___RemovedDuplicateEntries_411; }
	inline String_t** get_address_of_RemovedDuplicateEntries_411() { return &___RemovedDuplicateEntries_411; }
	inline void set_RemovedDuplicateEntries_411(String_t* value)
	{
		___RemovedDuplicateEntries_411 = value;
		Il2CppCodeGenWriteBarrier((&___RemovedDuplicateEntries_411), value);
	}

	inline static int32_t get_offset_of_RemovedEntriesWithNoName_412() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemovedEntriesWithNoName_412)); }
	inline String_t* get_RemovedEntriesWithNoName_412() const { return ___RemovedEntriesWithNoName_412; }
	inline String_t** get_address_of_RemovedEntriesWithNoName_412() { return &___RemovedEntriesWithNoName_412; }
	inline void set_RemovedEntriesWithNoName_412(String_t* value)
	{
		___RemovedEntriesWithNoName_412 = value;
		Il2CppCodeGenWriteBarrier((&___RemovedEntriesWithNoName_412), value);
	}

	inline static int32_t get_offset_of_RemovedEntriesWithNullPrefabs_413() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemovedEntriesWithNullPrefabs_413)); }
	inline String_t* get_RemovedEntriesWithNullPrefabs_413() const { return ___RemovedEntriesWithNullPrefabs_413; }
	inline String_t** get_address_of_RemovedEntriesWithNullPrefabs_413() { return &___RemovedEntriesWithNullPrefabs_413; }
	inline void set_RemovedEntriesWithNullPrefabs_413(String_t* value)
	{
		___RemovedEntriesWithNullPrefabs_413 = value;
		Il2CppCodeGenWriteBarrier((&___RemovedEntriesWithNullPrefabs_413), value);
	}

	inline static int32_t get_offset_of_RemovedEntry_414() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemovedEntry_414)); }
	inline String_t* get_RemovedEntry_414() const { return ___RemovedEntry_414; }
	inline String_t** get_address_of_RemovedEntry_414() { return &___RemovedEntry_414; }
	inline void set_RemovedEntry_414(String_t* value)
	{
		___RemovedEntry_414 = value;
		Il2CppCodeGenWriteBarrier((&___RemovedEntry_414), value);
	}

	inline static int32_t get_offset_of_RemovedNullEntries_415() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RemovedNullEntries_415)); }
	inline String_t* get_RemovedNullEntries_415() const { return ___RemovedNullEntries_415; }
	inline String_t** get_address_of_RemovedNullEntries_415() { return &___RemovedNullEntries_415; }
	inline void set_RemovedNullEntries_415(String_t* value)
	{
		___RemovedNullEntries_415 = value;
		Il2CppCodeGenWriteBarrier((&___RemovedNullEntries_415), value);
	}

	inline static int32_t get_offset_of_Rename_416() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Rename_416)); }
	inline String_t* get_Rename_416() const { return ___Rename_416; }
	inline String_t** get_address_of_Rename_416() { return &___Rename_416; }
	inline void set_Rename_416(String_t* value)
	{
		___Rename_416 = value;
		Il2CppCodeGenWriteBarrier((&___Rename_416), value);
	}

	inline static int32_t get_offset_of_RenameCategory_417() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameCategory_417)); }
	inline String_t* get_RenameCategory_417() const { return ___RenameCategory_417; }
	inline String_t** get_address_of_RenameCategory_417() { return &___RenameCategory_417; }
	inline void set_RenameCategory_417(String_t* value)
	{
		___RenameCategory_417 = value;
		Il2CppCodeGenWriteBarrier((&___RenameCategory_417), value);
	}

	inline static int32_t get_offset_of_RenameCategoryDialogMessage_418() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameCategoryDialogMessage_418)); }
	inline String_t* get_RenameCategoryDialogMessage_418() const { return ___RenameCategoryDialogMessage_418; }
	inline String_t** get_address_of_RenameCategoryDialogMessage_418() { return &___RenameCategoryDialogMessage_418; }
	inline void set_RenameCategoryDialogMessage_418(String_t* value)
	{
		___RenameCategoryDialogMessage_418 = value;
		Il2CppCodeGenWriteBarrier((&___RenameCategoryDialogMessage_418), value);
	}

	inline static int32_t get_offset_of_RenameGameObjectTo_419() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameGameObjectTo_419)); }
	inline String_t* get_RenameGameObjectTo_419() const { return ___RenameGameObjectTo_419; }
	inline String_t** get_address_of_RenameGameObjectTo_419() { return &___RenameGameObjectTo_419; }
	inline void set_RenameGameObjectTo_419(String_t* value)
	{
		___RenameGameObjectTo_419 = value;
		Il2CppCodeGenWriteBarrier((&___RenameGameObjectTo_419), value);
	}

	inline static int32_t get_offset_of_RenameNodeTo_420() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameNodeTo_420)); }
	inline String_t* get_RenameNodeTo_420() const { return ___RenameNodeTo_420; }
	inline String_t** get_address_of_RenameNodeTo_420() { return &___RenameNodeTo_420; }
	inline void set_RenameNodeTo_420(String_t* value)
	{
		___RenameNodeTo_420 = value;
		Il2CppCodeGenWriteBarrier((&___RenameNodeTo_420), value);
	}

	inline static int32_t get_offset_of_RenamePrefix_421() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenamePrefix_421)); }
	inline String_t* get_RenamePrefix_421() const { return ___RenamePrefix_421; }
	inline String_t** get_address_of_RenamePrefix_421() { return &___RenamePrefix_421; }
	inline void set_RenamePrefix_421(String_t* value)
	{
		___RenamePrefix_421 = value;
		Il2CppCodeGenWriteBarrier((&___RenamePrefix_421), value);
	}

	inline static int32_t get_offset_of_RenameSoundDatabase_422() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameSoundDatabase_422)); }
	inline String_t* get_RenameSoundDatabase_422() const { return ___RenameSoundDatabase_422; }
	inline String_t** get_address_of_RenameSoundDatabase_422() { return &___RenameSoundDatabase_422; }
	inline void set_RenameSoundDatabase_422(String_t* value)
	{
		___RenameSoundDatabase_422 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSoundDatabase_422), value);
	}

	inline static int32_t get_offset_of_RenameSoundDatabaseDialogMessage_423() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameSoundDatabaseDialogMessage_423)); }
	inline String_t* get_RenameSoundDatabaseDialogMessage_423() const { return ___RenameSoundDatabaseDialogMessage_423; }
	inline String_t** get_address_of_RenameSoundDatabaseDialogMessage_423() { return &___RenameSoundDatabaseDialogMessage_423; }
	inline void set_RenameSoundDatabaseDialogMessage_423(String_t* value)
	{
		___RenameSoundDatabaseDialogMessage_423 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSoundDatabaseDialogMessage_423), value);
	}

	inline static int32_t get_offset_of_RenameSuffix_424() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameSuffix_424)); }
	inline String_t* get_RenameSuffix_424() const { return ___RenameSuffix_424; }
	inline String_t** get_address_of_RenameSuffix_424() { return &___RenameSuffix_424; }
	inline void set_RenameSuffix_424(String_t* value)
	{
		___RenameSuffix_424 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSuffix_424), value);
	}

	inline static int32_t get_offset_of_RenameTo_425() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RenameTo_425)); }
	inline String_t* get_RenameTo_425() const { return ___RenameTo_425; }
	inline String_t** get_address_of_RenameTo_425() { return &___RenameTo_425; }
	inline void set_RenameTo_425(String_t* value)
	{
		___RenameTo_425 = value;
		Il2CppCodeGenWriteBarrier((&___RenameTo_425), value);
	}

	inline static int32_t get_offset_of_Reset_426() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Reset_426)); }
	inline String_t* get_Reset_426() const { return ___Reset_426; }
	inline String_t** get_address_of_Reset_426() { return &___Reset_426; }
	inline void set_Reset_426(String_t* value)
	{
		___Reset_426 = value;
		Il2CppCodeGenWriteBarrier((&___Reset_426), value);
	}

	inline static int32_t get_offset_of_ResetAnimationSettings_427() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetAnimationSettings_427)); }
	inline String_t* get_ResetAnimationSettings_427() const { return ___ResetAnimationSettings_427; }
	inline String_t** get_address_of_ResetAnimationSettings_427() { return &___ResetAnimationSettings_427; }
	inline void set_ResetAnimationSettings_427(String_t* value)
	{
		___ResetAnimationSettings_427 = value;
		Il2CppCodeGenWriteBarrier((&___ResetAnimationSettings_427), value);
	}

	inline static int32_t get_offset_of_ResetClosedPosition_428() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetClosedPosition_428)); }
	inline String_t* get_ResetClosedPosition_428() const { return ___ResetClosedPosition_428; }
	inline String_t** get_address_of_ResetClosedPosition_428() { return &___ResetClosedPosition_428; }
	inline void set_ResetClosedPosition_428(String_t* value)
	{
		___ResetClosedPosition_428 = value;
		Il2CppCodeGenWriteBarrier((&___ResetClosedPosition_428), value);
	}

	inline static int32_t get_offset_of_ResetDatabase_429() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetDatabase_429)); }
	inline String_t* get_ResetDatabase_429() const { return ___ResetDatabase_429; }
	inline String_t** get_address_of_ResetDatabase_429() { return &___ResetDatabase_429; }
	inline void set_ResetDatabase_429(String_t* value)
	{
		___ResetDatabase_429 = value;
		Il2CppCodeGenWriteBarrier((&___ResetDatabase_429), value);
	}

	inline static int32_t get_offset_of_ResetOpenedPosition_430() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetOpenedPosition_430)); }
	inline String_t* get_ResetOpenedPosition_430() const { return ___ResetOpenedPosition_430; }
	inline String_t** get_address_of_ResetOpenedPosition_430() { return &___ResetOpenedPosition_430; }
	inline void set_ResetOpenedPosition_430(String_t* value)
	{
		___ResetOpenedPosition_430 = value;
		Il2CppCodeGenWriteBarrier((&___ResetOpenedPosition_430), value);
	}

	inline static int32_t get_offset_of_ResetPosition_431() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetPosition_431)); }
	inline String_t* get_ResetPosition_431() const { return ___ResetPosition_431; }
	inline String_t** get_address_of_ResetPosition_431() { return &___ResetPosition_431; }
	inline void set_ResetPosition_431(String_t* value)
	{
		___ResetPosition_431 = value;
		Il2CppCodeGenWriteBarrier((&___ResetPosition_431), value);
	}

	inline static int32_t get_offset_of_ResetRoot_432() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetRoot_432)); }
	inline String_t* get_ResetRoot_432() const { return ___ResetRoot_432; }
	inline String_t** get_address_of_ResetRoot_432() { return &___ResetRoot_432; }
	inline void set_ResetRoot_432(String_t* value)
	{
		___ResetRoot_432 = value;
		Il2CppCodeGenWriteBarrier((&___ResetRoot_432), value);
	}

	inline static int32_t get_offset_of_ResetTrigger_433() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetTrigger_433)); }
	inline String_t* get_ResetTrigger_433() const { return ___ResetTrigger_433; }
	inline String_t** get_address_of_ResetTrigger_433() { return &___ResetTrigger_433; }
	inline void set_ResetTrigger_433(String_t* value)
	{
		___ResetTrigger_433 = value;
		Il2CppCodeGenWriteBarrier((&___ResetTrigger_433), value);
	}

	inline static int32_t get_offset_of_ResetValue_434() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ResetValue_434)); }
	inline String_t* get_ResetValue_434() const { return ___ResetValue_434; }
	inline String_t** get_address_of_ResetValue_434() { return &___ResetValue_434; }
	inline void set_ResetValue_434(String_t* value)
	{
		___ResetValue_434 = value;
		Il2CppCodeGenWriteBarrier((&___ResetValue_434), value);
	}

	inline static int32_t get_offset_of_ReversedProgress_435() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ReversedProgress_435)); }
	inline String_t* get_ReversedProgress_435() const { return ___ReversedProgress_435; }
	inline String_t** get_address_of_ReversedProgress_435() { return &___ReversedProgress_435; }
	inline void set_ReversedProgress_435(String_t* value)
	{
		___ReversedProgress_435 = value;
		Il2CppCodeGenWriteBarrier((&___ReversedProgress_435), value);
	}

	inline static int32_t get_offset_of_Right_436() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Right_436)); }
	inline String_t* get_Right_436() const { return ___Right_436; }
	inline String_t** get_address_of_Right_436() { return &___Right_436; }
	inline void set_Right_436(String_t* value)
	{
		___Right_436 = value;
		Il2CppCodeGenWriteBarrier((&___Right_436), value);
	}

	inline static int32_t get_offset_of_Rotate_437() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Rotate_437)); }
	inline String_t* get_Rotate_437() const { return ___Rotate_437; }
	inline String_t** get_address_of_Rotate_437() { return &___Rotate_437; }
	inline void set_Rotate_437(String_t* value)
	{
		___Rotate_437 = value;
		Il2CppCodeGenWriteBarrier((&___Rotate_437), value);
	}

	inline static int32_t get_offset_of_RotateBy_438() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RotateBy_438)); }
	inline String_t* get_RotateBy_438() const { return ___RotateBy_438; }
	inline String_t** get_address_of_RotateBy_438() { return &___RotateBy_438; }
	inline void set_RotateBy_438(String_t* value)
	{
		___RotateBy_438 = value;
		Il2CppCodeGenWriteBarrier((&___RotateBy_438), value);
	}

	inline static int32_t get_offset_of_RotateFrom_439() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RotateFrom_439)); }
	inline String_t* get_RotateFrom_439() const { return ___RotateFrom_439; }
	inline String_t** get_address_of_RotateFrom_439() { return &___RotateFrom_439; }
	inline void set_RotateFrom_439(String_t* value)
	{
		___RotateFrom_439 = value;
		Il2CppCodeGenWriteBarrier((&___RotateFrom_439), value);
	}

	inline static int32_t get_offset_of_RotateMode_440() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RotateMode_440)); }
	inline String_t* get_RotateMode_440() const { return ___RotateMode_440; }
	inline String_t** get_address_of_RotateMode_440() { return &___RotateMode_440; }
	inline void set_RotateMode_440(String_t* value)
	{
		___RotateMode_440 = value;
		Il2CppCodeGenWriteBarrier((&___RotateMode_440), value);
	}

	inline static int32_t get_offset_of_RotateTo_441() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RotateTo_441)); }
	inline String_t* get_RotateTo_441() const { return ___RotateTo_441; }
	inline String_t** get_address_of_RotateTo_441() { return &___RotateTo_441; }
	inline void set_RotateTo_441(String_t* value)
	{
		___RotateTo_441 = value;
		Il2CppCodeGenWriteBarrier((&___RotateTo_441), value);
	}

	inline static int32_t get_offset_of_RuntimeOptions_442() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RuntimeOptions_442)); }
	inline String_t* get_RuntimeOptions_442() const { return ___RuntimeOptions_442; }
	inline String_t** get_address_of_RuntimeOptions_442() { return &___RuntimeOptions_442; }
	inline void set_RuntimeOptions_442(String_t* value)
	{
		___RuntimeOptions_442 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeOptions_442), value);
	}

	inline static int32_t get_offset_of_RuntimePreset_443() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___RuntimePreset_443)); }
	inline String_t* get_RuntimePreset_443() const { return ___RuntimePreset_443; }
	inline String_t** get_address_of_RuntimePreset_443() { return &___RuntimePreset_443; }
	inline void set_RuntimePreset_443(String_t* value)
	{
		___RuntimePreset_443 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimePreset_443), value);
	}

	inline static int32_t get_offset_of_Save_444() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Save_444)); }
	inline String_t* get_Save_444() const { return ___Save_444; }
	inline String_t** get_address_of_Save_444() { return &___Save_444; }
	inline void set_Save_444(String_t* value)
	{
		___Save_444 = value;
		Il2CppCodeGenWriteBarrier((&___Save_444), value);
	}

	inline static int32_t get_offset_of_SaveAs_445() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SaveAs_445)); }
	inline String_t* get_SaveAs_445() const { return ___SaveAs_445; }
	inline String_t** get_address_of_SaveAs_445() { return &___SaveAs_445; }
	inline void set_SaveAs_445(String_t* value)
	{
		___SaveAs_445 = value;
		Il2CppCodeGenWriteBarrier((&___SaveAs_445), value);
	}

	inline static int32_t get_offset_of_SavePreset_446() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SavePreset_446)); }
	inline String_t* get_SavePreset_446() const { return ___SavePreset_446; }
	inline String_t** get_address_of_SavePreset_446() { return &___SavePreset_446; }
	inline void set_SavePreset_446(String_t* value)
	{
		___SavePreset_446 = value;
		Il2CppCodeGenWriteBarrier((&___SavePreset_446), value);
	}

	inline static int32_t get_offset_of_Scale_447() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Scale_447)); }
	inline String_t* get_Scale_447() const { return ___Scale_447; }
	inline String_t** get_address_of_Scale_447() { return &___Scale_447; }
	inline void set_Scale_447(String_t* value)
	{
		___Scale_447 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_447), value);
	}

	inline static int32_t get_offset_of_ScaleBy_448() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ScaleBy_448)); }
	inline String_t* get_ScaleBy_448() const { return ___ScaleBy_448; }
	inline String_t** get_address_of_ScaleBy_448() { return &___ScaleBy_448; }
	inline void set_ScaleBy_448(String_t* value)
	{
		___ScaleBy_448 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleBy_448), value);
	}

	inline static int32_t get_offset_of_ScaleFrom_449() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ScaleFrom_449)); }
	inline String_t* get_ScaleFrom_449() const { return ___ScaleFrom_449; }
	inline String_t** get_address_of_ScaleFrom_449() { return &___ScaleFrom_449; }
	inline void set_ScaleFrom_449(String_t* value)
	{
		___ScaleFrom_449 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleFrom_449), value);
	}

	inline static int32_t get_offset_of_ScaleTo_450() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ScaleTo_450)); }
	inline String_t* get_ScaleTo_450() const { return ___ScaleTo_450; }
	inline String_t** get_address_of_ScaleTo_450() { return &___ScaleTo_450; }
	inline void set_ScaleTo_450(String_t* value)
	{
		___ScaleTo_450 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleTo_450), value);
	}

	inline static int32_t get_offset_of_Scene_451() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Scene_451)); }
	inline String_t* get_Scene_451() const { return ___Scene_451; }
	inline String_t** get_address_of_Scene_451() { return &___Scene_451; }
	inline void set_Scene_451(String_t* value)
	{
		___Scene_451 = value;
		Il2CppCodeGenWriteBarrier((&___Scene_451), value);
	}

	inline static int32_t get_offset_of_SceneActivationDelay_452() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SceneActivationDelay_452)); }
	inline String_t* get_SceneActivationDelay_452() const { return ___SceneActivationDelay_452; }
	inline String_t** get_address_of_SceneActivationDelay_452() { return &___SceneActivationDelay_452; }
	inline void set_SceneActivationDelay_452(String_t* value)
	{
		___SceneActivationDelay_452 = value;
		Il2CppCodeGenWriteBarrier((&___SceneActivationDelay_452), value);
	}

	inline static int32_t get_offset_of_SceneBuildIndex_453() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SceneBuildIndex_453)); }
	inline String_t* get_SceneBuildIndex_453() const { return ___SceneBuildIndex_453; }
	inline String_t** get_address_of_SceneBuildIndex_453() { return &___SceneBuildIndex_453; }
	inline void set_SceneBuildIndex_453(String_t* value)
	{
		___SceneBuildIndex_453 = value;
		Il2CppCodeGenWriteBarrier((&___SceneBuildIndex_453), value);
	}

	inline static int32_t get_offset_of_SceneLoad_454() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SceneLoad_454)); }
	inline String_t* get_SceneLoad_454() const { return ___SceneLoad_454; }
	inline String_t** get_address_of_SceneLoad_454() { return &___SceneLoad_454; }
	inline void set_SceneLoad_454(String_t* value)
	{
		___SceneLoad_454 = value;
		Il2CppCodeGenWriteBarrier((&___SceneLoad_454), value);
	}

	inline static int32_t get_offset_of_SceneName_455() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SceneName_455)); }
	inline String_t* get_SceneName_455() const { return ___SceneName_455; }
	inline String_t** get_address_of_SceneName_455() { return &___SceneName_455; }
	inline void set_SceneName_455(String_t* value)
	{
		___SceneName_455 = value;
		Il2CppCodeGenWriteBarrier((&___SceneName_455), value);
	}

	inline static int32_t get_offset_of_SceneUnload_456() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SceneUnload_456)); }
	inline String_t* get_SceneUnload_456() const { return ___SceneUnload_456; }
	inline String_t** get_address_of_SceneUnload_456() { return &___SceneUnload_456; }
	inline void set_SceneUnload_456(String_t* value)
	{
		___SceneUnload_456 = value;
		Il2CppCodeGenWriteBarrier((&___SceneUnload_456), value);
	}

	inline static int32_t get_offset_of_ScriptingDefineSymbol_457() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ScriptingDefineSymbol_457)); }
	inline String_t* get_ScriptingDefineSymbol_457() const { return ___ScriptingDefineSymbol_457; }
	inline String_t** get_address_of_ScriptingDefineSymbol_457() { return &___ScriptingDefineSymbol_457; }
	inline void set_ScriptingDefineSymbol_457(String_t* value)
	{
		___ScriptingDefineSymbol_457 = value;
		Il2CppCodeGenWriteBarrier((&___ScriptingDefineSymbol_457), value);
	}

	inline static int32_t get_offset_of_Search_458() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Search_458)); }
	inline String_t* get_Search_458() const { return ___Search_458; }
	inline String_t** get_address_of_Search_458() { return &___Search_458; }
	inline void set_Search_458(String_t* value)
	{
		___Search_458 = value;
		Il2CppCodeGenWriteBarrier((&___Search_458), value);
	}

	inline static int32_t get_offset_of_SearchForCategories_459() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SearchForCategories_459)); }
	inline String_t* get_SearchForCategories_459() const { return ___SearchForCategories_459; }
	inline String_t** get_address_of_SearchForCategories_459() { return &___SearchForCategories_459; }
	inline void set_SearchForCategories_459(String_t* value)
	{
		___SearchForCategories_459 = value;
		Il2CppCodeGenWriteBarrier((&___SearchForCategories_459), value);
	}

	inline static int32_t get_offset_of_SearchForDatabases_460() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SearchForDatabases_460)); }
	inline String_t* get_SearchForDatabases_460() const { return ___SearchForDatabases_460; }
	inline String_t** get_address_of_SearchForDatabases_460() { return &___SearchForDatabases_460; }
	inline void set_SearchForDatabases_460(String_t* value)
	{
		___SearchForDatabases_460 = value;
		Il2CppCodeGenWriteBarrier((&___SearchForDatabases_460), value);
	}

	inline static int32_t get_offset_of_SearchForUIPopupLinks_461() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SearchForUIPopupLinks_461)); }
	inline String_t* get_SearchForUIPopupLinks_461() const { return ___SearchForUIPopupLinks_461; }
	inline String_t** get_address_of_SearchForUIPopupLinks_461() { return &___SearchForUIPopupLinks_461; }
	inline void set_SearchForUIPopupLinks_461(String_t* value)
	{
		___SearchForUIPopupLinks_461 = value;
		Il2CppCodeGenWriteBarrier((&___SearchForUIPopupLinks_461), value);
	}

	inline static int32_t get_offset_of_Seconds_462() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Seconds_462)); }
	inline String_t* get_Seconds_462() const { return ___Seconds_462; }
	inline String_t** get_address_of_Seconds_462() { return &___Seconds_462; }
	inline void set_Seconds_462(String_t* value)
	{
		___Seconds_462 = value;
		Il2CppCodeGenWriteBarrier((&___Seconds_462), value);
	}

	inline static int32_t get_offset_of_SecondsDelay_463() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SecondsDelay_463)); }
	inline String_t* get_SecondsDelay_463() const { return ___SecondsDelay_463; }
	inline String_t** get_address_of_SecondsDelay_463() { return &___SecondsDelay_463; }
	inline void set_SecondsDelay_463(String_t* value)
	{
		___SecondsDelay_463 = value;
		Il2CppCodeGenWriteBarrier((&___SecondsDelay_463), value);
	}

	inline static int32_t get_offset_of_Select_464() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Select_464)); }
	inline String_t* get_Select_464() const { return ___Select_464; }
	inline String_t** get_address_of_Select_464() { return &___Select_464; }
	inline void set_Select_464(String_t* value)
	{
		___Select_464 = value;
		Il2CppCodeGenWriteBarrier((&___Select_464), value);
	}

	inline static int32_t get_offset_of_SelectButton_465() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SelectButton_465)); }
	inline String_t* get_SelectButton_465() const { return ___SelectButton_465; }
	inline String_t** get_address_of_SelectButton_465() { return &___SelectButton_465; }
	inline void set_SelectButton_465(String_t* value)
	{
		___SelectButton_465 = value;
		Il2CppCodeGenWriteBarrier((&___SelectButton_465), value);
	}

	inline static int32_t get_offset_of_SelectSwipeDirection_466() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SelectSwipeDirection_466)); }
	inline String_t* get_SelectSwipeDirection_466() const { return ___SelectSwipeDirection_466; }
	inline String_t** get_address_of_SelectSwipeDirection_466() { return &___SelectSwipeDirection_466; }
	inline void set_SelectSwipeDirection_466(String_t* value)
	{
		___SelectSwipeDirection_466 = value;
		Il2CppCodeGenWriteBarrier((&___SelectSwipeDirection_466), value);
	}

	inline static int32_t get_offset_of_SelectedLoopAnimation_467() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SelectedLoopAnimation_467)); }
	inline String_t* get_SelectedLoopAnimation_467() const { return ___SelectedLoopAnimation_467; }
	inline String_t** get_address_of_SelectedLoopAnimation_467() { return &___SelectedLoopAnimation_467; }
	inline void set_SelectedLoopAnimation_467(String_t* value)
	{
		___SelectedLoopAnimation_467 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedLoopAnimation_467), value);
	}

	inline static int32_t get_offset_of_Send_468() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Send_468)); }
	inline String_t* get_Send_468() const { return ___Send_468; }
	inline String_t** get_address_of_Send_468() { return &___Send_468; }
	inline void set_Send_468(String_t* value)
	{
		___Send_468 = value;
		Il2CppCodeGenWriteBarrier((&___Send_468), value);
	}

	inline static int32_t get_offset_of_SendGameEvent_469() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SendGameEvent_469)); }
	inline String_t* get_SendGameEvent_469() const { return ___SendGameEvent_469; }
	inline String_t** get_address_of_SendGameEvent_469() { return &___SendGameEvent_469; }
	inline void set_SendGameEvent_469(String_t* value)
	{
		___SendGameEvent_469 = value;
		Il2CppCodeGenWriteBarrier((&___SendGameEvent_469), value);
	}

	inline static int32_t get_offset_of_SendGameEvents_470() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SendGameEvents_470)); }
	inline String_t* get_SendGameEvents_470() const { return ___SendGameEvents_470; }
	inline String_t** get_address_of_SendGameEvents_470() { return &___SendGameEvents_470; }
	inline void set_SendGameEvents_470(String_t* value)
	{
		___SendGameEvents_470 = value;
		Il2CppCodeGenWriteBarrier((&___SendGameEvents_470), value);
	}

	inline static int32_t get_offset_of_SetActiveNode_471() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetActiveNode_471)); }
	inline String_t* get_SetActiveNode_471() const { return ___SetActiveNode_471; }
	inline String_t** get_address_of_SetActiveNode_471() { return &___SetActiveNode_471; }
	inline void set_SetActiveNode_471(String_t* value)
	{
		___SetActiveNode_471 = value;
		Il2CppCodeGenWriteBarrier((&___SetActiveNode_471), value);
	}

	inline static int32_t get_offset_of_SetAsSoundName_472() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetAsSoundName_472)); }
	inline String_t* get_SetAsSoundName_472() const { return ___SetAsSoundName_472; }
	inline String_t** get_address_of_SetAsSoundName_472() { return &___SetAsSoundName_472; }
	inline void set_SetAsSoundName_472(String_t* value)
	{
		___SetAsSoundName_472 = value;
		Il2CppCodeGenWriteBarrier((&___SetAsSoundName_472), value);
	}

	inline static int32_t get_offset_of_SetBoolValueTo_473() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetBoolValueTo_473)); }
	inline String_t* get_SetBoolValueTo_473() const { return ___SetBoolValueTo_473; }
	inline String_t** get_address_of_SetBoolValueTo_473() { return &___SetBoolValueTo_473; }
	inline void set_SetBoolValueTo_473(String_t* value)
	{
		___SetBoolValueTo_473 = value;
		Il2CppCodeGenWriteBarrier((&___SetBoolValueTo_473), value);
	}

	inline static int32_t get_offset_of_SetFloatValueTo_474() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetFloatValueTo_474)); }
	inline String_t* get_SetFloatValueTo_474() const { return ___SetFloatValueTo_474; }
	inline String_t** get_address_of_SetFloatValueTo_474() { return &___SetFloatValueTo_474; }
	inline void set_SetFloatValueTo_474(String_t* value)
	{
		___SetFloatValueTo_474 = value;
		Il2CppCodeGenWriteBarrier((&___SetFloatValueTo_474), value);
	}

	inline static int32_t get_offset_of_SetIntValueTo_475() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetIntValueTo_475)); }
	inline String_t* get_SetIntValueTo_475() const { return ___SetIntValueTo_475; }
	inline String_t** get_address_of_SetIntValueTo_475() { return &___SetIntValueTo_475; }
	inline void set_SetIntValueTo_475(String_t* value)
	{
		___SetIntValueTo_475 = value;
		Il2CppCodeGenWriteBarrier((&___SetIntValueTo_475), value);
	}

	inline static int32_t get_offset_of_SetPosition_476() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetPosition_476)); }
	inline String_t* get_SetPosition_476() const { return ___SetPosition_476; }
	inline String_t** get_address_of_SetPosition_476() { return &___SetPosition_476; }
	inline void set_SetPosition_476(String_t* value)
	{
		___SetPosition_476 = value;
		Il2CppCodeGenWriteBarrier((&___SetPosition_476), value);
	}

	inline static int32_t get_offset_of_SetTargetGameObject_477() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetTargetGameObject_477)); }
	inline String_t* get_SetTargetGameObject_477() const { return ___SetTargetGameObject_477; }
	inline String_t** get_address_of_SetTargetGameObject_477() { return &___SetTargetGameObject_477; }
	inline void set_SetTargetGameObject_477(String_t* value)
	{
		___SetTargetGameObject_477 = value;
		Il2CppCodeGenWriteBarrier((&___SetTargetGameObject_477), value);
	}

	inline static int32_t get_offset_of_SetUIButtonToListenFor_478() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetUIButtonToListenFor_478)); }
	inline String_t* get_SetUIButtonToListenFor_478() const { return ___SetUIButtonToListenFor_478; }
	inline String_t** get_address_of_SetUIButtonToListenFor_478() { return &___SetUIButtonToListenFor_478; }
	inline void set_SetUIButtonToListenFor_478(String_t* value)
	{
		___SetUIButtonToListenFor_478 = value;
		Il2CppCodeGenWriteBarrier((&___SetUIButtonToListenFor_478), value);
	}

	inline static int32_t get_offset_of_SetUIDrawerToListenFor_479() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetUIDrawerToListenFor_479)); }
	inline String_t* get_SetUIDrawerToListenFor_479() const { return ___SetUIDrawerToListenFor_479; }
	inline String_t** get_address_of_SetUIDrawerToListenFor_479() { return &___SetUIDrawerToListenFor_479; }
	inline void set_SetUIDrawerToListenFor_479(String_t* value)
	{
		___SetUIDrawerToListenFor_479 = value;
		Il2CppCodeGenWriteBarrier((&___SetUIDrawerToListenFor_479), value);
	}

	inline static int32_t get_offset_of_SetUIViewToListenFor_480() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetUIViewToListenFor_480)); }
	inline String_t* get_SetUIViewToListenFor_480() const { return ___SetUIViewToListenFor_480; }
	inline String_t** get_address_of_SetUIViewToListenFor_480() { return &___SetUIViewToListenFor_480; }
	inline void set_SetUIViewToListenFor_480(String_t* value)
	{
		___SetUIViewToListenFor_480 = value;
		Il2CppCodeGenWriteBarrier((&___SetUIViewToListenFor_480), value);
	}

	inline static int32_t get_offset_of_SetValue_481() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SetValue_481)); }
	inline String_t* get_SetValue_481() const { return ___SetValue_481; }
	inline String_t** get_address_of_SetValue_481() { return &___SetValue_481; }
	inline void set_SetValue_481(String_t* value)
	{
		___SetValue_481 = value;
		Il2CppCodeGenWriteBarrier((&___SetValue_481), value);
	}

	inline static int32_t get_offset_of_Settings_482() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Settings_482)); }
	inline String_t* get_Settings_482() const { return ___Settings_482; }
	inline String_t** get_address_of_Settings_482() { return &___Settings_482; }
	inline void set_Settings_482(String_t* value)
	{
		___Settings_482 = value;
		Il2CppCodeGenWriteBarrier((&___Settings_482), value);
	}

	inline static int32_t get_offset_of_Show_483() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Show_483)); }
	inline String_t* get_Show_483() const { return ___Show_483; }
	inline String_t** get_address_of_Show_483() { return &___Show_483; }
	inline void set_Show_483(String_t* value)
	{
		___Show_483 = value;
		Il2CppCodeGenWriteBarrier((&___Show_483), value);
	}

	inline static int32_t get_offset_of_ShowAnimationWillNotWork_484() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowAnimationWillNotWork_484)); }
	inline String_t* get_ShowAnimationWillNotWork_484() const { return ___ShowAnimationWillNotWork_484; }
	inline String_t** get_address_of_ShowAnimationWillNotWork_484() { return &___ShowAnimationWillNotWork_484; }
	inline void set_ShowAnimationWillNotWork_484(String_t* value)
	{
		___ShowAnimationWillNotWork_484 = value;
		Il2CppCodeGenWriteBarrier((&___ShowAnimationWillNotWork_484), value);
	}

	inline static int32_t get_offset_of_ShowCurveModifier_485() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowCurveModifier_485)); }
	inline String_t* get_ShowCurveModifier_485() const { return ___ShowCurveModifier_485; }
	inline String_t** get_address_of_ShowCurveModifier_485() { return &___ShowCurveModifier_485; }
	inline void set_ShowCurveModifier_485(String_t* value)
	{
		___ShowCurveModifier_485 = value;
		Il2CppCodeGenWriteBarrier((&___ShowCurveModifier_485), value);
	}

	inline static int32_t get_offset_of_ShowPopup_486() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowPopup_486)); }
	inline String_t* get_ShowPopup_486() const { return ___ShowPopup_486; }
	inline String_t** get_address_of_ShowPopup_486() { return &___ShowPopup_486; }
	inline void set_ShowPopup_486(String_t* value)
	{
		___ShowPopup_486 = value;
		Il2CppCodeGenWriteBarrier((&___ShowPopup_486), value);
	}

	inline static int32_t get_offset_of_ShowProgressor_487() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowProgressor_487)); }
	inline String_t* get_ShowProgressor_487() const { return ___ShowProgressor_487; }
	inline String_t** get_address_of_ShowProgressor_487() { return &___ShowProgressor_487; }
	inline void set_ShowProgressor_487(String_t* value)
	{
		___ShowProgressor_487 = value;
		Il2CppCodeGenWriteBarrier((&___ShowProgressor_487), value);
	}

	inline static int32_t get_offset_of_ShowView_488() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowView_488)); }
	inline String_t* get_ShowView_488() const { return ___ShowView_488; }
	inline String_t** get_address_of_ShowView_488() { return &___ShowView_488; }
	inline void set_ShowView_488(String_t* value)
	{
		___ShowView_488 = value;
		Il2CppCodeGenWriteBarrier((&___ShowView_488), value);
	}

	inline static int32_t get_offset_of_ShowViews_489() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ShowViews_489)); }
	inline String_t* get_ShowViews_489() const { return ___ShowViews_489; }
	inline String_t** get_address_of_ShowViews_489() { return &___ShowViews_489; }
	inline void set_ShowViews_489(String_t* value)
	{
		___ShowViews_489 = value;
		Il2CppCodeGenWriteBarrier((&___ShowViews_489), value);
	}

	inline static int32_t get_offset_of_Simulate_490() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Simulate_490)); }
	inline String_t* get_Simulate_490() const { return ___Simulate_490; }
	inline String_t** get_address_of_Simulate_490() { return &___Simulate_490; }
	inline void set_Simulate_490(String_t* value)
	{
		___Simulate_490 = value;
		Il2CppCodeGenWriteBarrier((&___Simulate_490), value);
	}

	inline static int32_t get_offset_of_SocialLinks_491() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SocialLinks_491)); }
	inline String_t* get_SocialLinks_491() const { return ___SocialLinks_491; }
	inline String_t** get_address_of_SocialLinks_491() { return &___SocialLinks_491; }
	inline void set_SocialLinks_491(String_t* value)
	{
		___SocialLinks_491 = value;
		Il2CppCodeGenWriteBarrier((&___SocialLinks_491), value);
	}

	inline static int32_t get_offset_of_Socket_492() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Socket_492)); }
	inline String_t* get_Socket_492() const { return ___Socket_492; }
	inline String_t** get_address_of_Socket_492() { return &___Socket_492; }
	inline void set_Socket_492(String_t* value)
	{
		___Socket_492 = value;
		Il2CppCodeGenWriteBarrier((&___Socket_492), value);
	}

	inline static int32_t get_offset_of_Sort_493() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Sort_493)); }
	inline String_t* get_Sort_493() const { return ___Sort_493; }
	inline String_t** get_address_of_Sort_493() { return &___Sort_493; }
	inline void set_Sort_493(String_t* value)
	{
		___Sort_493 = value;
		Il2CppCodeGenWriteBarrier((&___Sort_493), value);
	}

	inline static int32_t get_offset_of_SortDatabase_494() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SortDatabase_494)); }
	inline String_t* get_SortDatabase_494() const { return ___SortDatabase_494; }
	inline String_t** get_address_of_SortDatabase_494() { return &___SortDatabase_494; }
	inline void set_SortDatabase_494(String_t* value)
	{
		___SortDatabase_494 = value;
		Il2CppCodeGenWriteBarrier((&___SortDatabase_494), value);
	}

	inline static int32_t get_offset_of_SortingSteps_495() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SortingSteps_495)); }
	inline String_t* get_SortingSteps_495() const { return ___SortingSteps_495; }
	inline String_t** get_address_of_SortingSteps_495() { return &___SortingSteps_495; }
	inline void set_SortingSteps_495(String_t* value)
	{
		___SortingSteps_495 = value;
		Il2CppCodeGenWriteBarrier((&___SortingSteps_495), value);
	}

	inline static int32_t get_offset_of_Sound_496() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Sound_496)); }
	inline String_t* get_Sound_496() const { return ___Sound_496; }
	inline String_t** get_address_of_Sound_496() { return &___Sound_496; }
	inline void set_Sound_496(String_t* value)
	{
		___Sound_496 = value;
		Il2CppCodeGenWriteBarrier((&___Sound_496), value);
	}

	inline static int32_t get_offset_of_SoundAction_497() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundAction_497)); }
	inline String_t* get_SoundAction_497() const { return ___SoundAction_497; }
	inline String_t** get_address_of_SoundAction_497() { return &___SoundAction_497; }
	inline void set_SoundAction_497(String_t* value)
	{
		___SoundAction_497 = value;
		Il2CppCodeGenWriteBarrier((&___SoundAction_497), value);
	}

	inline static int32_t get_offset_of_SoundDatabases_498() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundDatabases_498)); }
	inline String_t* get_SoundDatabases_498() const { return ___SoundDatabases_498; }
	inline String_t** get_address_of_SoundDatabases_498() { return &___SoundDatabases_498; }
	inline void set_SoundDatabases_498(String_t* value)
	{
		___SoundDatabases_498 = value;
		Il2CppCodeGenWriteBarrier((&___SoundDatabases_498), value);
	}

	inline static int32_t get_offset_of_SoundName_499() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundName_499)); }
	inline String_t* get_SoundName_499() const { return ___SoundName_499; }
	inline String_t** get_address_of_SoundName_499() { return &___SoundName_499; }
	inline void set_SoundName_499(String_t* value)
	{
		___SoundName_499 = value;
		Il2CppCodeGenWriteBarrier((&___SoundName_499), value);
	}

	inline static int32_t get_offset_of_SoundNodeName_500() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundNodeName_500)); }
	inline String_t* get_SoundNodeName_500() const { return ___SoundNodeName_500; }
	inline String_t** get_address_of_SoundNodeName_500() { return &___SoundNodeName_500; }
	inline void set_SoundNodeName_500(String_t* value)
	{
		___SoundNodeName_500 = value;
		Il2CppCodeGenWriteBarrier((&___SoundNodeName_500), value);
	}

	inline static int32_t get_offset_of_SoundSource_501() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundSource_501)); }
	inline String_t* get_SoundSource_501() const { return ___SoundSource_501; }
	inline String_t** get_address_of_SoundSource_501() { return &___SoundSource_501; }
	inline void set_SoundSource_501(String_t* value)
	{
		___SoundSource_501 = value;
		Il2CppCodeGenWriteBarrier((&___SoundSource_501), value);
	}

	inline static int32_t get_offset_of_Sounds_502() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Sounds_502)); }
	inline String_t* get_Sounds_502() const { return ___Sounds_502; }
	inline String_t** get_address_of_Sounds_502() { return &___Sounds_502; }
	inline void set_Sounds_502(String_t* value)
	{
		___Sounds_502 = value;
		Il2CppCodeGenWriteBarrier((&___Sounds_502), value);
	}

	inline static int32_t get_offset_of_Soundy_503() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Soundy_503)); }
	inline String_t* get_Soundy_503() const { return ___Soundy_503; }
	inline String_t** get_address_of_Soundy_503() { return &___Soundy_503; }
	inline void set_Soundy_503(String_t* value)
	{
		___Soundy_503 = value;
		Il2CppCodeGenWriteBarrier((&___Soundy_503), value);
	}

	inline static int32_t get_offset_of_SoundyDatabase_504() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundyDatabase_504)); }
	inline String_t* get_SoundyDatabase_504() const { return ___SoundyDatabase_504; }
	inline String_t** get_address_of_SoundyDatabase_504() { return &___SoundyDatabase_504; }
	inline void set_SoundyDatabase_504(String_t* value)
	{
		___SoundyDatabase_504 = value;
		Il2CppCodeGenWriteBarrier((&___SoundyDatabase_504), value);
	}

	inline static int32_t get_offset_of_SoundySettings_505() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SoundySettings_505)); }
	inline String_t* get_SoundySettings_505() const { return ___SoundySettings_505; }
	inline String_t** get_address_of_SoundySettings_505() { return &___SoundySettings_505; }
	inline void set_SoundySettings_505(String_t* value)
	{
		___SoundySettings_505 = value;
		Il2CppCodeGenWriteBarrier((&___SoundySettings_505), value);
	}

	inline static int32_t get_offset_of_SourceName_506() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SourceName_506)); }
	inline String_t* get_SourceName_506() const { return ___SourceName_506; }
	inline String_t** get_address_of_SourceName_506() { return &___SourceName_506; }
	inline void set_SourceName_506(String_t* value)
	{
		___SourceName_506 = value;
		Il2CppCodeGenWriteBarrier((&___SourceName_506), value);
	}

	inline static int32_t get_offset_of_Sources_507() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Sources_507)); }
	inline String_t* get_Sources_507() const { return ___Sources_507; }
	inline String_t** get_address_of_Sources_507() { return &___Sources_507; }
	inline void set_Sources_507(String_t* value)
	{
		___Sources_507 = value;
		Il2CppCodeGenWriteBarrier((&___Sources_507), value);
	}

	inline static int32_t get_offset_of_SpatialBlend_508() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SpatialBlend_508)); }
	inline String_t* get_SpatialBlend_508() const { return ___SpatialBlend_508; }
	inline String_t** get_address_of_SpatialBlend_508() { return &___SpatialBlend_508; }
	inline void set_SpatialBlend_508(String_t* value)
	{
		___SpatialBlend_508 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialBlend_508), value);
	}

	inline static int32_t get_offset_of_Speed_509() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Speed_509)); }
	inline String_t* get_Speed_509() const { return ___Speed_509; }
	inline String_t** get_address_of_Speed_509() { return &___Speed_509; }
	inline void set_Speed_509(String_t* value)
	{
		___Speed_509 = value;
		Il2CppCodeGenWriteBarrier((&___Speed_509), value);
	}

	inline static int32_t get_offset_of_StartDelay_510() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StartDelay_510)); }
	inline String_t* get_StartDelay_510() const { return ___StartDelay_510; }
	inline String_t** get_address_of_StartDelay_510() { return &___StartDelay_510; }
	inline void set_StartDelay_510(String_t* value)
	{
		___StartDelay_510 = value;
		Il2CppCodeGenWriteBarrier((&___StartDelay_510), value);
	}

	inline static int32_t get_offset_of_StartNodeName_511() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StartNodeName_511)); }
	inline String_t* get_StartNodeName_511() const { return ___StartNodeName_511; }
	inline String_t** get_address_of_StartNodeName_511() { return &___StartNodeName_511; }
	inline void set_StartNodeName_511(String_t* value)
	{
		___StartNodeName_511 = value;
		Il2CppCodeGenWriteBarrier((&___StartNodeName_511), value);
	}

	inline static int32_t get_offset_of_StopAllSounds_512() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StopAllSounds_512)); }
	inline String_t* get_StopAllSounds_512() const { return ___StopAllSounds_512; }
	inline String_t** get_address_of_StopAllSounds_512() { return &___StopAllSounds_512; }
	inline void set_StopAllSounds_512(String_t* value)
	{
		___StopAllSounds_512 = value;
		Il2CppCodeGenWriteBarrier((&___StopAllSounds_512), value);
	}

	inline static int32_t get_offset_of_StopAnimation_513() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StopAnimation_513)); }
	inline String_t* get_StopAnimation_513() const { return ___StopAnimation_513; }
	inline String_t** get_address_of_StopAnimation_513() { return &___StopAnimation_513; }
	inline void set_StopAnimation_513(String_t* value)
	{
		___StopAnimation_513 = value;
		Il2CppCodeGenWriteBarrier((&___StopAnimation_513), value);
	}

	inline static int32_t get_offset_of_StopBehavior_514() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StopBehavior_514)); }
	inline String_t* get_StopBehavior_514() const { return ___StopBehavior_514; }
	inline String_t** get_address_of_StopBehavior_514() { return &___StopBehavior_514; }
	inline void set_StopBehavior_514(String_t* value)
	{
		___StopBehavior_514 = value;
		Il2CppCodeGenWriteBarrier((&___StopBehavior_514), value);
	}

	inline static int32_t get_offset_of_StopSound_515() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___StopSound_515)); }
	inline String_t* get_StopSound_515() const { return ___StopSound_515; }
	inline String_t** get_address_of_StopSound_515() { return &___StopSound_515; }
	inline void set_StopSound_515(String_t* value)
	{
		___StopSound_515 = value;
		Il2CppCodeGenWriteBarrier((&___StopSound_515), value);
	}

	inline static int32_t get_offset_of_SubGraph_516() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SubGraph_516)); }
	inline String_t* get_SubGraph_516() const { return ___SubGraph_516; }
	inline String_t** get_address_of_SubGraph_516() { return &___SubGraph_516; }
	inline void set_SubGraph_516(String_t* value)
	{
		___SubGraph_516 = value;
		Il2CppCodeGenWriteBarrier((&___SubGraph_516), value);
	}

	inline static int32_t get_offset_of_SubGraphNodeName_517() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SubGraphNodeName_517)); }
	inline String_t* get_SubGraphNodeName_517() const { return ___SubGraphNodeName_517; }
	inline String_t** get_address_of_SubGraphNodeName_517() { return &___SubGraphNodeName_517; }
	inline void set_SubGraphNodeName_517(String_t* value)
	{
		___SubGraphNodeName_517 = value;
		Il2CppCodeGenWriteBarrier((&___SubGraphNodeName_517), value);
	}

	inline static int32_t get_offset_of_Suffix_518() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Suffix_518)); }
	inline String_t* get_Suffix_518() const { return ___Suffix_518; }
	inline String_t** get_address_of_Suffix_518() { return &___Suffix_518; }
	inline void set_Suffix_518(String_t* value)
	{
		___Suffix_518 = value;
		Il2CppCodeGenWriteBarrier((&___Suffix_518), value);
	}

	inline static int32_t get_offset_of_SupportEmail_519() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SupportEmail_519)); }
	inline String_t* get_SupportEmail_519() const { return ___SupportEmail_519; }
	inline String_t** get_address_of_SupportEmail_519() { return &___SupportEmail_519; }
	inline void set_SupportEmail_519(String_t* value)
	{
		___SupportEmail_519 = value;
		Il2CppCodeGenWriteBarrier((&___SupportEmail_519), value);
	}

	inline static int32_t get_offset_of_SwipeDirection_520() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SwipeDirection_520)); }
	inline String_t* get_SwipeDirection_520() const { return ___SwipeDirection_520; }
	inline String_t** get_address_of_SwipeDirection_520() { return &___SwipeDirection_520; }
	inline void set_SwipeDirection_520(String_t* value)
	{
		___SwipeDirection_520 = value;
		Il2CppCodeGenWriteBarrier((&___SwipeDirection_520), value);
	}

	inline static int32_t get_offset_of_SwipeLength_521() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SwipeLength_521)); }
	inline String_t* get_SwipeLength_521() const { return ___SwipeLength_521; }
	inline String_t** get_address_of_SwipeLength_521() { return &___SwipeLength_521; }
	inline void set_SwipeLength_521(String_t* value)
	{
		___SwipeLength_521 = value;
		Il2CppCodeGenWriteBarrier((&___SwipeLength_521), value);
	}

	inline static int32_t get_offset_of_SwipeLengthDescription_522() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SwipeLengthDescription_522)); }
	inline String_t* get_SwipeLengthDescription_522() const { return ___SwipeLengthDescription_522; }
	inline String_t** get_address_of_SwipeLengthDescription_522() { return &___SwipeLengthDescription_522; }
	inline void set_SwipeLengthDescription_522(String_t* value)
	{
		___SwipeLengthDescription_522 = value;
		Il2CppCodeGenWriteBarrier((&___SwipeLengthDescription_522), value);
	}

	inline static int32_t get_offset_of_SwitchBackNodeName_523() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SwitchBackNodeName_523)); }
	inline String_t* get_SwitchBackNodeName_523() const { return ___SwitchBackNodeName_523; }
	inline String_t** get_address_of_SwitchBackNodeName_523() { return &___SwitchBackNodeName_523; }
	inline void set_SwitchBackNodeName_523(String_t* value)
	{
		___SwitchBackNodeName_523 = value;
		Il2CppCodeGenWriteBarrier((&___SwitchBackNodeName_523), value);
	}

	inline static int32_t get_offset_of_Target_524() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Target_524)); }
	inline String_t* get_Target_524() const { return ___Target_524; }
	inline String_t** get_address_of_Target_524() { return &___Target_524; }
	inline void set_Target_524(String_t* value)
	{
		___Target_524 = value;
		Il2CppCodeGenWriteBarrier((&___Target_524), value);
	}

	inline static int32_t get_offset_of_TargetAnimator_525() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetAnimator_525)); }
	inline String_t* get_TargetAnimator_525() const { return ___TargetAnimator_525; }
	inline String_t** get_address_of_TargetAnimator_525() { return &___TargetAnimator_525; }
	inline void set_TargetAnimator_525(String_t* value)
	{
		___TargetAnimator_525 = value;
		Il2CppCodeGenWriteBarrier((&___TargetAnimator_525), value);
	}

	inline static int32_t get_offset_of_TargetAnimatorDoesNotHaveAnAnimatorController_526() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetAnimatorDoesNotHaveAnAnimatorController_526)); }
	inline String_t* get_TargetAnimatorDoesNotHaveAnAnimatorController_526() const { return ___TargetAnimatorDoesNotHaveAnAnimatorController_526; }
	inline String_t** get_address_of_TargetAnimatorDoesNotHaveAnAnimatorController_526() { return &___TargetAnimatorDoesNotHaveAnAnimatorController_526; }
	inline void set_TargetAnimatorDoesNotHaveAnAnimatorController_526(String_t* value)
	{
		___TargetAnimatorDoesNotHaveAnAnimatorController_526 = value;
		Il2CppCodeGenWriteBarrier((&___TargetAnimatorDoesNotHaveAnAnimatorController_526), value);
	}

	inline static int32_t get_offset_of_TargetAnimatorDoesNotHaveAnyParameters_527() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetAnimatorDoesNotHaveAnyParameters_527)); }
	inline String_t* get_TargetAnimatorDoesNotHaveAnyParameters_527() const { return ___TargetAnimatorDoesNotHaveAnyParameters_527; }
	inline String_t** get_address_of_TargetAnimatorDoesNotHaveAnyParameters_527() { return &___TargetAnimatorDoesNotHaveAnyParameters_527; }
	inline void set_TargetAnimatorDoesNotHaveAnyParameters_527(String_t* value)
	{
		___TargetAnimatorDoesNotHaveAnyParameters_527 = value;
		Il2CppCodeGenWriteBarrier((&___TargetAnimatorDoesNotHaveAnyParameters_527), value);
	}

	inline static int32_t get_offset_of_TargetCanvas_528() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetCanvas_528)); }
	inline String_t* get_TargetCanvas_528() const { return ___TargetCanvas_528; }
	inline String_t** get_address_of_TargetCanvas_528() { return &___TargetCanvas_528; }
	inline void set_TargetCanvas_528(String_t* value)
	{
		___TargetCanvas_528 = value;
		Il2CppCodeGenWriteBarrier((&___TargetCanvas_528), value);
	}

	inline static int32_t get_offset_of_TargetFsm_529() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetFsm_529)); }
	inline String_t* get_TargetFsm_529() const { return ___TargetFsm_529; }
	inline String_t** get_address_of_TargetFsm_529() { return &___TargetFsm_529; }
	inline void set_TargetFsm_529(String_t* value)
	{
		___TargetFsm_529 = value;
		Il2CppCodeGenWriteBarrier((&___TargetFsm_529), value);
	}

	inline static int32_t get_offset_of_TargetGameObject_530() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetGameObject_530)); }
	inline String_t* get_TargetGameObject_530() const { return ___TargetGameObject_530; }
	inline String_t** get_address_of_TargetGameObject_530() { return &___TargetGameObject_530; }
	inline void set_TargetGameObject_530(String_t* value)
	{
		___TargetGameObject_530 = value;
		Il2CppCodeGenWriteBarrier((&___TargetGameObject_530), value);
	}

	inline static int32_t get_offset_of_TargetLabel_531() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetLabel_531)); }
	inline String_t* get_TargetLabel_531() const { return ___TargetLabel_531; }
	inline String_t** get_address_of_TargetLabel_531() { return &___TargetLabel_531; }
	inline void set_TargetLabel_531(String_t* value)
	{
		___TargetLabel_531 = value;
		Il2CppCodeGenWriteBarrier((&___TargetLabel_531), value);
	}

	inline static int32_t get_offset_of_TargetOrientation_532() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetOrientation_532)); }
	inline String_t* get_TargetOrientation_532() const { return ___TargetOrientation_532; }
	inline String_t** get_address_of_TargetOrientation_532() { return &___TargetOrientation_532; }
	inline void set_TargetOrientation_532(String_t* value)
	{
		___TargetOrientation_532 = value;
		Il2CppCodeGenWriteBarrier((&___TargetOrientation_532), value);
	}

	inline static int32_t get_offset_of_TargetProgress_533() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetProgress_533)); }
	inline String_t* get_TargetProgress_533() const { return ___TargetProgress_533; }
	inline String_t** get_address_of_TargetProgress_533() { return &___TargetProgress_533; }
	inline void set_TargetProgress_533(String_t* value)
	{
		___TargetProgress_533 = value;
		Il2CppCodeGenWriteBarrier((&___TargetProgress_533), value);
	}

	inline static int32_t get_offset_of_TargetTimeScale_534() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetTimeScale_534)); }
	inline String_t* get_TargetTimeScale_534() const { return ___TargetTimeScale_534; }
	inline String_t** get_address_of_TargetTimeScale_534() { return &___TargetTimeScale_534; }
	inline void set_TargetTimeScale_534(String_t* value)
	{
		___TargetTimeScale_534 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTimeScale_534), value);
	}

	inline static int32_t get_offset_of_TargetValue_535() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetValue_535)); }
	inline String_t* get_TargetValue_535() const { return ___TargetValue_535; }
	inline String_t** get_address_of_TargetValue_535() { return &___TargetValue_535; }
	inline void set_TargetValue_535(String_t* value)
	{
		___TargetValue_535 = value;
		Il2CppCodeGenWriteBarrier((&___TargetValue_535), value);
	}

	inline static int32_t get_offset_of_TargetVariable_536() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TargetVariable_536)); }
	inline String_t* get_TargetVariable_536() const { return ___TargetVariable_536; }
	inline String_t** get_address_of_TargetVariable_536() { return &___TargetVariable_536; }
	inline void set_TargetVariable_536(String_t* value)
	{
		___TargetVariable_536 = value;
		Il2CppCodeGenWriteBarrier((&___TargetVariable_536), value);
	}

	inline static int32_t get_offset_of_Text_537() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Text_537)); }
	inline String_t* get_Text_537() const { return ___Text_537; }
	inline String_t** get_address_of_Text_537() { return &___Text_537; }
	inline void set_Text_537(String_t* value)
	{
		___Text_537 = value;
		Il2CppCodeGenWriteBarrier((&___Text_537), value);
	}

	inline static int32_t get_offset_of_TextLabel_538() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TextLabel_538)); }
	inline String_t* get_TextLabel_538() const { return ___TextLabel_538; }
	inline String_t** get_address_of_TextLabel_538() { return &___TextLabel_538; }
	inline void set_TextLabel_538(String_t* value)
	{
		___TextLabel_538 = value;
		Il2CppCodeGenWriteBarrier((&___TextLabel_538), value);
	}

	inline static int32_t get_offset_of_TextMeshPro_539() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TextMeshPro_539)); }
	inline String_t* get_TextMeshPro_539() const { return ___TextMeshPro_539; }
	inline String_t** get_address_of_TextMeshPro_539() { return &___TextMeshPro_539; }
	inline void set_TextMeshPro_539(String_t* value)
	{
		___TextMeshPro_539 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshPro_539), value);
	}

	inline static int32_t get_offset_of_TextMeshProLabel_540() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TextMeshProLabel_540)); }
	inline String_t* get_TextMeshProLabel_540() const { return ___TextMeshProLabel_540; }
	inline String_t** get_address_of_TextMeshProLabel_540() { return &___TextMeshProLabel_540; }
	inline void set_TextMeshProLabel_540(String_t* value)
	{
		___TextMeshProLabel_540 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshProLabel_540), value);
	}

	inline static int32_t get_offset_of_Time_541() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Time_541)); }
	inline String_t* get_Time_541() const { return ___Time_541; }
	inline String_t** get_address_of_Time_541() { return &___Time_541; }
	inline void set_Time_541(String_t* value)
	{
		___Time_541 = value;
		Il2CppCodeGenWriteBarrier((&___Time_541), value);
	}

	inline static int32_t get_offset_of_TimeScaleNodeName_542() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TimeScaleNodeName_542)); }
	inline String_t* get_TimeScaleNodeName_542() const { return ___TimeScaleNodeName_542; }
	inline String_t** get_address_of_TimeScaleNodeName_542() { return &___TimeScaleNodeName_542; }
	inline void set_TimeScaleNodeName_542(String_t* value)
	{
		___TimeScaleNodeName_542 = value;
		Il2CppCodeGenWriteBarrier((&___TimeScaleNodeName_542), value);
	}

	inline static int32_t get_offset_of_ToggleComponentBehaviors_543() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleComponentBehaviors_543)); }
	inline String_t* get_ToggleComponentBehaviors_543() const { return ___ToggleComponentBehaviors_543; }
	inline String_t** get_address_of_ToggleComponentBehaviors_543() { return &___ToggleComponentBehaviors_543; }
	inline void set_ToggleComponentBehaviors_543(String_t* value)
	{
		___ToggleComponentBehaviors_543 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleComponentBehaviors_543), value);
	}

	inline static int32_t get_offset_of_ToggleLabel_544() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleLabel_544)); }
	inline String_t* get_ToggleLabel_544() const { return ___ToggleLabel_544; }
	inline String_t** get_address_of_ToggleLabel_544() { return &___ToggleLabel_544; }
	inline void set_ToggleLabel_544(String_t* value)
	{
		___ToggleLabel_544 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleLabel_544), value);
	}

	inline static int32_t get_offset_of_ToggleOFF_545() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleOFF_545)); }
	inline String_t* get_ToggleOFF_545() const { return ___ToggleOFF_545; }
	inline String_t** get_address_of_ToggleOFF_545() { return &___ToggleOFF_545; }
	inline void set_ToggleOFF_545(String_t* value)
	{
		___ToggleOFF_545 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleOFF_545), value);
	}

	inline static int32_t get_offset_of_ToggleON_546() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleON_546)); }
	inline String_t* get_ToggleON_546() const { return ___ToggleON_546; }
	inline String_t** get_address_of_ToggleON_546() { return &___ToggleON_546; }
	inline void set_ToggleON_546(String_t* value)
	{
		___ToggleON_546 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleON_546), value);
	}

	inline static int32_t get_offset_of_ToggleProgressor_547() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleProgressor_547)); }
	inline String_t* get_ToggleProgressor_547() const { return ___ToggleProgressor_547; }
	inline String_t** get_address_of_ToggleProgressor_547() { return &___ToggleProgressor_547; }
	inline void set_ToggleProgressor_547(String_t* value)
	{
		___ToggleProgressor_547 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleProgressor_547), value);
	}

	inline static int32_t get_offset_of_ToggleSettings_548() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleSettings_548)); }
	inline String_t* get_ToggleSettings_548() const { return ___ToggleSettings_548; }
	inline String_t** get_address_of_ToggleSettings_548() { return &___ToggleSettings_548; }
	inline void set_ToggleSettings_548(String_t* value)
	{
		___ToggleSettings_548 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleSettings_548), value);
	}

	inline static int32_t get_offset_of_ToggleSupportForThirdPartyPlugins_549() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ToggleSupportForThirdPartyPlugins_549)); }
	inline String_t* get_ToggleSupportForThirdPartyPlugins_549() const { return ___ToggleSupportForThirdPartyPlugins_549; }
	inline String_t** get_address_of_ToggleSupportForThirdPartyPlugins_549() { return &___ToggleSupportForThirdPartyPlugins_549; }
	inline void set_ToggleSupportForThirdPartyPlugins_549(String_t* value)
	{
		___ToggleSupportForThirdPartyPlugins_549 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleSupportForThirdPartyPlugins_549), value);
	}

	inline static int32_t get_offset_of_TouchySettings_550() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TouchySettings_550)); }
	inline String_t* get_TouchySettings_550() const { return ___TouchySettings_550; }
	inline String_t** get_address_of_TouchySettings_550() { return &___TouchySettings_550; }
	inline void set_TouchySettings_550(String_t* value)
	{
		___TouchySettings_550 = value;
		Il2CppCodeGenWriteBarrier((&___TouchySettings_550), value);
	}

	inline static int32_t get_offset_of_TriggerAction_551() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TriggerAction_551)); }
	inline String_t* get_TriggerAction_551() const { return ___TriggerAction_551; }
	inline String_t** get_address_of_TriggerAction_551() { return &___TriggerAction_551; }
	inline void set_TriggerAction_551(String_t* value)
	{
		___TriggerAction_551 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerAction_551), value);
	}

	inline static int32_t get_offset_of_TriggerEventsAfterAnimation_552() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TriggerEventsAfterAnimation_552)); }
	inline String_t* get_TriggerEventsAfterAnimation_552() const { return ___TriggerEventsAfterAnimation_552; }
	inline String_t** get_address_of_TriggerEventsAfterAnimation_552() { return &___TriggerEventsAfterAnimation_552; }
	inline void set_TriggerEventsAfterAnimation_552(String_t* value)
	{
		___TriggerEventsAfterAnimation_552 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerEventsAfterAnimation_552), value);
	}

	inline static int32_t get_offset_of_TriggerName_553() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___TriggerName_553)); }
	inline String_t* get_TriggerName_553() const { return ___TriggerName_553; }
	inline String_t** get_address_of_TriggerName_553() { return &___TriggerName_553; }
	inline void set_TriggerName_553(String_t* value)
	{
		___TriggerName_553 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerName_553), value);
	}

	inline static int32_t get_offset_of_UIDrawerNodeName_554() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UIDrawerNodeName_554)); }
	inline String_t* get_UIDrawerNodeName_554() const { return ___UIDrawerNodeName_554; }
	inline String_t** get_address_of_UIDrawerNodeName_554() { return &___UIDrawerNodeName_554; }
	inline void set_UIDrawerNodeName_554(String_t* value)
	{
		___UIDrawerNodeName_554 = value;
		Il2CppCodeGenWriteBarrier((&___UIDrawerNodeName_554), value);
	}

	inline static int32_t get_offset_of_UINodeNodeName_555() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UINodeNodeName_555)); }
	inline String_t* get_UINodeNodeName_555() const { return ___UINodeNodeName_555; }
	inline String_t** get_address_of_UINodeNodeName_555() { return &___UINodeNodeName_555; }
	inline void set_UINodeNodeName_555(String_t* value)
	{
		___UINodeNodeName_555 = value;
		Il2CppCodeGenWriteBarrier((&___UINodeNodeName_555), value);
	}

	inline static int32_t get_offset_of_UIPopupDatabase_556() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UIPopupDatabase_556)); }
	inline String_t* get_UIPopupDatabase_556() const { return ___UIPopupDatabase_556; }
	inline String_t** get_address_of_UIPopupDatabase_556() { return &___UIPopupDatabase_556; }
	inline void set_UIPopupDatabase_556(String_t* value)
	{
		___UIPopupDatabase_556 = value;
		Il2CppCodeGenWriteBarrier((&___UIPopupDatabase_556), value);
	}

	inline static int32_t get_offset_of_UnityEvent_557() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnityEvent_557)); }
	inline String_t* get_UnityEvent_557() const { return ___UnityEvent_557; }
	inline String_t** get_address_of_UnityEvent_557() { return &___UnityEvent_557; }
	inline void set_UnityEvent_557(String_t* value)
	{
		___UnityEvent_557 = value;
		Il2CppCodeGenWriteBarrier((&___UnityEvent_557), value);
	}

	inline static int32_t get_offset_of_UnityEvents_558() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnityEvents_558)); }
	inline String_t* get_UnityEvents_558() const { return ___UnityEvents_558; }
	inline String_t** get_address_of_UnityEvents_558() { return &___UnityEvents_558; }
	inline void set_UnityEvents_558(String_t* value)
	{
		___UnityEvents_558 = value;
		Il2CppCodeGenWriteBarrier((&___UnityEvents_558), value);
	}

	inline static int32_t get_offset_of_UnloadSceneNodeName_559() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnloadSceneNodeName_559)); }
	inline String_t* get_UnloadSceneNodeName_559() const { return ___UnloadSceneNodeName_559; }
	inline String_t** get_address_of_UnloadSceneNodeName_559() { return &___UnloadSceneNodeName_559; }
	inline void set_UnloadSceneNodeName_559(String_t* value)
	{
		___UnloadSceneNodeName_559 = value;
		Il2CppCodeGenWriteBarrier((&___UnloadSceneNodeName_559), value);
	}

	inline static int32_t get_offset_of_UnmuteAllSounds_560() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnmuteAllSounds_560)); }
	inline String_t* get_UnmuteAllSounds_560() const { return ___UnmuteAllSounds_560; }
	inline String_t** get_address_of_UnmuteAllSounds_560() { return &___UnmuteAllSounds_560; }
	inline void set_UnmuteAllSounds_560(String_t* value)
	{
		___UnmuteAllSounds_560 = value;
		Il2CppCodeGenWriteBarrier((&___UnmuteAllSounds_560), value);
	}

	inline static int32_t get_offset_of_UnpauseAllSounds_561() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnpauseAllSounds_561)); }
	inline String_t* get_UnpauseAllSounds_561() const { return ___UnpauseAllSounds_561; }
	inline String_t** get_address_of_UnpauseAllSounds_561() { return &___UnpauseAllSounds_561; }
	inline void set_UnpauseAllSounds_561(String_t* value)
	{
		___UnpauseAllSounds_561 = value;
		Il2CppCodeGenWriteBarrier((&___UnpauseAllSounds_561), value);
	}

	inline static int32_t get_offset_of_Up_562() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Up_562)); }
	inline String_t* get_Up_562() const { return ___Up_562; }
	inline String_t** get_address_of_Up_562() { return &___Up_562; }
	inline void set_Up_562(String_t* value)
	{
		___Up_562 = value;
		Il2CppCodeGenWriteBarrier((&___Up_562), value);
	}

	inline static int32_t get_offset_of_UpdateContainer_563() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdateContainer_563)); }
	inline String_t* get_UpdateContainer_563() const { return ___UpdateContainer_563; }
	inline String_t** get_address_of_UpdateContainer_563() { return &___UpdateContainer_563; }
	inline void set_UpdateContainer_563(String_t* value)
	{
		___UpdateContainer_563 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateContainer_563), value);
	}

	inline static int32_t get_offset_of_UpdateEffect_564() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdateEffect_564)); }
	inline String_t* get_UpdateEffect_564() const { return ___UpdateEffect_564; }
	inline String_t** get_address_of_UpdateEffect_564() { return &___UpdateEffect_564; }
	inline void set_UpdateEffect_564(String_t* value)
	{
		___UpdateEffect_564 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEffect_564), value);
	}

	inline static int32_t get_offset_of_UpdateOnHide_565() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdateOnHide_565)); }
	inline String_t* get_UpdateOnHide_565() const { return ___UpdateOnHide_565; }
	inline String_t** get_address_of_UpdateOnHide_565() { return &___UpdateOnHide_565; }
	inline void set_UpdateOnHide_565(String_t* value)
	{
		___UpdateOnHide_565 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateOnHide_565), value);
	}

	inline static int32_t get_offset_of_UpdateOnShow_566() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdateOnShow_566)); }
	inline String_t* get_UpdateOnShow_566() const { return ___UpdateOnShow_566; }
	inline String_t** get_address_of_UpdateOnShow_566() { return &___UpdateOnShow_566; }
	inline void set_UpdateOnShow_566(String_t* value)
	{
		___UpdateOnShow_566 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateOnShow_566), value);
	}

	inline static int32_t get_offset_of_UpdatePopupName_567() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdatePopupName_567)); }
	inline String_t* get_UpdatePopupName_567() const { return ___UpdatePopupName_567; }
	inline String_t** get_address_of_UpdatePopupName_567() { return &___UpdatePopupName_567; }
	inline void set_UpdatePopupName_567(String_t* value)
	{
		___UpdatePopupName_567 = value;
		Il2CppCodeGenWriteBarrier((&___UpdatePopupName_567), value);
	}

	inline static int32_t get_offset_of_UpdatePopupPrefab_568() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdatePopupPrefab_568)); }
	inline String_t* get_UpdatePopupPrefab_568() const { return ___UpdatePopupPrefab_568; }
	inline String_t** get_address_of_UpdatePopupPrefab_568() { return &___UpdatePopupPrefab_568; }
	inline void set_UpdatePopupPrefab_568(String_t* value)
	{
		___UpdatePopupPrefab_568 = value;
		Il2CppCodeGenWriteBarrier((&___UpdatePopupPrefab_568), value);
	}

	inline static int32_t get_offset_of_UpdateValue_569() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UpdateValue_569)); }
	inline String_t* get_UpdateValue_569() const { return ___UpdateValue_569; }
	inline String_t** get_address_of_UpdateValue_569() { return &___UpdateValue_569; }
	inline void set_UpdateValue_569(String_t* value)
	{
		___UpdateValue_569 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateValue_569), value);
	}

	inline static int32_t get_offset_of_UseBackButtonDescription_570() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UseBackButtonDescription_570)); }
	inline String_t* get_UseBackButtonDescription_570() const { return ___UseBackButtonDescription_570; }
	inline String_t** get_address_of_UseBackButtonDescription_570() { return &___UseBackButtonDescription_570; }
	inline void set_UseBackButtonDescription_570(String_t* value)
	{
		___UseBackButtonDescription_570 = value;
		Il2CppCodeGenWriteBarrier((&___UseBackButtonDescription_570), value);
	}

	inline static int32_t get_offset_of_UseCustomFromAndTo_571() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UseCustomFromAndTo_571)); }
	inline String_t* get_UseCustomFromAndTo_571() const { return ___UseCustomFromAndTo_571; }
	inline String_t** get_address_of_UseCustomFromAndTo_571() { return &___UseCustomFromAndTo_571; }
	inline void set_UseCustomFromAndTo_571(String_t* value)
	{
		___UseCustomFromAndTo_571 = value;
		Il2CppCodeGenWriteBarrier((&___UseCustomFromAndTo_571), value);
	}

	inline static int32_t get_offset_of_UseMultiplier_572() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UseMultiplier_572)); }
	inline String_t* get_UseMultiplier_572() const { return ___UseMultiplier_572; }
	inline String_t** get_address_of_UseMultiplier_572() { return &___UseMultiplier_572; }
	inline void set_UseMultiplier_572(String_t* value)
	{
		___UseMultiplier_572 = value;
		Il2CppCodeGenWriteBarrier((&___UseMultiplier_572), value);
	}

	inline static int32_t get_offset_of_UsefulLinks_573() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UsefulLinks_573)); }
	inline String_t* get_UsefulLinks_573() const { return ___UsefulLinks_573; }
	inline String_t** get_address_of_UsefulLinks_573() { return &___UsefulLinks_573; }
	inline void set_UsefulLinks_573(String_t* value)
	{
		___UsefulLinks_573 = value;
		Il2CppCodeGenWriteBarrier((&___UsefulLinks_573), value);
	}

	inline static int32_t get_offset_of_Value_574() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Value_574)); }
	inline String_t* get_Value_574() const { return ___Value_574; }
	inline String_t** get_address_of_Value_574() { return &___Value_574; }
	inline void set_Value_574(String_t* value)
	{
		___Value_574 = value;
		Il2CppCodeGenWriteBarrier((&___Value_574), value);
	}

	inline static int32_t get_offset_of_Version_575() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Version_575)); }
	inline String_t* get_Version_575() const { return ___Version_575; }
	inline String_t** get_address_of_Version_575() { return &___Version_575; }
	inline void set_Version_575(String_t* value)
	{
		___Version_575 = value;
		Il2CppCodeGenWriteBarrier((&___Version_575), value);
	}

	inline static int32_t get_offset_of_Vibrato_576() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Vibrato_576)); }
	inline String_t* get_Vibrato_576() const { return ___Vibrato_576; }
	inline String_t** get_address_of_Vibrato_576() { return &___Vibrato_576; }
	inline void set_Vibrato_576(String_t* value)
	{
		___Vibrato_576 = value;
		Il2CppCodeGenWriteBarrier((&___Vibrato_576), value);
	}

	inline static int32_t get_offset_of_ViewCategory_577() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ViewCategory_577)); }
	inline String_t* get_ViewCategory_577() const { return ___ViewCategory_577; }
	inline String_t** get_address_of_ViewCategory_577() { return &___ViewCategory_577; }
	inline void set_ViewCategory_577(String_t* value)
	{
		___ViewCategory_577 = value;
		Il2CppCodeGenWriteBarrier((&___ViewCategory_577), value);
	}

	inline static int32_t get_offset_of_ViewName_578() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ViewName_578)); }
	inline String_t* get_ViewName_578() const { return ___ViewName_578; }
	inline String_t** get_address_of_ViewName_578() { return &___ViewName_578; }
	inline void set_ViewName_578(String_t* value)
	{
		___ViewName_578 = value;
		Il2CppCodeGenWriteBarrier((&___ViewName_578), value);
	}

	inline static int32_t get_offset_of_VirtualButton_579() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___VirtualButton_579)); }
	inline String_t* get_VirtualButton_579() const { return ___VirtualButton_579; }
	inline String_t** get_address_of_VirtualButton_579() { return &___VirtualButton_579; }
	inline void set_VirtualButton_579(String_t* value)
	{
		___VirtualButton_579 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButton_579), value);
	}

	inline static int32_t get_offset_of_VolumeDb_580() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___VolumeDb_580)); }
	inline String_t* get_VolumeDb_580() const { return ___VolumeDb_580; }
	inline String_t** get_address_of_VolumeDb_580() { return &___VolumeDb_580; }
	inline void set_VolumeDb_580(String_t* value)
	{
		___VolumeDb_580 = value;
		Il2CppCodeGenWriteBarrier((&___VolumeDb_580), value);
	}

	inline static int32_t get_offset_of_WaitFor_581() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WaitFor_581)); }
	inline String_t* get_WaitFor_581() const { return ___WaitFor_581; }
	inline String_t** get_address_of_WaitFor_581() { return &___WaitFor_581; }
	inline void set_WaitFor_581(String_t* value)
	{
		___WaitFor_581 = value;
		Il2CppCodeGenWriteBarrier((&___WaitFor_581), value);
	}

	inline static int32_t get_offset_of_WaitForAnimationToFinish_582() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WaitForAnimationToFinish_582)); }
	inline String_t* get_WaitForAnimationToFinish_582() const { return ___WaitForAnimationToFinish_582; }
	inline String_t** get_address_of_WaitForAnimationToFinish_582() { return &___WaitForAnimationToFinish_582; }
	inline void set_WaitForAnimationToFinish_582(String_t* value)
	{
		___WaitForAnimationToFinish_582 = value;
		Il2CppCodeGenWriteBarrier((&___WaitForAnimationToFinish_582), value);
	}

	inline static int32_t get_offset_of_WaitForSceneToUnload_583() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WaitForSceneToUnload_583)); }
	inline String_t* get_WaitForSceneToUnload_583() const { return ___WaitForSceneToUnload_583; }
	inline String_t** get_address_of_WaitForSceneToUnload_583() { return &___WaitForSceneToUnload_583; }
	inline void set_WaitForSceneToUnload_583(String_t* value)
	{
		___WaitForSceneToUnload_583 = value;
		Il2CppCodeGenWriteBarrier((&___WaitForSceneToUnload_583), value);
	}

	inline static int32_t get_offset_of_WaitNodeName_584() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WaitNodeName_584)); }
	inline String_t* get_WaitNodeName_584() const { return ___WaitNodeName_584; }
	inline String_t** get_address_of_WaitNodeName_584() { return &___WaitNodeName_584; }
	inline void set_WaitNodeName_584(String_t* value)
	{
		___WaitNodeName_584 = value;
		Il2CppCodeGenWriteBarrier((&___WaitNodeName_584), value);
	}

	inline static int32_t get_offset_of_Warning_585() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Warning_585)); }
	inline String_t* get_Warning_585() const { return ___Warning_585; }
	inline String_t** get_address_of_Warning_585() { return &___Warning_585; }
	inline void set_Warning_585(String_t* value)
	{
		___Warning_585 = value;
		Il2CppCodeGenWriteBarrier((&___Warning_585), value);
	}

	inline static int32_t get_offset_of_Weight_586() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Weight_586)); }
	inline String_t* get_Weight_586() const { return ___Weight_586; }
	inline String_t** get_address_of_Weight_586() { return &___Weight_586; }
	inline void set_Weight_586(String_t* value)
	{
		___Weight_586 = value;
		Il2CppCodeGenWriteBarrier((&___Weight_586), value);
	}

	inline static int32_t get_offset_of_WhenClosed_587() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WhenClosed_587)); }
	inline String_t* get_WhenClosed_587() const { return ___WhenClosed_587; }
	inline String_t** get_address_of_WhenClosed_587() { return &___WhenClosed_587; }
	inline void set_WhenClosed_587(String_t* value)
	{
		___WhenClosed_587 = value;
		Il2CppCodeGenWriteBarrier((&___WhenClosed_587), value);
	}

	inline static int32_t get_offset_of_WhenUIDrawerIsClosed_588() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WhenUIDrawerIsClosed_588)); }
	inline String_t* get_WhenUIDrawerIsClosed_588() const { return ___WhenUIDrawerIsClosed_588; }
	inline String_t** get_address_of_WhenUIDrawerIsClosed_588() { return &___WhenUIDrawerIsClosed_588; }
	inline void set_WhenUIDrawerIsClosed_588(String_t* value)
	{
		___WhenUIDrawerIsClosed_588 = value;
		Il2CppCodeGenWriteBarrier((&___WhenUIDrawerIsClosed_588), value);
	}

	inline static int32_t get_offset_of_WhenUIPopupIsHiddenDisable_589() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WhenUIPopupIsHiddenDisable_589)); }
	inline String_t* get_WhenUIPopupIsHiddenDisable_589() const { return ___WhenUIPopupIsHiddenDisable_589; }
	inline String_t** get_address_of_WhenUIPopupIsHiddenDisable_589() { return &___WhenUIPopupIsHiddenDisable_589; }
	inline void set_WhenUIPopupIsHiddenDisable_589(String_t* value)
	{
		___WhenUIPopupIsHiddenDisable_589 = value;
		Il2CppCodeGenWriteBarrier((&___WhenUIPopupIsHiddenDisable_589), value);
	}

	inline static int32_t get_offset_of_WhenUIViewIsHiddenDisable_590() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WhenUIViewIsHiddenDisable_590)); }
	inline String_t* get_WhenUIViewIsHiddenDisable_590() const { return ___WhenUIViewIsHiddenDisable_590; }
	inline String_t** get_address_of_WhenUIViewIsHiddenDisable_590() { return &___WhenUIViewIsHiddenDisable_590; }
	inline void set_WhenUIViewIsHiddenDisable_590(String_t* value)
	{
		___WhenUIViewIsHiddenDisable_590 = value;
		Il2CppCodeGenWriteBarrier((&___WhenUIViewIsHiddenDisable_590), value);
	}

	inline static int32_t get_offset_of_WholeNumbers_591() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WholeNumbers_591)); }
	inline String_t* get_WholeNumbers_591() const { return ___WholeNumbers_591; }
	inline String_t** get_address_of_WholeNumbers_591() { return &___WholeNumbers_591; }
	inline void set_WholeNumbers_591(String_t* value)
	{
		___WholeNumbers_591 = value;
		Il2CppCodeGenWriteBarrier((&___WholeNumbers_591), value);
	}

	inline static int32_t get_offset_of_Width_592() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Width_592)); }
	inline String_t* get_Width_592() const { return ___Width_592; }
	inline String_t** get_address_of_Width_592() { return &___Width_592; }
	inline void set_Width_592(String_t* value)
	{
		___Width_592 = value;
		Il2CppCodeGenWriteBarrier((&___Width_592), value);
	}

	inline static int32_t get_offset_of_X_593() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___X_593)); }
	inline String_t* get_X_593() const { return ___X_593; }
	inline String_t** get_address_of_X_593() { return &___X_593; }
	inline void set_X_593(String_t* value)
	{
		___X_593 = value;
		Il2CppCodeGenWriteBarrier((&___X_593), value);
	}

	inline static int32_t get_offset_of_Y_594() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Y_594)); }
	inline String_t* get_Y_594() const { return ___Y_594; }
	inline String_t** get_address_of_Y_594() { return &___Y_594; }
	inline void set_Y_594(String_t* value)
	{
		___Y_594 = value;
		Il2CppCodeGenWriteBarrier((&___Y_594), value);
	}

	inline static int32_t get_offset_of_Yes_595() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___Yes_595)); }
	inline String_t* get_Yes_595() const { return ___Yes_595; }
	inline String_t** get_address_of_Yes_595() { return &___Yes_595; }
	inline void set_Yes_595(String_t* value)
	{
		___Yes_595 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_595), value);
	}

	inline static int32_t get_offset_of_YouAreResponsibleToUpdateYourCode_596() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___YouAreResponsibleToUpdateYourCode_596)); }
	inline String_t* get_YouAreResponsibleToUpdateYourCode_596() const { return ___YouAreResponsibleToUpdateYourCode_596; }
	inline String_t** get_address_of_YouAreResponsibleToUpdateYourCode_596() { return &___YouAreResponsibleToUpdateYourCode_596; }
	inline void set_YouAreResponsibleToUpdateYourCode_596(String_t* value)
	{
		___YouAreResponsibleToUpdateYourCode_596 = value;
		Il2CppCodeGenWriteBarrier((&___YouAreResponsibleToUpdateYourCode_596), value);
	}

	inline static int32_t get_offset_of_YouCanOnlyAddPrefabsToDatabase_597() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___YouCanOnlyAddPrefabsToDatabase_597)); }
	inline String_t* get_YouCanOnlyAddPrefabsToDatabase_597() const { return ___YouCanOnlyAddPrefabsToDatabase_597; }
	inline String_t** get_address_of_YouCanOnlyAddPrefabsToDatabase_597() { return &___YouCanOnlyAddPrefabsToDatabase_597; }
	inline void set_YouCanOnlyAddPrefabsToDatabase_597(String_t* value)
	{
		___YouCanOnlyAddPrefabsToDatabase_597 = value;
		Il2CppCodeGenWriteBarrier((&___YouCanOnlyAddPrefabsToDatabase_597), value);
	}

	inline static int32_t get_offset_of_YouTube_598() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___YouTube_598)); }
	inline String_t* get_YouTube_598() const { return ___YouTube_598; }
	inline String_t** get_address_of_YouTube_598() { return &___YouTube_598; }
	inline void set_YouTube_598(String_t* value)
	{
		___YouTube_598 = value;
		Il2CppCodeGenWriteBarrier((&___YouTube_598), value);
	}

	inline static int32_t get_offset_of_MissingTargetFsmMessage_599() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingTargetFsmMessage_599)); }
	inline String_t* get_MissingTargetFsmMessage_599() const { return ___MissingTargetFsmMessage_599; }
	inline String_t** get_address_of_MissingTargetFsmMessage_599() { return &___MissingTargetFsmMessage_599; }
	inline void set_MissingTargetFsmMessage_599(String_t* value)
	{
		___MissingTargetFsmMessage_599 = value;
		Il2CppCodeGenWriteBarrier((&___MissingTargetFsmMessage_599), value);
	}

	inline static int32_t get_offset_of_SelectListenerToActivateMessage_600() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SelectListenerToActivateMessage_600)); }
	inline String_t* get_SelectListenerToActivateMessage_600() const { return ___SelectListenerToActivateMessage_600; }
	inline String_t** get_address_of_SelectListenerToActivateMessage_600() { return &___SelectListenerToActivateMessage_600; }
	inline void set_SelectListenerToActivateMessage_600(String_t* value)
	{
		___SelectListenerToActivateMessage_600 = value;
		Il2CppCodeGenWriteBarrier((&___SelectListenerToActivateMessage_600), value);
	}

	inline static int32_t get_offset_of_HowToUsePlaymakerEventDispatcherMessage_601() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___HowToUsePlaymakerEventDispatcherMessage_601)); }
	inline String_t* get_HowToUsePlaymakerEventDispatcherMessage_601() const { return ___HowToUsePlaymakerEventDispatcherMessage_601; }
	inline String_t** get_address_of_HowToUsePlaymakerEventDispatcherMessage_601() { return &___HowToUsePlaymakerEventDispatcherMessage_601; }
	inline void set_HowToUsePlaymakerEventDispatcherMessage_601(String_t* value)
	{
		___HowToUsePlaymakerEventDispatcherMessage_601 = value;
		Il2CppCodeGenWriteBarrier((&___HowToUsePlaymakerEventDispatcherMessage_601), value);
	}

	inline static int32_t get_offset_of_MissingDrawerNameTitle_602() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingDrawerNameTitle_602)); }
	inline String_t* get_MissingDrawerNameTitle_602() const { return ___MissingDrawerNameTitle_602; }
	inline String_t** get_address_of_MissingDrawerNameTitle_602() { return &___MissingDrawerNameTitle_602; }
	inline void set_MissingDrawerNameTitle_602(String_t* value)
	{
		___MissingDrawerNameTitle_602 = value;
		Il2CppCodeGenWriteBarrier((&___MissingDrawerNameTitle_602), value);
	}

	inline static int32_t get_offset_of_MissingDrawerNameMessage_603() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingDrawerNameMessage_603)); }
	inline String_t* get_MissingDrawerNameMessage_603() const { return ___MissingDrawerNameMessage_603; }
	inline String_t** get_address_of_MissingDrawerNameMessage_603() { return &___MissingDrawerNameMessage_603; }
	inline void set_MissingDrawerNameMessage_603(String_t* value)
	{
		___MissingDrawerNameMessage_603 = value;
		Il2CppCodeGenWriteBarrier((&___MissingDrawerNameMessage_603), value);
	}

	inline static int32_t get_offset_of_MissingGameEventTitle_604() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingGameEventTitle_604)); }
	inline String_t* get_MissingGameEventTitle_604() const { return ___MissingGameEventTitle_604; }
	inline String_t** get_address_of_MissingGameEventTitle_604() { return &___MissingGameEventTitle_604; }
	inline void set_MissingGameEventTitle_604(String_t* value)
	{
		___MissingGameEventTitle_604 = value;
		Il2CppCodeGenWriteBarrier((&___MissingGameEventTitle_604), value);
	}

	inline static int32_t get_offset_of_MissingGameEventMessage_605() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingGameEventMessage_605)); }
	inline String_t* get_MissingGameEventMessage_605() const { return ___MissingGameEventMessage_605; }
	inline String_t** get_address_of_MissingGameEventMessage_605() { return &___MissingGameEventMessage_605; }
	inline void set_MissingGameEventMessage_605(String_t* value)
	{
		___MissingGameEventMessage_605 = value;
		Il2CppCodeGenWriteBarrier((&___MissingGameEventMessage_605), value);
	}

	inline static int32_t get_offset_of_MissingSceneNameTitle_606() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingSceneNameTitle_606)); }
	inline String_t* get_MissingSceneNameTitle_606() const { return ___MissingSceneNameTitle_606; }
	inline String_t** get_address_of_MissingSceneNameTitle_606() { return &___MissingSceneNameTitle_606; }
	inline void set_MissingSceneNameTitle_606(String_t* value)
	{
		___MissingSceneNameTitle_606 = value;
		Il2CppCodeGenWriteBarrier((&___MissingSceneNameTitle_606), value);
	}

	inline static int32_t get_offset_of_MissingSceneNameMessage_607() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___MissingSceneNameMessage_607)); }
	inline String_t* get_MissingSceneNameMessage_607() const { return ___MissingSceneNameMessage_607; }
	inline String_t** get_address_of_MissingSceneNameMessage_607() { return &___MissingSceneNameMessage_607; }
	inline void set_MissingSceneNameMessage_607(String_t* value)
	{
		___MissingSceneNameMessage_607 = value;
		Il2CppCodeGenWriteBarrier((&___MissingSceneNameMessage_607), value);
	}

	inline static int32_t get_offset_of_WrongSceneBuildIndexTitle_608() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WrongSceneBuildIndexTitle_608)); }
	inline String_t* get_WrongSceneBuildIndexTitle_608() const { return ___WrongSceneBuildIndexTitle_608; }
	inline String_t** get_address_of_WrongSceneBuildIndexTitle_608() { return &___WrongSceneBuildIndexTitle_608; }
	inline void set_WrongSceneBuildIndexTitle_608(String_t* value)
	{
		___WrongSceneBuildIndexTitle_608 = value;
		Il2CppCodeGenWriteBarrier((&___WrongSceneBuildIndexTitle_608), value);
	}

	inline static int32_t get_offset_of_WrongSceneBuildIndexMessage_609() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___WrongSceneBuildIndexMessage_609)); }
	inline String_t* get_WrongSceneBuildIndexMessage_609() const { return ___WrongSceneBuildIndexMessage_609; }
	inline String_t** get_address_of_WrongSceneBuildIndexMessage_609() { return &___WrongSceneBuildIndexMessage_609; }
	inline void set_WrongSceneBuildIndexMessage_609(String_t* value)
	{
		___WrongSceneBuildIndexMessage_609 = value;
		Il2CppCodeGenWriteBarrier((&___WrongSceneBuildIndexMessage_609), value);
	}

	inline static int32_t get_offset_of_DoubleClickNodeToOpenSubGraphMessage_610() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DoubleClickNodeToOpenSubGraphMessage_610)); }
	inline String_t* get_DoubleClickNodeToOpenSubGraphMessage_610() const { return ___DoubleClickNodeToOpenSubGraphMessage_610; }
	inline String_t** get_address_of_DoubleClickNodeToOpenSubGraphMessage_610() { return &___DoubleClickNodeToOpenSubGraphMessage_610; }
	inline void set_DoubleClickNodeToOpenSubGraphMessage_610(String_t* value)
	{
		___DoubleClickNodeToOpenSubGraphMessage_610 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleClickNodeToOpenSubGraphMessage_610), value);
	}

	inline static int32_t get_offset_of_DuplicateNodeMessage_611() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DuplicateNodeMessage_611)); }
	inline String_t* get_DuplicateNodeMessage_611() const { return ___DuplicateNodeMessage_611; }
	inline String_t** get_address_of_DuplicateNodeMessage_611() { return &___DuplicateNodeMessage_611; }
	inline void set_DuplicateNodeMessage_611(String_t* value)
	{
		___DuplicateNodeMessage_611 = value;
		Il2CppCodeGenWriteBarrier((&___DuplicateNodeMessage_611), value);
	}

	inline static int32_t get_offset_of_DuplicateNodeTitle_612() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___DuplicateNodeTitle_612)); }
	inline String_t* get_DuplicateNodeTitle_612() const { return ___DuplicateNodeTitle_612; }
	inline String_t** get_address_of_DuplicateNodeTitle_612() { return &___DuplicateNodeTitle_612; }
	inline void set_DuplicateNodeTitle_612(String_t* value)
	{
		___DuplicateNodeTitle_612 = value;
		Il2CppCodeGenWriteBarrier((&___DuplicateNodeTitle_612), value);
	}

	inline static int32_t get_offset_of_NoGraphReferencedMessage_613() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoGraphReferencedMessage_613)); }
	inline String_t* get_NoGraphReferencedMessage_613() const { return ___NoGraphReferencedMessage_613; }
	inline String_t** get_address_of_NoGraphReferencedMessage_613() { return &___NoGraphReferencedMessage_613; }
	inline void set_NoGraphReferencedMessage_613(String_t* value)
	{
		___NoGraphReferencedMessage_613 = value;
		Il2CppCodeGenWriteBarrier((&___NoGraphReferencedMessage_613), value);
	}

	inline static int32_t get_offset_of_NoGraphReferencedTitle_614() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoGraphReferencedTitle_614)); }
	inline String_t* get_NoGraphReferencedTitle_614() const { return ___NoGraphReferencedTitle_614; }
	inline String_t** get_address_of_NoGraphReferencedTitle_614() { return &___NoGraphReferencedTitle_614; }
	inline void set_NoGraphReferencedTitle_614(String_t* value)
	{
		___NoGraphReferencedTitle_614 = value;
		Il2CppCodeGenWriteBarrier((&___NoGraphReferencedTitle_614), value);
	}

	inline static int32_t get_offset_of_NoSourceConnectedMessage_615() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSourceConnectedMessage_615)); }
	inline String_t* get_NoSourceConnectedMessage_615() const { return ___NoSourceConnectedMessage_615; }
	inline String_t** get_address_of_NoSourceConnectedMessage_615() { return &___NoSourceConnectedMessage_615; }
	inline void set_NoSourceConnectedMessage_615(String_t* value)
	{
		___NoSourceConnectedMessage_615 = value;
		Il2CppCodeGenWriteBarrier((&___NoSourceConnectedMessage_615), value);
	}

	inline static int32_t get_offset_of_NoSourceConnectedTitle_616() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSourceConnectedTitle_616)); }
	inline String_t* get_NoSourceConnectedTitle_616() const { return ___NoSourceConnectedTitle_616; }
	inline String_t** get_address_of_NoSourceConnectedTitle_616() { return &___NoSourceConnectedTitle_616; }
	inline void set_NoSourceConnectedTitle_616(String_t* value)
	{
		___NoSourceConnectedTitle_616 = value;
		Il2CppCodeGenWriteBarrier((&___NoSourceConnectedTitle_616), value);
	}

	inline static int32_t get_offset_of_NoSubGraphReferencedMessage_617() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSubGraphReferencedMessage_617)); }
	inline String_t* get_NoSubGraphReferencedMessage_617() const { return ___NoSubGraphReferencedMessage_617; }
	inline String_t** get_address_of_NoSubGraphReferencedMessage_617() { return &___NoSubGraphReferencedMessage_617; }
	inline void set_NoSubGraphReferencedMessage_617(String_t* value)
	{
		___NoSubGraphReferencedMessage_617 = value;
		Il2CppCodeGenWriteBarrier((&___NoSubGraphReferencedMessage_617), value);
	}

	inline static int32_t get_offset_of_NoSubGraphReferencedTitle_618() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoSubGraphReferencedTitle_618)); }
	inline String_t* get_NoSubGraphReferencedTitle_618() const { return ___NoSubGraphReferencedTitle_618; }
	inline String_t** get_address_of_NoSubGraphReferencedTitle_618() { return &___NoSubGraphReferencedTitle_618; }
	inline void set_NoSubGraphReferencedTitle_618(String_t* value)
	{
		___NoSubGraphReferencedTitle_618 = value;
		Il2CppCodeGenWriteBarrier((&___NoSubGraphReferencedTitle_618), value);
	}

	inline static int32_t get_offset_of_NoTargetConnectedMessage_619() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoTargetConnectedMessage_619)); }
	inline String_t* get_NoTargetConnectedMessage_619() const { return ___NoTargetConnectedMessage_619; }
	inline String_t** get_address_of_NoTargetConnectedMessage_619() { return &___NoTargetConnectedMessage_619; }
	inline void set_NoTargetConnectedMessage_619(String_t* value)
	{
		___NoTargetConnectedMessage_619 = value;
		Il2CppCodeGenWriteBarrier((&___NoTargetConnectedMessage_619), value);
	}

	inline static int32_t get_offset_of_NoTargetConnectedTitle_620() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NoTargetConnectedTitle_620)); }
	inline String_t* get_NoTargetConnectedTitle_620() const { return ___NoTargetConnectedTitle_620; }
	inline String_t** get_address_of_NoTargetConnectedTitle_620() { return &___NoTargetConnectedTitle_620; }
	inline void set_NoTargetConnectedTitle_620(String_t* value)
	{
		___NoTargetConnectedTitle_620 = value;
		Il2CppCodeGenWriteBarrier((&___NoTargetConnectedTitle_620), value);
	}

	inline static int32_t get_offset_of_NotConnectedMessage_621() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotConnectedMessage_621)); }
	inline String_t* get_NotConnectedMessage_621() const { return ___NotConnectedMessage_621; }
	inline String_t** get_address_of_NotConnectedMessage_621() { return &___NotConnectedMessage_621; }
	inline void set_NotConnectedMessage_621(String_t* value)
	{
		___NotConnectedMessage_621 = value;
		Il2CppCodeGenWriteBarrier((&___NotConnectedMessage_621), value);
	}

	inline static int32_t get_offset_of_NotConnectedTitle_622() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotConnectedTitle_622)); }
	inline String_t* get_NotConnectedTitle_622() const { return ___NotConnectedTitle_622; }
	inline String_t** get_address_of_NotConnectedTitle_622() { return &___NotConnectedTitle_622; }
	inline void set_NotConnectedTitle_622(String_t* value)
	{
		___NotConnectedTitle_622 = value;
		Il2CppCodeGenWriteBarrier((&___NotConnectedTitle_622), value);
	}

	inline static int32_t get_offset_of_NotListeningForAnyGameEventMessage_623() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotListeningForAnyGameEventMessage_623)); }
	inline String_t* get_NotListeningForAnyGameEventMessage_623() const { return ___NotListeningForAnyGameEventMessage_623; }
	inline String_t** get_address_of_NotListeningForAnyGameEventMessage_623() { return &___NotListeningForAnyGameEventMessage_623; }
	inline void set_NotListeningForAnyGameEventMessage_623(String_t* value)
	{
		___NotListeningForAnyGameEventMessage_623 = value;
		Il2CppCodeGenWriteBarrier((&___NotListeningForAnyGameEventMessage_623), value);
	}

	inline static int32_t get_offset_of_NotListeningForAnyGameEventTitle_624() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotListeningForAnyGameEventTitle_624)); }
	inline String_t* get_NotListeningForAnyGameEventTitle_624() const { return ___NotListeningForAnyGameEventTitle_624; }
	inline String_t** get_address_of_NotListeningForAnyGameEventTitle_624() { return &___NotListeningForAnyGameEventTitle_624; }
	inline void set_NotListeningForAnyGameEventTitle_624(String_t* value)
	{
		___NotListeningForAnyGameEventTitle_624 = value;
		Il2CppCodeGenWriteBarrier((&___NotListeningForAnyGameEventTitle_624), value);
	}

	inline static int32_t get_offset_of_NotSendingAnyGameEventMessage_625() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotSendingAnyGameEventMessage_625)); }
	inline String_t* get_NotSendingAnyGameEventMessage_625() const { return ___NotSendingAnyGameEventMessage_625; }
	inline String_t** get_address_of_NotSendingAnyGameEventMessage_625() { return &___NotSendingAnyGameEventMessage_625; }
	inline void set_NotSendingAnyGameEventMessage_625(String_t* value)
	{
		___NotSendingAnyGameEventMessage_625 = value;
		Il2CppCodeGenWriteBarrier((&___NotSendingAnyGameEventMessage_625), value);
	}

	inline static int32_t get_offset_of_NotSendingAnyGameEventTitle_626() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___NotSendingAnyGameEventTitle_626)); }
	inline String_t* get_NotSendingAnyGameEventTitle_626() const { return ___NotSendingAnyGameEventTitle_626; }
	inline String_t** get_address_of_NotSendingAnyGameEventTitle_626() { return &___NotSendingAnyGameEventTitle_626; }
	inline void set_NotSendingAnyGameEventTitle_626(String_t* value)
	{
		___NotSendingAnyGameEventTitle_626 = value;
		Il2CppCodeGenWriteBarrier((&___NotSendingAnyGameEventTitle_626), value);
	}

	inline static int32_t get_offset_of_ProgressTargetAnimatorParameterInfo_627() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ProgressTargetAnimatorParameterInfo_627)); }
	inline String_t* get_ProgressTargetAnimatorParameterInfo_627() const { return ___ProgressTargetAnimatorParameterInfo_627; }
	inline String_t** get_address_of_ProgressTargetAnimatorParameterInfo_627() { return &___ProgressTargetAnimatorParameterInfo_627; }
	inline void set_ProgressTargetAnimatorParameterInfo_627(String_t* value)
	{
		___ProgressTargetAnimatorParameterInfo_627 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressTargetAnimatorParameterInfo_627), value);
	}

	inline static int32_t get_offset_of_ReferencedGraphIsNotSubGraphMessage_628() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ReferencedGraphIsNotSubGraphMessage_628)); }
	inline String_t* get_ReferencedGraphIsNotSubGraphMessage_628() const { return ___ReferencedGraphIsNotSubGraphMessage_628; }
	inline String_t** get_address_of_ReferencedGraphIsNotSubGraphMessage_628() { return &___ReferencedGraphIsNotSubGraphMessage_628; }
	inline void set_ReferencedGraphIsNotSubGraphMessage_628(String_t* value)
	{
		___ReferencedGraphIsNotSubGraphMessage_628 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedGraphIsNotSubGraphMessage_628), value);
	}

	inline static int32_t get_offset_of_ReferencedGraphIsNotSubGraphTitle_629() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ReferencedGraphIsNotSubGraphTitle_629)); }
	inline String_t* get_ReferencedGraphIsNotSubGraphTitle_629() const { return ___ReferencedGraphIsNotSubGraphTitle_629; }
	inline String_t** get_address_of_ReferencedGraphIsNotSubGraphTitle_629() { return &___ReferencedGraphIsNotSubGraphTitle_629; }
	inline void set_ReferencedGraphIsNotSubGraphTitle_629(String_t* value)
	{
		___ReferencedGraphIsNotSubGraphTitle_629 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedGraphIsNotSubGraphTitle_629), value);
	}

	inline static int32_t get_offset_of_ReferencedGraphIsSubGraphMessage_630() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ReferencedGraphIsSubGraphMessage_630)); }
	inline String_t* get_ReferencedGraphIsSubGraphMessage_630() const { return ___ReferencedGraphIsSubGraphMessage_630; }
	inline String_t** get_address_of_ReferencedGraphIsSubGraphMessage_630() { return &___ReferencedGraphIsSubGraphMessage_630; }
	inline void set_ReferencedGraphIsSubGraphMessage_630(String_t* value)
	{
		___ReferencedGraphIsSubGraphMessage_630 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedGraphIsSubGraphMessage_630), value);
	}

	inline static int32_t get_offset_of_ReferencedGraphIsSubGraphTitle_631() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ReferencedGraphIsSubGraphTitle_631)); }
	inline String_t* get_ReferencedGraphIsSubGraphTitle_631() const { return ___ReferencedGraphIsSubGraphTitle_631; }
	inline String_t** get_address_of_ReferencedGraphIsSubGraphTitle_631() { return &___ReferencedGraphIsSubGraphTitle_631; }
	inline void set_ReferencedGraphIsSubGraphTitle_631(String_t* value)
	{
		___ReferencedGraphIsSubGraphTitle_631 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedGraphIsSubGraphTitle_631), value);
	}

	inline static int32_t get_offset_of_SomeProgressTargetsGetUpdatedOnlyInPlayMode_632() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632)); }
	inline String_t* get_SomeProgressTargetsGetUpdatedOnlyInPlayMode_632() const { return ___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632; }
	inline String_t** get_address_of_SomeProgressTargetsGetUpdatedOnlyInPlayMode_632() { return &___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632; }
	inline void set_SomeProgressTargetsGetUpdatedOnlyInPlayMode_632(String_t* value)
	{
		___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632 = value;
		Il2CppCodeGenWriteBarrier((&___SomeProgressTargetsGetUpdatedOnlyInPlayMode_632), value);
	}

	inline static int32_t get_offset_of_SupportForMasterAudioNotEnabled_633() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SupportForMasterAudioNotEnabled_633)); }
	inline String_t* get_SupportForMasterAudioNotEnabled_633() const { return ___SupportForMasterAudioNotEnabled_633; }
	inline String_t** get_address_of_SupportForMasterAudioNotEnabled_633() { return &___SupportForMasterAudioNotEnabled_633; }
	inline void set_SupportForMasterAudioNotEnabled_633(String_t* value)
	{
		___SupportForMasterAudioNotEnabled_633 = value;
		Il2CppCodeGenWriteBarrier((&___SupportForMasterAudioNotEnabled_633), value);
	}

	inline static int32_t get_offset_of_SupportForPlaymakerNotEnabled_634() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SupportForPlaymakerNotEnabled_634)); }
	inline String_t* get_SupportForPlaymakerNotEnabled_634() const { return ___SupportForPlaymakerNotEnabled_634; }
	inline String_t** get_address_of_SupportForPlaymakerNotEnabled_634() { return &___SupportForPlaymakerNotEnabled_634; }
	inline void set_SupportForPlaymakerNotEnabled_634(String_t* value)
	{
		___SupportForPlaymakerNotEnabled_634 = value;
		Il2CppCodeGenWriteBarrier((&___SupportForPlaymakerNotEnabled_634), value);
	}

	inline static int32_t get_offset_of_SupportForTextMeshProNotEnabled_635() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___SupportForTextMeshProNotEnabled_635)); }
	inline String_t* get_SupportForTextMeshProNotEnabled_635() const { return ___SupportForTextMeshProNotEnabled_635; }
	inline String_t** get_address_of_SupportForTextMeshProNotEnabled_635() { return &___SupportForTextMeshProNotEnabled_635; }
	inline void set_SupportForTextMeshProNotEnabled_635(String_t* value)
	{
		___SupportForTextMeshProNotEnabled_635 = value;
		Il2CppCodeGenWriteBarrier((&___SupportForTextMeshProNotEnabled_635), value);
	}

	inline static int32_t get_offset_of_ThisClassShouldBeExtended_636() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___ThisClassShouldBeExtended_636)); }
	inline String_t* get_ThisClassShouldBeExtended_636() const { return ___ThisClassShouldBeExtended_636; }
	inline String_t** get_address_of_ThisClassShouldBeExtended_636() { return &___ThisClassShouldBeExtended_636; }
	inline void set_ThisClassShouldBeExtended_636(String_t* value)
	{
		___ThisClassShouldBeExtended_636 = value;
		Il2CppCodeGenWriteBarrier((&___ThisClassShouldBeExtended_636), value);
	}

	inline static int32_t get_offset_of_UnnamedNodeMessage_637() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnnamedNodeMessage_637)); }
	inline String_t* get_UnnamedNodeMessage_637() const { return ___UnnamedNodeMessage_637; }
	inline String_t** get_address_of_UnnamedNodeMessage_637() { return &___UnnamedNodeMessage_637; }
	inline void set_UnnamedNodeMessage_637(String_t* value)
	{
		___UnnamedNodeMessage_637 = value;
		Il2CppCodeGenWriteBarrier((&___UnnamedNodeMessage_637), value);
	}

	inline static int32_t get_offset_of_UnnamedNodeTitle_638() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29, ___UnnamedNodeTitle_638)); }
	inline String_t* get_UnnamedNodeTitle_638() const { return ___UnnamedNodeTitle_638; }
	inline String_t** get_address_of_UnnamedNodeTitle_638() { return &___UnnamedNodeTitle_638; }
	inline void set_UnnamedNodeTitle_638(String_t* value)
	{
		___UnnamedNodeTitle_638 = value;
		Il2CppCodeGenWriteBarrier((&___UnnamedNodeTitle_638), value);
	}
};

struct UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields
{
public:
	// Doozy.Engine.Language Doozy.Engine.Utils.UILanguagePack::s_loadedLanguage
	int32_t ___s_loadedLanguage_7;
	// Doozy.Engine.Utils.UILanguagePack Doozy.Engine.Utils.UILanguagePack::s_instance
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29 * ___s_instance_8;

public:
	inline static int32_t get_offset_of_s_loadedLanguage_7() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields, ___s_loadedLanguage_7)); }
	inline int32_t get_s_loadedLanguage_7() const { return ___s_loadedLanguage_7; }
	inline int32_t* get_address_of_s_loadedLanguage_7() { return &___s_loadedLanguage_7; }
	inline void set_s_loadedLanguage_7(int32_t value)
	{
		___s_loadedLanguage_7 = value;
	}

	inline static int32_t get_offset_of_s_instance_8() { return static_cast<int32_t>(offsetof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields, ___s_instance_8)); }
	inline UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29 * get_s_instance_8() const { return ___s_instance_8; }
	inline UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29 ** get_address_of_s_instance_8() { return &___s_instance_8; }
	inline void set_s_instance_8(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29 * value)
	{
		___s_instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILANGUAGEPACK_TE7D8D00757F959D5424D6E03F460B617D47A0D29_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef COROUTINER_T3D935290A596856B403CB67CC2386BF565BFC095_H
#define COROUTINER_T3D935290A596856B403CB67CC2386BF565BFC095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Coroutiner
struct  Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095_StaticFields
{
public:
	// Doozy.Engine.Coroutiner Doozy.Engine.Coroutiner::s_instance
	Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095 * ___s_instance_4;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095_StaticFields, ___s_instance_4)); }
	inline Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095 * get_s_instance_4() const { return ___s_instance_4; }
	inline Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINER_T3D935290A596856B403CB67CC2386BF565BFC095_H
#ifndef GAMEEVENTLISTENER_TFE0DBD55DAFB2378BBBAC17ACED407DD7637222D_H
#define GAMEEVENTLISTENER_TFE0DBD55DAFB2378BBBAC17ACED407DD7637222D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.GameEventListener
struct  GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.GameEventListener::DebugMode
	bool ___DebugMode_4;
	// Doozy.Engine.Events.StringEvent Doozy.Engine.GameEventListener::Event
	StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6 * ___Event_5;
	// System.String Doozy.Engine.GameEventListener::GameEvent
	String_t* ___GameEvent_6;
	// System.Boolean Doozy.Engine.GameEventListener::ListenForAllGameEvents
	bool ___ListenForAllGameEvents_7;

public:
	inline static int32_t get_offset_of_DebugMode_4() { return static_cast<int32_t>(offsetof(GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D, ___DebugMode_4)); }
	inline bool get_DebugMode_4() const { return ___DebugMode_4; }
	inline bool* get_address_of_DebugMode_4() { return &___DebugMode_4; }
	inline void set_DebugMode_4(bool value)
	{
		___DebugMode_4 = value;
	}

	inline static int32_t get_offset_of_Event_5() { return static_cast<int32_t>(offsetof(GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D, ___Event_5)); }
	inline StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6 * get_Event_5() const { return ___Event_5; }
	inline StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6 ** get_address_of_Event_5() { return &___Event_5; }
	inline void set_Event_5(StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6 * value)
	{
		___Event_5 = value;
		Il2CppCodeGenWriteBarrier((&___Event_5), value);
	}

	inline static int32_t get_offset_of_GameEvent_6() { return static_cast<int32_t>(offsetof(GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D, ___GameEvent_6)); }
	inline String_t* get_GameEvent_6() const { return ___GameEvent_6; }
	inline String_t** get_address_of_GameEvent_6() { return &___GameEvent_6; }
	inline void set_GameEvent_6(String_t* value)
	{
		___GameEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_6), value);
	}

	inline static int32_t get_offset_of_ListenForAllGameEvents_7() { return static_cast<int32_t>(offsetof(GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D, ___ListenForAllGameEvents_7)); }
	inline bool get_ListenForAllGameEvents_7() const { return ___ListenForAllGameEvents_7; }
	inline bool* get_address_of_ListenForAllGameEvents_7() { return &___ListenForAllGameEvents_7; }
	inline void set_ListenForAllGameEvents_7(bool value)
	{
		___ListenForAllGameEvents_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTLISTENER_TFE0DBD55DAFB2378BBBAC17ACED407DD7637222D_H
#ifndef GAMEEVENTMANAGER_T0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_H
#define GAMEEVENTMANAGER_T0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.GameEventManager
struct  GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields
{
public:
	// Doozy.Engine.GameEventManager Doozy.Engine.GameEventManager::s_instance
	GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6 * ___s_instance_4;
	// System.Boolean Doozy.Engine.GameEventManager::<ApplicationIsQuitting>k__BackingField
	bool ___U3CApplicationIsQuittingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields, ___s_instance_4)); }
	inline GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6 * get_s_instance_4() const { return ___s_instance_4; }
	inline GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields, ___U3CApplicationIsQuittingU3Ek__BackingField_5)); }
	inline bool get_U3CApplicationIsQuittingU3Ek__BackingField_5() const { return ___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return &___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline void set_U3CApplicationIsQuittingU3Ek__BackingField_5(bool value)
	{
		___U3CApplicationIsQuittingU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTMANAGER_T0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_H
#ifndef PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#define PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTarget
struct  ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#ifndef EXAMPLESHOWPOPUP_T62083886546870E3F44F9957F38A94595A28CF4C_H
#define EXAMPLESHOWPOPUP_T62083886546870E3F44F9957F38A94595A28CF4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Examples.ExampleShowPopup
struct  ExampleShowPopup_t62083886546870E3F44F9957F38A94595A28CF4C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESHOWPOPUP_T62083886546870E3F44F9957F38A94595A28CF4C_H
#ifndef UIBUTTONAUTOMATEDTEXTVALUE_T1B2551623340A131E2C9733454178C180EE95CD3_H
#define UIBUTTONAUTOMATEDTEXTVALUE_T1B2551623340A131E2C9733454178C180EE95CD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Examples.UIButtonAutomatedTextValue
struct  UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Doozy.Examples.UIButtonAutomatedTextValue::PresetCategory
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___PresetCategory_4;
	// UnityEngine.UI.Text Doozy.Examples.UIButtonAutomatedTextValue::PresetName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___PresetName_5;
	// Doozy.Engine.UI.UIButtonBehaviorType Doozy.Examples.UIButtonAutomatedTextValue::BehaviorType
	int32_t ___BehaviorType_6;

public:
	inline static int32_t get_offset_of_PresetCategory_4() { return static_cast<int32_t>(offsetof(UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3, ___PresetCategory_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_PresetCategory_4() const { return ___PresetCategory_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_PresetCategory_4() { return &___PresetCategory_4; }
	inline void set_PresetCategory_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___PresetCategory_4 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_4), value);
	}

	inline static int32_t get_offset_of_PresetName_5() { return static_cast<int32_t>(offsetof(UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3, ___PresetName_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_PresetName_5() const { return ___PresetName_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_PresetName_5() { return &___PresetName_5; }
	inline void set_PresetName_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___PresetName_5 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_5), value);
	}

	inline static int32_t get_offset_of_BehaviorType_6() { return static_cast<int32_t>(offsetof(UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3, ___BehaviorType_6)); }
	inline int32_t get_BehaviorType_6() const { return ___BehaviorType_6; }
	inline int32_t* get_address_of_BehaviorType_6() { return &___BehaviorType_6; }
	inline void set_BehaviorType_6(int32_t value)
	{
		___BehaviorType_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONAUTOMATEDTEXTVALUE_T1B2551623340A131E2C9733454178C180EE95CD3_H
#ifndef PLAYMAKEREVENTDISPATCHER_TDBF88F4151445147881C798BB0FECB7722B3BA3F_H
#define PLAYMAKEREVENTDISPATCHER_TDBF88F4151445147881C798BB0FECB7722B3BA3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Integrations.Playmaker.PlaymakerEventDispatcher
struct  PlaymakerEventDispatcher_tDBF88F4151445147881C798BB0FECB7722B3BA3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKEREVENTDISPATCHER_TDBF88F4151445147881C798BB0FECB7722B3BA3F_H
#ifndef EASYTOUCH_T6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_H
#define EASYTOUCH_T6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch
struct  EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch::_currentGesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ____currentGesture_44;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture> HedgehogTeam.EasyTouch.EasyTouch::_currentGestures
	List_1_t4247767F3D684EE6D1C2815C234363AA5767B283 * ____currentGestures_45;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable
	bool ___enable_46;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableRemote
	bool ___enableRemote_47;
	// HedgehogTeam.EasyTouch.EasyTouch_GesturePriority HedgehogTeam.EasyTouch.EasyTouch::gesturePriority
	int32_t ___gesturePriority_48;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::StationaryTolerance
	float ___StationaryTolerance_49;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::longTapTime
	float ___longTapTime_50;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::swipeTolerance
	float ___swipeTolerance_51;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minPinchLength
	float ___minPinchLength_52;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minTwistAngle
	float ___minTwistAngle_53;
	// HedgehogTeam.EasyTouch.EasyTouch_DoubleTapDetection HedgehogTeam.EasyTouch.EasyTouch::doubleTapDetection
	int32_t ___doubleTapDetection_54;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::doubleTapTime
	float ___doubleTapTime_55;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::alwaysSendSwipe
	bool ___alwaysSendSwipe_56;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersGesture
	bool ___enable2FingersGesture_57;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableTwist
	bool ___enableTwist_58;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enablePinch
	bool ___enablePinch_59;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersSwipe
	bool ___enable2FingersSwipe_60;
	// HedgehogTeam.EasyTouch.EasyTouch_TwoFingerPickMethod HedgehogTeam.EasyTouch.EasyTouch::twoFingerPickMethod
	int32_t ___twoFingerPickMethod_61;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch::touchCameras
	List_1_tFBA3FF3651F5CED1D17338B3B16949F652FC67EF * ___touchCameras_62;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoSelect
	bool ___autoSelect_63;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers3D
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___pickableLayers3D_64;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2D
	bool ___enable2D_65;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers2D
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___pickableLayers2D_66;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedObject
	bool ___autoUpdatePickedObject_67;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::allowUIDetection
	bool ___allowUIDetection_68;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableUIMode
	bool ___enableUIMode_69;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedUI
	bool ___autoUpdatePickedUI_70;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enabledNGuiMode
	bool ___enabledNGuiMode_71;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::nGUILayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___nGUILayers_72;
	// System.Collections.Generic.List`1<UnityEngine.Camera> HedgehogTeam.EasyTouch.EasyTouch::nGUICameras
	List_1_tBCCD79D8856B6659FEAA93388A85C5AF306364A6 * ___nGUICameras_73;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableSimulation
	bool ___enableSimulation_74;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::twistKey
	int32_t ___twistKey_75;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::swipeKey
	int32_t ___swipeKey_76;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGuiInspector
	bool ___showGuiInspector_77;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSelectInspector
	bool ___showSelectInspector_78;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGestureInspector
	bool ___showGestureInspector_79;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showTwoFingerInspector
	bool ___showTwoFingerInspector_80;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSecondFingerInspector
	bool ___showSecondFingerInspector_81;
	// HedgehogTeam.EasyTouch.EasyTouchInput HedgehogTeam.EasyTouch.EasyTouch::input
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708 * ___input_82;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::fingers
	FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* ___fingers_83;
	// UnityEngine.Texture HedgehogTeam.EasyTouch.EasyTouch::secondFingerTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___secondFingerTexture_84;
	// HedgehogTeam.EasyTouch.TwoFingerGesture HedgehogTeam.EasyTouch.EasyTouch::twoFinger
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B * ___twoFinger_85;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch::oldTouchCount
	int32_t ___oldTouchCount_86;
	// HedgehogTeam.EasyTouch.EasyTouch_DoubleTap[] HedgehogTeam.EasyTouch.EasyTouch::singleDoubleTap
	DoubleTapU5BU5D_t35C6527C7D639577BA6AD5110AB8199AEA4C3E98* ___singleDoubleTap_87;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::tmpArray
	FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* ___tmpArray_88;
	// HedgehogTeam.EasyTouch.EasyTouch_PickedObject HedgehogTeam.EasyTouch.EasyTouch::pickedObject
	PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05 * ___pickedObject_89;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HedgehogTeam.EasyTouch.EasyTouch::uiRaycastResultCache
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___uiRaycastResultCache_90;
	// UnityEngine.EventSystems.PointerEventData HedgehogTeam.EasyTouch.EasyTouch::uiPointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___uiPointerEventData_91;
	// UnityEngine.EventSystems.EventSystem HedgehogTeam.EasyTouch.EasyTouch::uiEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___uiEventSystem_92;

public:
	inline static int32_t get_offset_of__currentGesture_44() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ____currentGesture_44)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get__currentGesture_44() const { return ____currentGesture_44; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of__currentGesture_44() { return &____currentGesture_44; }
	inline void set__currentGesture_44(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		____currentGesture_44 = value;
		Il2CppCodeGenWriteBarrier((&____currentGesture_44), value);
	}

	inline static int32_t get_offset_of__currentGestures_45() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ____currentGestures_45)); }
	inline List_1_t4247767F3D684EE6D1C2815C234363AA5767B283 * get__currentGestures_45() const { return ____currentGestures_45; }
	inline List_1_t4247767F3D684EE6D1C2815C234363AA5767B283 ** get_address_of__currentGestures_45() { return &____currentGestures_45; }
	inline void set__currentGestures_45(List_1_t4247767F3D684EE6D1C2815C234363AA5767B283 * value)
	{
		____currentGestures_45 = value;
		Il2CppCodeGenWriteBarrier((&____currentGestures_45), value);
	}

	inline static int32_t get_offset_of_enable_46() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enable_46)); }
	inline bool get_enable_46() const { return ___enable_46; }
	inline bool* get_address_of_enable_46() { return &___enable_46; }
	inline void set_enable_46(bool value)
	{
		___enable_46 = value;
	}

	inline static int32_t get_offset_of_enableRemote_47() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enableRemote_47)); }
	inline bool get_enableRemote_47() const { return ___enableRemote_47; }
	inline bool* get_address_of_enableRemote_47() { return &___enableRemote_47; }
	inline void set_enableRemote_47(bool value)
	{
		___enableRemote_47 = value;
	}

	inline static int32_t get_offset_of_gesturePriority_48() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___gesturePriority_48)); }
	inline int32_t get_gesturePriority_48() const { return ___gesturePriority_48; }
	inline int32_t* get_address_of_gesturePriority_48() { return &___gesturePriority_48; }
	inline void set_gesturePriority_48(int32_t value)
	{
		___gesturePriority_48 = value;
	}

	inline static int32_t get_offset_of_StationaryTolerance_49() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___StationaryTolerance_49)); }
	inline float get_StationaryTolerance_49() const { return ___StationaryTolerance_49; }
	inline float* get_address_of_StationaryTolerance_49() { return &___StationaryTolerance_49; }
	inline void set_StationaryTolerance_49(float value)
	{
		___StationaryTolerance_49 = value;
	}

	inline static int32_t get_offset_of_longTapTime_50() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___longTapTime_50)); }
	inline float get_longTapTime_50() const { return ___longTapTime_50; }
	inline float* get_address_of_longTapTime_50() { return &___longTapTime_50; }
	inline void set_longTapTime_50(float value)
	{
		___longTapTime_50 = value;
	}

	inline static int32_t get_offset_of_swipeTolerance_51() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___swipeTolerance_51)); }
	inline float get_swipeTolerance_51() const { return ___swipeTolerance_51; }
	inline float* get_address_of_swipeTolerance_51() { return &___swipeTolerance_51; }
	inline void set_swipeTolerance_51(float value)
	{
		___swipeTolerance_51 = value;
	}

	inline static int32_t get_offset_of_minPinchLength_52() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___minPinchLength_52)); }
	inline float get_minPinchLength_52() const { return ___minPinchLength_52; }
	inline float* get_address_of_minPinchLength_52() { return &___minPinchLength_52; }
	inline void set_minPinchLength_52(float value)
	{
		___minPinchLength_52 = value;
	}

	inline static int32_t get_offset_of_minTwistAngle_53() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___minTwistAngle_53)); }
	inline float get_minTwistAngle_53() const { return ___minTwistAngle_53; }
	inline float* get_address_of_minTwistAngle_53() { return &___minTwistAngle_53; }
	inline void set_minTwistAngle_53(float value)
	{
		___minTwistAngle_53 = value;
	}

	inline static int32_t get_offset_of_doubleTapDetection_54() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___doubleTapDetection_54)); }
	inline int32_t get_doubleTapDetection_54() const { return ___doubleTapDetection_54; }
	inline int32_t* get_address_of_doubleTapDetection_54() { return &___doubleTapDetection_54; }
	inline void set_doubleTapDetection_54(int32_t value)
	{
		___doubleTapDetection_54 = value;
	}

	inline static int32_t get_offset_of_doubleTapTime_55() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___doubleTapTime_55)); }
	inline float get_doubleTapTime_55() const { return ___doubleTapTime_55; }
	inline float* get_address_of_doubleTapTime_55() { return &___doubleTapTime_55; }
	inline void set_doubleTapTime_55(float value)
	{
		___doubleTapTime_55 = value;
	}

	inline static int32_t get_offset_of_alwaysSendSwipe_56() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___alwaysSendSwipe_56)); }
	inline bool get_alwaysSendSwipe_56() const { return ___alwaysSendSwipe_56; }
	inline bool* get_address_of_alwaysSendSwipe_56() { return &___alwaysSendSwipe_56; }
	inline void set_alwaysSendSwipe_56(bool value)
	{
		___alwaysSendSwipe_56 = value;
	}

	inline static int32_t get_offset_of_enable2FingersGesture_57() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enable2FingersGesture_57)); }
	inline bool get_enable2FingersGesture_57() const { return ___enable2FingersGesture_57; }
	inline bool* get_address_of_enable2FingersGesture_57() { return &___enable2FingersGesture_57; }
	inline void set_enable2FingersGesture_57(bool value)
	{
		___enable2FingersGesture_57 = value;
	}

	inline static int32_t get_offset_of_enableTwist_58() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enableTwist_58)); }
	inline bool get_enableTwist_58() const { return ___enableTwist_58; }
	inline bool* get_address_of_enableTwist_58() { return &___enableTwist_58; }
	inline void set_enableTwist_58(bool value)
	{
		___enableTwist_58 = value;
	}

	inline static int32_t get_offset_of_enablePinch_59() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enablePinch_59)); }
	inline bool get_enablePinch_59() const { return ___enablePinch_59; }
	inline bool* get_address_of_enablePinch_59() { return &___enablePinch_59; }
	inline void set_enablePinch_59(bool value)
	{
		___enablePinch_59 = value;
	}

	inline static int32_t get_offset_of_enable2FingersSwipe_60() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enable2FingersSwipe_60)); }
	inline bool get_enable2FingersSwipe_60() const { return ___enable2FingersSwipe_60; }
	inline bool* get_address_of_enable2FingersSwipe_60() { return &___enable2FingersSwipe_60; }
	inline void set_enable2FingersSwipe_60(bool value)
	{
		___enable2FingersSwipe_60 = value;
	}

	inline static int32_t get_offset_of_twoFingerPickMethod_61() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___twoFingerPickMethod_61)); }
	inline int32_t get_twoFingerPickMethod_61() const { return ___twoFingerPickMethod_61; }
	inline int32_t* get_address_of_twoFingerPickMethod_61() { return &___twoFingerPickMethod_61; }
	inline void set_twoFingerPickMethod_61(int32_t value)
	{
		___twoFingerPickMethod_61 = value;
	}

	inline static int32_t get_offset_of_touchCameras_62() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___touchCameras_62)); }
	inline List_1_tFBA3FF3651F5CED1D17338B3B16949F652FC67EF * get_touchCameras_62() const { return ___touchCameras_62; }
	inline List_1_tFBA3FF3651F5CED1D17338B3B16949F652FC67EF ** get_address_of_touchCameras_62() { return &___touchCameras_62; }
	inline void set_touchCameras_62(List_1_tFBA3FF3651F5CED1D17338B3B16949F652FC67EF * value)
	{
		___touchCameras_62 = value;
		Il2CppCodeGenWriteBarrier((&___touchCameras_62), value);
	}

	inline static int32_t get_offset_of_autoSelect_63() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___autoSelect_63)); }
	inline bool get_autoSelect_63() const { return ___autoSelect_63; }
	inline bool* get_address_of_autoSelect_63() { return &___autoSelect_63; }
	inline void set_autoSelect_63(bool value)
	{
		___autoSelect_63 = value;
	}

	inline static int32_t get_offset_of_pickableLayers3D_64() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___pickableLayers3D_64)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_pickableLayers3D_64() const { return ___pickableLayers3D_64; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_pickableLayers3D_64() { return &___pickableLayers3D_64; }
	inline void set_pickableLayers3D_64(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___pickableLayers3D_64 = value;
	}

	inline static int32_t get_offset_of_enable2D_65() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enable2D_65)); }
	inline bool get_enable2D_65() const { return ___enable2D_65; }
	inline bool* get_address_of_enable2D_65() { return &___enable2D_65; }
	inline void set_enable2D_65(bool value)
	{
		___enable2D_65 = value;
	}

	inline static int32_t get_offset_of_pickableLayers2D_66() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___pickableLayers2D_66)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_pickableLayers2D_66() const { return ___pickableLayers2D_66; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_pickableLayers2D_66() { return &___pickableLayers2D_66; }
	inline void set_pickableLayers2D_66(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___pickableLayers2D_66 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedObject_67() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___autoUpdatePickedObject_67)); }
	inline bool get_autoUpdatePickedObject_67() const { return ___autoUpdatePickedObject_67; }
	inline bool* get_address_of_autoUpdatePickedObject_67() { return &___autoUpdatePickedObject_67; }
	inline void set_autoUpdatePickedObject_67(bool value)
	{
		___autoUpdatePickedObject_67 = value;
	}

	inline static int32_t get_offset_of_allowUIDetection_68() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___allowUIDetection_68)); }
	inline bool get_allowUIDetection_68() const { return ___allowUIDetection_68; }
	inline bool* get_address_of_allowUIDetection_68() { return &___allowUIDetection_68; }
	inline void set_allowUIDetection_68(bool value)
	{
		___allowUIDetection_68 = value;
	}

	inline static int32_t get_offset_of_enableUIMode_69() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enableUIMode_69)); }
	inline bool get_enableUIMode_69() const { return ___enableUIMode_69; }
	inline bool* get_address_of_enableUIMode_69() { return &___enableUIMode_69; }
	inline void set_enableUIMode_69(bool value)
	{
		___enableUIMode_69 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedUI_70() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___autoUpdatePickedUI_70)); }
	inline bool get_autoUpdatePickedUI_70() const { return ___autoUpdatePickedUI_70; }
	inline bool* get_address_of_autoUpdatePickedUI_70() { return &___autoUpdatePickedUI_70; }
	inline void set_autoUpdatePickedUI_70(bool value)
	{
		___autoUpdatePickedUI_70 = value;
	}

	inline static int32_t get_offset_of_enabledNGuiMode_71() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enabledNGuiMode_71)); }
	inline bool get_enabledNGuiMode_71() const { return ___enabledNGuiMode_71; }
	inline bool* get_address_of_enabledNGuiMode_71() { return &___enabledNGuiMode_71; }
	inline void set_enabledNGuiMode_71(bool value)
	{
		___enabledNGuiMode_71 = value;
	}

	inline static int32_t get_offset_of_nGUILayers_72() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___nGUILayers_72)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_nGUILayers_72() const { return ___nGUILayers_72; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_nGUILayers_72() { return &___nGUILayers_72; }
	inline void set_nGUILayers_72(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___nGUILayers_72 = value;
	}

	inline static int32_t get_offset_of_nGUICameras_73() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___nGUICameras_73)); }
	inline List_1_tBCCD79D8856B6659FEAA93388A85C5AF306364A6 * get_nGUICameras_73() const { return ___nGUICameras_73; }
	inline List_1_tBCCD79D8856B6659FEAA93388A85C5AF306364A6 ** get_address_of_nGUICameras_73() { return &___nGUICameras_73; }
	inline void set_nGUICameras_73(List_1_tBCCD79D8856B6659FEAA93388A85C5AF306364A6 * value)
	{
		___nGUICameras_73 = value;
		Il2CppCodeGenWriteBarrier((&___nGUICameras_73), value);
	}

	inline static int32_t get_offset_of_enableSimulation_74() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___enableSimulation_74)); }
	inline bool get_enableSimulation_74() const { return ___enableSimulation_74; }
	inline bool* get_address_of_enableSimulation_74() { return &___enableSimulation_74; }
	inline void set_enableSimulation_74(bool value)
	{
		___enableSimulation_74 = value;
	}

	inline static int32_t get_offset_of_twistKey_75() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___twistKey_75)); }
	inline int32_t get_twistKey_75() const { return ___twistKey_75; }
	inline int32_t* get_address_of_twistKey_75() { return &___twistKey_75; }
	inline void set_twistKey_75(int32_t value)
	{
		___twistKey_75 = value;
	}

	inline static int32_t get_offset_of_swipeKey_76() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___swipeKey_76)); }
	inline int32_t get_swipeKey_76() const { return ___swipeKey_76; }
	inline int32_t* get_address_of_swipeKey_76() { return &___swipeKey_76; }
	inline void set_swipeKey_76(int32_t value)
	{
		___swipeKey_76 = value;
	}

	inline static int32_t get_offset_of_showGuiInspector_77() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___showGuiInspector_77)); }
	inline bool get_showGuiInspector_77() const { return ___showGuiInspector_77; }
	inline bool* get_address_of_showGuiInspector_77() { return &___showGuiInspector_77; }
	inline void set_showGuiInspector_77(bool value)
	{
		___showGuiInspector_77 = value;
	}

	inline static int32_t get_offset_of_showSelectInspector_78() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___showSelectInspector_78)); }
	inline bool get_showSelectInspector_78() const { return ___showSelectInspector_78; }
	inline bool* get_address_of_showSelectInspector_78() { return &___showSelectInspector_78; }
	inline void set_showSelectInspector_78(bool value)
	{
		___showSelectInspector_78 = value;
	}

	inline static int32_t get_offset_of_showGestureInspector_79() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___showGestureInspector_79)); }
	inline bool get_showGestureInspector_79() const { return ___showGestureInspector_79; }
	inline bool* get_address_of_showGestureInspector_79() { return &___showGestureInspector_79; }
	inline void set_showGestureInspector_79(bool value)
	{
		___showGestureInspector_79 = value;
	}

	inline static int32_t get_offset_of_showTwoFingerInspector_80() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___showTwoFingerInspector_80)); }
	inline bool get_showTwoFingerInspector_80() const { return ___showTwoFingerInspector_80; }
	inline bool* get_address_of_showTwoFingerInspector_80() { return &___showTwoFingerInspector_80; }
	inline void set_showTwoFingerInspector_80(bool value)
	{
		___showTwoFingerInspector_80 = value;
	}

	inline static int32_t get_offset_of_showSecondFingerInspector_81() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___showSecondFingerInspector_81)); }
	inline bool get_showSecondFingerInspector_81() const { return ___showSecondFingerInspector_81; }
	inline bool* get_address_of_showSecondFingerInspector_81() { return &___showSecondFingerInspector_81; }
	inline void set_showSecondFingerInspector_81(bool value)
	{
		___showSecondFingerInspector_81 = value;
	}

	inline static int32_t get_offset_of_input_82() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___input_82)); }
	inline EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708 * get_input_82() const { return ___input_82; }
	inline EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708 ** get_address_of_input_82() { return &___input_82; }
	inline void set_input_82(EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708 * value)
	{
		___input_82 = value;
		Il2CppCodeGenWriteBarrier((&___input_82), value);
	}

	inline static int32_t get_offset_of_fingers_83() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___fingers_83)); }
	inline FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* get_fingers_83() const { return ___fingers_83; }
	inline FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88** get_address_of_fingers_83() { return &___fingers_83; }
	inline void set_fingers_83(FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* value)
	{
		___fingers_83 = value;
		Il2CppCodeGenWriteBarrier((&___fingers_83), value);
	}

	inline static int32_t get_offset_of_secondFingerTexture_84() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___secondFingerTexture_84)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_secondFingerTexture_84() const { return ___secondFingerTexture_84; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_secondFingerTexture_84() { return &___secondFingerTexture_84; }
	inline void set_secondFingerTexture_84(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___secondFingerTexture_84 = value;
		Il2CppCodeGenWriteBarrier((&___secondFingerTexture_84), value);
	}

	inline static int32_t get_offset_of_twoFinger_85() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___twoFinger_85)); }
	inline TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B * get_twoFinger_85() const { return ___twoFinger_85; }
	inline TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B ** get_address_of_twoFinger_85() { return &___twoFinger_85; }
	inline void set_twoFinger_85(TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B * value)
	{
		___twoFinger_85 = value;
		Il2CppCodeGenWriteBarrier((&___twoFinger_85), value);
	}

	inline static int32_t get_offset_of_oldTouchCount_86() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___oldTouchCount_86)); }
	inline int32_t get_oldTouchCount_86() const { return ___oldTouchCount_86; }
	inline int32_t* get_address_of_oldTouchCount_86() { return &___oldTouchCount_86; }
	inline void set_oldTouchCount_86(int32_t value)
	{
		___oldTouchCount_86 = value;
	}

	inline static int32_t get_offset_of_singleDoubleTap_87() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___singleDoubleTap_87)); }
	inline DoubleTapU5BU5D_t35C6527C7D639577BA6AD5110AB8199AEA4C3E98* get_singleDoubleTap_87() const { return ___singleDoubleTap_87; }
	inline DoubleTapU5BU5D_t35C6527C7D639577BA6AD5110AB8199AEA4C3E98** get_address_of_singleDoubleTap_87() { return &___singleDoubleTap_87; }
	inline void set_singleDoubleTap_87(DoubleTapU5BU5D_t35C6527C7D639577BA6AD5110AB8199AEA4C3E98* value)
	{
		___singleDoubleTap_87 = value;
		Il2CppCodeGenWriteBarrier((&___singleDoubleTap_87), value);
	}

	inline static int32_t get_offset_of_tmpArray_88() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___tmpArray_88)); }
	inline FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* get_tmpArray_88() const { return ___tmpArray_88; }
	inline FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88** get_address_of_tmpArray_88() { return &___tmpArray_88; }
	inline void set_tmpArray_88(FingerU5BU5D_t20606C5FF70F96492257D5BF76951077D79B3D88* value)
	{
		___tmpArray_88 = value;
		Il2CppCodeGenWriteBarrier((&___tmpArray_88), value);
	}

	inline static int32_t get_offset_of_pickedObject_89() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___pickedObject_89)); }
	inline PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05 * get_pickedObject_89() const { return ___pickedObject_89; }
	inline PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05 ** get_address_of_pickedObject_89() { return &___pickedObject_89; }
	inline void set_pickedObject_89(PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05 * value)
	{
		___pickedObject_89 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_89), value);
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_90() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___uiRaycastResultCache_90)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_uiRaycastResultCache_90() const { return ___uiRaycastResultCache_90; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_uiRaycastResultCache_90() { return &___uiRaycastResultCache_90; }
	inline void set_uiRaycastResultCache_90(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___uiRaycastResultCache_90 = value;
		Il2CppCodeGenWriteBarrier((&___uiRaycastResultCache_90), value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_91() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___uiPointerEventData_91)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_uiPointerEventData_91() const { return ___uiPointerEventData_91; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_uiPointerEventData_91() { return &___uiPointerEventData_91; }
	inline void set_uiPointerEventData_91(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___uiPointerEventData_91 = value;
		Il2CppCodeGenWriteBarrier((&___uiPointerEventData_91), value);
	}

	inline static int32_t get_offset_of_uiEventSystem_92() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080, ___uiEventSystem_92)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_uiEventSystem_92() const { return ___uiEventSystem_92; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_uiEventSystem_92() { return &___uiEventSystem_92; }
	inline void set_uiEventSystem_92(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___uiEventSystem_92 = value;
		Il2CppCodeGenWriteBarrier((&___uiEventSystem_92), value);
	}
};

struct EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel
	TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398 * ___On_Cancel_4;
	// HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel2Fingers
	Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * ___On_Cancel2Fingers_5;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart
	TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * ___On_TouchStart_6;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown
	TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * ___On_TouchDown_7;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp
	TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * ___On_TouchUp_8;
	// HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap
	SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3 * ___On_SimpleTap_9;
	// HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap
	DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11 * ___On_DoubleTap_10;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart
	LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1 * ___On_LongTapStart_11;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap
	LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30 * ___On_LongTap_12;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd
	LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB * ___On_LongTapEnd_13;
	// HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart
	DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940 * ___On_DragStart_14;
	// HedgehogTeam.EasyTouch.EasyTouch_DragHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag
	DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801 * ___On_Drag_15;
	// HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd
	DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF * ___On_DragEnd_16;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart
	SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB * ___On_SwipeStart_17;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe
	SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0 * ___On_Swipe_18;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd
	SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F * ___On_SwipeEnd_19;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart2Fingers
	TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * ___On_TouchStart2Fingers_20;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown2Fingers
	TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * ___On_TouchDown2Fingers_21;
	// HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp2Fingers
	TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * ___On_TouchUp2Fingers_22;
	// HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap2Fingers
	SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * ___On_SimpleTap2Fingers_23;
	// HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap2Fingers
	DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * ___On_DoubleTap2Fingers_24;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart2Fingers
	LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * ___On_LongTapStart2Fingers_25;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap2Fingers
	LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * ___On_LongTap2Fingers_26;
	// HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd2Fingers
	LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * ___On_LongTapEnd2Fingers_27;
	// HedgehogTeam.EasyTouch.EasyTouch_TwistHandler HedgehogTeam.EasyTouch.EasyTouch::On_Twist
	TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * ___On_Twist_28;
	// HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_TwistEnd
	TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * ___On_TwistEnd_29;
	// HedgehogTeam.EasyTouch.EasyTouch_PinchHandler HedgehogTeam.EasyTouch.EasyTouch::On_Pinch
	PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * ___On_Pinch_30;
	// HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchIn
	PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29 * ___On_PinchIn_31;
	// HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchOut
	PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B * ___On_PinchOut_32;
	// HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchEnd
	PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57 * ___On_PinchEnd_33;
	// HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart2Fingers
	DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * ___On_DragStart2Fingers_34;
	// HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag2Fingers
	Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * ___On_Drag2Fingers_35;
	// HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd2Fingers
	DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * ___On_DragEnd2Fingers_36;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart2Fingers
	SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * ___On_SwipeStart2Fingers_37;
	// HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe2Fingers
	Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * ___On_Swipe2Fingers_38;
	// HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd2Fingers
	SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * ___On_SwipeEnd2Fingers_39;
	// HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler HedgehogTeam.EasyTouch.EasyTouch::On_EasyTouchIsReady
	EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621 * ___On_EasyTouchIsReady_40;
	// HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler HedgehogTeam.EasyTouch.EasyTouch::On_OverUIElement
	OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F * ___On_OverUIElement_41;
	// HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_UIElementTouchUp
	UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3 * ___On_UIElementTouchUp_42;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch::_instance
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * ____instance_43;

public:
	inline static int32_t get_offset_of_On_Cancel_4() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Cancel_4)); }
	inline TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398 * get_On_Cancel_4() const { return ___On_Cancel_4; }
	inline TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398 ** get_address_of_On_Cancel_4() { return &___On_Cancel_4; }
	inline void set_On_Cancel_4(TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398 * value)
	{
		___On_Cancel_4 = value;
		Il2CppCodeGenWriteBarrier((&___On_Cancel_4), value);
	}

	inline static int32_t get_offset_of_On_Cancel2Fingers_5() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Cancel2Fingers_5)); }
	inline Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * get_On_Cancel2Fingers_5() const { return ___On_Cancel2Fingers_5; }
	inline Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD ** get_address_of_On_Cancel2Fingers_5() { return &___On_Cancel2Fingers_5; }
	inline void set_On_Cancel2Fingers_5(Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD * value)
	{
		___On_Cancel2Fingers_5 = value;
		Il2CppCodeGenWriteBarrier((&___On_Cancel2Fingers_5), value);
	}

	inline static int32_t get_offset_of_On_TouchStart_6() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchStart_6)); }
	inline TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * get_On_TouchStart_6() const { return ___On_TouchStart_6; }
	inline TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 ** get_address_of_On_TouchStart_6() { return &___On_TouchStart_6; }
	inline void set_On_TouchStart_6(TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853 * value)
	{
		___On_TouchStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchStart_6), value);
	}

	inline static int32_t get_offset_of_On_TouchDown_7() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchDown_7)); }
	inline TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * get_On_TouchDown_7() const { return ___On_TouchDown_7; }
	inline TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 ** get_address_of_On_TouchDown_7() { return &___On_TouchDown_7; }
	inline void set_On_TouchDown_7(TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955 * value)
	{
		___On_TouchDown_7 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchDown_7), value);
	}

	inline static int32_t get_offset_of_On_TouchUp_8() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchUp_8)); }
	inline TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * get_On_TouchUp_8() const { return ___On_TouchUp_8; }
	inline TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E ** get_address_of_On_TouchUp_8() { return &___On_TouchUp_8; }
	inline void set_On_TouchUp_8(TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E * value)
	{
		___On_TouchUp_8 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchUp_8), value);
	}

	inline static int32_t get_offset_of_On_SimpleTap_9() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SimpleTap_9)); }
	inline SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3 * get_On_SimpleTap_9() const { return ___On_SimpleTap_9; }
	inline SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3 ** get_address_of_On_SimpleTap_9() { return &___On_SimpleTap_9; }
	inline void set_On_SimpleTap_9(SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3 * value)
	{
		___On_SimpleTap_9 = value;
		Il2CppCodeGenWriteBarrier((&___On_SimpleTap_9), value);
	}

	inline static int32_t get_offset_of_On_DoubleTap_10() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DoubleTap_10)); }
	inline DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11 * get_On_DoubleTap_10() const { return ___On_DoubleTap_10; }
	inline DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11 ** get_address_of_On_DoubleTap_10() { return &___On_DoubleTap_10; }
	inline void set_On_DoubleTap_10(DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11 * value)
	{
		___On_DoubleTap_10 = value;
		Il2CppCodeGenWriteBarrier((&___On_DoubleTap_10), value);
	}

	inline static int32_t get_offset_of_On_LongTapStart_11() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTapStart_11)); }
	inline LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1 * get_On_LongTapStart_11() const { return ___On_LongTapStart_11; }
	inline LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1 ** get_address_of_On_LongTapStart_11() { return &___On_LongTapStart_11; }
	inline void set_On_LongTapStart_11(LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1 * value)
	{
		___On_LongTapStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapStart_11), value);
	}

	inline static int32_t get_offset_of_On_LongTap_12() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTap_12)); }
	inline LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30 * get_On_LongTap_12() const { return ___On_LongTap_12; }
	inline LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30 ** get_address_of_On_LongTap_12() { return &___On_LongTap_12; }
	inline void set_On_LongTap_12(LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30 * value)
	{
		___On_LongTap_12 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTap_12), value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd_13() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTapEnd_13)); }
	inline LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB * get_On_LongTapEnd_13() const { return ___On_LongTapEnd_13; }
	inline LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB ** get_address_of_On_LongTapEnd_13() { return &___On_LongTapEnd_13; }
	inline void set_On_LongTapEnd_13(LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB * value)
	{
		___On_LongTapEnd_13 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapEnd_13), value);
	}

	inline static int32_t get_offset_of_On_DragStart_14() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DragStart_14)); }
	inline DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940 * get_On_DragStart_14() const { return ___On_DragStart_14; }
	inline DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940 ** get_address_of_On_DragStart_14() { return &___On_DragStart_14; }
	inline void set_On_DragStart_14(DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940 * value)
	{
		___On_DragStart_14 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragStart_14), value);
	}

	inline static int32_t get_offset_of_On_Drag_15() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Drag_15)); }
	inline DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801 * get_On_Drag_15() const { return ___On_Drag_15; }
	inline DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801 ** get_address_of_On_Drag_15() { return &___On_Drag_15; }
	inline void set_On_Drag_15(DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801 * value)
	{
		___On_Drag_15 = value;
		Il2CppCodeGenWriteBarrier((&___On_Drag_15), value);
	}

	inline static int32_t get_offset_of_On_DragEnd_16() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DragEnd_16)); }
	inline DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF * get_On_DragEnd_16() const { return ___On_DragEnd_16; }
	inline DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF ** get_address_of_On_DragEnd_16() { return &___On_DragEnd_16; }
	inline void set_On_DragEnd_16(DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF * value)
	{
		___On_DragEnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragEnd_16), value);
	}

	inline static int32_t get_offset_of_On_SwipeStart_17() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SwipeStart_17)); }
	inline SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB * get_On_SwipeStart_17() const { return ___On_SwipeStart_17; }
	inline SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB ** get_address_of_On_SwipeStart_17() { return &___On_SwipeStart_17; }
	inline void set_On_SwipeStart_17(SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB * value)
	{
		___On_SwipeStart_17 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeStart_17), value);
	}

	inline static int32_t get_offset_of_On_Swipe_18() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Swipe_18)); }
	inline SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0 * get_On_Swipe_18() const { return ___On_Swipe_18; }
	inline SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0 ** get_address_of_On_Swipe_18() { return &___On_Swipe_18; }
	inline void set_On_Swipe_18(SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0 * value)
	{
		___On_Swipe_18 = value;
		Il2CppCodeGenWriteBarrier((&___On_Swipe_18), value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd_19() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SwipeEnd_19)); }
	inline SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F * get_On_SwipeEnd_19() const { return ___On_SwipeEnd_19; }
	inline SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F ** get_address_of_On_SwipeEnd_19() { return &___On_SwipeEnd_19; }
	inline void set_On_SwipeEnd_19(SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F * value)
	{
		___On_SwipeEnd_19 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeEnd_19), value);
	}

	inline static int32_t get_offset_of_On_TouchStart2Fingers_20() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchStart2Fingers_20)); }
	inline TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * get_On_TouchStart2Fingers_20() const { return ___On_TouchStart2Fingers_20; }
	inline TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE ** get_address_of_On_TouchStart2Fingers_20() { return &___On_TouchStart2Fingers_20; }
	inline void set_On_TouchStart2Fingers_20(TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE * value)
	{
		___On_TouchStart2Fingers_20 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchStart2Fingers_20), value);
	}

	inline static int32_t get_offset_of_On_TouchDown2Fingers_21() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchDown2Fingers_21)); }
	inline TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * get_On_TouchDown2Fingers_21() const { return ___On_TouchDown2Fingers_21; }
	inline TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 ** get_address_of_On_TouchDown2Fingers_21() { return &___On_TouchDown2Fingers_21; }
	inline void set_On_TouchDown2Fingers_21(TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454 * value)
	{
		___On_TouchDown2Fingers_21 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchDown2Fingers_21), value);
	}

	inline static int32_t get_offset_of_On_TouchUp2Fingers_22() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TouchUp2Fingers_22)); }
	inline TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * get_On_TouchUp2Fingers_22() const { return ___On_TouchUp2Fingers_22; }
	inline TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 ** get_address_of_On_TouchUp2Fingers_22() { return &___On_TouchUp2Fingers_22; }
	inline void set_On_TouchUp2Fingers_22(TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0 * value)
	{
		___On_TouchUp2Fingers_22 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchUp2Fingers_22), value);
	}

	inline static int32_t get_offset_of_On_SimpleTap2Fingers_23() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SimpleTap2Fingers_23)); }
	inline SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * get_On_SimpleTap2Fingers_23() const { return ___On_SimpleTap2Fingers_23; }
	inline SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 ** get_address_of_On_SimpleTap2Fingers_23() { return &___On_SimpleTap2Fingers_23; }
	inline void set_On_SimpleTap2Fingers_23(SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30 * value)
	{
		___On_SimpleTap2Fingers_23 = value;
		Il2CppCodeGenWriteBarrier((&___On_SimpleTap2Fingers_23), value);
	}

	inline static int32_t get_offset_of_On_DoubleTap2Fingers_24() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DoubleTap2Fingers_24)); }
	inline DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * get_On_DoubleTap2Fingers_24() const { return ___On_DoubleTap2Fingers_24; }
	inline DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 ** get_address_of_On_DoubleTap2Fingers_24() { return &___On_DoubleTap2Fingers_24; }
	inline void set_On_DoubleTap2Fingers_24(DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8 * value)
	{
		___On_DoubleTap2Fingers_24 = value;
		Il2CppCodeGenWriteBarrier((&___On_DoubleTap2Fingers_24), value);
	}

	inline static int32_t get_offset_of_On_LongTapStart2Fingers_25() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTapStart2Fingers_25)); }
	inline LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * get_On_LongTapStart2Fingers_25() const { return ___On_LongTapStart2Fingers_25; }
	inline LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 ** get_address_of_On_LongTapStart2Fingers_25() { return &___On_LongTapStart2Fingers_25; }
	inline void set_On_LongTapStart2Fingers_25(LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779 * value)
	{
		___On_LongTapStart2Fingers_25 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapStart2Fingers_25), value);
	}

	inline static int32_t get_offset_of_On_LongTap2Fingers_26() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTap2Fingers_26)); }
	inline LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * get_On_LongTap2Fingers_26() const { return ___On_LongTap2Fingers_26; }
	inline LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 ** get_address_of_On_LongTap2Fingers_26() { return &___On_LongTap2Fingers_26; }
	inline void set_On_LongTap2Fingers_26(LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6 * value)
	{
		___On_LongTap2Fingers_26 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTap2Fingers_26), value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd2Fingers_27() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_LongTapEnd2Fingers_27)); }
	inline LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * get_On_LongTapEnd2Fingers_27() const { return ___On_LongTapEnd2Fingers_27; }
	inline LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 ** get_address_of_On_LongTapEnd2Fingers_27() { return &___On_LongTapEnd2Fingers_27; }
	inline void set_On_LongTapEnd2Fingers_27(LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8 * value)
	{
		___On_LongTapEnd2Fingers_27 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapEnd2Fingers_27), value);
	}

	inline static int32_t get_offset_of_On_Twist_28() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Twist_28)); }
	inline TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * get_On_Twist_28() const { return ___On_Twist_28; }
	inline TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D ** get_address_of_On_Twist_28() { return &___On_Twist_28; }
	inline void set_On_Twist_28(TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D * value)
	{
		___On_Twist_28 = value;
		Il2CppCodeGenWriteBarrier((&___On_Twist_28), value);
	}

	inline static int32_t get_offset_of_On_TwistEnd_29() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_TwistEnd_29)); }
	inline TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * get_On_TwistEnd_29() const { return ___On_TwistEnd_29; }
	inline TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 ** get_address_of_On_TwistEnd_29() { return &___On_TwistEnd_29; }
	inline void set_On_TwistEnd_29(TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929 * value)
	{
		___On_TwistEnd_29 = value;
		Il2CppCodeGenWriteBarrier((&___On_TwistEnd_29), value);
	}

	inline static int32_t get_offset_of_On_Pinch_30() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Pinch_30)); }
	inline PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * get_On_Pinch_30() const { return ___On_Pinch_30; }
	inline PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 ** get_address_of_On_Pinch_30() { return &___On_Pinch_30; }
	inline void set_On_Pinch_30(PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4 * value)
	{
		___On_Pinch_30 = value;
		Il2CppCodeGenWriteBarrier((&___On_Pinch_30), value);
	}

	inline static int32_t get_offset_of_On_PinchIn_31() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_PinchIn_31)); }
	inline PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29 * get_On_PinchIn_31() const { return ___On_PinchIn_31; }
	inline PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29 ** get_address_of_On_PinchIn_31() { return &___On_PinchIn_31; }
	inline void set_On_PinchIn_31(PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29 * value)
	{
		___On_PinchIn_31 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchIn_31), value);
	}

	inline static int32_t get_offset_of_On_PinchOut_32() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_PinchOut_32)); }
	inline PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B * get_On_PinchOut_32() const { return ___On_PinchOut_32; }
	inline PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B ** get_address_of_On_PinchOut_32() { return &___On_PinchOut_32; }
	inline void set_On_PinchOut_32(PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B * value)
	{
		___On_PinchOut_32 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchOut_32), value);
	}

	inline static int32_t get_offset_of_On_PinchEnd_33() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_PinchEnd_33)); }
	inline PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57 * get_On_PinchEnd_33() const { return ___On_PinchEnd_33; }
	inline PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57 ** get_address_of_On_PinchEnd_33() { return &___On_PinchEnd_33; }
	inline void set_On_PinchEnd_33(PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57 * value)
	{
		___On_PinchEnd_33 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchEnd_33), value);
	}

	inline static int32_t get_offset_of_On_DragStart2Fingers_34() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DragStart2Fingers_34)); }
	inline DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * get_On_DragStart2Fingers_34() const { return ___On_DragStart2Fingers_34; }
	inline DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B ** get_address_of_On_DragStart2Fingers_34() { return &___On_DragStart2Fingers_34; }
	inline void set_On_DragStart2Fingers_34(DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B * value)
	{
		___On_DragStart2Fingers_34 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragStart2Fingers_34), value);
	}

	inline static int32_t get_offset_of_On_Drag2Fingers_35() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Drag2Fingers_35)); }
	inline Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * get_On_Drag2Fingers_35() const { return ___On_Drag2Fingers_35; }
	inline Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA ** get_address_of_On_Drag2Fingers_35() { return &___On_Drag2Fingers_35; }
	inline void set_On_Drag2Fingers_35(Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA * value)
	{
		___On_Drag2Fingers_35 = value;
		Il2CppCodeGenWriteBarrier((&___On_Drag2Fingers_35), value);
	}

	inline static int32_t get_offset_of_On_DragEnd2Fingers_36() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_DragEnd2Fingers_36)); }
	inline DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * get_On_DragEnd2Fingers_36() const { return ___On_DragEnd2Fingers_36; }
	inline DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A ** get_address_of_On_DragEnd2Fingers_36() { return &___On_DragEnd2Fingers_36; }
	inline void set_On_DragEnd2Fingers_36(DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A * value)
	{
		___On_DragEnd2Fingers_36 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragEnd2Fingers_36), value);
	}

	inline static int32_t get_offset_of_On_SwipeStart2Fingers_37() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SwipeStart2Fingers_37)); }
	inline SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * get_On_SwipeStart2Fingers_37() const { return ___On_SwipeStart2Fingers_37; }
	inline SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 ** get_address_of_On_SwipeStart2Fingers_37() { return &___On_SwipeStart2Fingers_37; }
	inline void set_On_SwipeStart2Fingers_37(SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5 * value)
	{
		___On_SwipeStart2Fingers_37 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeStart2Fingers_37), value);
	}

	inline static int32_t get_offset_of_On_Swipe2Fingers_38() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_Swipe2Fingers_38)); }
	inline Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * get_On_Swipe2Fingers_38() const { return ___On_Swipe2Fingers_38; }
	inline Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 ** get_address_of_On_Swipe2Fingers_38() { return &___On_Swipe2Fingers_38; }
	inline void set_On_Swipe2Fingers_38(Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862 * value)
	{
		___On_Swipe2Fingers_38 = value;
		Il2CppCodeGenWriteBarrier((&___On_Swipe2Fingers_38), value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd2Fingers_39() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_SwipeEnd2Fingers_39)); }
	inline SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * get_On_SwipeEnd2Fingers_39() const { return ___On_SwipeEnd2Fingers_39; }
	inline SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC ** get_address_of_On_SwipeEnd2Fingers_39() { return &___On_SwipeEnd2Fingers_39; }
	inline void set_On_SwipeEnd2Fingers_39(SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC * value)
	{
		___On_SwipeEnd2Fingers_39 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeEnd2Fingers_39), value);
	}

	inline static int32_t get_offset_of_On_EasyTouchIsReady_40() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_EasyTouchIsReady_40)); }
	inline EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621 * get_On_EasyTouchIsReady_40() const { return ___On_EasyTouchIsReady_40; }
	inline EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621 ** get_address_of_On_EasyTouchIsReady_40() { return &___On_EasyTouchIsReady_40; }
	inline void set_On_EasyTouchIsReady_40(EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621 * value)
	{
		___On_EasyTouchIsReady_40 = value;
		Il2CppCodeGenWriteBarrier((&___On_EasyTouchIsReady_40), value);
	}

	inline static int32_t get_offset_of_On_OverUIElement_41() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_OverUIElement_41)); }
	inline OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F * get_On_OverUIElement_41() const { return ___On_OverUIElement_41; }
	inline OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F ** get_address_of_On_OverUIElement_41() { return &___On_OverUIElement_41; }
	inline void set_On_OverUIElement_41(OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F * value)
	{
		___On_OverUIElement_41 = value;
		Il2CppCodeGenWriteBarrier((&___On_OverUIElement_41), value);
	}

	inline static int32_t get_offset_of_On_UIElementTouchUp_42() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ___On_UIElementTouchUp_42)); }
	inline UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3 * get_On_UIElementTouchUp_42() const { return ___On_UIElementTouchUp_42; }
	inline UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3 ** get_address_of_On_UIElementTouchUp_42() { return &___On_UIElementTouchUp_42; }
	inline void set_On_UIElementTouchUp_42(UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3 * value)
	{
		___On_UIElementTouchUp_42 = value;
		Il2CppCodeGenWriteBarrier((&___On_UIElementTouchUp_42), value);
	}

	inline static int32_t get_offset_of__instance_43() { return static_cast<int32_t>(offsetof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields, ____instance_43)); }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * get__instance_43() const { return ____instance_43; }
	inline EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 ** get_address_of__instance_43() { return &____instance_43; }
	inline void set__instance_43(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080 * value)
	{
		____instance_43 = value;
		Il2CppCodeGenWriteBarrier((&____instance_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCH_T6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_H
#ifndef QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#define QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase
struct  QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HedgehogTeam.EasyTouch.QuickBase::quickActionName
	String_t* ___quickActionName_4;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isMultiTouch
	bool ___isMultiTouch_5;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::is2Finger
	bool ___is2Finger_6;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isOnTouch
	bool ___isOnTouch_7;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::enablePickOverUI
	bool ___enablePickOverUI_8;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::resetPhysic
	bool ___resetPhysic_9;
	// HedgehogTeam.EasyTouch.QuickBase_DirectAction HedgehogTeam.EasyTouch.QuickBase::directAction
	int32_t ___directAction_10;
	// HedgehogTeam.EasyTouch.QuickBase_AffectedAxesAction HedgehogTeam.EasyTouch.QuickBase::axesAction
	int32_t ___axesAction_11;
	// System.Single HedgehogTeam.EasyTouch.QuickBase::sensibility
	float ___sensibility_12;
	// UnityEngine.CharacterController HedgehogTeam.EasyTouch.QuickBase::directCharacterController
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___directCharacterController_13;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::inverseAxisValue
	bool ___inverseAxisValue_14;
	// UnityEngine.Rigidbody HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___cachedRigidBody_15;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic
	bool ___isKinematic_16;
	// UnityEngine.Rigidbody2D HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody2D
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___cachedRigidBody2D_17;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic2D
	bool ___isKinematic2D_18;
	// HedgehogTeam.EasyTouch.QuickBase_GameObjectType HedgehogTeam.EasyTouch.QuickBase::realType
	int32_t ___realType_19;
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase::fingerIndex
	int32_t ___fingerIndex_20;

public:
	inline static int32_t get_offset_of_quickActionName_4() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___quickActionName_4)); }
	inline String_t* get_quickActionName_4() const { return ___quickActionName_4; }
	inline String_t** get_address_of_quickActionName_4() { return &___quickActionName_4; }
	inline void set_quickActionName_4(String_t* value)
	{
		___quickActionName_4 = value;
		Il2CppCodeGenWriteBarrier((&___quickActionName_4), value);
	}

	inline static int32_t get_offset_of_isMultiTouch_5() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isMultiTouch_5)); }
	inline bool get_isMultiTouch_5() const { return ___isMultiTouch_5; }
	inline bool* get_address_of_isMultiTouch_5() { return &___isMultiTouch_5; }
	inline void set_isMultiTouch_5(bool value)
	{
		___isMultiTouch_5 = value;
	}

	inline static int32_t get_offset_of_is2Finger_6() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___is2Finger_6)); }
	inline bool get_is2Finger_6() const { return ___is2Finger_6; }
	inline bool* get_address_of_is2Finger_6() { return &___is2Finger_6; }
	inline void set_is2Finger_6(bool value)
	{
		___is2Finger_6 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_7() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isOnTouch_7)); }
	inline bool get_isOnTouch_7() const { return ___isOnTouch_7; }
	inline bool* get_address_of_isOnTouch_7() { return &___isOnTouch_7; }
	inline void set_isOnTouch_7(bool value)
	{
		___isOnTouch_7 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_8() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___enablePickOverUI_8)); }
	inline bool get_enablePickOverUI_8() const { return ___enablePickOverUI_8; }
	inline bool* get_address_of_enablePickOverUI_8() { return &___enablePickOverUI_8; }
	inline void set_enablePickOverUI_8(bool value)
	{
		___enablePickOverUI_8 = value;
	}

	inline static int32_t get_offset_of_resetPhysic_9() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___resetPhysic_9)); }
	inline bool get_resetPhysic_9() const { return ___resetPhysic_9; }
	inline bool* get_address_of_resetPhysic_9() { return &___resetPhysic_9; }
	inline void set_resetPhysic_9(bool value)
	{
		___resetPhysic_9 = value;
	}

	inline static int32_t get_offset_of_directAction_10() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___directAction_10)); }
	inline int32_t get_directAction_10() const { return ___directAction_10; }
	inline int32_t* get_address_of_directAction_10() { return &___directAction_10; }
	inline void set_directAction_10(int32_t value)
	{
		___directAction_10 = value;
	}

	inline static int32_t get_offset_of_axesAction_11() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___axesAction_11)); }
	inline int32_t get_axesAction_11() const { return ___axesAction_11; }
	inline int32_t* get_address_of_axesAction_11() { return &___axesAction_11; }
	inline void set_axesAction_11(int32_t value)
	{
		___axesAction_11 = value;
	}

	inline static int32_t get_offset_of_sensibility_12() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___sensibility_12)); }
	inline float get_sensibility_12() const { return ___sensibility_12; }
	inline float* get_address_of_sensibility_12() { return &___sensibility_12; }
	inline void set_sensibility_12(float value)
	{
		___sensibility_12 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_13() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___directCharacterController_13)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_directCharacterController_13() const { return ___directCharacterController_13; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_directCharacterController_13() { return &___directCharacterController_13; }
	inline void set_directCharacterController_13(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___directCharacterController_13 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_13), value);
	}

	inline static int32_t get_offset_of_inverseAxisValue_14() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___inverseAxisValue_14)); }
	inline bool get_inverseAxisValue_14() const { return ___inverseAxisValue_14; }
	inline bool* get_address_of_inverseAxisValue_14() { return &___inverseAxisValue_14; }
	inline void set_inverseAxisValue_14(bool value)
	{
		___inverseAxisValue_14 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody_15() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___cachedRigidBody_15)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_cachedRigidBody_15() const { return ___cachedRigidBody_15; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_cachedRigidBody_15() { return &___cachedRigidBody_15; }
	inline void set_cachedRigidBody_15(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___cachedRigidBody_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody_15), value);
	}

	inline static int32_t get_offset_of_isKinematic_16() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isKinematic_16)); }
	inline bool get_isKinematic_16() const { return ___isKinematic_16; }
	inline bool* get_address_of_isKinematic_16() { return &___isKinematic_16; }
	inline void set_isKinematic_16(bool value)
	{
		___isKinematic_16 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody2D_17() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___cachedRigidBody2D_17)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_cachedRigidBody2D_17() const { return ___cachedRigidBody2D_17; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_cachedRigidBody2D_17() { return &___cachedRigidBody2D_17; }
	inline void set_cachedRigidBody2D_17(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___cachedRigidBody2D_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody2D_17), value);
	}

	inline static int32_t get_offset_of_isKinematic2D_18() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___isKinematic2D_18)); }
	inline bool get_isKinematic2D_18() const { return ___isKinematic2D_18; }
	inline bool* get_address_of_isKinematic2D_18() { return &___isKinematic2D_18; }
	inline void set_isKinematic2D_18(bool value)
	{
		___isKinematic2D_18 = value;
	}

	inline static int32_t get_offset_of_realType_19() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___realType_19)); }
	inline int32_t get_realType_19() const { return ___realType_19; }
	inline int32_t* get_address_of_realType_19() { return &___realType_19; }
	inline void set_realType_19(int32_t value)
	{
		___realType_19 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_20() { return static_cast<int32_t>(offsetof(QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850, ___fingerIndex_20)); }
	inline int32_t get_fingerIndex_20() const { return ___fingerIndex_20; }
	inline int32_t* get_address_of_fingerIndex_20() { return &___fingerIndex_20; }
	inline void set_fingerIndex_20(int32_t value)
	{
		___fingerIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKBASE_T00BF409C6521F8079B3B3B04A00349E1CA600850_H
#ifndef EXAMPLEPROGRESSTARGETAUDIOMIXER_T80A29FB554C8D2571F8920AD576873DAF38517B9_H
#define EXAMPLEPROGRESSTARGETAUDIOMIXER_T80A29FB554C8D2571F8920AD576873DAF38517B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Examples.ExampleProgressTargetAudioMixer
struct  ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9  : public ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93
{
public:
	// UnityEngine.Audio.AudioMixer Doozy.Examples.ExampleProgressTargetAudioMixer::AudioMixer
	AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * ___AudioMixer_4;
	// System.String Doozy.Examples.ExampleProgressTargetAudioMixer::ExposedParameter
	String_t* ___ExposedParameter_5;
	// Doozy.Engine.Progress.TargetVariable Doozy.Examples.ExampleProgressTargetAudioMixer::TargetVariable
	int32_t ___TargetVariable_6;
	// System.Single Doozy.Examples.ExampleProgressTargetAudioMixer::m_targetValue
	float ___m_targetValue_7;

public:
	inline static int32_t get_offset_of_AudioMixer_4() { return static_cast<int32_t>(offsetof(ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9, ___AudioMixer_4)); }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * get_AudioMixer_4() const { return ___AudioMixer_4; }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F ** get_address_of_AudioMixer_4() { return &___AudioMixer_4; }
	inline void set_AudioMixer_4(AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * value)
	{
		___AudioMixer_4 = value;
		Il2CppCodeGenWriteBarrier((&___AudioMixer_4), value);
	}

	inline static int32_t get_offset_of_ExposedParameter_5() { return static_cast<int32_t>(offsetof(ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9, ___ExposedParameter_5)); }
	inline String_t* get_ExposedParameter_5() const { return ___ExposedParameter_5; }
	inline String_t** get_address_of_ExposedParameter_5() { return &___ExposedParameter_5; }
	inline void set_ExposedParameter_5(String_t* value)
	{
		___ExposedParameter_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExposedParameter_5), value);
	}

	inline static int32_t get_offset_of_TargetVariable_6() { return static_cast<int32_t>(offsetof(ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9, ___TargetVariable_6)); }
	inline int32_t get_TargetVariable_6() const { return ___TargetVariable_6; }
	inline int32_t* get_address_of_TargetVariable_6() { return &___TargetVariable_6; }
	inline void set_TargetVariable_6(int32_t value)
	{
		___TargetVariable_6 = value;
	}

	inline static int32_t get_offset_of_m_targetValue_7() { return static_cast<int32_t>(offsetof(ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9, ___m_targetValue_7)); }
	inline float get_m_targetValue_7() const { return ___m_targetValue_7; }
	inline float* get_address_of_m_targetValue_7() { return &___m_targetValue_7; }
	inline void set_m_targetValue_7(float value)
	{
		___m_targetValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPROGRESSTARGETAUDIOMIXER_T80A29FB554C8D2571F8920AD576873DAF38517B9_H
#ifndef QUICKTOUCH_TDA5EE39DB56965E758B033ADE1C8246F7DE70452_H
#define QUICKTOUCH_TDA5EE39DB56965E758B033ADE1C8246F7DE70452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch
struct  QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickTouch_OnTouch HedgehogTeam.EasyTouch.QuickTouch::onTouch
	OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064 * ___onTouch_21;
	// HedgehogTeam.EasyTouch.QuickTouch_OnTouchNotOverMe HedgehogTeam.EasyTouch.QuickTouch::onTouchNotOverMe
	OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B * ___onTouchNotOverMe_22;
	// HedgehogTeam.EasyTouch.QuickTouch_ActionTriggering HedgehogTeam.EasyTouch.QuickTouch::actionTriggering
	int32_t ___actionTriggering_23;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTouch::currentGesture
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * ___currentGesture_24;

public:
	inline static int32_t get_offset_of_onTouch_21() { return static_cast<int32_t>(offsetof(QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452, ___onTouch_21)); }
	inline OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064 * get_onTouch_21() const { return ___onTouch_21; }
	inline OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064 ** get_address_of_onTouch_21() { return &___onTouch_21; }
	inline void set_onTouch_21(OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064 * value)
	{
		___onTouch_21 = value;
		Il2CppCodeGenWriteBarrier((&___onTouch_21), value);
	}

	inline static int32_t get_offset_of_onTouchNotOverMe_22() { return static_cast<int32_t>(offsetof(QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452, ___onTouchNotOverMe_22)); }
	inline OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B * get_onTouchNotOverMe_22() const { return ___onTouchNotOverMe_22; }
	inline OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B ** get_address_of_onTouchNotOverMe_22() { return &___onTouchNotOverMe_22; }
	inline void set_onTouchNotOverMe_22(OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B * value)
	{
		___onTouchNotOverMe_22 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchNotOverMe_22), value);
	}

	inline static int32_t get_offset_of_actionTriggering_23() { return static_cast<int32_t>(offsetof(QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452, ___actionTriggering_23)); }
	inline int32_t get_actionTriggering_23() const { return ___actionTriggering_23; }
	inline int32_t* get_address_of_actionTriggering_23() { return &___actionTriggering_23; }
	inline void set_actionTriggering_23(int32_t value)
	{
		___actionTriggering_23 = value;
	}

	inline static int32_t get_offset_of_currentGesture_24() { return static_cast<int32_t>(offsetof(QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452, ___currentGesture_24)); }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * get_currentGesture_24() const { return ___currentGesture_24; }
	inline Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 ** get_address_of_currentGesture_24() { return &___currentGesture_24; }
	inline void set_currentGesture_24(Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95 * value)
	{
		___currentGesture_24 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTOUCH_TDA5EE39DB56965E758B033ADE1C8246F7DE70452_H
#ifndef QUICKTWIST_T29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F_H
#define QUICKTWIST_T29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist
struct  QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F  : public QuickBase_t00BF409C6521F8079B3B3B04A00349E1CA600850
{
public:
	// HedgehogTeam.EasyTouch.QuickTwist_OnTwistAction HedgehogTeam.EasyTouch.QuickTwist::onTwistAction
	OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203 * ___onTwistAction_21;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::isGestureOnMe
	bool ___isGestureOnMe_22;
	// HedgehogTeam.EasyTouch.QuickTwist_ActionTiggering HedgehogTeam.EasyTouch.QuickTwist::actionTriggering
	int32_t ___actionTriggering_23;
	// HedgehogTeam.EasyTouch.QuickTwist_ActionRotationDirection HedgehogTeam.EasyTouch.QuickTwist::rotationDirection
	int32_t ___rotationDirection_24;
	// System.Single HedgehogTeam.EasyTouch.QuickTwist::axisActionValue
	float ___axisActionValue_25;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::enableSimpleAction
	bool ___enableSimpleAction_26;

public:
	inline static int32_t get_offset_of_onTwistAction_21() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___onTwistAction_21)); }
	inline OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203 * get_onTwistAction_21() const { return ___onTwistAction_21; }
	inline OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203 ** get_address_of_onTwistAction_21() { return &___onTwistAction_21; }
	inline void set_onTwistAction_21(OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203 * value)
	{
		___onTwistAction_21 = value;
		Il2CppCodeGenWriteBarrier((&___onTwistAction_21), value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_22() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___isGestureOnMe_22)); }
	inline bool get_isGestureOnMe_22() const { return ___isGestureOnMe_22; }
	inline bool* get_address_of_isGestureOnMe_22() { return &___isGestureOnMe_22; }
	inline void set_isGestureOnMe_22(bool value)
	{
		___isGestureOnMe_22 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_23() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___actionTriggering_23)); }
	inline int32_t get_actionTriggering_23() const { return ___actionTriggering_23; }
	inline int32_t* get_address_of_actionTriggering_23() { return &___actionTriggering_23; }
	inline void set_actionTriggering_23(int32_t value)
	{
		___actionTriggering_23 = value;
	}

	inline static int32_t get_offset_of_rotationDirection_24() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___rotationDirection_24)); }
	inline int32_t get_rotationDirection_24() const { return ___rotationDirection_24; }
	inline int32_t* get_address_of_rotationDirection_24() { return &___rotationDirection_24; }
	inline void set_rotationDirection_24(int32_t value)
	{
		___rotationDirection_24 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_25() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___axisActionValue_25)); }
	inline float get_axisActionValue_25() const { return ___axisActionValue_25; }
	inline float* get_address_of_axisActionValue_25() { return &___axisActionValue_25; }
	inline void set_axisActionValue_25(float value)
	{
		___axisActionValue_25 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_26() { return static_cast<int32_t>(offsetof(QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F, ___enableSimpleAction_26)); }
	inline bool get_enableSimpleAction_26() const { return ___enableSimpleAction_26; }
	inline bool* get_address_of_enableSimpleAction_26() { return &___enableSimpleAction_26; }
	inline void set_enableSimpleAction_26(bool value)
	{
		___enableSimpleAction_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTWIST_T29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4000[4] = 
{
	QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452::get_offset_of_onTouch_21(),
	QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452::get_offset_of_onTouchNotOverMe_22(),
	QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452::get_offset_of_actionTriggering_23(),
	QuickTouch_tDA5EE39DB56965E758B033ADE1C8246F7DE70452::get_offset_of_currentGesture_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (OnTouch_t82B81EB29D25795F77E81BAD75857C97A1712064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (OnTouchNotOverMe_t4ECA3D0BCC536612665000E70BDEB32F4D978C5B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (ActionTriggering_t4EBEF0C41F664AD4F2486D1D94FB08B68756BECC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4003[4] = 
{
	ActionTriggering_t4EBEF0C41F664AD4F2486D1D94FB08B68756BECC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4004[6] = 
{
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_onTwistAction_21(),
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_isGestureOnMe_22(),
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_actionTriggering_23(),
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_rotationDirection_24(),
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_axisActionValue_25(),
	QuickTwist_t29FB13D29B2B3D85EF51E1B29B8CB45F8FB2B61F::get_offset_of_enableSimpleAction_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (OnTwistAction_tBA2645893393633838C9D725F00FE87DB85F2203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (ActionTiggering_t25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4006[3] = 
{
	ActionTiggering_t25C6ABF424B3F33C6EE3F853F355E55B00BF6B0D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (ActionRotationDirection_tA352B79DFBD10C88D34C35B51D576CB8FABC76D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4007[4] = 
{
	ActionRotationDirection_tA352B79DFBD10C88D34C35B51D576CB8FABC76D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[19] = 
{
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_fingerIndex_0(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_touchCount_1(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_startPosition_2(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_position_3(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_deltaPosition_4(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_actionTime_5(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_deltaTime_6(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_pickedCamera_7(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_pickedObject_8(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_isGuiCamera_9(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_isOverGui_10(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_pickedUIElement_11(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_altitudeAngle_12(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_azimuthAngle_13(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_maximumPossiblePressure_14(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_pressure_15(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_radius_16(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_radiusVariance_17(),
	BaseFinger_t9768A6FE4C44544089206888AB7529092AB2FD10::get_offset_of_touchType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080), -1, sizeof(EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4009[89] = 
{
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Cancel_4(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Cancel2Fingers_5(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchStart_6(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchDown_7(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchUp_8(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SimpleTap_9(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DoubleTap_10(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTapStart_11(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTap_12(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTapEnd_13(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DragStart_14(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Drag_15(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DragEnd_16(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SwipeStart_17(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Swipe_18(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SwipeEnd_19(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchStart2Fingers_20(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchDown2Fingers_21(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TouchUp2Fingers_22(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SimpleTap2Fingers_23(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DoubleTap2Fingers_24(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTapStart2Fingers_25(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTap2Fingers_26(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_LongTapEnd2Fingers_27(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Twist_28(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_TwistEnd_29(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Pinch_30(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_PinchIn_31(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_PinchOut_32(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_PinchEnd_33(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DragStart2Fingers_34(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Drag2Fingers_35(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_DragEnd2Fingers_36(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SwipeStart2Fingers_37(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_Swipe2Fingers_38(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_SwipeEnd2Fingers_39(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_EasyTouchIsReady_40(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_OverUIElement_41(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of_On_UIElementTouchUp_42(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080_StaticFields::get_offset_of__instance_43(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of__currentGesture_44(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of__currentGestures_45(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enable_46(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enableRemote_47(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_gesturePriority_48(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_StationaryTolerance_49(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_longTapTime_50(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_swipeTolerance_51(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_minPinchLength_52(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_minTwistAngle_53(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_doubleTapDetection_54(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_doubleTapTime_55(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_alwaysSendSwipe_56(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enable2FingersGesture_57(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enableTwist_58(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enablePinch_59(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enable2FingersSwipe_60(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_twoFingerPickMethod_61(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_touchCameras_62(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_autoSelect_63(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_pickableLayers3D_64(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enable2D_65(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_pickableLayers2D_66(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_autoUpdatePickedObject_67(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_allowUIDetection_68(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enableUIMode_69(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_autoUpdatePickedUI_70(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enabledNGuiMode_71(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_nGUILayers_72(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_nGUICameras_73(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_enableSimulation_74(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_twistKey_75(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_swipeKey_76(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_showGuiInspector_77(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_showSelectInspector_78(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_showGestureInspector_79(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_showTwoFingerInspector_80(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_showSecondFingerInspector_81(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_input_82(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_fingers_83(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_secondFingerTexture_84(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_twoFinger_85(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_oldTouchCount_86(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_singleDoubleTap_87(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_tmpArray_88(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_pickedObject_89(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_uiRaycastResultCache_90(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_uiPointerEventData_91(),
	EasyTouch_t6E834AF87D9BD6B03756DEEBF0EA71CBF7FB1080::get_offset_of_uiEventSystem_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[5] = 
{
	DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD::get_offset_of_inDoubleTap_0(),
	DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD::get_offset_of_inWait_1(),
	DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD::get_offset_of_time_2(),
	DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD::get_offset_of_count_3(),
	DoubleTap_t770BAA916711CCD2CFCCBA7305212234BD0CA3CD::get_offset_of_finger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4011[3] = 
{
	PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05::get_offset_of_pickedObj_0(),
	PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05::get_offset_of_pickedCamera_1(),
	PickedObject_t1252EC4DBC7B31CB1265013835D4CBBEE7700B05::get_offset_of_isGUI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (TouchCancelHandler_t6F03769BCF5479D8D674AF13F319913EFF995398), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (Cancel2FingersHandler_t7CB2A8C009515F4B09D7B8F7F16489F486E28FBD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (TouchStartHandler_t7C4A2E33DF016E9802FE7059781725CAD91E6853), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (TouchDownHandler_t25633332BED9DC567480AE796788433ADA5A4955), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (TouchUpHandler_tFC3606D15F5A86B81730262C45C99738033DD46E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (SimpleTapHandler_t19011340636873AF4B306D0E18C3B4D27AD168D3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (DoubleTapHandler_t3FDE1E5C632D85BB111A67CFF698B3519DC7AD11), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (LongTapStartHandler_t1BF61F7E0A4FD8FA13892A857539164EB9D459E1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (LongTapHandler_tB8340A0546B488510FF8A64074446971AD2B8F30), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (LongTapEndHandler_t437BD68FF1BA1CEF31FCA34E4F57D062840503EB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (DragStartHandler_t59144B31D5B9DECED9817E48885D6F47ABC1C940), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (DragHandler_tFE30182A454D9F6BF14FB28EE9D72902AEE7A801), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (DragEndHandler_t2A5E65F8FE7760D89A3E3876D9FD1A305A4F6FFF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (SwipeStartHandler_tE23E70331EFDAEF1749F136E22F2A2D4DB19DDCB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (SwipeHandler_t27BB40235B941FBCA58EFD2A53012354ECC00CB0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (SwipeEndHandler_t51D6BA995C5C6DDECEFA7CB25AB2370503CCA00F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (TouchStart2FingersHandler_tA11253D3A78D2C563463B595B5705A6014E786BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (TouchDown2FingersHandler_t251A5FA76A3F9423662FE35E40F058F327AF6454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (TouchUp2FingersHandler_tCED587BF5F82C26E21B89F248E968C122C0AE1C0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (SimpleTap2FingersHandler_t69B9A77A3C573460C22EB6AC66BE58CFED414B30), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (DoubleTap2FingersHandler_t17FE5590609D32A98F6E325E57959AD2630989A8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (LongTapStart2FingersHandler_t87451224DEA4225531CE32BB25025362C9FF8779), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (LongTap2FingersHandler_t7CC530A00C7AB029CB472EFAF3D3E51A0B32A4B6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (LongTapEnd2FingersHandler_tB15E971E1C3AD601EC5CBA90E12B0CCAA91C98E8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (TwistHandler_t8669F951A600E6F2AD113BD25B754C4C10EC5C1D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (TwistEndHandler_t96B95E104506BDB3EE3E0762FE554E3BF302B929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (PinchInHandler_tEEC531E31D0693EF3B8B5C003F29EAA49643FB29), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (PinchOutHandler_tE83ACE23BADAC7A2674EDCAF2110B6E7F214321B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (PinchEndHandler_t328BC2ADF08E4EAC8CA7A7CC89D758E7820BAB57), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (PinchHandler_tF6C154F2E6B285725DCBCA31093A671FB63D82C4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (DragStart2FingersHandler_t2619E363975FAC51655EE9C799F91228F759992B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (Drag2FingersHandler_tD0EA90C5A55FB1447BDDC17A218E381914486AAA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (DragEnd2FingersHandler_tAECC166E35C4D391CE8638C9F7A097C7095DF83A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (SwipeStart2FingersHandler_tC1C95E21681B8F61FE6D26963E144E82E3C9F0A5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (Swipe2FingersHandler_tD078F362AFFD04689777BD0ED938378B5043C862), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (SwipeEnd2FingersHandler_t28B15420F3C5A52BFB546A14A6E2B3DF505BA3CC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (EasyTouchIsReadyHandler_t9BA04EFD67A48EDD1484C54C36D063A9AAF47621), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (OverUIElementHandler_t90BBA9590BC6FC4763CB4904C0FDC7A1789E841F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (UIElementTouchUpHandler_tF7CB7C9434A913316961E5665EB14357CC007FD3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (GesturePriority_t510B565B1C472F1433ECC01CB698AF28E26E7595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4051[3] = 
{
	GesturePriority_t510B565B1C472F1433ECC01CB698AF28E26E7595::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (DoubleTapDetection_tCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4052[3] = 
{
	DoubleTapDetection_tCCE39BEA2928FE044B7258DD2AFD015D6F0C9CD1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (GestureType_t363EDA7F208202EE99EF740869A7DC7223E1D811)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4053[10] = 
{
	GestureType_t363EDA7F208202EE99EF740869A7DC7223E1D811::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4054[12] = 
{
	SwipeDirection_tEC87368746528A6E300E8C86A39C67F74E4004FE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (TwoFingerPickMethod_t2C80556D7952522FA119B278E0C0D330039C772B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4055[3] = 
{
	TwoFingerPickMethod_t2C80556D7952522FA119B278E0C0D330039C772B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4056[41] = 
{
	EvtType_tF2599608DD71DDF1D3511085CE11F92749BBEDEB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D), -1, sizeof(U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4057[2] = 
{
	U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7AE38AA56DCFDACF6F1D7A894E3D3A5FD0337D7D_StaticFields::get_offset_of_U3CU3E9__221_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4058[4] = 
{
	U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625::get_offset_of_U3CU3E1__state_0(),
	U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625::get_offset_of_U3CU3E2__current_1(),
	U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625::get_offset_of_U3CU3E4__this_2(),
	U3CSingleOrDoubleU3Ed__229_tA04A31FF7C737CB4E23EBEA145EB660FAE758625::get_offset_of_fingerIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[3] = 
{
	U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355::get_offset_of_U3CU3E1__state_0(),
	U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355::get_offset_of_U3CU3E2__current_1(),
	U3CSingleOrDouble2FingersU3Ed__235_tAA54E2FBB78113A1929421716CD9EF7735873355::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (U3CU3Ec__DisplayClass240_0_t046EBBE2BE2D9E387606842666C648D1537299D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[1] = 
{
	U3CU3Ec__DisplayClass240_0_t046EBBE2BE2D9E387606842666C648D1537299D4::get_offset_of_gesture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (U3CU3Ec__DisplayClass273_0_t1D044BA6A7BACD05300F7506B518CD042C180845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4061[1] = 
{
	U3CU3Ec__DisplayClass273_0_t1D044BA6A7BACD05300F7506B518CD042C180845::get_offset_of_cam_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4062[9] = 
{
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_oldMousePosition_0(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_tapCount_1(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_startActionTime_2(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_deltaTime_3(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_tapeTime_4(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_bComplex_5(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_deltaFingerPosition_6(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_oldFinger2Position_7(),
	EasyTouchInput_t128D6FA07BC618F0BA809DA3D8C0C733E8400708::get_offset_of_complexCenter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[2] = 
{
	ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9::get_offset_of_camera_0(),
	ECamera_t51665FF089A47BC95DFFA0B5DB7AB9AC4BC031E9::get_offset_of_guiCamera_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (Finger_t1C55110621038C162D210785A1F1490A0273B205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[6] = 
{
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_startTimeAction_19(),
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_oldPosition_20(),
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_tapCount_21(),
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_phase_22(),
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_gesture_23(),
	Finger_t1C55110621038C162D210785A1F1490A0273B205::get_offset_of_oldSwipeType_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[7] = 
{
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_swipe_19(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_swipeLength_20(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_swipeVector_21(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_deltaPinch_22(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_twistAngle_23(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_twoFingerDistance_24(),
	Gesture_tBB08976F00DA24D38D7CE640CCD907A521BA3E95::get_offset_of_type_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4066[27] = 
{
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_currentGesture_0(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_oldGesture_1(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_finger0_2(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_finger1_3(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_startTimeAction_4(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_timeSinceStartAction_5(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_startPosition_6(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_position_7(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_deltaPosition_8(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_oldStartPosition_9(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_startDistance_10(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_fingerDistance_11(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_oldFingerDistance_12(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_lockPinch_13(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_lockTwist_14(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_lastPinch_15(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_lastTwistAngle_16(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_pickedObject_17(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_oldPickedObject_18(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_pickedCamera_19(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_isGuiCamera_20(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_isOverGui_21(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_pickedUIElement_22(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_dragStart_23(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_swipeStart_24(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_inSingleDoubleTaps_25(),
	TwoFingerGesture_tE01A70108992AD19FFB0D7F47A418873C9269A0B::get_offset_of_tapCurentTime_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (PlaymakerEventDispatcher_tDBF88F4151445147881C798BB0FECB7722B3BA3F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[4] = 
{
	ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9::get_offset_of_AudioMixer_4(),
	ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9::get_offset_of_ExposedParameter_5(),
	ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9::get_offset_of_TargetVariable_6(),
	ExampleProgressTargetAudioMixer_t80A29FB554C8D2571F8920AD576873DAF38517B9::get_offset_of_m_targetValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (ExampleShowPopup_t62083886546870E3F44F9957F38A94595A28CF4C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4070[3] = 
{
	UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3::get_offset_of_PresetCategory_4(),
	UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3::get_offset_of_PresetName_5(),
	UIButtonAutomatedTextValue_t1B2551623340A131E2C9733454178C180EE95CD3::get_offset_of_BehaviorType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4071[3] = 
{
	Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (Module_t9ABC50C1B8826DC14B89EDC57A836097536536CE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4072[6] = 
{
	Module_t9ABC50C1B8826DC14B89EDC57A836097536536CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (SetupMode_tADC952D5BB14901A477FDE98692C068C3B931560)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4073[7] = 
{
	SetupMode_tADC952D5BB14901A477FDE98692C068C3B931560::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (SystemGameEvent_tF2E56884D1E47C416C36DC3B16CDA2CCC2B90053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4074[4] = 
{
	SystemGameEvent_tF2E56884D1E47C416C36DC3B16CDA2CCC2B90053::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095), -1, sizeof(Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4075[1] = 
{
	Coroutiner_t3D935290A596856B403CB67CC2386BF565BFC095_StaticFields::get_offset_of_s_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4076[4] = 
{
	GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D::get_offset_of_DebugMode_4(),
	GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D::get_offset_of_Event_5(),
	GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D::get_offset_of_GameEvent_6(),
	GameEventListener_tFE0DBD55DAFB2378BBBAC17ACED407DD7637222D::get_offset_of_ListenForAllGameEvents_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6), -1, sizeof(GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4077[2] = 
{
	GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields::get_offset_of_s_instance_4(),
	GameEventManager_t0C6EF63E04B3C0BD192773CB5C3A66CF0CD115A6_StaticFields::get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4078[4] = 
{
	0,
	GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE::get_offset_of_U3CIsSystemEventU3Ek__BackingField_4(),
	GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE::get_offset_of_EventName_5(),
	GameEventMessage_tB361B1D7C6FDF3FFD5AEE0A182E2A549B9A9B2AE::get_offset_of_Source_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4079[15] = 
{
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_Name_0(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M50_1(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M100_2(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M200_3(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M300_4(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M400_5(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M500_6(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M600_7(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M700_8(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M800_9(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_M900_10(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_A100_11(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_A200_12(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_A400_13(),
	MColor_t478B8420C0565BF2D2F37B6ADEFCBE97F8E22AB0::get_offset_of_A700_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E), -1, sizeof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4080[3] = 
{
	0,
	Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields::get_offset_of_Handlers_1(),
	Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields::get_offset_of_OnMessageHandle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (U3CU3Ec__DisplayClass15_0_t16C75EC0F7B6B6764DC1D62AFAF622266A77CE88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[1] = 
{
	U3CU3Ec__DisplayClass15_0_t16C75EC0F7B6B6764DC1D62AFAF622266A77CE88::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (MessageExtensions_t51C1F2A0256E90FDE441416BE35590DCF6D44375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109)+ sizeof (RuntimeObject), sizeof(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4084[2] = 
{
	RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109::get_offset_of_MinValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109::get_offset_of_MaxValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (AssetUtils_t61FDC3B29E3D755E2B26673EF6859DB0E79668B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (ColorUtils_t98FDBBDA3C9612D6E9C2E5AA710BD6353F87204A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (Conversions_t7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4087[19] = 
{
	Conversions_t7DC3DFC33BF8CC07831B24B0980F70A82ABECBDA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[5] = 
{
	0,
	DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37::get_offset_of_ColorName_1(),
	DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37::get_offset_of_Light_2(),
	DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37::get_offset_of_Normal_3(),
	DColor_t873FDF9DF709B3B89C77EB9BBE27E6BC890DDA37::get_offset_of_Dark_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (DoozyExecutionOrder_t365361C4936386D0C5DE804E9ED8AC4620C4BD35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7), -1, sizeof(DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4090[87] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_DOOZY_PATH_41(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_PATH_42(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_PATH_43(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_FONTS_PATH_44(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_GUI_PATH_45(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_IMAGES_PATH_46(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_INTERNAL_PATH_47(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_SETTINGS_PATH_48(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_SKINS_PATH_49(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_NODY_PATH_50(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_NODY_IMAGES_PATH_51(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_NODY_SKINS_PATH_52(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_NODY_SETTINGS_PATH_53(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_EDITOR_NODY_UTILS_PATH_54(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_NODY_PATH_55(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_NODY_RESOURCES_PATH_56(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_SOUNDY_PATH_57(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_SOUNDY_RESOURCES_PATH_58(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_TOUCHY_PATH_59(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_TOUCHY_RESOURCES_PATH_60(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_PATH_61(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_PATH_62(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_SOUNDY_PATH_63(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_UIBUTTON_PATH_64(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_UICANVAS_PATH_65(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_UIDRAWER_PATH_66(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_UIPOPUP_PATH_67(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_RESOURCES_DATA_UIVIEW_PATH_68(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_UI_PATH_69(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_UI_RESOURCES_PATH_70(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIANIMATIONS_RESOURCES_PATH_71(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_HIDE_UIANIMATIONS_RESOURCES_PATH_72(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_LOOP_UIANIMATIONS_RESOURCES_PATH_73(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_PUNCH_UIANIMATIONS_RESOURCES_PATH_74(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_SHOW_UIANIMATIONS_RESOURCES_PATH_75(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_STATE_UIANIMATIONS_RESOURCES_PATH_76(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIBUTTON_PATH_77(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIBUTTON_RESOURCES_PATH_78(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UICANVAS_PATH_79(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UICANVAS_RESOURCES_PATH_80(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIDRAWER_PATH_81(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIDRAWER_RESOURCES_PATH_82(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIPOPUP_PATH_83(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIPOPUP_RESOURCES_PATH_84(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIVIEW_PATH_85(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UIVIEW_RESOURCES_PATH_86(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UITOGGLE_PATH_87(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_UITOGGLE_RESOURCES_PATH_88(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_ENGINE_UTILS_PATH_89(),
	DoozyPath_t20CB84820AE4FD6B12869EE65FCC35AE255A6DB7_StaticFields::get_offset_of_s_basePath_90(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (ComponentName_tE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4091[7] = 
{
	ComponentName_tE8A90AC2D0F10F8D34186EC49F9FA1419DE2BF2D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11), -1, sizeof(DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4092[7] = 
{
	0,
	0,
	DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields::get_offset_of_BackgroundColor_2(),
	DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields::get_offset_of_CheckmarkColor_3(),
	DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields::get_offset_of_OverlayColor_4(),
	DoozyUtils_t72A3A0CFC27CCA482C818B1CA67CFBECB6548D11_StaticFields::get_offset_of_TextColor_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (FileUtils_t96473A9EF48218F647190AD1F034A7DA49E8F2F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4093[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5), -1, sizeof(U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4094[6] = 
{
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9__9_0_1(),
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9__9_1_2(),
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9__9_2_3(),
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9__9_3_4(),
	U3CU3Ec_t62B536DE70510AFFABFD5F8FB3A71ABEA0968CA5_StaticFields::get_offset_of_U3CU3E9__17_0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30), -1, sizeof(LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4095[3] = 
{
	0,
	0,
	LanguagePack_t279C78E79D8922AB9E588FDD820BA0E125242C30_StaticFields::get_offset_of_s_currentLanguage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (MenuUtils_tBDD43269757F837F55C408B4C40313E5CD77F66C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4096[315] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931), -1, sizeof(ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4097[3] = 
{
	0,
	0,
	ScriptUtils_tA3F985D6ECC70B592FDA88C95951828CED2D2931_StaticFields::get_offset_of_debug_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29), -1, sizeof(UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4098[632] = 
{
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields::get_offset_of_s_loadedLanguage_7(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29_StaticFields::get_offset_of_s_instance_8(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetLanguage_9(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Action_10(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Actions_11(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ActivateLoadedScenesNodeName_12(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ActiveNode_13(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ActiveSceneChange_14(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddCategory_15(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddItem_16(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddSounds_17(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddSource_18(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddSymbolToAllBuildTargetGroups_19(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AddToPopupQueue_20(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_After_21(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AllSoundsBeforePlaying_22(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AllowMultipleClicks_23(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AllowSceneActivation_24(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AlternateInput_25(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AlternateKeyCode_26(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AlternateVirtualButton_27(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnimateAllUIViewsWithSameCategoryAndName_28(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnimateValue_29(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Animation_30(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnimationCurve_31(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnimationType_32(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Animator_33(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnimatorEvents_34(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Animators_35(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnotherEntryExists_36(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyGameEvent_37(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyGameEventWillTriggerThisListener_38(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyScene_39(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyUIButtonWillTriggerThisListener_40(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyUIDrawerWillTriggerThisListener_41(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AnyUIViewWillTriggerThisListener_42(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ApplicationQuit_43(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureConvertToGraph_44(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureConvertToSubGraph_45(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureYouWantToDeleteDatabase_46(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureYouWantToDeletePopupReference_47(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureYouWantToRemoveCategory_48(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureYouWantToRemoveTheEntry_49(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AreYouSureYouWantToResetDatabase_50(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Arrow_51(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ArrowComponents_52(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AtFinish_53(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AtStart_54(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AudioClip_55(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoDisableUIInteractionsDescription_56(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoHideAfterShow_57(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoKillIdleControllers_58(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoKillIdleControllersDescription_59(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoResetSequence_60(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoSelectButtonAfterShow_61(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoSort_62(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_AutoStartLoopAnimation_63(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_BackButton_64(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_BackButtonNodeName_65(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Behavior_66(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_BehaviorAtStart_67(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Behaviors_68(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_BuildIndex_69(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ButtonCategory_70(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ButtonLabel_71(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ButtonName_72(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Buttons_73(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Cancel_74(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CannotAddEmptyEntry_75(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Canvas_76(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CanvasName_77(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Category_78(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CategoryIsEmpty_79(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Center_80(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CenterSelectedNodes_81(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Chance_82(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CheckingForIssues_83(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Clear_84(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClearRecentOpenedGraphsList_85(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClearRecentOpenedGraphsListDescription_86(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClearSearch_87(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClickAnywhere_88(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClickContainer_89(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClickMode_90(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClickOverlay_91(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Close_92(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CloseDirection_93(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CloseDrawer_94(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CloseSpeed_95(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Closed_96(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ClosedPosition_97(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Compiling_98(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ComponentDisabled_99(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Connected_100(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ConnectionPoint_101(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Connections_102(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Container_103(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ContainerSize_104(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Continue_105(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ControllerIdleKillDuration_106(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ControllerIdleKillDurationDescription_107(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ControllerName_108(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ConvertToGraph_109(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ConvertToSubGraph_110(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Copy_111(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Create_112(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateAnimation_113(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateGraph_114(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateNewCategory_115(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateNewGraph_116(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateNewGraphAsSubGraph_117(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateNode_118(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateParentAndCenterPivot_119(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CreateSubGraph_120(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CurrentTimeScale_121(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Custom_122(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomName_123(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomPosition_124(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomResetValue_125(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomSortingLayer_126(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomSortingOrder_127(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_CustomStartPosition_128(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Cut_129(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Database_130(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseAlreadyExists_131(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseHasBeenReset_132(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseIsEmpty_133(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseName_134(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseRefreshed_135(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DatabaseSorted_136(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Debug_137(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DebugMode_138(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DefaultDotAnimationSpeedDescription_139(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DefaultValues_140(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DefaultValuesDescription_141(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DefaultZoom_142(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DefaultZoomDescription_143(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Delete_144(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeleteDatabase_145(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeletePopupReference_146(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeletePreset_147(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeletedPopupReference_148(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Description_149(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Deselect_150(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeselectAnyButton_151(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeselectButton_152(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DeselectButtonAfterClick_153(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DestroyAfterHide_154(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DetectGestures_155(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Direction_156(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Disable_157(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisableButtonInterval_158(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisableCanvas_159(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisableFunctionality_160(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisableGameObject_161(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisableInterval_162(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisablePlugin_163(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Disabled_164(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Disconnect_165(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DispatchButtonClicks_166(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DispatchGameEvents_167(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DisplayTarget_168(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DontDestroyGameObjectOnLoad_169(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DotAnimationSpeed_170(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Down_171(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DragDrawer_172(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DrawerName_173(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DropAudioClipsHere_174(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Duration_175(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Ease_176(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EaseType_177(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Effect_178(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Elasticity_179(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Email_180(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Empty_181(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Enable_182(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnableFunctionality_183(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnablePlugin_184(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnableSupportForMasterAudio_185(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Enabled_186(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnterCategoryName_187(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnterDatabaseName_188(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnterGameEventToListenFor_189(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EnterNodeName_190(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Error_191(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Event_192(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EveryLongTapWillTriggerThisListener_193(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EverySwipeWillTriggerThisListener_194(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_EveryTapWillTriggerThisListener_195(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ExitNodeName_196(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Fade_197(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FadeBy_198(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FadeFrom_199(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FadeOutContainer_200(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FadeTo_201(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FixedSize_202(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FsmName_203(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Functionality_204(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_FunctionalityDescription_205(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GameEvent_206(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GameEvents_207(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GameObject_208(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GeneralSettings_209(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GestureType_210(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GetInTouch_211(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GetPosition_212(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GetSceneBy_213(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GlobalListener_214(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GoToEnterNode_215(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GoToExitNode_216(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GoToStartNode_217(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Graph_218(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GraphHasNoNodes_219(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GraphId_220(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GraphModel_221(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_GraphicRaycaster_222(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HasBeenAddedToClipboard_223(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Height_224(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Help_225(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HelpResources_226(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Hide_227(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HideAnimationWillNotWork_228(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HidePopup_229(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HideProgressor_230(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HideUIPopupBy_231(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HideView_232(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HideViews_233(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HowToUse_234(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IdleCheckInterval_235(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IdleCheckIntervalDescription_236(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IgnoreListenerPause_237(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IgnoreTimescale_238(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Image_239(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Images_240(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IncludeAudioClipNamesInSearch_241(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Info_242(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InputConnected_243(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InputConnections_244(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InputMode_245(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InputNotConnected_246(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Installed_247(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InstantAction_248(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_InstantAnimation_249(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Integrations_250(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_IsNotPrefab_251(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Key_252(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_KeyCode_253(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Label_254(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Labels_255(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Language_256(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LastModified_257(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Left_258(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListIsEmpty_259(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListenForAllGameEvents_260(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListenForAllUIButtons_261(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListenForAllUIDrawers_262(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListenForAllUIViews_263(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ListeningForGameEvent_264(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Load_265(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadBehavior_266(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadPreset_267(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadSceneBy_268(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadSceneMode_269(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadSceneNodeName_270(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadSelectedPresetAtRuntime_271(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoadedGraph_272(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LongTapDuration_273(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LongTapDurationDescription_274(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoopSound_275(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoopType_276(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_LoopView_277(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Manual_278(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Max_279(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MaxValue_280(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Min_281(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MinValue_282(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MinimumNumberOfControllers_283(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MinimumNumberOfControllersDescription_284(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MinimumSize_285(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingPrefabReference_286(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Move_287(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MoveBy_288(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MoveDown_289(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MoveFrom_290(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MoveTo_291(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MoveUp_292(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Multiplier_293(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MuteAllSounds_294(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Name_295(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_New_296(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewCategory_297(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewCategoryNameCannotBeEmpty_298(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewDatabase_299(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewPopup_300(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewPreset_301(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewPresetNameCannotBeEmpty_302(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NewSoundDatabase_303(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_News_304(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_No_305(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoAnimationEnabled_306(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoAnimatorFound_307(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoGraphReferenced_308(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSound_309(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSoundsHaveBeenAdded_310(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Node_311(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodeId_312(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodeName_313(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodeNameTooltip_314(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodeState_315(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodeWidth_316(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Nodes_317(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NodySettings_318(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NormalLoopAnimation_319(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotConnected_320(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotInstalled_321(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoteworthyInformation_322(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NumberOfLoops_323(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Ok_324(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnAnimationFinished_325(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnAnimationStart_326(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnClick_327(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnDeselected_328(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnDoubleClick_329(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnEnter_330(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnEnterNode_331(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnExit_332(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnExitNode_333(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnFixedUpdate_334(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnLateUpdate_335(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnLoadScene_336(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnLongClick_337(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnNodeFixedUpdate_338(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnNodeLateUpdate_339(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnNodeUpdate_340(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnPointerDown_341(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnPointerEnter_342(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnPointerExit_343(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnPointerUp_344(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnSceneLoaded_345(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnSelected_346(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnTrigger_347(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OnUpdate_348(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Open_349(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenControlPanel_350(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenDatabase_351(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenDrawer_352(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenGraph_353(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenNody_354(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenSpeed_355(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenSubGraph_356(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Opened_357(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OpenedPosition_358(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OperationCannotBeUndone_359(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OrientationDetectorDescription_360(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OtherReferences_361(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OutputAudioMixerGroup_362(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OutputConnected_363(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OutputConnections_364(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OutputMixerGroup_365(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OutputNotConnected_366(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Overlay_367(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Override_368(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OverrideColor_369(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Overview_370(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_OverviewZoom_371(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ParameterName_372(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ParameterType_373(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ParticleSystem_374(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Paste_375(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PauseAllSounds_376(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PercentageOfScreenZeroToOne_377(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PitchSemitones_378(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Play_379(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PlayAnimationInZeroSeconds_380(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PlayMode_381(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PlaySound_382(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PleaseEnterNewName_383(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PopupName_384(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PopupPrefab_385(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Portal_386(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PortalNodeName_387(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Prefix_388(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PresetCategory_389(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PresetName_390(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PreviewAnimation_391(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Progress_392(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ProgressTargets_393(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Progressor_394(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Progressors_395(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_PunchBy_396(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RandomDuration_397(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RandomNodeName_398(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Recent_399(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Refresh_400(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RefreshDatabase_401(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RegisterInterval_402(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Remove_403(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveCategory_404(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveDuplicates_405(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveEmptyCategories_406(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveEmptyEntries_407(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveItem_408(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveNullEntries_409(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemoveSymbolFromAllBuildTargetGroups_410(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemovedDuplicateEntries_411(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemovedEntriesWithNoName_412(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemovedEntriesWithNullPrefabs_413(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemovedEntry_414(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RemovedNullEntries_415(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Rename_416(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameCategory_417(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameCategoryDialogMessage_418(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameGameObjectTo_419(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameNodeTo_420(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenamePrefix_421(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameSoundDatabase_422(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameSoundDatabaseDialogMessage_423(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameSuffix_424(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RenameTo_425(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Reset_426(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetAnimationSettings_427(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetClosedPosition_428(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetDatabase_429(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetOpenedPosition_430(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetPosition_431(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetRoot_432(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetTrigger_433(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ResetValue_434(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ReversedProgress_435(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Right_436(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Rotate_437(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RotateBy_438(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RotateFrom_439(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RotateMode_440(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RotateTo_441(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RuntimeOptions_442(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_RuntimePreset_443(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Save_444(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SaveAs_445(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SavePreset_446(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Scale_447(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ScaleBy_448(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ScaleFrom_449(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ScaleTo_450(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Scene_451(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SceneActivationDelay_452(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SceneBuildIndex_453(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SceneLoad_454(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SceneName_455(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SceneUnload_456(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ScriptingDefineSymbol_457(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Search_458(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SearchForCategories_459(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SearchForDatabases_460(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SearchForUIPopupLinks_461(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Seconds_462(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SecondsDelay_463(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Select_464(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SelectButton_465(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SelectSwipeDirection_466(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SelectedLoopAnimation_467(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Send_468(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SendGameEvent_469(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SendGameEvents_470(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetActiveNode_471(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetAsSoundName_472(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetBoolValueTo_473(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetFloatValueTo_474(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetIntValueTo_475(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetPosition_476(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetTargetGameObject_477(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetUIButtonToListenFor_478(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetUIDrawerToListenFor_479(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetUIViewToListenFor_480(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SetValue_481(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Settings_482(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Show_483(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowAnimationWillNotWork_484(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowCurveModifier_485(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowPopup_486(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowProgressor_487(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowView_488(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ShowViews_489(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Simulate_490(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SocialLinks_491(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Socket_492(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Sort_493(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SortDatabase_494(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SortingSteps_495(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Sound_496(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundAction_497(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundDatabases_498(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundName_499(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundNodeName_500(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundSource_501(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Sounds_502(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Soundy_503(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundyDatabase_504(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SoundySettings_505(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SourceName_506(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Sources_507(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SpatialBlend_508(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Speed_509(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StartDelay_510(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StartNodeName_511(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StopAllSounds_512(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StopAnimation_513(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StopBehavior_514(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_StopSound_515(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SubGraph_516(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SubGraphNodeName_517(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Suffix_518(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SupportEmail_519(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SwipeDirection_520(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SwipeLength_521(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SwipeLengthDescription_522(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SwitchBackNodeName_523(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Target_524(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetAnimator_525(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetAnimatorDoesNotHaveAnAnimatorController_526(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetAnimatorDoesNotHaveAnyParameters_527(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetCanvas_528(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetFsm_529(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetGameObject_530(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetLabel_531(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetOrientation_532(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetProgress_533(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetTimeScale_534(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetValue_535(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TargetVariable_536(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Text_537(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TextLabel_538(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TextMeshPro_539(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TextMeshProLabel_540(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Time_541(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TimeScaleNodeName_542(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleComponentBehaviors_543(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleLabel_544(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleOFF_545(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleON_546(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleProgressor_547(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleSettings_548(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ToggleSupportForThirdPartyPlugins_549(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TouchySettings_550(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TriggerAction_551(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TriggerEventsAfterAnimation_552(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_TriggerName_553(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UIDrawerNodeName_554(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UINodeNodeName_555(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UIPopupDatabase_556(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnityEvent_557(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnityEvents_558(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnloadSceneNodeName_559(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnmuteAllSounds_560(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnpauseAllSounds_561(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Up_562(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdateContainer_563(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdateEffect_564(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdateOnHide_565(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdateOnShow_566(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdatePopupName_567(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdatePopupPrefab_568(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UpdateValue_569(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UseBackButtonDescription_570(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UseCustomFromAndTo_571(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UseMultiplier_572(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UsefulLinks_573(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Value_574(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Version_575(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Vibrato_576(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ViewCategory_577(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ViewName_578(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_VirtualButton_579(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_VolumeDb_580(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WaitFor_581(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WaitForAnimationToFinish_582(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WaitForSceneToUnload_583(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WaitNodeName_584(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Warning_585(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Weight_586(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WhenClosed_587(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WhenUIDrawerIsClosed_588(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WhenUIPopupIsHiddenDisable_589(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WhenUIViewIsHiddenDisable_590(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WholeNumbers_591(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Width_592(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_X_593(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Y_594(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_Yes_595(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_YouAreResponsibleToUpdateYourCode_596(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_YouCanOnlyAddPrefabsToDatabase_597(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_YouTube_598(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingTargetFsmMessage_599(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SelectListenerToActivateMessage_600(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_HowToUsePlaymakerEventDispatcherMessage_601(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingDrawerNameTitle_602(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingDrawerNameMessage_603(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingGameEventTitle_604(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingGameEventMessage_605(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingSceneNameTitle_606(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_MissingSceneNameMessage_607(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WrongSceneBuildIndexTitle_608(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_WrongSceneBuildIndexMessage_609(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DoubleClickNodeToOpenSubGraphMessage_610(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DuplicateNodeMessage_611(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_DuplicateNodeTitle_612(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoGraphReferencedMessage_613(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoGraphReferencedTitle_614(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSourceConnectedMessage_615(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSourceConnectedTitle_616(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSubGraphReferencedMessage_617(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoSubGraphReferencedTitle_618(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoTargetConnectedMessage_619(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NoTargetConnectedTitle_620(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotConnectedMessage_621(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotConnectedTitle_622(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotListeningForAnyGameEventMessage_623(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotListeningForAnyGameEventTitle_624(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotSendingAnyGameEventMessage_625(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_NotSendingAnyGameEventTitle_626(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ProgressTargetAnimatorParameterInfo_627(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ReferencedGraphIsNotSubGraphMessage_628(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ReferencedGraphIsNotSubGraphTitle_629(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ReferencedGraphIsSubGraphMessage_630(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ReferencedGraphIsSubGraphTitle_631(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SomeProgressTargetsGetUpdatedOnlyInPlayMode_632(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SupportForMasterAudioNotEnabled_633(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SupportForPlaymakerNotEnabled_634(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_SupportForTextMeshProNotEnabled_635(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_ThisClassShouldBeExtended_636(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnnamedNodeMessage_637(),
	UILanguagePack_tE7D8D00757F959D5424D6E03F460B617D47A0D29::get_offset_of_UnnamedNodeTitle_638(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (UnityResources_t30DB39338ECD04577D96032EEF6FF8552A21C9AF), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
